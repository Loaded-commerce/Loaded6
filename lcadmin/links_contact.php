<?php
/*
  $Id: links_contact.php,v 1.00 2003/10/03 Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if ( ($action == 'send_email_to_user') && isset($_POST['link_partners_email_address']) && !isset($_POST['back_x']) ) {
    switch ($_POST['link_partners_email_address']) {
      case '***':
        $mail_query = tep_db_query("select distinct links_contact_name, links_contact_email from " . TABLE_LINKS);
        $mail_sent_to = TEXT_ALL_LINK_PARTNERS;
        break;
      default:
        $link_partners_email_address = tep_db_prepare_input($_POST['link_partners_email_address']);

        $mail_query = tep_db_query("select links_contact_email, links_contact_name from " . TABLE_LINKS . " where links_contact_email = '" . tep_db_input($link_partners_email_address) . "'");
        $mail_sent_to = $_POST['link_partners_email_address'];
        break;
    }

    $from = tep_db_prepare_input($_POST['from']);
    $subject = tep_db_prepare_input($_POST['subject']);
    $message = tep_db_prepare_input($_POST['message']);


    if (EMAIL_USE_HTML == "false") {
	  $email_content = $message;
    } else {
	  $email_content['text'] = strip_tags($message);
	  $email_content['html'] = nl2br($message);
    }


    while ($mail = tep_db_fetch_array($mail_query)) {
	  tep_mail($mail['links_contact_name'], $mail['links_contact_email'], $subject, $email_content, $from, STORE_OWNER_EMAIL_ADDRESS);
    }

    tep_redirect(tep_href_link(FILENAME_LINKS_CONTACT, 'mail_sent_to=' . urlencode($mail_sent_to)));
  }

  if ( ($action == 'preview') && !isset($_POST['link_partners_email_address']) ) {
    $messageStack->add('search', ERROR_NO_LINK_PARTNER_SELECTED, 'error');
  }

  if (isset($_GET['mail_sent_to'])) {
    $messageStack->add('search', sprintf(NOTICE_EMAIL_SENT_TO, $_GET['mail_sent_to']), 'success');
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <?php
    if ($messageStack->size('link_categories') > 0) {
      echo $messageStack->output('link_categories');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-linkcontacts" class="table-linkcontacts">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">

<table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  if ( ($action == 'preview') && isset($_POST['link_partners_email_address']) ) {
    switch ($_POST['link_partners_email_address']) {
      case '***':
        $mail_sent_to = TEXT_ALL_LINK_PARTNERS;
        break;
      default:
        $mail_sent_to = $_POST['link_partners_email_address'];
        break;
    }
?>
          <tr><?php echo tep_draw_form('mail', FILENAME_LINKS_CONTACT, 'action=send_email_to_user'); ?>
            <td><table border="0" width="100%" cellpadding="0" cellspacing="2">
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo TEXT_LINK_PARTNER; ?></b><br><?php echo $mail_sent_to; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo TEXT_FROM; ?></b><br><?php echo htmlspecialchars(stripslashes($_POST['from'])); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo TEXT_SUBJECT; ?></b><br><?php echo htmlspecialchars(stripslashes($_POST['subject'])); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo TEXT_MESSAGE; ?></b><br><?php echo nl2br(htmlspecialchars(stripslashes($_POST['message']))); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td>
<?php
/* Re-Post all POST'ed variables */
    reset($_POST);
	foreach($_POST as $key => $value) {
      if (!is_array($_POST[$key])) {
        echo tep_draw_hidden_field($key, htmlspecialchars(stripslashes($value)));
      }
    }
?>
                <table border="0" width="100%" cellpadding="0" cellspacing="2">
				  <tr>
					<td align="right"><?php if (EMAIL_USE_HTML == 'false') { echo '<button class="btn btn-success btn-sm mt-0 mb-3 mr-2" name="back" type="button" onclick="javascript:editlinkcontact()"> Edit </button>'; } ?><?php echo '<a class="btn btn-default btn-sm mt-0 mb-3 mr-2" href="' . tep_href_link(FILENAME_LINKS_CONTACT) . '">' . IMAGE_CANCEL . '</a><button class="btn btn-success btn-sm mt-0 mb-3" type="submit">' . IMAGE_SEND_EMAIL . '</button>' ; ?></td>
				  </tr>
                </table></td>
              </tr>
            </table></td>
          </form></tr>
<?php
  } else {
?>
          <tr><?php echo tep_draw_form('mail', FILENAME_LINKS_CONTACT, 'action=preview'); ?>
            <td><table border="0" cellpadding="3" cellspacing="2">
              <?php
                $link_partners = array();
                $link_partners[] = array('id' => '', 'text' => TEXT_SELECT_LINK_PARTNER);
                $link_partners[] = array('id' => '***', 'text' => TEXT_ALL_LINK_PARTNERS);

                $mail_query = tep_db_query("select distinct links_contact_email, links_contact_name from " . TABLE_LINKS . " order by links_contact_name");
                while($link_partners_values = tep_db_fetch_array($mail_query)) {
                  $link_partners[] = array('id' => $link_partners_values['links_contact_email'],
                                       'text' => $link_partners_values['links_contact_name'] . ' (' . $link_partners_values['links_contact_email'] . ')');
                }
              ?>
              <tr>
                <td class="form-label mt-3"><?php echo TEXT_LINK_PARTNER; ?></td>
                <td><?php echo tep_draw_pull_down_menu('link_partners_email_address', $link_partners, (isset($_GET['link_partner']) ? $_GET['link_partner'] : ''));?></td>
              </tr>
              <tr>
                <td class="form-label mt-3"><?php echo TEXT_FROM; ?></td>
                <td><?php echo tep_draw_input_field('from', (isset($_POST['from'])?$_POST['from']:STORE_OWNER_EMAIL_ADDRESS)); ?></td>
              </tr>
              <tr>
                <td class="form-label mt-3"><?php echo TEXT_SUBJECT; ?></td>
                <td><?php echo tep_draw_input_field('subject', (isset($_POST['subject'])?$_POST['subject']:'')); ?></td>
              </tr>
              <tr>
                <td valign="top" class="form-label mt-3"><?php echo TEXT_MESSAGE; ?></td>
                <td><?php echo tep_draw_textarea_field('message', 'soft', '60', '15', (isset($_POST['message'])?$_POST['message']:''), 'class="ckeditor"'); ?></td>
              </tr>
              <tr>
                <td colspan="2" align="right"><button type="submit" class="btn btn-success"><?php echo IMAGE_SEND_EMAIL; ?></button></td>
              </tr>
            </table></td>
          </form></tr>
<?php
  }
?>
<!-- body_text_eof //-->
        </table>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script language="javascript">
function editlinkcontact() {
	document.mail.action = '<?php echo tep_href_link(FILENAME_LINKS_CONTACT)?>';
	document.mail.submit();
}
</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
