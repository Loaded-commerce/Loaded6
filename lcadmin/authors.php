<?php
/*
  $Id: authors.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {
      case 'insert':
      case 'save':
        if (isset($_GET['auID'])) $authors_id = tep_db_prepare_input($_GET['auID']);
        $authors_name = tep_db_prepare_input($_POST['authors_name']);

        $sql_data_array = array('authors_name' => $authors_name);

        if ($action == 'insert') {
          $insert_sql_data = array('date_added' => 'now()');

          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

          tep_db_perform(TABLE_AUTHORS, $sql_data_array);
          $authors_id = tep_db_insert_id();
        } elseif ($action == 'save') {
          $update_sql_data = array('last_modified' => 'now()');

          $sql_data_array = array_merge($sql_data_array, $update_sql_data);

          tep_db_perform(TABLE_AUTHORS, $sql_data_array, 'update', "authors_id = '" . (int)$authors_id . "'");
        }

        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $authors_desc_array = $_POST['authors_description'];
          $authors_url_array = $_POST['authors_url'];
          $language_id = $languages[$i]['id'];

          $sql_data_array = array('authors_description' => tep_db_prepare_input($authors_desc_array[$language_id]),
                                  'authors_url' => tep_db_prepare_input($authors_url_array[$language_id]));

          if ($action == 'insert') {
            $insert_sql_data = array('authors_id' => $authors_id,
                                     'languages_id' => $language_id);

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            tep_db_perform(TABLE_AUTHORS_INFO, $sql_data_array);
          } elseif ($action == 'save') {
            tep_db_perform(TABLE_AUTHORS_INFO, $sql_data_array, 'update', "authors_id = '" . (int)$authors_id . "' and languages_id = '" . (int)$language_id . "'");
          }
        }

        if (USE_CACHE == 'true') {
          tep_reset_cache_block('authors');
        }

		  $mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
		  if ($mode == 'save') {
			tep_redirect(tep_href_link(FILENAME_AUTHORS, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . '&auID=' . $authors_id));
		  } else {  // save & stay
			tep_redirect(tep_href_link(FILENAME_AUTHORS, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . '&auID=' . $authors_id . '&action=edit'));
		  }

        break;
      case 'deleteconfirm':
        $authors_id = tep_db_prepare_input($_GET['auID']);

        tep_db_query("delete from " . TABLE_AUTHORS . " where authors_id = '" . (int)$authors_id . "'");
        tep_db_query("delete from " . TABLE_AUTHORS_INFO . " where authors_id = '" . (int)$authors_id . "'");

        if (isset($_POST['delete_articles']) && ($_POST['delete_articles'] == 'on')) {
          $articles_query = tep_db_query("select articles_id from " . TABLE_ARTICLES . " where authors_id = '" . (int)$authors_id . "'");
          while ($articles = tep_db_fetch_array($articles_query)) {
            tep_remove_article($articles['articles_id']);
          }
        } else {
          tep_db_query("update " . TABLE_ARTICLES . " set authors_id = '' where authors_id = '" . (int)$authors_id . "'");
        }

        if (USE_CACHE == 'true') {
          tep_reset_cache_block('authors');
        }

        tep_redirect(tep_href_link(FILENAME_AUTHORS, 'page=' . $_GET['page']));
        break;
    }
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');

if( ($action == 'new' or $action == 'edit')  && ARTICLE_WYSIWYG_ENABLE == 'Enable' ){
    echo tep_load_html_editor();
    $languages = tep_get_languages();
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $authors_description .= 'authors_description[' . $languages[$i]['id'] . '],';
    }
    echo tep_insert_html_editor($authors_description,'advanced');
}
  if ($action == 'new') {
      $title = TEXT_HEADING_NEW_AUTHOR;
  } elseif ($action == 'edit') {
      $title = TEXT_HEADING_EDIT_AUTHOR;
  } elseif ($action == 'preview') {

    $authors_query = tep_db_query("select authors_id, authors_name from " . TABLE_AUTHORS . " where authors_id = '" . $_GET['auID'] . "'");
    $authors = tep_db_fetch_array($authors_query);

    $title = TEXT_ARTICLE_BY . $authors['authors_name'];
  } else {
      $title = HEADING_TITLE;
  }
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo $title; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <?php
    if ($messageStack->size('authors') > 0) {
      echo $messageStack->output('authors');
    }
    ?>

<?php
if ($action == 'new' || $action == 'edit')
{
	echo '<form id="authors" name="authors" method="post" enctype="multipart/form-data" data-parsley-validate>';
?>
      <!-- begin button bar -->
      <div id="button-bar" class="row">
        <div class="col-9 m-b-10 w-100 pt-1 pl-0 pr-0">
            <a href="<?php echo tep_href_link(FILENAME_AUTHORS, (isset($_GET['page'])?'page='.$_GET['page']:'')); ?>" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> <?php echo ((isset($cID) && empty($cID) === false) ? BUTTON_RETURN_TO_LIST :  IMAGE_CANCEL); ?></a>
            <button type="submit" onclick="updateAuthor('save');" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo BUTTON_SAVE; ?></button>
            <button type="submit" onclick="updateAuthor('stay');" class="btn btn-info m-r-3 btn-save-stay"><i class="fa fa-plus-square"></i> <?php echo BUTTON_SAVE_STAY; ?></button>
        </div>
		<div class="col-3 m-b-10 pt-1 pr-2">
		  <div class="btn-group pull-right dark"> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class="btn btn-white dropdown-toggle"> <span id="langDropdownTitle"><?php echo ucwords($_SESSION['language']); ?></span> <span class="caret"></span> </a>
			<ul class="dropdown-menu pull-right" id="langselector" style="left:-50px!important;">
			  <?php
			  $languages = tep_get_languages();
			  for ($i=0; $i<sizeof($languages); $i++) {
				?>
				<li id="langsel_<?php echo $languages[$i]['id']; ?>" class="langval<?php echo (($languages[$i]['id'] == $_SESSION['languages_id'])? ' active':'');?>">
				  <a aria-expanded="false" href="javascript:changeLang('page', '<?php echo $languages[$i]['name'];?>', <?php echo $languages[$i]['id']; ?>)"><?php echo '<span class="ml-2">' . $languages[$i]['name'];?></span></a>
				</li>
				<?php
			  }
			  ?>
			</ul>
		  </div>
		</div>
	  </div>
      <!-- end button bar -->
<?php
}
?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-authors" class="table-authors">
        <div class="row">

<?php
if ($action == 'new')
{
?>
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
<?php
  $languages = tep_get_languages();
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
		$display = ($languages[$i]['id'] == $_SESSION['languages_id']) ? '' : 'display:none;'
?>
			<div style="<?php echo $display; ?>" class="page-lang-pane <?php echo (($languages[$i]['id'] == $_SESSION['languages_id']) ? 'active' : '');?>" id="page-default-pane-<?php echo $languages[$i]['id'];?>">
			  <div class="ml-2 mr-2">
				<div class="main-heading"><span>Author's Info</span>
				  <div class="main-heading-footer"></div>
				</div>

				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_AUTHORS_DESCRIPTION; ?></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php echo tep_draw_textarea_field('authors_description[' . $languages[$i]['id'] . ']', 'soft', '70', '15', '', 'class="form-control ckeditor"'); ?>
				  </div>
				</div>
				  <div class="form-group row mb-3">
					<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_AUTHORS_URL; ?></label>
					<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					  <?php echo tep_draw_input_field('authors_url[' . $languages[$i]['id'] . ']', '', 'id="author_url_' . $languages[$i]['id'] . '" class="form-control" placeholder="www.domain.com" data-parsley-pattern="^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$"'); ?>
					</div>
					<div class="col-xs-1 pl-1 m-t-1"><button onclick="goToUrl('<?php echo $languages[$i]['id']; ?>', 'author'); return false;" class="btn btn-white btn-xs"><i class="fa fa-external-link"></i></button></div>
				  </div>

               </div>
              </div>

<?php
  }

?>
	</div>
	<div class="col-md-3 col-xl-2 panel-right rounded-right light">
		<div class="sidebar-container p-4"><div class="sidebar-heading"><span><div class="text-truncate">Options</div></span></div>
		<div class="sidebar-heading-footer"></div>
			<div class="sidebar-content-container">
			<div class="form-group row mt-3">
			  <label class="col-sm-5 control-label sidebar-edit pr-0 c-pointer text-muted mt-2"><?php echo TEXT_AUTHORS_NAME; ?><span class="required"></span></label>
			  <div class="col-sm-7 p-relative"><?php echo tep_draw_input_field('authors_name', '', 'required'); ?></div>
			</div>
		  </div>
    </div>
  </form>
<?php
  } elseif ($action == 'edit') {

?>
          <div class="col-md-8 col-xl-9 dark panel-left rounded-left">
<?
    $authors_query = tep_db_query("select authors_id, authors_name from " . TABLE_AUTHORS . " where authors_id = '" . $_GET['auID'] . "'");
    $authors = tep_db_fetch_array($authors_query);

  $languages = tep_get_languages();
  for ($i=0, $n=sizeof($languages); $i<$n; $i++)
  {
	$display = ($languages[$i]['id'] == $_SESSION['languages_id']) ? '' : 'display:none;'
?>

              <div style="<?php echo $display; ?>" class="tab-page" id="<?php echo $languages[$i]['name'];?>">
			  <div class="ml-2 mr-2">
				<div class="main-heading"><span>Author's Info</span>
				  <div class="main-heading-footer"></div>
				</div>

				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_AUTHORS_DESCRIPTION; ?></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php echo tep_draw_textarea_field('authors_description[' . $languages[$i]['id'] . ']', 'soft', '70', '15', tep_get_author_description($authors['authors_id'], $languages[$i]['id']), 'class="form-control ckeditor"'); ?>
				  </div>
				</div>
				  <div class="form-group row mb-3">
					<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_AUTHORS_URL; ?></label>
					<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					  <?php echo tep_draw_input_field('authors_url[' . $languages[$i]['id'] . ']', tep_get_author_url($authors['authors_id'], $languages[$i]['id']), 'id="author_url_' . $languages[$i]['id'] . '" class="form-control" placeholder="www.domain.com" data-parsley-pattern="^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$"'); ?>
					</div>
					<div class="col-xs-1 pl-1 m-t-1"><button onclick="goToUrl('<?php echo $languages[$i]['id']; ?>', 'author'); return false;" class="btn btn-white btn-xs"><i class="fa fa-external-link"></i></button></div>
				  </div>

               </div>
              </div>

<?php
   }

?>

	</div>
	<div class="col-md-4 col-xl-3 panel-right rounded-right light">
		<div class="sidebar-container p-4"><div class="sidebar-heading"><span><div class="text-truncate">Options</div></span></div>
		<div class="sidebar-heading-footer"></div>
			<div class="sidebar-content-container">
			<div class="form-group row mt-3">
			  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo TEXT_AUTHORS_NAME; ?><span class="required"></span></label>
			  <div class="col-sm-7 p-relative"><?php echo tep_draw_input_field('authors_name', $authors['authors_name'], 'required'); ?></div>
			</div>
		  </div>
    </div>
  </form>
<?php
  } else {
?>
	  <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
      	<table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_AUTHORS; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
             </thead>
<?php
  $authors_query_raw = "select authors_id, authors_name, date_added, last_modified from " . TABLE_AUTHORS . " order by authors_name";
  $authors_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $authors_query_raw, $authors_query_numrows);
  $authors_query = tep_db_query($authors_query_raw);
  while ($authors = tep_db_fetch_array($authors_query)) {
    if ((!isset($_GET['auID']) || (isset($_GET['auID']) && ($_GET['auID'] == $authors['authors_id']))) && !isset($auInfo) && (substr($action, 0, 3) != 'new')) {
      $author_articles_query = tep_db_query("select count(*) as articles_count from " . TABLE_ARTICLES . " where authors_id = '" . (int)$authors['authors_id'] . "'");
      $author_articles = tep_db_fetch_array($author_articles_query);

     $auInfo_array = array_merge($authors, $author_articles);
      $auInfo = new objectInfo($auInfo_array);
    }

	$selected = (isset($auInfo) && is_object($auInfo) && ($authors['authors_id'] == $auInfo->authors_id))? true : false;
	$col_selected = ($selected) ? ' selected' : '';
    if ($selected) {
      echo '              <tr id="defaultSelected" class="table-row dark selected"onclick="document.location.href=\'' . tep_href_link(FILENAME_AUTHORS, 'page=' . $_GET['page'] . '&auID=' . $authors['authors_id'] . '&action=edit') . '\'">' . "\n";
    } else {
      echo '              <tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_AUTHORS, 'page=' . $_GET['page'] . '&auID=' . $authors['authors_id']) . '\'">' . "\n";
    }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $authors['authors_name']; ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if (isset($auInfo) && is_object($auInfo) && ($authors['authors_id'] == $auInfo->authors_id)) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>' ; } else { echo '<a href="' . tep_href_link(FILENAME_AUTHORS, 'page=' . $_GET['page'] . '&auID=' . $authors['authors_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>
              </table>
              <table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $authors_split->display_count($authors_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_AUTHORS); ?></td>
                    <td class="smallText" align="right"><?php echo $authors_split->display_links($authors_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table>
              </table>
<?php
  if (empty($action)) {
?>             <table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" colspan="2" class="smallText"><?php echo '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_AUTHORS, 'page=' . $_GET['page'] . '&action=new') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>'; ?></td>
              </tr>
              </table>
<?php
  }
?>
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'delete':
      $heading[] = array('text' => TEXT_HEADING_DELETE_AUTHOR);

      $contents = array('form' => tep_draw_form('authors', FILENAME_AUTHORS, 'page=' . $_GET['page'] . '&auID=' . $auInfo->authors_id . '&action=deleteconfirm'));
      $str_msg = TEXT_DELETE_INTRO .'<br><b>' . $auInfo->authors_name . '</b>';

      if ($auInfo->articles_count > 0) {
        $str_msg .= '<br><br>' . tep_draw_checkbox_field('delete_articles') . ' ' . TEXT_DELETE_ARTICLES;
        $str_msg .= '<br><br>' . sprintf(TEXT_DELETE_WARNING_ARTICLES, $auInfo->articles_count);
      }

      $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'. $str_msg . '</p></div></div></div>');
      $contents[] = array('align' => 'center', 'text' => '<br><button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_DELETE . '</button> <a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_AUTHORS, 'page=' . $_GET['page'] . '&auID=' . $auInfo->authors_id) . '">' . IMAGE_CANCEL . '</a>');
      break;
    default:
      if (isset($auInfo) && is_object($auInfo)) {
        $heading[] = array('text' => $auInfo->authors_name );

        $contents[] = array('align' => 'center', 'text' => '<a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_AUTHORS, 'page=' . $_GET['page'] . '&auID=' . $auInfo->authors_id . '&action=edit') . '">' . IMAGE_EDIT . '</a> <a class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_AUTHORS, 'page=' . $_GET['page'] . '&auID=' . $auInfo->authors_id . '&action=delete') . '">' . IMAGE_DELETE . '</a>');
        $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_DATE_ADDED . '<span class="sidebar-title ml-2">'  . tep_date_short($auInfo->date_added) . '</span></div>');
        if (tep_not_null($auInfo->last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-1">'.TEXT_LAST_MODIFIED . '<span class="sidebar-title ml-2">'  . tep_date_short($auInfo->last_modified) . '</span></div>');
        $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_ARTICLES . '<span class="sidebar-title ml-2">' . $auInfo->articles_count . '</span></div>');
      }
      break;
  }

	  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
		$box = new box;
		echo $box->showSidebar($heading, $contents);
	  }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script language="javascript">
function changeLang(pagetype, langName, langId) {
    $('#langDropdownTitle').html(langName);
    $(".langval").removeClass("active");
    $("#langsel_"+langId).addClass("active");
	$( "."+ pagetype +"-lang-pane" ).hide( "slow" );
	$( "#"+ pagetype +"-default-pane-"+langId ).show( "slow" );
}

function updateAuthor(mode) {
  var action = '<?php echo str_replace('&amp;', '&', tep_href_link(FILENAME_AUTHORS, (isset($_GET['page']) ? '&page=' . $_GET['page'] : '') . (isset($_GET['auID']) ? '&auID=' . $_GET['auID'] : '') . '&action=' . (isset($_GET['auID']) ? 'save' : 'insert'))); ?>';
  // set the save mode in hidden form input
  $('<input />').attr('type', 'hidden')
      .attr('name', "mode")
      .attr('value', mode)
      .appendTo('#authors');

  $('#authors').attr('action', action).submit();
}
function goToUrl(lang, mode) {
  var url = ($('#'+ mode +'_url_' + lang).val()).replace('https://', '').replace('http://', '');

  if (url == '' || url == undefined) {
    swal("Oh Crap!", "Product URL is Empty!", "error");
    return false;
  }
  if (url.indexOf('.') === -1) {
    swal("Awe Geez!", "The domain is invalid!", "error");
    return false;
  }

  window.open('http://' + url);
}
</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
