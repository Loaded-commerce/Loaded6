<?php
/*
  $Id: create_order_payment.php,v 1.1 2004/08/19 23:38:51 teo Exp $
  http://www.chainreactionworks.com

  Copyright (c) 2005 chainreactionworks.com

  Released under the GNU General Public License
*/

Header("Cache-control: private, no-cache");
Header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); # Past date
Header("Pragma: no-cache");
  require('includes/application_top.php');
  
  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {

      case 'save':
      if (isset($_GET['payID'])) $pay_methods_id = tep_db_prepare_input($_GET['payID']);
       //print_r(

         $languages = tep_get_languages();
      for ($i=0; $i<sizeof($languages); $i++) {
            $language_id = $languages[$i]['id'] ;
               // echo 'pay_method ' . $pay_method[$language_id] . '<br>';  
                     $pay_methods_id = tep_db_prepare_input($_GET['payID']) ;
                     
                 $sql_data_array1 = array('pay_method' => tep_db_prepare_input($_POST['pay_method'][$language_id]),
                                      'pay_method_language'=> tep_db_prepare_input($language_id),
                                            'pay_method_sort'=> tep_db_prepare_input($_POST['pay_method_sort']));
                 
             tep_db_perform(TABLE_ORDERS_PAY_METHODS, $sql_data_array1, 'update', 'pay_methods_id = \'' . $pay_methods_id . '\'and pay_method_language = \'' . $language_id . '\'');

             } 
        //    tep_redirect(tep_href_link(FILENAME_CREATE_ORDERS_PAY,'page=' . $_GET['page'] . '&payID=' . $pay_methods_id));
              break;
      case 'insert':
          if ($action == 'insert') {
          if (isset($_GET['payID'])) $pay_methods_id = tep_db_prepare_input($_GET['payID']);
          if (empty($pay_methods_id)) {
            $next_id_query = tep_db_query("select max(pay_methods_id) as pay_methods_id from " . TABLE_ORDERS_PAY_METHODS . "");
            $next_id = tep_db_fetch_array($next_id_query);
            $pay_methods_id = $next_id['pay_methods_id'] + 1;
          }
          $languages = tep_get_languages();
          
          for ($i = 0; $i < sizeof($languages); $i++) {
            $language_id=$languages[$i]['id'];
            $sql_data_array = array('pay_method_sort' => tep_db_prepare_input($_POST['sort'][$language_id]),
                                    'pay_method' => tep_db_prepare_input($_POST['pay_method'][$language_id]),
                                    'date_added' => 'now()');
   
           $insert_sql_data = array('pay_methods_id' => $pay_methods_id,
                                    'pay_method_language' => $language_id);
           $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
           tep_db_perform(TABLE_ORDERS_PAY_METHODS, $sql_data_array);
         }
      }
      tep_redirect(tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] ));
        break;
      case 'deleteconfirm':
        $payID = tep_db_prepare_input($_GET['payID']);
        tep_db_query("delete from " . TABLE_ORDERS_PAY_METHODS . " where pay_methods_id = '" . tep_db_input($payID) . "'");

        tep_redirect(tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page']));
        break;
      case 'delete':
        $payID = tep_db_prepare_input($_GET['payID']);

           $remove_status = true;
         
        break;
    }
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-countries" class="table-countries">
        <div class="row">
          <div class="col-md-8 col-xl-9 dark panel-left rounded-left">
          	<table border="0" width="100%" cellspacing="0" cellpadding="2" class="table table-hover w-100 mt-2">
          		<thead>
					<tr class="th-row">
					  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CREATE_ORDERS_ADMIN; ?></th>
                	  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CREATE_ORDERS_SORT; ?></th>
                	  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              		</tr>
          		</thead>
          		<tbody>
<?php
  $payment_module_query_raw = "select pay_methods_id, pay_method_language, pay_method_sort, pay_method, date_added from " . TABLE_ORDERS_PAY_METHODS . " where pay_method_language ='" . $languages_id . "' order by pay_methods_id";
  $payment_module_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $payment_module_query_raw, $payment_module_query_numrows);
  $payment_module_query = tep_db_query($payment_module_query_raw);
  while ($payment_module = tep_db_fetch_array($payment_module_query)) {
    if ((!isset($_GET['payID']) || (isset($_GET['payID']) && ($_GET['payID'] == $payment_module['pay_methods_id']))) && !isset($oInfo) && (substr($action, 0, 3) != 'new')) {
      $oInfo = new objectInfo($payment_module);
    }

	$selected = (isset($oInfo) && is_object($oInfo) && ($payment_module['pay_methods_id'] == $oInfo->pay_methods_id))? true : false;
	$col_selected = ($selected) ? ' selected' : '';
    if ($selected) {
      echo '                  <tr id="defaultSelected" class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&payID=' . $oInfo->pay_methods_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '                  <tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&payID=' . $payment_module['pay_methods_id']) . '\'">' . "\n";
    }
    
      echo '                <td class="table-col dark text-left'. $col_selected .'">' . $payment_module['pay_method'] . '</td>' . "\n";
      echo '                <td class="table-col dark text-left'. $col_selected .'">' . $payment_module['pay_method_sort'] . '</td>' . "\n";

?>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if (isset($oInfo) && is_object($oInfo) && ($payment_module['pay_methods_id'] == $oInfo->pay_methods_id)) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&payID=' . $payment_module['pay_methods_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>
          		</tbody>
              </table>
              <table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $payment_module_split->display_count($payment_module_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PAYMENT_MODULES); ?></td>
                    <td class="smallText" align="right"><?php echo $payment_module_split->display_links($payment_module_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
<?php
  if (empty($action)) {
?>
                  <tr>
                    <td colspan="2" align="right"><?php echo '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&action=new') . '">' . IMAGE_INSERT . '</a>'; ?></td>
                  </tr>
<?php
  }
?>
                </table>
          </div>
          <div class="col-md-4 col-xl-3 dark panel-right rounded-right">                
                
<?php
  $heading = array();
  $contents = array();
  $payment_module_inputs_string = (isset($payment_module_inputs_string) ? $payment_module_inputs_string : '');
  switch ($action) {
    case 'new':
      $heading[] = array('text' => TEXT_INFO_HEADING_NEW_PAYMENT);

      $contents[] = array('form' => tep_draw_form('status', FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&action=insert', 'post')) ; 
      $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-3">'. TEXT_INFO_INSERT_INTRO .'</div>');

      $payment_module_inputs_string = '';
      $languages = tep_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $payment_module_inputs_string .= '<br>' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('pay_method[' . $languages[$i]['id'] . ']');
      }

      $contents[] = array('text' => '<label class="control-label sidebar-edit mb-0">' . TEXT_INFO_PAYMENT_MODULES_NAME .'</label>'. $payment_module_inputs_string);
      $contents[] = array('text' => '<label class="control-label sidebar-edit mb-0">' . tep_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT .'</label>');
      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm" type="submit">' . IMAGE_INSERT . '</button><a class="btn btn-default btn-sm ml-2" href="' . tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page']) . '">' . IMAGE_CANCEL . '</a></div>');
      break;
    case 'edit':
     $heading[] = array('text' => TEXT_INFO_HEADING_EDIT_PAYMENT );
     $contents[] = array('form' => tep_draw_form('status', FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&payID=' . $oInfo->pay_methods_id  . '&action=save')) ; 
     $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-3">'. TEXT_INFO_EDIT_INTRO .'</div>');
       $languages = tep_get_languages();
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
    
      $payment_module_inputs_string .= '<br>'. tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('pay_method[' . $languages[$i]['id'] . ']', tep_get_pay_method($_GET['payID'] . '[' . $languages[$i]['id'] . ']' ));
    }
    $payment_module_inputs_string .= '<label class="control-label sidebar-edit mb-0">' . TEXT_INFO_SORT_ORDER . '</label> &nbsp;'. tep_draw_input_field('pay_method_sort' , $oInfo->pay_method_sort);
    $contents[] = array('text' => '<label class="control-label sidebar-edit mb-0">' . TEXT_INFO_PAYMENT_MODULES_NAME .'</label>'. $payment_module_inputs_string);
    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm" type="submit">' . IMAGE_UPDATE . '</button><a class="btn btn-default btn-sm ml-2" href="' . tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&payID=' . $oInfo->pay_methods_id) . '">' . IMAGE_CANCEL . '</a></div>');
      break;
    
    case 'delete':
      $heading[] = array('text' => '<b>' . TEXT_INFO_DELETE_INTRO . '</b>');

      $contents[] = array('form' => tep_draw_form('status', FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&payID=' . $oInfo->pay_methods_id  . '&action=deleteconfirm')) ; 
      $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0 fw-400">'. TEXT_INFO_DELETE_INTRO .'<br><b>' . $oInfo->pay_method . '</b></div></div></div>');
      if ($remove_status) $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-danger btn-sm" type="submit">' . IMAGE_DELETE . '</button> <a class="btn btn-default btn-sm ml-2" href="' . tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&payID=' . $oInfo->pay_methods_id) . '">' . IMAGE_CANCEL . '</a></div>');
      break;
    default:
      if (isset($oInfo) && is_object($oInfo)) {
        $heading[] = array('text' => $oInfo->pay_method );

        $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-success btn-sm ml-2" href="' . tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&payID=' . $oInfo->pay_methods_id . '&action=edit') . '">' . IMAGE_EDIT . '</a> <a class="btn btn-danger btn-sm ml-2" href="' . tep_href_link(FILENAME_CREATE_ORDERS_PAY, 'page=' . $_GET['page'] . '&payID=' . $oInfo->pay_methods_id . '&action=delete') . '">' . IMAGE_DELETE . '</a></div>');

        $payment_module_inputs_string = '';
        $languages = tep_get_languages();
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $payment_module_inputs_string .= '<br>' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_get_pay_method($oInfo->pay_methods_id, $languages[$i]['id']);
       }

        $contents[] = array('text' => '<div class="sidebar-text mt-3">'.$payment_module_inputs_string.'</div>');
      }
      break;
  }

	  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
		$box = new box;
		echo $box->showSidebar($heading, $contents);
	  }
?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>