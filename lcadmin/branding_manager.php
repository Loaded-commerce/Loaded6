<?php
/*
  $Id: logo_manager.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

if(!function_exists('cre_db_update')){
  function cre_db_update($table, $data, $link = 'db_link') {
    reset($data);
     $query = 'REPLACE INTO ' . $table . ' set ';
	  foreach($data as $columns=>$value) {
        switch ((string)$value) {
          case 'now()':
            $query .= $columns . ' = now(), ';
            break;
          case 'null':
            $query .= $columns .= ' = null, ';
            break;
          default:
            $query .= $columns . ' = \'' . tep_db_input($value) . '\', ';
            break;
        }
      }
      $query = substr($query, 0, -2) ;

    return tep_db_query($query, $link);
  }
}

//destination for uploaded logos
define('DIR_FS_LOGO',DIR_FS_CATALOG_IMAGES . 'logo/');
define('DIR_WS_LOGO',HTTP_SERVER . DIR_WS_CATALOG . DIR_WS_IMAGES . 'logo/');
  // get languages
$languages = tep_get_languages();
 $_SESSION['languages_id'] = 1;
  if (isset($_GET['action'])) {
  $action = $_GET['action'] ;
  }else if (isset($_POST['action'])){
  $action = $_POST['action'] ;
  } else {
  $action = '' ;
 }

 if (isset($action) && $action == 'upload') {
         $store_brand_telephone = (isset($_POST['store_brand_telephone' . $_SESSION['languages_id']]) ? $_POST['store_brand_telephone' . $_SESSION['languages_id']] : '');
         $store_brand_fax = (isset($_POST['store_brand_fax' . $_SESSION['languages_id']]) ? $_POST['store_brand_fax' . $_SESSION['languages_id']] : '');
         $store_brand_homepage = (isset($_POST['store_brand_homepage' . $_SESSION['languages_id']]) ? $_POST['store_brand_homepage' . $_SESSION['languages_id']] : '');
         $store_brand_image = (isset($_POST['store_brand_image' . $_SESSION['languages_id']]) ? $_POST['store_brand_image' . $_SESSION['languages_id']] : '');
         $store_brand_name = (isset($_POST['store_brand_name' . $_SESSION['languages_id']]) ? $_POST['store_brand_name' . $_SESSION['languages_id']] : '');
         $store_brand_slogan = (isset($_POST['store_brand_slogan' . $_SESSION['languages_id']]) ? $_POST['store_brand_slogan' . $_SESSION['languages_id']] : '');
         $store_brand_support_email = (isset($_POST['store_brand_support_email' . $_SESSION['languages_id']]) ? $_POST['store_brand_support_email' . $_SESSION['languages_id']] : '');
         $store_brand_support_phone = (isset($_POST['store_brand_support_phone' . $_SESSION['languages_id']]) ? $_POST['store_brand_support_phone' . $_SESSION['languages_id']] : '');
         $store_brand_image_existing = (isset($_POST['store_brand_image_existing' . $_SESSION['languages_id']]) ? $_POST['store_brand_image_existing' . $_SESSION['languages_id']] : '');
         $delete_image = (isset($_POST['delete_image' . $_SESSION['languages_id']]) ? $_POST['delete_image' . $_SESSION['languages_id']] : '');

         $store_brand_favicon = (isset($_POST['store_brand_favicon' . $_SESSION['languages_id']]) ? $_POST['store_brand_favicon' . $_SESSION['languages_id']] : '');
         $store_brand_favicon_existing = (isset($_POST['store_brand_favicon_existing' . $_SESSION['languages_id']]) ? $_POST['store_brand_favicon_existing' . $_SESSION['languages_id']] : '');
         $delete_favicon = (isset($_POST['delete_favicon' . $_SESSION['languages_id']]) ? $_POST['delete_favicon' . $_SESSION['languages_id']] : '');

         $store_og_image = (isset($_POST['store_og_image' . $_SESSION['languages_id']]) ? $_POST['store_og_image' . $_SESSION['languages_id']] : '');
         $store_og_image_existing = (isset($_POST['store_og_image_existing' . $_SESSION['languages_id']]) ? $_POST['store_og_image_existing' . $_SESSION['languages_id']] : '');
         $delete_store_og_image = (isset($_POST['delete_store_og_image' . $_SESSION['languages_id']]) ? $_POST['delete_store_og_image' . $_SESSION['languages_id']] : '');

         $facebook_link = (isset($_POST['facebook_link' . $_SESSION['languages_id']]) ? $_POST['facebook_link' . $_SESSION['languages_id']] : '');
         $twitter_link = (isset($_POST['twitter_link' . $_SESSION['languages_id']]) ? $_POST['twitter_link' . $_SESSION['languages_id']] : '');
         $pinterest_link = (isset($_POST['pinterest_link' . $_SESSION['languages_id']]) ? $_POST['pinterest_link' . $_SESSION['languages_id']] : '');
         $google_link = (isset($_POST['google_link' . $_SESSION['languages_id']]) ? $_POST['google_link' . $_SESSION['languages_id']] : '');
         $youtube_link = (isset($_POST['youtube_link' . $_SESSION['languages_id']]) ? $_POST['youtube_link' . $_SESSION['languages_id']] : '');
         $linkedin_link = (isset($_POST['linkedin_link' . $_SESSION['languages_id']]) ? $_POST['linkedin_link' . $_SESSION['languages_id']] : '');
         $store_brand_address = (isset($_POST['store_brand_address' . $_SESSION['languages_id']]) ? cleanHTMLControls($_POST['store_brand_address' . $_SESSION['languages_id']]) : '');
         $custom_css = (isset($_POST['custom_css' . $_SESSION['languages_id']]) ? cleanHTMLControls($_POST['custom_css' . $_SESSION['languages_id']]) : '');
         
         $json_set['default_button_color'] = $_POST['default_button_color'];
		 $json_set['addtocart_button_color'] = $_POST['addtocart_button_color'];
		 $json_set['quick_view_button_color'] = $_POST['quick_view_button_color'];
		 $json_set['display_compare_button'] = $_POST['display_compare_button'];
		 $json_set['viewcart_button_color'] = $_POST['viewcart_button_color'];
		 $json_set['checkout_button_color'] = $_POST['checkout_button_color'];
		 $json_set['prodcut_name_link_color'] = $_POST['prodcut_name_link_color'];
		 $json_set['page_heading_color'] = $_POST['page_heading_color'];
		 $json_set['product_name_size'] = $_POST['product_name_size'];
		 $json_set['page_heading_size'] = $_POST['page_heading_size'];
		 $json_set['top_bar_background_color'] = $_POST['top_bar_background_color'];
		 $json_set['top_bar_link_color'] = $_POST['top_bar_link_color'];
		 $json_set['top_bar_text_color'] = $_POST['top_bar_text_color'];
		 $json_set['top_bar_link_hover_color'] = $_POST['top_bar_link_hover_color'];
		 $json_set['top_main_menu_background_color'] = $_POST['top_main_menu_background_color'];
		 $json_set['top_mainmenu_link_color'] = $_POST['top_mainmenu_link_color'];
		 $json_set['top_mainmenu_link_hover_color'] = $_POST['top_mainmenu_link_hover_color'];
		 $json_set['top_menu_text_color'] = $_POST['top_menu_text_color'];
		 $json_set['footer_background_color'] = $_POST['footer_background_color'];
		 $json_set['footer_text_color'] = $_POST['footer_text_color'];
		 $json_set['footer_link_color'] = $_POST['footer_link_color'];
		 $json_set['footer_link_hover_color'] = $_POST['footer_link_hover_color'];
		 $json_set['infobox_background_color'] = $_POST['infobox_background_color'];
		 $json_set['infobox_heading_background_color'] = $_POST['infobox_heading_background_color'];
		 $json_set['page_background_color'] = $_POST['page_background_color'];
		 $json_set['infobox_shape'] = $_POST['infobox_shape'];
		 $json_set['contentarea_background_color'] = $_POST['contentarea_background_color'];
		 $json_set['contentarea_text_color'] = $_POST['contentarea_text_color'];
		 $json_set['contentarea_link_color'] = $_POST['contentarea_link_color'];
		 $json_set['contentarea_link_hover_color'] = $_POST['contentarea_link_hover_color'];
		 $json_set['infobox_shape_radius_size'] = $_POST['infobox_shape_radius_size'];

		 $json_encode_data = json_encode($json_set);
		//print_r($json_encode_data);exit;


         //delete image
         $deleted_image = false;
         if($delete_image !='' && $store_brand_image_existing !='' || $store_brand_image != ''){
           $image_query = tep_db_query("SELECT store_brand_image FROM " . TABLE_BRANDING_DESCRIPTION . " WHERE store_brand_image = '" . $store_brand_image_existing . "' AND language_id <> '" . $_SESSION['languages_id'] . "'");
           if (tep_db_num_rows($image_query) == 0) {
             unlink(DIR_FS_LOGO . $store_brand_image_existing);
           }
             $delete_logo = "Update " . TABLE_BRANDING_DESCRIPTION . " set store_brand_image = '' where language_id = '" . $_SESSION['languages_id'] . "'";
             tep_db_query($delete_logo);
             $deleted_image = true;
         }

         //delete favicon
         $deleted_favicon = false;
         if($delete_favicon !='' && $store_brand_favicon_existing !='' || $store_brand_favicon != ''){
           $favicon_query = tep_db_query("SELECT store_brand_image FROM " . TABLE_BRANDING_DESCRIPTION . " WHERE store_brand_favicon = '" . $store_brand_favicon_existing . "' AND language_id <> '" . $_SESSION['languages_id'] . "'");
           if (tep_db_num_rows($favicon_query) == 0) {
             unlink(DIR_FS_LOGO . $store_brand_favicon_existing);
           }
             $delete_favicon = "Update " . TABLE_BRANDING_DESCRIPTION . " set store_brand_favicon = '' where language_id = '" . $_SESSION['languages_id'] . "'";
             tep_db_query($delete_favicon);
             $deleted_favicon = true;
         }

         //delete og image
         $deleted_store_og_image = false;
         if($delete_store_og_image !='' && $store_og_image_existing !='' || $store_og_image != ''){
           $favicon_query = tep_db_query("SELECT store_brand_image FROM " . TABLE_BRANDING_DESCRIPTION . " WHERE store_og_image = '" . $store_og_image_existing . "' AND language_id <> '" . $_SESSION['languages_id'] . "'");
           if (tep_db_num_rows($favicon_query) == 0) {
             unlink(DIR_FS_LOGO . $store_og_image_existing);
           }
             $delete_store_og_image = "Update " . TABLE_BRANDING_DESCRIPTION . " set store_og_image = '' where language_id = '" . $_SESSION['languages_id'] . "'";
             tep_db_query($delete_store_og_image);
             $deleted_store_og_image = true;
         }

         @unlink(DIR_FS_LOGO.'custom.css');
         if(trim($custom_css) != "")
         {
         	 $fp = fopen(DIR_FS_LOGO.'custom.css', 'w');
         	 fwrite($fp, $custom_css);
         	 fclose($fp);
         }

         //upload image
             $store_brand_image_tmp = '';
             $store_brand_image_name = '';
             $store_brand_image_tmp = new upload('store_brand_image' . $_SESSION['languages_id']);
             $store_brand_image_tmp->set_destination(DIR_FS_LOGO);
             #$store_brand_image_tmp->set_extensions('gif','jpg','png');
             if ($store_brand_image_tmp->parse() && $store_brand_image_tmp->save()) {
                 $store_brand_image_name = $store_brand_image_tmp->filename;
             } else if (is_file(DIR_FS_LOGO . $store_brand_image)){
                 $store_brand_image_name = $store_brand_image;
             } else {
                 $store_brand_image_name = $store_brand_image_existing;
             }
             if($deleted_image) $store_brand_image_name = '';

         //upload favicon
             $store_brand_favicon_tmp = '';
             $store_brand_favicon_name = '';
             $store_brand_favicon_tmp = new upload('store_brand_favicon' . $_SESSION['languages_id']);
             $store_brand_favicon_tmp->set_destination(DIR_FS_LOGO);
             #$store_brand_favicon_tmp->set_extensions('gif','jpg','png');
             if ($store_brand_favicon_tmp->parse() && $store_brand_favicon_tmp->save()) {
                 $store_brand_favicon_name = $store_brand_favicon_tmp->filename;
             } else if (is_file(DIR_FS_LOGO . $store_brand_favicon)){
                 $store_brand_favicon_name = $store_brand_favicon;
             } else {
                 $store_brand_favicon_name = $store_brand_favicon_existing;
             }
             if($deleted_favicon) $store_brand_favicon_name = '';

         //upload og image
             $store_og_image_tmp = '';
             $store_og_image_name = '';
             $store_og_image_tmp = new upload('store_og_image' . $_SESSION['languages_id']);
             $store_og_image_tmp->set_destination(DIR_FS_LOGO);
             #$store_og_image_tmp->set_extensions('gif','jpg','png');
             if ($store_og_image_tmp->parse() && $store_og_image_tmp->save()) {
                 $store_og_image_name = $store_og_image_tmp->filename;
             } else if (is_file(DIR_FS_LOGO . $store_og_image)){
                 $store_og_image_name = $store_og_image;
             } else {
                 $store_og_image_name = $store_og_image_existing;
             }
             if($deleted_store_og_image) $store_og_image_name = '';

             $sql_data_array = array( 'store_brand_image' =>   $store_brand_image_name,
             						  'store_brand_favicon' =>   $store_brand_favicon_name,
             						  'store_og_image' =>   $store_og_image_name,
                                      'store_brand_slogan' =>    tep_db_prepare_input($store_brand_slogan),
                                      'store_brand_telephone' =>  tep_db_prepare_input($store_brand_telephone),
                                      'store_brand_fax' => tep_db_prepare_input($store_brand_fax),
                                      'store_brand_homepage' => tep_db_prepare_input($store_brand_homepage),
                                      'store_brand_name' => tep_db_prepare_input($store_brand_name),
                                      'store_brand_support_email' => tep_db_prepare_input($store_brand_support_email),
                                      'store_brand_support_phone' => tep_db_prepare_input($store_brand_support_phone),
                                      'store_brand_address' => tep_db_prepare_input($store_brand_address),
                                      'custom_css' => tep_db_prepare_input($custom_css),
                                      'facebook_link' => tep_db_prepare_input($facebook_link),
                                      'twitter_link' => tep_db_prepare_input($twitter_link),
                                      'pinterest_link' => tep_db_prepare_input($pinterest_link),
                                      'google_link' => tep_db_prepare_input($google_link),
                                      'youtube_link' => tep_db_prepare_input($youtube_link),
                                      'linkedin_link' => tep_db_prepare_input($linkedin_link),
                                      'language_id' => tep_db_input($_SESSION['languages_id']),
                                      'site_settings' => $json_encode_data
                                      );
             cre_db_update(TABLE_BRANDING_DESCRIPTION, $sql_data_array);

  tep_redirect(tep_href_link(FILENAME_BRANDING_MANAGER));
}// action end

//check directory is exists and writable
$error = false;
if (is_dir(DIR_FS_LOGO)) {
  if (!is_writeable(DIR_FS_LOGO)) {
    $messageStack->add('search',ERROR_LOGO_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
     $error = true;
  }
} else {
  $messageStack->add('search',ERROR_LOGO_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
  $error = true;
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('branding_manager') > 0) {
      echo $messageStack->output('branding_manager'); 
    }
    ?>
      <div class="row">
        <div class="col-8"></div>
        <div class="col-4 pr-0">
          <div class="btn-group pull-right dark"> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class="btn btn-white dropdown-toggle"> <?php echo ucwords($_SESSION['language']); ?> <span class="caret"></span> </a>
            <ul class="dropdown-menu pull-right">
              <?php
              $languages = tep_get_languages();
              for ($i=0; $i<sizeof($languages); $i++) {
                ?>
                <li <?php echo (($_SESSION['languages_id'] == $_SESSION['languages_id'])? 'class="active"':'');?>>
                  <a aria-expanded="false" href="#"><?php echo tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name'],'','','align="absmiddle" style="height:16px; "') . '<span class="ml-2">' . $languages[$i]['name'];?></span></a>
                </li>
                <?php
              }
              ?>
            </ul>
          </div>
        </div>
      </div>

    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-brandingmanager" class="table-brandingmanager">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">
       			<?php echo tep_draw_form('frm_upload', FILENAME_BRANDING_MANAGER, 'action=upload', 'post', 'enctype="multipart/form-data" class="form-horizontal"'); ?>
            <?php
            $store_brand_info_qry = tep_db_query("SELECT * from " . TABLE_BRANDING_DESCRIPTION ." where language_id = '" . $_SESSION['languages_id'] . "'");
            $store_brand_info = tep_db_fetch_array($store_brand_info_qry);
            ?>
		        <div class="main-heading">
              <span><?php echo sprintf(TITLE_STORE_BRAND,$languages[0]['name']); ?></span>
		          <div class="main-heading-footer"></div>
		        </div>
		        <div class="mt-2"></div>
		        <table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_TELEPHONE_NUMBER; ?></label></td>
                <td><?php echo tep_draw_input_field('store_brand_telephone' . $_SESSION['languages_id'], $store_brand_info['store_brand_telephone'], 'maxlength="32" class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_FAX_NUMBER; ?></label></td>
                <td><?php echo tep_draw_input_field('store_brand_fax' . $_SESSION['languages_id'], $store_brand_info['store_brand_fax'], 'maxlength="32" class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_HOMEPAGE; ?></label></td>
                <td><?php echo tep_draw_input_field('store_brand_homepage' . $_SESSION['languages_id'], $store_brand_info['store_brand_homepage'], 'maxlength="64" class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_COMPANY_LOGO; ?></label></td>
                <td><label class="control-label sidebar-edit mb-0"><?php echo tep_draw_file_field('store_brand_image' . $_SESSION['languages_id'], '', '', 'class="filestyle"');  if (tep_not_null($store_brand_info['store_brand_image']) && file_exists( DIR_FS_LOGO . $store_brand_info['store_brand_image'] )) { echo '<br>' . DIR_FS_LOGO . $store_brand_info['store_brand_image']; }?>
                </td>
              </tr>
              <?php
              if (tep_not_null($store_brand_info['store_brand_image']) ) {
              ?>
              <tr>
                <td></td>
                <td valign="middle" class="sidebar-title"><?php
                if( file_exists( DIR_FS_LOGO . $store_brand_info['store_brand_image'] )) {
                    echo tep_image(DIR_WS_LOGO . $store_brand_info['store_brand_image']) ;
                } else {
                    echo '<span class="errorText">' . BRANDING_ERROR_IMAGE_MISSING .'</span>';
                }
                echo ' &nbsp; ' . tep_draw_hidden_field('store_brand_image_existing' . $_SESSION['languages_id'], $store_brand_info['store_brand_image']) . tep_draw_checkbox_field('delete_image' . $_SESSION['languages_id'],'yes') . '&nbsp;' .  DELETE_STORE_BRANDING_COMPANY_LOGO;?></td>
              </tr>
              <?php
              }
              ?>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_COMPANY_NAME; ?></label></td>
                <td><?php echo tep_draw_input_field('store_brand_name' . $_SESSION['languages_id'], $store_brand_info['store_brand_name'],'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_SLOGAN; ?></label></td>
                <td><?php echo tep_draw_input_field('store_brand_slogan' . $_SESSION['languages_id'], $store_brand_info['store_brand_slogan'],'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_SUPPORT_EMAIL; ?></label></td>
                <td><?php echo tep_draw_input_field('store_brand_support_email' . $_SESSION['languages_id'],$store_brand_info['store_brand_support_email'],'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_SUPPORT_PHONE; ?></label></td>
                <td><?php echo tep_draw_input_field('store_brand_support_phone' . $_SESSION['languages_id'],$store_brand_info['store_brand_support_phone'],'class="form-control"'); ?></td>
              </tr>

              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_COMPANY_FAVICON; ?></label></td>
                <td><?php echo tep_draw_file_field('store_brand_favicon' . $_SESSION['languages_id'], '', '', 'class="filestyle"');  if (tep_not_null($store_brand_info['store_brand_favicon']) && file_exists( DIR_FS_LOGO . $store_brand_info['store_brand_favicon'] )) { echo '<br>' . DIR_FS_LOGO . $store_brand_info['store_brand_favicon']; }?>
                </td>
              </tr>
              <?php
              if (tep_not_null($store_brand_info['store_brand_favicon']) ) {
              ?>
              <tr>
                <td></td>
                <td valign="middle" class="sidebar-title"><?php
                if( file_exists( DIR_FS_LOGO . $store_brand_info['store_brand_favicon'] )) {
                    echo tep_image(DIR_WS_LOGO . $store_brand_info['store_brand_favicon']) ;
                } else {
                    echo '<span class="errorText">' . BRANDING_ERROR_FAVICON_MISSING .'</span>';
                }
                echo ' &nbsp; ' . tep_draw_hidden_field('store_brand_favicon_existing' . $_SESSION['languages_id'], $store_brand_info['store_brand_favicon']) . tep_draw_checkbox_field('delete_favicon' . $_SESSION['languages_id'],'yes') . '&nbsp;' .  DELETE_STORE_BRANDING_COMPANY_FAVICON;?></td>
              </tr>
              <?php
              }
              ?>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_OG_IMGE; ?></label></td>
                <td><?php echo tep_draw_file_field('store_og_image' . $_SESSION['languages_id'], '', '', 'class="filestyle"');  if (tep_not_null($store_brand_info['store_og_image']) && file_exists( DIR_FS_LOGO . $store_brand_info['store_og_image'] )) { echo '<br>' . DIR_FS_LOGO . $store_brand_info['store_og_image']; }?>
                </td>
              </tr>
              <?php
              if (tep_not_null($store_brand_info['store_og_image']) ) {
              ?>
              <tr>
                <td></td>
                <td class="sidebar-title" valign="middle"><?php
                if( file_exists( DIR_FS_LOGO . $store_brand_info['store_og_image'] )) {
                    echo tep_image(DIR_WS_LOGO . $store_brand_info['store_og_image']) ;
                } else {
                    echo '<span class="errorText">' . BRANDING_ERROR_OG_IMAGE_MISSING .'</span>';
                }
                echo ' &nbsp; ' . tep_draw_hidden_field('store_og_image_existing' . $_SESSION['languages_id'], $store_brand_info['store_og_image']) . tep_draw_checkbox_field('delete_store_og_image' . $_SESSION['languages_id'],'yes') . '&nbsp;' .  DELETE_STORE_BRANDING_OG_IMAGE;?></td>
              </tr>
              <?php
              }
              ?>

              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_ADDRESS; ?></label></td>
                <td><?php echo tep_draw_textarea_field('store_brand_address' . $_SESSION['languages_id'],true, 40, 10, $store_brand_info['store_brand_address'], 'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_CUSTOM_CSS; ?></label></td>
                <td><?php echo tep_draw_textarea_field('custom_css' . $_SESSION['languages_id'],true, 40, 10, $store_brand_info['custom_css'], 'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_FACEBOOK_LINK; ?></label></td>
                <td><?php echo tep_draw_input_field('facebook_link' . $_SESSION['languages_id'],$store_brand_info['facebook_link'], 'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_TWITTER_LINK; ?></label></td>
                <td><?php echo tep_draw_input_field('twitter_link' . $_SESSION['languages_id'],$store_brand_info['twitter_link'], 'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_PINTEREST_LINK; ?></label></td>
                <td><?php echo tep_draw_input_field('pinterest_link' . $_SESSION['languages_id'],$store_brand_info['pinterest_link'], 'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_GOOGLEPLUS_LINK; ?></label></td>
                <td><?php echo tep_draw_input_field('google_link' . $_SESSION['languages_id'],$store_brand_info['google_link'], 'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_YOUTUBE_LINK; ?></label></td>
                <td><?php echo tep_draw_input_field('youtube_link' . $_SESSION['languages_id'],$store_brand_info['youtube_link'], 'class="form-control"'); ?></td>
              </tr>
              <tr>
                <td><label class="control-label sidebar-edit mb-0"><?php echo STORE_BRAND_BRANDING_LINKEDIN_LINK; ?></label></td>
                <td><?php echo tep_draw_input_field('linkedin_link',$store_brand_info['linkedin_link'], 'class="form-control"'); ?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Default Button Color:</label></td>
                  <td><?php echo tep_draw_input_field('default_button_color',$store_brand_info['default_button_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Add to Cart Button Color:</label></td>
                  <td><?php echo tep_draw_input_field('addtocart_button_color',$store_brand_info['addtocart_button_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Quick View Button Color:</label></td>
                  <td><?php echo tep_draw_input_field('quick_view_button_color',$store_brand_info['quick_view_button_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Compare Button Color:</label></td>
                  <td><?php echo tep_draw_input_field('display_compare_button',$store_brand_info['display_compare_button'] , 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">View Cart Button Color:</label></td>
                  <td><?php echo tep_draw_input_field('viewcart_button_color',$store_brand_info['viewcart_button_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Checkout Button Color:</label></td>
                  <td><?php echo tep_draw_input_field('checkout_button_color',$store_brand_info['checkout_button_color'] , 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
<?php /* ?>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Product Name Link Color:</label></td>
                  <td><?php echo tep_draw_input_field('prodcut_name_link_color',$store_brand_info['prodcut_name_link_color'] , 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Page Heading Color:</label></td>
                  <td><?php echo tep_draw_input_field('page_heading_color',$store_brand_info['page_heading_color'] , 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Product Name Size:</label></td>
                  <td><?php echo tep_draw_input_field('product_name_size',$store_brand_info['product_name_size'], 'class="form-control" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Page Heading Size:</label></td>
                  <td><?php echo tep_draw_input_field('page_heading_size',$store_brand_info['page_heading_size'], 'class="form-control" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Top Bar Background Color:</label></td>
                  <td><?php echo tep_draw_input_field('top_bar_background_color',$store_brand_info['top_bar_background_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Top Bar Link Color:</label></td>
                  <td><?php echo tep_draw_input_field('top_bar_link_color',$store_brand_info['top_bar_link_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Top Bar Text Color:</label></td>
                  <td><?php echo tep_draw_input_field('top_bar_text_color',$store_brand_info['top_bar_text_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Top Bar Link Hover Color:</label></td>
                  <td><?php echo tep_draw_input_field('top_bar_link_hover_color',$store_brand_info['top_bar_link_hover_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Top Menu Background Color:</label></td>
                  <td><?php echo tep_draw_input_field('top_main_menu_background_color',$store_brand_info['top_main_menu_background_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Top Menu Link Color:</label></td>
                  <td><?php echo tep_draw_input_field('top_mainmenu_link_color',$store_brand_info['top_mainmenu_link_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Top Menu Link Hover Color:</label></td>
                  <td><?php echo tep_draw_input_field('top_mainmenu_link_hover_color',$store_brand_info['top_mainmenu_link_hover_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Top Menu Text Color:</label></td>
                  <td><?php echo tep_draw_input_field('top_menu_text_color',$store_brand_info['top_menu_text_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Footer Background Color:</label></td>
                  <td><?php echo tep_draw_input_field('footer_background_color',$store_brand_info['footer_background_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Footer Text Color:</label></td>
                  <td><?php echo tep_draw_input_field('footer_text_color',$store_brand_info['footer_text_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Footer Link Color:</label></td>
                  <td><?php echo tep_draw_input_field('footer_link_color',$store_brand_info['footer_link_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Footer Link Hover Color:</label></td>
                  <td><?php echo tep_draw_input_field('footer_link_hover_color',$store_brand_info['footer_link_hover_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Infobox Background Color:</label></td>
                  <td><?php echo tep_draw_input_field('infobox_background_color',$store_brand_info['infobox_background_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Infobox Heading Background Color:</label></td>
                  <td><?php echo tep_draw_input_field('infobox_heading_background_color',$store_brand_info['infobox_heading_background_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Page Background Color:</label></td>
                  <td><?php echo tep_draw_input_field('page_background_color',$store_brand_info['page_background_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Infobox Shape:</label></td>
                  <td><?php echo tep_draw_input_field('infobox_shape',$store_brand_info['infobox_shape'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Contentarea Background Color:</label></td>
                  <td><?php echo tep_draw_input_field('contentarea_background_color',$store_brand_info['contentarea_background_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Contentarea Text Color:</label></td>
                  <td><?php echo tep_draw_input_field('contentarea_text_color',$store_brand_info['contentarea_text_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Contentarea Link Color:</label></td>
                  <td><?php echo tep_draw_input_field('contentarea_link_color',$store_brand_info['contentarea_link_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Contentarea Link Hover Color:</label></td>
                  <td><?php echo tep_draw_input_field('contentarea_link_hover_color',$store_brand_info['contentarea_link_hover_color'], 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off" style="width:50%;"');?></td>
              </tr>
              <tr>
                  <td><label class="control-label sidebar-edit mb-0">Infobox Shape Radius Size:</label></td>
                  <td><?php echo tep_draw_input_field('infobox_shape_radius_size',$store_brand_info['infobox_shape_radius_size'], 'class="form-control" autocomplete="off" style="width:50%;"');?></td>
              </tr>
<?php */ ?>
              
            </table>
        		<div class="text-center mt-3 mb-3" role="group">
        		  <?php 
        			if (!$error) {
        				echo '<button class="btn btn-success btn-sm mt-3 mb-4" type="submit">'. IMAGE_UPDATE_STORE_BRANDING .'</button>';
        			}
        		  ?>
        		</div>                         

            </form>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
