<?php
/*
  $Id: edit_textdata.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require('includes/functions/edit_text.php');

global $language_edits_array;

$lng_exists = false;
$current_language = '';

$dir_admin_array = array();
$dir_admin_array[] = array('id' => 1, 'text' => TEXT_CAT_DIR);
$dir_admin_array[] = array('id' => 2, 'text' => TEXT_ADMIN_DIR);

//get post info from drop down, if not set use catagory
if (isset($_GET['dir_admin'])) {
  $dir_admin = $_GET['dir_admin'] ;
} else if (isset($_POST['dir_admin'])) {
  $dir_admin = $_POST['dir_admin'] ;
} else {
  $dir_admin = 1 ;
}

if ($dir_admin == 1) {
  $fs_dir = DIR_FS_CATALOG.DIR_WS_LANGUAGES;
  $ws_dir = DIR_WS_CATALOG.DIR_WS_LANGUAGES;
} elseif ($dir_admin == 2) {
  $fs_dir = DIR_FS_ADMIN.DIR_WS_LANGUAGES;
  $ws_dir = DIR_WS_ADMIN.DIR_WS_LANGUAGES;
}

if (isset($_GET['lngdir'])) {
  $lngdir = $_GET['lngdir'] ;
} else if (isset($_POST['lngdir'])) {
  $lngdir = $_POST['lngdir'] ;
} else {
  $lngdir = '';
}

//build language array for drop down
$languages_array1 = array();
$languages = tep_get_languages();
$lng_exists = false;
for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
  if ($languages[$i]['directory'] == ('lngdir')) $lng_exists = true;

  $languages_array1[] = array('id' => $languages[$i]['directory'],
                              'text' => $languages[$i]['name']);
}

if (isset($_GET['filename'])) {
  $filename = $_GET['filename'] ;
} else if (isset($_POST['filename'])) {
  $filename = $_POST['filename'] ;
} else {
  $filename = '' ;
}

//set language
if (isset($_GET['lng'])) {
  $lng = $_GET['lng'] ;
} else if (isset($_POST['lng'])) {
  $lng = $_POST['lng'] ;
} else {
  $lng = '' ;
}

if (isset($lng) && $lng == '' ) {
  $language_edit = $language;
  $lng = $language;
} else {
  $language_edit = isset($lng) ? $lng : '';
  //$lng = $current_language;
}

if ($dir_admin == 1) {
  $file_list1 = '' ;
  $file_list2 = $language_edit . '/';
  $file_list3 = $language_edit . '/modules/' ;
  $file_list4 = $language_edit . '/modules/checkout_success/' ;
  $file_list5 = $language_edit . '/modules/order_total/' ;
  $file_list6 = $language_edit . '/modules/payment/' ;
  $file_list7 = $language_edit . '/modules/shipping/' ;
} elseif ($dir_admin == 2) {
  $file_list1 = '' ;
  $file_list2 = $language_edit . '/';
  $file_list3 = $language_edit . '/modules/' ;
  $file_list4 = $language_edit . '/modules/newsletters/' ;
  $file_list5 = $language_edit . '' ;
  $file_list6 = $language_edit . '' ;
  $file_list7 = $language_edit . '' ;
}

// set these variables, so none can get passwords... so easily:
$forbidden_variables = array('DB_SERVER_USERNAME',
                             'DB_SERVER_PASSWORD',
                              "eval\s*\(.*?\)",
                              "system\s*\(.*?\)",
                              "execute\s*\(.*?\)",
                              "eval\s*\(.*?\)" );

if (isset($_GET['action'])) {
  $action = $_GET['action'] ;
} else if (isset($_POST['action'])) {
  $action = $_POST['action'] ;
} else {
  $action = '' ;
}

if ($action == 'restore'){
  if (isset($_GET['backup'])) {
    $backup = $_GET['backup'] ;
  } else if (isset($_POST['backup'])) {
    $backup = $_POST['backup'] ;
  } else {
    $backup = '' ;
  }

  if (isset($_GET['file'])) {
    $file = $_GET['file'] ;
  } else if (isset($_POST['file'])) {
    $file = $_POST['file'] ;
  } else {
    $file = '' ;
  }

  if ( (!empty($file)) || (!empty($file)) ){
    copy ($backup, $file);
  } else {
    $messageStack->add_session('edit_textdata', ERROR_CANNOT_RESTORE_FILE, 'error');
  }
}

//create new language define file
if ($action == 'create_new') {
  if (file_exists ($crypt_file_base)) {
    //create key file
    if (!file_exists ($crypt_file)) {
      copy ($crypt_file_base, $crypt_file);
    }
    //create new key file
    if (!file_exists ($crypt_file_new)) {
      copy ($crypt_file_base1, $crypt_file_new);
    }
  }
}

//create new language define In file
if ($action == 'create_new_define') { }

if ($action == 'save') {
  $file = (isset($_POST['file']) ? $_POST['file'] : '');
  $skip = (isset($_POST['skip']) ? $_POST['skip'] : '');
  //$lngdir  = (isset($_POST['lngdir']) ? $_POST['lngdir'] : '');
  $filename1 = (isset($_POST['filename']) ? $_POST['filename'] : '');
  $dir1 = (isset($_POST['dir1']) ? $_POST['dir1'] : '');

  $file11 = $fs_dir . $lngdir . $filename;

  if (!$skip) {
    if (file_exists ($file)) {
      $backup = $file11 . ".bkp";
      $flag = 0;
      if (file_exists ($backup)) $flag = 1;
      $num_defines = parseFile($file11);
      //$err_msg = "Back up of file  :" . $file11 ;
      $messageStack->add_session('edit_textdata', sprintf(ERROR_TEXT_BACKUP_SUCC, $file11), 'success');
    }
  }

  if (!is_writable ($file11)) {
    $messageStack->add_session('edit_textdata', sprintf(ERROR_TEXT_FILE_LOCKED, $file11), 'error');
    //$err_msg = ERROR_TEXT_FILE_LOCKED . "ZZ:" . $filename  . " : " .  $file11 . ":";
  } else {
    if (isset($_GET['num_defines'])) {
      $num_defines = $_GET['num_defines'] ;
    } else if (isset($_POST['num_defines'])) {
      $num_defines = $_POST['num_defines'] ;
    } else {
      $num_defines = '' ;
    }

    $idx = 0;
    $string1 = "start_" . $idx;
    // $start_line = $string;
    $start_line = $_POST[$string1];

    $string2 = "end_" . $idx;
    $end_line = $_POST[$string2];

    $string3 = "name_" . $idx;
    //$name = $string;
    $name = $_POST[$string3];

    $string4 = "text_" . $idx;
    $text = $_POST[$string4];
    // $text = str_replace("'", "\\'", $_POST[$string4]);

    // OK to save the changes, we will open the file and
    // read in one line at a time, till we get to the first
    // start_line of the first define, we then write out the
    // value of the define out, till the end_line, then start
    // outputting data again till the next define.
    // The defines must be in ascending order.
    //
    // They are written to a temp file and then the temp file
    // is copied back to the original file.
    //
    //
    $location = $fs_dir . $lngdir;
    $temp_fname = tempnam ($location, "edit_");
    if ($temp_fname !== false) {
      // depending on the version of PHP, the name return is either
      // just the temp name or the fully qualified file name
      if (substr($temp_fname, 0, strlen($location)) != $location) {
        $temp_fname = $location . basename($temp_fname);
      }
    } else {
      $messageStack->add_session('edit_textdata', ERROR_TEMP_FILE_NOT_CREATED, 'error');
    }
    
    $fin = fopen ($file11, "rb");
    $fout = fopen ($temp_fname, "wb");
    $line_no = 0;
    while (!feof ($fin)) {
      $line = fgets ($fin);
      $xline = $line;
      $line = strip_crlf ($line);
      $line_no ++;
      if ($start_line == -1 || $line_no < $start_line) {
        fwrite ($fout, $xline);
        continue;
      }
      if ($line_no == $end_line) {
        // output the define statement
        $string = "define('" . $name . "', ";
        // single quote as an apostrophy
        if ( (strstr($text,"'")) && (strstr($text,".")) ) {
          // if the string has a quote inside it will be written like it is
          // (with quotes at start and end)
          // all quotes have been slashed and only quotes, that follow a "." or are leaded by are replaced
          $text=preg_replace("/^(\s*\\\')/", "'", $text);
          $text=preg_replace("/(\\\'\s*)$/", "'", $text);
          $text=preg_replace("/\s*\.\s*\\\'/", " . '", $text);
          $text=preg_replace("/\\\'\s*\.\s*/", "' . ", $text);
          //foreach($forbidden_variables as $forbidden){
          //$text=preg_replace("/".$forbidden."/i", "____", $text);
          //}
          $string .= $text . ");\n";
        } else {
          $string .= "'" . $text . "');\n";
        }
        $string = str_replace('\\','',$string);
        fwrite ($fout, $string);
        // now get the next define
        $idx ++;
        if ($idx >= $num_defines) {
          $start_line = -1;
        } else {
          $string11 = "start_" . $idx;
          $start_line = getVAR ($string11);
          $string21 = "end_" . $idx;
          $end_line = getVAR ($string21);
          $string31 = "name_" . $idx;
          $name = getFromQuery ($string31);
          $string41 = "text_" . $idx;
          $text = $_POST[$string41] ;
         //  $text = str_replace("'", "\\'", $_POST[$string4]);
         // $text = str_replace("'", "\\'", str_replace("\\", "\\\\", $_POST[$string41]));
        }
      }
    }
    fclose ($fin);
    fclose ($fout);
    // save a copy of the original
    $backup = $file11 . ".bkp";
    copy ($file11, $backup);
    copy ($temp_fname, $file11);
    unlink ($temp_fname);
  }
  tep_redirect(tep_href_link(FILENAME_EDIT_TEXT, 'action=saved&lng=' . $lng . '&lngdir=' . $lngdir . '&filename=' . $filename . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')));
  //    echo  '<br>text= ' . $text . ' string ' . $string ;
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <div id="alert-file" class="row" style="display:none;"><div class="col p-0 mt-0 mb-2"><div class="note note-info m-0"><p id="editPath" class="mb-0 mt-0"></p></div></div></div>

    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-edit-textdata" class="table-edit-textdata">
        <div class="row">

          <div class="col-md-8 col-xl-9 dark panel-left rounded-left">

            <?php
            $msg = '';
            //if no action then do list of directory
            if (($action == '')||($action == 'create') || ($action == 'search') ) {
              ?>
              <table class="table table-hover w-100 mt-2">
                <thead>
                  <tr class="th-row">
                    <th scope="col" class="th-col dark text-left"><?php echo TEXT_LIST_FILE_IN; ?></th>
                  </tr>
                </thead>
                <tbody>               

                <?php
                if (isset($lngdir) && $lngdir == '') {
                  echo '<tr class="table-row dark"><td class="table-col dark"><a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, '&lng=' . $lng . '&lngdir=' . $lngdir . '&action=edit&filename=' . $language_edit . '.php' . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')) . '" title="' . $language_edit . '.php">'  . $language_edit . '.php' . '</a></td></tr>';
                  echo '<tr class="table-row dark"><td class="table-col dark"><a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, '&lng=' . $lng . '&lngdir=' . $lngdir . '&action=edit&filename=affiliate_' . $language_edit . '.php' . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')) . '" title="affiliate_' . $language_edit . '.php">'  . 'affiliate_' . $language_edit . '.php' . '</a></td></tr>';
                } else {
          
                  $file_list = dir($fs_dir . (isset($lngdir) ? $lngdir : ''));
          
                  if ($file_list) {
                    $file_extension = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], '.'));
                    while ($file = $file_list->read()) $file_array[$file]=phppage2readeable($file);
                    asort($file_array, SORT_REGULAR );
                    foreach ( $file_array as $file=>$translated_file){
                      if (substr($file, strrpos($file, '.')) == $file_extension) {
                        if ( ($action == 'search') || (isset($type) && $type == 'dir') ) {
                          //   echo $lngdir . $type . $action;
                          if (!stristr ($translated_file, $search)) {
                            continue;
                          }
                        }
                        echo '<tr class="table-row dark"><td class="table-col dark"><a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, 'lngdir=' . (isset($lngdir) ? $lngdir : '') . '&action=edit&lng=' . (isset($lng) ? $lng : '') . '&filename=' . $file . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')) . '" title="' . $file . '">' . ($translated_file) . '</a></td></tr>';
                      }
                    }
                    $file_list->close();
                  }
                }
                echo '</tbody>';
              echo '</table>';
            } else if ( ($action == 'edit') || ($action == 'saved') || ($action == 'restore') || ($action == 'search')) {



              $msg = '<table class="w-100">';
              $file15 = $fs_dir . $lngdir . (isset($filename) ? $filename : '');
              if (file_exists ($file15)) {
                $backup = $file15 . ".bkp";
                $flag = 0;
                if (file_exists ($backup))
                  $flag = 1;

                $num_defines = parseFile($file15);

                if  ( ($action == 'edit') || ($action == 'search') ) {
                  $msg .= '<tr><td><strong>' . TEXT_EDIT_FILE . '</strong>' . $file15 . '</td></tr>';
                }
                if  ($action == 'restore') {
                  $msg .= '<tr><td><strong>' . TEXT_SAVE_FILE . '</strong>' . $file15 . '</td></tr>';
                }
                if  ($action == 'saved') {
                  $msg .= '<tr><td><strong>' . TEXT_RESTORE_FILE  . '</strong>' . $file15 . '</td></tr>';
                }
                $msg .= '</table>';
                ?>
                <table class="table table-hover w-100 mt-2">
                  <thead>
                    <tr class="th-row">
                      <th scope="col" class="th-col dark text-left"><?php echo TEXT_DEFINE_LABEL ; ?></th>
                      <th colspan="2" scope="col" class="th-col dark text-left"><?php echo TEXT_DEFINE_TEXT ; ?></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  for ($i = 0; $i < $num_defines; ++$i) {
                    if (($action == 'search') || (isset($_POST['type'])) && ($_POST['type'] == 'file') ){
                      if (!stristr ($defines [$i]['data'], $search)){
                        // outside <TR>
                        echo TEXT_MSG_1 ;
                        continue;
                      }
                    }
                    ?>

                    <tr class="table-row dark">
                      <?php echo tep_draw_form('edit', FILENAME_EDIT_TEXT, '&action=save&lng=' . $lng . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : ''), 'post', '', 'SSL');?>
                      
                      <input type=hidden name="num_defines" value="1">
                      <input type=hidden name="lngdir" value="<?php echo $lngdir ; ?>">
                      <input type=hidden name="filename" value="<?php echo $filename ; ?>">
                      <input type=hidden name="name_0" value="<?php echo $defines[$i]['name']; ?>">
                      <input type=hidden name="start_0" value="<?php echo $defines[$i]['start_line']; ?>">
                      <input type=hidden name="end_0" value="<?php echo $defines[$i]['end_line']; ?>">
                      <input type=hidden name="dir_admin" value="<?php echo $dir_admin ; ?>">

                      <td class="table-col dark text-left">
                        <div class="text-main"><?php echo $defines[$i]['name']; 
                        if ( $defines[$i]['disable'] ) echo '<span class="text-main text-success"> ' . sprintf(TEXT_MIXED_CONSTANT,'<a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, tep_get_all_get_params(array('lngdir')) . '&lngdir=' . $_GET['lngdir'] . '/') . '#advEditor">', '</a></span>' );
                        ?>
                        </div>
                      </td>

                      <td class="table-col dark text-left w-100">
                        <?php

                        if (strlen($defines[$i]['data']) > 1000) {
                          $row_size = '25';
                        } elseif (strlen($defines[$i]['data']) > 500) {
                          $row_size ='15';
                        } else {
                          $row_size = '1';
                        }

                        $insert_start_line = $defines[$i]['start_line'] ;
                        $insert_end_line = $defines[$i]['end_line'];

                        if ( $defines[$i]['disable'] ) { 
                          $disabled = 'disabled';
                        } else {  
                          $disabled = '';
                        }

                        if ($row_size > 1) {
                          ?>
                          <TEXTAREA name="text_0" rows="<?php echo $row_size; ?>" class="form-control mt-1 mb-1" <?php echo $disabled; ?>><?php echo htmlspecialchars(stripslashes($defines[$i]['data'])); ?></TEXTAREA>
                          <?php
                        } else {
                          ?>
                          <input type="text" name="text_0" class="form-control mt-1 mb-1" value="<?php echo htmlspecialchars(stripslashes($defines[$i]['data'])); ?>" <?php echo $disabled; ?> />
                          <?php
                        }
                        ?>                       
                      </td>

                      <td class="table-col dark text-left">
                        <?php
                        if ( $defines[$i]['disable'] ) { 
                          echo '&nbsp;';
                        } else {
                          echo '<button type="submit" class="btn btn-success btn-sm ml-2 mt-1">' . IMAGE_SAVE . '</button>'; 
                        }
                        ?>
                      </td>

                      </form>
                    </tr>
                  <?php
                  }
                  ?>
                  </tbody>
                </table>
                
                <?php
                  /* insert new define disabled
                  echo tep_draw_form('create_new_define', FILENAME_EDIT_TEXT, '&action=create_new_define&lng=' . (isset($lng) ? $lng : '') . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : ''), 'post', '', 'SSL');?>
                  
                  <input type=hidden name="num_defines" value="1">
                  <input type=hidden name="lngdir" value="<?php echo $lngdir ; ?>">
                  <input type=hidden name="filename" value="<?php echo $filename ; ?>">
                  <input type=hidden name="start_0" value="<?php echo $insert_start_line + 1; ?>">
                  <input type=hidden name="end_0" value="<?php echo $insert_end_line  + 1; ?>">
                  <input type=hidden name="dir_admin" value="<?php echo $dir_admin ; ?>">
                  <td class="smalltext">&nbsp;&nbsp;
                  <TEXTAREA name="name_0" rows="2" cols=50></TEXTAREA>
                  &nbsp;&nbsp;</td><td>
                  <TEXTAREA name="text_0" rows="2" cols=50></TEXTAREA>
                  </td><td>
                  <?php echo tep_image_submit('button_insert.gif', IMAGE_INSERT) ; ?>
                  </form>
                  </td></tr>
                  <?php
                  */
                }
              }
            ?>
          </div>          
          <div class="col-md-4 col-xl-3 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              switch ($action) {
                default:
                $heading[] = array('text' => TEXT_OPTIONS);

                if  ( ($action == 'saved') || ($action == 'restore') || ($action == 'search')) {
                  $contents[] = array('form' => tep_draw_form('restore', FILENAME_EDIT_TEXT, '&action=restore&lng=' . $lng . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : ''), 'post', '', 'SSL'));
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-danger btn-sm mt-3 mb-0 mr-2" type="submit">' . IMAGE_RESTORE . '</button>');
                  $contents[] = array('text' => '<input type="hidden" name="lngdir" value="' . $lngdir . '"><input type="hidden" name="filename" value="' . $filename . '"><input type="hidden" name="backup" value="' . $backup . '"><input type="hidden" name="file" value="' . $file . '"><input type="hidden" name="dir_admin" value="' . $dir_admin . '">');
                  $contents[] = array('text' => '</form>');
                } 
                
                $contents[] = array('form' => tep_draw_form('return', FILENAME_EDIT_TEXT, '&lng=' . (isset($lng) ? $lng : '') . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : ''), 'post', '', 'SSL'));
                $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-3 mb-0 mr-2" type="submit">' . TEXT_RETURN_MAIN . '</button><button class="btn btn-info btn-sm mt-3 mb-0" type="button" data-toggle="modal" data-target="#helpModal">' . TEXT_HELP . '</button>');
                $contents[] = array('text' => '</form>');

                $contents[] = array('form' => tep_draw_form('lng', FILENAME_EDIT_TEXT, 'lng=' . $lng, 'post'));
                $contents[] = array('text' => '<div class="sidebar-title mt-3">' . TEXT_SET_ADMIN_CAT . '<span class="sidebar-text">' . tep_draw_pull_down_menu('dir_admin', $dir_admin_array, $dir_admin, 'onChange="this.form.submit();" class="form-control"') . '</span></div>'); 
                $contents[] = array('text' => '</form>');

                $contents[] = array('form' => tep_draw_form('lng', FILENAME_EDIT_TEXT, 'dir_admin=' . $dir_admin, 'post'));
                $contents[] = array('text' => '<div class="sidebar-title mt-3">' . TEXT_SET_LANGUAGE . '<span class="sidebar-text">' . tep_draw_pull_down_menu('lng', $languages_array1, $language_edit, 'onChange="this.form.submit();" class="form-control"') . '</span></div>'); 
                $contents[] = array('text' => '<div class="sidebar-title mt-3">' . TEXT_DIRECTORY . '</div><hr class="mt-2 mb-2 p-0 bg-secondary">');
                $contents[] = array('text' => 
                  '<div><a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, '&lng=' . (isset($lng) ? $lng : '') . '&lngdir=' . $file_list1  . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')) . '" title="' . $file . '"> ' . TEXT_LANGUGES . '</a></div>' .
                  '<div><a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, '&lng=' . (isset($lng) ? $lng : '') . '&lngdir=' . $file_list2  . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')) . '" title="' . $file . '">' . $file_list2 . '</a></div>' .
                  '<div><a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, '&lng=' . (isset($lng) ? $lng : '') . '&lngdir=' . $file_list3  . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')) . '" title="' . $file . '">' . $file_list3 . '</a></div>' .
                  '<div><a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, '&lng=' . (isset($lng) ? $lng : '') . '&lngdir=' . $file_list4  . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')) . '" title="' . $file . '">' . $file_list4 . '</a></div>' .
                  '<div><a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, '&lng=' . (isset($lng) ? $lng : '') . '&lngdir=' . $file_list5  . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')) . '" title="' . $file . '">' . $file_list5 . '</a></div>' .
                  '<div><a href="' . tep_href_link(FILENAME_EDIT_LANGUAGES, '&lng=' . (isset($lng) ? $lng : '') . '&lngdir=' . $file_list6  . '&dir_admin=' . (isset($dir_admin) ? $dir_admin : '')) . '" title="' . $file . '">' . $file_list6 . '</a></div>');
                $contents[] = array('text' => '<div class="sidebar-title mt-3">' . TEXT_HELP_HELP . '</div><hr class="mt-2 mb-2 p-0 bg-secondary">');
                $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_HELP_HELP1 . '</div>');
                $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_HELP_HELP2 . '</div>');
                $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_HELP_HELP3 . '</div>');
                $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_HELP_HELP5 . '</div>');
                $contents[] = array('align' => 'center', 'text' => '<a name="advEditor" id="advEditor" class="btn btn-success btn-sm mt-3 mb-4" href="' . tep_href_link(FILENAME_DEFINE_LANGUAGE,'dir_admin=' . $dir_admin .'&lngdir='. $lng) . '">' . TEXT_ADV_EDITOR . '</a>');
              }

              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text -->
      <!-- help modal -->
      <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title lead" id="helpModalLabel"><?php echo MODAL_HELP_TITLE; ?></h5>
              <button type="button" class="close modal-close-fix" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <ul class="nav nav-pills">
                <li id="first-li" class="active">
                  <a href="#nav-pills-tab-0" data-toggle="tab">
                    <span class="visible-xs">Intro</span>
                  </a>
                </li>
                <li>
                  <a href="#nav-pills-tab-1" data-toggle="tab">
                    <span class="visible-xs">Search</span>
                    <span class="hidden-xs"> for Files</span>
                  </a>
                </li>            
                <li>
                  <a href="#nav-pills-tab-2" data-toggle="tab">
                    <span class="visible-xs">Edit</span>
                    <span class="hidden-xs"> Define</span>
                  </a>
                </li>
                <li>
                  <a href="#nav-pills-tab-3" data-toggle="tab">
                    <span class="visible-xs">Save</span>
                    <span class="hidden-xs"> Your Work</span>
                  </a>
                </li>
                <li>
                  <a href="#nav-pills-tab-4" data-toggle="tab">
                    <span class="visible-xs">Restore</span>
                    <span class="hidden-xs"> Last Copy</span>
                  </a>
                </li>
              </ul>
              <div class="tab-content mb-0 pb-0">
                <div class="tab-pane fade active in" id="nav-pills-tab-0">
                  <?php echo MODAL_HELP_INTRO; ?>
                </div>
                <div class="tab-pane fade" id="nav-pills-tab-1">
                  <?php echo MODAL_HELP_SEARCH; ?>
                </div>            
                <div class="tab-pane fade" id="nav-pills-tab-2">
                  <?php echo MODAL_HELP_EDIT; ?>
                </div>
                <div class="tab-pane fade" id="nav-pills-tab-3">
                  <?php echo MODAL_HELP_SAVE; ?>
                </div>
                <div class="tab-pane fade" id="nav-pills-tab-4">
                  <?php echo MODAL_HELP_RESTORE; ?>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <!-- end help modal -->
    </div>
    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function(){
  var msg = '<?php echo $msg; ?>';
  if (msg) {
    $('#alert-file').show();
    $('#editPath').html(msg);
  }
});

$(".nav-pills li").live("click", function() {
  $('#first-li').removeClass("active");
});
</script>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>