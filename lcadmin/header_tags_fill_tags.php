<?php
/*
  $Id: header_tags_fill_tags.php,v 1.0 2005/08/25
  Originally Created by: Jack York - http://www.oscommerce-solution.com
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
 
  require('includes/application_top.php'); 
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_HEADER_TAGS_CONTROLLER);
 
  /****************** READ IN FORM DATA ******************/
  $categories_fill = isset($_POST['group1']) ? $_POST['group1'] : '';
  $manufacturers_fill = isset($_POST['group2']) ? $_POST['group2'] : '';
  $products_fill = isset($_POST['group3']) ? $_POST['group3'] : '';
  $productsMetaDesc = isset($_POST['group4']) ? $_POST['group4'] : '';
  $productsMetaDescLength = isset($_POST['fillMetaDescrlength']) ? $_POST['fillMetaDescrlength'] : '';
 
  $checkedCats = array();
  $checkedManuf = array();
  $checkedProds = array();
  $checkedMetaDesc = array();
  
  $languages = tep_get_languages();
  $languages_array = array();
  for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
    $languages_array[] = array('id' => $languages[$i]['id'], // $i + 1, 
                               'text' => $languages[$i]['name']);
  }
  $langID = $languages_id; 
  $updateDB = false;
  $updateTextCat = '';
  $updateTextManuf = '';
  $updateTextProd = '';
    
  /****************** FILL THE CATEGORIES ******************/
   
  if ($categories_fill != '')
  {
    $langID = $_POST['fill_language'];
    
    if ($categories_fill == 'none') 
    {
       $checkedCats['none'] = 'Checked';
    }
    else
    { 
      $categories_tags_query = tep_db_query("select categories_name, categories_id, categories_htc_title_tag, categories_htc_desc_tag, categories_htc_keywords_tag, language_id from  " . TABLE_CATEGORIES_DESCRIPTION . " where language_id = '" . $langID . "'");
      while ($categories_tags = tep_db_fetch_array($categories_tags_query))
      {
        $updateDB = false;
        
        if ($categories_fill == 'empty')
        {
           if (! tep_not_null($categories_tags['categories_htc_title_tag']))
           {
             $updateDB = true;
             $updateTextCat = 'Empty Category tags have been filled.';
           }  
           $checkedCats['empty'] = 'Checked';
        }
        else if ($categories_fill == 'full')
        {
           $updateDB = true;
           $updateTextCat = 'All Category tags have been filled.';
           $checkedCats['full'] = 'Checked';
        }
        else      //assume clear all
        {
           tep_db_query("update " . TABLE_CATEGORIES_DESCRIPTION . " set categories_htc_title_tag='', categories_htc_desc_tag = '', categories_htc_keywords_tag = '' where categories_id = '" . $categories_tags['categories_id']."' and language_id  = '" . $langID . "'");
           $updateTextCat = 'All Category tags have been cleared.';
           $checkedCats['clear'] = 'Checked';
        }      
             
        if ($updateDB)
          tep_db_query("update " . TABLE_CATEGORIES_DESCRIPTION . " set categories_htc_title_tag='".addslashes($categories_tags['categories_name'])."', categories_htc_desc_tag = '". addslashes($categories_tags['categories_name'])."', categories_htc_keywords_tag = '". addslashes($categories_tags['categories_name']) . "' where categories_id = '" . $categories_tags['categories_id']."' and language_id  = '" . $langID . "'");
      }
    }
  }
  else
    $checkedCats['none'] = 'Checked';
   
  /****************** FILL THE MANUFACTURERS ******************/
   
  if ($manufacturers_fill != '')
  {
    $langID = $_POST['fill_language'];
    
    if ($manufacturers_fill == 'none') 
    {
       $checkedManuf['none'] = 'Checked';
    }
    else
    { 
      $manufacturers_tags_query = tep_db_query("select m.manufacturers_name, m.manufacturers_id, mi.languages_id, mi.manufacturers_htc_title_tag, mi.manufacturers_htc_desc_tag, mi.manufacturers_htc_keywords_tag from " . TABLE_MANUFACTURERS . " m, " . TABLE_MANUFACTURERS_INFO . " mi where mi.languages_id = '" . $langID . "'");
      while ($manufacturers_tags = tep_db_fetch_array($manufacturers_tags_query))
      {
        $updateDB = false;
        
        if ($manufacturers_fill == 'empty')
        {
           if (! tep_not_null($manufacturers_tags['manufacturers_htc_title_tag']))
           {
             $updateDB = true;
             $updateTextManuf = 'Empty Manufacturers tags have been filled.';
           }  
           $checkedManuf['empty'] = 'Checked';
        }
        else if ($manufacturers_fill == 'full')
        {
           $updateDB = true;
           $updateTextManuf = 'All Manufacturers tags have been filled.';
           $checkedManuf['full'] = 'Checked';
        }
        else      //assume clear all
        {
           tep_db_query("update " . TABLE_MANUFACTURERS_INFO . " set manufacturers_htc_title_tag='', manufacturers_htc_desc_tag = '', manufacturers_htc_keywords_tag = '' where manufacturers_id = '" . $manufacturers_tags['manufacturers_id']."' and languages_id  = '" . $langID . "'");
           $updateTextManuf = 'All Manufacturers tags have been cleared.';
           $checkedManuf['clear'] = 'Checked';
        }      
             
        if ($updateDB)
          tep_db_query("update " . TABLE_MANUFACTURERS_INFO . " set manufacturers_htc_title_tag='".addslashes($manufacturers_tags['manufacturers_name'])."', manufacturers_htc_desc_tag = '". addslashes($manufacturers_tags['manufacturers_name'])."', manufacturers_htc_keywords_tag = '". addslashes($manufacturers_tags['manufacturers_name']) . "' where manufacturers_id = '" . $manufacturers_tags['manufacturers_id']."' and languages_id  = '" . $langID . "'");
      }
    }
  }
  else
    $checkedManuf['none'] = 'Checked';
       
  /****************** FILL THE PRODUCTS ******************/  
  
  if ($products_fill != '')
  {
    $langID = $_POST['fill_language'];
    
    if ($products_fill == 'none') 
    {
       $checkedProds['none'] = 'Checked';
    }
    else
    { 
      $products_tags_query = tep_db_query("select products_name, products_description, products_id, products_head_title_tag, products_head_desc_tag, products_head_keywords_tag, language_id from " . TABLE_PRODUCTS_DESCRIPTION . " where language_id = '" . $langID . "'");
      while ($products_tags = tep_db_fetch_array($products_tags_query))
      {
        $updateDB = false;
        
        if ($products_fill == 'empty')
        {
          if (! tep_not_null($products_tags['products_head_title_tag']))
          {
            $updateDB = true;
            $updateTextProd = 'Empty Product tags have been filled.';
          }  
          $checkedProds['empty'] = 'Checked';
        }
        else if ($products_fill == 'full')
        {
          $updateDB = true;
          $updateTextProd = 'All Product tags have been filled.';
          $checkedProds['full'] = 'Checked';
        }
        else      //assume clear all
        {
          tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_head_title_tag='', products_head_desc_tag = '', products_head_keywords_tag =  '' where products_id = '" . $products_tags['products_id'] . "' and language_id='". $langID ."'");
          $updateTextProd = 'All Product tags have been cleared.';
          $checkedProds['clear'] = 'Checked';
        }
               
        if ($updateDB)
        {
          if ($productsMetaDesc == 'fillMetaDesc_yes')          //fill the description with all or part of the 
          {                                                     //product description
            if (! empty($products_tags['products_description']))
            {
              if (isset($productsMetaDescLength) && (int)$productsMetaDescLength > 3 && (int)$productsMetaDescLength < strlen($products_tags['products_description']))
                $desc = substr($products_tags['products_description'], 0, (int)$productsMetaDescLength);
              else                                              //length not entered or too small    
                $desc = $products_tags['products_description']; //so use the whole description
            }   
            else
              $desc = $products_tags['products_name'];  

            $checkedMetaDesc['no'] = '';
            $checkedMetaDesc['yes'] = 'Checked';
          }  
          else
          {        
            $desc = $products_tags['products_name'];           
            $checkedMetaDesc['no'] = 'Checked';
            $checkedMetaDesc['yes'] = '';
          }  

          tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_head_title_tag='".addslashes($products_tags['products_name'])."', products_head_desc_tag = '". addslashes(strip_tags($desc))."', products_head_keywords_tag =  '" . addslashes($products_tags['products_name']) . "' where products_id = '" . $products_tags['products_id'] . "' and language_id='". $langID ."'");
        } 
      }  
    }
  }
  else
  { 
    $checkedProds['none'] = 'Checked';
    $checkedMetaDesc['no'] = 'Checked';
    $checkedMetaDesc['yes'] = '';
  }
 
include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE_FILL_TAGS; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('header_tags_fill_tags') > 0) {
      echo $messageStack->output('header_tags_fill_tags'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-headertagsfilltags" class="table-headertagsfilltags">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">
            <div class="sidebar-title mt-2">
              <div class="row">
                <div class="col p-0 mt-3 ml-2 mr-2">
                  <div class="note note-warning m-0">
                    <p class="mb-0 mt-0 f-w-400"><?php echo TEXT_FILL_TAGS; ?></p>
                  </div>
                </div>
              </div>
            </div>

          	<table border="0" width="100%" cellspacing="0" cellpadding="2" class="data-table">    
              <tr>
                <td align="right"><?php echo tep_draw_form('header_tags', FILENAME_HEADER_TAGS_FILL_TAGS, '', 'post') . tep_draw_hidden_field('action', 'process'); ?></td>
              </tr>
              <tr>
                <td>
                  <table width="100%" border="0">
                    <tr><td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '5'); ?></td></tr>
                    <tr>
                      <td class="main" width="12%"><label class="control-label sidebar-edit mb-0"><?php echo TEXT_LANGUAGE; ?>&nbsp;</label></td>
                      <td><?php echo tep_draw_pull_down_menu('fill_language', $languages_array, $langID, 'class="form-control w-25"');?></td>
                    </tr>
                  </table> 

                  <table cellpadding="5" border="0">
                    <tr><td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '5'); ?></td></tr>
                    <tr class="main"> 
                      <td><label class="control-label sidebar-edit mb-0"><?php echo TEXT_FILL_WITH_DESCIPTION; ?></label></td>
                      <td class="sidebar-title mt-2 text-left"><INPUT TYPE="radio" NAME="group4" VALUE="fillMetaDesc_yes"<?php echo $checkedMetaDesc['yes']; ?>> <?php echo TEXT_YES; ?>&nbsp;</td>
                      <td class="sidebar-title mt-2 text-left"><INPUT TYPE="radio" NAME="group4" VALUE="fillMetaDesc_no"<?php echo $checkedMetaDesc['no']; ?>>&nbsp;<?php echo TEXT_NO; ?></td>
                      <td class="sidebar-title mt-2 text-left"><?php echo '<label class="control-label sidebar-edit mb-0">'.TEXT_LIMIT_TO . '</label></td><td class="sidebar-title mt-2 text-left">' . tep_draw_input_field('fillMetaDescrlength', '', 'class="form-control" w-25', false) . '</td><td class="sidebar-title mt-2 text-left">' . TEXT_CHARACTERS; ?> </td>
                    </tr>
                  </table>
                </td> 
              </tr>     
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '5'); ?></td>
              </tr>
                 
              <tr>
                <td><table border="0" width="50%">
                  <tr class="smallText">
                    <th scope="col" class="th-col dark text-left"><?php echo HEADING_TITLE_CONTROLLER_CATEGORIES; ?></th>
                    <th scope="col" class="th-col dark text-left"><?php echo HEADING_TITLE_CONTROLLER_MANUFACTURERS; ?></th>          
                    <th scope="col" class="th-col dark text-left"><?php echo HEADING_TITLE_CONTROLLER_PRODUCTS; ?></th>
                  </tr> 
                  <tr>          
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group1" VALUE="none" <?php echo $checkedCats['none']; ?>> <?php echo HEADING_TITLE_CONTROLLER_SKIPALL; ?></td>
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group2" VALUE="none" <?php echo $checkedManuf['none']; ?>> <?php echo HEADING_TITLE_CONTROLLER_SKIPALL; ?></td>
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group3" VALUE="none" <?php echo $checkedProds['none']; ?>> <?php echo HEADING_TITLE_CONTROLLER_SKIPALL; ?></td>
                  </tr>
                  <tr> 
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group1" VALUE="empty"<?php echo $checkedCats['empty']; ?> > <?php echo HEADING_TITLE_CONTROLLER_FILLONLY; ?></td>
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group2" VALUE="empty" <?php echo $checkedManuf['empty']; ?>> <?php echo HEADING_TITLE_CONTROLLER_FILLONLY; ?></td>
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group3" VALUE="empty" <?php echo $checkedProds['empty']; ?>> <?php echo HEADING_TITLE_CONTROLLER_FILLONLY; ?></td>
                  </tr>
                  <tr> 
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group1" VALUE="full" <?php echo $checkedCats['full']; ?>> <?php echo HEADING_TITLE_CONTROLLER_FILLALL; ?></td>
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group2" VALUE="full" <?php echo $checkedManuf['full']; ?>> <?php echo HEADING_TITLE_CONTROLLER_FILLALL; ?></td>
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group3" VALUE="full" <?php echo $checkedProds['full']; ?>> <?php echo HEADING_TITLE_CONTROLLER_FILLALL; ?></td>
                  </tr>
                  <tr> 
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group1" VALUE="clear" <?php echo $checkedCats['clear']; ?>> <?php echo HEADING_TITLE_CONTROLLER_CLEARALL; ?></td>
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group2" VALUE="clear" <?php echo $checkedManuf['clear']; ?>> <?php echo HEADING_TITLE_CONTROLLER_CLEARALL; ?></td>
                    <td class="table-col dark text-left"><INPUT TYPE="radio" NAME="group3" VALUE="clear" <?php echo $checkedProds['clear']; ?>> <?php echo HEADING_TITLE_CONTROLLER_CLEARALL; ?></td>
                  </tr>
                </table></td>
              </tr> 
                 
              <tr>
                <td><table border="0" width="50%">
                  <tr> 
                    <td align="right"><button class="btn btn-success btn-sm mt-3 mb-3" type="submit"><?php echo IMAGE_UPDATE; ?></button></td>
                   </tr>
                   <?php 
                   if (tep_not_null($updateTextCat)) { 
                    ?>
                    <tr>
                      <td class="sidebar-title mt-2"><?php echo $updateTextCat; ?></td>
                    </tr> 
                    <?php 
                  }  
                  if (tep_not_null($updateTextManuf)) { 
                    ?>
                    <tr>
                      <td class="sidebar-title mt-2"><?php echo $updateTextManuf; ?></td>
                    </tr>
                    <?php 
                  } 
                  if (tep_not_null($updateTextProd)) { 
                    ?>
                    <tr>
                      <td class="sidebar-title mt-2"><?php echo $updateTextProd; ?></td>
                    </tr>
                    <?php 
                  } 
                  ?> 
                </table></td>
              </tr>

              </form>
               <!-- end of Header Tags -->        
            </table>
            <!-- body_text_eof //-->

          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>