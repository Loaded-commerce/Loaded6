<?php
/*
  Written by Marc Sauton, September 2004
  Daily Product Report Contribution for the OsCommerce Community
  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();
  $date = (isset($_REQUEST['date']) ? $_REQUEST['date'] : '');
  $csv_accum = (isset($csv_accum) ? $csv_accum: '');
  // start csv - bounce csv string back as file
  if (isset($_POST['csv'])) {
    if ($_POST['saveas']) {  // rebound posted csv as save file
   $savename= $_POST['saveas'] . ".csv";
  }
  else $savename='unknown.csv';
  $csv_string = '';
  if ($_POST['csv']) $csv_string=$_POST['csv'];
  if (strlen($csv_string)>0){
    header("Expires: Mon, 26 Nov 1962 00:00:00 GMT");
    header("Last-Modified: " . gmdate('D,d M Y H:i:s') . ' GMT');
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-Type: Application/octet-stream");
    header("Content-Disposition: attachment; filename=$savename");
    echo $csv_string;
  }
  else echo "CSV string empty";
  exit;
  };
  //end csv

  if($date == "") {
     $date = date('Y-m-d'); #2003-09-07%
  } else {
      if(  preg_match("/([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})/", $date)) {
          $date = $date;
      } else {
          $date = date('Y-m-d'); #2003-09-07%
      }
  }
  if( $date == "") { $date = date('Y-m-d'); }
  $cal1maxdate = date("Y") . "," . date("m") . "," . date("d");

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<script language="javascript"><!--
  var cal1 = new ctlSpiffyCalendarBox("cal1", "dailyreportform", "reportdate","btnDate3","",scBTNMODE_CALBTN);
  cal1.readonly=true;
  cal1.displayLeft=true;
  // cal1.JStoRunOnSelect="document.dailyreportform.submit();";
<?php
  if (isset($_GET[tep_session_name()])) {
    $oscid = '&' . tep_session_name() . '=' . $_GET[tep_session_name()];
  } else {
    $oscid = '';
  }
?>
  cal1.JStoRunOnSelect="document.dailyreportform.action='<?php echo tep_href_link(basename($PHP_SELF));?>?date='+document.dailyreportform.reportdate.value + '<?php echo $oscid; ?>'; document.dailyreportform.submit();";
  cal1.useDateRange=true;
  cal1.setMinDate(2004,1,1);
  cal1.setMaxDate( <?php echo $cal1maxdate; ?> );
//--></script>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE . $date; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

<div class="col main-col">
  <div class="row">
	<div class="col-12 pr-0 mb-2">

		<table border="0" width="100%" cellspacing="3" cellpadding="3">
          <tr>
            <td class="main">
            <!-- BOF drop down OS -->
            <?php echo tep_draw_form('status', FILENAME_STATS_DAILY_SALES_REPORT, '', 'get');
            // get list of orders_status names for dropdown selection
            if (isset($_GET[tep_session_name()])) {
              echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
            }
            $orders_status_array = array();
            $orders_statuses[] = array('id' => '', 'text' => TEXT_ALL_ORDERS);
            $orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . $languages_id . "'");
            while ($orders_status = tep_db_fetch_array($orders_status_query)) {
                $orders_statuses[] = array('id' => $orders_status['orders_status_id'], 'text' => $orders_status['orders_status_name']);
                $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
            }
            echo TEXT_ORDERS_STATUS . ': ' . tep_draw_pull_down_menu('status', $orders_statuses, '', 'onChange="this.form.submit();"');
            if(isset($_GET['date']))  echo tep_draw_hidden_field('date', $date);
            ?>
             </form>
            <!-- EOF drop down OS --></td>
            <td class="main">
                <?php echo tep_draw_form('dailyreportform', FILENAME_STATS_DAILY_SALES_REPORT, 'date=' . $date, 'post');?>
                 <?php echo DISPLAY_ANOTHER_REPORT_DATE;?>
                 <input type="text" class="report_date form-control" name="reportdate" data-date-format="yyyy-mm-dd">
                </td>
            	<td class="main pt-3 pl-3"><?php echo '<button class="btn btn-success btn-sm" type="submit"> Go </button>'; ?></td>
               </form>
          </tr>
        </table>


	</div>
  </div>
</div>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-statsproductspurchased" class="table-statsproductspurchased">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">
<?php
$csv_accum .= "";
?>
        <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_NUMBER; ?></th>
                  <th scope="col" class="th-col dark text-left"><?php mirror_out(TABLE_HEADING_ORDER_QUANTITY); ?></th>
                  <th scope="col" class="th-col dark text-left"><?php mirror_out(TABLE_HEADING_PRODUCT_NAME); ?></th>
                  <th scope="col" class="th-col dark text-left"><?php mirror_out(TABLE_HEADING_PRODUCT_MODEL); ?></th>
                  <th scope="col" class="th-col dark text-left"><?php mirror_out(TABLE_HEADING_UNITPRICE); ?></th>
                  <th scope="col" class="th-col dark text-left"><?php mirror_out(TABLE_HEADING_PRODUCT_QUANTITY); ?></th>
                  <th scope="col" class="th-col dark text-right"><?php mirror_out(TABLE_HEADING_TOTAL_PURCHASED); ?>&nbsp;</th>
               </tr>
              </thead>
<?php
// new line for CSV
$csv_accum .= "\n";
//

  if (isset($_GET['page']) && ($_GET['page'] > 1)) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS - MAX_DISPLAY_SEARCH_RESULTS;
  $products_query_sql = "select ot.value, sum(ot.value) as dailyvalue, count(distinct o.orders_id) as howmany_orders, o.orders_id, sum(op.products_quantity) as howmany_tickets, op.products_id, op.products_name, op.products_model, op.final_price as ticket_price, op.final_price * sum(op.products_quantity) as howmuch from orders_total ot, orders o, orders_products op where ";
  $status = '';
  if (isset($_GET['status']) && $_GET['status'] != '') $status = tep_db_prepare_input($_GET['status']);
  if ($status <> '') $products_query_sql .= "o.orders_status ='" . $status . "' and ";
  $products_query_sql .= "o.date_purchased like \"$date%\" and o.orders_id = op.orders_id and ot.orders_id = op.orders_id and ot.class='ot_total' group by op.products_name";
  $products_query_raw = $products_query_sql;
  $customers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $products_query_raw, $products_query_numrows);
  $products_query_numrows = tep_db_query($products_query_sql);
  $products_query_numrows = tep_db_num_rows($products_query_numrows);

  $rows = 0;
  $total_purchased = 0;
  $list_total_purchased = 0;
  $products_query = tep_db_query($products_query_raw);
  while ($products = tep_db_fetch_array($products_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
    ?>
      <tr class="dataTableRow" onclick="document.location.href='<?php echo tep_href_link(FILENAME_CATEGORIES, 'action=new_product_preview&read=only&pID=' . $products['products_id'] . '?page=' . $_GET['page'], 'NONSSL'); ?>'">
        <td class="table-col dark text-left"><?php echo $rows; ?>.</td>
        <td class="table-col dark text-left"><?php mirror_out(number_format($products['howmany_orders'],0)) ?></td>
        <td class="table-col dark text-left"><?php mirror_out($products['products_name']) ?></td>
        <td class="table-col dark text-left"><?php mirror_out($products['products_model']) ?></td>
        <td class="table-col dark text-left"><?php mirror_out(number_format($products['ticket_price'],2)) ?></td>
        <td class="table-col dark text-left"><?php mirror_out(number_format($products['howmany_tickets'],0)) ?></td>
        <td class="table-col dark text-right"><?php mirror_out(number_format($products['howmuch'],2)); ?></td>
      </tr>
      <?php
      $list_total_purchased = $list_total_purchased + $products['howmuch'];
      // new line for CSV
      $csv_accum .= "\n";
      //
  }
  $total_purchased = 0;
  $total_query = tep_db_query("select op.final_price * op.products_quantity as total from orders o, orders_products op where o.date_purchased like \"$date%\" and o.orders_id = op.orders_id");
  while ($total_data = tep_db_fetch_array($total_query)) {
    $total_purchased += $total_data['total'];
  }
  ?>
    </table>

			<table border="0" width="100%" cellspacing="0" cellpadding="0" class="data-table">
			   <tr>
				 <td class="sidebar-title" align="center">
				   <?php  echo '<b>' . TABLE_DAILY_VALUE . $currencies->format($list_total_purchased) . '</b>&nbsp;&nbsp;&nbsp;';
				   echo '<b>' . TABLE_ACCUMULATED_VALUE . $currencies->format($total_purchased) . '</b>'; ?>
				 </td>
			   </tr>
				  <tr>
					<td class="text-center pt-3" colspan="4"><form action="<?php echo tep_href_link(FILENAME_STATS_DAILY_SALES_REPORT, '', 'SSL'); ?>" method=post>
					  <input type='hidden' name='csv' value='<?php echo $csv_accum; ?>'>
					  <input type="hidden" name="saveas" value="daily_product_sales_report_<?php echo date('YmdHi'); ?>">
					  <?php echo '<button class="btn btn-success btn-sm" type="submit">' . TEXT_BUTTON_REPORT_SAVE . '</button>'; ?>
					  </form>
					</td>
				  </tr>
			</table>

		    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="data-table-foot">
              <tr>
                <td class="smallText" valign="top"><?php echo $customers_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
                <td class="smallText" align="right"><?php echo $customers_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page'))); ?>&nbsp;</td>
              </tr>
            </table>

			<div class="mb-1">&nbsp;</div>
		 </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script script type="text/javascript">

 	$(document).ready(function() {
 		$('.report_date').datepicker({
 		 changeMonth: true,
          changeYear: true,
          todayHighlight: true,
 		});
 	});

</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');

function mirror_out ($field)
{
	global $csv_accum;
	echo $field;
	$field = strip_tags($field);
	$field = preg_replace ("/,/","",$field);
	if ($csv_accum=='') $csv_accum=$field;
	else
	{
		if (strrpos($csv_accum,chr(10)) == (strlen($csv_accum)-1))
			$csv_accum .= $field;
		else
			$csv_accum .= "," . $field;
	}
	return;
}

?>