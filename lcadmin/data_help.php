<?php
/*
  $Id: data_help.php,v 1.0.0.0 2008/05/13 13:41:11 wa4u Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License

*/
require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/help/data_help.php');

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
 
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-datahelp" class="table-datahelp">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">


	<table border="0" width="100%" cellspacing="0" cellpadding="0" class="data-table">
      <tr>  
        <?php
        if (isset($_GET['help_id'])) {
          $help_id = $_GET['help_id'];
        } else {
          $help_id = '';
          define('HEADING_TITLE', HEADING_TITLE0);
        }
        if ($help_id == '1') {
          define('HEADING_TITLE', HEADING_TITLE1);
        }
        if ($help_id == '2') {
          define('HEADING_TITLE', HEADING_TITLE2);
        }
        if ($help_id == '3') {
          define('HEADING_TITLE', HEADING_TITLE3);
        }
        if ($help_id == '4') {
          define('HEADING_TITLE', HEADING_TITLE4);
        }
        if ($help_id == '5') {
          define('HEADING_TITLE', HEADING_TITLE5);
        }
        if ($help_id == '6') {
          define('HEADING_TITLE', HEADING_TITLE6);
        }
        if ($help_id == '9') {
          define('HEADING_TITLE', HEADING_TITLE9);
        }
        if ($help_id == '10') {
          define('HEADING_TITLE', HEADING_TITLE10);
        } 
        if ($help_id == '11') {
          define('HEADING_TITLE', HEADING_TITLE11);
        }
        if ($help_id == '12') {
          define('HEADING_TITLE', HEADING_TITLE12);
        }
        ?>
        <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2" class="menuBoxHeading">

          <tr>
            <td valign="top" clsss="sidebar-title">
              <?php
              if ($help_id == '1') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_intro.html') ;
              }
              if ($help_id == '2') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_import.html') ;
              }
              if ($help_id == '3') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_export.html') ;
              }
              if ($help_id == '4') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_basicimport.html') ;
              }
              if ($help_id == '5') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_basicexport.html') ;
              }
              if ($help_id == '6') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_spreadsheet.html') ;
              }
              if ($help_id == '9') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_feed_intro.html') ;
              }
              if ($help_id == '10') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_googlebacisgettingstarted.html') ;
              }
              if ($help_id == '11') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_googleconfigure.html') ;
              }
              if ($help_id == '12') {
                include(DIR_WS_LANGUAGES . $language . '/help/ep/data_googlerun.html') ;
              }
              ?>
            </td>
          </tr>
          <?php 
          if(tep_not_null($help_id)){
            ?>
            <tr>
              <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?> </td>
            </tr>
            <tr>
              <td  valign="top" class="main" align="right"><a class="btn btn-success btn-sm mt-2 mb-2" href="<?php echo tep_href_link(FILENAME_DATA,'selected_box=data');?>"><?php echo tep_image_button('button_return.gif','Return');?></a></td>
            </tr>
            <?php 
          } 
          ?>
        </table></td>
      </tr>
    </table>
    
		 </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>    