<?php
/*
  $Id: create_order_admin.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-createorderadmin" class="table-createorderadmin">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">


			<table class="table w-100 mt-2">
              <tr>
                <td class="table-col text-center"><?php echo TEXT_CREATE_ORDERS_ADMIN_HELP ;?></td>
              </tr> 
              <tr>
                <td class="table-col text-center">
                  <?php 
                  echo TEXT_LABEL_CREATE_ORDERS_ADMIN_PAYMENT . ': &nbsp;<button type="button" class="btn btn-success btn-sm mt-2" onclick="window.open(\'' . tep_href_link(FILENAME_CREATE_ORDERS_PAY) . '\')">' . IMAGE_EDIT . '</button>';
                  ?>
                </td>
              </tr>
              <tr>
                <td class="table-col text-center">
                  <?php
                  echo TEXT_LABEL_CREATE_ORDERS_ADMIN_SHIPPING . ': &nbsp;<button type="button" class="btn btn-success btn-sm mt-2" onclick="window.open(\'' . tep_href_link(FILENAME_CREATE_ORDERS_SHIP) . '\')">' . IMAGE_EDIT . '</button>';
                  ?>
                </td>
              </tr>
            </table>

          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>            
