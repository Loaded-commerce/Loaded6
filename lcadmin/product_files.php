<?php
/*
  $Id: admin_files.php,v 1.1.1.1 2004/03/04 23:38:04 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$current_boxes = DIR_FS_ADMIN . DIR_WS_BOXES;
$current_files = DIR_FS_ADMIN;

if (isset($_GET['cID'])) {
  $cID = $_GET['cID'] ;
} else if (isset($_POST['cID'])) {
  $cID = $_POST['cID'] ;
} else {
  $cID = '' ;
}
if (isset($_GET['action'])) {
  $action = $_GET['action'] ;
} else if (isset($_POST['action'])) {
  $action = $_POST['action'] ;
} else {
  $action = '' ;
}
if (isset($_GET['cPath'])) {
  $cPath = $_GET['cPath'] ;
} else if (isset($_POST['cPath'])) {
  $cPath = $_POST['cPath'] ;
} else {
  $cPath = '' ;
}

if (tep_not_null($action)) {
  switch ($action) {
    case 'box_store':
    $sql_data_array = array('admin_files_name' => tep_db_prepare_input($_GET['box']),
      'admin_files_is_boxes' => '1');
    tep_db_perform(TABLE_ADMIN_FILES, $sql_data_array);
    $admin_boxes_id = tep_db_insert_id();

    tep_redirect(tep_href_link(FILENAME_ADMIN_FILES, 'cID=' . $admin_boxes_id));
    break;
    case 'box_remove':
  // NOTE: ALSO DELETE FILES STORED IN REMOVED BOX //
    $admin_boxes_id = tep_db_prepare_input($_GET['cID']);
    tep_db_query("delete from " . TABLE_ADMIN_FILES . " where admin_files_id = '" . $admin_boxes_id . "' or admin_files_to_boxes = '" . $admin_boxes_id . "'");

    tep_redirect(tep_href_link(FILENAME_ADMIN_FILES));
    break;
    case 'file_store':
    $sql_data_array = array('admin_files_name' => tep_db_prepare_input($_POST['admin_files_name']),
      'admin_files_to_boxes' => tep_db_prepare_input($_POST['admin_files_to_boxes']),
      'admin_files_is_boxes' => '0');
    tep_db_perform(TABLE_ADMIN_FILES, $sql_data_array);
    $admin_files_id = tep_db_insert_id();

    tep_redirect(tep_href_link(FILENAME_ADMIN_FILES, 'cPath=' . $_GET['cPath'] . '&fID=' . $admin_files_id));
    break;
    case 'file_remove':
    $admin_files_id = tep_db_prepare_input($_POST['admin_files_id']);
    tep_db_query("delete from " . TABLE_ADMIN_FILES . " where admin_files_id = '" . $admin_files_id . "'");

    tep_redirect(tep_href_link(FILENAME_ADMIN_FILES, 'cPath=' . $_GET['cPath']));
    break;
  }
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-admin-files" class="table-admin-files">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">

               <table class="table table-file-permissions w-100 mt-2">

                <tbody class="row_email_position">

                </tbody>
             </table>
             <a class="btn btn-success btn-sm mt-2 mb-2 ml-2 pull-right" href="javascript:void(0)" onclick="javascript:updateSortOrder()">Update Sort Order</a>

          </div>

        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
?>
<script type="text/javascript">
function updateSortOrder(){
     var selectedData = new Array();
     $('.row_email_position .toprow').each(function() {
	 selectedData.push($(this).attr("id"));
     });
     //alert(selectedData);
     updateGroupingSortOrder(selectedData);
    //file sort
     var selectedDatafile = new Array();
     $('.row_email_position_inner>tr').each(function() {
	 selectedDatafile.push($(this).attr("id"));
     });
     //alert(selectedDatafile);
     updateFilesSortOrder(selectedDatafile);
     $.gritter.add({ text:'Sort order updated successfully!', sticky:false,time:"2500"});
}
$( ".row_email_position" ).sortable({
 delay: 150,
 stop: function() {
    /* var selectedData = new Array();
     $('.row_email_position .toprow').each(function() {
	 selectedData.push($(this).attr("id"));
     });
     //alert(selectedData);
    // updateGroupingSortOrder(selectedData);
    */
 }
});
$( ".row_email_position_inner" ).sortable({
 delay: 150,
 stop: function() {
    /* var selectedData = new Array();
     $('.row_email_position_inner>tr').each(function() {
	 selectedData.push($(this).attr("id"));
     });
    // alert(selectedData);
     updateFilesSortOrder(selectedData);*/
 }
});
function updateGroupingSortOrder(data) {
      var requestURL = '<?php echo tep_href_link("ajax_common.php", "action=setgroupingsorting");?>';
        $.ajax({
            url:requestURL,
            type:'post',
            data:{position:data},
            success:function(response){
             // alert('Sorting updated successfully!');

            }
        })
}
function updateFilesSortOrder(data) {
      var requestURL = '<?php echo tep_href_link("ajax_common.php", "action=setfilesorting");?>';
        $.ajax({
            url:requestURL,
            type:'post',
            data:{position:data},
            success:function(response){
              //alert('Sorting updated successfully!');

            }
        })
}
function setFileStatus(admin_files_id, cstatus){
      if(cstatus == '1'){
        var message = 'Status Activated successfully';
        var params = 'status=1&admin_files_id='+admin_files_id;
      }else{
        var message = 'Status deactivated successfully';
        var params = 'status=0&admin_files_id='+admin_files_id;

      }
      var active_id = '.row_'+admin_files_id+ ' .sactive';
      var deactive_id = '.row_'+admin_files_id+ ' .sdeactive';

	 var requestURL = '<?php echo tep_href_link("ajax_common.php", "action=changefilestatus");?>';
         $.ajax({
            url:requestURL,
            type:'post',
            data: params,
            success:function(retval){
	    if(parseInt(retval) == 0){
	       $(active_id).css('display', 'none');
	       $(deactive_id).css('display', 'inline-block');

	    }
	    if(parseInt(retval) == 1){
	       $(active_id).css('display', 'inline-block');
	       $(deactive_id).css('display', 'none');
	    }
	    $.gritter.add({ text:message, sticky:false,time:"2500"});               	 }
	})
}
function setFileVisible(admin_files_id, cvisible){
      if(cvisible == '1'){
        var message = 'Visible status activated successfully';
        var params = 'visible=1&admin_files_id='+admin_files_id;
      }else{
        var message = 'Visible status deactivated successfully';
        var params = 'visible=0&admin_files_id='+admin_files_id;

      }
      var active_id = '.row_'+admin_files_id+ ' .svisible';
      var deactive_id = '.row_'+admin_files_id+ ' .sinvisible';

	 var requestURL = '<?php echo tep_href_link("ajax_common.php", "action=changefilevisible");?>';
         $.ajax({
            url:requestURL,
            type:'post',
            data: params,
            success:function(retval){
	    if(parseInt(retval) == 0){
	       $(active_id).css('display', 'none');
	       $(deactive_id).css('display', 'inline-block');

	    }
	    if(parseInt(retval) == 1){
	       $(active_id).css('display', 'inline-block');
	       $(deactive_id).css('display', 'none');
	    }
	    $.gritter.add({ text:message, sticky:false,time:"2500"});               	 }
	})
}

</script>
<script>
 function showHideSection(fileid){
     $('.filesection_'+fileid).toggle();

     if($( '#pm_'+fileid).hasClass( "open" )){
        $('#pm_'+fileid+ ' .fa').removeClass( "fa-minus-square" );
        $('#pm_'+fileid+ ' .fa').addClass( "fa-plus-square" );
         $('#pm_'+fileid).removeClass( "open" );
     }else{
     	$('#pm_'+fileid).addClass( "open" );
        $('#pm_'+fileid+ ' .fa').removeClass( "fa-plus-square" );
        $('#pm_'+fileid+ ' .fa').addClass( "fa-minus-square" );

     }
 }
</script>
<?php
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>