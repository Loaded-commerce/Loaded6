<?php
/*
  $Id: articles_config.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  
  // get the configuration group ID
  $cfg_group_query = tep_db_query("select configuration_group_id from " . TABLE_CONFIGURATION_GROUP . " where  configuration_group_title = '" . tep_db_input('Articles Management') . "'");
  $cfg_group = tep_db_fetch_array($cfg_group_query);
  $gID = $cfg_group['configuration_group_id'];
  
  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  $cID = (isset($_GET['cID']) ? $_GET['cID'] : '');
  if ($action) {
    switch ($action) {
      case 'save':
        $configuration_value = tep_db_prepare_input($_POST['configuration_value']);
        $cID = tep_db_prepare_input($_GET['cID']);

        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value) . "', last_modified = now() where configuration_id = '" . tep_db_input($cID) . "'");
        tep_redirect(tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cID));
        break;
    }
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('taxrates') > 0) {
      echo $messageStack->output('taxrates'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-articlesconfig" class="table-articlesconfig">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CONFIGURATION_TITLE; ?></th>
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CONFIGURATION_VALUE; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
<?php
  $configuration_query = tep_db_query("select configuration_id, configuration_title, configuration_value, use_function from " . TABLE_CONFIGURATION . " where configuration_group_id = '" . $gID . "' order by sort_order");
  while ($configuration = tep_db_fetch_array($configuration_query)) {
    if (tep_not_null($configuration['use_function'])) {
      $use_function = $configuration['use_function'];
      if (preg_match('/->/', $use_function)) {
        $class_method = explode('->', $use_function);
        if (!is_object(${$class_method[0]})) {
          include(DIR_WS_CLASSES . $class_method[0] . '.php');
          ${$class_method[0]} = new $class_method[0]();
        }
        $cfgValue = tep_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
      } else {
        $cfgValue = tep_call_function($use_function, $configuration['configuration_value']);
      }
    } else {
      $cfgValue = $configuration['configuration_value'];
    }

    if (((!isset($cID) && !is_object($cID)) || (@$cID == $configuration['configuration_id'])) && (!is_object($cInfo)) && (substr($action, 0, 3) != 'new')) {
      $cfg_extra_query = tep_db_query("select configuration_key, configuration_description, date_added, last_modified, use_function, set_function from " . TABLE_CONFIGURATION . " where configuration_id = '" . $configuration['configuration_id'] . "'");
      $cfg_extra = tep_db_fetch_array($cfg_extra_query);

      $cInfo_array = array_merge($configuration, $cfg_extra);
      $cInfo = new objectInfo($cInfo_array);
    }

	$selected = ((isset($cInfo)) && (is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id))? true : false;
	$col_selected = ($selected) ? ' selected' : '';
    if ($selected) {
      echo '                  <tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cInfo->configuration_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '                  <tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $gID . '&cID=' . $configuration['configuration_id']) . '\'">' . "\n";
    }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $configuration['configuration_title']; ?></td>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo htmlspecialchars($cfgValue); ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if ( ((isset($cInfo)) && (is_object($cInfo))) && ($configuration['configuration_id'] == $cInfo->configuration_id) ) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>' ; } else { echo '<a href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $gID . '&cID=' . $configuration['configuration_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>
            </table>




              </div>
	        </div>
	      </div>   
	      <!-- end body_text //-->
	    </div>
	    <!-- end panel -->
	  </div>
	</div>
	<!-- body_eof //-->
	<?php 
	include(DIR_WS_INCLUDES . 'html_bottom.php');
	require(DIR_WS_INCLUDES . 'application_bottom.php'); 
	?>
