<?php
/*
  $Id: faq_list.php,v 1.1 2008/06/11 00:18:17 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo $title; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('faqlist') > 0) {
      echo $messageStack->output('faqlist'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-faqlist" class="table-faqlist">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">

  <table class="table table-hover w-100 mt-2">
	  <thead>
	   <tr class="th-row">
		<th scope="col" class="th-col dark text-center"><?php echo FAQ_NUMBER;?></th>
		<th scope="col" class="th-col dark text-center"><?php echo FAQ_DATE;?></th>
		<th scope="col" class="th-col dark text-center"><?php echo tep_image(DIR_WS_IMAGES . 'icons/sort.gif', FAQ_SORT_BY); ?></th>
		<th scope="col" class="th-col dark text-center"><?php echo FAQ_QUESTION;?></th>
		<th scope="col" class="th-col dark text-center"><?php echo FAQ_ID;?></th>
		<th scope="col" class="th-col dark text-center"><?php echo FAQ_STATUS;?></th>
		<th scope="col" class="th-col dark text-center"><?php echo FAQ_ACTION;?></th>
	  </tr>
	 </thead>
	 <tbody>
    <?php 
    $no = 1;
    if (sizeof($data) > 0) {
	  foreach($data as $key => $val) {
        $no % 2 ? $bgcolor="#DEE4E8" : $bgcolor="#F0F1F1";
        ?>
        <tr>
          <td class="table-col dark text-center"><?php echo $no;?></td>
          <td class="table-col dark text-center"><?php echo $val['d']?></td>
          <td class="table-col dark text-center"><?php echo $val['v_order'];?></td>
          <td class="table-col dark text-center"><?php echo $val['question'] . ' (' . $val['language'] . ')';?></td>
          <td class="table-col dark text-center"><?php echo $val['faq_id'];?></td>
          <td class="table-col dark text-center">
            <?php 
            if ($val['visible'] == 1) {
              echo '<i class="fa fa-lg fa-check-circle text-success mr-2"></i><a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Visible&faq_id=$val[faq_id]&visible=$val[visible]") . '"><i class="fa fa-lg fa-times-circle text-secondary mr-2"></i></a>';
            } else {
              echo '<i class="fa fa-lg fa-times-circle text-danger mr-2"></i><a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Visible&faq_id=$val[faq_id]&visible=$val[visible]") . '"><i class="fa fa-lg fa-check-circle text-secondary mr-2"></i></a>';
            }
            ?>
          </td>
          <td class="table-col dark text-center">
            <?php echo '<a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Edit&faq_id=$val[faq_id]&faq_lang=$val[language]", 'NONSSL') . '"><i class="fa fa-edit fa-lg text-success mr-2"></i></a><a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Delete&faq_id=$val[faq_id]", 'NONSSL') . '"><i class="fa fa-trash fa-lg text-danger mr-2"></i></a>'; ?>
          </td>
        </tr>
        <?php 
        $no++;
      }
    } else {
      ?>
      <tr>
        <td colspan="7" class="table-col dark text-center"><?php echo FAQ_ALERT; ?></td>
      </tr>
      <?php 
    }
    ?>
  </table>

  <table border="0" width="100%" cellspacing="0" cellpadding="2">
	<tr>
	  <td align="right">
		<?php echo '<a class="btn btn-default btn-sm mt-2 mb-2 mr-2"  href="' . tep_href_link(FILENAME_FAQ_MANAGER, '', 'NONSSL') . '">' . IMAGE_CANCEL . '</a><a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_FAQ_MANAGER, 'faq_action=Added', 'NONSSL') . '">' . FAQ_ADD . '</a>'; ?>
	  </td>
	</tr>
    <tr>              
    <?php
    // RCI code start
    echo $cre_RCI->get('faqlist', 'listingbottom');
    // RCI code eof
    ?>
    </tr>
  </table>
  
            </div>
          </div>
        </div>   
        <!-- end body_text //-->
      </div>
      <!-- end panel -->
    </div>
  </div>
  <!-- body_eof //-->
