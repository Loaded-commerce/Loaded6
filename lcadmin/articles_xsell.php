<?php
/*
  $Id: articles_xsell.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  cross.sale.php created By Isaac Mualem im@imwebdesigning.com

  Modified by Andrew Edmond (osc@aravia.com)
  Sept 16th, 2002

  Further Modified by Rob Anderson 12 Dec 03

  Released under the GNU General Public License
*/

/* general_db_conct($query) function */
  /* calling the function:  list ($test_a, $test_b) = general_db_conct($query); */
  require('includes/application_top.php');
  
  if (isset($_GET['add_related_article_ID'])) {
    $add_related_article_ID = (int)$_GET['add_related_article_ID'];
  } elseif (isset($_POST['add_related_article_ID'])) {
    $add_related_article_ID = (int)$_POST['add_related_article_ID'];
  }


include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('taxrates') > 0) {
      echo $messageStack->output('taxrates'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-articlexsell" class="table-articlexsell">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">


<?php
// first major piece of the program
// we have no instructions, so just dump a full list of products and their status for cross selling 
if (!isset($add_related_article_ID) )
{
	$query = "select a.articles_id, ad.articles_name, ad.articles_description, ad.articles_url from " . TABLE_ARTICLES . " a, " . TABLE_ARTICLES_DESCRIPTION . " ad where ad.articles_id = a.articles_id and ad.language_id = '" . (int)$languages_id . "' order by ad.articles_name";
	$rs = tep_db_query($query);
?>
        
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo HEADING_ARTICLE_ID;?></th>
                <th scope="col" class="th-col dark text-left"><?php echo HEADING_ARTICLE_NAME; ?></th>
                <th scope="col" class="th-col dark text-left"><?php echo HEADING_CROSS_ASSOCIATION; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo HEADING_CROSS_SELL_ACTIONS; ?></th>
              </tr>
             </thead>
			<?php 
			$num_of_articles = tep_db_num_rows($rs);
			while($rw = tep_db_fetch_array($rs))
			{
				/* now we will query the DB for existing related items */
				$query = "select pd.products_name, ax.xsell_id from " . TABLE_ARTICLES_XSELL . " ax, " . TABLE_PRODUCTS_DESCRIPTION . " pd where pd.products_id = ax.xsell_id and ax.articles_id ='".  $rw['articles_id'] ."' and pd.language_id = '" . (int)$languages_id . "' order by ax.sort_order";
				$rs_related = tep_db_query($query);

				echo "<tr bgcolor='#FFFFFF'>";
				echo "<td class=\"table-col dark text-left\" valign=\"top\">&nbsp;".$rw['articles_id']."&nbsp;</td>\n";
				echo "<td class=\"table-col dark text-left\" valign=\"top\">&nbsp;".$rw['articles_name']."&nbsp;</td>\n";
				if(tep_db_num_rows($rs_related) > 0)
				{
					echo "<td  class=\"dataTableContent\"><ol>";
					while($rw_related = tep_db_fetch_array($rs_related))
					{
						echo '<li>'. $rw_related['products_name'] .'&nbsp;';
					}
					echo"</ol></td>\n";
				}
				else
					echo "<td class=\"table-col dark text-left\">--</td>\n";
				echo '<td class="table-col dark text-left"  valign="top">&nbsp;<a href="' . tep_href_link(FILENAME_ARTICLES_XSELL, 'add_related_article_ID=' . $rw['articles_id'], 'SSL') . '">'.TEXT_ADD_REMOVE.'</a></td>';

				if(tep_db_num_rows($rs_related) > 1)
				{
					echo '<td class="table-col dark text-left" valign="top">&nbsp;<a href="' . tep_href_link(FILENAME_ARTICLES_XSELL, 'sort=1&add_related_article_ID=' . $rw['articles_id'], 'SSL') . '">'.TEXT_OF_SORT.'</a>&nbsp;</td>';
				} else {
					echo "<td class=\"table-col dark text-left\">--</td>";
				}
				echo "</tr>\n";
			}
			?>
            </table>
<?php 
} // the end of -> if (!$add_related_article_ID)

if ($_POST && !$sort)
{
	if ($_POST['run_update']==true)
	{
		$query ="DELETE FROM " . TABLE_ARTICLES_XSELL . " WHERE articles_id = '".$_POST['add_related_article_ID']."'";
		if (!tep_db_query($query))
			exit(TEXT_NO_DELETE);
	}
	if ($_POST['xsell_id'])
	foreach ($_POST['xsell_id'] as $temp)
	{
		$query = "INSERT INTO " . TABLE_ARTICLES_XSELL . " VALUES (''," . $_POST['add_related_article_ID'] . ", " . $temp . ",1)";
		if (!tep_db_query($query))
		exit(TEXT_NO_INSERT);
	}
?>
		<table width="100%" border="0" cellpadding="0"  cellspacing="0">
              <tr>
                  <td class="main"><?php echo TEXT_DATABASE_UPDATED; ?></td>
                </tr>
                <tr>
                  <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="main"><?php echo sprintf(TEXT_LINK_SORT_PRODUCTS, tep_href_link(FILENAME_ARTICLES_XSELL, '&sort=1&add_related_article_ID=' . $add_related_article_ID, 'SSL')); ?></td>
                </tr>
                <tr>
                  <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="main"><?php echo sprintf(TEXT_LINK_MAIN_PAGE, tep_href_link(FILENAME_ARTICLES_XSELL, '', 'SSL')); ?></td>
                </tr>
                <tr>
                  <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
		</table>                
<?php
	//    if ($_POST[xsell_id])
	//  echo '<a href="' . tep_href_link(FILENAME_ARTICLES_XSELL, 'sort=1&add_related_article_ID=' . $_POST[add_related_article_ID], 'SSL') . '">Click here to sort (top to bottom) the added cross sale</a>' . "\n";
}
    
if (isset($add_related_article_ID) && ! $_POST && !$sort)
{
?>
	<form action="<?php tep_href_link(FILENAME_ARTICLES_XSELL, '', 'SSL'); ?>" method="post">
     <table class="table table-hover w-100 mt-2">
	  <thead>
	   <tr class="th-row">
		  <th scope="col" class="th-col dark text-left">&nbsp;</th>
		  <th scope="col" class="th-col dark text-center"><?php echo HEADING_ARTICLE_ID;?></th>
		  <th scope="col" class="th-col dark text-left"><?php echo HEADING_PRODUCT_NAME; ?></th>
		</tr>
	  </thead>

	<?php
        $query = "select * from " . TABLE_ARTICLES_XSELL . " where articles_id = '".$add_related_article_ID."'";
        $rs_xsell = tep_db_query($query);
        while($rw_xsell = tep_db_fetch_array($rs_xsell))
        {
        	$ID_PR[] = $rw_xsell['ID'];
        	$products_id_pr[] = $rw_xsell['articles_id'];
        	$xsell_id_pr[] = $rw_xsell['xsell_id'];
        }
            //list ($ID_PR, $products_id_pr, $xsell_id_pr) = general_db_conct($query);
		$query = "select p.products_id, pd.products_name, pd.products_description, pd.products_url from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where pd.products_id = p.products_id and pd.language_id = '" . (int)$languages_id . "' order by pd.products_name";
		$rs = tep_db_query($query);
		$num_of_products = tep_db_num_rows($rs);
		while($rw = tep_db_fetch_array($rs))
		{
          ?><tr>
            <td class="table-col dark text-left">
          
          <input <?php /* this is to see it it is in the DB */
            $run_update=false; // set to false to insert new entry in the DB
            if ($xsell_id_pr) foreach ($xsell_id_pr as $compare_checked)if ($rw['products_id']===$compare_checked) {echo "checked"; $run_update=true;} ?> size="20"  size="20"  name="xsell_id[]" type="checkbox" value="<?php echo $rw['products_id']; ?>"></td>
          
          <?php echo "<td  class=\"table-col dark text-center\">".$rw['products_id']."</td>\n"
            ."<td class=\"table-col dark text-left\">". $rw['products_name'] ."</td>\n";
          }?>
          <tr>
                      <td class="table-col dark text-left">&nbsp;</td>
                      <td class="table-col dark text-left">&nbsp;</td>
                      <td class="table-col dark text-left">
                        <input type="hidden" name="run_update" value="<?php if ($run_update==true) echo "true"; else echo "false" ?>">
		                <input type="hidden" name="add_related_article_ID" value="<?php echo $add_related_article_ID; ?>">
                        <?php echo '<button class="btn btn-success btn-sm mt-2 mb-2">'. IMAGE_SAVE .'</button> <a class="btn btn-default btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_ARTICLES_XSELL, '', 'SSL') . '">' . IMAGE_CANCEL . '</a>'; ?>
                      </td>
                </tr>
              </form>
            </table>
<?php 
}
// sort routines
if ((isset($_GET['sort'])) && ($_GET['sort']==1))
{
	//  first lets take care of the DB update.
	$run_once=0;
	if ($_POST)
	{
		foreach ($_POST as $key_a => $value_a)
		{
			if (is_numeric ($value_a)) {
				$query = "UPDATE " . TABLE_ARTICLES_XSELL . " SET sort_order = '".$value_a."' WHERE xsell_id= '" . $key_a . "' AND articles_id = '" . $_GET['add_related_article_ID'] . "'";
			}
			if ($value_a != 'Update')
			if (!tep_db_query($query))
				exit(TEXT_NO_UPDATE);
			else
			if ($run_once==0)
			{ 
?>
                <tr>
                  <td class="main"><?php echo TEXT_DATABASE_UPDATED; ?></td>
                </tr>
                <tr>
                  <td class="main"><?php echo sprintf(TEXT_LINK_MAIN_PAGE, tep_href_link(FILENAME_ARTICLES_XSELL, '', 'SSL')); ?></td>
                </tr>
<?php
				$run_once++;
			}

		}// end of foreach.
	}
?>
		  <form method="post" action="<?php echo tep_href_link(FILENAME_ARTICLES_XSELL, 'sort=1&add_related_article_ID=' . $add_related_article_ID, 'SSL'); ?>">
             <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo HEADING_ARTICLE_ID;?></th>
                  <th scope="col" class="th-col dark text-left"><?php echo HEADING_PRODUCT_NAME; ?></th>
                  <th scope="col" class="th-col dark text-center"><?php echo HEADING_PRODUCT_ORDER; ?></th>
                </tr>
              </thead>
              <tbody>
<?php 
		$query = "select * from " . TABLE_ARTICLES_XSELL . " where articles_id = '".$add_related_article_ID."'";
		//list ($ID_PR, $products_id_pr, $xsell_id_pr, $order_PR) = general_db_conct($query);
		//$ordering_size =sizeof($ID_PR);
		//for ($i=0;$i<$ordering_size;$i++)
		$rs_sort = tep_db_query($query);
		$ordering_size = tep_db_num_rows($rs_sort);
		while($rw_sort = tep_db_fetch_array($rs_sort))
		{
			$query = "select p.products_id, pd.products_name, pd.products_description, pd.products_url from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where pd.products_id = p.products_id and pd.language_id = '" . (int)$languages_id . "' and p.products_id = ".$rw_sort['xsell_id']."";
			$rw_prod_data = tep_db_fetch_array(tep_db_query($query));
?>
          <tr class="dataTableContentRow" bgcolor="#FFFFFF">
            <td class="table-col dark text-left"><?php echo $rw_prod_data['products_id']; ?></td>
            <td class="table-col dark text-left"><?php echo $rw_prod_data['products_name']; ?></td>
            <td class="table-col dark text-center"><select name="<?php echo $rw_prod_data['products_id']; ?>">
              <?php for ($y=1;$y<=$ordering_size;$y++)
                  {
                echo "<option value=\"$y\"";
                  if (!(strcmp($y, $rw_sort['sort_order']))) {echo ' selected="selected" ';}
                  echo ">$y</option>";
                }
                ?>
            </select></td>
          </tr>
          <?php } // the end of foreach
			?>
		<tr>
		  <td class="table-col dark text-left">&nbsp;</td>
		  <td class="table-col dark text-left"><?php echo '<button class="btn btn-success btn-sm mt-2 mb-2">'. IMAGE_SAVE .'</button> <a class="btn btn-default btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_ARTICLES_XSELL, '', 'SSL') . '">' . IMAGE_CANCEL . '</a>'; ?></td>
		  <td class="table-col dark text-left">&nbsp;</td>
		</tr>
	  </tbody>
	  </table>
	</form>
      
<?php 
}
?>
    

          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
