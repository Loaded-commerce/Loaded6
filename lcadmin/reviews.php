<?php
/*
  $Id: reviews.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require(DIR_WS_CLASSES . 'Reviews.class.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');
$reviews_rating = (isset($_POST['reviews_rating']) ? $_POST['reviews_rating'] : '');
$reviews_text = (isset($_POST['reviews_text']) ? $_POST['reviews_text'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'update':
      $reviews_id = tep_db_prepare_input($_GET['rID']);
      $reviews_rating = tep_db_prepare_input($reviews_rating);
      $reviews_text = tep_db_prepare_input($reviews_text);
      tep_db_query("update " . TABLE_REVIEWS . " set reviews_rating = '" . tep_db_input($reviews_rating) . "', last_modified = now() where reviews_id = '" . (int)$reviews_id . "'");
      tep_db_query("update " . TABLE_REVIEWS_DESCRIPTION . " set reviews_text = '" . tep_db_input($reviews_text) . "' where reviews_id = '" . (int)$reviews_id . "'");
      tep_redirect(tep_href_link(FILENAME_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $reviews_id));
      break;
    case 'deleteconfirm':
      $reviews_id = tep_db_prepare_input($_GET['rID']);
      tep_db_query("delete from " . TABLE_REVIEWS . " where reviews_id = '" . (int)$reviews_id . "'");
      tep_db_query("delete from " . TABLE_REVIEWS_DESCRIPTION . " where reviews_id = '" . (int)$reviews_id . "'");
      tep_redirect(tep_href_link(FILENAME_REVIEWS, 'page=' . $_GET['page']));
      break;
    case 'setapproveStatus':
      $reviews_id = tep_db_prepare_input($_GET['rID']);
      if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
        if (isset($reviews_id)) {
          tep_set_review_approve_status($reviews_id, $_GET['flag']);
        }
         echo $_GET['flag'];exit;
      }
    break;
  }
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-reviews" class="table-reviews">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_PRODUCTS; ?></th>
                  <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_APPROVE; ?></th>
                  <th scope="col" class="th-col dark text-center d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_RATING; ?></th>
                  <th scope="col" class="th-col dark text-center d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_DATE_ADDED; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $reviews_query_raw = "SELECT reviews_id, products_id, date_added, last_modified, reviews_rating,isapproved
                                        from " . TABLE_REVIEWS . "
                                      ORDER BY date_added DESC";
                $reviews_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $reviews_query_raw, $reviews_query_numrows);
                $reviews_query = tep_db_query($reviews_query_raw);
                $cnt = 0;
                while ($reviews = tep_db_fetch_array($reviews_query)) {
                  $cnt++;
                  if ((!isset($_GET['rID']) || (isset($_GET['rID']) && ($_GET['rID'] == $reviews['reviews_id']))) && !isset($rInfo)) {
                    $reviews_text_query = tep_db_query("SELECT r.reviews_read, r.customers_name, length(rd.reviews_text) as reviews_text_size, isapproved
                                                          from " . TABLE_REVIEWS . " r,
                                                               " . TABLE_REVIEWS_DESCRIPTION . " rd
                                                        WHERE r.reviews_id = '" . (int)$reviews['reviews_id'] . "'
                                                          and r.reviews_id = rd.reviews_id");
                    $reviews_text = tep_db_fetch_array($reviews_text_query);
                    $products_image_query = tep_db_query("SELECT products_image
                                                            from " . TABLE_PRODUCTS . "
                                                          WHERE products_id = '" . (int)$reviews['products_id'] . "'");
                    $products_image = tep_db_fetch_array($products_image_query);
                    $products_name_query = tep_db_query("SELECT products_name
                                                           from " . TABLE_PRODUCTS_DESCRIPTION . "
                                                         WHERE products_id = '" . (int)$reviews['products_id'] . "'
                                                         and language_id = '" . (int)$languages_id . "'");
                    $products_name = tep_db_fetch_array($products_name_query);
                    $reviews_average_query = tep_db_query("SELECT (avg(reviews_rating) / 5 * 100) as average_rating
                                                             from " . TABLE_REVIEWS . "
                                                           WHERE products_id = '" . (int)$reviews['products_id'] . "'");
                    $reviews_average = tep_db_fetch_array($reviews_average_query);
                    $review_info = array_merge((array)$reviews_text, (array)$reviews_average, (array)$products_name);
                    $rInfo_array = array_merge((array)$reviews, (array)$review_info, (array)$products_image);
                    $rInfo = new objectInfo($rInfo_array);
                  }
                  $selected = (isset($rInfo) && is_object($rInfo) && ($reviews['reviews_id'] == $rInfo->reviews_id)) ? ' selected' : '';
                  if ($selected) {
                    echo '<tr class="table-row dark selected" id="crow_'.$reviews['reviews_id'].'">' . "\n";
                  	$onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id . '&action=preview') . '\'"';
                  } else {
                    echo '<tr class="table-row dark" id="crow_'.$reviews['reviews_id'].'">' . "\n";
                    $onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $reviews['reviews_id']) . '\'"';
                  }
                  $col_selected = ($selected) ? ' selected' : '';
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo '<i class="fa fa-circle-o fa-lg text-info mr-2"></i>' . tep_get_products_name($reviews['products_id']); ?></td>
                  <td class="setapproveStatus table-col dark text-center <?php echo $col_selected; ?>">

					  <?php
					  $ajax_link = tep_href_link(FILENAME_REVIEWS);
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setapproveStatus\',\''.$ajax_link.'\',\'action=setapproveStatus&flag=0&rID='.$reviews['reviews_id'].'\', '.$reviews['reviews_id'].',0 )" '.(($reviews['isapproved'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setapproveStatus\',\''.$ajax_link.'\',\'action=setapproveStatus&flag=1&rID='.$reviews['reviews_id'].'\', '.$reviews['reviews_id'].',1  )" '.(($reviews['isapproved'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
					  ?>

                  </td>
                  <td class="table-col dark text-center d-none d-lg-table-cell col-blank<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo tep_image('assets/img/stars_' . $reviews['reviews_rating'] . '.png'); ?></td>
                  <td class="table-col dark text-center d-none d-lg-table-cell col-blank<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo tep_date_short($reviews['date_added']); ?></td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?>" <?php echo $onclick;?> >
                    <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $reviews['reviews_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                  </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>

            <div class="pagination-container ml-2 mr-2 mb-3">
              <div class="results-right"><?php echo $reviews_split->display_count($reviews_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></div>
              <div class="results-left"><?php echo $reviews_split->display_links($reviews_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></div>
            </div>
            <?php
            // RCI code start
            echo $cre_RCI->get('reviews', 'bottom');
            // RCI code eof
            ?>
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              switch ($action) {
                case 'delete':
                  $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_REVIEW);
                  $contents[] = array('form' => tep_draw_form('reviews', FILENAME_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id . '&action=deleteconfirm'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0 fw-400">' . sprintf(TEXT_INFO_DELETE_REVIEW_INTRO, $rInfo->products_name) . '</div></div></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_CONFIRM_DELETE . '</button><button type="button" class="btn btn-grey btn-sm mr-2 mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                default:
                  if (isset($rInfo) && is_object($rInfo)) {

                    $rData = Reviews::get($rInfo->reviews_id);

                    $heading[] = array('text' => '<div class="text-truncate">' . $rInfo->products_name . '</div>');
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
                        <button class="btn btn-success btn-sm mt-2 btn-edit" data-toggle="modal" data-target="#editModal">' . IMAGE_EDIT . '</button>
                        <button class="btn btn-danger btn-sm mr-1 ml-1 mt-2 btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id . '&action=delete')  . '\'">' . IMAGE_DELETE . '</button>');
                    $contents[] = array('text' => '<div class="quote-container border rounded mt-3">
                                                     <blockquote class="blockquote pl-2 pr-2 mb-0 pb-2">
                                                       <p class="mb-0 blockquote-text"><i class="fa fa-quote-left f-s-10 align-top mr-1 mt-1"></i>' . $rData->reviews_text . '<i class="fa fa-quote-right fa-xs ml-2 f-s-10 align-top mt-1"></i></p>
                                                         <footer class="blockquote-footer text-right mt-0">' . $rData->customers_name . '</footer></blockquote></div>');
                    //$contents[] = array('align' => 'center', 'text' => '<div class="sidebar-img mt-3 well ml-4 mr-4 mb-3">' . tep_info_image($rInfo->products_image, $rInfo->products_name, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</div><div class="sidebar-text mb-0 mt-0">' . $cInfo->categories_image . '</div>');
                    //$contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_REVIEW_AUTHOR . '<span class="sidebar-title ml-2">' . $rInfo->customers_name . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-2">' . TEXT_INFO_REVIEW_RATING . '<span class="sidebar-title ml-2">' . tep_image('assets/img/stars_' . $rInfo->reviews_rating . '.png') . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_REVIEW_READ . '<span class="sidebar-title ml-2">' . $rInfo->reviews_read . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_REVIEW_SIZE . '<span class="sidebar-title ml-2">' . $rInfo->reviews_text_size . ' bytes</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_PRODUCTS_AVERAGE_RATING . '<span class="sidebar-title ml-2">' . number_format($rInfo->average_rating, 2) . '%</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($rInfo->date_added) . '</span></div>');
                    if (tep_not_null($rInfo->last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($rInfo->last_modified) . '</span></div>');
                    $contents[] = array('text' => '<div class="mb-3"></div>');
                  } else {
                    $heading[] = array('text' => TEXT_INFO_REVIEWS);
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2 mb-3"><div class="note note-info m-0 fw-400">' . TEXT_NO_REVIEWS . '</div></div></div>');
                  }
                  break;
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php
      $rID = (isset($rInfo)) ? $rInfo->reviews_id : $_GET['rID'];
      $rData = Reviews::get($rID);
      ?>
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Review for <?php echo '<b>' . $rData->products_name . '</b>'; ?></h5>
        <button type="button" class="close modal-close-fix" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo tep_draw_form('review', FILENAME_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rID . '&action=update'); ?>
        <div class="modal-body">
          <div class="row">
            <div class="col-8">
              <div class="sidebar-text"><?php echo ENTRY_FROM; ?><span class="sidebar-title ml-2"><b><?php echo $rData->customers_name; ?></b></span></div>
              <div class="sidebar-text"><?php echo ENTRY_DATE; ?><span class="sidebar-title ml-2 mt-3"><b><?php echo tep_date_short($rData->date_added); ?></b></span></div></td>

              <div class="form-group">
                <span class="col-2 pl-0 pr-0 sidebar-text pb-3"><?php echo ENTRY_RATING; ?></label>
                <div class="col-md-10 mt-1 pl-0">
                  <input type="text" id="rangeSlider" name="reviews_rating" value="<?php echo $rData->reviews_rating; ?>" />
                </div>
              </div>

            </div>
            <div class="col-4">
              <?php echo tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $rData->products_image, $rData->products_name, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'hspace="5" vspace="5"'); ?>
            </div>
          </div>

          <div class="form-group">
            <label class=""><?php echo ENTRY_REVIEW_TEXT; ?></label>
            <textarea class="form-control" name="reviews_text" rows="10" cols="90"><?php echo $rData->reviews_text; ?></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="document.review.submit();"><?php echo IMAGE_UPDATE; ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
$("#rangeSlider").ionRangeSlider({
    min: 0,
    max: 5,
    step: 1,
    grid: true,
    grid_snap: true
});
</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>