<?php
/*
  $Id: geo_zones.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$saction = (isset($_GET['saction']) ? $_GET['saction'] : '');

if (tep_not_null($saction)) {
  switch ($saction) {
    case 'insert_sub':
      $zID = tep_db_prepare_input($_GET['zID']);
      $zone_country_id = tep_db_prepare_input($_POST['zone_country_id']);
      $zone_id = tep_db_prepare_input($_POST['zone_id']);

      tep_db_query("insert into " . TABLE_ZONES_TO_GEO_ZONES . " (zone_country_id, zone_id, geo_zone_id, date_added) values ('" . (int)$zone_country_id . "', '" . (int)$zone_id . "', '" . (int)$zID . "', now())");
      $new_subzone_id = tep_db_insert_id();

      tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $new_subzone_id));
      break;
    case 'save_sub':
      $sID = tep_db_prepare_input($_GET['sID']);
      $zID = tep_db_prepare_input($_GET['zID']);
      $zone_country_id = tep_db_prepare_input($_POST['zone_country_id']);
      $zone_id = tep_db_prepare_input($_POST['zone_id']);

      tep_db_query("update " . TABLE_ZONES_TO_GEO_ZONES . " set geo_zone_id = '" . (int)$zID . "', zone_country_id = '" . (int)$zone_country_id . "', zone_id = " . (tep_not_null($zone_id) ? "'" . (int)$zone_id . "'" : 'null') . ", last_modified = now() where association_id = '" . (int)$sID . "'");

      tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $_GET['sID']));
      break;
    case 'deleteconfirm_sub':
      $sID = tep_db_prepare_input($_GET['sID']);

      tep_db_query("delete from " . TABLE_ZONES_TO_GEO_ZONES . " where association_id = '" . (int)$sID . "'");

      tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage']));
      break;
  }
}

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'insert_zone':
      $geo_zone_name = tep_db_prepare_input($_POST['geo_zone_name']);
      $geo_zone_description = tep_db_prepare_input($_POST['geo_zone_description']);

      tep_db_query("insert into " . TABLE_GEO_ZONES . " (geo_zone_name, geo_zone_description, date_added) values ('" . tep_db_input($geo_zone_name) . "', '" . tep_db_input($geo_zone_description) . "', now())");
      $new_zone_id = tep_db_insert_id();

      tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $new_zone_id));
      break;
    case 'save_zone':
      $zID = tep_db_prepare_input($_GET['zID']);
      $geo_zone_name = tep_db_prepare_input($_POST['geo_zone_name']);
      $geo_zone_description = tep_db_prepare_input($_POST['geo_zone_description']);

      tep_db_query("update " . TABLE_GEO_ZONES . " set geo_zone_name = '" . tep_db_input($geo_zone_name) . "', geo_zone_description = '" . tep_db_input($geo_zone_description) . "', last_modified = now() where geo_zone_id = '" . (int)$zID . "'");

      tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID']));
      break;
    case 'deleteconfirm_zone':
      $zID = tep_db_prepare_input($_GET['zID']);

      tep_db_query("delete from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$zID . "'");
      tep_db_query("delete from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . (int)$zID . "'");

      tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage']));
      break;
  }
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-geo-zones" class="table-geo-zones">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <?php
            if ($action == 'list') {
              ?>
              <table class="table table-hover w-100 mt-2">
                <thead>
                  <tr class="th-row">
                    <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_COUNTRY; ?></th>
                    <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_COUNTRY_ZONE; ?></th>
                    <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                  </tr>
                </thead>
                <tbody>               
                  <?php
                  $rows = 0;
                  $zones_query_raw = "select a.association_id, a.zone_country_id, c.countries_name, a.zone_id, a.geo_zone_id, a.last_modified, a.date_added, z.zone_name
                                      from " . TABLE_ZONES_TO_GEO_ZONES . " a
                                      left join " . TABLE_ZONES . " z using(zone_id)
                                      left join " . TABLE_COUNTRIES . " c on a.zone_country_id = c.countries_id
                                      where a.geo_zone_id = " . $_GET['zID'] . "
                                      order by association_id";
                  $zones_split = new splitPageResults($_GET['spage'], MAX_DISPLAY_SEARCH_RESULTS, $zones_query_raw, $zones_query_numrows);
                  $zones_query = tep_db_query($zones_query_raw);
                  while ($zones = tep_db_fetch_array($zones_query)) {
                    $rows++;
                    if ((!isset($_GET['sID']) || (isset($_GET['sID']) && ($_GET['sID'] == $zones['association_id']))) && !isset($sInfo) && (substr($action, 0, 3) != 'new')) {
                      $sInfo = new objectInfo($zones);
                    }
                    $selected = (isset($sInfo) && is_object($sInfo) && ($zones['association_id'] == $sInfo->association_id)) ? ' selected' : '';
                    if (isset($sInfo) && is_object($sInfo) && ($zones['association_id'] == $sInfo->association_id)) {
                      echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=edit') . '\'">' . "\n";
                    } else {
                      echo '<tr class="table-row dark " onclick="document.location.href=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $zones['association_id']) . '\'">' . "\n";
                    }
                    $col_selected = ($selected) ? ' selected' : '';
                    ?>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo (($zones['countries_name']) ? $zones['countries_name'] : TEXT_ALL_COUNTRIES); ?></td>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo (($zones['zone_id']) ? $zones['zone_name'] : PLEASE_SELECT); ?></td>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>">
                      <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $zones['association_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                    </td>                    
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>

              <div class="pagination-container ml-2 mr-2">
                <div class="results-right"><?php echo $zones_split->display_count($zones_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['spage'], TEXT_DISPLAY_NUMBER_OF_COUNTRIES); ?></div>
                <div class="results-left"><?php echo $zones_split->display_links($zones_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['spage'], 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list', 'spage'); ?></div>
              </div>
              <?php
              if (empty($saction)) {
                ?>
                <div class="float-right mr-2 mt-3 mb-3" role="group">
                  <button class="btn btn-success btn-sm" onclick="window.location='<?php echo tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&' . (isset($sInfo) ? 'sID=' . $sInfo->association_id . '&' : '') . 'saction=new'); ?>'"><?php echo IMAGE_INSERT; ?></button> 
                </div>
                <?php 
              } 
            } else {
              ?>
              <table class="table table-hover w-100 mt-2">
                <thead>
                  <tr class="th-row">
                    <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_TAX_ZONES; ?></th>
                    <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                  </tr>
                </thead>
                <tbody> 
                  <?php
                  $zones_query_raw = "select geo_zone_id, geo_zone_name, geo_zone_description, last_modified, date_added from " . TABLE_GEO_ZONES . " order by geo_zone_name";
                  $zones_split = new splitPageResults($_GET['zpage'], MAX_DISPLAY_SEARCH_RESULTS, $zones_query_raw, $zones_query_numrows);
                  $zones_query = tep_db_query($zones_query_raw);
                  while ($zones = tep_db_fetch_array($zones_query)) {
                    if ((!isset($_GET['zID']) || (isset($_GET['zID']) && ($_GET['zID'] == $zones['geo_zone_id']))) && !isset($zInfo) && (substr($action, 0, 3) != 'new')) {
                      $num_zones_query = tep_db_query("select count(*) as num_zones from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . (int)$zones['geo_zone_id'] . "' group by geo_zone_id");
                      $num_zones = tep_db_fetch_array($num_zones_query);

                      if ($num_zones['num_zones'] > 0) {
                        $zones['num_zones'] = $num_zones['num_zones'];
                      } else {
                        $zones['num_zones'] = 0;
                      }

                      $zInfo = new objectInfo($zones);
                    }
                    $selected = (isset($zInfo) && is_object($zInfo) && ($zones['geo_zone_id'] == $zInfo->geo_zone_id)) ? ' selected' : '';
                    if ($selected) {
                      echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=list') . '\'">' . "\n";
                    } else {
                      echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zones['geo_zone_id']) . '\'">' . "\n";
                    }
                    $col_selected = ($selected) ? ' selected' : '';
                    ?>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zones['geo_zone_id'] . '&action=list') . '"><i class="fa fa-folder fa-lg text-warning mr-2"></i></a>&nbsp;' . $zones['geo_zone_name']; ?></td>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>">
                      <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zones['geo_zone_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                    </td>                    
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
              <div class="pagination-container ml-2 mr-2">
                <div class="results-right"><?php echo $zones_split->display_count($zones_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['zpage'], TEXT_DISPLAY_NUMBER_OF_TAX_ZONES); ?></div>
                <div class="results-left"><?php echo $zones_split->display_links($zones_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['zpage'], '', 'zpage'); ?></div>
              </div>
              <?php
              if (empty($action)) {
                ?>
                <div class="float-right mr-2 mt-3 mb-3" role="group">
                  <button class="btn btn-success btn-sm" onclick="window.location='<?php echo tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=new_zone'); ?>'"><?php echo IMAGE_NEW_ZONE; ?></button> 
                </div>
                <?php 
              } 
           }
           ?>
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              if ($action == 'list') {
                switch ($saction) {
                  case 'new':
                    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_NEW_SUB_ZONE . '</b>');

                    $contents[] = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&' . (isset($_GET['sID']) ? 'sID=' . $_GET['sID'] . '&' : '') . 'saction=insert_sub'));
                    $contents[] = array('text' => TEXT_INFO_NEW_SUB_ZONE_INTRO);
                    $contents[] = array('text' => '<br>' . TEXT_INFO_COUNTRY . '<br>' . tep_draw_pull_down_menu('zone_country_id', tep_get_countries(TEXT_ALL_COUNTRIES), '', 'onChange="update_zone(this.form);"'));
                    $contents[] = array('text' => '<br>' . TEXT_INFO_COUNTRY_ZONE . '<br>' . tep_draw_pull_down_menu('zone_id', tep_prepare_country_zones_pull_down()));
                    $contents[] = array('align' => 'center', 'text' => '<div><button class="btn btn-success btn-sm mt-2 mb-2 btn-insert" type="submit">' . IMAGE_INSERT . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&' . (isset($_GET['sID']) ? 'sID=' . $_GET['sID'] : '')) . '">' . IMAGE_CANCEL . '</a></div>');
                    break;
                  case 'edit':
                    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT_SUB_ZONE . '</b>');

                    $contents[] = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=save_sub'));
                    $contents[] = array('text' => TEXT_INFO_EDIT_SUB_ZONE_INTRO);
                    $contents[] = array('text' => '<br>' . TEXT_INFO_COUNTRY . '<br>' . tep_draw_pull_down_menu('zone_country_id', tep_get_countries(TEXT_ALL_COUNTRIES), $sInfo->zone_country_id, 'onChange="update_zone(this.form);"'));
                    $contents[] = array('text' => '<br>' . TEXT_INFO_COUNTRY_ZONE . '<br>' . tep_draw_pull_down_menu('zone_id', tep_prepare_country_zones_pull_down($sInfo->zone_country_id), $sInfo->zone_id));
                    $contents[] = array('align' => 'center', 'text' => '<div><button class="btn btn-success btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id) . '">' . IMAGE_CANCEL . '</a></div>');
                    break;
                  case 'delete':
                    $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_SUB_ZONE . '</b>');

                    $contents[] = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=deleteconfirm_sub'));
                    $contents[] = array('text' => TEXT_INFO_DELETE_SUB_ZONE_INTRO);
                    $contents[] = array('text' => '<br><b>' . $sInfo->countries_name . '</b>');
                    $contents[] = array('align' => 'center', 'text' => '<div><button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_CONFIRM_DELETE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id) . '">' . IMAGE_CANCEL . '</a></div>');
                    break;
                  default:
                    if (isset($sInfo) && is_object($sInfo)) {
                      $heading[] = array('text' => '<b>' . $sInfo->countries_name . '</b>');

                      $contents[] = array('align' => 'center', 'text' => '<div><a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=edit') . '">' . IMAGE_EDIT . '</a><a class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=delete') . '">' . IMAGE_DELETE . '</a>');
                      $contents[] = array('text' => '<br>' . TEXT_INFO_DATE_ADDED . ' <b>' . tep_date_short($sInfo->date_added) . '</b>');
                      if (tep_not_null($sInfo->last_modified)) $contents[] = array('text' => TEXT_INFO_LAST_MODIFIED . ' <b>' . tep_date_short($sInfo->last_modified) . '</b>');
                    }
                    break;
                }
              } else {
                switch ($action) {
                  case 'new_zone':
                    $heading[] = array('text' => TEXT_INFO_HEADING_NEW_ZONE);
                    $contents[] = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=insert_zone','post','data-parsley-validate'));
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_NEW_ZONE_INTRO . '</p></div></div></div>');                                           
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_ZONE_NAME . '<span class="sidebar-title ml-2">' . tep_draw_input_field('geo_zone_name', null, 'class="form-control" required') . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_ZONE_DESCRIPTION . '<span class="sidebar-title ml-2">' . tep_draw_input_field('geo_zone_description', null, 'class="form-control"') . '</span></div>');
                    $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-insert" type="submit">' . IMAGE_INSERT . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage']) . '\'">' . IMAGE_CANCEL . '</button>');
                    break;
                  case 'edit_zone':
                    $heading[] = array('text' => TEXT_INFO_HEADING_EDIT_ZONE);
                    $contents[] = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=save_zone','post','data-parsley-validate'));
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_EDIT_ZONE_INTRO . '</p></div></div></div>');                                           
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_ZONE_NAME . '<span class="sidebar-title ml-2">' . tep_draw_input_field('geo_zone_name', $zInfo->geo_zone_name, 'class="form-control" required') . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_ZONE_DESCRIPTION . '<span class="sidebar-title ml-2">' . tep_draw_input_field('geo_zone_description', $zInfo->geo_zone_description, 'class="form-control"') . '</span></div>');
                    $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-1 mb-1 btn-update" type="submit">' . IMAGE_UPDATE . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id) . '\'">' . IMAGE_CANCEL . '</button>');
                    break;
                  case 'delete_zone':
                    $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_ZONE);
                    $contents[] = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=deleteconfirm_zone'));
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . sprintf(TEXT_INFO_DELETE_ZONE_INTRO, '<b>' . $zInfo->geo_zone_name . '</b>') . '</p></div></div></div>');                                           
                    $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_CONFIRM_DELETE . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id) . '\'">' . IMAGE_CANCEL . '</button>');
                    break;
                  default:
                    if (isset($zInfo) && is_object($zInfo)) {
                      $heading[] = array('text' => '<div class="text-truncate">' . $zInfo->geo_zone_name . '</div>');
                      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
                                      <button class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=edit_zone')  . '\'">' . IMAGE_EDIT . '</button>
                                      <button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=delete_zone') . '\'">' . IMAGE_DELETE . '</button>');
                      $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_NUMBER_ZONES . '<span class="sidebar-title ml-2 f-w-600">' . $zInfo->num_zones . '</span></div>');
                      $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_DATE_ADDED . '<span class="sidebar-title ml-2 f-w-600">' . tep_date_short($zInfo->date_added) . '</span></div>');
                      if (tep_not_null($zInfo->last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_LAST_MODIFIED . '<span class="sidebar-title ml-2 f-w-600">' . tep_date_short($zInfo->last_modified) . '</span></div>');
                      $contents[] = array('text' => '<div class="sidebar-text mt-1 mb-3">' . TEXT_INFO_ZONE_DESCRIPTION . '<span class="sidebar-title ml-2 f-w-600">' . $zInfo->geo_zone_description . '</span></div>');
                    }
                    break;
                }
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<?php
if (isset($_GET['zID']) && (($saction == 'edit') || ($saction == 'new'))) {
  ?>  
  <script>
  function resetZoneSelected(theForm) {
    if (theForm.state.value != '') {
      theForm.zone_id.selectedIndex = '0';
      if (theForm.zone_id.options.length > 0) {
        theForm.state.value = '<?php echo JS_STATE_SELECT; ?>';
      }
    }
  }

  function update_zone(theForm) {
    var NumState = theForm.zone_id.options.length;
    var SelectedCountry = "";

    while(NumState > 0) {
      NumState--;
      theForm.zone_id.options[NumState] = null;
    }

    SelectedCountry = theForm.zone_country_id.options[theForm.zone_country_id.selectedIndex].value;

    <?php echo tep_js_zone_list('SelectedCountry', 'theForm', 'zone_id'); ?>
  }
  </script>
  <?php
}
?>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>