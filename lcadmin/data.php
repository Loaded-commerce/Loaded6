<?php
/*
  $Id: data.php,v 2.0.0.0 2008/05/13 13:41:11 wa4u Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License

*/
require('includes/application_top.php');

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
 
  <div class="col">   
    <?php
    if ($messageStack->size('easypopulate_export') > 0) {
      echo $messageStack->output('easypopulate_export'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-data" class="table-data">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">


			<div class="main-heading"><span><?php echo WELCOME_TO_DATA_EXPORT_IMPORT_SYSTEM; ?></span>
			  <div class="main-heading-footer"></div>
			</div>             

			<table border="0" width="100%" cellspacing="0" cellpadding="0" class="data-table">
			  <?php
			  // RCI start
			  echo $cre_RCI->get('data', 'listingtop');
			  // RCI eof
			  ?>     
			  <tr>
				<td valign="top"><table width="70%" border="0" cellspacing="2" cellpadding="2">
				  <tr>
					<td width="50%" valign="top" class="sidebar-title"><p style="font-weight:bold;"><?php echo TEXT_HELP_EASY_POPULATE;?></p>
					  <ul>                                                                                                                       
						<li><?php echo '<a href="' . tep_href_link(FILENAME_DATA_HELP, 'help_id=1') . '">' . TEXT_EP_INTRO . '</a>'; ?></li>
						<li><?php echo '<a href="' . tep_href_link(FILENAME_DATA_HELP, 'help_id=2') . '">' . TEXT_EP_ADV_IMPORT . '</a>'; ?></li>
						<li><?php echo '<a href="' . tep_href_link(FILENAME_DATA_HELP, 'help_id=3') . '">' . TEXT_EP_ADV_EXPORT . '</a>'; ?></li>
						<li><?php echo '<a href="' . tep_href_link(FILENAME_DATA_HELP, 'help_id=4') . '">' . TEXT_EP_BASIC_IMPORT . '</a>'; ?></li>
						<li><?php echo '<a href="' . tep_href_link(FILENAME_DATA_HELP, 'help_id=5') . '">' . TEXT_EP_BASIC_EXPORT . '</a>'; ?></li>
						<li><?php echo '<a href="' . tep_href_link(FILENAME_DATA_HELP, 'help_id=6') . '">' . TEXT_EP_EDITING_FILE . '</a>'; ?></li>
					</ul></td>
				  </tr>
				</table>
				</td>
			  </tr>
			</table>
    


		 </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>