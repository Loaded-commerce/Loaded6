<?php
/*
  $Id: order_status.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'insert':
    case 'save':
      if (isset($_GET['oID'])) $orders_status_id = tep_db_prepare_input($_GET['oID']);

      $languages = tep_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $orders_status_name_array = $_POST['orders_status_name'];
        $language_id = $languages[$i]['id'];

        $sql_data_array = array('orders_status_name' => tep_db_prepare_input($orders_status_name_array[$language_id]));

        if ($action == 'insert') {
          if (empty($orders_status_id)) {
            $next_id_query = tep_db_query("select max(orders_status_id) as orders_status_id from " . TABLE_ORDERS_STATUS . "");
            $next_id = tep_db_fetch_array($next_id_query);
            $orders_status_id = $next_id['orders_status_id'] + 1;
          }

          $insert_sql_data = array('orders_status_id' => $orders_status_id,
                                   'language_id' => $language_id);

          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

          tep_db_perform(TABLE_ORDERS_STATUS, $sql_data_array);
        } elseif ($action == 'save') {
          tep_db_perform(TABLE_ORDERS_STATUS, $sql_data_array, 'update', "orders_status_id = '" . (int)$orders_status_id . "' and language_id = '" . (int)$language_id . "'");
        }
      }

      if (isset($_POST['default']) && ($_POST['default'] == 'on')) {
        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($orders_status_id) . "' where configuration_key = 'DEFAULT_ORDERS_STATUS_ID'");
      }

      tep_redirect(tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $orders_status_id));
      break;
    case 'deleteconfirm':
      $oID = tep_db_prepare_input($_GET['oID']);

      $orders_status_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'DEFAULT_ORDERS_STATUS_ID'");
      $orders_status = tep_db_fetch_array($orders_status_query);

      if ($orders_status['configuration_value'] == $oID) {
        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '' where configuration_key = 'DEFAULT_ORDERS_STATUS_ID'");
      }

      tep_db_query("delete from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . tep_db_input($oID) . "'");

      tep_redirect(tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page']));
      break;
    case 'delete':
      $oID = tep_db_prepare_input($_GET['oID']);

      $status_query = tep_db_query("select count(*) as count from " . TABLE_ORDERS . " where orders_status = '" . (int)$oID . "'");
      $status = tep_db_fetch_array($status_query);

      $remove_status = true;
      if ($oID == DEFAULT_ORDERS_STATUS_ID) {
        $remove_status = false;
        $messageStack->add('orders_status', ERROR_REMOVE_DEFAULT_ORDER_STATUS, 'error');
      } elseif ($status['count'] > 0) {
        $remove_status = false;
        $messageStack->add('orders_status', ERROR_STATUS_USED_IN_ORDERS, 'error');
      } else {
        $history_query = tep_db_query("select count(*) as count from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_status_id = '" . (int)$oID . "'");
        $history = tep_db_fetch_array($history_query);
        if ($history['count'] > 0) {
          $remove_status = false;
          $messageStack->add('orders_status', ERROR_STATUS_USED_IN_HISTORY, 'error');
        }
      }
      break;
  }
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-order-status" class="table-order-status">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_ORDERS_STATUS; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody> 
              <?php
              $orders_status_query_raw = "select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' order by orders_status_id";
              $orders_status_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $orders_status_query_raw, $orders_status_query_numrows);
              $orders_status_query = tep_db_query($orders_status_query_raw);
              while ($orders_status = tep_db_fetch_array($orders_status_query)) {
                if ((!isset($_GET['oID']) || (isset($_GET['oID']) && ($_GET['oID'] == $orders_status['orders_status_id']))) && !isset($oInfo) && (substr($action, 0, 3) != 'new')) {
                  $oInfo = new objectInfo($orders_status);
                }

                $selected = (isset($oInfo) && is_object($oInfo) && ($orders_status['orders_status_id'] == $oInfo->orders_status_id)) ? ' selected' : '';
                $col_selected = ($selected) ? ' selected' : '';

                if ($selected) {
                  echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id . '&action=edit') . '\'">' . "\n";
                } else {
                  echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $orders_status['orders_status_id']) . '\'">' . "\n";
                }

                if (DEFAULT_ORDERS_STATUS_ID == $orders_status['orders_status_id']) {
                  echo '<td class="table-col dark text-left' . $col_selected . '">' . $orders_status['orders_status_name'] . ' (' . TEXT_DEFAULT . ')</td>' . "\n";
                } else {
                  echo '<td class="table-col dark text-left' . $col_selected . '">' . $orders_status['orders_status_name'] . '</td>' . "\n";
                }

                ?>
                <td class="table-col dark text-right<?php echo $col_selected; ?>">
                  <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $orders_status['orders_status_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                </td>
                </tr>
                <?php
              }
              ?>
              </tbody> 
            </table>

            <div class="pagination-container ml-2 mr-2">
              <div class="results-right"><?php echo $orders_status_split->display_count($orders_status_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS_STATUS); ?></div>
              <div class="results-left"><?php echo  $orders_status_split->display_links($orders_status_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></div>
            </div>

            <div class="float-right mr-2 mt-0 mb-3" role="group">
              <button class="btn btn-success btn-sm" onclick="window.location='<?php echo tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&action=new'); ?>'"><?php echo IMAGE_NEW_ORDER_STATUS; ?></button> 
            </div>            

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();
              // right sidebar content goes here
              switch ($action) {
                case 'new':
                  $heading[] = array('text' => TEXT_INFO_HEADING_NEW_ORDERS_STATUS);
                  $contents[] = array('form' => tep_draw_form('status', FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&action=insert','POST','onSubmit="return func_form_validate();" '));
                  $contents[] = array('text' =>  '<div class="sidebar-text mt-3 mb-3">' . TEXT_INFO_INSERT_INTRO . '</div>');

                  $orders_status_inputs_string = '';
                  $languages = tep_get_languages();
                  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                    $orders_status_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' . 
                      tep_draw_input_field('orders_status_name[' . $languages[$i]['id'] . ']', tep_get_orders_status_name($oInfo->orders_status_id, $languages[$i]['id']), 'class="form-control"') . '</div>'; 
                  }
                  $contents[] = array('text' => $orders_status_inputs_string);
                  $contents[] = array('text' => '<div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text">' . TEXT_SET_DEFAULT . '</label><input type="checkbox" name="default" class="js-switch js-default"></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE . '</button><button type="button" class="btn btn-grey btn-sm mr-2 mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page']) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'edit':
                  $heading[] = array('text' => TEXT_INFO_HEADING_EDIT_ORDERS_STATUS);
                  $contents[] = array('form' => tep_draw_form('status', FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id  . '&action=save','POST','onSubmit="return func_form_validate();" '));
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_EDIT_INTRO . '</div>');

                  $orders_status_inputs_string = '';
                  $languages = tep_get_languages();
                  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                    $orders_status_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' . 
                      tep_draw_input_field('orders_status_name[' . $languages[$i]['id'] . ']', tep_get_orders_status_name($oInfo->orders_status_id, $languages[$i]['id']), 'class="form-control"') . '</div>'; 
                  }

                  $contents[] = array('text' => '<label class="control-label sidebar-title mt-2 mb-1">' . TEXT_INFO_ORDERS_STATUS_NAME . '</label>');
                  $contents[] = array('text' => $orders_status_inputs_string);
                  if (DEFAULT_ORDERS_STATUS_ID != $oInfo->orders_status_id) {
                    $contents[] = array('text' => '<div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 mb-3 main-text">' . TEXT_SET_DEFAULT . '</label><input type="checkbox" name="default" class="js-switch js-default"></div>');
                  }
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><button type="button" class="btn btn-grey btn-sm mr-2 mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'delete':
                  $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_ORDERS_STATUS);
                  $contents[] = array('form' => tep_draw_form('status', FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id  . '&action=deleteconfirm'));

                  if ($messageStack->size('orders_status') > 0) {
                    $contents[] = array('text' => '<div class="mt-2 ml-2 mr-2 f-w-400">' . $messageStack->output('orders_status') . '</div>');                    
                    $contents[] = array('align' => 'center', 'text' => '<button type="button" class="btn btn-grey btn-sm mr-2 mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  } else {
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . sprintf(TEXT_INFO_DELETE_INTRO, $oInfo->orders_status_name) . '</p></div></div></div>');                                           
                    $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-danger btn-sm mt-2 mb-2 btn-deleteconfirm" type="submit">' . IMAGE_CONFIRM_DELETE . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  }
                  break;
                default:
                  if (isset($oInfo) && is_object($oInfo)) {
                    $heading[] = array('text' => $oInfo->orders_status_name);
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
                                    <button class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id . '&action=edit')  . '\'">' . IMAGE_EDIT . '</button>
                                    <button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_ORDERS_STATUS, 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id . '&action=delete')  . '\'">' . IMAGE_DELETE . '</button>');

                    $orders_status_inputs_string = '';
                    $languages = tep_get_languages();
                    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                      $orders_status_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' . 
                      tep_draw_input_field('orders_status_name[' . $languages[$i]['id'] . ']', tep_get_orders_status_name($oInfo->orders_status_id, $languages[$i]['id']), 'class="form-control" readonly') . '</div>'; 
                    }
                    $contents[] = array('text' => '<label class="control-label sidebar-title mt-2 mb-1">' . TEXT_INFO_ORDERS_STATUS_NAME . '</label>');
                    $contents[] = array('text' => $orders_status_inputs_string);
                  }
                  break;
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function(){
  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small', 
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  });
});
</script>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>