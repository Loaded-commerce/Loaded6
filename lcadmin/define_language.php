<?php
/*
  $Id: define_languages.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

if (!isset($_GET['lngdir'])) $_GET['lngdir'] = $language;

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'save':
      if (isset($_GET['lngdir']) && isset($_GET['filename'])) {
        if ($_GET['filename'] == $_GET['lngdir'] . '.php') {
          $file = DIR_FS_CATALOG_LANGUAGES . $_GET['filename'];
        } else {
          $file = DIR_FS_CATALOG_LANGUAGES . $_GET['lngdir'] . '/' . $_GET['filename'];
        }

        if (file_exists($file)) {
          if (file_exists('bak' . $file)) {
            @unlink('bak' . $file);
          }

          @rename($file, 'bak' . $file);

          $new_file = fopen($file, 'w');
          $file_contents = stripslashes($_POST['file_contents']);
          //$file_contents = $_POST['file_contents'];
          fwrite($new_file, $file_contents, strlen($file_contents));
          fclose($new_file);
        }
        tep_redirect(tep_href_link(FILENAME_DEFINE_LANGUAGE, 'lngdir=' . $_GET['lngdir']));
      }
      break;
  }
}

$languages_array = array();
$languages = tep_get_languages();
$lng_exists = false;
for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
  if ($languages[$i]['directory'] == $_GET['lngdir']) $lng_exists = true;

  $languages_array1[] = array('id' => $languages[$i]['directory'],
                             'text' => $languages[$i]['name']);
}

if (!$lng_exists) $_GET['lngdir'] = $language;

if (isset($_GET['lngdir']) && isset($_GET['filename'])) {
  if ($_GET['filename'] == $_GET['lngdir'] . '.php') {
    $file = DIR_FS_CATALOG_LANGUAGES . $_GET['filename'];
  } else {
    $file = DIR_FS_CATALOG_LANGUAGES . $_GET['lngdir'] . '/' . $_GET['filename'];
  }

  if (file_exists($file)) {

    $file_writeable = true;
    if (!is_writeable($file)) {
      $file_writeable = false;
      $messageStack->reset();
      $messageStack->add('define_language', sprintf(ERROR_FILE_NOT_WRITEABLE, $file), 'error');
    }
  }
}
include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<style>
textarea {
  width:100%;
}
</style>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php 
  if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php');
  
  if ($messageStack->size('define_language') > 0) {
    echo '<div class="ml-3 mr-3">' . $messageStack->output('define_language') . '</div>';
  }
  ?>
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-languages" class="table-languages">
        <div class="row">
          <div class="col-12 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left">
                    <div id="lng-select" class="form-group row mb-1 mt-0">
                      <?php echo tep_draw_form('lng', FILENAME_DEFINE_LANGUAGE, '', 'get'); ?>
                      <div class=""><?php echo tep_draw_pull_down_menu('lngdir', $languages_array1, $_GET['lngdir'], 'onChange="this.form.submit();" class="form-control"'); ?></div>
                    </div>
                  </th>
                  <th scope="col" class="th-col dark text-left">
                    <div class="btn-grp text-right m-r-10 m-t-5">
                      <?php
                      if (isset($_GET['filename'])) {
                        if (file_exists($file) && $file_writeable == true) { 
                          echo '<button class="btn btn-default btn-sm mr-2" onclick="window.location=\'' . tep_href_link(FILENAME_DEFINE_LANGUAGE, 'lngdir=' . $_GET['lngdir']) . '\'">' . IMAGE_CANCEL . '</button><button class="btn btn-success btn-sm" type="submit">' . IMAGE_SAVE . '</button>';
                        } else { 
                          echo '<button class="btn btn-default btn-sm" onclick="window.location=\'' . tep_href_link(FILENAME_DEFINE_LANGUAGE, 'lngdir=' . $_GET['lngdir']) . '\'">' . IMAGE_CANCEL . '</button>';
                        } 
                      }
                      ?>
                    </div>                    
                  </th>
                </tr>
              </thead>
              <tbody>  
                <?php
                if (isset($_GET['lngdir']) && isset($_GET['filename'])) {
                  if (file_exists($file)) {
                    $file_array = file($file);
                    $contents = implode('', $file_array);
                    ?>
                    <tr class="table-row dark"><?php echo tep_draw_form('language', FILENAME_DEFINE_LANGUAGE, 'lngdir=' . $_GET['lngdir'] . '&filename=' . $_GET['filename'] . '&action=save'); ?>
                      <td class="table-col dark text-left" colspan="2"><?php echo tep_draw_textarea_field('file_contents', 'soft', '80', '20', $contents, (($file_writeable) ? 'class="form-control"' : 'class="w-100 form-control" readonly')); ?></td>
                    </form></tr>
                    <?php
                  } else {
                    ?>
                    <tr class="table-row dark">
                      <td class="table-col dark text-left"><b><?php echo TEXT_FILE_DOES_NOT_EXIST; ?></b></td>
                    </tr>
                    <?php
                  }
                } else {
                  $filename = $_GET['lngdir'] . '.php';
                  ?>
                  <tr class="table-row dark">
                    <td class="table-col dark text-left"><a href="<?php echo tep_href_link(FILENAME_DEFINE_LANGUAGE, 'lngdir=' . $_GET['lngdir'] . '&filename=' . $filename); ?>"><b><?php echo $filename; ?></b></a></td>
                    <?php
                    $left = false;
                    if ($dir = dir(DIR_FS_CATALOG_LANGUAGES . $_GET['lngdir'])) {
                      $file_extension = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], '.'));
                      while ($file = $dir->read()) {
                        if (substr($file, strrpos($file, '.')) == $file_extension) {
                          echo '<td class="table-col dark text-left"><a href="' . tep_href_link(FILENAME_DEFINE_LANGUAGE, 'lngdir=' . $_GET['lngdir'] . '&filename=' . $file) . '">' . $file . '</a></td>' . "\n";
                          if (!$left) {
                            echo '</tr>' . "\n" .
                                 '<tr class="table-row dark">' . "\n";
                          }
                          $left = !$left;
                        }
                      }
                      $dir->close();
                      if (!$left) echo '<td class="table-col dark text-left">&nbsp;</td>';
                    }
                    ?>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
            <div class="btn-grp text-right m-r-20 m-b-20">
              <?php
              if (isset($_GET['filename'])) {
                if (file_exists($file) && $file_writeable == true) { 
                  echo '<button class="btn btn-default btn-sm mr-2" onclick="window.location=\'' . tep_href_link(FILENAME_DEFINE_LANGUAGE, 'lngdir=' . $_GET['lngdir']) . '\'">' . IMAGE_CANCEL . '</button><button class="btn btn-success btn-sm" type="submit">' . IMAGE_SAVE . '</button>';
                } else { 
                  echo '<button class="btn btn-default btn-sm" onclick="window.location=\'' . tep_href_link(FILENAME_DEFINE_LANGUAGE, 'lngdir=' . $_GET['lngdir']) . '\'">' . IMAGE_CANCEL . '</button>';
                } 
              }
              ?>
            </div>
          </div>

        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>