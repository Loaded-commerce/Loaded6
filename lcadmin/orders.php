<?php
/*
  $Id: orders.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
require(DIR_WS_CLASSES . 'order.php');

$currencies = new currencies();

// RCI code start
echo $cre_RCI->get('global', 'top', false);
echo $cre_RCI->get('orders', 'top', false);
// RCI code eof

$orders_statuses = array();
$orders_status_array = array();
$orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "'");
while ($orders_status = tep_db_fetch_array($orders_status_query)) {
  $orders_statuses[] = array('id' => $orders_status['orders_status_id'],
                             'text' => $orders_status['orders_status_name']);
  $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
}

$action = (isset($_GET['action']) ? $_GET['action'] : '');
$_SESSION['order_status'] = $_GET['status'];
//echo $_SERVER['QUERY_STRING'];
if (tep_not_null($action)) {
  switch ($action) {

    case 'accept_order':
      include(DIR_FS_CATALOG_MODULES.'payment/paypal/admin/AcceptOrder.inc.php');
      break;
    case 'update_order':
      $oID = tep_db_prepare_input($_POST['oID']);
      $status = tep_db_prepare_input($_POST['status']);
      $comments = tep_db_prepare_input($_POST['comments']);

      $order_updated = false;
      $check_status_query = tep_db_query("SELECT customers_name, customers_email_address, orders_status, date_purchased
                                          FROM " . TABLE_ORDERS . "
                                          WHERE orders_id = " . (int)$oID);
      $check_status = tep_db_fetch_array($check_status_query);

      // always update date and time on order_status
      //check to see if can download status change
      if ( ($check_status['orders_status'] != $status) || tep_not_null($comments) || ($status == DOWNLOADS_ORDERS_STATUS_UPDATED_VALUE) ) {
        // RCI update order
        echo $cre_RCI->get('orders', 'updateorder', false);
        tep_db_query("update " . TABLE_ORDERS . " set orders_status = '" . tep_db_input($status) . "', last_modified = now() where orders_id = '" . (int)$oID . "'");
        if ( $status == DOWNLOADS_ORDERS_STATUS_UPDATED_VALUE ) {
          tep_db_query("update " . TABLE_ORDERS_PRODUCTS_DOWNLOAD . " set download_maxdays = '" . tep_get_configuration_key_value('DOWNLOAD_MAX_DAYS') . "', download_count = '" . tep_get_configuration_key_value('DOWNLOAD_MAX_COUNT') . "' where orders_id = '" . (int)$oID . "'");
        }

        $customer_notified = '0';
        if (isset($_POST['notify']) && ($_POST['notify'] == 'on')) {
          $notify_comments = '';

          if (isset($_POST['notify_comments']) && ($_POST['notify_comments'] == 'on')) {
            $notify_comments = sprintf(EMAIL_TEXT_COMMENTS_UPDATE, $comments) . "\n\n";
          }

          $email = STORE_NAME . "\n" . EMAIL_SEPARATOR . "\n" . EMAIL_TEXT_ORDER_NUMBER . ' ' . $oID . "\n" . EMAIL_TEXT_INVOICE_URL . ' ' .
            tep_catalog_href_link(FILENAME_CATALOG_ACCOUNT_HISTORY_INFO, 'order_id=' . $oID, 'SSL') . "\n" . EMAIL_TEXT_DATE_ORDERED . ' ' . tep_date_long($check_status['date_purchased']) . "\n\n" . $notify_comments . sprintf(EMAIL_TEXT_STATUS_UPDATE, $orders_status_array[$status]);
          tep_mail($check_status['customers_name'], $check_status['customers_email_address'], EMAIL_TEXT_SUBJECT, $email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
          $customer_notified = '1';
        }

		$sql_data_oh_array = array('orders_id' => $oID,
							  'admin_id' => $_SESSION['login_id'],
							  'orders_status_id' => tep_db_input($status),
							  'date_added' => 'now()',
							  'comments' => tep_db_input($comments),
							  'customer_notified' => tep_db_input($customer_notified));
		tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_oh_array);

        $order_updated = true;
      }

      if ($order_updated == true) {
        $messageStack->add_session('search', SUCCESS_ORDER_UPDATED, 'success');
      } else {
        $messageStack->add_session('search', WARNING_ORDER_NOT_UPDATED, 'warning');
      }

      tep_redirect(tep_href_link(FILENAME_ORDERS, 'page=' . $_GET['page'] . '&oID=' . $_GET['oID'] . '&action=edit', 'SSL'));
      break;
    case 'deleteconfirm':
      $oID = tep_db_prepare_input($_GET['oID']);
      tep_remove_order($oID, $_POST['restock']);

      tep_redirect(tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')), 'SSL'));
      break;
    default :
      // RCI extend switch
      echo $cre_RCI->get('orders', 'actionswitch');
      break;
  }
}

// enhanced search
$order_exists = false;
if (($action == 'edit') && isset($_GET['SoID'])) {
  if (is_numeric($_GET['SoID'])) {  // this must be an order id, so use the old format
    $_GET['oID'] = $_GET['SoID'];
    unset($_GET['SoID']);
  }
  // see if there are any matches
  $SoID = tep_db_input(tep_db_prepare_input($_GET['SoID']));

  $sql = "SELECT orders_id
          FROM " . TABLE_ORDERS . "
          WHERE customers_name LIKE '%" . $SoID . "%'
             OR LOWER( customers_email_address ) LIKE '%" . $SoID . "%'
             OR customers_company LIKE '%" . $SoID . "%'";
  $orders_query = tep_db_query($sql);
  $row_count = tep_db_num_rows($orders_query);
  if ($row_count < 1) {
    unset($_GET['SoID']);
    $messageStack->add('search', sprintf(ERROR_ORDER_DOES_NOT_EXIST, $SoID), 'error');
  } elseif ($row_count == 1) {
    // special case, only one, so go direct to edit
    $orders = tep_db_fetch_array($orders_query);
    $_GET['oID'] = $orders['orders_id'];
    $order_exists = true;
    unset($_GET['SoID']);
  } // if greater than 1, list all the matches
}

if (($action == 'edit') && isset($_GET['oID']) && $order_exists === false) {
  $oID = tep_db_prepare_input($_GET['oID']);

  $orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
  if (tep_db_num_rows($orders_query) > 0) {
    $order_exists = true;
  } else {
    unset($_GET['oID']);
    $messageStack->add('search', sprintf(ERROR_ORDER_DOES_NOT_EXIST, $oID), 'error');
  }
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
	  <div class="row">
		<div class="col-9"></div>
		<div class="col-3 pr-0">
		  <?php echo tep_draw_form('orders', FILENAME_ORDERS, '', 'get', '', 'SSL');?>
		   <div class="form-group row mb-2 pr-0">
			<label for="cPath" class="hidden-xs col-sm-3 col-form-label text-center m-t-10 pr-0"><?php echo HEADING_TITLE_STATUS; ?></label>
			<div class="col-sm-9 p-0 dark rounded">
			  <?php echo tep_draw_pull_down_menu('status', array_merge(array(array('id' => '', 'text' => TEXT_ALL_ORDERS)), $orders_statuses), (isset($_GET['status']) ? (int)$_GET['status']: ''), 'onChange="this.form.submit();"'); ?>
			</div>
		  </div>
		  <?php
		  if (isset($_GET[tep_session_name()])) {
			echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
		  }
		  echo '</form></div>';
		  ?>
		</div>
	  </div>

    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-languages" class="table-languages">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <?php
            // RCI start
            echo $cre_RCI->get('orders', 'listingtop');
            // RCI eof

            $oscid = '&' . tep_session_name() . '=' . $_GET[tep_session_name()];
            if (isset($_GET['SoID'])) {
              $oscid .= '&SoID=' . $_GET['SoID'];
            }
            $_SESSION['back_url'] = $_SERVER['QUERY_STRING'];

            $HEADING_CUSTOMERS = TABLE_HEADING_CUSTOMER;
            $HEADING_CUSTOMERS .= '<a href="' . tep_href_link(basename($PHP_SELF), 'sort=customer&order=ascending') . '">';
            $HEADING_CUSTOMERS .= '<span class="fa fa-toggle-up ml-1 text-success"></span>';
            $HEADING_CUSTOMERS .= '<a href="' . tep_href_link(basename($PHP_SELF), 'sort=customer&order=decending') . '">';
            $HEADING_CUSTOMERS .= '<span class="fa fa-toggle-down ml-1 text-success"></span>';
            $HEADING_DATE_PURCHASED = TABLE_HEADING_DATE_PURCHASED;
            $HEADING_DATE_PURCHASED .= '<a href="' . tep_href_link(basename($PHP_SELF), 'sort=date&order=ascending') . '">';
            $HEADING_DATE_PURCHASED .= '<span class="fa fa-toggle-up ml-1 text-success"></span>';
            $HEADING_DATE_PURCHASED .= '<a href="' . tep_href_link(basename($PHP_SELF), 'sort=date&order=decending') . '">';
            $HEADING_DATE_PURCHASED .= '<span class="fa fa-toggle-down ml-1 text-success"></span>';
            ?>

            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_ORDERID; ?></th>
                  <th scope="col" class="th-col dark text-left"><?php echo $HEADING_CUSTOMERS; ?></th>
                  <?php
                  echo $cre_RCI->get('orders', 'headingcol', false);
                  ?>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ORDER_TOTAL; ?></th>
                  <th scope="col" class="th-col dark text-center"><?php echo $HEADING_DATE_PURCHASED; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_PAYMENT_METHOD; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_STATUS; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $sortorder = 'order by ';
                $sort = (isset($_GET["sort"]) ? $_GET["sort"] : '');
                if  ($sort == 'customer') {
                  if ($_GET["order"] == 'ascending') {
                    $sortorder .= 'o.customers_name  asc, ';
                  } else {
                    $sortorder .= 'o.customers_name desc, ';
                  }
                } elseif ($sort == 'date') {
                  if ($_GET["order"] == 'ascending') {
                    $sortorder .= 'o.date_purchased  asc, ';
                  } else {
                    $sortorder .= 'o.date_purchased desc, ';
                  }
                }
                $sortorder .= 'o.orders_id DESC';
                if (isset($_GET['cID'])) {
                  $cID = tep_db_prepare_input($_GET['cID']);
                  $orders_query_raw = "select o.orders_id, o.customers_name, o.customers_id, o.payment_method, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name, ot.text as order_total from " . TABLE_ORDERS . " o, " . TABLE_ORDERS_TOTAL . " ot, " . TABLE_ORDERS_STATUS . " s where o.customers_id = '" . (int)$cID . "' and ot.orders_id = o.orders_id and o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' and ot.class = 'ot_total' order by orders_id DESC";
                } elseif ((isset($_GET['status']) && (tep_not_null($_GET['status']))) || (isset($_SESSION['order_status']) && $_SESSION['order_status'] != '')) {
                  $status = tep_db_prepare_input($_GET['status']);
                  $orders_query_raw = "select o.orders_id, o.customers_name, o.payment_method, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name from " . TABLE_ORDERS . " o, " . TABLE_ORDERS_STATUS . " s where o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' and s.orders_status_id = '" . (int)$status . "' order by o.orders_id DESC";
                } elseif (isset($_GET['SoID'])) {
                  $SoID = tep_db_input(tep_db_prepare_input($_GET['SoID']));
                  $orders_query_raw = "SELECT o.purchased_without_account, o.orders_id, o.customers_name, o.customers_id, o.payment_method,o.payment_module_name, o.date_purchased,
                                              o.last_modified, o.currency, o.currency_value, s.orders_status_name
                                       FROM " . TABLE_ORDERS . " o,
                                            " . TABLE_ORDERS_STATUS . " s
                                       WHERE o.orders_status = s.orders_status_id
                                         AND s.language_id = " . (int)$languages_id . "
                                         AND (o.customers_name LIKE '%" . $SoID . "%'
                                              OR LOWER( o.customers_email_address ) LIKE '%" . $SoID . "%'
                                              OR o.customers_company LIKE '%" . $SoID . "%'
                                              OR o.orders_id LIKE '%" . $SoID . "%'
                                             ) " . $sortorder;
                } else {
                  $orders_query_raw = "SELECT o.purchased_without_account, o.orders_id, o.customers_name, o.customers_id, o.payment_method,o.payment_module_name, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name
                                       FROM " . TABLE_ORDERS . " o,
                                            " . TABLE_ORDERS_STATUS . " s
                                       WHERE o.orders_status = s.orders_status_id
                                         AND s.language_id = " . (int)$languages_id . "
                                       " . $sortorder;
                }
                //echo $orders_query_raw;
                $orders_split = new splitPageResults($_GET['page'], MAX_DISPLAY_ADMIN_RESULTS, $orders_query_raw, $orders_query_numrows);
                $orders_query = tep_db_query($orders_query_raw);
                while ($orders = tep_db_fetch_array($orders_query)) {
                  unset($order_total1);
                  $order_total1 = TEXT_INFO_ABANDONDED;
                  $orders_total_query_raw = "select ot.text as order_total from " . TABLE_ORDERS_TOTAL . " ot where  ot.orders_id = '" . $orders['orders_id'] . "' and ot.class = 'ot_total' ";
                  $orders_query_total = tep_db_query($orders_total_query_raw);
                  while ($orders1 = tep_db_fetch_array($orders_query_total)) {
                    $order_total1 = $orders1['order_total'];
                    if (!$order_total1){
                      $order_total1 = TEXT_INFO_ABANDONDED;
                    }
                  }

                  if ((!isset($_GET['oID']) || (isset($_GET['oID']) && ($_GET['oID'] == $orders['orders_id']))) && !isset($oInfo)) {
                    $oInfo = new objectInfo($orders);
                  }
                  // RCO start
                  if ($cre_RCO->get('orders', 'listingselect') !== true) {
                    $selected = (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)) ? ' selected' : '';
                    if ($selected) {
                      echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id . '&action=edit', 'SSL') . '\'">' . "\n";
                    } else {
                      echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID')) . 'oID=' . $orders['orders_id'], 'SSL') . '\'">' . "\n";
                    }
                    $col_selected = ($selected) ? ' selected' : '';
                    ?>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>" nowrap="nowrap">
                      <?php
                      echo '<b>' . $orders['orders_id'] . '</b>';
                      $products = "";
                      $products_query = tep_db_query("SELECT orders_products_id, products_name, products_quantity
                                                      from " . TABLE_ORDERS_PRODUCTS . "
                                                      WHERE orders_id = '" . tep_db_input($orders['orders_id']) . "' ");
                      while ($products_rows = tep_db_fetch_array($products_query)) {
                        $products .= ($products_rows["products_quantity"]) . "x " . (tep_html_noquote($products_rows["products_name"])) . "\n";
                        $result_attributes = tep_db_query("SELECT products_options, products_options_values
                                                           from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . "
                                                           WHERE orders_id = '" . tep_db_input($orders['orders_id']). "'
                                                             and orders_products_id = '" . $products_rows["orders_products_id"] . "'
                                                           ORDER BY products_options");
                        while ($row_attributes = tep_db_fetch_array($result_attributes)) {
                          $products .= " - " . (tep_html_noquote($row_attributes["products_options"])) . ": " . (tep_html_noquote($row_attributes["products_options_values"])) . "\n";
                        }
                      }
                      if ($products == '') $products = TEXT_NO_PRODUCTS."\n";
                      ?>
                      <i class="fa fa-comment text-info" title="<?php echo $products . TEXT_INFO_PAYMENT_METHOD.': '.(tep_not_null($orders['payment_method']) ? $orders['payment_method'] : TEXT_UNKNOWN); ?>"></i>
                    </td>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo '<a title="' . IMAGE_PREVIEW . '" href="' . tep_href_link(FILENAME_EDIT_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $orders['orders_id'] . '&action=edit', 'SSL') . '"><i class="fa fa-search text-success"></i></a>&nbsp;<i class="fa '. (($orders['purchased_without_account'])?'fa-user-times text-danger':'fa-user text-primary') .'" title="'. (($orders['purchased_without_account'])?'Guest Customer':'Customer') .'"></i>&nbsp;' . $orders['customers_name']; ?></td>
					  <?php
					  echo $cre_RCI->get('orders', 'headingcolval', false);
					  ?>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo strip_tags($order_total1); ?></td>
                    <td class="table-col dark text-center<?php echo $col_selected; ?>"><?php echo tep_datetime_short($orders['date_purchased']); ?></td>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if(substr_count($orders['payment_module_name'], 'paypal') > 0) { ?><i class="fa fa-paypal text-primary" title="Paypal"></i><?php } elseif(substr_count($orders['payment_module_name'], 'free') > 0) {?><i class="fa fa-gift text-warning" title="Free Order"></i><?php } elseif(substr_count($orders['payment_module_name'], 'check') > 0 || substr_count($orders['payment_module_name'], 'money') > 0 || substr_count($orders['payment_module_name'], 'bank') > 0 || substr_count($orders['payment_module_name'], 'wire') > 0 || substr_count($orders['payment_module_name'], 'transfer') > 0) { ?><i class="fa fa-money text-danger" title="Check and Money Order"></i><?php } else { ?><i class="fa fa-cc text-success" title="Credit Card"></i><?php } ?>&nbsp;<?php echo ucfirst($orders['payment_module_name']); ?></td>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo $orders['orders_status_name']; ?></td>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo '<a href="' . tep_href_link(FILENAME_EDIT_ORDERS, 'oID=' . $oInfo->orders_id, 'SSL') . '"><i class="fa fa-edit fa-lg text-success"></i></a>'; ?>
                      <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID')) . 'oID=' . $orders['orders_id'], 'SSL') . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                    </td>
                    </tr>
                    <?php
                  }  // RCO eof
                }
                ?>
              </tbody>
            </table>

            <div class="pagination-container ml-2 mr-2 mb-4">
              <div class="results-left"><?php echo $orders_split->display_count($orders_query_numrows, MAX_DISPLAY_ADMIN_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS); ?></div>
              <div class="results-right"><?php echo $orders_split->display_links($orders_query_numrows, MAX_DISPLAY_ADMIN_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'oID', 'action'))); ?></div>
            </div>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              switch ($action) {
                case 'delete':
                  $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_ORDER);
                  $contents[] = array('form' => tep_draw_form('orders', FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id . '&action=deleteconfirm', 'post' , '', 'SSL'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . sprintf(TEXT_INFO_DELETE_INTRO, '<b>' . $oInfo->orders_id . '</b>') . '</p></div></div></div>');
                  $contents[] = array('text' => '<div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 mb-1 mt-4 main-text">' . TEXT_INFO_RESTOCK_PRODUCT_QUANTITY . '</label><input type="checkbox" name="restock" class="js-switch js-default"></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button type="button" class="btn btn-default btn-sm mr-2 mt-3 mb-0" onclick="window.location=\'' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id, 'SSL') . '\'">' . IMAGE_CANCEL . '</button><button class="btn btn-danger btn-sm mt-3 mb-0" type="submit">' . IMAGE_CONFIRM_DELETE . '</button>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_DELETE_DATA . '<span class="sidebar-title ml-2 f-w-600">' . $oInfo->customers_name . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-1 mb-4">' . TEXT_INFO_DELETE_DATA_OID . '<span class="sidebar-title ml-2 f-w-600">' . $oInfo->orders_id . '</span></div>');
                  break;
                default:
                  if (isset($oInfo) && is_object($oInfo)) {
                    $heading[] = array('text' => sprintf(HEADING_ORDER_NUMBER, $oInfo->orders_id));

                    // RCO start
                    if ($cre_RCO->get('orders', 'sidebarbuttons') !== true) {
                      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
                                      <button class="btn btn-primary btn-sm mr-0 ml-1 mt-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_EDIT_ORDERS, 'oID=' . $oInfo->orders_id, 'SSL') . '\'">' . IMAGE_EDIT_ORDER . '</button>

                                      <a href="javascript:;" onclick="javascript:packaging_slip('.$oInfo->orders_id.',\'packaging_slip\')" class="btn btn-grey btn-sm mr-1 ml-1 mt-2 btn-packingslip">' . IMAGE_ORDERS_PACKINGSLIP . '</a>
                                      <a href="javascript:;" onclick="javascript:invoice('.$oInfo->orders_id.',\'invoice\')" class="btn btn-grey btn-sm mr-1 ml-1 mt-2 btn-packingslip">Invoice</a>
                                      <button class="btn btn-grey btn-sm mt-2 btn-editstatus" onclick="updateStatus(\'' . $oInfo->orders_id . '\');">' . IMAGE_EDIT_STATUS . '</button>
                                      '. $cre_RCI->get('orders', 'sidebarbuttons') .'
                                      <button class="btn btn-danger btn-sm ml-1 mt-2 btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id . '&action=delete', 'SSL') . '\'">' . IMAGE_DELETE . '</button>');
                    }
                    // RCO eof
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_DATE_ORDER_CREATED . '<span class="sidebar-title ml-2 f-w-600">' . tep_date_short($oInfo->date_purchased) . '</span></div>');
                    if (tep_not_null($oInfo->last_modified) && $oInfo->last_modified != '0000-00-00 00:00:00') $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_DATE_ORDER_LAST_MODIFIED . '<span class="sidebar-title ml-2 f-w-600">' . tep_date_short($oInfo->last_modified) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1 mb-4">' . TEXT_INFO_PAYMENT_METHOD . '<span class="sidebar-title ml-2 f-w-600">' . (tep_not_null($oInfo->payment_method) ? $oInfo->payment_method : TEXT_UNKNOWN) . '</span></div>');
                    // RCI sidebar bottom
                    $returned_rci = $cre_RCI->get('orders', 'sidebarbottom');
                    $contents[] = array('text' => $returned_rci);
                  }
                  break;
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>

<style>
.invoice-company-address {
  font-size: 14px;
}
.invoice-heading-text {
  font-size: 30px;
  margin-top: -20px;
}
</style>

    <!-- edit order status modal -->
    <div class="modal fade" id="editOrderStatusModal" tabindex="-1" role="dialog" aria-labelledby="editOrderStatusModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">

        <?php echo tep_draw_form('status', FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=update_order', 'post', '', 'SSL'); ?>
		<input type="hidden" name="oID" value="0">
		
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title f-s-16 fw-600" id="editOrderStatusModalLabel"><?php echo HEADING_EDIT_ORDER_STATUS; ?><span id="span-oID"></span></h4>
            <button type="button" class="close modal-close-fix" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <table class="table table-order-comments w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_DATE_ADDED; ?></th>
                  <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_CUSTOMER_NOTIFIED; ?></th>
                  <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_STATUS; ?></th>
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_COMMENTS; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $oID = (isset($oInfo)) ? (int)$oInfo->orders_id : 0;
                $orders_history_query = tep_db_query("select orders_status_id, date_added, customer_notified, comments from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_id = '" . tep_db_input($oID) . "' order by date_added");
                if (tep_db_num_rows($orders_history_query)) {
                  while ($orders_history = tep_db_fetch_array($orders_history_query)) {
                    echo '<tr class="table-row">' . "\n" .
                         '  <td class="table-col">' . tep_datetime_short($orders_history['date_added']) . '</td>' . "\n" .
                         '  <td class="table-col text-center">';
                    if ($orders_history['customer_notified'] == '1') {
                      echo '<span class="fa fa-check fa-2x text-success"></span>';
                    } else {
                      echo '<span class="fa fa-times fa-2x text-danger"></span>';
                    }
                    echo '  <td class="table-col text-center">' . $orders_status_array[$orders_history['orders_status_id']] . '</td>' . "\n" .
                         '  <td class="table-col">' . nl2br(tep_db_output($orders_history['comments'])) . '&nbsp;</td>' . "\n" .
                         '</tr>' . "\n";
                  }
                } else {
                  echo '<tr class="table-row">' . "\n" .
                       '  <td class="table-col" colspan="4">' . TEXT_NO_ORDER_HISTORY . '</td>' . "\n" .
                       '</tr>' . "\n";
                }
                ?>
              </tbody>
            </table>

            <div class="row ml-2 mr-2">
              <div class="col-6">
                <div class="form-group">
                  <label class="main-text"><?php echo TABLE_HEADING_COMMENTS; ?></label>
                  <?php echo tep_draw_textarea_field('comments', 'soft', '60', '5', null, 'class="form-control"'); ?>
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label class="main-text"><?php echo ENTRY_STATUS; ?></label>
                  <?php echo tep_draw_pull_down_menu('status', $orders_statuses, $order->info['orders_status_number'], 'class="form-control"'); ?>
                </div>
                <div class="form-check">
                  <?php echo tep_draw_checkbox_field('notify', '', 0, null, 'class="form-check-input js-switch"'); ?>
                  <label class="form-check-label main-text ml-1" for="notify"><?php echo ENTRY_NOTIFY_CUSTOMER; ?></label>
                </div>
                <div class="form-check mt-2">
                  <?php echo tep_draw_checkbox_field('notify_comments', '', 0, null, 'class="form-check-input js-switch"'); ?>
                  <label class="form-check-label main-text ml-1" for="notify_comments"><?php echo ENTRY_NOTIFY_COMMENTS; ?></label>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </div>
        </form>
      </div>
    </div>

    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function() {
  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small',
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  });
});

function updateStatus(oID) {
  $('#span-oID').text(oID);
  $('#editOrderStatusModal').modal('show');
  document.status.oID.value = oID;
}

$("#status").change(function(){
//alert("hii");

var select_amount = $("#status option:selected").val();
//alert(select_amount);
        $.post('orders.php',function(data){
            if(data != ''){ alert("hii");
                alert(data);
            }
        });

});


</script>
<script type="text/javascript">
function packaging_slip(order_id,type)
{
		$.fancybox.open({
				href : '<?php echo tep_href_link('print_order.php', '', 'SSL')?>?oID='+order_id+'&type='+type,
				type : 'iframe',
				width: '900',
				height: '600px',
				padding : 5,
				modal : true,
				showCloseButton : true,
				autoScale : true,
				openEffect: 'elastic',
				afterShow : function() {
					$('.fancybox-skin').append('<a title="Close" class="fancybox-item fancybox-close" href="javascript:jQuery.fancybox.close();"></a>');
				}
			});
}

function invoice(order_id,type)
{
		$.fancybox.open({
				href : '<?php echo tep_href_link('print_order.php', '', 'SSL')?>?oID='+order_id+'&type='+type,
				type : 'iframe',
				width: '900',
				height: '600px',
				padding : 5,
				modal : true,
				showCloseButton : true,
				autoScale : true,
				openEffect: 'elastic',
				afterShow : function() {
					$('.fancybox-skin').append('<a title="Close" class="fancybox-item fancybox-close" href="javascript:jQuery.fancybox.close();"></a>');
				}
			});
}

</script>

<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
