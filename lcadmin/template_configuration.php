<?php
/*
  $Id: template_configuration.php,v 1.1.1.1 2004/03/04 23:39:01 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');

// Alias function for Store configuration values in the Administration Tool
function tep_fixweight(){
  global  $infobox_id, $cID;
  //$column = $_GET['flag'];
  $rightpos = 'right';
  $leftpos = 'left';
  $result_query = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where display_in_column = '" . $leftpos . "' and template_id = '" . (int)$cID . "' order by location");

  $sorted_position = 0;
  while ($result = tep_db_fetch_array($result_query)) {
    $sorted_position++;
    tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set location = '" . $sorted_position . "' where infobox_id = '" . (int)$result['infobox_id'] . "' and template_id = '" . (int)$cID . "'");
  }
  $result_query = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where display_in_column = '" . $rightpos . "' and template_id = '" . (int)$cID . "' order by location");
  $sorted_position = 0;
  while ($result = tep_db_fetch_array($result_query)) {
    $sorted_positionright++;
    tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set location = '" . $sorted_position . "' where infobox_id = '" . (int)$result['infobox_id'] . "' and template_id = '" . (int)$cID . "'");
  }
  tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $cID . '&action=edit'));
}

// find gID
if (isset($_GET['gID'])) {
  $gID = $_GET['gID'] ;
}else if (isset($_POST['gID'])){
  $gID = $_POST['gID'] ;
} else {
  $gID = '' ;
}

// find cID
if (isset($_GET['cID'])) {
  $cID = $_GET['cID'] ;
}else if (isset($_POST['cID'])){
  $cID = $_POST['cID'] ;
} else {
 $cID = '' ;
}

// begin action
if (isset($_GET['action'])) {
  $action = $_GET['action'] ;
}else if (isset($_POST['action'])){
  $action = $_POST['action'] ;
} else {
  $action = '' ;
}


if (tep_not_null($action)) {
  switch ($action) {
    case 'fixweight':
      global  $infobox_id, $cID;
      $rightpos = 'right';
      $leftpos = 'left';
      $result_query = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where display_in_column = '" . $leftpos . "' and template_id = '" . (int)$cID . "' order by location");
      $sorted_position = 0;
      while ($result = tep_db_fetch_array($result_query)) {
        $sorted_position++;
        tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set location = '" . $sorted_position . "' where infobox_id = '" . (int)$result['infobox_id'] . "' and template_id = '" . (int)$cID . "'");
      }
      $result_query = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where display_in_column = '" . $rightpos . "' and template_id = '" . (int)$cID . "' order by location");
      $sorted_position = 0;
      while ($result = tep_db_fetch_array($result_query)) {
        $sorted_position++;
        tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set location = '" . $sorted_position . "' where infobox_id = '" . (int)$result['infobox_id'] . "' and template_id = '" . (int)$cID . "'");
      }

      tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $cID . '&action=edit'));
      break;

    case 'setflag': //set the status of a template active.
      if ( ($_GET['flag'] == 0) || ($_GET['flag'] == 1) ) {
        if ($cID) {
          tep_db_query("update " . TABLE_TEMPLATE . " set active = '" . $_GET['flag'] . "' where template_id = '" . (int)$cID . "'");
        }
        echo $_GET['flag'];exit;
      }

      tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $cID));
      break;

    case 'setflagtemplate': //set the status of a template active buttons.
      if ( ($_GET['flag'] == 'no' ) || ($_GET['flag'] == 'yes') ) {
        if ($cID) {
      if  ($_GET['case'] != 'infobox_display'){
          tep_db_query("update " . TABLE_TEMPLATE . " set  " . $_GET['case'] . " = '" . $_GET['flag'] . "' where template_id = '" . (int)$cID . "'");
          }else{
          tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set " . $_GET['case'] . " = '" . $_GET['flag'] . "' where infobox_id = '" . (int)$iID . "'");
          }
        }
        echo ($_GET['flag'] == 'yes')? '1':'0';exit;
      }

      tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $cID . '&action=edit'));
      break;
    case 'setflagtemplate_right': //set the status of a template active buttons.
      if ( ($_GET['flag'] == 'no' ) || ($_GET['flag'] == 'yes') ) {
        if ($cID) {
      if  ($_GET['case'] != 'infobox_display'){
          tep_db_query("update " . TABLE_TEMPLATE . " set  " . $_GET['case'] . " = '" . $_GET['flag'] . "' where template_id = '" . (int)$cID . "'");
          }else{
          tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set " . $_GET['case'] . " = '" . $_GET['flag'] . "' where infobox_id = '" . (int)$iID . "'");
          }
        }
        echo ($_GET['flag'] == 'yes')? '1':'0';exit;
      }

      tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $cID . '&action=edit'));
      break;

    case 'position_update': //set the status of a template active buttons.
      if ( ($_GET['flag'] == 'up') || ($_GET['flag'] == 'down') ) {
        if ($_GET['cID']) {
          tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set  location = '" . $_GET['loc'] .  "', last_modified = now() where location = '" . $_GET['loc1'] . "' and display_in_column = '" . $_GET['col'] . "'");
          tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set  location = '" . $_GET['loc1'] .  "', last_modified = now() where infobox_id = '" . (int)$iID . "' and display_in_column = '" . $_GET['col'] . "'");
        }
      }

      tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $cID . '&action=edit'));
      break;

    case 'setflaginfobox': //set the status of a news item.
      if ( ($_GET['flag'] == 'left') || ($_GET['flag'] == 'right') ) {
        if ($_GET['cID']) {
          tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set $case = '" . $_GET['flag'] . "' where infobox_id = '" . (int)$iID . "'");
        }
      }
      tep_fixweight();
      tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $cID . '&action=edit'));
      break;

    case 'save':
      $template_name = tep_db_prepare_input($_POST['template_name']);
      $cID = tep_db_prepare_input($cID);
      $sql_data_array = array('template_name' => tep_db_prepare_input($_POST['template_name']),
                              'template_cellpadding_main' => tep_db_prepare_input($_POST['template_cellpadding_main']),
                              'template_cellpadding_left' => tep_db_prepare_input($_POST['template_cellpadding_left']),
                              'template_cellpadding_right' => tep_db_prepare_input($_POST['template_cellpadding_right']),
                               'template_cellpadding_sub' => tep_db_prepare_input($_POST['template_cellpadding_sub']),
                               'box_width_left' => tep_db_prepare_input($_POST['box_width_left']),
                               'box_width_right' => tep_db_prepare_input($_POST['box_width_right']),
                               'cart_in_header' => tep_db_prepare_input($_POST['cart_in_header']),
                               'languages_in_header' => tep_db_prepare_input($_POST['languages_in_header']),
                               'show_header_link_buttons' => tep_db_prepare_input($_POST['show_header_link_buttons']),
                               'include_column_left' => tep_db_prepare_input($_POST['include_column_left']),
                               'include_column_right' => tep_db_prepare_input($_POST['include_column_right']),
                               'module_one' => tep_db_prepare_input($_POST['module_one']),
                               'module_two' => tep_db_prepare_input($_POST['module_two']),
                               'module_three' => tep_db_prepare_input($_POST['module_three']),
                               'module_four' => tep_db_prepare_input($_POST['module_four']),
                               'module_five' => tep_db_prepare_input($_POST['module_five']),
                               'module_six' => tep_db_prepare_input($_POST['module_six']),
                               'customer_greeting' => tep_db_prepare_input($_POST['customer_greeting']),
                               'edit_customer_greeting_personal' => '',
                               'edit_customer_greeting_personal_relogon' => '',
                               'edit_greeting_guest' => '',
                               'main_table_border' => tep_db_prepare_input($_POST['main_table_border']),
                               'show_heading_title_original' => tep_db_prepare_input($_POST['show_heading_title_original']),
                               'site_width' => tep_db_prepare_input($_POST['site_width']),
                               'side_box_left_width' => tep_db_prepare_input($_POST['side_box_left_width']),
                               'side_box_right_width' => tep_db_prepare_input($_POST['side_box_right_width']) );

      $update_sql_data = array('last_modified' => 'now()');
      $sql_data_array = array_merge($sql_data_array, $update_sql_data);
      tep_db_perform(TABLE_TEMPLATE, $sql_data_array, 'update', "template_id = '" . (int)$cID . "'");

      if (isset($_POST['default'])){
         if ($_POST['default'] == 'on') {
          tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . $template_name . "' where configuration_key = 'DEFAULT_TEMPLATE'");
        }
      }

      tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $cID));
      //tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cID));
      break;

    case 'insert':
      $template_sql_file = '';
      $template_name_new = $_POST['template_name'];

      if (file_exists(DIR_FS_TEMPLATES . $template_name_new . "/install.sql")){
        $template_sql_file = "install.sql";
      } else if (file_exists(DIR_FS_TEMPLATES . $template_name_new . "/" . $template_name_new . ".sql")){
        $template_sql_file = $template_name_new . ".sql";
      } else {
        $messageStack->add_session('search', ERROR1, 'error');
        tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'action=new'));
      }

      if($template_sql_file == 'default.sql'){
        $messageStack->add_session('search', ERROR3, 'error');
        tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'action=new'));
      } else {
        $template_sql_file = DIR_FS_TEMPLATES . $template_name_new . "/" . $template_sql_file;
        $sql_data_array = array('template_name' => $template_name_new);
        $update_sql_data = array('date_added' => 'now()');
        $sql_data_array = array_merge($sql_data_array, $update_sql_data);
        tep_db_perform(TABLE_TEMPLATE, $sql_data_array);
        $cID = tep_db_insert_id();
        $data_query = fread(fopen( $template_sql_file, 'rb'), filesize($template_sql_file)) ;
        $data_query = str_replace('#tID#', $cID, $data_query);
        $data_query = str_replace(';', '', $data_query);

        //make an array split on end of line and
        if (isset($data_query)) {
          $sql_array = array();
          $sql_length = strlen($data_query);
          $pos =  strpos($data_query, "\n");

          $data_query1 = explode("\n",$data_query);
          $key = key($data_query1);
          $sql_length = count($data_query1);
          $pos = $data_query1[$key];
          for ($i=$key; $i<$sql_length; $i++) {
            if ( strrchr($data_query1[$i], '--') ) {
             //if line starts with -- it's a comment ignore
            } else if ($data_query1[$i] == '') {
             //if line is empty ignore
            } else {
              tep_db_query( $data_query1[$i] );
            }
          }
        }

        // pull infobox info
        $infobox_query = tep_db_query("select box_heading, infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" .$cID. "'");
        // $infobox = tep_db_fetch_array($infobox_query);
        while ($infobox = tep_db_fetch_array($infobox_query)) {
          $languages = tep_get_languages();
          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $language_id = $languages[$i]['id'];
            $box_heading = tep_db_prepare_input($infobox['box_heading']);
            $infobox_id = $infobox['infobox_id'];
            $box_heading = str_replace("'", "\\'", $box_heading);
            tep_db_query("insert into " . TABLE_INFOBOX_HEADING . " (infobox_id, languages_id, box_heading) values ('" . $infobox_id . "', '" . $language_id . "', '" . $box_heading . "') ");
          }
        }
      }

      $messageStack->add_session('search', sprintf(TEMPLATE_INSTALLED_SUCCESS, $template_name_new) , 'success');
      tep_redirect(tep_href_link(FILENAME_INFOBOX_CONFIGURATION,'gID=' . $cID));
      break;

    case 'deleteconfirm':
      $cID = tep_db_prepare_input($_GET['cID']);

      //set customer template to default if selected template is deleted.
      $theme_query1 = tep_db_query("select template_name from " . TABLE_TEMPLATE . " where template_id = '" . (int)$cID . "'");
      $theme1 = tep_db_fetch_array($theme_query1);
      $sql_data_array5 = array('customers_selected_template' => '');// make it blank to use site default template
      tep_db_perform(TABLE_CUSTOMERS, $sql_data_array5, 'update', "customers_selected_template = '" . $theme1['template_name'] . "'");

      //RCI added to add delete logic for CDS and other template systems.
      echo $cre_RCI->get('templateconfiguration', 'delete', false);

      $infobox_query1 = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" .$cID. "'");
      // $infobox = tep_db_fetch_array($infobox_query);
      while ($infobox1 = tep_db_fetch_array($infobox_query1)) {
         $info_id = $infobox1['infobox_id'];
         tep_db_query("delete from " . TABLE_INFOBOX_HEADING . " where infobox_id = '" . (int)$info_id . "'");
      }

      tep_db_query("delete from " . TABLE_TEMPLATE . " where template_id = '" . (int)$cID . "'");
      tep_db_query("delete from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" . (int)$cID . "'");

      //need to get default ID
      $template_id_select_query5 = tep_db_query("select template_id from " . TABLE_TEMPLATE . "  where template_name = '" . DEFAULT_TEMPLATE . "'");
      $template_id_select5 =  tep_db_fetch_array($template_id_select_query5);
      $template_default_id5 = $template_id_select5['template_id'] ;
      $messageStack->add_session('search', sprintf(TEMPLATE_UNINSTALLED_SUCCESS, $theme1['template_name']) , 'success');
      tep_redirect(tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $template_default_id5 ));
      break;
  }
}


include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <?php
    if ($messageStack->size('search') > 0) {
      echo $messageStack->output('search');
    }
    ?>
    <!-- begin panel -->
<style>
.note-info-text {
  color: #6196b0;
}
</style>
    <div class="dark">
      <!-- body_text //-->
      <div id="table-templateconfiguration" class="table-templateconfiguration">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_TEMPLATE; ?></th>
                  <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_ACTIVE; ?></th>
                  <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_DISPLAY_COLUMN_LEFT; ?></th>
                  <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_DISPLAY_COLUMN_RIGHT; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $count_left_active = 0;
                $count_right_active = 0;
                $infobox_query = tep_db_query("select display_in_column, infobox_display  from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" . $cID . "'");
                while ($infobox = tep_db_fetch_array($infobox_query)) {
                  $infcol = $infobox['display_in_column'];
                  $infValue = $infobox['infobox_display'];
                  if (($infcol == 'left') && ($infValue != 'no')) {
                    $count_left_active++;
                  } else if (($infcol == 'right') && ($infValue != 'no')) {
                    $count_right_active++;
                  }
                }

                $curr_templates = '';

                $template_query = tep_db_query("select * from " . TABLE_TEMPLATE . "  order by template_name");
                while ($template = tep_db_fetch_array($template_query)) {

                  $curr_templates .= $template['template_name'].",";

                  if ((!isset($cID) || (isset($cID) && ($cID == $template['template_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
                    $tInfo_array = ($template);
                    $tInfo = new objectInfo($tInfo_array);
                    define('TEMPLATE_NAME', $tInfo->template_name);
                  }

              	  $selected = (isset($tInfo) && $template['template_id'] == $tInfo->template_id) ? ' selected' : '';
                  if  ($selected)  {
                    echo '<tr class="table-row dark selected" id="crow_'.$template['template_id'].'"  >' . "\n";
                    $onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION,'cID=' .       $template['template_id'] . '&action=edit') . '\'"';
                  } else {

                    echo '<tr class="table-row dark" id="crow_'.$template['template_id'].'">' . "\n";
                    $onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION,'cID=' . $template['template_id']) . '\'"';
                   }
                  $col_selected = ($selected) ? ' selected' : '';
                  if (DEFAULT_TEMPLATE == $template['template_name']) {
                    echo '<td class="table-col dark text-left'. $col_selected .'" '.$onclick.'><b>' . $template['template_name'] . ' (' . TEXT_DEFAULT . ')</b></td>' . "\n";
                  } else {
                    echo '<td class="table-col dark text-left'. $col_selected .'" '.$onclick.'>' . $template['template_name'] . '</td>' . "\n";
                  }
                  ?>
                  <td class="setflag table-col dark text-center<?php echo $col_selected; ?>">
                    <?php
/*                    if ($template['active'] == '1') {
                      echo '<i class="fa fa-lg fa-check-circle text-success mr-2"></i><a href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'action=setflag&flag=0&cID=' . $template['template_id'] ) . '"><i class="fa fa-lg fa-times-circle text-secondary"></i></a>';
                    } else {
                      echo '<a href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'action=setflag&flag=1&cID=' . $template['template_id']) . '"><i class="fa fa-lg fa-check-circle text-secondary"></i></a>&nbsp;&nbsp;<i class="fa fa-lg fa-times-circle text-danger"></i>';
                    }
*/

					  $ajax_link = tep_href_link(FILENAME_TEMPLATE_CONFIGURATION);
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=0&cID='.$template['template_id'].'\', '.$template['template_id'].',0 )" '.(($template['active'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=1&cID='.$template['template_id'].'\', '.$template['template_id'].',1  )" '.(($template['active'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
                    ?>
                  </td>
                  <td class="setflagtemplate table-col dark text-center<?php echo $col_selected; ?>">
                    <?php
                  /*  if ($template['include_column_left'] == 'yes') {
                      echo '<i class="fa fa-lg fa-check-circle text-success mr-2"></i><a href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'action=setflagtemplate&case=include_column_left&flag=no&cID=' . $template['template_id']) . '"><i class="fa fa-lg fa-times-circle text-secondary"></i></a>';
                    } else {
                      echo '<a href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'action=setflagtemplate&case=include_column_left&flag=yes&cID=' . $template['template_id']) . '"><i class="fa fa-lg fa-check-circle text-secondary"></i></a>&nbsp;&nbsp;<i class="fa fa-lg fa-times-circle text-danger"></i>';
                    } */

 					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflagtemplate\',\''.$ajax_link.'\',\'action=setflagtemplate&case=include_column_left&flag=no&cID='.$template['template_id'].'\', '.$template['template_id'].',0 )" '.(($template['include_column_left'] == 'yes')? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflagtemplate\',\''.$ajax_link.'\',\'action=setflagtemplate&case=include_column_left&flag=yes&cID='.$template['template_id'].'\', '.$template['template_id'].',1 )" '.(($template['include_column_left'] == 'no')? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
                    ?>
                  </td>
                  <td class="setflagtemplate_right table-col dark text-center<?php echo $col_selected; ?>"><?php
                 /*   if ($template['include_column_right'] == 'yes') {
                      echo '<i class="fa fa-lg fa-check-circle text-success mr-2"></i><a href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'action=setflagtemplate&case=include_column_right&flag=no&cID=' . $template['template_id']) . '"><i class="fa fa-lg fa-times-circle text-secondary"></i></a>';
                    } else {
                      echo '<a href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'action=setflagtemplate&case=include_column_right&flag=yes&cID=' . $template['template_id']) . '"><i class="fa fa-lg fa-check-circle text-secondary"></i></a>&nbsp;&nbsp;<i class="fa fa-lg fa-times-circle text-danger"></i>';
                    } */

 					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflagtemplate_right\',\''.$ajax_link.'\',\'action=setflagtemplate_right&case=include_column_right&flag=no&cID='.$template['template_id'].'\', '.$template['template_id'].',0 )" '.(($template['include_column_right'] == 'yes')? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflagtemplate_right\',\''.$ajax_link.'\',\'action=setflagtemplate_right&case=include_column_right&flag=yes&cID='.$template['template_id'].'\', '.$template['template_id'].',1 )" '.(($template['include_column_right'] == 'no')? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';

                    ?>
                  </td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank"><?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION,'cID=' . $template['template_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                  &nbsp;</td>
                  <?php
                }
                ?>
                </tr>
              </tbody>
            </table>
            <?php
            if ($action != 'new') {
              ?>
              <div class="float-right mr-2 mt-3 mb-3" role="group">
                <?php echo '<a class="btn btn-success btn-sm" href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION,'action=new') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>'; ?>&nbsp;&nbsp;</td>
              </div>
              <?php
            }
            ?>
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php

            $heading = array();
            $contents = array();

            //get template type
            if (isset($tInfo->template_name)) {

              if ( file_exists(DIR_FS_TEMPLATES .$tInfo->template_name . '/template.php')) {
                include_once(DIR_FS_TEMPLATES .$tInfo->template_name . '/template.php');
                $template_type = TEMPLATE_SYSTEM ;
              } else {
                $template_type = 'cre_legacy';
              }

            }

            switch ($action) {
              case 'new':
                $heading[] = array('text' => TEXT_HEADING_NEW_TEMPLATE );
                $contents[] = array('form' => tep_draw_form('new_template', FILENAME_TEMPLATE_CONFIGURATION, 'action=insert', 'post', 'enctype="multipart/form-data"'));
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 mb-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_NEW_INTRO . '</p></div></div></div>');

                if ($handle = opendir(DIR_FS_TEMPLATES)) {
                  /* This is the correct way to loop over the directory. */
                  while (false !== ($file = readdir($handle))) {
                    if(is_dir(DIR_FS_TEMPLATES . '/' . $file) && stristr($curr_templates.".,..,content,CVS", $file ) == FALSE){
                      if ( ($file == 'default') || ($file == 'boxes') || ($file == 'mainpage_modules')|| ($file == '.svn') ){
                      }else{
                        $dirs[] = $file;
                        $dirs_array[] = array('id' => $file,
                                              'text' => $file);
                      }
                    }
                  }
                  closedir($handle);
                }

                if(count($dirs_array) == 0){
          		    $contents[] = array('text' => '<label class="control-label sidebar-edit mb-0">' . TEXT_TEMPLATE_NAME . '</label>' . tep_draw_input_field('template_name', null, 'class="form-control"'));
                }else{
                  sort($dirs_array);
                  $contents[] = array('text' => '<label class="control-label sidebar-edit mb-0">' . TEXT_TEMPLATE_NAME . '</label>' . tep_draw_pull_down_menu('template_name', $dirs_array, '', 'class="form-control"'));
                }

                $contents[] = array('text' => '<br/><label class="control-label sidebar-edit mb-0">' . TEXT_TEMPLATE_IMAGE . '</label>' . tep_draw_file_field('template_image', '', '', 'class="filestyle"'));
                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE .'</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION,'gID=1') . '">' . IMAGE_CANCEL . '</a></div>');
                break;
              case 'edit':

                switch ($tInfo->cart_in_header) {
                  case 'no': $cart_in_status = false; $cart_out_status = true; break;
                  case 'yes': $cart_in_status = true; $cart_out_status = false; break;
                  default: $cart_in_status = true; $cart_out_status = false;
                }
                switch ($tInfo->languages_in_header) {
                  case 'no': $yes_status = false; $no_status = true; break;
                  case 'yes': $yes_status = true; $no_status = false; break;
                  default: $yes_status = true; $no_status = false;

                }
                switch ($tInfo->include_column_left) {
                  case 'no': $no_left_status = true; $yes_left_status = false; break;
                  case 'yes': $no_left_status = false; $yes_left_status = true; break;
                  default: $no_left_status = false; $yes_left_status = true;
                }
                switch ($tInfo->include_column_right) {
                  case 'no': $no_right_status = true; $yes_right_status = false; break;
                  case 'yes': $no_right_status = false; $yes_right_status = true; break;
                  default: $no_right__status = false; $yes_right_status = true;
                }

                switch ($tInfo->show_header_link_buttons) {
                  case 'no': $links_no_status = true; $links_yes_status = false; break;
                  case 'yes': $links_no_status = false; $links_yes_status = true; break;
                  default: $links_no_status = false; $links_yes_status = true;
                }


                $heading[] = array('text' => TEXT_HEADING_EDIT_TEMPLATE );
                $contents[] = array('form' =>  tep_draw_form('template', FILENAME_TEMPLATE_CONFIGURATION,'cID=' . $tInfo->template_id . '&action=save', 'post', 'enctype="multipart/form-data"'));
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_EDIT_INTRO . '</p></div></div></div>');
                $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-2">'. TEXT_TEMPLATE_NAME . tep_draw_hidden_field('template_name', $tInfo->template_name, 'class="form-control"') . '<span class="sidebar-title ml-1">' . $tInfo->template_name .'</span></div>');
                if (DEFAULT_TEMPLATE != $tInfo->template_name) $contents[] = array('text' => '<div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text">' . TEXT_SET_DEFAULT . '</label><input type="checkbox" name="default" class="js-switch"></div>');
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_TEMPLATE_SYSTEM . '<span class="text-danger f-w-600">' . $template_type . '</span>'. TEXT_TEMPLATE_SYSTEM_1 . '</p></div></div></div>');
            		$contents[] = array('text' => '<div class="form-inline mt-3"><div class="form-group"><label class="sidebar-text mr-1">'.TEXT_SITE_WIDTH . '</label>' . tep_draw_input_field('site_width', $tInfo->site_width,'class="form-control w-50"') . '</div></div>');


                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADER . '</h5>
                                                     <div class="form-group mb-0 mt-1"><label class="note-info-text mb-0 mr-1">'.TEXT_INCLUDE_CART_IN_HEADER. '</label>' . tep_draw_radio_field('cart_in_header', 'yes', $cart_in_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_YES . '</span>' . tep_draw_radio_field('cart_in_header', 'no', $cart_out_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_NO . '</span></div>
                                                     <div class="form-group mb-0"><label class="note-info-text mb-0 mr-1">'.TEXT_INCLUDE_LANGUAGES_IN_HEADER. '</label>' . tep_draw_radio_field('languages_in_header', 'yes', $yes_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_YES . '</span>' . tep_draw_radio_field('languages_in_header', 'no', $no_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_NO . '</span></div>
                                                     <div class="form-group mb-0"><label class="note-info-text mb-0 mr-1">'.TEXT_INCLUDE_HEADER_LINK_BUTTONS. '</label>' . tep_draw_radio_field('show_header_link_buttons', 'yes', $links_yes_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_YES . '</span>' . tep_draw_radio_field('show_header_link_buttons', 'no', $links_no_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_NO . '</span></div>
                                                   </div>
                                                 </div>
                                               </div>');


                $select_box = '<select class="form-control mb-1" name="template_cellpadding_main" " style="width: 40">';
                for ($i = 0; $i <= 10; $i++) {
                  $select_box .= '<option value="' . $i . '"';
                  if ($i == $tInfo->template_cellpadding_main) $select_box .= ' selected="selected"';
                  $select_box .= '>' . $i . '</option>';
                }
                $select_box .= "</select>";

                $select_box2 = '<select class="form-control mb-1" name="template_cellpadding_sub" " style="width: 40">';
                for ($i = 0; $i <= 10; $i++) {
                  $select_box2 .= '<option value="' . $i . '"';
                  if ($i == $tInfo->template_cellpadding_sub) $select_box2 .= ' selected="selected"';
                  $select_box2 .= '>' . $i . '</option>';
                }
                $select_box2 .= "</select>";

                $select_box3 = '<select class="form-control mb-1" name="side_box_left_width" " style="width: 40">';
                for ($i = 0; $i <= 20; $i++) {
                  $select_box3 .= '<option value="' . $i . '"';
                  if ($i == $tInfo->side_box_left_width) $select_box3 .= ' selected="selected"';
                  $select_box3 .= '>' . $i . '</option>';
                }
                $select_box3 .= "</select>";

                $select_box4 = '<select class="form-control mb-1" name="side_box_right_width" " style="width: 40">';
                for ($i = 0; $i <= 20; $i++) {
                  $select_box4 .= '<option value="' . $i . '"';
                  if ($i == $tInfo->side_box_right_width) $select_box4 .= ' selected="selected"';
                  $select_box4 .= '>' . $i . '</option>';
                }
                $select_box4 .= "</select>";

                $select_box5 = '<select class="form-control mb-1" name="template_cellpadding_left" " style="width: 40">';
                for ($i = 0; $i <= 10; $i++) {
                  $select_box5 .= '<option value="' . $i . '"';
                  if ($i == $tInfo->template_cellpadding_left) $select_box5 .= ' selected="selected"';
                  $select_box5 .= '>' . $i . '</option>';
                }
                $select_box5 .= "</select>";

                $select_box6 = '<select class="form-control mb-1" name="template_cellpadding_right" " style="width: 40">';
                for ($i = 0; $i <= 10; $i++) {
                  $select_box6 .= '<option value="' . $i . '"';
                  if ($i == $tInfo->template_cellpadding_right) $select_box6 .= ' selected="selected"';
                  $select_box6 .= '>' . $i . '</option>';
                }
                $select_box6 .= "</select>";

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_TABLE_CELL_PADDING . '</h5>
                 <div class="form-group mt-2"><label class="note-info-text mb-0">' . TEXT_TEMPLATE_CELLPADDING_MAIN . '</label>' .  $select_box . '</div>
                 <div class="form-group mt-1"><label class="note-info-text mb-0">' . TEXT_TEMPLATE_CELLPADDING_SUB . '</label>' .  $select_box2 . '</div>
                 <div class="form-group mt-1"><label class="note-info-text mb-0">' . TEXT_TEMPLATE_LEFT_SIDE . '</label>' .  $select_box3 . '</div>
                 <div class="form-group mt-1"><label class="note-info-text mb-0">' . TEXT_TEMPLATE_RIGHT_SIDE . '</label>' .  $select_box4 . '</div>
                                                    </div>
                                                  </div>
                                                </div>');

                $col_left_html = '';
                if ($tInfo->include_column_left == 'yes') {
                  $col_left_html .= '<div class="form-inline mb-0 mt-1"><label class="note-info-text mb-0 mr-1">'.TEXT_COLUMN_LEFT_WIDTH . '</label>' . tep_draw_input_field('box_width_left', $tInfo->box_width_left,'class="form-control w-50"'). '</div>';
                  $col_left_html .= '<div class="form-group mb-0 mt-1"><label class="note-info-text mb-0 mr-1">'.TEXT_TEMPLATE_CELLPADDING_LEFT . '</label>' . $select_box5 . '</div>';
                } else {
                  $col_left_html .= tep_draw_hidden_field('box_width_left', $tInfo->box_width_left);
                  $col_left_html .= tep_draw_hidden_field('template_cellpadding_left', $tInfo->template_cellpadding_left);
                }

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_LEFT_COLUMN . '</h5>

                  <div class="form-group mb-0 mt-1"><label class="note-info-text mb-0 mr-1">'.TEXT_INCLUDE_COLUMN_LEFT. '</label>' . tep_draw_radio_field('include_column_left', 'yes', $yes_left_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_YES . '</span>' . tep_draw_radio_field('include_column_left', 'no', $no_left_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_NO . '</span></div>' .
                  $col_left_html . '
                                                    </div>
                                                  </div>
                                                </div>');

                $col_right_html = '';
                if ($tInfo->include_column_right == 'yes') {
                  $col_right_html .= '<div class="form-inline mb-0 mt-1"><label class="note-info-text mb-0 mr-1">'. TEXT_COLUMN_RIGHT_WIDTH . '</label>' . tep_draw_input_field('box_width_right', $tInfo->box_width_right,'class="form-control w-50"') . '</div>';
                  $col_right_html .= '<div class="form-group mb-0 mt-1"><label class="note-info-text mb-0 mr-1">'. TEXT_TEMPLATE_CELLPADDING_RIGHT  . '</label>' . $select_box6 . '</div>';
                } else {
                  $col_right_html .= tep_draw_hidden_field('box_width_right', $tInfo->box_width_right);
                  $col_right_html .= tep_draw_hidden_field('template_cellpadding_right', $tInfo->template_cellpadding_right);
                }

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_RIGHT_COLUMN . '</h5>

                  <div class="form-group mb-0 mt-1"><label class="note-info-text mb-0 mr-1">'.TEXT_INCLUDE_COLUMN_RIGHT. '</label>' . tep_draw_radio_field('include_column_right', 'yes', $yes_right_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_YES . '</span>' . tep_draw_radio_field('include_column_right', 'no', $no_right_status) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_NO . '</span></div>' .
                  $col_right_html . '
                                                    </div>
                                                  </div>
                                                </div>');

                // find the main page modules folder
                // if the constant is define, use it, otherwise check
                if ( ! defined('DIR_FS_TEMPLATE_MAINPAGES') ) {
                  if (file_exists(DIR_FS_TEMPLATES . $tInfo->template_name . '/mainpage_modules/')) {
                    define('DIR_FS_TEMPLATE_MAINPAGES', DIR_FS_TEMPLATES . $tInfo->template_name . '/mainpage_modules/');
                  } else {
                    define('DIR_FS_TEMPLATE_MAINPAGES', DIR_FS_TEMPLATES . 'default/mainpage_modules/');
                  }
                }
                // this is to provide backward compatability
                $modules_folder = DIR_FS_TEMPLATE_MAINPAGES;
                $modules_folder_1 = $modules_folder  ;
                $modules_folder_2 = wordwrap($modules_folder_1, 36, "<br>\n", 1);

                if ($handle = opendir($modules_folder)) {
                  $dirs[] = array();
                  $dirs_array[] = array('text' => '');

                  while (false !== ($file = readdir($handle))) {
                    if (stristr($file,".php") || stristr($file,".htm") || stristr($file,".html")) {
                      $dirs_array[] = array('id' => $file,
                                            'text' => $file);
                    }
                  }
                  closedir($handle);
                }
                sort($dirs_array);

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_MAINPAGE_MODULES . '</h5>
                                                     <div class="mt-2 f-w-600">' . TEXT_MAINPAGE_MODULES_LOCATION .'</div><div class=""><small>' . $modules_folder_2 .'</small></div>
                 <div class="form-group mt-2">' . tep_draw_pull_down_menu('module_one', $dirs_array, $tInfo->module_one, 'class="form-control"') . '</div>
                 <div class="form-group mt-1">' . tep_draw_pull_down_menu('module_two', $dirs_array, $tInfo->module_two, 'class="form-control"') . '</div>
                 <div class="form-group mt-1">' . tep_draw_pull_down_menu('module_three', $dirs_array, $tInfo->module_three, 'class="form-control"') . '</div>
                 <div class="form-group mt-1">' . tep_draw_pull_down_menu('module_four', $dirs_array, $tInfo->module_four, 'class="form-control"') . '</div>
                 <div class="form-group mt-1">' . tep_draw_pull_down_menu('module_five', $dirs_array, $tInfo->module_five, 'class="form-control"') . '</div>
                 <div class="form-group mt-1">' . tep_draw_pull_down_menu('module_six', $dirs_array, $tInfo->module_six, 'class="form-control"') . '</div>
                                                    </div>
                                                  </div>
                                                </div>');

                switch ($tInfo->customer_greeting) {
                  case 'no': $show_greet_no = true; $show_greet_yes = false; break;
                  case 'yes': $show_greet_no = false; $show_greet_yes = true; break;
                  default: $show_greet_no = false; $show_greet_yes = true;
                }
                switch ($tInfo->main_table_border) {
                  case 'no': $use_border_no = true; $use_border_yes = false; break;
                  case 'yes': $use_border_no = false; $use_border_yes = true; break;
                  default: $use_border_no = false; $use_border_yes = true;
                }
                switch ($tInfo->show_heading_title_original) {
                  case 'no': $orig_page_headers_no = true; $orig_page_headers_yes = false; break;
                  case 'yes': $orig_page_headers_no = false; $orig_page_headers_yes = true; break;
                  default: $orig_page_headers_no = false; $orig_page_headers_yes = true;
                }


                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_OTHER . '</h5>
                  <div class="form-group mb-0"><label class="note-info-text mb-0 mr-1">'.TEXT_SHOW_CUSTOMER_GREETING. '</label>' .
                  tep_draw_radio_field('customer_greeting', 'yes', $show_greet_yes) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_YES . '</span>' .
                  tep_draw_radio_field('customer_greeting', 'no', $show_greet_no)  . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_NO . '</span></div>

                  <div class="form-group mb-0"><label class="note-info-text mb-0 mr-1">'.TEXT_INCLUDE_MAIN_TABLE_BORDER. '</label>' .
                  tep_draw_radio_field('main_table_border', 'yes', $use_border_yes) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_YES . '</span>' .
                  tep_draw_radio_field('main_table_border', 'no', $use_border_no)  . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_NO . '</span></div>

                  <div class="form-group mb-0"><label class="note-info-text mb-0 mr-1">'.TEXT_SHOW_ORIGINAL_PAGE_HEADERS. '</label>' .
                  tep_draw_radio_field('show_heading_title_original', 'yes', $orig_page_headers_yes) . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_YES . '</span>' .
                  tep_draw_radio_field('show_heading_title_original', 'no', $orig_page_headers_no)  . '<span class="note-info-text mr-1 ml-1 f-w-600">' . TEXT_NO . '</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE .'</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION,'&cID=' . $tInfo->template_id) . '">' . IMAGE_CANCEL . '</a></div>');
                break;
              case 'delete':
                $heading[] = array('text' => TEXT_HEADING_DELETE_TEMPLATE);
                $contents[] = array('form' => tep_draw_form('theme', FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $tInfo->template_id . '&action=deleteconfirm'));
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0 fw-400">'. sprintf(TEXT_DELETE_INTRO, $tInfo->template_name) . '</b></div></div></div>');
                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_CONFIRM_DELETE .'</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION,'cID=' . $tInfo->template_id) . '">' . IMAGE_CANCEL . '</a></div>');
                break;
              default:
                if (is_object($tInfo)) {
                  $heading[] = array('text' => $tInfo->template_name );
                  if(DEFAULT_TEMPLATE == $tInfo->template_name) { // Do not allow deleting site default template!
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $tInfo->template_id . '&action=edit') . '">' . IMAGE_EDIT . '</a><a class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="javascript:onClick=alert(\'' . ERROR_NO_DELETE . '\');">' . IMAGE_DELETE . '</a></div>');
                  } else {
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION, 'cID=' . $tInfo->template_id . '&action=edit') . '">' . IMAGE_EDIT . '</a><a class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_TEMPLATE_CONFIGURATION,'cID=' . $tInfo->template_id . '&action=delete') . '">' . IMAGE_DELETE . '</a></div>');
                  }
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_DATE_ADDED . ' <span class="sidebar-title ml-2">' . tep_date_short($tInfo->date_added) . '</span></div>');
                  if (tep_not_null($tInfo->last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-2">'.TEXT_LAST_MODIFIED . ' <b>' . tep_date_short($tInfo->last_modified) . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text text-center mt-2 mb-2">' . tep_image(DIR_WS_CATALOG_TEMPLATES . $tInfo->template_name .'/images/' .$tInfo->template_image, $tInfo->template_name,'200','160').'</div>');
                }
            }

            // use $box->showSidebar()
            if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
          	$box = new box;
          	echo $box->showSidebar($heading, $contents);
            }
            ?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function(){
  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small',
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  });
});
</script>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
