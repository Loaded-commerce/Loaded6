<?php
/*
  $Id: specials.php,v 1.1.1.1 2004/03/04 23:38:58 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {
      case 'setflag':
        tep_set_specials_status($_GET['id'], $_GET['flag']);
        echo $_GET['flag'];
        exit;
        //tep_redirect(tep_href_link(FILENAME_SPECIALS, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'sID=' . $_GET['id'], 'NONSSL'));
        break;
      case 'insert':
       // print_r($_POST);exit;
        $products_id = tep_db_prepare_input($_POST['products_id']);
        $products_price = tep_db_prepare_input($_POST['products_price']);
        // remove the $ if it has been placed in the speical_price field
        $_POST['specials_price'] = str_replace('$', '', $_POST['specials_price']);
        $_POST['specials_price'] = str_replace(' ', '', $_POST['specials_price']);
        $specials_price = tep_db_prepare_input($_POST['specials_price']);
        $expires_date = (($_POST['expires_date'] == '')?'0000-00-00':date('Y-m-d', strtotime($_POST['expires_date'])));

        if (substr($specials_price, -1) == '%') {
          $new_special_insert_query = tep_db_query("select products_id, products_price from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
          $new_special_insert = tep_db_fetch_array($new_special_insert_query);
          $special_percentage= $specials_price;
          $products_price = $new_special_insert['products_price'];
          $specials_price = ($products_price - (($specials_price / 100) * $products_price));

        }
        else{
          $new_special_insert_query = tep_db_query("select products_id, products_price from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
          $new_special_insert = tep_db_fetch_array($new_special_insert_query);
         $special_percentage = number_format(100 - (($specials_price / $new_special_insert['products_price']) * 100));
        }

		// RCI for action insert
		echo $cre_RCI->get('specials', 'addprevalidation', false);

        tep_db_query("insert into " . TABLE_SPECIALS . " (products_id, specials_new_products_price,special_percentage, specials_date_added, expires_date, status) values ('" . (int)$products_id . "', '" . tep_db_input($specials_price) . "','".$special_percentage."', now(), '" . tep_db_input($expires_date) . "', '1')");
		$specials_id = tep_db_insert_id();
		// RCI for action insert
		echo $cre_RCI->get('specials', 'action', false);

        $messageStack->add_session('specials', "Special Product Added Sucessfully!!", 'success');
        tep_redirect(tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page']));
        break;
      case 'update':
        $specials_id = tep_db_prepare_input($_POST['specials_id']);
        $products_price = tep_db_prepare_input($_POST['products_price']);
        $_POST['specials_price'] = str_replace('$', '', $_POST['specials_price']);
        $_POST['specials_price'] = str_replace(' ', '', $_POST['specials_price']);
        $specials_price = tep_db_prepare_input($_POST['specials_price']);
        $expires_date = (($_POST['expires_date'] == '')?'0000-00-00':date('Y-m-d', strtotime($_POST['expires_date'])));

        if (substr($specials_price, -1) == '%'){
         $special_percentage= $specials_price;
         $specials_price = ($products_price - (($specials_price / 100) * $products_price));
        }else{
         $special_percentage = number_format(100 - (($specials_price / $products_price) * 100));
        }
		// RCI for action insert
		echo $cre_RCI->get('specials', 'updateprevalidation', false);

        tep_db_query("update " . TABLE_SPECIALS . " set specials_new_products_price = '" . tep_db_input($specials_price) . "',special_percentage= '" . tep_db_input($special_percentage) . "', specials_last_modified = now(), expires_date = '" . tep_db_input($expires_date) . "' where specials_id = '" . (int)$specials_id . "'");

		// RCI for action
		echo $cre_RCI->get('specials', 'action', false);

        $messageStack->add_session('specials', "Special Product Updated Sucessfully!!", 'success');
        tep_redirect(tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page'] . '&sID=' . $specials_id));
        break;
      case 'deleteconfirm':
        $specials_id = tep_db_prepare_input($_GET['sID']);

        tep_db_query("delete from " . TABLE_SPECIALS . " where specials_id = '" . (int)$specials_id . "'");

		// RCI for action
		echo $cre_RCI->get('specials', 'action', false);

        $messageStack->add_session('specials', "Special Product Deleted Sucessfully!!", 'warning');
        tep_redirect(tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page']));
        break;
    }
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');

  if ( ($action == 'new') || ($action == 'edit') ) {
    $form_action = 'insert';
    if ( ($action == 'edit') && isset($_GET['sID']) ) {
      $form_action = 'update';

// Eversun mod for sppc and qty price breaks
//      $product_query = tep_db_query("select p.products_id, pd.products_name, p.products_price, s.specials_new_products_price, s.expires_date from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_SPECIALS . " s where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and p.products_id = s.products_id and s.specials_id = '" . (int)$_GET['sID'] . "'");
      $product_query = tep_db_query("select p.products_id, pd.products_name, p.products_price, s.specials_new_products_price, s.expires_date from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_SPECIALS . " s where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and p.products_id = s.products_id and s.specials_id = '" . (int)$_GET['sID'] . "'");
// Eversun mod end for sppc and qty price breaks

      $product = tep_db_fetch_array($product_query);

      $sInfo = new objectInfo($product);
    } else {
      $sInfo = new objectInfo(array());

      // if it is active or not, get it if it is in the specials
      $on_special_array = array();
      $on_special_query = tep_db_query("SELECT DISTINCT products_id
                                        FROM " .  TABLE_SPECIALS . "
                                        ORDER BY products_id
                                       ");
      while ($on_special = tep_db_fetch_array($on_special_query)) {
        $on_special_array[] = $on_special['products_id'];
      }
    }
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col main-col">
    <?php
    if ($messageStack->size('specials') > 0) {
      echo $messageStack->output('specials');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-bannermanager" class="table-bannermanager">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">
			  <div class="main-heading m-t-20"><span><?php echo "New Special Product"; ?></span>
				<div class="main-heading-footer"></div>
			  </div>

      <form name="new_special" <?php echo 'action="' . tep_href_link(FILENAME_SPECIALS, tep_get_all_get_params(array('action', 'info', 'sID')) . 'action=' . $form_action, 'NONSSL') . '"'; ?> method="post" data-parsley-validate>
      <?php if ($form_action == 'update') echo tep_draw_hidden_field('specials_id', $_GET['sID']); ?>
		  <div class="form-group row mb-3 mt-3">
			<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo "Product:"; ?></label>
			<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
			   <?php // echo (isset($sInfo->products_name)) ? $sInfo->products_name . ' <small>(' . $currencies->format($sInfo->products_price) . ')</small>' : tep_draw_products_pull_down('products_id', 'style="font-size:10px"', $on_special_array); echo tep_draw_hidden_field('products_price', (isset($sInfo->products_price) ? $sInfo->products_price : '')); ?>
               <div id="replacediv" class=" well" style="padding:0px;margin-bottom: 0px;border: none;">
				  <div id="search_section">
					 <?php echo ((isset($sInfo->products_name))? '<input type="text" name="products_id" class="form-control" id="special_data_info" value="'.$sInfo->products_name.'" readonly >' :'<input type="text" class="form-control custom-search cust-input ac_input" id="special_data_info" autocomplete="off" name="products_id" value="" height="50px;"  placeholder="Search Product...." style="">'); ?>
					 <?php  if (isset($sInfo->products_price)) {
						echo tep_draw_hidden_field('products_price', $sInfo->products_price);
					  } else {
						echo tep_draw_hidden_field('products_price', 0);
					  } ?>
				  </div>
			 </div>
			</div>
		 </div>
		  <div class="form-group row mb-3 mt-3">
			<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_SPECIALS_SPECIAL_PRICE; ?></label>
			<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input"><?php echo tep_draw_input_field('specials_price', (isset($sInfo->specials_new_products_price) ? $sInfo->specials_new_products_price : ''), 'required="required"'); ?></div>
		  </div>
		  <div class="form-group row mb-3 mt-3">
			<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_SPECIALS_EXPIRES_DATE; ?></label>
			<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input"><?php echo tep_draw_input_field('expires_date',(($sInfo->expires_date == '' || $sInfo->expires_date == '0000-00-00 00:00:00')?'':date('m/d/Y', strtotime($sInfo->expires_date))),'class="calender form-control" id="calender" data-date-format="mm/dd/yyyy"'); ?></div>
		  </div>
		 <?php
		  // RCI for inserting the extra fields
		  echo $cre_RCI->get('specials', 'bottominsideform');
		 ?>
		  <div class="form-group row mb-3 mt-3">
			<div class="col-xs-12 col-md-12 col-lg-12 p-r-10 meta-input text-right"><a class="btn btn-default btn-sm mt-2 mb-2 mr-2" href="<?php echo tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page'] . (isset($_GET['sID']) ? '&sID=' . $_GET['sID'] : '')); ?>"><?php echo IMAGE_CANCEL; ?></a><?php echo (($form_action == 'insert') ? '<button class="btn btn-success btn-sm" type="submit">' . IMAGE_INSERT . '</button>' : '<button class="btn btn-success btn-sm" type="submit">' . IMAGE_UPDATE . '</button>'); ?></div>
		  </div>
		  <div class="sidebar-title mt-2">
		    <div class="row">
		  	  <div class="col p-0 mt-3 ml-2 mr-2">
			    <div class="note note-warning m-0">
				  <p class="mb-0 mt-0 f-w-400"><?php echo TEXT_SPECIALS_PRICE_TIP; ?></p>
			    </div>
			  </div>
		    </div>
		  </div>

      </form>
<?php
  } else {
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <div style="padding-left:7px;">
  	<a class="btn btn-success btn-sm mt-2 mb-2" data-toggle="modal" data-target="#myModal" style="color:#fff;">New Product</a>
    <a class="btn btn-success btn-sm mt-2 mb-2" href="javascript:void(0)" onclick="javascript:updateSortOrder()">Update Sort Order</a>
  </div>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <?php
    if ($messageStack->size('bannermanager') > 0) {
      echo $messageStack->output('bannermanager');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-bannermanager" class="table-bannermanager">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_PRODUCTS; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_PRODUCTS_PRICE; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_STATUS; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_SORT; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
              </thead>
              <tbody class="row_special_position">
<?php
    $specials_query_raw = "select p.products_id, pd.products_name, p.products_price, s.specials_id, s.specials_new_products_price, s.specials_date_added, s.specials_last_modified, s.expires_date, s.date_status_change,s.specials_sort, s.status from " . TABLE_PRODUCTS . " p, " . TABLE_SPECIALS . " s, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and p.products_id = s.products_id order by s.specials_sort , pd.products_name";
    $specials_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $specials_query_raw, $specials_query_numrows);
    $specials_query = tep_db_query($specials_query_raw);
    while ($specials = tep_db_fetch_array($specials_query)) {
      if ((!isset($_GET['sID']) || (isset($_GET['sID']) && ($_GET['sID'] == $specials['specials_id']))) && !isset($sInfo)) {
        $products_query = tep_db_query("select products_image from " . TABLE_PRODUCTS . " where products_id = '" . (int)$specials['products_id'] . "'");
        $products = tep_db_fetch_array($products_query);
        $sInfo_array = array_merge($specials, $products);
        $sInfo = new objectInfo($sInfo_array);
      }

	  $selected = (isset($sInfo) && is_object($sInfo) && ($specials['specials_id'] == $sInfo->specials_id))? true : false;
	  $col_selected = ($selected) ? ' selected' : '';
      if ($selected) {
        echo '       <tr id="crow_'.$specials['specials_id'].'" rowid="'.$specials['specials_id'].'" class="toprow table-row dark selected" >' . "\n";
        $onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page'] . '&sID=' . $sInfo->specials_id . '&action=edit') . '\'"';
      } else {
        echo '    <tr class="toprow table-row dark" id="crow_'.$specials['specials_id'].'" rowid="'.$specials['specials_id'].'">' . "\n";
        $onclick  = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page'] . '&sID=' . $specials['specials_id']) . '\'"';
      }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo $specials['products_name']; ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>" <?php echo $onclick;?>><span class="oldPrice"><strike><?php echo $currencies->format($specials['products_price']); ?></strike></span> <span class="specialPrice"><?php echo $currencies->format($specials['specials_new_products_price']); ?></span></td>
                <td class="setflag table-col dark text-right<?php echo $col_selected; ?>">
				<?php
					  $ajax_link = tep_href_link(FILENAME_SPECIALS);
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=0&id='.$specials['specials_id'].'\', '.$specials['specials_id'].',0 )" '.(($specials['status'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=1&id='.$specials['specials_id'].'\', '.$specials['specials_id'].',1  )" '.(($specials['status'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
				?>



				</td>
				<td class="table-col dark text-right<?php echo $col_selected; ?>"><i class="fa fa-arrows fa-lg text-success"></i></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if (isset($sInfo) && is_object($sInfo) && ($specials['specials_id'] == $sInfo->specials_id)) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page'] . '&sID=' . $specials['specials_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
      </tr>
<?php
    }
?>
              </tbody>
			</table>
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $specials_split->display_count($specials_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_SPECIALS); ?></td>
                    <td class="smallText" align="right"><?php echo $specials_split->display_links($specials_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
<?php
  if (empty($action)) {
?>
                 <!-- <tr>
                    <td colspan="2" align="right"><?php echo '<a class="btn btn-success btn-sm mt-2 mb-2" data-toggle="modal" data-target="#myModal" style="color:#fff;">New Product</a>'; ?></td>
                  </tr>-->
<?php
  }
?>
                </table>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'delete':
      $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_SPECIALS );

      $contents[] = array('form' => tep_draw_form('specials', FILENAME_SPECIALS, 'page=' . $_GET['page'] . '&sID=' . $sInfo->specials_id . '&action=deleteconfirm'));
      $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_INFO_DELETE_INTRO .'<br><b>' . $sInfo->products_name . '</b></p></div></div></div>');
      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
					  <button class="btn btn-danger btn-sm mb-2 mt-2 btn-delete" type="submit">' . IMAGE_DELETE . '</button>
					  <a class="btn btn-grey btn-sm mb-2 mt-2 btn-cancel" href="' . tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page'] . '&sID=' . $sInfo->specials_id) . '">' . IMAGE_CANCEL . '</a>
					  </div>');
      break;
    default:
      if (isset($sInfo) && is_object($sInfo)) {
        $heading[] = array('text' => $sInfo->products_name);

        $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
        				<a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="editSpecials('.$sInfo->specials_id.')" style="color:#fff;">' . IMAGE_EDIT . '</a>
        				<a class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page'] . '&sID=' . $sInfo->specials_id . '&action=delete') . '">' . IMAGE_DELETE . '</a></div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_DATE_ADDED . ' <span class="sidebar-title ml-2">' . tep_date_short($sInfo->specials_date_added) . '</span></div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_LAST_MODIFIED . ' <span class="sidebar-title ml-2">' . tep_date_short($sInfo->specials_last_modified) . '</span></div>');
        $contents[] = array('align' => 'center', 'text' => '<div class="sidebar-img mt-3 well ml-4 mr-4 mb-1">' . tep_info_image($sInfo->products_image, $sInfo->products_name, 100, 100).'</span></div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_ORIGINAL_PRICE . '<span class="sidebar-title ml-2">' . $currencies->format($sInfo->products_price) . '</div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_NEW_PRICE . '<span class="sidebar-title ml-2">' . $currencies->format($sInfo->specials_new_products_price) . '</div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_PERCENTAGE . '<span class="sidebar-title ml-2">' . number_format(100 - (($sInfo->specials_new_products_price / $sInfo->products_price) * 100)) . '%</span></div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_EXPIRES_DATE . '<span class="sidebar-title ml-2">' . tep_date_short($sInfo->expires_date) . '</span></div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_STATUS_CHANGE . '<span class="sidebar-title ml-2">' . tep_date_short($sInfo->date_status_change) . '</span></div>');
      }
      break;
  }
	  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
		$box = new box;
		echo $box->showSidebar($heading, $contents);
	  }
}


?>

          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
          <div class="row mt-2">
          <div class="col-md-12 col-xl-12 pb-3 dark panel-left rounded">
          <table>
          <?php
            // RCI code start
            echo $cre_RCI->get('customersgroups', 'bottom');
            // RCI code eof
          ?>
          </table>
        </div>
        </div>
  </div>
</div>
<!-- body_eof //-->
<script type="text/javascript" src="includes/javascript/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<script src="includes/javascript/jquery.autocomplete_pos.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="includes/javascript/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery.autocomplete.css" />
<style>
.ac_results { width: 462px !important; }
.ac_results ul{max-height:280px !important;}
</style>
<script>

$(document).ready(function() {
//Code for the Auto Complete for pos
   $("#special_data_info").autocomplete("autocomplete_special_search.php", {
	  selectFirst: true,
	  fun: 'selectCurrent2'
   });

	$('.iframe-btn').fancybox({
	'width'		: 900,
	'height'	: 600,
	'type'		: 'iframe',
	'autoScale'    	: false,
	'autoSize':     false
	});
});

function specialProduct(products_id)
{
//alert(products_id);
params = 'pID='+products_id;
$('.ac_results').css('display', 'none');

	$.ajax({
		type:'post',
		data: params,
		url: '<?php echo tep_href_link("ajax_common.php","action=add_ajax_special_product");?>',
		success:function(retval){
		//alert(retval);
         $('#replacediv').html(retval);
			   $("#special_data_info").autocomplete("autocomplete_product_search.php", {
				  selectFirst: true,
				  fun: 'selectCurrent2',
			   });

       }
	});
}

function updateSortOrder(){
     var selectedData = new Array();
     $('.row_special_position .toprow').each(function() {
	 selectedData.push($(this).attr("rowid"));
     });
    //alert(selectedData);
     updatespecialSortOrder(selectedData);

}
$( ".row_special_position" ).sortable({
 delay: 150
});
function updatespecialSortOrder(data) {
      var requestURL = '<?php echo tep_href_link("ajax_common.php", "action=setspecialsorting");?>';
        $.ajax({
            url:requestURL,
            type:'post',
            data:{position:data},
            success:function(response){
             $.gritter.add({ text:'Sort order updated successfully!', sticky:false,time:"2500"});
            }
        })
}
/***** modal edit in special ****/
  function editSpecials(id) {
	var params = {
		buttons: false,
		loadingHtml: '<span class="fa fa-circle-o-notch fa-spin fa-3x text-primary"></span><span class="h4">Loading</span>',
		title: 'Edit Specials Product',
		url: './ajax/modal_edit_specials_products.php?action=edit&sID='+id,
	};

	return eModal.ajax(params);
}

</script>
<script src="assets/plugins/eModal/eModal.min.js"></script>
<script type="text/javascript">

$('#calender').datepicker({
	todayHighlight: true
});


$(function () {
	$('#calender').datepicker({
	todayHighlight: true
	});
});
</script>
<style>
.oldPrice {color:red;}
</style>

<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
 <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header"  style="display:block ruby;">
          <h4 class="modal-title">New Special Products</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
              <form name="new_special" <?php echo 'action="' . tep_href_link(FILENAME_SPECIALS, tep_get_all_get_params(array('action', 'info', 'sID')) . 'action=insert', 'NONSSL') . '"'; ?> method="post" data-parsley-validate onSubmit="submitform()">
		       <?php if ($form_action == 'new') echo tep_draw_hidden_field('specials_id', $_GET['sID']); ?>
		 		  <div class="form-group row mb-3 mt-3">
		 			<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo "Product:"; ?></label>
		 			<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
		 			   <?php // echo (isset($sInfo->products_name)) ? $sInfo->products_name . ' <small>(' . $currencies->format($sInfo->products_price) . ')</small>' : tep_draw_products_pull_down('products_id', 'style="font-size:10px"', $on_special_array); echo tep_draw_hidden_field('products_price', (isset($sInfo->products_price) ? $sInfo->products_price : '')); ?>
		                <div id="replacediv" class=" well" style="padding:0px;margin-bottom: 0px;border: none;">
		 				  <div id="search_section">
		 					 <?php echo ((isset($sInfo->products_name))? '<input type="text" name="products_id" class="form-control" id="special_data_info" value="" >' :'<input type="text" class="form-control custom-search cust-input ac_input" id="special_data_info" autocomplete="off" name="products_id" value="" height="50px;"  placeholder="Search Product...." style="">'); ?>
		 					 <?php  if (isset($sInfo->products_price)) {
		 						echo tep_draw_hidden_field('products_price', $sInfo->products_price);
		 					  } else {
		 						echo tep_draw_hidden_field('products_price', 0);
		 					  } ?>
		 				  </div>
		 			 </div>
		 			</div>
		 		 </div>
		 		  <div class="form-group row mb-3 mt-3">
		 			<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_SPECIALS_SPECIAL_PRICE; ?></label>
		 			<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input"><?php echo tep_draw_input_field('specials_price', '', 'required="required"'); ?></div>
		 		  </div>
		 		  <div class="form-group row mb-3 mt-3">
		 			<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_SPECIALS_EXPIRES_DATE; ?></label>
		 			<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input"><?php echo tep_draw_input_field('expires_date',(($sInfo->expires_date == '' || $sInfo->expires_date == '0000-00-00 00:00:00')?'':date('m/d/Y', strtotime($sInfo->expires_date))),'class="calender form-control" id="calender" data-date-format="mm/dd/yyyy"'); ?></div>
		 		  </div>
		 		 <?php
		 		  // RCI for inserting the extra fields
		 		  echo $cre_RCI->get('specials', 'bottominsideform');
		 		 ?>
		 		  <div class="form-group row mb-3 mt-3">
		 			<div class="col-xs-12 col-md-12 col-lg-12 p-r-10 meta-input text-right"><a class="btn btn-default btn-sm mt-2 mb-2 mr-2" data-dismiss="modal" href="<?php echo tep_href_link(FILENAME_SPECIALS, 'page=' . $_GET['page'] . (isset($_GET['sID']) ? '&sID=' . $_GET['sID'] : '')); ?>">Close</a><?php echo (($form_action == 'insert') ? '<button class="btn btn-success btn-sm" type="submit">' . IMAGE_INSERT . '</button>' : '<button class="btn btn-success btn-sm" type="submit">' . IMAGE_UPDATE . '</button>'); ?></div>
		 		  </div>
		 		  <div class="sidebar-title mt-2">
		 		    <div class="row">
		 		  	  <div class="col p-0 mt-3 ml-2 mr-2">
		 			    <div class="note note-warning m-0">
		 				  <p class="mb-0 mt-0 f-w-400"><?php echo TEXT_SPECIALS_PRICE_TIP; ?></p>
		 			    </div>
		 			  </div>
		 		    </div>
		 		  </div>

           </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
        </div>

      </div>
    </div>
  </div>

