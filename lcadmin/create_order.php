<?php
/*
  $Id: create_order.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = isset($_GET['action'])?$_GET['action']:'';
if($action != '')
{
	switch($action) {
		case'process':
			if (count($_POST) <= 2) {
			   tep_redirect(tep_href_link(FILENAME_CREATE_ORDER, '', 'SSL'));
			 }
			include('create_order_process.php');
			break;
	}
}


$query = tep_db_query("select customers_id, customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " ORDER BY customers_lastname DESC");
$result = $query;

if (tep_db_num_rows($result) > 0) {
  // Query Successful
  $SelectCustomerBox = "<select onchange='selectCustomer(this.value);' class='form-control' name='Customer'><option value=''>". BUTTON_TEXT_CHOOSE_CUST . "</option>\n";
  while($db_Row = tep_db_fetch_array($result)){
    $SelectCustomerBox .= "<option value='" . $db_Row["customers_id"] . "'";
    if(isset($_GET['Customer']) and $db_Row["customers_id"]==$_GET['Customer']) $SelectCustomerBox .= ' selected="selected" ';
    $SelectCustomerBox .= ">" . $db_Row["customers_lastname"] . " , " . $db_Row["customers_firstname"] . "</option>\n";
  }
  $SelectCustomerBox .= "</select>\n";
}
//newcode below
$query = tep_db_query("select code, value from " . TABLE_CURRENCIES . " ORDER BY code");
$result = $query;

if (tep_db_num_rows($result) > 0) {
  // Query Successful
  $SelectCurrencyBox = '<select class="form-control" name="Currency"><option value="" selected="selected">' . TEXT_SELECT_CURRENCY . "</option>\n";
  while($db_Row = tep_db_fetch_array($result)) {
    $SelectCurrencyBox .= "<option value='" . $db_Row["code"] . " , " . $db_Row["value"] . "'";
    $SelectCurrencyBox .= ">" . $db_Row["code"] . "</option>\n";
  }
  $SelectCurrencyBox .= "</select>\n";
}
if(isset($_GET['Customer'])) {
  //$account_query = tep_db_query("select * from " . TABLE_CUSTOMERS . " where customers_id = '" . $_GET['Customer'] . "'");
  $account_query = tep_db_query("select c.*,ab.entry_telephone as customers_telephone from " . TABLE_CUSTOMERS . " c, ".TABLE_ADDRESS_BOOK." ab where c.customers_id = '" . $_GET['Customer'] . "' and c.customers_id = ab.customers_id and ab.address_book_id = c.customers_default_address_id");

  $account = tep_db_fetch_array($account_query);
  $customer = $account['customers_id'];
  $address_query = tep_db_query("select ab.* from " . TABLE_CUSTOMERS . " c, ".TABLE_ADDRESS_BOOK." ab where c.customers_id = '" . $_GET['Customer'] . "' and c.customers_id = ab.customers_id and ab.address_book_id = c.customers_default_address_id");
  $address = tep_db_fetch_array($address_query);
}

include(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<style>
 #customer_data_search .ac_results{left:unset;top:unset;}
</style>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-languages" class="table-languages">
        <div class="row">
          <div class="col-5 dark panel-left rounded-left pl-4 pr-4">
            <?php
            echo tep_draw_form('select_customer', FILENAME_CREATE_ORDER, tep_get_all_get_params(array('action','select_customer')) . '', 'get', 'class="form-inline"', 'SSL') ;
              if (isset($_GET[tep_session_name()])) {
                echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
              }
              ?>
              <div class="form-group mt-3">
                <label class="sidebar-text mr-2" for="Customer"><?php echo TEXT_SELECT_CUST; ?></label>
                <?php //echo $SelectCustomerBox;
                ?>
              <div id="search_section">
               <input type="text" class="form-control custom-search cust-input ac_input" id="customer_data_search" autocomplete="off" name="cname" value="" height="50px;" placeholder="Search Customer...." style="margin-top:10px;">
              </div>
              </div>
            </form>
            <?php
            echo tep_draw_form('select_customer', FILENAME_CREATE_ORDER, tep_get_all_get_params(array('action','select_customer', 'Customer')) . '', 'get', 'class="form-inline"', 'SSL') ;
              if (isset($_GET[tep_session_name()])) {
                echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
              }
              ?>
              <div class="form-group mt-3">
                <label class="sidebar-text mr-2" for="Customer"><?php echo TEXT_OR_BY; ?></label>
                <input type="text" id="customerID" name="Customer" class="form-control ml-2" style="margin-right:5px;">
                <button type="submit" class="btn btn-primary"><?php echo IMAGE_SELECT; ?></button>
              </div>
            </form>

          </div>
          <div class="col-7 dark panel-right rounded-right pl-4 pr-4">
            <?php
            echo tep_draw_form('account_edit', FILENAME_CREATE_ORDER, tep_get_all_get_params(array('action','create_order')) . 'action=process', 'post', 'onSubmit="return check_form(account_edit);"', 'SSL');
              echo (isset($account['customers_id']) ? tep_draw_hidden_field('customers_id', $account['customers_id']) : '');
              ?>
              <div class="customerDetails mt-3">
                <?php require(DIR_WS_MODULES . 'create_order_details.php'); ?>
              </div>

              <div class="text-center mt-3 mb-3" role="group">
                <button class="btn btn-default btn-sm mr-1" onclick="history.go(-1); return false;"><?php echo IMAGE_CANCEL; ?></button>
                <button class="btn btn-success btn-sm" type="submit"><?php echo IMAGE_BUTTON_CONFIRM; ?></button>
              </div>

            </form>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<script type="text/javascript" src="includes/javascript/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<script src="includes/javascript/jquery.autocomplete_pos.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="includes/javascript/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery.autocomplete.css" />
<script>
$(document).ready(function() {
  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small',
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  });
});

function selectCustomer(id) {
  self.location='<?php echo tep_href_link(FILENAME_CREATE_ORDER, 'p=1'); ?>&Customer='+id;
}
</script>

<script>
$(document).ready(function() {
//Code for the Auto Complete for pos
   $("#customer_data_search").autocomplete("autocomplete_customer_search.php", {
	  selectFirst: true,
	  fun: 'selectCurrent2'
   });

	$('.iframe-btn').fancybox({
	'width'		: 900,
	'height'	: 600,
	'type'		: 'iframe',
	'autoScale'    	: false,
	'autoSize':     false
	});
});
</script>

<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
