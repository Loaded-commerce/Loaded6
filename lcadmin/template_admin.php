<?php
/*
  $Id: template_admin.php,v 1.1.1.1 2004/03/04 23:39:01 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
//300

  require('includes/application_top.php');

  // #CP - local dir to the template directory where you are uploading the company logo
  $template_query = tep_db_query("select configuration_id, configuration_title, configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'DEFAULT_TEMPLATE'");
  $template = tep_db_fetch_array($template_query);
  $CURR_TEMPLATE = $template['configuration_value'] . '/';

  // get the configuration group ID
  $cfg_group_query = tep_db_query("select configuration_group_id from " . TABLE_CONFIGURATION_GROUP . " where  configuration_group_title = '" . tep_db_input('Design and layout') . "'");
  $cfg_group = tep_db_fetch_array($cfg_group_query);
  $gID = $cfg_group['configuration_group_id'];

  $upload_fs_dir = DIR_FS_TEMPLATES.$CURR_TEMPLATE.DIR_WS_IMAGES;
  $upload_ws_dir = DIR_WS_CATALOG_TEMPLATES.$CURR_TEMPLATE.DIR_WS_IMAGES;
  // #CP

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {
      case 'save':
        $configuration_value = tep_db_prepare_input($_POST['configuration_value']);
        $cID = tep_db_prepare_input($_GET['cID']);

        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");

        tep_redirect(tep_href_link(FILENAME_TEMPLATE_ADMIN, 'cID=' . $cID));
        break;

      case 'save1':
        $configuration_value = tep_db_prepare_input($_POST['configuration_value']);
        $cID = tep_db_prepare_input($_GET['cID']);

        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");

        tep_redirect(tep_href_link(FILENAME_TEMPLATE_ADMIN, 'gID=' . $_GET['gID1'] . '&cID=' . $cID));
        break;

// #CP - supporting functions to upload company logo to template images directory
    }
// #CP
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE ?> <small>(<?php echo TEXT_HEADER_EXPLAIN ; ?>)</small></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('currencies') > 0) {
      echo $messageStack->output('currencies'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-templateadmin" class="table-templateadmin">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CONFIGURATION_TITLE; ?></th>
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CONFIGURATION_VALUE; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $configuration_query = tep_db_query("select configuration_id, configuration_title, configuration_value, use_function from " . TABLE_CONFIGURATION . " where configuration_group_id = '" . (int)$gID . "' order by sort_order");
                while ($configuration = tep_db_fetch_array($configuration_query)) {
                  if (tep_not_null($configuration['use_function'])) {
                    $use_function = $configuration['use_function'];
                    if (preg_match('/->/', $use_function)) {
                      $class_method = explode('->', $use_function);
                      if (!is_object(${$class_method[0]})) {
                        include(DIR_WS_CLASSES . $class_method[0] . '.php');
                        ${$class_method[0]} = new $class_method[0]();
                      }
                      $cfgValue = tep_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
                    } else {
                      $cfgValue = tep_call_function($use_function, $configuration['configuration_value']);
                    }
                  } else {
                    $cfgValue = $configuration['configuration_value'];
                  }

                  if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $configuration['configuration_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
                    $cfg_extra_query = tep_db_query("select configuration_key, configuration_description, date_added, last_modified, use_function, set_function from " . TABLE_CONFIGURATION . " where configuration_id = '" . (int)$configuration['configuration_id'] . "'");
                    $cfg_extra = tep_db_fetch_array($cfg_extra_query);

                    $cInfo_array = array_merge($configuration, $cfg_extra);
                    $cInfo = new objectInfo($cInfo_array);
                  }

              	  $selected = ((isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id)) ? ' selected' : '';
                  if ($selected) {
                    if($cInfo->set_function == 'file_upload'){
                    echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_TEMPLATE_ADMIN, 'cID=' . $cInfo->configuration_id . '&action=upload') . '\'">' . "\n";
                    } else {
                    echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_TEMPLATE_ADMIN, 'cID=' . $cInfo->configuration_id . '&action=edit') . '\'">' . "\n";
                    }
                  } else {
                    echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_TEMPLATE_ADMIN, 'cID=' . $configuration['configuration_id']) . '\'">' . "\n";
                  }
                  $col_selected = ($selected) ? ' selected' : '';
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $configuration['configuration_title']; ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo htmlspecialchars($cfgValue); ?></td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank"><?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $currency['currencies_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                  </tr>
                  <?php
                }
                ?>
              </tbody>                  
            </table>
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
    			  $heading = array();
    			  $contents = array();

    			  switch ($action) {
    				case 'edit':
    				  $heading[] = array('text' => $cInfo->configuration_title);
    				  if ($cInfo->set_function) {
    					  eval('$value_field = ' . $cInfo->set_function . '"' . htmlspecialchars($cInfo->configuration_value) . '");');
    				  } else {
    					  $value_field = tep_draw_input_field('configuration_value', $cInfo->configuration_value);
    				  }
    				  $contents[] = array('form' => tep_draw_form('configuration', FILENAME_TEMPLATE_ADMIN, 'cID=' . $cInfo->configuration_id . '&action=save'));
              $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 mb-2 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_EDIT_INTRO . '</p></div></div></div>');                                           
    				  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . $cInfo->configuration_description . '</div><span class="sidebar-title ml-2">' . $value_field.'</span>');
    				  $contents[] = array('align' => 'center', 'text' => '<div class="sidebar-text mt-3 mb-3"><button class="btn btn-success btn-sm btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_TEMPLATE_ADMIN, 'cID=' . $cInfo->configuration_id) . '">' .  IMAGE_CANCEL . '</a></div>');
    				  break;
    				default:
    				  if (isset($cInfo) && is_object($cInfo)) {
    					$heading[] = array('text' => $cInfo->configuration_title );
    				  if ($cInfo->set_function == 'file_upload') {
    					  $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-success btn-sm mt-2 mb-2 btn-sidebar btn-edit" href="' . tep_href_link(FILENAME_TEMPLATE_ADMIN, 'cID=' . $cInfo->configuration_id . '&action=upload') . '">' . IMAGE_EDIT . '</a><div>');
    					  $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-sidebar btn-edit" type="submit">'.IMAGE_EDIT.'</button></div>');
    				  } else {
    					  $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-success btn-sm mt-2 mb-2 btn-sidebar btn-edit" href="' . tep_href_link(FILENAME_TEMPLATE_ADMIN, 'cID=' . $cInfo->configuration_id . '&action=edit') . '">' . IMAGE_EDIT . '</a><div>');
    				  }
              $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 mb-2 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . $cInfo->configuration_description . '</p></div></div></div>');                                           
    					$contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->date_added) . '</span></div>');
    					if (tep_not_null(tep_date_short($cInfo->last_modified))) $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->last_modified) . '</span></div>');
    				  }

    				  break;
    			  }

            // use $box->showSidebar()
            if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
              $box = new box;
              echo $box->showSidebar($heading, $contents);
            }
            ?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>