<?php
/*
  $Id: coupon_admin.php,v 1.2 2004/03/09 17:56:06 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
$page = (isset($_GET['page']) ? $_GET['page'] : '');
if ($_GET['action'] == 'send_email_to_user' && ($_POST['customers_email_address']) && (!$_POST['back_x'])) {
  switch ($_POST['customers_email_address']) {
    case '***':
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS);
      $mail_sent_to = TEXT_ALL_CUSTOMERS;
      break;
    case '**D':
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_newsletter = '1'");
      $mail_sent_to = TEXT_NEWSLETTER_CUSTOMERS;
      break;
    default:
      $customers_email_address = strtolower(tep_db_prepare_input($_POST['customers_email_address']));
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where lower(customers_email_address) = '" . tep_db_input($customers_email_address) . "'");
      $mail_sent_to = $_POST['customers_email_address'];
      break;
  }
  $coupon_query = tep_db_query("select coupon_code from " . TABLE_COUPONS . " where coupon_id = '" . (int)$_GET['cid'] . "'");
  $coupon_result = tep_db_fetch_array($coupon_query);
  $coupon_name_query = tep_db_query("select coupon_name from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" . (int)$_GET['cid'] . "' and language_id = '" . $languages_id . "'");
  $coupon_name = tep_db_fetch_array($coupon_name_query);
  $from = tep_db_prepare_input($_POST['from']);
  $subject = tep_db_prepare_input($_POST['subject']);
  while ($mail = tep_db_fetch_array($mail_query)) {
    if (EMAIL_USE_HTML == 'false') {
      $message = tep_db_encoder(tep_db_prepare_input($_POST['message'])) . "\n\n";
      $message .= sprintf(TEXT_TO_REDEEM_TEXT,HTTP_SERVER  . DIR_WS_CATALOG . 'gv_redeem.php' . '?gv_no='. $coupon_result['coupon_code']);
      $message .= TEXT_WHICH_IS . $coupon_result['coupon_code'] . TEXT_IN_CASE . "\n\n";
      $message .= TEXT_OR_VISIT . HTTP_SERVER  . DIR_WS_CATALOG  . TEXT_ENTER_CODE;
      $message .= TEXT_VOUCHER_IS . $coupon_result['coupon_code'] . "\n\n";
      $message .= TEXT_TO_REDEEM1 ;
      $message .= TEXT_REMEMBER . "\n";
    } else {
      $message = tep_db_encoder(tep_db_prepare_input($_POST['message'])) . "\n\n";
      $message .= sprintf(TEXT_TO_REDEEM_TEXT,'<a href="' . HTTP_SERVER  . DIR_WS_CATALOG . 'gv_redeem.php' . '?gv_no='. $coupon_result['coupon_code'] . '">' . HTTP_SERVER  . DIR_WS_CATALOG . 'gv_redeem.php' . '?gv_no='. $coupon_result['coupon_code'] . '</a>');
      $message .= TEXT_WHICH_IS . '<font color="red"><strong>' . $coupon_result['coupon_code'] . '</strong></font>' . TEXT_IN_CASE . "\n\n";
      $message .= TEXT_OR_VISIT . '<a href="' . HTTP_SERVER  . DIR_WS_CATALOG  . TEXT_ENTER_CODE . '">' . STORE_NAME . '</a>';
      $message .= ' ' . TEXT_VOUCHER_IS . '<strong><u>' . $coupon_result['coupon_code'] . '</u></strong>' . "\n\n";
      $message .= TEXT_TO_REDEEM1 ;
      $message .= TEXT_REMEMBER . "\n";
    }

    if (EMAIL_USE_HTML == "false") {
      $email_content = $message;
    } else {
      $email_content['text'] = strip_tags($message);
      $email_content['html'] = nl2br($message);
    }
	tep_mail($mail['customers_firstname'] . ' ' . $mail['customers_lastname'], $mail['customers_email_address'], $subject, $email_content, $from, STORE_OWNER_EMAIL_ADDRESS);

    // create the coupon email entry
    $insert_query = tep_db_query("insert into " . TABLE_COUPON_EMAIL_TRACK . " (coupon_id, customer_id_sent, sent_firstname, emailed_to, date_sent) values ('" . (int)$_GET['cid'] ."', '0', 'Admin', '" . $mail['customers_email_address'] . "', now() )");
  }
  tep_redirect(tep_href_link(FILENAME_COUPON_ADMIN, 'mail_sent_to=' . urlencode($mail_sent_to)));
}
if ( (isset($_GET['action']) && $_GET['action'] == 'preview_email') && (!$_POST['customers_email_address']) ) {
  $_GET['action'] = 'email';
  $messageStack->add('search', ERROR_NO_CUSTOMER_SELECTED, 'error');
}
if ( isset($_GET['mail_sent_to']) ) {
  $messageStack->add('search', sprintf(NOTICE_EMAIL_SENT_TO, $_GET['mail_sent_to']), 'success');
}
$coupon_id = ((isset($_GET['cid'])) ? tep_db_prepare_input($_GET['cid']) : '');
if ( isset($_GET['action']) ) {
  switch ($_GET['action']) {
    case 'setflag':
      if ( ($_GET['flag'] == 'N') || ($_GET['flag'] == 'Y') ) {
        if (isset($_GET['cid'])) {
          tep_set_coupon_status($coupon_id, $_GET['flag']);
        }
      }
      tep_redirect(tep_href_link(FILENAME_COUPON_ADMIN, '&cid=' . $_GET['cid']));
      break;
    case 'confirmdelete':
      //$delete_query=tep_db_query("update " . TABLE_COUPONS . " set coupon_active = 'N' where coupon_id='".(int)$_GET['cid']."'");
      $delete_query = tep_db_query("delete from " . TABLE_COUPONS . " where coupon_id='".(int)$_GET['cid']."'");
      break;
     case 'update':
      // get all _POST and validate
      $_POST['coupon_code'] = trim($_POST['coupon_code']);
      $languages = tep_get_languages();
      for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
        $language_id = $languages[$i]['id'];
        if ($_POST['coupon_name'][$language_id]) $_POST['coupon_name'][$language_id] = trim($_POST['coupon_name'][$language_id]);
        if ($_POST['coupon_desc'][$language_id]) $_POST['coupon_desc'][$language_id] = trim($_POST['coupon_desc'][$language_id]);
      }
      $_POST['coupon_amount'] = str_replace($currencies->currencies[DEFAULT_CURRENCY]['symbol_left'], '', $_POST['coupon_amount']);
      $_POST['coupon_amount'] = str_replace($currencies->currencies[DEFAULT_CURRENCY]['symbol_right'], '', $_POST['coupon_amount']);
      $_POST['coupon_amount'] = trim($_POST['coupon_amount']);
      $update_errors = 0;
      if (!$_POST['coupon_name']) {
        $update_errors = 1;
        $messageStack->add('search', ERROR_NO_COUPON_NAME, 'error');
      }
      if ((!isset($_POST['coupon_amount'])) && (!isset($_POST['coupon_free_ship']))) {
        $update_errors = 1;
        $messageStack->add('search', ERROR_NO_COUPON_AMOUNT, 'error');
      }
      if (isset($_POST['coupon_code'])) $coupon_code = $_POST['coupon_code'];
      if ($coupon_code == '') $coupon_code = create_coupon_code();
      $query1 = tep_db_query("select coupon_code from " . TABLE_COUPONS . " where coupon_code = '" . tep_db_input(tep_db_prepare_input($coupon_code)) . "'");
      if (tep_db_num_rows($query1) && $_POST['coupon_code'] && $_GET['oldaction'] != 'voucheredit')  {
        $update_errors = 1;
        $messageStack->add('search', ERROR_COUPON_EXISTS, 'error');
      }
      if ($update_errors != 0) {
        $_GET['action'] = 'new';
      } else {
        $_GET['action'] = 'update_preview';
      }
      break;
    case 'update_confirm':
       if(isset($_POST['Back']) ){
        $coupon_min_order = (($_POST['coupon_min_order'] == round($_POST['coupon_min_order'])) ? number_format($_POST['coupon_min_order']) : number_format($_POST['coupon_min_order'],2));
        $coupon_amount = (($_POST['coupon_amount'] == round($_POST['coupon_amount'])) ? number_format($_POST['coupon_amount']) : number_format($_POST['coupon_amount'],2));

            $_GET['action'] = 'new';
      } else {
      if ( isset($_POST['back_x']) || isset($_POST['back_y']) ) {
        $_GET['action'] = 'new';
      } else {
        $coupon_type = "F";
        $coupon_amount = $_POST['coupon_amount'];
        if (substr($_POST['coupon_amount'], -1) == '%') $coupon_type='P';
        if ($_POST['coupon_free_ship']) {
          $coupon_type = 'S';
          $coupon_amount = 0;
        }
        
        $coupon_status = (isset($_POST['coupon_status']) && $_POST['coupon_status'] == 'on')?'Y':'N';
        $sql_data_array = array('coupon_active' => $coupon_status,
                                'coupon_code' => tep_db_prepare_input($_POST['coupon_code']),
                                /*******************/
                                    'coupon_sale_exclude' => tep_db_prepare_input($_POST['coupon_sale_exclude']),
                                /*******************/
                                'coupon_amount' => tep_db_prepare_input($coupon_amount),
                                'coupon_type' => tep_db_prepare_input($coupon_type),
                                'uses_per_coupon' => tep_db_prepare_input($_POST['coupon_uses_coupon']),
                                'uses_per_user' => tep_db_prepare_input($_POST['coupon_uses_user']),
                                'coupon_minimum_order' => tep_db_prepare_input($_POST['coupon_min_order']),
                                'restrict_to_products' => tep_db_prepare_input($_POST['coupon_products']),
                                'restrict_to_categories' => tep_db_prepare_input($_POST['coupon_categories']),
                                'coupon_start_date' => (($_POST['coupon_startdate'] == '')?'0000-00-00':(date("Y-m-d", strtotime($_POST['coupon_startdate'])))),
                                'coupon_expire_date' => (($_POST['coupon_finishdate'] == '')?'0000-00-00':(date("Y-m-d", strtotime($_POST['coupon_finishdate'])))),
                                'date_modified' => 'now()');
        $languages = tep_get_languages();
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $language_id = $languages[$i]['id'];
          $sql_data_marray[$i] = array('coupon_name' => htmlspecialchars(tep_db_prepare_input($_POST['coupon_name'][$language_id])),
                                 'coupon_description' => htmlspecialchars(tep_db_prepare_input($_POST['coupon_desc'][$language_id]))
                                 );
        }
        if ($_GET['oldaction']=='voucheredit') {
          $insert_id = (int)$_GET['cid'];
          tep_db_perform(TABLE_COUPONS, $sql_data_array, 'update', "coupon_id='" . (int)$_GET['cid']."'");
          for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $language_id = $languages[$i]['id'];
            $update = tep_db_query("update " . TABLE_COUPONS_DESCRIPTION . " set coupon_name = '" . tep_db_input(tep_db_prepare_input($_POST['coupon_name'][$language_id])) . "', coupon_description = '" . tep_db_input(tep_db_prepare_input($_POST['coupon_desc'][$language_id])) . "' where coupon_id = '" . (int)$_GET['cid'] . "' and language_id = '" . $language_id . "'");
          }
        } else {
          $sql_data_array['date_created'] = ($_POST['date_created'] != '0') ? $_POST['date_created'] : 'now()';
          $query = tep_db_perform(TABLE_COUPONS, $sql_data_array);
          $insert_id = tep_db_insert_id();
          for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
            $language_id = $languages[$i]['id'];
            $sql_data_marray[$i]['coupon_id'] = $insert_id;
            $sql_data_marray[$i]['language_id'] = $language_id;
            tep_db_perform(TABLE_COUPONS_DESCRIPTION, $sql_data_marray[$i]);
          }
        }
        
		$mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
		if ($mode == 'save') {
			tep_redirect(tep_href_link(FILENAME_COUPON_ADMIN, 'cid=' . $insert_id));
		} else {  // save & stay
			tep_redirect(tep_href_link(FILENAME_COUPON_ADMIN, 'action=voucheredit&cid=' . $insert_id ));
		}

        
      }
    }
  } // end switch
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');

echo tep_load_html_editor();
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <?php
    if ($messageStack->size('couponadmin') > 0) {
      echo $messageStack->output('couponadmin');
    }
    ?>

    <?php
    switch ($_GET['action']) {
      case 'report':
        ?>
    <!-- begin panel -->
				<table class="table table-hover w-100 mt-2">
				  <thead>
				   <tr class="th-row">
					 <th scope="col" class="th-col dark text-left"><?php echo CUSTOMER_ID; ?></td>
					 <th scope="col" class="th-col dark text-center"><?php echo CUSTOMER_NAME; ?></td>
					 <th scope="col" class="th-col dark text-center"><?php echo IP_ADDRESS; ?></td>
					 <th scope="col" class="th-col dark text-center"><?php echo REDEEM_DATE; ?></td>
					 <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                  </tr>
				  </thead>
				  <tbody>
                  <?php
                  $cc_query_raw = "select * from " . TABLE_COUPON_REDEEM_TRACK . " where coupon_id = '" . (int)$_GET['cid'] . "'";
                  $cc_query = tep_db_query($cc_query_raw);
                  $cc_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $cc_query_raw, $cc_query_numrows);
                  while ($cc_list = tep_db_fetch_array($cc_query)) {
                    $rows++;
                    if (strlen($rows) < 2) {
                      $rows = '0' . $rows;
                    }
                    if (((!$_GET['uid']) || (@$_GET['uid'] == $cc_list['unique_id'])) && (!$cInfo)) {
                      $cInfo = new objectInfo($cc_list);
                    }

					$selected = ((is_object($cInfo)) && ($cc_list['unique_id'] == $cInfo->unique_id))? true : false;
					$col_selected = ($selected) ? ' selected' : '';
                    if ($selected ) {
                      echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link('coupon_admin.php', tep_get_all_get_params(array('cid', 'action', 'uid')) . 'cid=' . $cInfo->coupon_id . '&action=report&uid=' . $cinfo->unique_id) . '\'">' . "\n";
                    } else {
                      echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link('coupon_admin.php', tep_get_all_get_params(array('cid', 'action', 'uid')) . 'cid=' . $cc_list['coupon_id'] . '&action=report&uid=' . $cc_list['unique_id']) . '\'">' . "\n";
                    }
                    $customer_query = tep_db_query("select customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " where customers_id = '" . $cc_list['customer_id'] . "'");
                    $customer = tep_db_fetch_array($customer_query);
                    ?>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $cc_list['customer_id']; ?></td>
                    <td class="table-col dark text-center<?php echo $col_selected; ?>"><?php echo $customer['customers_firstname'] . ' ' . $customer['customers_lastname']; ?></td>
                    <td class="table-col dark text-center<?php echo $col_selected; ?>"><?php echo $cc_list['redeem_ip']; ?></td>
                    <td class="table-col dark text-center<?php echo $col_selected; ?>"><?php echo tep_date_short($cc_list['redeem_date']); ?></td>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if ( (is_object($cInfo)) && ($cc_list['unique_id'] == $cInfo->unique_id) ) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_COUPON_ADMIN, 'page=' . $_GET['page'] . '&cid=' . $cc_list['coupon_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
                  </tr>
                  <?php
                  }
                  ?>
				  </tbody>
                </table>

          </div>
          <div class="col-md-4 col-xl-3 dark panel-right rounded-right">
            <?php
            $heading = array();
            $contents = array();
            $coupon_description_query = tep_db_query("select coupon_name from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" . (int)$_GET['cid'] . "' and language_id = '" . $languages_id . "'");
            $coupon_desc = tep_db_fetch_array($coupon_description_query);
            $count_customers = tep_db_query("select * from " . TABLE_COUPON_REDEEM_TRACK . " where coupon_id = '" . (int)$_GET['cid'] . "' and customer_id = '" . $cInfo->customer_id . "'");
            $heading[] = array('text' => '[' . $_GET['cid'] . ']' . COUPON_NAME . ' ' . $coupon_desc['coupon_name']);
            $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_REDEMPTIONS . '</div>');
            $contents[] = array('text' => '<div class="sidebar-text">'.TEXT_REDEMPTIONS_TOTAL . '= <span class="sidebar-title ml-2">' . tep_db_num_rows($cc_query) .'</span></div>');
            $contents[] = array('text' => '<div class="sidebar-text">'.TEXT_REDEMPTIONS_CUSTOMER . ': <span class="sidebar-title ml-2">' . tep_db_num_rows($count_customers) .'</span></div>');
            $contents[] = array('text' => '');

			  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
				$box = new box;
				echo $box->showSidebar($heading, $contents);
			  }
        break;

      case 'preview_email':
        $coupon_query = tep_db_query("select coupon_code from " .TABLE_COUPONS . " where coupon_id = '" . (int)$_GET['cid'] . "'");
        $coupon_result = tep_db_fetch_array($coupon_query);
        $coupon_name_query = tep_db_query("select coupon_name from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" . (int)$_GET['cid'] . "' and language_id = '" . $languages_id . "'");
        $coupon_name = tep_db_fetch_array($coupon_name_query);
        switch ($_POST['customers_email_address']) {
          case '***':
            $mail_sent_to = TEXT_ALL_CUSTOMERS;
            break;
          case '**D':
            $mail_sent_to = TEXT_NEWSLETTER_CUSTOMERS;
            break;
          default:
            $mail_sent_to = strtolower($_POST['customers_email_address']);
            break;
        }
        ?>
          <?php echo tep_draw_form('mail', FILENAME_COUPON_ADMIN, 'action=send_email_to_user&cid=' . $_GET['cid']); ?>
            <table border="0" width="100%" cellpadding="5" cellspacing="2">
              <tr>
                <td class="control-label main-text text-left"><b><?php echo TEXT_CUSTOMER; ?></b> <?php echo $mail_sent_to; ?></td>
              </tr>
              <tr>
                <td class="control-label main-text text-left"><b><?php echo TEXT_COUPON; ?></b> <?php echo tep_db_encoder(tep_db_prepare_input($coupon_name['coupon_name'])); ?></td>
              </tr>
              <tr>
                <td class="control-label main-text text-left"><b><?php echo TEXT_FROM; ?></b> <?php echo tep_db_encoder(tep_db_prepare_input($_POST['from'])); ?></td>
              </tr>
              <tr>
                <td class="control-label main-text text-left"><b><?php echo TEXT_SUBJECT; ?></b> <?php echo tep_db_encoder(tep_db_prepare_input($_POST['subject'])); ?></td>
              </tr>
              <tr>
                <td class="control-label main-text text-left"><?php echo tep_db_encoder(tep_db_prepare_input($_POST['message'])); ?></td>
              </tr>
			  <tr>
			    <td align="right"><?php echo '<a class="btn btn-default btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_COUPON_ADMIN) . '">' . IMAGE_CANCEL . '</a> <button class="btn btn-success btn-sm" type="submit">' . IMAGE_SEND_EMAIL . '</button>'; ?></td>
			  </tr>
			  <tr>
			    <td colspan="2" class="control-label main-text text-left">
				<?php
				if (EMAIL_USE_HTML == 'false'){
				  echo(TEXT_EMAIL_BUTTON_HTML);
				} else {
				  echo(TEXT_EMAIL_BUTTON_TEXT);
				}
				?>
			    </td>
			  </tr>
		    </table>
                  <?php
                  /* Re-Post all POST'ed variables */
                  reset($_POST);
				  foreach($_POST as $key => $value) {
                    if (!is_array($_POST[$key])) {
                      echo tep_draw_hidden_field($key, tep_db_encoder(tep_db_prepare_input($value))) . "\n";
                    }
                  }
                  ?>

                </form>
        <?php
        break;

      case 'email':
        $coupon_query = tep_db_query("select coupon_code from " . TABLE_COUPONS . " where coupon_id = '" . (int)$_GET['cid'] . "'");
        $coupon_result = tep_db_fetch_array($coupon_query);
        $coupon_name_query = tep_db_query("select coupon_name from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" . (int)$_GET['cid'] . "' and language_id = '" . $languages_id . "'");
        $coupon_name = tep_db_fetch_array($coupon_name_query);
        // load editor only if html email option is true
        if (EMAIL_USE_HTML == 'true'){
            echo tep_insert_html_editor('message','simple','400');
        }
        echo tep_draw_separator('pixel_trans.gif', '100%', '15');
        ?>
            <?php echo tep_draw_form('mail', FILENAME_COUPON_ADMIN, 'action=preview_email&cid='. $_GET['cid']); ?>
              <table border="0" cellpadding="5" cellspacing="0">
                <?php
                $customers = array();
                $customers[] = array('id' => '', 'text' => TEXT_SELECT_CUSTOMER);
                $customers[] = array('id' => '***', 'text' => TEXT_ALL_CUSTOMERS);
                $customers[] = array('id' => '**D', 'text' => TEXT_NEWSLETTER_CUSTOMERS);
                $mail_query = tep_db_query("select customers_email_address, customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " order by customers_lastname");
                while($customers_values = tep_db_fetch_array($mail_query)) {
                  $customers[] = array('id' => $customers_values['customers_email_address'],
                                       'text' => $customers_values['customers_lastname'] . ', ' . $customers_values['customers_firstname'] . ' (' . $customers_values['customers_email_address'] . ')');
                }
                ?>
                <tr>
                  <td class="control-label main-text mb-3"><?php echo TEXT_COUPON; ?>&nbsp;&nbsp;</td>
                  <td class="control-label main-text text-left  mb-3"><?php echo $coupon_name['coupon_name']; ?></td>
                </tr>
                <tr>
                  <td class="control-label main-text mb-3"><?php echo TEXT_CUSTOMER; ?>&nbsp;&nbsp;</td>
                  <td class="mb-3"><?php echo tep_draw_pull_down_menu('customers_email_address', $customers, $_GET['customer']);?></td>
                </tr>
                <tr>
                  <td class="control-label main-text"><?php echo TEXT_FROM; ?>&nbsp;&nbsp;</td>
                  <td><?php echo tep_draw_input_field('from', STORE_OWNER_EMAIL_ADDRESS); ?></td>
                </tr>
                <tr>
                  <td class="control-label main-text"><?php echo TEXT_SUBJECT; ?>&nbsp;&nbsp;</td>
                  <td><?php echo tep_draw_input_field('subject'); ?></td>
                </tr>
                <tr>
                  <td valign="top" class="control-label main-text"><?php echo TEXT_MESSAGE; ?></td>
                  <td class="main">
                    <?php echo tep_draw_textarea_field('message', 'hard', 60, 3, $message, 'style="width: 100%" class="form-control"'); ?>
                  </td>
                </tr>
				<tr>
				  <td colspan="2" class="text-center">
					<?php
					if (EMAIL_USE_HTML == 'false') {
						echo '<button class="btn btn-success btn-sm" type="submit" onClick="validate();return returnVal;">' . IMAGE_SEND_EMAIL . '</button>';
					} else {
					  echo '<button class="btn btn-success btn-sm" type="submit">' . IMAGE_SEND_EMAIL . '</button>';
					}
					?>
				  </td>
				</tr>
				<tr>
				  <td colspan="2" class="control-label main-text text-left">
					<?php
					 if (EMAIL_USE_HTML == 'false'){
						 echo(TEXT_EMAIL_BUTTON_HTML);
					 } else {
					  //   echo(TEXT_EMAIL_BUTTON_TEXT);
					 }
							?>
				  </td>
				</tr>
          </table>
          </form>
        <?php
        break;
      case 'voucheredit':
        $languages = tep_get_languages();
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $language_id = $languages[$i]['id'];
          $coupon_query = tep_db_query("select coupon_name,coupon_description from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" .  (int)$_GET['cid'] . "' and language_id = '" . $language_id . "'");
          $coupon = tep_db_fetch_array($coupon_query);
          $coupon_name[$language_id] = $coupon['coupon_name'];
          $coupon_desc[$language_id] = $coupon['coupon_description'];
        }
        $coupon_query=tep_db_query("select coupon_active, coupon_code, coupon_amount, coupon_type, coupon_sale_exclude, coupon_minimum_order, coupon_start_date, coupon_expire_date, date_created, uses_per_coupon, uses_per_user, restrict_to_products, restrict_to_categories from " . TABLE_COUPONS . " where coupon_id = '" . (int)$_GET['cid'] . "'");
        $coupon=tep_db_fetch_array($coupon_query);

        $coupon_amount = (($coupon['coupon_amount'] == round($coupon['coupon_amount'])) ? number_format($coupon['coupon_amount']) : number_format($coupon['coupon_amount'],2));
        if ($coupon['coupon_type'] == 'P') {
          // not floating point value, don't display decimal info
          $coupon_amount = (($coupon_amount == round($coupon_amount)) ? number_format($coupon_amount) : number_format($coupon_amount,2)) . '%';
        }
        if ($coupon['coupon_type'] == 'S') {
          $coupon_free_ship .= true;
        }
        $coupon_min_order = (($coupon['coupon_minimum_order'] == round($coupon['coupon_minimum_order'])) ? number_format($coupon['coupon_minimum_order']) : number_format($coupon['coupon_minimum_order'],2));
        $coupon_code = $coupon['coupon_code'];
        $coupon_uses_coupon = $coupon['uses_per_coupon'];
        $coupon_uses_user = $coupon['uses_per_user'];
        $coupon_sale_exclude = $coupon['coupon_sale_exclude'];
        $coupon_products = $coupon['restrict_to_products'];
        $coupon_categories = $coupon['restrict_to_categories'];
        $date_created = $coupon['date_created'];
        $coupon_status = $coupon['coupon_active'];
        $coupon_startdate = ($coupon['coupon_start_date'] == '' || $coupon['coupon_start_date'] == '0000-00-00 00:00:00')?'':$coupon['coupon_start_date'];
        $coupon_finishdate = ($coupon['coupon_expire_date'] == '' || $coupon['coupon_expire_date'] == '0000-00-00 00:00:00')?'':$coupon['coupon_expire_date'];

      case 'new':
        // set default if not editing an existing coupon or showing an error
        $oldaction = (isset($_GET['oldaction']) ? $_GET['oldaction'] : '');
        $coupon_uses_user = (isset($coupon_uses_user) ? $coupon_uses_user : '');
        $date_created = (isset($date_created) ? $date_created : '');
        $cid = (isset($_GET['cid']) ? $_GET['cid'] : '');
        //$coupon_startdate = (isset($_POST['coupon_startdate']) ? $_POST['coupon_startdate'] : '');
        //$coupon_finishdate = (isset($_POST['coupon_finishdate']) ? $_POST['coupon_finishdate'] : '');
        if ($_GET['action'] == 'new' && !$oldaction == 'new') {
          if (!$coupon_uses_user) {
            $coupon_uses_user=1;
          }
          if (!$date_created) {
            $date_created = '0';
          }
        }
        if(!isset($coupon_status) && isset($_POST['coupon_status'])) {
          $coupon_status = $_POST['coupon_status'];
        }
        if (!isset($coupon_status)) $coupon_status = 'Y';
        switch ($coupon_status) {
          case 'N': $in_status = false; $out_status = true; break;
          case 'Y':
          default: $in_status = true; $out_status = false;
        }
        // set some defaults
        //if (!$coupon_uses_user) $coupon_uses_user = 1;
			  echo tep_draw_form('coupon', 'coupon_admin.php', 'action=update_confirm&oldaction='. (($oldaction == 'voucheredit') ? $oldaction : $_GET['action']) . '&cid=' . $cid, 'POST', ' id="couponForm" data-parsley-validate');


              if(!isset($coupon_name) && isset($_POST['coupon_name'])) {
                $coupon_name = $_POST['coupon_name'];
              }
              if(!isset($coupon_desc) && isset($_POST['coupon_desc'])) {
                $coupon_desc = $_POST['coupon_desc'];
              }
              if(!isset($coupon_amount) && isset($_POST['coupon_amount'])) {
                $coupon_amount = $_POST['coupon_amount'];
              }
              if(!isset($coupon_min_order) && isset($_POST['coupon_min_order'])) {
                $coupon_min_order = $_POST['coupon_min_order'];
              }
              if(!isset($coupon_free_ship) && isset($_POST['coupon_free_ship'])) {
                $coupon_free_ship = $_POST['coupon_free_ship'];
              }
              if(!isset($coupon_code) && isset($_POST['coupon_code'])) {
                $coupon_code = $_POST['coupon_code'];
              }
              if(!isset($coupon_uses_coupon) && isset($_POST['coupon_uses_coupon'])) {
                $coupon_uses_coupon = $_POST['coupon_uses_coupon'];
              }
              /*******************/
              if(!isset($coupon_sale_exclude) && isset($_POST['coupon_sale_exclude'])) {
                $coupon_sale_exclude = $_POST['coupon_sale_exclude'];
              }
                /*******************/
              if(!isset($coupon_uses_user) && isset($_POST['coupon_uses_user'])) {
                $coupon_uses_user = $_POST['coupon_uses_user'];
              }
              if(!isset($coupon_products) && isset($_POST['coupon_products'])) {
                $coupon_products = $_POST['coupon_products'];
              }
              if(!isset($coupon_categories) && isset($_POST['coupon_categories'])) {
                $coupon_categories = $_POST['coupon_categories'];
              }
              if(!isset($coupon_startdate) && isset($_POST['coupon_startdate'])) {
                $coupon_startdate = $_POST['coupon_startdate'];
              }
              if(!isset($coupon_finishdate) && isset($_POST['coupon_finishdate'])) {
                $coupon_finishdate = $_POST['coupon_finishdate'];
              }
              $in_status = 1;
              ?>

      <!-- begin button bar -->
      <div id="button-bar" class="row">
        <div class="col-9 m-b-10 w-100 pt-1 pl-0 pr-0">
            <a href="<?php echo tep_href_link(FILENAME_COUPON_ADMIN, ''); ?>" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> <?php echo ((isset($pID) && empty($pID) === false) ? BUTTON_RETURN_TO_LIST :  IMAGE_CANCEL); ?></a>
            <button type="button" onclick="updateForm('save');" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo BUTTON_SAVE; ?></button>
            <button type="button" onclick="updateForm('stay');" class="btn btn-info m-r-3 btn-save-stay"><i class="fa fa-plus-square"></i> <?php echo BUTTON_SAVE_STAY; ?></button>
            <?php
            if (isset($pID) && empty($pID) === false) {
              ?>
              <a href="<?php echo tep_catalog_href_link('product_info.php', 'products_id=' . $pID); ?>" target="_blank" class="hidden-xs hidden-sm hidden-md btn btn-link m-r-5 f-w-200 text-primary"><i class="fa fa-laptop"></i> <?php echo BUTTON_VIEW_IN_CATALOG; ?></a>
              <?php
            }
            ?>
        </div>
        <div class="col-3 m-b-10 pt-1 pr-2"></div>
      </div>
      <!-- end button bar -->


    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-couponadmin" class="table-couponadmin">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
			<div class="main-heading" style="margin-top:10px!important"><span>Coupon Info</span>
			  <div class="main-heading-footer"></div>
			</div>

              <table border="0" width="100%" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_CODE; ?><span class="required"></span></td>
                  <td><?php echo tep_draw_input_field('coupon_code', (isset($coupon_code) ? $coupon_code : ''), 'required'); ?></td>
                  <td class="control-label main-text text-left"><?php echo COUPON_CODE_HELP; ?></td>
                </tr>
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_AMOUNT; ?><span class="required"></span></td>
                  <td><?php echo tep_draw_input_field('coupon_amount', (isset($coupon_amount) ? $coupon_amount : ''), 'required'); ?></td>
                  <td class="control-label main-text text-left"><?php echo COUPON_AMOUNT_HELP; ?></td>
                </tr>
                <?php
                $languages = tep_get_languages();
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                  $language_id = $languages[$i]['id'];
		  		  $lang_control = '<div class="input-group"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
				  $required_validation = ($languages[$i]['id'] == $_SESSION['languages_id']) ? ' required ' : '';
                  ?>
                  <tr>
                    <td class="control-label main-text"><?php if ($i==0) echo COUPON_NAME.'<span class="required"></span>'; ?></td>
                    <td class="form-value"><?php echo $lang_control.tep_draw_input_field('coupon_name[' . $languages[$i]['id'] . ']', (isset($coupon_name[$language_id]) ? $coupon_name[$language_id] : ''), $required_validation) .'</div>'; ?></td>
                    <td class="control-label main-text text-left" width="40%"><?php if ($i==0) echo COUPON_NAME_HELP; ?></td>
                  </tr>
                <?php
                }
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                  $language_id = $languages[$i]['id'];
		  		  $lang_control = '<div class="input-group"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
                  ?>
                  <tr>
                    <td class="control-label main-text"><?php if ($i==0) echo COUPON_DESC; ?></td>
                    <td><?php echo $lang_control.tep_draw_textarea_field('coupon_desc[' . $languages[$i]['id'] . ']','physical','24','3', (isset($coupon_desc[$language_id]) ? $coupon_desc[$language_id] : ''), 'class="form-control"') .'</div>'; ?></td>
                    <td class="control-label main-text text-left"><?php if ($i==0) echo COUPON_DESC_HELP; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_PRODUCTS; ?></td>
                  <td>
                    <?php echo '<div class="input-group">'.tep_draw_input_field('coupon_products', (isset($coupon_products) ? $coupon_products : '')) .'<span class="input-group-addon">'; ?><a
                     href="javascript:void(0)" onclick="window.open('<?php echo tep_href_link('treeview.php', 'script=' . urlencode('window.opener.document.coupon.coupon_products.value = prod_value;
				      window.opener.document.coupon.coupon_categories.value = cat_value;'), $request_type); ?>', 'popuppage', 'scrollbars=yes,resizable=yes,menubar=yes,width=400,height=600');"><i class="fa fa-external-link" aria-hidden="true"></i></a></span></div>
                  </td>
                  <td class="control-label main-text text-left"><?php echo COUPON_PRODUCTS_HELP; ?></td>
                </tr>
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_CATEGORIES; ?></td>
                  <td><div class="input-group">
                    <?php echo tep_draw_input_field('coupon_categories', isset($coupon_categories) ? $coupon_categories : '' ); ?><span class="input-group-addon"><a href="javascript:void(0)"
                     onclick="window.open('<?php echo tep_href_link('treeview.php', 'script=' . urlencode('window.opener.document.coupon.coupon_products.value = prod_value;window.opener.document.coupon.coupon_categories.value = cat_value;'), $request_type); ?>', 'popuppage', 'scrollbars=yes,resizable=yes,menubar=yes,width=400,height=600');"><i class="fa fa-external-link" aria-hidden="true"></i></a></span></div>
                  </td>
                  <td class="control-label main-text text-left"><?php echo COUPON_CATEGORIES_HELP; ?></td>
                </tr>
               </table> 
			</div>
			<div class="col-md-3 col-xl-2 dark panel-right rounded-right">
			  <div class="sidebar-heading mt-3">
				<span>Additional Info</span>
			  </div>
			  <div class="sidebar-heading-footer w-100"></div>
              <table border="0" width="100%" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_STATUS; ?><span class="required"></span></td>
                  <td class="control-label main-text text-left"><?php                   
                  echo '<input name="coupon_status" ' . (($in_status) ? 'checked' : '') . ' data-toggle="toggle" data-on="<i class=\'fa fa-check\'></i> ' . TEXT_ACTIVE . '" data-off="<i class=\'fa fa-times\'></i> ' . TEXT_INACTIVE . '" data-onstyle="success" data-offstyle="danger" type="checkbox" data-size="small">';
                  ?></td>
                </tr>
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_FREE_SHIP; ?></td>
                  <td><?php echo tep_draw_checkbox_field('coupon_free_ship', (isset($coupon_free_ship) ? $coupon_free_ship : ''), '', '', 'class="js-switch"'); ?></td>
                </tr>
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_MIN_ORDER; ?></td>
                  <td><?php echo '<div class="input-group"> <span class="input-group-addon text-white bg-blue-lighter p-5">' . $currencies->get_symbol_left(DEFAULT_CURRENCY) . '</span>'.tep_draw_input_field('coupon_min_order', (isset($coupon_min_order) ? $coupon_min_order : ''), 'data-parsley-pattern="^-?[0-9]\d*(\.\d+)?$"').'</div>'; ?></td>
                </tr>
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_USES_COUPON; ?></td>
                  <td><?php echo tep_draw_input_field('coupon_uses_coupon', (isset($coupon_uses_coupon) ? $coupon_uses_coupon : ''), ' data-parsley-type="integer"'); ?></td>
                </tr>
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_USES_USER; ?></td>
                  <td><?php echo tep_draw_input_field('coupon_uses_user', (isset($coupon_uses_user) ? $coupon_uses_user : ''), ' data-parsley-type="integer"'); ?></td>
                </tr>
<!--gsr  -->

          <?php
              //print("coupon_sale_exclude:".$coupon_sale_exclude."<br>");
             if(isset($coupon_sale_exclude) && $coupon_sale_exclude == 1) {
             $s_coupon_sale_exclude = ' checked';
            } else {
              $s_coupon_sale_exclude = '';
            }
            ?>
				<tr>
				   <td class="control-label main-text"><?php echo COUPON_SALE_EXCLUSION; ?></td>
				   <td><input type="checkbox" name="coupon_sale_exclude" class="js-switch" value="1" <?php echo $s_coupon_sale_exclude; ?> >
				</tr>
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_STARTDATE; ?></td>
                  <td>
                  <?php 
                  $coupon_startdate = (isset($coupon_startdate) && $coupon_startdate != '')?date('m/d/Y', strtotime($coupon_startdate)):'';
                  echo tep_draw_input_field('coupon_startdate', $coupon_startdate,'class="calender form-control" id="calender" data-date-format="mm/dd/yyyy"'); ?>
                </tr>
                <tr>
                  <td class="control-label main-text"><?php echo COUPON_FINISHDATE; ?></td>
                  <td>
                  <?php 
                  $coupon_exp_date = isset($coupon_finishdate)?date('m/d/Y', strtotime($coupon_finishdate)):'';
                  echo tep_draw_input_field('coupon_finishdate', $coupon_exp_date,'class="calender form-control" id="calender1" data-date-format="mm/dd/yyyy"'); ?>
                </tr>
                <?php
                echo tep_draw_hidden_field('date_created', $date_created);
                ?>
              </table>
              </form>
        <?php
        break;

      default:
        ?>
      <div class="row">
        <div class="col-9"></div>
        <div class="col-3 pr-0">
          <?php echo tep_draw_form('status', FILENAME_COUPON_ADMIN, '', 'get');?>
           <div class="form-group row mb-2 pr-0">
            <label for="cPath" class="hidden-xs col-sm-3 col-form-label text-center m-t-10 pr-0"><?php echo HEADING_TITLE_STATUS; ?></label>
            <div class="col-sm-9 p-0 dark rounded">
			<?php
                  $status_array[] = array('id' => 'Y', 'text' => TEXT_COUPON_ACTIVE);
                  $status_array[] = array('id' => 'N', 'text' => TEXT_COUPON_INACTIVE);
                  $status_array[] = array('id' => 'R', 'text' => TEXT_COUPON_REDEEMED);
                  $status_array[] = array('id' => '*', 'text' => TEXT_COUPON_ALL);
                  if ( isset($_GET['status']) ) {
                    $status = tep_db_prepare_input($_GET['status']);
                  } else {
                    $status = 'Y';
                  }
                  echo tep_draw_pull_down_menu('status', $status_array, $status, 'onChange="this.form.submit();"');
			  ?>

            </div>
          </div>
          <?php
          if (isset($_GET[tep_session_name()])) {
            echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
          }
          echo '</form></div>';
          ?>
        </div>
      </div>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-couponadmin" class="table-couponadmin">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">


        		<table class="table table-hover w-100 mt-2">
				  <thead>
				   <tr class="th-row">
					<th scope="col" class="th-col dark text-left"><?php echo COUPON_NAME; ?></th>
                    <th scope="col" class="th-col dark text-center"><?php echo COUPON_AMOUNT; ?></th>
                    <th scope="col" class="th-col dark text-center"><?php echo COUPON_CODE; ?></th>
                    <th scope="col" class="th-col dark text-center"><?php echo TEXT_REDEMPTIONS; ?></th>
                    <th scope="col" class="th-col dark text-center"><?php echo COUPON_STATUS; ?></th>
                    <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                  </tr>
                  <?php
                  if (isset($_GET['page']) && $_GET['page'] > 1) $rows = $_GET['page'] * 20 - 20;
                  if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
                    $cc_query_raw = "select c.coupon_active, c.coupon_id, c.coupon_code, c.coupon_amount, c.coupon_minimum_order, c.coupon_type, c.coupon_start_date,c.coupon_expire_date,c.uses_per_user,c.uses_per_coupon,c.restrict_to_products, c.restrict_to_categories, c.date_created,c.date_modified from " . TABLE_COUPONS . " c, " . TABLE_COUPONS_DESCRIPTION . " cd where c.coupon_id = cd.coupon_id and cd.language_id = '" . $_SESSION['languages_id'] . "' and cd.coupon_name like '%" . tep_db_input($_GET['search']) . "%'";
                  } elseif ($status == 'Y' || $status == 'N') {
                    $cc_query_raw = "select coupon_active, coupon_id, coupon_code, coupon_amount, coupon_minimum_order, coupon_type, coupon_start_date,coupon_expire_date,uses_per_user,uses_per_coupon,restrict_to_products, restrict_to_categories, date_created,date_modified from " . TABLE_COUPONS ." where coupon_active='" . tep_db_input($status) . "' and coupon_type != 'G'";
                  } else {
                    $cc_query_raw = "select coupon_active, coupon_id, coupon_code, coupon_amount, coupon_minimum_order, coupon_type, coupon_start_date,coupon_expire_date,uses_per_user,uses_per_coupon,restrict_to_products, restrict_to_categories, date_created,date_modified from " . TABLE_COUPONS . " where coupon_type != 'G'";
                  }
                  $cc_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $cc_query_raw, $cc_query_numrows);
                  $cc_query = tep_db_query($cc_query_raw);
                  while ($cc_list = tep_db_fetch_array($cc_query)) {
                    $redeem_query = tep_db_query("select redeem_date from " . TABLE_COUPON_REDEEM_TRACK . " where coupon_id = '" . $cc_list['coupon_id'] . "'");
                    if ($status == 'R' && tep_db_num_rows($redeem_query) == 0) {
                      continue;
                    }
                    $rows++;
                    if (strlen($rows) < 2) {
                      $rows = '0' . $rows;
                    }
                    if (((!$_GET['cid']) || (@$_GET['cid'] == $cc_list['coupon_id'])) && (!$cInfo)) {
                      $cInfo = new objectInfo($cc_list);
                    }

					$selected = ((is_object($cInfo)) && ($cc_list['coupon_id'] == $cInfo->coupon_id))? true : false;
					$col_selected = ($selected) ? ' selected' : '';
                    if ( $selected ) {
                      echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link('coupon_admin.php', tep_get_all_get_params(array('cid', 'action')) . 'cid=' . $cInfo->coupon_id . '&action=edit') . '\'">' . "\n";
                    } else {
                      echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link('coupon_admin.php', tep_get_all_get_params(array('cid', 'action')) . 'cid=' . $cc_list['coupon_id']) . '\'">' . "\n";
                    }
                    $coupon_description_query = tep_db_query("select coupon_name from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" . $cc_list['coupon_id'] . "' and language_id = '" . $languages_id . "'");
                    $coupon_desc = tep_db_fetch_array($coupon_description_query);
                    ?>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $coupon_desc['coupon_name']; ?></td>
                    <td class="table-col dark text-center<?php echo $col_selected; ?>">
                      <?php
                      if ($cc_list['coupon_type'] == 'P') {
                        // not floating point value, don't display decimal info
                        echo (($cc_list['coupon_amount'] == round($cc_list['coupon_amount'])) ? number_format($cc_list['coupon_amount']) : number_format($cc_list['coupon_amount'],2)) . '%';
                      } elseif ($cc_list['coupon_type'] == 'S') {
                        echo TEXT_FREE_SHIPPING;
                      } else {
                        echo $currencies->format($cc_list['coupon_amount']);
                      }
                      ?>&nbsp;
                    </td>
                    <td class="table-col dark text-center<?php echo $col_selected; ?>"><?php echo $cc_list['coupon_code']; ?></td>
                    <td class="table-col dark text-center<?php echo $col_selected; ?>">
                      <?php
                      echo tep_db_num_rows($redeem_query);   // number of redemptions
                      ?>
                    </td>
                    <td class="table-col dark text-center<?php echo $col_selected; ?>">
                      <?php
                      if ($cc_list['coupon_active'] == 'Y') {
                        echo '<i class="fa fa-lg fa-check-circle text-success mr-2"></i> <a href="' . tep_href_link(FILENAME_COUPON_ADMIN, 'action=setflag&flag=N&cid=' . $cc_list['coupon_id']) . '"><i class="fa fa-lg fa-times-circle text-secondary mr-2"></i></a>';
                      } else {
                        echo '<a href="' . tep_href_link(FILENAME_COUPON_ADMIN, 'action=setflag&flag=Y&cid=' . $cc_list['coupon_id']) . '"><i class="fa fa-lg fa-times-circle text-danger mr-2"></i></a><i class="fa fa-lg fa-check-circle text-secondary mr-2"></i>';
                      }
                      ?>
                    </td>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if ( (is_object($cInfo)) && ($cc_list['coupon_id'] == $cInfo->coupon_id) ) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_COUPON_ADMIN, 'page=' . $_GET['page'] . '&cid=' . $cc_list['coupon_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
                    </tr>
                    <?php
                    $redeem_date = '';
                    while ($redeem_list = tep_db_fetch_array($redeem_query)) {   // retrieve last redeem date
                      $redeem_date = $redeem_list['redeem_date'];
                    }
                    if ( (is_object($cInfo)) && ($cc_list['coupon_id'] == $cInfo->coupon_id) ) {   // store for later
                      $rInfo = new objectInfo(array('redeem_date' => $redeem_date));
                    }
                  }
                  ?>
                  </table>
                  <table border="0" width="100%" cellspacing="0" cellpadding="2">
                      <tr>
                        <td class="smallText">&nbsp;<?php echo $cc_split->display_count($cc_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_COUPONS); ?>&nbsp;</td>
                        <td align="right" class="smallText">&nbsp;<?php echo $cc_split->display_links($cc_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], 'status=' . $status); ?>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="right" colspan="2" class="smallText"><?php echo '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link('coupon_admin.php', 'page=' . $_GET['page'] . '&cID=' . isset($cInfo->coupon_id) . '&action=new') . '">' . tep_image_button('button_insert.gif', IMAGE_NEW_COUPON) . '</a>'; ?></td>
                      </tr>
                    </table>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
                <?php
                $heading = array();
                $contents = array();
                switch ($_GET['action']) {
                  default:
			      if (isset($cInfo) && is_object($cInfo)) {
                    $heading[] = array('text'=> COUPON_CODE . ': ' . $cInfo->coupon_code);
                    $amount = $cInfo->coupon_amount;
                    if ($cInfo->coupon_type == 'P') {
                      // not floating point value, don't display decimal info
                      $amount = (($amount == round($amount)) ? number_format($amount) : number_format($amount,2)) . '%';
                    } else {
                      $amount = $currencies->format($amount);
                    }
                    $coupon_min_order = $currencies->format($cInfo->coupon_minimum_order);
                    if (isset($_GET['action']) && $_GET['action'] == 'voucherdelete') {
                      $contents[] = array('text'=> '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_CONFIRM_DELETE.'</p></div></div></div>');
                      $contents[] = array('align' => 'center', 'text'=> '<div class="mt-2 mb-2">' .
                                                   '<a class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="'.tep_href_link('coupon_admin.php','action=confirmdelete&status=' . $status . (($_GET['page'] > 1) ? '&page=' . $_GET['page']: '') . '&cid='.$_GET['cid'],'NONSSL').'">'. IMAGE_CONFIRM .'</a><a class="btn btn-grey btn-sm mb-2 mt-2 btn-cancel" href="'.tep_href_link('coupon_admin.php','cid='.$cInfo->coupon_id,'NONSSL').'">'. IMAGE_CANCEL .'</a></div>'
                                         );
                    } else {
                      $prod_details = NONE;
                      if ($cInfo->restrict_to_products) {
                        $prod_details = '<A HREF="listproducts.php?cid=' . $cInfo->coupon_id . '" TARGET="_blank" ONCLICK="window.open(\'listproducts.php?cid=' . $cInfo->coupon_id . '\', \'Valid_Categories\', \'scrollbars=yes,resizable=yes,menubar=yes,width=600,height=600\'); return false">' . VIEW . '</A>';
                      }
                      $cat_details = NONE;
                      if ($cInfo->restrict_to_categories) {
                        $cat_details = '<A HREF="listcategories.php?cid=' . $cInfo->coupon_id . '" TARGET="_blank" ONCLICK="window.open(\'listcategories.php?cid=' . $cInfo->coupon_id . '\', \'Valid_Categories\', \'scrollbars=yes,resizable=yes,menubar=yes,width=600,height=600\'); return false">' . VIEW . '</A>';
                      }
                      $coupon_name_query = tep_db_query("select coupon_name from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" . $cInfo->coupon_id . "' and language_id = '" . $languages_id . "'");
                      $coupon_name = tep_db_fetch_array($coupon_name_query);
                      $contents[] = array('text'=> '<div class="mt-2 mb-2 text-center">
                      								<a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="'.tep_href_link('coupon_admin.php','action=voucheredit&cid='.$cInfo->coupon_id,'NONSSL').'">'. COUPON_BUTTON_EDIT_VOUCHER .'</a>' .
                                                   '<a class="btn btn-grey btn-sm mt-2 mb-2 btn-emailcoupon" href="'.tep_href_link('coupon_admin.php','action=email&cid='.$cInfo->coupon_id,'NONSSL').'">'. COUPON_BUTTON_EMAIL_VOUCHER .'</a>' .
                                                   '<a class="btn btn-grey btn-sm mt-2 mb-2 btn-couponreport" href="'.tep_href_link('coupon_admin.php','action=report&cid='.$cInfo->coupon_id,'NONSSL').'">'. COUPON_BUTTON_VOUCHER_REPORT .'</a>' .
                                                   '<a class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="'.tep_href_link('coupon_admin.php','action=voucherdelete&status=' . $status . (($_GET['page'] > 1) ? '&page=' . $_GET['page']: '') . '&cid='.$cInfo->coupon_id,'NONSSL').'">'. COUPON_BUTTON_DELETE_VOUCHER .'</a></div>' .
                                                   '<div class="sidebar-text">' .COUPON_NAME . ':<span class="sidebar-title ml-2">' . $coupon_name['coupon_name'] . '</span></div>' .
                                                   '<div class="sidebar-text">' .COUPON_AMOUNT . ':<span class="sidebar-title ml-2">' . $amount . '</span></div>' .
                                                   '<div class="sidebar-text">' .REDEEM_DATE_LAST . ':<span class="sidebar-title ml-2">' . ((isset($rInfo->redeem_date)) ? tep_date_short($rInfo->redeem_date) : '') . '</span></div>' .
                                                   '<div class="sidebar-text">' .COUPON_MIN_ORDER . ':<span class="sidebar-title ml-2">' . $coupon_min_order . '</span></div>' .
                                                   '<div class="sidebar-text">' .COUPON_STARTDATE . ':<span class="sidebar-title ml-2">' . tep_date_short($cInfo->coupon_start_date) . '</span></div>' .
                                                   '<div class="sidebar-text">' .COUPON_FINISHDATE . ':<span class="sidebar-title ml-2">' . tep_date_short($cInfo->coupon_expire_date) . '</span></div>' .
                                                   '<div class="sidebar-text">' .COUPON_USES_COUPON . ':<span class="sidebar-title ml-2">' . $cInfo->uses_per_coupon . '</span></div>' .
                                                   '<div class="sidebar-text">' .COUPON_USES_USER . ':<span class="sidebar-title ml-2">' . $cInfo->uses_per_user . '</span></div>' .
                                                   '<div class="sidebar-text">' .COUPON_PRODUCTS . ':<span class="sidebar-title ml-2">' . $prod_details . '</span></div>' .
                                                   '<div class="sidebar-text">' .COUPON_CATEGORIES . ':<span class="sidebar-title ml-2">' . $cat_details . '</span></div>' .
                                                   '<div class="sidebar-text">' .DATE_CREATED . ':<span class="sidebar-title ml-2">' . tep_date_short($cInfo->date_created) . '</span></div>' .
                                                   '<div class="sidebar-text">' .DATE_MODIFIED . ':<span class="sidebar-title ml-2">' . tep_date_short($cInfo->date_modified) . '</span></div>'
                                          );
                      }
                    }
                    break;
                } // end switch
                
			  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
				$box = new box;
				echo $box->showSidebar($heading, $contents);
			  }
        break;
    } // end switch $_GET['action']
    ?>

          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
          <div class="row mt-2">
          <div class="col-md-12 col-xl-12 pb-3 dark panel-left rounded">
          <table>
          <?php
            // RCI code start
             echo $cre_RCI->get('coupon', 'bottom');
            // RCI code eof
          ?>
          </table>
        </div>
        </div>
  </div>
</div>
<!-- body_eof //-->
<script>
function updateForm(mode) {
  var action = '<?php echo str_replace('&amp;', '&', tep_href_link(FILENAME_COUPON_ADMIN, 'action=update_confirm&oldaction='. (($oldaction == 'voucheredit') ? $oldaction : $_GET['action']) . '&cid=' . $cid)); ?>';
  // set the save mode in hidden form input
  $('<input />').attr('type', 'hidden')
      .attr('name', "mode")
      .attr('value', mode)
      .appendTo('#couponForm');

  $('#couponForm').attr('action', action).submit();
}

$(document).ready(function(){

  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small',
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  });
});
$('#calender').datepicker({
	    todayHighlight: true,
	    dateFormat: "mm/dd/yyyy",
        startDate: new Date()

});
$('#calender1').datepicker({
	    todayHighlight: true,
	    dateFormat: "mm/dd/yyyy",
	    startDate: new Date()
});


</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
