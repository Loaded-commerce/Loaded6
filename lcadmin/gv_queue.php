<?php
/*
  $Id: gv_queue.php,v 1.1.1.1 2004/03/04 23:38:36 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 - 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  if ( isset($_GET['action']) && $_GET['action']=='confirmrelease' && isset($_GET['gid']) ) {
    $gv_query=tep_db_query("select release_flag from " . TABLE_COUPON_GV_QUEUE . " where unique_id='".$_GET['gid']."'");
    $gv_result=tep_db_fetch_array($gv_query);
    if ($gv_result['release_flag']=='N') {
      $gv_query=tep_db_query("select customer_id, amount from " . TABLE_COUPON_GV_QUEUE ." where unique_id='".$_GET['gid']."'");
      if ($gv_resulta=tep_db_fetch_array($gv_query)) {
      $gv_amount = $gv_resulta['amount'];
      //Let's build a message object using the email class
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_id = '" . $gv_resulta['customer_id'] . "'");
      $mail = tep_db_fetch_array($mail_query);
      $message = TEXT_REDEEM_COUPON_MESSAGE_HEADER;
      $message .= sprintf(TEXT_REDEEM_COUPON_MESSAGE_AMOUNT, $currencies->format($gv_amount));
      $message .= TEXT_REDEEM_COUPON_MESSAGE_BODY;
      $message .= TEXT_REDEEM_COUPON_MESSAGE_FOOTER;


	  if (EMAIL_USE_HTML == "false") {
	    $email_content = $message;
	  } else {
	    $email_content['text'] = strip_tags($message);
	    $email_content['html'] = nl2br($message);
	  }
	  tep_mail($mail['customers_firstname'] . ' ' . $mail['customers_lastname'], $mail['customers_email_address'], TEXT_REDEEM_COUPON_SUBJECT, $email_content, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);

      $gv_amount=$gv_resulta['amount'];
      $gv_query=tep_db_query("select amount from " . TABLE_COUPON_GV_CUSTOMER . " where customer_id='".$gv_resulta['customer_id']."'");
      $customer_gv=false;
      $total_gv_amount=0;
      if ($gv_result=tep_db_fetch_array($gv_query)) {
        $total_gv_amount=$gv_result['amount'];
        $customer_gv=true;
      }
      $total_gv_amount=$total_gv_amount+$gv_amount;
      if ($customer_gv) {
        $gv_update=tep_db_query("update " . TABLE_COUPON_GV_CUSTOMER . " set amount='".$total_gv_amount."' where customer_id='".$gv_resulta['customer_id']."'");
      } else {
        $gv_insert=tep_db_query("insert into " .TABLE_COUPON_GV_CUSTOMER . " (customer_id, amount) values ('".$gv_resulta['customer_id']."','".$total_gv_amount."')");
      }
        $gv_update=tep_db_query("update " . TABLE_COUPON_GV_QUEUE . " set release_flag='Y' where unique_id='".$_GET['gid']."'");
      }
    }
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('gvqueue') > 0) {
      echo $messageStack->output('gvqueue'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-gvqueue" class="table-gvqueue">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CUSTOMERS; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_ORDERS_ID; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_VOUCHER_VALUE; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_DATE_PURCHASED; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
              </thead>
              <tbody>
<?php
  $gv_query_raw = "select c.customers_firstname, c.customers_lastname, gv.unique_id, gv.date_created, gv.amount, gv.order_id from " . TABLE_CUSTOMERS . " c, " . TABLE_COUPON_GV_QUEUE . " gv where (gv.customer_id = c.customers_id and gv.release_flag = 'N')";
  $gv_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $gv_query_raw, $gv_query_numrows);
  $gv_query = tep_db_query($gv_query_raw);
  while ($gv_list = tep_db_fetch_array($gv_query)) {
    if (((!isset($_GET['gid'])) || (@$_GET['gid'] == $gv_list['unique_id'])) && (!isset($gInfo))) {
      $gInfo = new objectInfo($gv_list);
    }

	$selected = ((is_object($gInfo)) && ($gv_list['unique_id'] == $gInfo->unique_id))? true : false;
	$col_selected = ($selected) ? ' selected' : '';
    if ($selected) {
      echo '              <tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link('gv_queue.php', tep_get_all_get_params(array('gid', 'action')) . 'gid=' . $gInfo->unique_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '              <tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link('gv_queue.php', tep_get_all_get_params(array('gid', 'action')) . 'gid=' . $gv_list['unique_id']) . '\'">' . "\n";
    }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $gv_list['customers_firstname'] . ' ' . $gv_list['customers_lastname']; ?></td>
                <td class="table-col dark text-center<?php echo $col_selected; ?>"><?php echo $gv_list['order_id']; ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo $currencies->format($gv_list['amount']); ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo tep_datetime_short($gv_list['date_created']); ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if ( (is_object($gInfo)) && ($gv_list['unique_id'] == $gInfo->unique_id) ) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_GV_QUEUE, 'page=' . $_GET['page'] . '&gid=' . $gv_list['unique_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>
		  </tbody>
		</table>
		<table border="0" width="100%" cellspacing="0" cellpadding="2">
		  <tr>
			<td class="smallText" valign="top"><?php echo $gv_split->display_count($gv_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_GIFT_VOUCHERS); ?></td>
			<td class="smallText" align="right"><?php echo $gv_split->display_links($gv_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
		  </tr>
		</table>


          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">                
<?php
if (!isset($gInfo)) {
  $gInfo = new objectInfo(array());
  $gInfo->customers_firstname = '';
  $gInfo->customers_lastname = '';
  $gInfo->unique_id = '';
  $gInfo->date_created = '';
  $gInfo->amount = '';
  $gInfo->order_id = '';
}
  $heading = array();
  $contents = array();
  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  switch ($action) {
    case 'release':
      $heading[] = array('text' => '[' . $gInfo->unique_id . '] ' . tep_datetime_short($gInfo->date_created) . ' ' . $currencies->format($gInfo->amount));

      $contents[] = array('align' => 'center', 'text' => '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link('gv_queue.php','action=confirmrelease&gid='.$gInfo->unique_id,'NONSSL').'">'. IMAGE_CONFIRM  . '</a><a class="btn btn-default btn-sm mt-2 mb-2" href="' . tep_href_link('gv_queue.php','action=cancel&gid=' . $gInfo->unique_id,'NONSSL') . '">' .  IMAGE_CANCEL . '</a>');
      break;
    default:
	  if (isset($gInfo) && is_object($gInfo) && $gInfo->unique_id > 0) {
		  $heading[] = array('text' => '[' . $gInfo->unique_id . '] ' . tep_datetime_short($gInfo->date_created) . ' ' . $currencies->format($gInfo->amount));
		  $contents[] = array('align' => 'center','text' => '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link('gv_queue.php','action=release&gid=' . $gInfo->unique_id,'NONSSL'). '">' . IMAGE_RELEASE . '</a>');
		}
      break;
   }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
	$box = new box;
	echo $box->showSidebar($heading, $contents);
  }
?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
