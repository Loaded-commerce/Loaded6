<?php
/*
  $Id: packingslip.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

$oID = tep_db_prepare_input($_GET['oID']);
$orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where orders_id = '" . tep_db_input($oID) . "'");

include(DIR_WS_CLASSES . 'order.php');
$order = new order($oID);

include(DIR_WS_INCLUDES . 'html_top.php');
?>
<style>
.invoice-company-address {
  font-size: 14px;
}
.invoice-heading-text {
  font-size: 30px;
  margin-top: -20px;
}
@media print {
  .non-printable { display: none; }
}
</style>
<div id="content" class="p-relative">         

  <div class="col">     
    <!-- begin button bar --> 
    <div id="top-buttons" class="row non-printable">
      <div class="col-9 m-b-10 w-100 pt-2 pl-0 pr-0"> 
        <a href="<?php echo tep_href_link(FILENAME_ORDERS, 'oID=' . $oID); ?>" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> <?php echo IMAGE_BACK; ?></a>
        <button type="button" onclick="window.print();" class="btn btn-info m-r-3"><i class="fa fa-print"></i> <?php echo BUTTON_PRINT; ?></button>
      </div>
      <div class="col-3 m-b-10 pt-1 pr-2">
      </div>
    </div>
    <!-- end button bar -->    
    <!-- begin panel -->
    <div class="light">
      <!-- body_text //-->     
      <div id="table-languages" class="table-languages">
        <div class="row">
          <div class="col-12 panel-left rounded-left">

            <!-- begin invoice -->
            <div class="invoice">
              <div class="invoice-company mb-0">
                <div class="pull-right"><img src="assets/img/logo_invoice.png" alt="Loaded Commerce" /></div>
                <div class="invoice-company-address"><?php echo nl2br(STORE_NAME_ADDRESS); ?></div>
              </div>
              <div class="invoice-heading-text text-center"><?php echo strtoupper(HEADING_TITLE); ?></div>

              <div class="invoice-header">
                <div class="invoice-from">
                  <small><?php echo ENTRY_SOLD_TO; ?></small>
                  <address class="m-t-5 m-b-5">
                    <strong><span id="order_customer_company"><?php echo $order->customer['company']; ?></span></strong><br />
                    <span id="order_customer_street_address"><?php echo $order->customer['street_address']; ?></span><br />
                    <?php if (isset($order->customer['suburb']) && $order->customer['suburb'] != '') echo '<span id="order_customer_suburb">' . $order->customer['suburb']; ?></span><br />
                    <span id="order_customer_city"><?php echo $order->customer['city'] . '</span>, <span id="order_customer_state">' . $order->customer['state'] . '</span> <span id="order_customer_postcode">' . $order->customer['postcode']; ?></span><br />
                    <span id="order_customer_country"><?php echo $order->customer['country']; ?></span><br />
                    <?php echo ENTRY_PHONE; ?> <span id="order_customer_telephone"><?php echo $order->customer['telephone']; ?></span><br />
                    <?php echo ENTRY_EMAIL; ?> <span id="order_customer_email_address"><?php echo $order->customer['email_address']; ?></span>
                  </address>
                </div>
                <div class="invoice-to">
                  <small><?php echo ENTRY_SHIP_TO; ?></small>
                  <address class="m-t-5 m-b-5">
                    <strong><span id="order_delivery_company"><?php echo $order->delivery['company']; ?></span></strong><br />
                    <span id="order_delivery_street_address"><?php echo $order->delivery['street_address']; ?></span><br />
                    <?php if (isset($order->delivery['suburb']) && $order->delivery['suburb'] != '') echo '<span id="order_delivery_suburb">' . $order->delivery['suburb']; ?></span><br />
                    <span id="order_delivery_city"><?php echo $order->delivery['city'] . '</span>, <span id="order_delivery_state">' . $order->delivery['state'] . '</span> <span id="order_delivery_postcode">' . $order->delivery['postcode']; ?></span><br />
                    <span id="order_delivery_country"><?php echo $order->delivery['country']; ?></span><br />
                    <?php echo ENTRY_PHONE; ?> <span id="order_delivery_telephone"><?php echo $order->delivery['telephone']; ?></span><br />
                    <?php echo ENTRY_EMAIL; ?> <span id="order_delivery_email_address"><?php echo $order->delivery['email_address']; ?></span>
                  </address>
                </div>
              
                <div class="invoice-date">
                  <small><?php echo TEXT_ORDER_INFO; ?></small>
                  <div class="date m-t-5"><?php 
                  $o_date = new DateTime($order->info['date_purchased']);
                  echo $o_date->format('F d, Y'); ?>
                </div>
                <div class="invoice-detail mt-2">
                  <?php echo ENTRY_ORDER_NUMBER; ?> <span class="f-w-600 ml-1 f-s-12"><?php echo $oID; ?></span><br />
                </div>

                <div class="invoice-detail mt-2">
                  <span class="text-main ml-1 f-s-11"><?php echo $order->info['payment_method']; ?></span>
                </div>

              </div>

              <div class="invoice-content mt-4">
                <div class="table-responsive">

                  <table id="products-table" class="table table-invoice">
                    <thead>
                      <tr>
                        <th scope="col" colspan="2" class="th-pcol text-left"><?php echo TABLE_HEADING_PRODUCTS; ?></th>
                        <th scope="col" class="th-pcol text-left"><?php echo TABLE_HEADING_PRODUCTS_MODEL; ?></th>
                      </tr>
                    </thead>  
                    <tbody> 
                      <?php
                      for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {
                        echo '<tr class="table-row">' . "\n" .
                             '  <td class="table-col">' . $order->products[$i]['qty'] . '&nbsp;x</td>' . "\n" .
                             '  <td class="table-col">' . $order->products[$i]['name'];

                        if (isset($order->products[$i]['attributes']) && sizeof($order->products[$i]['attributes']) > 0) {
                          for ($j = 0, $k = sizeof($order->products[$i]['attributes']); $j < $k; $j++) {
                            echo '<br><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option_name'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
                            echo '</i></small></nobr>';
                          }
                        }
                        echo '  </td>' . "\n" .
                             '  <td class="table-col">' . $order->products[$i]['model'] . '</td>' . "\n" .
                             '</tr>' . "\n";
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="invoice-footer text-muted">
                <p class="text-center m-b-5"><?php echo TEXT_THANK_YOU; ?></p>
              </div>
            </div>
            <!-- end invoice -->
           </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function(){
  $('#page-container').removeClass('page-header-fixed');
});
</script>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>