<?php
/*
  $Id: infobox_configuration.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');
// Alias function for Store configuration values in the Administration Tool
function tep_cfg_select_option_infobox($select_array, $key_value, $key = '') {
  $string = '';

  for ($i=0, $n=sizeof($select_array); $i<$n; $i++) {
    $name = ((tep_not_null($key)) ? 'infobox_' .$key  : 'infobox_display');
    $string .= '<input type="radio" name="' . $name . '" value="' . $select_array[$i] . '"';
    if ($key_value == $select_array[$i]) $string .= ' CHECKED';
    $string .= '> ' . $select_array[$i];
  }
  return $string;
}

function tep_get_templates() {
  $templates_query = tep_db_query("select template_id, template_name from " . TABLE_TEMPLATE . " order by template_id");
  while ($template = tep_db_fetch_array($templates_query)) {
    // does the folder exists
    if ( ! file_exists(DIR_FS_TEMPLATES . $template['template_name'] . '/')) continue;
    // does the main_page.tpl.php file exist
    if ( ! file_exists(DIR_FS_TEMPLATES . $template['template_name'] . '/main_page.tpl.php')) continue;

    $template_array[] = array('id' => $template['template_id'],
                              'text' => $template['template_name']);
  }

  return $template_array;
}

// find gID
if (isset($_GET['gID'])) {
    $gID = $_GET['gID'] ;
  }else if (isset($_POST['gID'])){
    $gID = $_POST['gID'] ;
  } else {
   $gID = '' ;
  }


// find cID
if (isset($_GET['cID'])) {
    $cID = $_GET['cID'] ;
  }else if (isset($_POST['cID'])){
    $cID = $_POST['cID'] ;
  } else {
   $cID = 1 ;
  }

// begin action
if (isset($_GET['action'])) {
     $action = $_GET['action'] ;
   }else if (isset($_POST['action'])){
     $action = $_POST['action'] ;
   } else {
    $action = '' ;
  }


$template_array = tep_get_templates();
$template_selected = '';
$template_name = '';

// get the selected template name
for ($i=0, $n=sizeof($template_array); $i<$n; $i++) {
  if ($gID == $template_array[$i]['id']) {
    $template_name = $template_array[$i]['text'];
    $template_selected = $template_array[$i]['id'];
  }
}
// define the template name constant
define('TEMPLATE_NAME', $template_name);

// check to see if the selected template is ATS or BTS
if (file_exists(DIR_FS_TEMPLATES . TEMPLATE_NAME . '/template.php')) {
  $template_type = 'ATS';
  include DIR_FS_TEMPLATES . TEMPLATE_NAME . '/template.php';
} else {
  $template_type = 'BTS';
}
// define the default location for the boxes
if ( ! defined('DIR_FS_TEMPLATE_BOXES')) {
  if (file_exists(DIR_FS_TEMPLATES . TEMPLATE_NAME . '/boxes/')) {
    define('DIR_FS_TEMPLATE_BOXES', DIR_FS_TEMPLATES . TEMPLATE_NAME . '/boxes/');
  } else {
    define('DIR_FS_TEMPLATE_BOXES', DIR_FS_TEMPLATES . 'default/boxes/');
  }
}
define('DIR_FS_TEMPLATE_DEFAULT_BOXES', DIR_FS_TEMPLATES . 'default/boxes/');

if (tep_not_null($action)) {
  switch ($action) {

    case 'add_language_files': //create language heading entries.
      // pull infobox info
      $infobox_query = tep_db_query("select box_heading, infobox_id from " . TABLE_INFOBOX_CONFIGURATION);
      // $infobox = tep_db_fetch_array($infobox_query);
      while ($infobox = tep_db_fetch_array($infobox_query)) {
        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $language_id = $languages[$i]['id'];
          $box_heading = tep_db_prepare_input($infobox['box_heading']);
          $infobox_id = $infobox['infobox_id'];
          $box_heading = str_replace("'", "\\'", $box_heading);
          tep_db_query("insert into " . TABLE_INFOBOX_HEADING . " (infobox_id, languages_id, box_heading) values ('" . $infobox_id . "', '" . $language_id . "', '" . $box_heading . "') ");
        }
      }

      tep_redirect(tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cID));
      break;

    case 'position_update': //set the status of a template active buttons.
      if ( ($_GET['flag'] == 'up') || ($_GET['flag'] == 'down') ) {
        if ($gID) {
          tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set  location = '" . $_GET['loc'] .  "', last_modified = now() where location = '" . $_GET['loc1'] . "' and display_in_column = '" . $_GET['col'] . "'");
          tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set  location = '" . $_GET['loc1'] .  "', last_modified = now() where infobox_id = '" . (int)$_GET['iID'] . "' and display_in_column = '" . $_GET['col'] . "'");
        }
      }

      tep_redirect(tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $gID . '&cID=' . $iID));
      break;

    case 'fixweight':
      global  $infobox_id, $cID;
      $rightpos = 'right';
      $leftpos = 'left';

      $result_query = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where display_in_column = '" . $leftpos . "' and template_id = '" . (int)$gID . "' order by location");

      $sorted_position = 0;
      while ($result = tep_db_fetch_array($result_query)) {
        $sorted_position++;
        tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set location = '" . $sorted_position . "' where infobox_id = '" . (int)$result['infobox_id'] . "' and template_id = '" . (int)$gID . "'");
      }

      $result_query = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where display_in_column = '" . $rightpos . "' and template_id = '" . (int)$gID . "' order by location");

      $sorted_position = 0;
      while ($result = tep_db_fetch_array($result_query)) {
        $sorted_position++;
        tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set location = '" . $sorted_position . "' where infobox_id = '" . (int)$result['infobox_id'] . "' and template_id = '" . (int)$gID . "'");
      }

      tep_redirect(tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cID));
      break;

    case 'setflag': //set the status of a news item.
      if ($_GET['cID'] && $_GET['flag'] == 'no') {
        tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set infobox_display = '" . $_GET['flag'] . "' where infobox_id = '" . (int)$cID . "'");
      } elseif ($_GET['cID'] && $_GET['flag'] == 'yes') {
        // find the name of the box
        $sql = "SELECT infobox_file_name
                FROM " . TABLE_INFOBOX_CONFIGURATION . "
                WHERE infobox_id = " . (int)$cID;
        $infobox_query = tep_db_query($sql);
        $infobox = tep_db_fetch_array($infobox_query);

        $found_box = false;
        if (file_exists(DIR_FS_TEMPLATE_BOXES . $infobox['infobox_file_name'])) {
          $found_box = true;
        } elseif (DIR_FS_TEMPLATE_DEFAULT_BOXES != DIR_FS_TEMPLATE_BOXES && file_exists(DIR_FS_TEMPLATE_DEFAULT_BOXES . $infobox['infobox_file_name'])) {
          $found_box = true;
        }
        if ($found_box) {
          tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set infobox_display = '" . $_GET['flag'] . "' where infobox_id = '" . (int)$cID . "'");
        }
      }
	  echo ($_GET['flag'] == 'yes')? '1':'0';exit;
      tep_redirect(tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cID));
      break;

    case 'setflagcolumn': //set the status of a news item.
      if ( ($_GET['flag'] == 'left') || ($_GET['flag'] == 'right') ) {
        if ($_GET['cID']) {
          tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set display_in_column = '" . $_GET['flag'] . "' where infobox_id = '" . (int)$cID . "' and template_id = '" . $gID . "'");
        }
      }

      tep_redirect(tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cID));
      break;

    case 'save':
      $configuration_active = tep_db_prepare_input($_POST['infobox_active']);
      $infobox_file_name = tep_db_prepare_input($_POST['infobox_file_name']);
      $infobox_define = tep_db_prepare_input($_POST['infobox_define']);
      $display_in_column = tep_db_prepare_input($_POST['infobox_column']);
      $location = tep_db_prepare_input($_POST['location']);
      $box_template = tep_db_prepare_input($_POST['box_template']);
      $box_heading_font_color = tep_db_prepare_input($_POST['hexval']);
      $hexval = tep_db_prepare_input($_POST['hexval']);

      $error = false;
      if ($infobox_file_name == "") {
        $error = true;
        $messageStack->add('infobox_error_save', JS_INFO_BOX_FILENAME);
      }

      if ($infobox_define == "" || $infobox_define == "BOX_HEADING_????") {
        $error = true;
        $messageStack->add('infobox_error_save', JS_BOX_HEADING);
      }

      if ($hexval == "") {
        $error = true;
        $messageStack->add('infobox_error_save', JS_BOX_COLOR);
      }

      if ($error == 'true') {
        //do nothing and display error
      } else {
        $cID = tep_db_prepare_input($_GET['cID']);

        tep_db_query("update " . TABLE_INFOBOX_CONFIGURATION . " set infobox_file_name = '" . tep_db_input($infobox_file_name) . "',
               infobox_define = '" . tep_db_input($infobox_define) . "',
               location = '" . tep_db_input($location) . "',
               display_in_column = '" . tep_db_input($display_in_column) . "',
               infobox_display = '" . tep_db_input($configuration_active) . "',
               box_template = '" . tep_db_input($box_template) . "',
               box_heading_font_color = '" . tep_db_input($box_heading_font_color) . "',
               last_modified = now() where infobox_id = '" . (int)$cID . "' and template_id = '" . $gID . "'");

        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $language_id = $languages[$i]['id'];
          $box_heading = tep_db_prepare_input($_POST['box_heading'][$language_id]);

          tep_db_query("update " . TABLE_INFOBOX_HEADING . " set box_heading = '" . tep_db_input($box_heading) . "',
                  languages_id = '" . tep_db_input($language_id) . "'
              where infobox_id = '" . (int)$cID . "'and languages_id = '" . (int)$language_id . "'");
        }
        tep_redirect(tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cID));
       }
       break;

    case 'insert':

      $infobox_file_name = tep_db_prepare_input($_POST['infobox_file_name']);
      $infobox_define = tep_db_prepare_input($_POST['infobox_define']);
      $configuration_active = tep_db_prepare_input($_POST['infobox_active']);
      $display_in_column = tep_db_prepare_input($_POST['infobox_column']);
      $location = tep_db_prepare_input($_POST['location']);
      $box_template = tep_db_prepare_input($_POST['box_template']);
      $box_heading_font_color = tep_db_prepare_input($_POST['hexval']);
      $template_id = tep_db_prepare_input($gID);
      $box_heading = tep_db_prepare_input('box_heading');

      $error = false;
      if ($infobox_file_name == "") {
        $error = true;
        $messageStack->add('infobox_error_insert', JS_INFO_BOX_FILENAME);
      }
      if ($box_heading == "") {
        $error = true;
        $messageStack->add('infobox_error_insert', JS_INFO_BOX_HEADING);
      }
      if ($infobox_define == "" || $infobox_define == "BOX_HEADING_????") {
        $error = true;
        $messageStack->add('infobox_error_insert', JS_BOX_HEADING);
      }
      if ($box_heading_font_color == "") {
        $error = true;
        $messageStack->add('infobox_error_insert', JS_BOX_COLOR);
      }

      if ($error == false) {
        tep_db_query("insert into " . TABLE_INFOBOX_CONFIGURATION . " (template_id, infobox_file_name, infobox_display, infobox_define, display_in_column, location, box_template, box_heading_font_color) values ('" . tep_db_input($template_id) . "', '" . tep_db_input($infobox_file_name) . "', '" . tep_db_input($configuration_active) . "', '" . tep_db_input($infobox_define) . "', '" . tep_db_input($display_in_column) . "', '" . tep_db_input($location) . "',  '" . tep_db_input($box_template) . "', '" . tep_db_input($box_heading_font_color) . "')");
        $infobox_id1 = tep_db_insert_id() ;
        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $language_id = $languages[$i]['id'];
          $box_heading = tep_db_prepare_input($_POST['box_heading'][$language_id]);
          tep_db_query("insert into " . TABLE_INFOBOX_HEADING . " (infobox_id, languages_id, box_heading) values ('" . $infobox_id1 . "', '" . $language_id . "', '" . tep_db_input($box_heading) . "')");
        }
        tep_redirect(tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $gID));
      }
      break;

    case 'deleteconfirm':
      $cIDa = tep_db_prepare_input($_GET['cID']);;

      tep_db_query("delete from " . TABLE_INFOBOX_CONFIGURATION . " where infobox_id = '" . tep_db_input($cIDa) . "'");
      tep_db_query("delete from " . TABLE_INFOBOX_HEADING . " where infobox_id = '" . tep_db_input($cIDa) . "'");

        tep_redirect(tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $gID));
      break;
  }  // end switch ($action)
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<script language="javascript" src="includes/javascript/color_picker/picker.js"></script>
<script language="javascript"><!--
function popupWindow(url) {
  window.open(url,'popupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=450,height=300%,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
<script language="javascript"><!--
var i=0;

function resize() {
  if (navigator.appName == 'Netscape') i = 40;
  window.resizeTo(document.images[0].width + 30, document.images[0].height + 60 - i);
}


<!-- Begin
function showColor(val) {
document.infobox_configuration.hexval.value = val;
}
// End -->

//--></script>

<!-- begin #content -->
<div id="content" class="content  p-relative">

  <!-- begin page-header -->
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <!-- end page-header -->
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  <?php
  if ($messageStack->size('infobox_error_save') > 0) {
    echo $messageStack->output('infobox_error_save');
  }
  if ($messageStack->size('infobox_error_insert') > 0) {
    echo $messageStack->output('infobox_error_insert');
  }
  ?>

  <div class="col"><!-- begin col -->

      <div class="row">
        <div class="col-8"></div>
        <div class="col-4 pr-0">
          <?php echo tep_draw_form('gID', FILENAME_INFOBOX_CONFIGURATION, '', 'get');?>
           <div class="form-inline row mb-2 pr-0">
            <label for="cPath" class="hidden-xs col-sm-4 col-form-label text-center m-t-10 pr-0"><?php echo TEXT_LABEL_GOTO; ?></label>
            <div class="col-sm-8 p-0">
				      <?php echo tep_draw_pull_down_menu('gID', $template_array,  $template_selected, 'class="form-control w-100" onChange="this.form.submit();"'); ?>
            </div>
          </div>
          <?php
          if (isset($_GET[tep_session_name()])) {
            echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
          }
          echo '</form></div>';
          ?>
        </div>
      </div>
    <!-- begin panel -->
    <div class="dark"><!-- begin dark -->
      <!-- body_text //-->
      <div id="table-infobox-configuration" class="table-infobox-configuration">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
          	<table class="table table-hover w-100 mt-2">
      				<thead>
      				  <tr class="th-row">
      					<th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_INFOBOX_TITLE; ?></th>
      					<th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_INFOBOX_FILE_NAME; ?></th>
      					<th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_ACTIVE; ?></th>
      					<th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_COLUMN; ?></th>
      					<th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_SORT_ORDER; ?></th>
      					<th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
      				  </tr>
      				</thead>
              <tbody>
                <?php
                $count_left_active = 0;
                $count_right_active = 0;
                $totInf_boxes = 1;

                $configuration_query = tep_db_query("select *  from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" . $_GET['gID'] . "' order by display_in_column, location");
                while ($configuration = tep_db_fetch_array($configuration_query)) {
                  // if the dispaly is active, check to see if it exists
                  if ($configuration['infobox_display'] == 'yes') {
                    $found_box = false;
                    if (file_exists(DIR_FS_TEMPLATE_BOXES . $configuration['infobox_file_name'])) {
                      $found_box = true;
                    } elseif ($configuration['addon_name'] != '' && file_exists(DIR_FS_CATALOG .'addons/lc_'.$configuration['addon_name'].'/catalog/templates/boxes/'. $configuration['infobox_file_name'])) {
                      $found_box = true;
                    } elseif (DIR_FS_TEMPLATE_DEFAULT_BOXES != DIR_FS_TEMPLATE_BOXES && file_exists(DIR_FS_TEMPLATE_DEFAULT_BOXES . $configuration['infobox_file_name'])) {
                      $found_box = true;
                    }
                    // updated to only turn off missing infoboxes not ALL boxes - maestro
                    if (!$found_box) {
                      tep_db_query("UPDATE " . TABLE_INFOBOX_CONFIGURATION . "
                                    SET infobox_display = 'no'
                                    WHERE infobox_id = " . $configuration['infobox_id'] . " AND template_id = " . $gID);
                      $configuration['infobox_display'] = 'no';
                    }
                  }

                  $languages = tep_get_languages();
                  $configuration_query1 = tep_db_query("select box_heading  from " . TABLE_INFOBOX_HEADING . " where infobox_id = '" . $configuration['infobox_id'] . "' and languages_id = '" . $languages_id . "'");
                  while($configuration1 = tep_db_fetch_array($configuration_query1)) {
                    $box_heading = $configuration1['box_heading'];
                  }

                  $totInf_boxes++;
                  $cfgfname = $configuration['infobox_file_name'];
                  $cfgloc = $configuration['location'];
                  $cfgValue = $configuration['infobox_display'];
                  $cfgcol = $configuration['display_in_column'];
                  $cfgtemp = $configuration['box_template'];
                  $cfgkey = $configuration['infobox_define'];
                  $cfgfont = $configuration['box_heading_font_color'];

                  $location1 = $cfgloc - 1;
                  $location3 = $cfgloc + 1;

                  $res = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" . $gID . "' and location = ' $location1 '  AND display_in_column ='$cfgcol'");
                  $con1 =  tep_db_fetch_array($res);

                  $res2 = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" . $gID . "' and location = ' $location3 '  AND display_in_column ='$cfgcol'");
                  $con2 =  tep_db_fetch_array($res2);

                  if (($cfgcol == 'left') && ($cfgValue != 'no')) {
                    $count_left_active++;
                  } elseif (($cfgcol == 'right') && ($cfgValue != 'no')) {
                    $count_right_active++;
                  }
                  if (!isset($infobox_list1)) {
                    $infobox_list1 = '';
                  }
                  $infobox_list1 .= $configuration['infobox_file_name']. ",";

                  if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $configuration['infobox_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
                    $cfg_extra_query = tep_db_query("select * from " . TABLE_INFOBOX_CONFIGURATION . " where infobox_id = '" . (int)$configuration['infobox_id'] . "'");
                    $cfg_extra = tep_db_fetch_array($cfg_extra_query);

                    $cInfo_array = array_merge($configuration, $cfg_extra);
                    $cInfo = new objectInfo($cInfo_array);
                  }
                	$selected = ((isset($cInfo) && is_object($cInfo)) && ($configuration['infobox_id'] == $cInfo->infobox_id)) ? true : false;
                  if ($selected) {
                    echo '<tr class="table-row dark selected" id="crow_'.$configuration['infobox_id'].'">' . "\n";
                    $onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->infobox_id . '&action=edit') . '\'"';
                  } else {
                    echo '<tr class="table-row dark" id="crow_'.$configuration['infobox_id'].'">' . "\n";
                    $onclick = 'onclick="document.location.href=\'' .   tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $configuration['infobox_id']) . '\'"';
                  }
                  $col_selected = ($selected) ? ' selected' : '';
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo $box_heading; ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo  $cfgfname; ?></td>
                  <td class="setflag table-col dark text-center<?php echo $col_selected; ?>">

					<?php
					  $ajax_link = tep_href_link(FILENAME_INFOBOX_CONFIGURATION);
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=no&cID='.$configuration['infobox_id'].'\', '.$configuration['infobox_id'].',0 )" '.(($configuration['infobox_display'] == 'yes')? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=yes&cID='.$configuration['infobox_id'].'\', '.$configuration['infobox_id'].',1 )" '.(($configuration['infobox_display'] == 'no')? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
					?>

                  </td>
                  <td class="table-col dark text-center<?php echo $col_selected; ?>">
                    <?php
                    if ($configuration['display_in_column'] == 'left') {
                      echo '<i class="fa fa-lg fa-arrow-left text-success mr-2" title="' . IMAGE_INFOBOX_STATUS_GREEN .'"></i><a href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'action=setflagcolumn&flag=right&gID=' . $_GET['gID'] . '&cID=' . $configuration['infobox_id']) . '"><i class="fa fa-lg fa-arrow-right text-muted mr-2" title="'. IMAGE_INFOBOX_STATUS_RED_LIGHT .'"></i></a>';
                    } else {
                      echo '<a href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'action=setflagcolumn&flag=left&gID=' . $_GET['gID'] . '&cID=' . $configuration['infobox_id']) . '"><i class="fa fa-lg fa-arrow-left text-muted mr-2" title="'. IMAGE_INFOBOX_STATUS_GREEN_LIGHT .'"></i></a><i class="fa fa-lg fa-arrow-right text-success mr-2" title="'. IMAGE_INFOBOX_STATUS_RED .'"></i>';
                    }
                    ?>
                  </td>
                  <td class="table-col dark text-center<?php echo $col_selected; ?>" valign="middle">
                    <?php
                    if ($con1) {
                      echo '<a href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'action=position_update&loc1=' .$location1.'&loc=' .$cfgloc.'&flag=up&col=' . $cfgcol . '&iID=' .$configuration['infobox_id'] . '&gID=' . $_GET['gID']) . '"><i class="fa fa-lg fa-arrow-up text-success mr-2"></i></a>';
                    }
                    if ($con2) {
                      echo '<a href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'action=position_update&loc1=' .$location3.'&loc=' .$cfgloc.'&flag=down&col=' . $cfgcol . '&iID=' .$configuration['infobox_id'] . '&gID=' . $_GET['gID']) . '"><i class="fa fa-lg fa-arrow-down text-danger mr-2"></i></a>';
                    }
                    ?>
                  </td>

                  <td class="table-col dark text-right<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank"><?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $currency['currencies_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>

            <div class="float-right mr-2 mt-3 mb-3" role="group">
       		    <?php
            	echo '<a class="btn btn-success btn-sm" href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'action=fixweight&gID=' . $gID) . '">' . tep_image_button('button_update_box_positions.gif', IMAGE_BUTTON_UPDATE_BOX_POSITIONS) . '</a>';
            	echo '<a class="btn btn-success btn-sm ml-2" href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $gID . '&action=new') . '">' . tep_image_button('button_module_install.gif', IMAGE_NEW_INFOBOX) . '</a>' ;
            	?>
            </div>
          </div>

       	  <div class="col-md-3 col-xl-2 dark panel-right rounded-right">

            <?php
            $heading = array();
            $contents = array();

            switch ($action) {
              case 'new':
                $heading[] = array('text' => TEXT_INFO_HEADING_NEW_INFOBOX);
                $contents[] = array('form' => tep_draw_form('infobox_configuration', FILENAME_INFOBOX_CONFIGURATION, tep_get_all_get_params(array('action')) . 'action=insert', 'post', 'data-parsley-validate') . tep_draw_hidden_field('cID', (isset($cInfo->infobox_id)? $cInfo->infobox_id : 1) ));

                // info boxes can be in one of two directories, read both in
                $unique_boxes = array();
                if ($handle1 = opendir(DIR_FS_TEMPLATE_BOXES)) {
                  while (($filename = readdir($handle1)) !== false) {
                    if ( ! isset($unique_boxes[$filename]) ) {
                      $unique_boxes[$filename] = $filename;
                    }
                  }
                }
                closedir($handle1);
                if (DIR_FS_TEMPLATE_DEFAULT_BOXES != DIR_FS_TEMPLATE_BOXES) {
                  if ($handle1 = opendir(DIR_FS_TEMPLATE_DEFAULT_BOXES)) {
                    while (($filename = readdir($handle1)) !== false) {
                      if ( ! isset($unique_boxes[$filename]) ) {
                        $unique_boxes[$filename] = $filename;
                      }
                    }
                  }
                  closedir($handle1);
                }

                // build the array of unknown boxes
                foreach ($unique_boxes as $file1) {
                  if (stristr($infobox_list1.".,..", $file1) == FALSE){
                    $dirs_array1[] = array('id' => $file1,
                                           'text' => $file1);
                  }
                }
                if ((isset($cInfo)) && (is_object($cInfo->box_heading_font_color)) ){
                  $cInfo->box_heading_font_color ;
                } else {
                  $font_color =  '#FFFFFF';
                }

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_FILENAME . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=filename&amp;templatename=' . substr( DIR_FS_TEMPLATE_BOXES, strlen(DIR_WS_CATALOG) + 2)) . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a><span class="required"></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_draw_pull_down_menu('infobox_file_name',$dirs_array1,'', 'class="form-control" required') .'</span>' . '</div>
                                                    </div>
                                                  </div>
                                                </div>');

                $lang_html = '';
                for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                  $lang_html .= '<div class="form-group mb-0"><label class="note-info-text mb-0 f-s-12">' . $languages[$i]['name'] .'</label><span class="required"></span></div>' . tep_draw_input_field('box_heading[' . $languages[$i]['id'] . ']', null,'class="form-control" required');
                }
                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_HEADING . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=heading') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . $lang_html .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_WHICH_TEMPLATE . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=template') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a><span class="required"></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_draw_input_field('box_template','infobox','class="form-control" required') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_WHICH_COL . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=column') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_cfg_select_option_infobox(array('left', 'right'),'left','column') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_WHAT_POS . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=position') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_draw_input_field('location',$totInf_boxes,'class="form-control"') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_DEFINE_KEY . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=define') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span><span class="required"></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_draw_input_field('infobox_define','BOX_HEADING_EXAMPLE','class="form-control" required') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_SET_ACTIVE . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=active') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_cfg_select_option_infobox(array('yes', 'no'),'yes','active') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_FONT_COLOR . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=active') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span><span class="required"></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"><div class="input-group">' .
                  tep_draw_input_field('hexval',$font_color,'class="form-control" required') .'<span class="input-group-addon cursor-pointer" onclick="TCP.popup(document.forms[\'infobox_configuration\'].elements[\'hexval\'], 1);"><i class="fa fa-paint-brush"></i></span></div></span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('align' => 'center', 'text' =>'<div class="mt-0 mt-3 mb-2 text-center"><button class="btn btn-success btn-sm mt-2 mb-2 btn-insert" type="submit">' . IMAGE_INSERT .'</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $_GET['gID']) . '">' . IMAGE_CANCEL . '</a></div>');
                break;

              case 'edit':
                $count_left_active = 0;
                $count_right_active = 0;
                $totInf_boxes = 1;
                $infobox_list1 = '';

                $configuration_query = tep_db_query("select *  from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" . $gID . "' order by display_in_column, location");
                while ($configuration = tep_db_fetch_array($configuration_query)) {
                  $languages = tep_get_languages();
                  $configuration_query1 = tep_db_query("select box_heading  from " . TABLE_INFOBOX_HEADING . " where infobox_id = '" . $configuration['infobox_id'] . "' and languages_id = '" . $languages_id . "'");
                  while($configuration1 = tep_db_fetch_array($configuration_query1)) {
                    $box_heading = $configuration1['box_heading'];
                  }
                  $totInf_boxes++;
                  $cfgloc = $configuration['location'];
                  $cfgValue = $configuration['infobox_display'];
                  $cfgcol = $configuration['display_in_column'];
                  $cfgtemp = $configuration['box_template'];
                  $cfgkey = $configuration['infobox_define'];
                  $cfgfont = $configuration['box_heading_font_color'];
                  $cfgfile = $configuration['infobox_file_name'];

                  $location1 = $cfgloc - 1;
                  $location3 = $cfgloc + 1;

                  $res = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" . $gID . "' and location = ' $location1 '  AND display_in_column ='$cfgcol'");
                  $con1 =  tep_db_fetch_array($res);

                  $res2 = tep_db_query("select infobox_id from " . TABLE_INFOBOX_CONFIGURATION . " where template_id = '" . $gID . "' and location = ' $location3 '  AND display_in_column ='$cfgcol'");
                  $con2 =  tep_db_fetch_array($res2);

                  if (($cfgcol == 'left') && ($cfgValue != 'no')) {
                    $count_left_active++;
                  } elseif (($cfgcol == 'right') && ($cfgValue != 'no')) {
                    $count_right_active++;
                  }
                  $infobox_list1 .= $configuration['infobox_file_name']. ",";

                  if ((!isset($cID) || (isset($cID) && ($cID == $configuration['infobox_id']))) && (substr($action, 0, 3) != 'new')) {
                    $cfg_extra_query = tep_db_query("select * from " . TABLE_INFOBOX_CONFIGURATION . " where infobox_id = '" . (int)$configuration['infobox_id'] . "'");
                    $cfg_extra = tep_db_fetch_array($cfg_extra_query);

                    $cInfo_array = array_merge($configuration, $cfg_extra);
                    $cInfo = new objectInfo($cInfo_array);
                  }
                }

                $heading[] = array('text' => '<div class="text-truncate">' . $cInfo->box_heading . '</div>');
                $contents[] = array('form' => tep_draw_form('infobox_configuration', FILENAME_INFOBOX_CONFIGURATION, tep_get_all_get_params(array('action')) . 'action=save', 'post', 'data-parsley-validate') . tep_draw_hidden_field('cID', $cInfo->infobox_id));

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_FILENAME . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=filename') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . $cInfo->infobox_file_name .'</span>'. tep_draw_hidden_field('infobox_file_name',$cInfo->infobox_file_name,'size="20"','true') . '</div>
                                                    </div>
                                                  </div>
                                                </div>');

                $lang_html = '';
                for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                  $lang_html .= '<div class="form-group mb-0"><label class="note-info-text mb-0 f-s-12">' . $languages[$i]['name'] .'</label><span class="required"></span></div>' . tep_draw_input_field('box_heading[' . $languages[$i]['id'] . ']', tep_get_box_heading($cInfo->infobox_id, $languages[$i]['id']),'class="form-control" required');
                }
                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_HEADING . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=heading') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . $lang_html .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_WHICH_TEMPLATE . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=template') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a><span class="required"></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_draw_input_field('box_template',$cInfo->box_template,'class="form-control" required') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_WHICH_COL . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=column') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_cfg_select_option_infobox(array('left', 'right'), $cInfo->display_in_column, 'column') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_WHAT_POS . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=position') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_draw_input_field('location',$cInfo->location,'class="form-control"') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_DEFINE_KEY . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=define') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span><span class="required"></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_draw_input_field('infobox_define',$cInfo->infobox_define,'class="form-control" required') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_SET_ACTIVE . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=active') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' . tep_cfg_select_option_infobox(array('yes', 'no'),$cInfo->infobox_display,'active') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_FONT_COLOR . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=active') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span><span class="required"></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"> ' .tep_draw_input_field('hexval',$cInfo->box_heading_font_color,'class="form-control" required') .'</span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                $contents[] = array('text' => '<div class="row">
                                                 <div class="col p-0 mt-3 ml-2 mr-2">
                                                   <div class="note note-info m-0">
                                                     <h5 class="mt-0 mb-0">' . TEXT_HEADING_FONT_COLOR . '
                  <a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_INFOBOX_HELP,'action=active') . '\')"><i class="fa fa-info-circle fa-lg text-info ml-1"></i></a></span><span class="required"></span></h5>
                  <div class="mt-2"><span class="note-info-text f-w-600"><div class="input-group">' .
                  tep_draw_input_field('hexval',$cInfo->box_heading_font_color,'class="form-control" required') .'<span class="input-group-addon cursor-pointer" onclick="TCP.popup(document.forms[\'infobox_configuration\'].elements[\'hexval\'], 1);"><i class="fa fa-paint-brush"></i></span></div></span></div>
                                                    </div>
                                                  </div>
                                                </div>');

                //$contents[] = array('text' => '<div class="mt-2 mb-2 text-center"><a class="btn btn-success btn-sm mr-1 mt-2 mb-0" href="javascript:TCP.popup(document.forms[\'infobox_configuration\'].elements[\'hexval\'], 1)">' . TEXT_HEADING_FONT_CHANGE_COLOR .'</a></div>');
                $contents[] = array('align' => 'center', 'text' =>'<div class="mt-0 mt-3 mb-2 text-center"><button class="btn btn-success btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE .'</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->infobox_id) . '">' . IMAGE_CANCEL . '</a></div>');
                break;

              case 'delete':
                $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_INFOBOX);
                $contents[] = array('form' => tep_draw_form('configuration', FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $_GET['cID'] . '&action=deleteconfirm'));
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0 fw-400">' . sprintf(TEXT_INFO_DELETE_INTRO, $cInfo->infobox_file_name) . '</div></div></div>');
                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">'  . IMAGE_CONFIRM_DELETE .'</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $_GET['cID']) . '">' . IMAGE_CANCEL . '</a></div>');
                break;

              default:
                if (is_object($cInfo)) {
                  //Get tdefault template info or selected template
                  $templates_query = tep_db_query("select template_id, template_name from " . TABLE_TEMPLATE . " where template_id = " . $gID);
                  $template = tep_db_fetch_array($templates_query);

                  $dir_template_boxes_show = '';
                  if (file_exists(DIR_FS_TEMPLATE_BOXES . $cInfo_array['infobox_file_name'])) {
                    $dir_template_boxes_show = DIR_FS_TEMPLATE_BOXES;
                  } elseif (DIR_FS_TEMPLATE_DEFAULT_BOXES != DIR_FS_TEMPLATE_BOXES && file_exists(DIR_FS_TEMPLATE_DEFAULT_BOXES . $cInfo_array['infobox_file_name'])) {
                    $dir_template_boxes_show = DIR_FS_TEMPLATE_DEFAULT_BOXES;
                  }

                  // info boxes can be in one of two directories, read both in
                  $unique_boxes = array();
                  if ($handle1 = opendir(DIR_FS_TEMPLATE_BOXES)) {
                    while (($filename = readdir($handle1)) !== false) {
                      if ( ! isset($unique_boxes[$filename]) ) {
                        $unique_boxes[$filename] = $filename;
                      }
                    }
                  }
                  closedir($handle1);
                  if (DIR_FS_TEMPLATE_DEFAULT_BOXES != DIR_FS_TEMPLATE_BOXES) {
                    if ($handle1 = opendir(DIR_FS_TEMPLATE_DEFAULT_BOXES)) {
                      while (($filename = readdir($handle1)) !== false) {
                        if ( ! isset($unique_boxes[$filename]) ) {
                          $unique_boxes[$filename] = $filename;
                        }
                      }
                    }
                    closedir($handle1);
                  }
                  // build the array of unknown boxes
                  $avail_boxes = 0;
                  foreach ($unique_boxes as $file1) {
                    if (stristr($infobox_list1.".,..", $file1) == FALSE){
                      $avail_boxes ++;
                    }
                  }

                  if ($dir_template_boxes_show != '') {
                    $heading[] = array('text' => '<div class="text-truncate">' . $cInfo->infobox_file_name . '</div>');
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $_GET['gID'] . '&amp;cID=' . $cInfo->infobox_id . '&amp;action=edit') . '">' . IMAGE_EDIT . '</a><a
                    								class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_INFOBOX_CONFIGURATION, 'gID=' . $_GET['gID'] . '&amp;cID=' . $cInfo->infobox_id . '&amp;action=delete') . '">' . IMAGE_DELETE . '</a></div>' );
                  } else {
                    $heading[] = array('text' => TEXT_ERROR);
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . infobox_error1 . '</p></div></div></div>');
                  }
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-2 mb-2 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TABLE_HEADING_BOX_DIRECTORY . '</p><small>' . chunk_split($dir_template_boxes_show, 36) . '</small></div></div></div>');
                  $contents[] = array('align' => 'left', 'text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->date_added) . '</span></div>');
                  if (tep_not_null(tep_date_short($cInfo->last_modified))) $contents[] = array('align' => 'left','text' => '<div class="sidebar-text mt-1">'. TEXT_INFO_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->last_modified) . '</span></div>');

                  if ($cInfo->include_column_left == 'yes' && $count_left_active == 0) {
                    $contents[] = array('align' => 'left','text' => '<div class="col p-0 mt-3"><div class="note note-danger m-0 fw-400">err'. $infobox_error2 .'</div></div>');
                  }
                  if ($cInfo->include_column_right == 'yes' && $count_right_active == 0) {
                    $contents[] = array('align' => 'left','text' => '<div class="col p-0 mt-3"><div class="note note-danger m-0 fw-400">err'. $infobox_error3  .'</div></div>');
                  }
                  $contents[] = array('align' => 'left','text' => '<div class="sidebar-text mt-1 mb-3">'.TEXT_INFO_MESSAGE_COUNT_1 . '<span class="sidebar-title">' . $count_left_active . '</span>' . TEXT_INFO_MESSAGE_COUNT_2 . '<span class="sidebar-title">' . $count_right_active . '</span>' . INFOBOX_ACTIVE_BOXES.'</div>');
                }
                break;
            }

            if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
          	  $box = new box;
          	  echo $box->showSidebar($heading, $contents);
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end #content -->

<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>