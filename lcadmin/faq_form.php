<?php
/*
  $Id: faq_form.php,v 1.1 2008/06/11 00:18:17 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
echo tep_load_html_editor();
echo tep_insert_html_editor('question','simple','200');
echo tep_insert_html_editor('answer','advanced','400');
?>
<script language="JavaScript">
  var MaxLen = 512;
  function countMe(form) {
    inputStr = form.question.value;
    strlength= inputStr.length;
    if (strlength > MaxLen ) form.question.value = inputStr.substring(0,MaxLen);
    form.num.value = (MaxLen - form.question.value.length);
    form.question.focus();
  }
  function change_lang(lang) {
    <?php echo "window.location.href = '" . FILENAME_FAQ_MANAGER . '?faq_action=' . $_GET['faq_action'] . '&' . "faq_lang='+lang;"; ?>
  }
</script>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo $title; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('faqforms') > 0) {
      echo $messageStack->output('faqforms'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-faqforms" class="table-faqforms">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">


    <table border="0" cellpadding="5" cellspacing="0" width="100%">
		<tr colspan="2" class="headerBar">
		  <td class="control-label main-text text-left">
			<?php 
			echo FAQ_QUEUE_LIST;
			$data = browse_faq($language,$_GET);
			$no = 1;
			if (sizeof($data) > 0) {
			  foreach($data as $key => $val) {
				echo $val[v_order] . ', ';
				$no++;
			  }
			} 
			?>
		  </td>
		</tr>

      <tr>
        <td class="control-label main-text text-left" width="10%"><?php echo FAQ_QUEUE;?></td>          
        <td>
          <?php 
          if ($edit[v_order]) {
            $no = $edit[v_order];
          }
          echo tep_draw_input_field('v_order', "$no", 'size=3 maxlength=4 style="width:40%" required',false,'text',false);
          ?>
        </td>
      </tr>
      <tr>
        <td valign="top" class="control-label main-text text-left" width="10%"><?php echo FAQ_VISIBLE; ?></td>
        <td valign="top">
          <?php
          if ($edit[visible]) {
            $checked = "checked";
          }
          echo tep_draw_checkbox_field('visible', '1', $checked, '', 'class="js-switch"');
          ?>
        </td>
      </tr>
      <?php 
      if ($_GET['faq_action'] != 'Edit') {
        ?>
        <tr>
          <td valign="top" class="control-label main-text text-left" width="10%"><?php echo FAQ_LANGUAGE; ?></td>
          <td valign="top">
            <?php
            $lang_query = tep_db_query("select directory from " . TABLE_LANGUAGES . " order by languages_id desc");
            while ($get_lang = tep_db_fetch_array($lang_query)) {
              $langs[] = array('id' => $get_lang['directory'], 'text' => $get_lang['directory']);
            }
            if ($_GET['faq_lang']) {
              $def_lang = $_GET['faq_lang'];
            } else {
              $def_lang = $language;
            }
            echo tep_draw_pull_down_menu('faq_language',$langs,$def_lang,'onchange="change_lang(this.value);"');
            ?>
          </td>
        </tr>
        <?php
      }
      $categories_array = array();
      $categories_array[] = array('id' => '', 'text' => TEXT_NO_CATEGORY);
      $categories_query = tep_db_query("select icd.categories_id, icd.categories_name from " . TABLE_FAQ_CATEGORIES_DESCRIPTION . " icd where language_id = '" . (int)$languages_id . "' order by icd.categories_name");
      while ($categories_values = tep_db_fetch_array($categories_query)) {
        $categories_array[] = array('id' => $categories_values['categories_id'], 'text' => $categories_values['categories_name']);
      }
      ?>
      <tr>
        <td class="control-label main-text text-left" width="10%"><?php echo ENTRY_CATEGORY; ?></td>
        <td><?php echo tep_draw_pull_down_menu('faq_category', $categories_array, (isset($edit['categories_id']) ? $edit['categories_id'] : (isset($_GET['faq_category']) ? $_GET['faq_category'] : ''))); ?></td>
      </tr>
      <tr>
        <td valign="top" class="control-label main-text text-left" width="10%"><?php echo FAQ_QUESTION; ?></td>
        <td valign="top"><?php echo tep_draw_textarea_field('question', '', '60', '5', (isset($edit['question']) ? $edit['question'] : (isset($_GET['question']) ? $_GET['question'] : '')), 'style="width: 100%" class="form-control" onChange="countMe(document.forms[0])" onKeyUp="countMe(document.forms[0])" ',false); ?></td>
      </tr>
      <tr>
        <td valign="top" class="control-label main-text text-left"><b><?php echo FAQ_ANSWER; ?></b></td>
        <td valign="top"><?php echo tep_draw_textarea_field('answer', '', '60', '25', (isset($edit['answer']) ? $edit['answer'] : (isset($_GET['answer']) ? $_GET['answer'] : '')),' style="width: 100%" class="ckeditor"',false); ?></td>
      </tr>
      <tr>
        <td colspan="2" align="right">
          <?php
          echo '<a class="btn btn-default btn-sm mt-2 mb-2 mr-2"  href="' . tep_href_link(FILENAME_FAQ_MANAGER, '', 'NONSSL') . '">' . IMAGE_CANCEL . '</a><button class="btn btn-success btn-sm" type="submit">' . IMAGE_SAVE . '</button>';
          ?>
        </td>
      </tr>
    </table>
    </form>


            </div>
          </div>
        </div>   
        <!-- end body_text //-->
      </div>
      <!-- end panel -->
    </div>
  </div>
  <!-- body_eof //-->
