<?php
/*
  $Id: countries.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'insert':
      $countries_name = tep_db_prepare_input($_POST['countries_name']);
      $countries_iso_code_2 = tep_db_prepare_input($_POST['countries_iso_code_2']);
      $countries_iso_code_3 = tep_db_prepare_input($_POST['countries_iso_code_3']);
      $address_format_id = tep_db_prepare_input($_POST['address_format_id']);

      tep_db_query("insert into " . TABLE_COUNTRIES . " (countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id) values ('" . tep_db_input($countries_name) . "', '" . tep_db_input($countries_iso_code_2) . "', '" . tep_db_input($countries_iso_code_3) . "', '" . (int)$address_format_id . "')");

      tep_redirect(tep_href_link(FILENAME_COUNTRIES));
      break;
    case 'save':
      $countries_id = tep_db_prepare_input($_GET['cID']);
      $countries_name = tep_db_prepare_input($_POST['countries_name']);
      $countries_iso_code_2 = tep_db_prepare_input($_POST['countries_iso_code_2']);
      $countries_iso_code_3 = tep_db_prepare_input($_POST['countries_iso_code_3']);
      $address_format_id = tep_db_prepare_input($_POST['address_format_id']);

      tep_db_query("update " . TABLE_COUNTRIES . " set countries_name = '" . tep_db_input($countries_name) . "', countries_iso_code_2 = '" . tep_db_input($countries_iso_code_2) . "', countries_iso_code_3 = '" . tep_db_input($countries_iso_code_3) . "', address_format_id = '" . (int)$address_format_id . "' where countries_id = '" . (int)$countries_id . "'");

      tep_redirect(tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $countries_id));
      break;
    case 'deleteconfirm':
      $countries_id = tep_db_prepare_input($_GET['cID']);

      tep_db_query("delete from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$countries_id . "'");

      tep_redirect(tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page']));
    break;
    case 'setcountryStatus':
      $cID = tep_db_prepare_input($_GET['cID']);
      if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
        if (isset($cID)) {
          tep_set_country_status($cID, $_GET['flag']);
        }
         echo $_GET['flag'];exit;
      }
    break;
    case 'setcountryAllowed':
      $cID = tep_db_prepare_input($_GET['cID']);
      if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
        if (isset($cID)) {
          tep_set_country_allowed($cID, $_GET['flag']);
        }
         echo $_GET['flag'];exit;
      }
    break;
  }
}
include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-countries" class="table-countries">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_COUNTRY_NAME; ?></th>
                  <th scope="col" class="th-col dark text-left d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_COUNTRY_CODE_2; ?></th>
                  <th scope="col" class="th-col dark text-left d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_COUNTRY_CODE_3; ?></th>
                  <th scope="col" class="th-col dark text-center d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_COUNTRY_STATUS; ?></th>
                  <th scope="col" class="th-col dark text-center d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_COUNTRY_ALLOWED; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $countries_query_raw = "select countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, country_status, country_allowed from " . TABLE_COUNTRIES . " order by countries_name";
                $countries_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $countries_query_raw, $countries_query_numrows);
                $countries_query = tep_db_query($countries_query_raw);
                while ($countries = tep_db_fetch_array($countries_query)) {
                  if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $countries['countries_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
                    $cInfo = new objectInfo($countries);
                  }

                  $selected = (isset($cInfo) && is_object($cInfo) && ($countries['countries_id'] == $cInfo->countries_id))  ? ' selected' : '';
                  if ($selected) {
                    echo '<tr class="table-row dark selected" id="crow_'.$countries['countries_id'].'">' . "\n";
                  	$onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id . '&action=edit') . '\'"';
                  } else {
                    echo '<tr class="table-row dark" id="crow_'.$countries['countries_id'].'">' . "\n";
                    $onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $countries['countries_id']) . '\'"';
                  }
                  $col_selected = ($selected) ? ' selected' : '';
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo $countries['countries_name']; ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank" <?php echo $onclick;?>><?php echo $countries['countries_iso_code_2']; ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank" <?php echo $onclick;?>><?php echo $countries['countries_iso_code_3']; ?></td>
                  <td class="setcountryStatus table-col dark text-center<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank">
					  <?php
					  $ajax_link = tep_href_link(FILENAME_COUNTRIES);
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setcountryStatus\',\''.$ajax_link.'\',\'action=setcountryStatus&flag=0&cID='.$countries['countries_id'].'\', '.$countries['countries_id'].',0 )" '.(($countries['country_status'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setcountryStatus\',\''.$ajax_link.'\',\'action=setcountryStatus&flag=1&cID='.$countries['countries_id'].'\', '.$countries['countries_id'].',1  )" '.(($countries['country_status'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
					  ?>
                  </td>
                  <td class="setcountryAllowed table-col dark text-center<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank">
					  <?php
					  $ajax_link = tep_href_link(FILENAME_COUNTRIES);
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setcountryAllowed\',\''.$ajax_link.'\',\'action=setcountryAllowed&flag=0&cID='.$countries['countries_id'].'\', '.$countries['countries_id'].',0 )" '.(($countries['country_allowed'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setcountryAllowed\',\''.$ajax_link.'\',\'action=setcountryAllowed&flag=1&cID='.$countries['countries_id'].'\', '.$countries['countries_id'].',1  )" '.(($countries['country_allowed'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
					  ?>
                  </td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?>">
                    <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $countries['countries_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                  </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>

            <div class="pagination-container ml-2 mr-2">
              <div class="results-right"><?php echo $countries_split->display_count($countries_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_COUNTRIES); ?></div>
              <div class="results-left"><?php echo $countries_split->display_links($countries_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></div>
            </div>
            <?php
            if (empty($action)) {
              ?>
              <div class="float-right mr-2 mt-0 mb-3" role="group">
                <button class="btn btn-success btn-sm" onclick="window.location='<?php echo tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&action=new'); ?>'"><?php echo IMAGE_NEW_COUNTRY; ?></button>
              </div>
              <?php
            }
            ?>
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              switch ($action) {
                case 'new':
                  $heading[] = array('text' => TEXT_INFO_HEADING_NEW_COUNTRY);
                  $contents[] = array('form' => tep_draw_form('countries', FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&action=insert','post','data-parsley-validate'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_INSERT_INTRO . '</p></div></div></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_COUNTRY_NAME . '<span class="required"></span><span class="sidebar-title ml-2">' . tep_draw_input_field('countries_name', null, 'class="form-control" required') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_COUNTRY_CODE_2 . '<span class="required"></span><span class="sidebar-title ml-2">' . tep_draw_input_field('countries_iso_code_2', null, 'class="form-control" required') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_COUNTRY_CODE_3 . '<span class="required"></span><span class="sidebar-title ml-2">' . tep_draw_input_field('countries_iso_code_3', null, 'class="form-control" required') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_ADDRESS_FORMAT . '<span class="sidebar-title ml-2">' . tep_draw_pull_down_menu('address_format_id', tep_get_address_formats(), null, 'class="form-control"') . '</span></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-insert" type="submit">' . IMAGE_INSERT . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page']) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'edit':
                  $heading[] = array('text' => TEXT_INFO_HEADING_EDIT_COUNTRY);
                  $contents[] = array('form' => tep_draw_form('countries', FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id . '&action=save','post','data-parsley-validate'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_EDIT_INTRO . '</p></div></div></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_COUNTRY_NAME . '<span class="required"></span><span class="sidebar-title ml-2">' . tep_draw_input_field('countries_name', $cInfo->countries_name, 'class="form-control" required') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_COUNTRY_CODE_2 . '<span class="required"></span><span class="sidebar-title ml-2">' . tep_draw_input_field('countries_iso_code_2', $cInfo->countries_iso_code_2, 'class="form-control" required') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_COUNTRY_CODE_3 . '<span class="required"></span><span class="sidebar-title ml-2">' . tep_draw_input_field('countries_iso_code_3', $cInfo->countries_iso_code_3, 'class="form-control" required') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_ADDRESS_FORMAT . '<span class="sidebar-title ml-2">' . tep_draw_pull_down_menu('address_format_id', tep_get_address_formats(), $cInfo->address_format_id, 'class="form-control"') . '</span></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'delete':
                  $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_COUNTRY);
                  $contents[] = array('form' => tep_draw_form('countries', FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id . '&action=deleteconfirm'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . sprintf(TEXT_INFO_DELETE_INTRO, '<b>' . $cInfo->countries_name . '</b>') . '</div></div></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_CONFIRM_DELETE . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                default:
                  if (is_object($cInfo)) {
                    $heading[] = array('text' => '<div class="text-truncate">' . $cInfo->countries_name . '</div>');
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
                                    <button class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id . '&action=edit')  . '\'">' . IMAGE_EDIT . '</button>
                                    <button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_COUNTRIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id . '&action=delete') . '\'">' . IMAGE_DELETE . '</button>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_COUNTRY_NAME . '<span class="sidebar-title ml-2 f-w-600">' . $cInfo->countries_name . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_COUNTRY_CODE_2 . '<span class="sidebar-title ml-2 f-w-600">' . $cInfo->countries_iso_code_2 . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_COUNTRY_CODE_3 . '<span class="sidebar-title ml-2 f-w-600">' . $cInfo->countries_iso_code_3 . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_ADDRESS_FORMAT . '<span class="sidebar-title ml-2 f-w-600">' . $cInfo->address_format_id . '</span></div>');
                  }
                  break;
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>