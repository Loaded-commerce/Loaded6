<?php
/*
  $Id: currencies.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'insert':
    case 'save':
      if (isset($_GET['cID'])) $currency_id = tep_db_prepare_input($_GET['cID']);
      $title = tep_db_prepare_input($_POST['title']);
      $code = tep_db_prepare_input($_POST['code']);
      $symbol_left = tep_db_prepare_input($_POST['symbol_left']);
      $symbol_right = tep_db_prepare_input($_POST['symbol_right']);
      $decimal_point = tep_db_prepare_input($_POST['decimal_point']);
      $thousands_point = tep_db_prepare_input($_POST['thousands_point']);
      $decimal_places = tep_db_prepare_input($_POST['decimal_places']);
      $value = tep_db_prepare_input($_POST['value']);

      if(trim($symbol_left) == '�'){
        $symbol_left = '&#163;';
      }
      if(trim($symbol_right) == '�'){          
        $symbol_right = '&#163;';
      }

      $sql_data_array = array('title' => $title,
                              'code' => $code,
                              'symbol_left' => $symbol_left,
                              'symbol_right' => $symbol_right,
                              'decimal_point' => $decimal_point,
                              'thousands_point' => $thousands_point,
                              'decimal_places' => $decimal_places,
                              'value' => $value);

      if ($action == 'insert') {
        tep_db_perform(TABLE_CURRENCIES, $sql_data_array);
        $currency_id = tep_db_insert_id();
      } elseif ($action == 'save') {
        tep_db_perform(TABLE_CURRENCIES, $sql_data_array, 'update', "currencies_id = '" . (int)$currency_id . "'");
      }

      if (isset($_POST['default']) && ($_POST['default'] == 'on')) {
        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($code) . "' where configuration_key = 'DEFAULT_CURRENCY'");
      }

      tep_redirect(tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $currency_id));
      break;
    case 'deleteconfirm':
      $currencies_id = tep_db_prepare_input($_GET['cID']);

      $currency_query = tep_db_query("select currencies_id from " . TABLE_CURRENCIES . " where code = '" . DEFAULT_CURRENCY . "'");
      $currency = tep_db_fetch_array($currency_query);

      if ($currency['currencies_id'] == $currencies_id) {
        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '' where configuration_key = 'DEFAULT_CURRENCY'");
      }

      tep_db_query("delete from " . TABLE_CURRENCIES . " where currencies_id = '" . (int)$currencies_id . "'");

      tep_redirect(tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page']));
      break;
    case 'update':
      $server_used = CURRENCY_SERVER_PRIMARY;
      $currency_query = tep_db_query("select currencies_id, code, title from " . TABLE_CURRENCIES);
      while ($currency = tep_db_fetch_array($currency_query)) {
        $quote_function = 'quote_' . CURRENCY_SERVER_PRIMARY . '_currency';
        $rate = $quote_function($currency['code']);

        if (empty($rate) && (tep_not_null(CURRENCY_SERVER_BACKUP))) {
          $messageStack->add_session('currencies', sprintf(WARNING_PRIMARY_SERVER_FAILED, CURRENCY_SERVER_PRIMARY, $currency['title'], $currency['code']), 'warning');

          $quote_function = 'quote_' . CURRENCY_SERVER_BACKUP . '_currency';
          $rate = $quote_function($currency['code']);

          $server_used = CURRENCY_SERVER_BACKUP;
        } 

        if ($rate > 0) {
          tep_db_query("update " . TABLE_CURRENCIES . " set value = '" . $rate . "', last_updated = now() where currencies_id = '" . (int)$currency['currencies_id'] . "'");
          $messageStack->add_session('currencies', sprintf(TEXT_INFO_CURRENCY_UPDATED, $currency['title'], $currency['code'], $server_used), 'success');
        } else {
          $messageStack->add_session('currencies', sprintf(ERROR_CURRENCY_INVALID, $currency['title'], $currency['code'], $server_used), 'error');
        }
      }

      tep_redirect(tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $_GET['cID']));
      break;
    case 'delete':
      $currencies_id = tep_db_prepare_input($_GET['cID']);

      $currency_query = tep_db_query("select code from " . TABLE_CURRENCIES . " where currencies_id = '" . (int)$currencies_id . "'");
      $currency = tep_db_fetch_array($currency_query);

      $remove_currency = true;
      if ($currency['code'] == DEFAULT_CURRENCY) {
        $remove_currency = false;
        $messageStack->add('currencies', ERROR_REMOVE_DEFAULT_CURRENCY, 'error');
      }
      break;
  }
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('currencies') > 0) {
      echo $messageStack->output('currencies'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-currencies" class="table-currencies">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CURRENCY_NAME; ?></th>
                  <th scope="col" class="th-col dark text-left d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_CURRENCY_CODES; ?></th>
                  <th scope="col" class="th-col dark text-right d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_CURRENCY_VALUE; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody> 
                <?php
                $currency_query_raw = "select currencies_id, title, code, symbol_left, symbol_right, decimal_point, thousands_point, decimal_places, last_updated, value from " . TABLE_CURRENCIES . " order by title";
                $currency_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $currency_query_raw, $currency_query_numrows);
                $currency_query = tep_db_query($currency_query_raw);
                while ($currency = tep_db_fetch_array($currency_query)) {
                  if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $currency['currencies_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
                    $cInfo = new objectInfo($currency);
                  }

                  $selected = (isset($cInfo) && is_object($cInfo) && ($currency['currencies_id'] == $cInfo->currencies_id)) ? ' selected' : '';

                  if ($selected) {
                    echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->currencies_id . '&action=edit') . '\'">' . "\n";
                  } else {
                    echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $currency['currencies_id']) . '\'">' . "\n";
                  }
                  $col_selected = ($selected) ? ' selected' : '';

                  if (DEFAULT_CURRENCY == $currency['code']) {
                    echo '<td class="table-col dark text-left' . $col_selected . '"><span class="f-w-600">' . $currency['title'] . ' (' . TEXT_DEFAULT . ')</span></td>' . "\n";
                  } else {
                    echo '<td class="table-col dark text-left' . $col_selected . '">' . $currency['title'] . '</td>' . "\n";
                  }
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $currency['code']; ?></td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank"><?php echo number_format($currency['value'], 8); ?></td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank"><?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $currency['currencies_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                </tr>
                <?php
              }
              ?>
              </tbody>
            </table>    

            <div class="pagination-container ml-2 mr-2">
              <div class="results-right"><?php echo $currency_split->display_count($currency_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CURRENCIES); ?></div>
              <div class="results-left"><?php echo  $currency_split->display_links($currency_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></div>
            </div>

            <div class="float-right mr-2 mt-3 mb-3" role="group">
              <?php 
              if (CURRENCY_SERVER_PRIMARY) { 
                echo '<a class="btn btn-success btn-sm mr-1" href="' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->currencies_id . '&action=update') . '">' . IMAGE_UPDATE . '</a>';
              }
              ?>
              <button class="btn btn-success btn-sm" onclick="window.location='<?php echo tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->currencies_id . '&action=new'); ?>'"><?php echo IMAGE_NEW_CURRENCY; ?></button> 
            </div>                         

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              switch ($action) {
                case 'new':
                  $heading[] = array('text' => TEXT_INFO_HEADING_NEW_CURRENCY);
                  $contents[] = array('form' => tep_draw_form('currencies', FILENAME_CURRENCIES, 'page=' . $_GET['page'] . (isset($cInfo) ? '&cID=' . $cInfo->currencies_id : '') . '&action=insert','POST','data-parsley-validate'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_INSERT_INTRO . '</p></div></div></div>');                                           
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_CURRENCY_TITLE . '<span class="sidebar-title ml-2">' . tep_draw_input_field('title', null, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_CODE . '<span class="sidebar-title ml-2">' . tep_draw_input_field('code', null, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_SYMBOL_LEFT . '<span class="sidebar-title ml-2">' . tep_draw_input_field('symbol_left', null, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_SYMBOL_RIGHT . '<span class="sidebar-title ml-2">' . tep_draw_input_field('symbol_right', null, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_DECIMAL_POINT . '<span class="sidebar-title ml-2">' . tep_draw_input_field('decimal_point', null, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_THOUSANDS_POINT . '<span class="sidebar-title ml-2">' . tep_draw_input_field('thousands_point', null, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_DECIMAL_PLACES . '<span class="sidebar-title ml-2">' . tep_draw_input_field('decimal_places', null, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_VALUE . '<span class="sidebar-title ml-2">' . tep_draw_input_field('value', null, 'class="form-control mb-3"') . '</span></div>');
                  $contents[] = array('text' => '<div class="col d-sm-inline p-0 mt-2"><label class="control-label ml-3 mr-2 main-text">' . TEXT_SET_DEFAULT . '</label><input type="checkbox" name="default" class="js-switch js-default"></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-insert" type="submit">' . IMAGE_INSERT . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page']) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'edit':
                  $heading[] = array('text' => TEXT_INFO_HEADING_EDIT_CURRENCY);
                  $contents[] = array('form' => tep_draw_form('currencies', FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->currencies_id . '&action=save','POST','data-parsley-validate'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_EDIT_INTRO . '</p></div></div></div>');                                           
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_CURRENCY_TITLE . '<span class="sidebar-title ml-2">' . tep_draw_input_field('title', $cInfo->title, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_CODE . '<span class="sidebar-title ml-2">' . tep_draw_input_field('code', $cInfo->code, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_SYMBOL_LEFT . '<span class="sidebar-title ml-2">' . tep_draw_input_field('symbol_left', $cInfo->symbol_left, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_SYMBOL_RIGHT . '<span class="sidebar-title ml-2">' . tep_draw_input_field('symbol_right', $cInfo->symbol_right, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_DECIMAL_POINT . '<span class="sidebar-title ml-2">' . tep_draw_input_field('decimal_point', $cInfo->decimal_point, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_THOUSANDS_POINT . '<span class="sidebar-title ml-2">' . tep_draw_input_field('thousands_point', $cInfo->thousands_point, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_DECIMAL_PLACES . '<span class="sidebar-title ml-2">' . tep_draw_input_field('decimal_places', $cInfo->decimal_places, 'class="form-control mb-2"') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENCY_VALUE . '<span class="sidebar-title ml-2">' . tep_draw_input_field('value', $cInfo->value, 'class="form-control mb-3"') . '</span></div>');
                  if (DEFAULT_CURRENCY != $cInfo->code) $contents[] = array('text' => '<div class="col d-sm-inline p-0 mt-2"><label class="control-label ml-3 mr-2 main-text">' . TEXT_SET_DEFAULT . '</label><input type="checkbox" name="default" class="js-switch js-default"></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->currencies_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'delete':
                  $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_CURRENCY);
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0 fw-400">' . sprintf(TEXT_INFO_DELETE_INTRO, $cInfo->title) . '</div></div></div>');
                  $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">' .
                    (($remove_currency) ? '<button class="btn btn-danger btn-sm mt-2 mb-2 btn-deleteconfirm" onclick="window.location=\'' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->currencies_id . '&action=deleteconfirm')  . '\'">' . IMAGE_CONFIRM_DELETE . '</button>' : '') . '<button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->currencies_id)  . '\'">' . IMAGE_CANCEL . '</button></div>');
                  break;
                default:
                  if (is_object($cInfo)) {
                    $heading[] = array('text' => $cInfo->title);
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
                        <button class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->currencies_id . '&action=edit') . '\'">' . IMAGE_EDIT . '</button>
                        <button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $cInfo->currencies_id . '&action=delete') . '\'">' . IMAGE_DELETE . '</button>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_CURRENCY_TITLE . '<span class="sidebar-title ml-2">' . $cInfo->title . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_CURRENCY_CODE . '<span class="sidebar-title ml-2">' . $cInfo->code . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_CURRENCY_SYMBOL_LEFT . '<span class="sidebar-title ml-2">' . $cInfo->symbol_left . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_CURRENCY_SYMBOL_RIGHT . '<span class="sidebar-title ml-2">' . $cInfo->symbol_right . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_CURRENCY_DECIMAL_POINT . '<span class="sidebar-title ml-2">' . $cInfo->decimal_point . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_CURRENCY_THOUSANDS_POINT . '<span class="sidebar-title ml-2">' . $cInfo->thousands_point . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_CURRENCY_DECIMAL_PLACES . '<span class="sidebar-title ml-2">' . $cInfo->decimal_places . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_CURRENCY_LAST_UPDATED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->last_updated) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_CURRENCY_VALUE . '<span class="sidebar-title ml-2">' . number_format($cInfo->value, 8) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1 mb-3">' . TEXT_INFO_CURRENCY_EXAMPLE . '<span class="sidebar-title ml-2">' . $currencies->format('30', false, DEFAULT_CURRENCY) . ' = ' . $currencies->format('30', true, $cInfo->code) . '</span></div>');
                  }
                  break;
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function(){

  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small', 
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  }); 
}); 
</script>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>