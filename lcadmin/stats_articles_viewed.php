<?php
/*
  $Id: stats_articles_viewed.php,v 1.29 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
  
  Chain Reaction Works, Inc.
  
  Copyright &copy; 2006 Chain Reaction Works, Inc
  
  Last Modifed by : $Author$
  Last Modified on : $Date$
  Latest Revision : $Revision: 300 $
  
  
*/

  require('includes/application_top.php');

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
 
  <div class="col">   
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-statsproductspurchased" class="table-statsproductspurchased">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">


            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_NUMBER; ?></th>
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_ARTICLES; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_VIEWED; ?>&nbsp;</th>
              </tr>
             </thead>
<?php
  if (isset($_GET['page']) && ($_GET['page'] > 1)) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS - MAX_DISPLAY_SEARCH_RESULTS;
  $rows = 0;
  $articles_query_raw = "select p.articles_id, pd.articles_name, pd.articles_viewed, l.name from " . TABLE_ARTICLES . " p, " . TABLE_ARTICLES_DESCRIPTION . " pd, " . TABLE_LANGUAGES . " l where p.articles_id = pd.articles_id and l.languages_id = pd.language_id order by pd.articles_viewed DESC";
  $articles_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $articles_query_raw, $articles_query_numrows);
  $articles_query = tep_db_query($articles_query_raw);
  while ($articles = tep_db_fetch_array($articles_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
?>
              <tr class="dataTableRow" onclick="document.location.href='<?php echo tep_href_link(FILENAME_CATEGORIES, 'action=new_article_preview&read=only&pID=' . $articles['articles_id'] . '&origin=' . FILENAME_STATS_ARTICLES_VIEWED . '?page=' . $_GET['page'], 'NONSSL'); ?>'">
                <td class="table-col dark text-left"><?php echo $rows; ?>.</td>
                <td class="table-col dark text-left"><?php echo '<a href="' . tep_href_link(FILENAME_CATEGORIES, 'action=new_product_preview&read=only&pID=' . $articles['articles_id'] . '&origin=' . FILENAME_STATS_ARTICLES_VIEWED . '?page=' . $_GET['page'], 'NONSSL') . '">' . $articles['articles_name'] . '</a> (' . $articles['name'] . ')'; ?></td>
                <td class="table-col dark text-center"><?php echo $articles['articles_viewed']; ?>&nbsp;</td>
              </tr>
<?php
  }
?>
            </table>
            
            <table border="0" width="100%" cellspacing="0" cellpadding="0" class="data-table-foot">
              <tr>
                <td class="smallText" valign="top"><?php echo $articles_split->display_count($articles_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ARTICLES); ?></td>
                <td class="smallText" align="right"><?php echo $articles_split->display_links($articles_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
              </tr>
            </table>
        
			<div class="mb-1">&nbsp;</div>
		 </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>