<?php
    chdir('../../');
    require('includes/application_top.php');
	define('TEXT_FEATURED_PRODUCT', 'Product:');
	define('TEXT_FEATURED_EXPIRES_DATE', 'Expiry Date:');
	define('IMAGE_CANCEL', 'Cancel');
	define('IMAGE_UPDATE', 'Update');
	define('FILENAME_FEATURED', 'featured.php');
    $action = (isset($_GET['action']) ? $_GET['action'] : '');
	$form_action = 'insert';
	$product_query = tep_db_query("SELECT p.products_id, pd.products_name, p.featured_expires
									 from " . TABLE_PRODUCTS . " p,
										  " . TABLE_PRODUCTS_DESCRIPTION . " pd
								   WHERE p.products_id = pd.products_id
									 and pd.language_id = '" . $languages_id . "'
									 and p.products_id = '" . $_GET['sID'] . "'
								   ORDER BY pd.products_name");
	$product = tep_db_fetch_array($product_query);
	echo tep_draw_form('new_feature', FILENAME_FEATURED.'?page=' . $_GET['page'] . '&sID=' . $_GET['sID'] . '&action=insert','POST','');
	echo tep_draw_hidden_field('products_id', $_GET['sID']);
?>
		<table border="0" cellspacing="5" cellpadding="2" class="data-table mt-3">
		  <tr>
			<td class="main"><?php echo TEXT_FEATURED_PRODUCT; ?>&nbsp;</td>
			<td class="main">
					 <?php echo $product['products_name']; ?>
			</td>
		  </tr>
		  <tr>
			<td class="main"><?php echo TEXT_FEATURED_EXPIRES_DATE; ?>&nbsp;</td>
			<td class="main"><?php echo tep_draw_input_field('expires_date',(($product['featured_expires'] == '' || $product['featured_expires'] == '0000-00-00')?'':date('m/d/Y', strtotime($product['featured_expires']))),'class="calender form-control" id="datepickeredit" data-date-format="mm/dd/yyyy"'); ?></td>
		  </tr>
		  <tr>
			<td align="center" colspan="2"><br><?php echo '<button type="button" class="btn btn-default btn-sm mt-2 mb-2 mr-2" data-dismiss="modal">'.IMAGE_CANCEL.'</button><button class="btn btn-success btn-sm" type="submit">'. IMAGE_UPDATE .'</button>'; ?>
			</td>
		  </tr>
		</table>
	  </form>

<script>
$(function () {
	$('#datepickeredit').datepicker({
		todayHighlight: true
	});
});
</script>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
