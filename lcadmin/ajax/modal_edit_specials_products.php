   <?php
   /*<script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="assets/plugins/pace/pace.min.js"></script>
    <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
    <script src="assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>*/?>
<?php
    chdir('../../');
    require('includes/application_top.php');
    include(DIR_WS_LANGUAGES . $language . '/specials.php');
    $action = (isset($_GET['action']) ? $_GET['action'] : '');
    define('TEXT_SPECIALS_SPECIAL_PRICE', 'Special Price:');
	define('TEXT_SPECIALS_EXPIRES_DATE', 'Expiry Date:');
	define('FILENAME_SPECIALS', 'specials.php');
    define('TEXT_SPECIALS_PRICE_TIP', '<b>Specials Notes:</b><ul><li>You can enter a percentage to deduct in the Specials Price field, for example: <b>20%</b></li><li>If you enter a new price, the decimal separator must be a \'.\' (decimal-point), example: <b>49.99</b></li><li>Leave the expiry date empty for no expiration</li></ul>');
    if ( ($action == 'edit') && isset($_GET['sID']) ) {
      $form_action = 'update';

// Eversun mod for sppc and qty price breaks
//    $product_query = tep_db_query("select p.products_id, pd.products_name, p.products_price, s.specials_new_products_price, s.expires_date from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_SPECIALS . " s where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and p.products_id = s.products_id and s.specials_id = '" . (int)$_GET['sID'] . "'");
      $product_query = tep_db_query("select p.products_id, pd.products_name, p.products_price, s.specials_new_products_price, s.expires_date from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_SPECIALS . " s where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and p.products_id = s.products_id and s.specials_id = '" . (int)$_GET['sID'] . "'");
// Eversun mod end for sppc and qty price breaks
      $product = tep_db_fetch_array($product_query);
      //$sInfo = new objectInfo($product);
    } else {
      $sInfo = new objectInfo(array());

      // if it is active or not, get it if it is in the specials
      $on_special_array = array();
      $on_special_query = tep_db_query("SELECT DISTINCT products_id
                                        FROM " .  TABLE_SPECIALS . "
                                        ORDER BY products_id
                                       ");
      while ($on_special = tep_db_fetch_array($on_special_query)) {
        $on_special_array[] = $on_special['products_id'];
      }
    }
    echo tep_draw_form('specials', FILENAME_SPECIALS.'?page=' . $_GET['page'] . '&sID=' . $_GET['sID'] . '&action=update','POST','');
?>

      <?php if ($form_action == 'update') echo tep_draw_hidden_field('specials_id', $_GET['sID']); ?>
		  <div class="form-group row mb-3 mt-3">
			<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo "Product:"; ?></label>
			<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
			   <?php // echo (isset($sInfo->products_name)) ? $sInfo->products_name . ' <small>(' . $currencies->format($sInfo->products_price) . ')</small>' : tep_draw_products_pull_down('products_id', 'style="font-size:10px"', $on_special_array); echo tep_draw_hidden_field('products_price', (isset($sInfo->products_price) ? $sInfo->products_price : '')); ?>
               <div id="replacediv" class=" well" style="padding:0px;margin-bottom: 0px;border: none;">
				  <div id="search_section">
					 <?php echo ((isset($product['products_name']))? $product['products_name'] :'<input type="text" class="form-control custom-search cust-input ac_input" id="special_data_info" autocomplete="off" name="products_id" value="" height="50px;"  placeholder="Search Product...." style="">'); ?>
					 <?php  if (isset($product['products_price'])) {
						echo tep_draw_hidden_field('products_price', $product['products_price']);
					  } else {
						echo tep_draw_hidden_field('products_price', 0);
					  } ?>
				  </div>
			 </div>
			</div>
		 </div>
		  <div class="form-group row mb-3 mt-3">
			<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_SPECIALS_SPECIAL_PRICE ;?></label>
			<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input"><?php echo tep_draw_input_field('specials_price', (isset($product['specials_new_products_price']) ? $product['specials_new_products_price'] : ''), 'required="required" class="form-control"'); ?></div>
		  </div>
		  <div class="form-group row mb-3 mt-3">
			<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_SPECIALS_EXPIRES_DATE ;?></label>
			<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input"><?php echo tep_draw_input_field('expires_date',(($product['expires_date'] == '' || $product['expires_date'] == '0000-00-00 00:00:00')?'':date('m/d/Y', strtotime($product['expires_date']))),'class="calender form-control" id="datepickeredit" data-date-format="mm/dd/yyyy"'); ?></div>
		  </div>
		 <?php
		  // RCI for inserting the extra fields
		  echo $cre_RCI->get('specials', 'bottominsideform');
		 ?>
		  <div class="form-group row mb-3 mt-3">
			<div class="col-xs-12 col-md-12 col-lg-12 p-r-10 meta-input text-right"><?php echo (($form_action == 'insert') ? '<button class="btn btn-success btn-sm" type="submit">' . IMAGE_INSERT . '</button>' : '<button class="btn btn-success btn-sm" type="submit">Update</button>'); ?></div>
		  </div>
		  <div class="sidebar-title mt-2">
		    <div class="row">
		  	  <div class="col p-0 mt-3 ml-2 mr-2">
			    <div class="note note-warning m-0">
				  <p class="mb-0 mt-0 f-w-400"><?php echo TEXT_SPECIALS_PRICE_TIP; ?></p>
			    </div>
			  </div>
		    </div>
		  </div>

      </form>
<script>
$(function () {
	$('#datepickeredit').datepicker({
	todayHighlight: true
	});
});
</script>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
