<?php
    chdir('../../');
    require('includes/application_top.php');
    $languages = tep_get_languages();
  $values[0]=array ('id' =>'0', 'text' => 'All');
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
  $values[$i+1]=array ('id' =>$languages[$i]['id'], 'text' =>$languages[$i]['name']);
  }
  //print_r($values);
    // Put languages information into an array for drop-down boxes

  $ftype[0]=array ('id' =>'1', 'text' => 'Text');
  $ftype[1]=array ('id' =>'2', 'text' => 'Dropdown');
  $ftype[2]=array ('id' =>'3', 'text' => 'Radio');

 /*   $values = tep_db_query("select distinct pov.products_options_values_id, pov.products_options_values_name, pov2po.products_options_id from
    " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov,
    " . TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS . " pov2po
    where
   pov2po.products_options_values_id = pov.products_options_values_id and
    pov.language_id = '" . (int)$languages_id . "' and pov.products_options_values_id = '" . $_GET['editOpValID'] . "'");
    $values_values = tep_db_fetch_array($values);

    for ($i = 0, $n = sizeof($languages); $i < $n; $i ++) {
        $value_name = tep_db_query("select products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " where products_options_values_id = '" . $_GET['editOpValID'] . "' and language_id = '" . $languages[$i]['id'] . "'");
        $value_name = tep_db_fetch_array($value_name);
        $option_value_name .= '<input type="text" name="value_name[' . $languages[$i]['id'] . ']" value="' . htmlspecialchars($value_name['products_options_values_name']) . '" class="form-control">';
    } */

?>
<form name="add_field" class="form-horizontal" action="<?php echo tep_href_link(FILENAME_PRODUCTS_EXTRA_FIELDS, 'action=add', 'NONSSL');?>" method="post" onSubmit="return filedformvalidate();">
    <input type="hidden" name="value_id" value="<?php echo $_GET['editOpValID']; ?>">
    <div class="row">
        <div class="col-sm-12">

            <div class="form-group">
                <label class="col-md-3 control-label">Field Name</label>
                <div class="col-md-5">
                         <?php echo tep_draw_input_field('field[name]', (isset($field['name']) ? $field['name'] : ''), 'id="fields_name" size=30', false, 'text', true);?>
                    	 <p id="responseses" style="margin:0px;"></p>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
			<div class="form-group">
				<label class="col-md-3 control-label">Fields Type</label>
				<div class="col-md-5">
				 <?php echo tep_draw_pull_down_menu('field[type]',$ftype, '0', '');?>
				 <!--<select name="products_extra_fields_type" class="form-control">
				 	<option value="1"<?php if($field['products_extra_fields_type'] == '1'){ echo ' selected="selected"'; } ?>>Text</option>
				 	<option value="2"<?php if($field['products_extra_fields_type'] == '2'){ echo ' selected="selected"'; } ?>>Dropdown</option>
				 	<option value="3"<?php if($field['products_extra_fields_type'] == '3'){ echo ' selected="selected"'; } ?>>Radio</option>
				 </select>-->
				 </div>
			</div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-md-3 control-label">Sort Order</label>
                <div class="col-md-5">
                 <?php echo tep_draw_input_field('field[order]', (isset($field['order']) ? $field['order'] : ''), 'size=5', false, 'text', true);?></div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <!--<label class="col-md-3 control-label">Language</label>-->
                <div class="col-md-5">
                    <?php /* echo tep_draw_pull_down_menu('field[language]', $values, '0', ''); */ ?>
                     <input type="hidden" name="field[language]" value="<?php echo $languages_id;?>">

                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-3">
                    <button type="submit" id="insert_extra_fields" class="btn btn-primary m-r-5"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>