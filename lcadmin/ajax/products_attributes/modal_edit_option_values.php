<?php
    chdir('../../');
    require('includes/application_top.php');
    $languages = tep_get_languages();

    $values = tep_db_query("select distinct pov.products_options_values_id, pov.products_options_values_name, pov2po.products_options_id from
    " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov,
    " . TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS . " pov2po
    where
    pov2po.products_options_values_id = pov.products_options_values_id and
    pov.language_id = '" . (int)$languages_id . "' and pov.products_options_values_id = '" . $_GET['editOpValID'] . "'");
    $values_values = tep_db_fetch_array($values);
	$products_options_id = $values_values['products_options_id'];

	$attr_arr_query  = tep_db_query("select pov.products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov, " . TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS . " p2p WHERE pov.products_options_values_id = p2p.products_options_values_id and p2p.products_options_id = '" .$products_options_id. "' and pov.products_options_values_id != '".$_GET['editOpValID']."' and pov.language_id = '" . $languages_id . "' order by pov.products_options_values_name");
	if(tep_db_num_rows($attr_arr_query) > 0){
		while($attr_arr = tep_db_fetch_array($attr_arr_query)){
			$mk_attr_arr[] = $attr_arr['products_options_values_name'];
		}
	}else{
		$mk_attr_arr[] = 'nodata-available';
	}
    $mk_js_attr_arr = json_encode($mk_attr_arr);

    for ($i = 0, $n = sizeof($languages); $i < $n; $i ++) {
        $value_name = tep_db_query("select products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " where products_options_values_id = '" . $_GET['editOpValID'] . "' and language_id = '" . $languages[$i]['id'] . "'");
        $value_name = tep_db_fetch_array($value_name);
        $option_value_name .= '<div class="input-group m-b-5"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span><input type="text" id="value_name_edit" name="value_name[' . $languages[$i]['id'] . ']" value="' . htmlspecialchars($value_name['products_options_values_name']) . '" class="form-control"></div>';
    }


?>
<form name="EditOptionValues" class="form-horizontal" action="<?php echo tep_href_link(FILENAME_PRODUCTS_ATTRIBUTES, 'action=update_value', 'NONSSL');?>" method="post" onSubmit="return checkoptvaluesname();">
    <input type="hidden" name="value_id" value="<?php echo $_GET['editOpValID']; ?>">
    <textarea name="attributesname" style="display:none;"><?php echo strtolower($mk_js_attr_arr); ?></textarea>
    <div class="row">
        <div class="col-sm-12">

            <div class="form-group" style="display:none;">
                <label class="col-md-3 control-label">Select Option</label>
                <div class="col-md-5">
                    <select class="form-control" name="option_id">
                        <?php
                            $options = tep_db_query("select po.products_options_id, pot.products_options_name from " . TABLE_PRODUCTS_OPTIONS . " AS po, " . TABLE_PRODUCTS_OPTIONS_TEXT  . " AS pot where po.options_type in (0,2,3,5) and po.products_options_id = pot.products_options_text_id  and pot.language_id = '" . (int)$languages_id . "' order by po.products_options_sort_order, pot.products_options_name");
                            while ($options_values = tep_db_fetch_array($options)) {
                                echo "\n" . '<option name="' . htmlspecialchars($options_values['products_options_name']) . '" value="' . $options_values['products_options_id'] . '"';
                                if ($values_values['products_options_id'] == $options_values['products_options_id']) {
                                    echo ' selected';
                                }
                                echo '>' . htmlspecialchars($options_values['products_options_name']) . '</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-md-3 control-label">Option Name</label>
                <div class="col-md-5">
					<?php
						$options = tep_db_query("select po.products_options_id, pot.products_options_name from " . TABLE_PRODUCTS_OPTIONS . " AS po, " . TABLE_PRODUCTS_OPTIONS_TEXT  . " AS pot where po.options_type in (0,2,3,5) and po.products_options_id = pot.products_options_text_id  and pot.language_id = '" . (int)$languages_id . "' and po.products_options_id = '".$products_options_id."'  order by po.products_options_sort_order, pot.products_options_name");
						$options_values = tep_db_fetch_array($options);
                        echo '<label class="control-label">'.htmlspecialchars($options_values['products_options_name']).'</label>';
                    ?>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-md-3 control-label">Option Value</label>
                <div class="col-md-5">
                    <?php
                        echo $option_value_name;
                    ?>
                    <p id="edit_opt_value_responsive"></p>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary m-r-5"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>