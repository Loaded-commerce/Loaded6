<?php
    chdir('../../');
    require('includes/application_top.php');
    $languages = tep_get_languages();
  $values[0]=array ('id' =>'0', 'text' => 'All');
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
  $values[$i+1]=array ('id' =>$languages[$i]['id'], 'text' =>$languages[$i]['name']);
  }
  //print_r($values);

  $ftype[0]=array ('id' =>'1', 'text' => 'Text');
  $ftype[1]=array ('id' =>'2', 'text' => 'Dropdown');
  $ftype[2]=array ('id' =>'3', 'text' => 'Radio');

 $query = "select products_extra_fields_id,products_extra_fields_name,products_extra_fields_type from products_extra_fields where products_extra_fields_id = '".$_GET['ID']."'";
 $query_rw = tep_db_query($query);
 $row = tep_db_fetch_array($query_rw);
 $products_extra_fields_id = $row['products_extra_fields_id'];
  if($row['products_extra_fields_type'] == '2'){
	 $fields_type = 'Dropdown';
	}else if($row['products_extra_fields_type'] == '3'){
	  $fields_type = 'Radio';
	 }
 echo tep_draw_form('add_field', FILENAME_PRODUCTS_EXTRA_FIELDS, 'action=add_field_radio', 'post','id="validate_form"');
?>
<style>
.modal-dialog{max-width:1000px !important;width:1000px !important;}
.valuename{font-size:17px;margin-bottom:6px;}
</style>

			   <input type="hidden" name="field_id" value="<?php echo $_GET['ID'];?>">
               <span class="valuename"><b><?php echo $row['products_extra_fields_name'];?> - <?php echo $fields_type;?></b></span>
               <div class="table-responsive" style="border:1px solid #ddd;">
				 <table class="table">
				 <thead>
				   <tr class="dataTableHeadingRow">
					   <td class="dataTableHeadingContent">Option Text</td>
					   <td class="dataTableHeadingContent">Option Value</td>
					   <td class="dataTableHeadingContent">Option Order</td>
					   <td class="dataTableHeadingContent" align="center"></td>
				  </tr>
				</thead>
					<tbody>
						<tr>
						<td class="dataTableContent">
						<?php echo tep_draw_input_field('field[pe_text]', (isset($field['pe_text']) ? $field['pe_text'] : ''), 'required size=30', false, 'text', true);?>
						</td>
						<td class="dataTableContent">
						 <?php echo tep_draw_input_field('field[pe_value]', (isset($field['pe_value']) ? $field['pe_value'] : ''), 'required size=30', false, 'text', true);?>
						</td>
						<td class="dataTableContent">
						 <?php echo tep_draw_input_field('field[pe_sortorder]', (isset($field['pe_sortorder']) ? $field['pe_sortorder'] : ''), 'size=2', false, 'text', true);?>
						</td>
						<td class="dataTableContent" align="right">
						<input type="submit" class="btn btn-primary m-r-5" name="submit" value="Add new field value">
						</td>
					  </tr>
				  </tbody>
			 </table></div>
                </form>
         <?php
               $products_extra_fields_values_query = tep_db_query("SELECT * FROM products_extra_fields_values WHERE pe_id = '".$_GET['ID']."' ORDER BY pe_sortorder");
               if(tep_db_num_rows($products_extra_fields_values_query) > 0){
               echo tep_draw_form('extra_fields',FILENAME_PRODUCTS_EXTRA_FIELDS, 'action=update_field_radio', 'post','id="validate_form"');?>
				<input type="hidden" name="field_id" value="<?php echo $_GET['ID'];?>">
			     <div class="table-responsive" style="border:1px solid #ddd;margin-top:10px;">
						<table class="table">
						 <thead>
						   <tr class="dataTableHeadingRow">
							<td class="dataTableHeadingContent" width="20">&nbsp;</td>
							<td class="dataTableHeadingContent">Option Text</td>
							<td class="dataTableHeadingContent">Option Values</td>
							<td class="dataTableHeadingContent">Option Order</td>
						   </tr>
						 </thead>
						 <tbody>
			<?php $sorting = 0;
				//$products_extra_fields_values_query = tep_db_query("SELECT * FROM products_extra_fields_values WHERE pe_id = '".$_GET['ID']."' ORDER BY pe_sortorder");
				while ($extra_fields = tep_db_fetch_array($products_extra_fields_values_query)) {
				$sorting += 10;
			?>
 				<tr>
				<td width="20"><?php echo tep_draw_checkbox_field('mark['.$extra_fields['pe_value_id'].']', 1);?></td>
				<td class="dataTableContent">
				<?php echo tep_draw_input_field('field['.$extra_fields['pe_value_id'].'][pe_text]', $extra_fields['pe_text'], 'required size=30', false, 'text', true);?>
				</td>
				<td class="dataTableContent">
				<?php echo tep_draw_hidden_field('field['.$extra_fields['pe_value_id'].'][pe_hidden_value]', $extra_fields['pe_value'], 'required size=30', false, 'text', true);?>
				<?php echo tep_draw_input_field('field['.$extra_fields['pe_value_id'].'][pe_value]', $extra_fields['pe_value'], 'required size=30', false, 'text', true);?>
				</td>
				<td class="dataTableContent" align="center">
				<?php echo tep_draw_input_field('field['.$extra_fields['pe_value_id'].'][pe_sortorder]', $sorting, ' style="width:70px" ', false, 'text', true);?>
				</td>
				</tr>
             <?php } ?>
              </table>
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
		   <tr>
			<td colspan="4">
				<input type="submit" name="update" class="btn btn-primary m-r-5" onclick="document.extra_fields.action=\'' . str_replace('&amp;', '&', tep_href_link('product_extra_fields.php', 'action=update_field_radio', 'SSL')) . '\';" value="Update field values" style="float:right;">
				<?php echo '<input type="submit" name="remove" class="btn btn-danger" value="Remove selected field values" onclick="if (! confirm(\'Are you sure want to delete the selected field?\')) { return false; } else { document.extra_fields.action=\'' . str_replace('&amp;', '&', tep_href_link('product_extra_fields.php', 'action=remove_radio_field', 'SSL')) . '\';}" >';?>
			</td>
		  </tr>
		   </form>
		  </table>
		  <?php } ?>

<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>