<?php
	if($_GET['type'] == 'delete'){
		$title = 'Please Select an Option from Dropdown to Delete.';
	} elseif($_GET['type'] == 'edit') {
		$title = 'Please Select an Option from Dropdown to Edit.';
	} elseif($_GET['type'] == 'add') {
		$title = 'Please choose an option from the dropdown list!.';
	}
?>
<div class="">
	<div class="alert alert-warning">
	  <strong>Warning!!</strong> <?php echo $title; ?>
	</div>
	<div class="clearfix"></div>
	<button type="button" class="btn btn-sm btn-white m-r-5 pull-right" data-dismiss="modal">Close</button>
</div>