<?php
    chdir('../../');
    require('includes/application_top.php');
    $languages = tep_get_languages();
    include_once(DIR_WS_LANGUAGES . $language . '/products_attributes.php');

?>
<div class="row">
    <div class="col-sm-12">
		<div class="alert alert-danger">Are You Sure you want to Delete??</div>
    </div>
    <div class="col-sm-12">
            <div class="col-md-10">
                <a href="javascript:;" class="btn btn-sm btn-white m-r-5" data-dismiss="modal">Close</a> <?php echo '<a class="btn btn-sm btn-danger" href="' . tep_href_link(FILENAME_ATTRIBUTES, 'action=delete_option_per_product&products_id='.$_GET['pID'].'&option_id=' . $_GET['delOpID'], 'NONSSL') . '">'; ?>Delete</a>
            </div>
        </div>
</div>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>