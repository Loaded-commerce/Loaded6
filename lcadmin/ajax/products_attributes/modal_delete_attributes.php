<?php
    chdir('../../');
    require('includes/application_top.php');
    $languages = tep_get_languages();
    include_once(DIR_WS_LANGUAGES . $language . '/products_attributes.php');

?>
<div class="row">
    <div class="col-sm-12">
		<div class="alert alert-danger">Are You Sure you want to Delete??</div>
    </div>
    <div class="col-sm-12">
            <div class="col-md-10">
            	  <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
                  <a href="<?php echo tep_href_link(FILENAME_PRODUCTS_ATTRIBUTES, 'action=delete_attribute&attribute_id=' . $_GET['deleteAttributelID'],'NONSSL');?>"  class="btn btn-sm btn-danger">Delete</a>
            </div>
        </div>
</div>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>