<?php
chdir('../../');
require('includes/application_top.php');
$languages = tep_get_languages();
function checkassignedattributes($value_id){
	$checkquery = tep_db_query("select `products_attributes_id` FROM `products_attributes` WHERE `options_values_id` = '".$value_id."'");
	if(tep_db_num_rows($checkquery) > 0){
		return true;
	}else{
		return false;
	}
}
echo '<form name="EditAttribute" class="form-horizontal" action="' . tep_href_link(FILENAME_ATTRIBUTES, 'action=add_options_values', 'NONSSL') . '" method="post" onsubmit="return checkattributesname('.$_GET['options_id'].')">';
$max_values_id_query = tep_db_query("select max(products_options_values_id) + 1 as next_id from " . TABLE_PRODUCTS_OPTIONS_VALUES);
$max_values_id_values = tep_db_fetch_array($max_values_id_query);
$next_id = $max_values_id_values['next_id'];

$attr_arr_query  = tep_db_query("select pov.products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov, " . TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS . " p2p WHERE pov.products_options_values_id = p2p.products_options_values_id and p2p.products_options_id = '" .$_GET['options_id']. "' and pov.language_id = '" . $languages_id . "' order by pov.products_options_values_name");
while($attr_arr = tep_db_fetch_array($attr_arr_query)){
	$mk_attr_arr[] = $attr_arr['products_options_values_name'];
}
$mk_js_attr_arr = json_encode($mk_attr_arr);

?>
<input type="hidden" name="products_id" value="<?php echo $_GET['pID']; ?>">
<input type="hidden" name="options_id" value="<?php echo $_GET['options_id']; ?>">
<input type="hidden" name="value_id" value="<?php echo $next_id; ?>">
<textarea name="attributesname" style="display:none;"><?php echo strtolower($mk_js_attr_arr); ?></textarea>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-3 control-label"><strong>Option Value</strong></label>
                    <div class="col-md-5">
						<?php
							for ($i = 0, $n = sizeof($languages); $i < $n; $i ++) {
								echo '<input required class="form-control m-b-5" id="new_option_value" placeholder="Option Value Name in ' . $languages[$i]['name'] . '" type="text" name="value_name[' . $languages[$i]['id'] . ']">';
							}
						?>
						<p id="response" style="margin:0px;"></p>
                    </div>
					<div class="col-md-3">
						<button type="submit" class="btn btn-primary m-r-5" id="insert_attr"><i class="fa fa-save"></i> Save</button>
					</div>
                </div>
            </div>
        </div>
	</div>
</div>
</form>
<div class="row">
	<div class="col-sm-12">
		<?php echo tep_draw_form('assign_attribute', FILENAME_ATTRIBUTES, 'action=assign_attribute', 'post', 'enctype="multipart/form-data"');?>
			<legend><h4>Assign Options Value</h4></legend>
			<div class="row">
				<?php
					$cnt = 0;
					echo tep_draw_hidden_field('options_id', $_GET['options_id'] );
					echo tep_draw_hidden_field('products_id', $_GET['pID'] );
					$values_query = tep_db_query("select pov.products_options_values_id, pov.products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov, " . TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS . " p2p WHERE pov.products_options_values_id = p2p.products_options_values_id and p2p.products_options_id = '" .$_GET['options_id']. "' and pov.language_id = '" . $languages_id . "' order by pov.products_options_values_name");
						if(tep_db_num_rows($values_query) > 0){
						while($values_values = tep_db_fetch_array($values_query)) {
							$value_id = $values_values['products_options_values_id'];
							$deletvalid = checkassignedattributes($value_id);
							$check_att_qry = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS_ATTRIBUTES . " WHERE products_id = '".$_GET['pID']."' AND options_id = '".$_GET['options_id']."' AND options_values_id = '".$value_id."' ");
							if(tep_db_num_rows($check_att_qry) > 0){
								continue;
							}
							$cnt++;
							echo '<div id="hidediv-'.$value_id.'" class="col-md-6" style="padding:5px;border-bottom:1px solid #ccc">'.
									tep_draw_hidden_field('hiddenoID[' . $value_id . ']', $_GET['options_id']) .
									tep_draw_checkbox_field('values_status[]', $values_values['products_options_values_id']) . '&nbsp;' . htmlspecialchars($values_values['products_options_values_name']);
							if(!$deletvalid){
								//echo  '<a class="btn btn-danger btn-xs addattributes pull-right" style="padding:0px 5px;margin-right:30px" href="javascript:" title="Delete Options Values" data-toggle="tooltip" onclick="javascript:ajax_delete_attributes('.$value_id.');" type="button"><i class="fa fa-times" aria-hidden="true"></i></a>';
							}
							echo '</div>';
						}
					}
					if($cnt==0){
						echo '<p style="color:#ff0000;margin-left:10px">There are no Option values available to assign!!</p>';
					}
				?>
				<div class="col-md-12" style="text-align:center;margin-top:10px;">
					<button type="submit" class="btn btn-primary m-r-5"><i class="fa fa-save"></i> Assign</button>
				</div>
			</div>
		</form>
	</div>
</div>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>