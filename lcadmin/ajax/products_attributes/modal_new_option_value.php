<!-- #modal-dialog new-option-value -->
<div class="modal fade" id="new-option-value">
    <div class="modal-dialog">
        <?php
            $max_values_id_query = tep_db_query("select max(products_options_values_id) + 1 as next_id from " . TABLE_PRODUCTS_OPTIONS_VALUES);
            $max_values_id_values = tep_db_fetch_array($max_values_id_query);
            $next_id = $max_values_id_values['next_id'];
        ?>
        <form name="values" action="<?php echo tep_href_link(FILENAME_PRODUCTS_ATTRIBUTES, 'action=add_product_option_values', 'NONSSL');?>" method="post" class="form-horizontal" onSubmit="return addoptvaluesname();">
            <input type="hidden" name="value_id" value="<?php echo $next_id; ?>">
            <div id="option_values_textarea" style="display:none;"></div>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add Product Option Value</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 m-b-10">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Select Option</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="option_id" onChange="fetchoptioncalues(this.value);">
                                    <option>--Select Option--</option>
                                        <?php
                                            $options = tep_db_query("select po.products_options_id, pot.products_options_name from " . TABLE_PRODUCTS_OPTIONS . " AS po, " . TABLE_PRODUCTS_OPTIONS_TEXT  . " AS pot where po.options_type in (0,2,3) and po.products_options_id = pot.products_options_text_id  and pot.language_id = '" . (int)$languages_id . "' order by po.products_options_sort_order, pot.products_options_name");
                                            while ($options_values = tep_db_fetch_array($options)) {
                                                echo "\n" . '<option name="' . htmlspecialchars($options_values['products_options_name']) . '" value="' . $options_values['products_options_id'] . '">' . htmlspecialchars($options_values['products_options_name']) . '</option>';
                                            }
                                        ?>
                                    </select>
                                    <p id="selectdpdown" style="margin:0"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Option Value</label>
                                <div class="col-md-6">
                                    <?php
                                        for ($i = 0, $n = sizeof($languages); $i < $n; $i ++) {
                                            echo '<div class="input-group m-b-5"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span><input id="value_name" class="form-control" placeholder="Option Value Name in ' . $languages[$i]['name'] . '" type="text" name="value_name[' . $languages[$i]['id'] . ']"></div>';
                                        }
                                    ?>
                                    <p id="edit_value_responsive"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary m-r-5"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- modal end new-option-value-->