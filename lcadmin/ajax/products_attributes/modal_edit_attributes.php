<?php
    chdir('../../');
    require('includes/application_top.php');
    $languages = tep_get_languages();

    $attributes = "select pa.* from " . TABLE_PRODUCTS_ATTRIBUTES . " pa,
      " . TABLE_PRODUCTS_DESCRIPTION . " pd
      where
      pd.products_id = pa.products_id and
      pd.language_id = '" . (int)$languages_id . "' and
      pa.products_attributes_id = '" . $_GET['editAttributelID'] . "'";
    $attributes = tep_db_query($attributes);
    $attributes_values = tep_db_fetch_array($attributes);
    $products_name_only = tep_get_products_name($attributes_values['products_id']);
    $options_name = tep_options_name($attributes_values['options_id']);
    $values_name = tep_values_name($attributes_values['options_values_id']);
    $default = $attributes_values['options_default'];

	$options_query = tep_db_query("SELECT options_type FROM " . TABLE_PRODUCTS_OPTIONS  . " WHERE products_options_id = '".$attributes_values['options_id']."' ");
	$row = tep_db_fetch_array($options_query);
	$options_type = $row['options_type'];

   echo '<form name="EditAttribute" class="form-horizontal" action="' . tep_href_link(FILENAME_PRODUCTS_ATTRIBUTES, 'action=update_product_attribute', 'NONSSL') . '" method="post">';
?>
<input type="hidden" name="attribute_id" value="<?php echo $attributes_values['products_attributes_id']; ?>">
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-5 control-label"><strong>Product Name</strong></label>
                    <div class="col-md-5">
                        <label style="padding:5px"><strong><?php echo $products_name_only; ?></strong><input type="hidden" name="products_id" value="<?php echo $attributes_values['products_id']; ?>"></label>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-5 control-label">Select an Option</label>
                    <div class="col-md-5">
                    	<input type="hidden" name="options_id" value="<?php echo $attributes_values['options_id']; ?>">
						<?php
							  $options = tep_db_query("select po.products_options_id, pot.products_options_name from " . TABLE_PRODUCTS_OPTIONS . " po, " . TABLE_PRODUCTS_OPTIONS_TEXT  . " pot where pot.products_options_text_id = po.products_options_id and pot.language_id = '" . (int)$languages_id . "' and products_options_id = '".$attributes_values['options_id']."' order by po.products_options_sort_order, pot.products_options_name");
							  $options_values = tep_db_fetch_array($options);
							  echo '<label style="padding:5px"><b>'.htmlspecialchars($options_values['products_options_name']).'</b></label>';
						?>
                        <div class="alloptionvalesDiv" id="alloptionvalesDiv">
                    </div>
                </div>
            </div>

            <?php if($options_type != 7){ ?>
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-5 control-label">Option Required</label>
                    <div class="col-md-5">
						<?php
							$validateQry = tep_db_query("select `settings` from `products_options_validation` where `option_id` = '".(int)$attributes_values['options_id']."' and products_id = '".(int)$attributes_values['products_id']."'");
							if(tep_db_num_rows($validateQry) > 0){
								$row = tep_db_fetch_array($validateQry);
								$setings = json_decode($row['settings'],1);
								$isreqd = $setings['required'];
							}
							echo '<input name="mandetory" ' . (($isreqd) ? 'checked' : '') . ' data-toggle="toggle" data-on="<i class=\'fa fa-check\'></i> Yes" data-off="<i class=\'fa fa-times\'></i> No" data-onstyle="success" data-offstyle="danger" id="toggle-one" type="checkbox" data-size="small">';
						?>
						<script>
						  $(function() {
							$('#toggle-one').bootstrapToggle();
						  })
						</script>
                    </div>
                </div>
            </div>
            <?php } ?>
			<?php
				if($options_type == 1 || $options_type == 4 || $options_type == 7){

				}   else {
			?>
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-5 control-label">Option Value</label>
                    <div class="col-md-5">
                        <select class="form-control" name="values_id">
						<?php
							  $values_query = tep_db_query("select pov.products_options_values_id, pov.products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov, " . TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS . " p2p WHERE pov.products_options_values_id = p2p.products_options_values_id and p2p.products_options_id = '" .$attributes_values['options_id']. "' and pov.language_id = '" . $languages_id . "' order by pov.products_options_values_name");
							  if(tep_db_num_rows($values_query) > 0){
								  while($values_values = tep_db_fetch_array($values_query)) {
									if ($attributes_values['options_values_id'] == $values_values['products_options_values_id']) {
									  echo "\n" . '<option name="' . htmlspecialchars($values_values['products_options_values_name']) . '" value="' . $values_values['products_options_values_id'] . '" selected="selected">' . htmlspecialchars($values_values['products_options_values_name']) . '</option>';
									} else {
									  echo "\n" . '<option name="' . htmlspecialchars($values_values['products_options_values_name']) . '" value="' . $values_values['products_options_values_id'] . '">' . htmlspecialchars($values_values['products_options_values_name']) . '</option>';
									}
								  }
							  }else{
							  	echo '<option>No option Available</option>';
							  }
						?>
                        </select>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if($options_type == 0 || $options_type == 2){ ?>
            <div class="col-sm-12 m-b-10">
                <div class="form-group">
                    <label class="col-md-5 control-label">Default Option</label>
                    <div class="col-md-5">
                        <div class="input-group">
                        <?php
							echo '<input value="1" name="select_default" ' . (($default) ? 'checked' : '') . ' data-toggle="toggle" data-on="<i class=\'fa fa-check\'></i> Yes" data-off="<i class=\'fa fa-times\'></i> No" data-onstyle="success" data-offstyle="danger" id="select_default" type="checkbox" data-size="small">';
						?>
						<script>
						  $(function() {
							$('#select_default').bootstrapToggle();
						  })
						</script>
                        </div>
                    </div>
                </div>
            </div>
            <?php }else{ ?>
            	<input type="hidden" name="select_default" value="0">
            <?php } ?>
             <?php if($options_type != 7){ ?>
            <div class="col-sm-12 m-b-10">
                <div class="form-group">
                	<?php
						$price_prefix = $attributes_values['price_prefix'];
						if($price_prefix == '-'){
							$display_price_prefix = $price_prefix;
						}else{
							$display_price_prefix = '';
						}
                	?>
                    <label class="col-md-5 control-label">Value Price</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input name="value_price" class="form-control" placeholder="" type="text" value="<?php echo $display_price_prefix.$attributes_values['options_values_price']; ?>">
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-5 control-label">Option Sort Order</label>
                    <div class="col-md-5">
                        <input name="option_sort_order" class="form-control" placeholder="" type="text" value="<?php echo $attributes_values['main_options_sorting']; ?>">
                    </div>
                </div>
            </div>
            <?php if($options_type != 7){ ?>
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-5 control-label">Option Value Sort Order</label>
                    <div class="col-md-5">
                        <input name="sort_order" class="form-control" placeholder="" type="text" value="<?php echo $attributes_values['products_options_sort_order']; ?>">
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
<?php
      if (DOWNLOAD_ENABLED == 'true') {
        $download_query_raw ="select products_attributes_filename, products_attributes_maxdays, products_attributes_maxcount
                              from " . TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD . "
                              where products_attributes_id='" . $attributes_values['products_attributes_id'] . "'";
        $download_query = tep_db_query($download_query_raw);
        if (tep_db_num_rows($download_query) > 0) {
          $download = tep_db_fetch_array($download_query);
          $products_attributes_filename = $download['products_attributes_filename'];
          $products_attributes_maxdays  = $download['products_attributes_maxdays'];
          $products_attributes_maxcount = $download['products_attributes_maxcount'];
        }
?>
        <fieldset style="display:none;">
            <legend>Downloadable products</legend>
            <div class="form-group">
                <label class="col-md-5 control-label">File Name</label>
                <div class="col-md-5">
                    <input name="products_attributes_filename" class="form-control" placeholder="" type="text" value="<?php echo $products_attributes_filename;?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-5 control-label">Expiry Days</label>
                <div class="col-md-5">
                    <input name="products_attributes_maxdays" value="<?php echo $products_attributes_maxdays;?>" class="form-control" placeholder="" type="text">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-5 control-label">Download Count</label>
                <div class="col-md-5">
                    <input name="products_attributes_maxcount" value="<?php echo $products_attributes_maxcount;?>" class="form-control" placeholder="" type="text">
                </div>
            </div>
        </fieldset>
    <div class="col-sm-12">
        <div class="form-group">
            <label class="col-md-5 control-label"></label>
            <div class="col-md-5">
                <button type="submit" class="btn btn-primary m-r-5"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
        <?php
      }
        ?>
    </div>
</div>
</form>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>