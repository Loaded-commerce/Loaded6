<?php
chdir('../../');
require('includes/application_top.php');
$languages = tep_get_languages();
$validateQry = tep_db_query("select `settings` from `products_options_validation` where `option_id` = '".(int)$_GET['options_id']."' and products_id = '".(int)$_GET['pID']."' ");
if(tep_db_num_rows($validateQry) > 0){
	$row = tep_db_fetch_array($validateQry);
	$setings = json_decode($row['settings'],1);
	$isreqd = $setings['required'];
	$minchar = $setings['minchar'];
	$maxchar = $setings['maxchar'];
	$validatetype = $setings['validatetype'];
}
?>
<form id="add_validate" name="add_validate" action="#" method="post" onsubmit="return checkaddvalidate()">
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-md-5 control-label"><strong>Option Required</strong></label>
					<div class="col-md-5" style="display:inline;">
						   <?php echo '<input name="mandetory" ' . (($isreqd) ? 'checked' : '') . ' data-toggle="toggle" data-on="<i class=\'fa fa-check\'></i> Yes" data-off="<i class=\'fa fa-times\'></i> No" data-onstyle="success" data-offstyle="danger" id="toggle-one" type="checkbox" data-size="small">'; ?>
							<script>
							  $(function() {
								$('#toggle-one').bootstrapToggle();
							  })
							</script>
		  				   <?php /*
		  				   <div class="btn-group" id="status" data-toggle="buttons">
		  					  <label class="btn btn-default btn-on btn-sm <?php echo ($isreqd)?'active':''; ?>">
		  					  <input type="radio" value="1" id="mandetory" name="mandetory" <?php echo ($isreqd)?'active':''; ?> class="nodisplayradio">Yes</label>
		  					  <label class="btn btn-default btn-off btn-sm <?php echo (!$isreqd)?'active':''; ?>">
		  					  <input type="radio" value="0" id="mandetory" name="mandetory" <?php echo (!$isreqd)?'checked':''; ?> class="nodisplayradio">No</label>
		  				   </div>
		  				   */ ?>
		  			</div>
		  			<input type="hidden" id="pID" name="pID" value="<?php echo (int)$_GET['pID']; ?>">
		  			<input type="hidden" id="oID" name="oID" value="<?php echo (int)$_GET['options_id']; ?>">
		  			<input type="hidden" id="otype" name="otype" value="<?php echo (int)$_GET['otype']; ?>">
					<div class="col-md-2"></div>

                    <label class="col-md-5 control-label"><strong>Minimum Characters allowed</strong></label>
					<div class="col-md-5" style="display:inline-block;">
						<input class="form-control" type="number" id="minchar" name="minchar" value="<?php echo $minchar; ?>">
					</div>
					<div class="col-md-2"></div>

                    <label class="col-md-5 control-label"><strong>Maximum Characters allowed</strong></label>
					<div class="col-md-5" style="display:inline-block;">
						<input class="form-control" type="number" id="maxchar" name="maxchar" value="<?php echo $maxchar; ?>">
					</div>
					<div class="col-md-2"></div>

                    <label class="col-md-5 control-label"><strong>Validation Type</strong></label>
					<div class="col-md-5" style="display:inline-block;">
						<select id="validation_type" name="validation_type" class="form-control">
							<option value="STRING" <?php echo ($validatetype == 'STRING' || $validatetype == '')?'selected':''; ?>>STRING</option>
							<option value="INTEGER" <?php echo ($validatetype == 'INTEGER')?'selected':''; ?>>INTEGER</option>
							<option value="CURRENCY" <?php echo ($validatetype == 'CURRENCY')?'selected':''; ?>>CURRENCY</option>
							<option value="EMAIL" <?php echo ($validatetype == 'EMAIL')?'selected':''; ?>>EMAIL</option>
						</select>
					</div>
					<div class="col-md-12" style="text-align:center">
						<button type="button" onclick="addextravalidation();" class="btn btn-primary m-r-5" id="insert_attr" style="margin-top:15px;"><i class="fa fa-save"></i> Save</button>
						<p id="successmsg" style="color:green"></p>
					</div>

                </div>
            </div>
        </div>
	</div>
</div>
</form>
<script>
function addextravalidation(){
	var otype = $('#otype').val();
	//var mandetory = $('input[name="mandetory"]:checked').val();
	if($('input[name="mandetory"]').is(':checked')){
		var mandetory = 1;
	}else{
		var mandetory = 0;
	}
	var pID = $('#pID').val();
	var oID = $('#oID').val();
	var vtype = $('#validation_type').val();
	var minchar = $('#minchar').val();
	var maxchar = $('#maxchar').val();
	$.ajax({
		url : 'ajax_common.php?action=optionsmandetory',
		type : 'post',
		data : {'oID':oID,'mandetory':mandetory,'pID':pID,'otype':otype,'vtype':vtype,'minchar':minchar,'maxchar':maxchar},
		success: function(res){
			$('#successmsg').html('Validation has been Add successfully!!');
		}
	});
}
</script>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>