<!-- #modal-dialog new products attribute -->
<div class="modal fade" id="new-attribute">
    <div class="modal-dialog">
        <form name="attributes" action="<?php echo tep_href_link(FILENAME_PRODUCTS_ATTRIBUTES, 'action=add_product_attributes', 'NONSSL');?>" method="post" class="form-horizontal" onSubmit="return validateattform();">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add Product Attribute</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Select Product</label>
                                <div class="col-md-5">
									   <div id="replacediv" class=" well" style="padding:0px;margin-bottom: 0px;border: none;">
										  <div id="search_section">
											 <input type="text" name="search_data" placeholder="Type to select product" class="form-control" id="search_data_list" value="" >
											 <p id="new_products_error" style="margin:0"></p>
										  </div>
									  </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Select an Option</label>
                                <div class="col-md-5">
                                    <select class="form-control" name="options_id" onchange="getoptionvalue(this.value);">
                                    	<option value="0">--Select Option--</option>
                                        <?php
                                            $options = tep_db_query("select po.products_options_id, pot.products_options_name from " . TABLE_PRODUCTS_OPTIONS . " po, " . TABLE_PRODUCTS_OPTIONS_TEXT  . " pot where pot.products_options_text_id = po.products_options_id and pot.language_id = '" . (int)$languages_id . "' order by po.products_options_sort_order, pot.products_options_name");
                                            while($options_values = tep_db_fetch_array($options)) {
                                                echo "\n" . '<option name="' . htmlspecialchars($options_values['products_options_name']) . '" value="' . $options_values['products_options_id'] . '">' . htmlspecialchars($options_values['products_options_name']) . '</option>';
                                            }
                                        ?>
                                    </select>
                                    <p id="new_option_error" style="margin:0"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" style="display:none;" id="optionvaluename">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Option Value</label>
                                <div class="col-md-5">
                                    <select class="form-control" name="values_id" id="option_values_id">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" id="attpricesection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Value Price</label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input name="value_price" class="form-control" placeholder="" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-sm-12" id="attoptionreqdsection">
							<div class="form-group">
								<label class="col-md-3 control-label">Option Required</label>
								<div class="col-md-5">
									<?php
										echo '<input name="mandetory" data-width="80" data-toggle="toggle" data-on="<i class=\'fa fa-check\'></i> Yes" data-off="<i class=\'fa fa-times\'></i> No" data-onstyle="success" data-offstyle="danger" id="add-toggle-one" type="checkbox" data-size="small">';
									?>
									<script>
									  $(function() {
										$('#add-toggle-one').bootstrapToggle();
									  })
									</script>
								</div>
							</div>
						</div>
						<div class="col-sm-12" id="morevalidation">

						</div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Option Sort Order</label>
                                <div class="col-md-3">
                                    <input name="option_sort_order" class="form-control" placeholder="" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" id="attvaluesortsection">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Option Value Sort Order</label>
                                <div class="col-md-3">
                                    <input name="sort_order" class="form-control" placeholder="" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <fieldset style="display:none;">
                        <legend>Downloadable products</legend>
                        <div class="form-group">
                            <label class="col-md-3 control-label">File Name</label>
                            <div class="col-md-3">
                                <input name="products_attributes_filename" class="form-control" placeholder="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Expiry Days</label>
                            <div class="col-md-3">
                                <input name="products_attributes_maxdays" value="<?php echo DOWNLOAD_MAX_DAYS;?>" class="form-control" placeholder="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Download Count</label>
                            <div class="col-md-3">
                                <input name="products_attributes_maxcount" value="<?php echo DOWNLOAD_MAX_COUNT;?>" class="form-control" placeholder="" type="text">
                            </div>
                        </div>
                    </fieldset>


                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary m-r-5"><i class="fa fa-save"></i> Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- modal new products attribute end-->
<script type="text/javascript" src="includes/javascript/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<script src="includes/javascript/jquery.autocomplete_pos.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="includes/javascript/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery.autocomplete.css" />
<script type="text/javascript">
$(document).ready(function() {
//Code for the Auto Complete for pos
   $("#search_data_list").autocomplete("autocomplete_search.php", {
	  selectFirst: true,
	  fun: 'selectCurrent2'
   });

	$('.iframe-btn').fancybox({
	'width'		: 900,
	'height'	: 600,
	'type'		: 'iframe',
	'autoScale'    	: false,
	'autoSize':     false
	});
});


</script>
