var blue		= '#348fe2',
    blueLight	= '#5da5e8',
    blueDark	= '#1993E4',
    aqua		= '#49b6d6',
    aquaLight	= '#6dc5de',
    aquaDark	= '#3a92ab',
    green		= '#00acac',
    greenLight	= '#33bdbd',
    greenDark	= '#008a8a',
    orange		= '#f59c1a',
    orangeLight	= '#f7b048',
    orangeDark	= '#c47d15',
    dark		= '#2d353c',
    grey		= '#b6c2c9',
    purple		= '#727cb6',
    purpleLight	= '#8e96c5',
    purpleDark	= '#5b6392',
    red         = '#ff5b57';


var handleLineChart = function() {
    "use strict";

    nv.addGraph(function() {

        var sin = [], cos = [];
        for (var i = 0; i < 100; i++) {
            sin.push({x: i, y:  Math.sin(i/10) });
            cos.push({x: i, y: .5 * Math.cos(i/10)});
        }
        var lineChartData = [
            { values: sin, key: 'Sine Wave', color: green },
            { values: cos, key: 'Cosine Wave', color: blue }
        ];

        var lineChart = nv.models.lineChart()
            .options({
                transitionDuration: 300,
                useInteractiveGuideline: true
            });

        lineChart.xAxis
            .axisLabel('Time (s)')
            .tickFormat(d3.format(',.1f'));

        lineChart.yAxis
            .axisLabel('Voltage (v)')
            .tickFormat(function(d) {
                if (d == null) {
                    return 'N/A';
                }
                return d3.format(',.2f')(d);
            });

        d3.select('#nv-line-chart').append('svg')
            .datum(lineChartData)
            .call(lineChart);

        nv.utils.windowResize(lineChart.update);

        return lineChart;
    });
};


var handleBarChart = function() {
    "use strict";

    var barChartData = [{
        key: 'Cumulative Return',
        values: [
            { 'label' : 'A', 'value' : 29, 'color' : red },
            { 'label' : 'B', 'value' : 15, 'color' : orange },
            { 'label' : 'C', 'value' : 32, 'color' : green },
            { 'label' : 'D', 'value' : 196, 'color' : aqua },
            { 'label' : 'E', 'value' : 44, 'color' : blue },
            { 'label' : 'F', 'value' : 98, 'color' : purple },
            { 'label' : 'G', 'value' : 13, 'color' : grey },
            { 'label' : 'H', 'value' : 5, 'color' : dark }
        ]
    }];

    nv.addGraph(function() {
        var barChart = nv.models.discreteBarChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .showValues(true)
            .duration(250);

        barChart.yAxis.axisLabel("Total Sales");
        barChart.xAxis.axisLabel('Product');

        d3.select('#nv-bar-chart').append('svg').datum(barChartData).call(barChart);
        nv.utils.windowResize(barChart.update);

        return barChart;
    });
}

var handlePieAndDonutChart = function() {
    "use strict";

    var pieChartData = [
        { 'label': 'One', 'value' : 29, 'color': red },
        { 'label': 'Two', 'value' : 12, 'color': orange },
        { 'label': 'Three', 'value' : 32, 'color': green },
        { 'label': 'Four', 'value' : 196, 'color': aqua },
        { 'label': 'Five', 'value' : 17, 'color': blue },
        { 'label': 'Six', 'value' : 98, 'color': purple },
        { 'label': 'Seven', 'value' : 13, 'color': grey },
        { 'label': 'Eight', 'value' : 5, 'color': dark }
    ];

    nv.addGraph(function() {
        var pieChart = nv.models.pieChart()
          .x(function(d) { return d.label })
          .y(function(d) { return d.value })
          .showLabels(true)
          .labelThreshold(.05);

        d3.select('#nv-pie-chart').append('svg')
            .datum(pieChartData)
            .transition().duration(350)
            .call(pieChart);

        return pieChart;
    });


    nv.addGraph(function() {
      var chart = nv.models.pieChart()
          .x(function(d) { return d.label })
          .y(function(d) { return d.value })
          .showLabels(true)
          .labelThreshold(.05)
          .labelType("percent")
          .donut(true)
          .donutRatio(0.35);

        d3.select('#nv-donut-chart').append('svg')
            .datum(pieChartData)
            .transition().duration(350)
            .call(chart);

        return chart;
    });
};

var ChartNvd3 = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleLineChart();
            handleBarChart();
            handlePieAndDonutChart();
        }
    };
}();