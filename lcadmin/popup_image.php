<?php
/*
  $Id: popup_image.php,v 1.1.1.1 2004/03/04 23:38:52 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

if ($_GET['image']) {

     $big_image = tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $_GET['image'], $_GET['image']);

} else {

  reset($_GET);
  foreach($_GET as $key => $tmpvalue) {
    switch ($key) {
      case 'banner':
        $banners_id = tep_db_prepare_input($_GET['banner']);

        $banner_query = tep_db_query("select banners_title, banners_image, banners_html_text from " . TABLE_BANNERS . " where banners_id = '" . (int)$banners_id . "'");
        $banner = tep_db_fetch_array($banner_query);

        $page_title = $banner['banners_title'];

        if ($banner['banners_html_text']) {
          $image_source = $banner['banners_html_text'];
        } elseif ($banner['banners_image']) {
          $image_source = tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $banner['banners_image'], $page_title, '', '', 'style="object-fit: contain; width:572px; " class="img-responsive"');
        }
        break;
    }
  }
 }

   if ($_GET['image']) {
         echo $big_image;
         } else {
         echo $image_source;
   }
 ?>

