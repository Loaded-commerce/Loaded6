<?php
/*
  $Id: server_info.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

if (function_exists(tep_get_system_information)) {
  $system = tep_get_system_information();
} else {
  $system = '';
}
if (!defined('TEXT_FAILED')) define('TEXT_FAILED', 'Failed');
if (!defined('TEXT_SUCCESS')) define('TEXT_SUCCESS', 'Success');
if (!defined('TEXT_ON')) define('TEXT_ON', 'On');
if (!defined('TEXT_OFF')) define('TEXT_OFF', 'Off');

function checkINI($option) {
  $value = strtolower( trim( ini_get( $option ) ) );
  if ( $value == 'on' ) $value = TEXT_ON;
  elseif ( $value == 'off' )  $value = TEXT_OFF;
  elseif ( $value == '1' )  $value = TEXT_ON;
  elseif ( $value == '0' ) $value = TEXT_OFF;
  else $value = TEXT_OFF;

  return $value;
}
include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

<style>
.logo-text {
  font-weight: 100;
  font-size: 18px;
  line-height: 30px;  
}
.logo-slogan i {
  font-size: 1.3em;
  color: #b8daff !important;
}

</style>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-serverinfo" class="table-serverinfo">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">
        	  <table class="table w-100 mt-2">
            <?php
            // RCI include version files
            $returned_rci = $cre_RCI->get('serverinfo', 'version');
            ?>
	            <tr>
                <td class="table-col" colspan="2"><div><span class="logo"><img src="assets/img/logo.png"></span><span class="ml-1 logo-text align-middlw"> Loaded Commerce </span><small class="logo-slogan align-middle"><i>Community Edition</i></small></div></td>
                <td class="table-col text-right align-middle text-white" colspan="2"><?php echo PROJECT_VERSION; ?></td>              
                </tr>
	              <?php if(trim($returned_rci) != '') { ?><tr><td class="table-col" colspan="4"><?php echo $returned_rci; ?></td></tr><?php } ?>
                <tr>
                  <td class="table-col smallText"><b><?php echo TITLE_SERVER_HOST; ?></b></td>
                  <td class="table-col smallText"><?php echo $system['host'] . ' (' . $system['ip'] . ')'; ?></td>
                  <td class="table-col smallText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo TITLE_DATABASE_HOST; ?></b></td>
                  <td class="table-col smallText"><?php echo $system['db_server'] . ' (' . $system['db_ip'] . ')'; ?></td>
                </tr>
                <tr>
                  <td class="table-col smallText"><b><?php echo TITLE_SERVER_OS; ?></b></td>
                  <td class="table-col smallText"><?php echo $system['system'] . ' ' . $system['kernel']; ?></td>
                  <td class="table-col smallText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo TITLE_DATABASE; ?></b></td>
                  <td class="table-col smallText"><?php echo $system['db_version']; ?></td>
                </tr>
                <tr>
                  <td class="table-col smallText"><b><?php echo TITLE_SERVER_DATE; ?></b></td>
                  <td class="table-col smallText"><?php echo $system['date']; ?></td>
                  <td class="table-col smallText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo TITLE_DATABASE_DATE; ?></b></td>
                  <td class="table-col smallText"><?php echo $system['db_date']; ?></td>
                </tr>
                <tr>
                  <td class="table-col smallText"><b><?php echo TITLE_SERVER_UP_TIME; ?></b></td>
                  <td colspan="3" class="table-col smallText"><?php echo $system['uptime']; ?></td>
                </tr>
                <tr>
                  <td class="table-col" colspan="4"><?php echo tep_draw_separator('pixel_trans.gif', '1', '5'); ?></td>
                </tr>
                <tr>
                  <td class="table-col smallText"><b><?php echo TITLE_HTTP_SERVER; ?></b></td>
                  <td colspan="3" class="table-col smallText"><?php echo $system['http_server']; ?></td>
                </tr>
                <tr>
                  <td class="table-col smallText"><b><?php echo TITLE_PHP_VERSION; ?></b></td>
                  <td colspan="3" class="table-col smallText"><?php echo $system['php'] . ' (' . TITLE_ZEND_VERSION . ' ' . $system['zend'] . ')'; ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end panel -->
    </div>
<!-- footer //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
