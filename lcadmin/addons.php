<?php
$routes = isset($_GET['routes'])?$_GET['routes']:'';
if(trim($routes) == '') {
	echo "Bad Data";
	exit();
}

$arr_routes = explode('/', $routes);
if(isset($arr_routes[0]) && isset($arr_routes[1]))
{
	
	define('MODULE_ADDONS_ADMIN_PATH', '../addons/lc_'.$arr_routes[0].'/admin/');
	define('MODULE_ADDONS_CATALOG_PATH', '../addons/lc_'.$arr_routes[0].'/catalog/');
	define('MODULE_ADDONS_REQUEST_MODULE', $arr_routes[1]);
	define('MODULE_ADDONS_REQUEST_PAGE', $arr_routes[1].'.php');
	define('MODULE_ADDONS', $arr_routes[0]);
	$addon_file_path = MODULE_ADDONS_ADMIN_PATH . $arr_routes[1].'.php';
	//echo $addon_file_path;
	if(file_exists($addon_file_path)) {
		$_SERVER['PHP_SELF'] = $arr_routes[1].'.php';
		include($addon_file_path);
	} else {
		header("Location: forbiden.php");
	}
}
else
	echo "Bad Data";
?>
