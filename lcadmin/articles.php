<?php
/*
  $Id: articles.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');
// RCI code start
echo $cre_RCI->get('global', 'top', false);
echo $cre_RCI->get('articles', 'top', false);
// RCI code eof
$action = (isset($_GET['action']) ? $_GET['action'] : '');
if (tep_not_null($action)) {
  switch ($action) {
    case 'setflag':
      if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
        if (isset($_GET['aID'])) {
          tep_set_article_status($_GET['aID'], $_GET['flag']);
        }
        if (USE_CACHE == 'true') {
          tep_reset_cache_block('topics');
        }
        echo $_GET['flag'];exit;
      }
      tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $_GET['tPath'] . '&aID=' . $_GET['aID']));
      break;
    case 'new_topic':
    case 'edit_topic':
      $_GET['action']=$_GET['action'] . '_ACD';
      break;
    case 'insert_topic':
    case 'update_topic':
      if ( ($_POST['edit_x']) || ($_POST['edit_y']) ) {
        $_GET['action'] = 'edit_topic_ACD';
      } else {
        if (isset($_POST['topics_id'])) $topics_id = tep_db_prepare_input($_POST['topics_id']);
        if ($topics_id == '') {
          $topics_id = tep_db_prepare_input($_GET['tID']);
        }
        $sort_order = tep_db_prepare_input($_POST['sort_order']);
        $sql_data_array = array('sort_order' => $sort_order);
        if ($action == 'insert_topic') {
          $insert_sql_data = array('parent_id' => $current_topic_id,
                                   'date_added' => 'now()');
          $sql_data_array = array_merge((array)$sql_data_array, (array)$insert_sql_data);
          tep_db_perform(TABLE_TOPICS, $sql_data_array);
          $topics_id = tep_db_insert_id();
        } elseif ($action == 'update_topic') {
          $update_sql_data = array('last_modified' => 'now()');
          $sql_data_array = array_merge((array)$sql_data_array, (array)$update_sql_data);
          tep_db_perform(TABLE_TOPICS, $sql_data_array, 'update', "topics_id = '" . (int)$topics_id . "'");
        }
        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $language_id = $languages[$i]['id'];
          $sql_data_array = array('topics_name' => tep_db_prepare_input($_POST['topics_name'][$language_id]),
                                  'topics_heading_title' => tep_db_prepare_input($_POST['topics_heading_title'][$language_id]),
                                  'topics_description' => tep_db_prepare_input($_POST['topics_description'][$language_id]));
          $permalink_name = tep_db_prepare_input($_POST['permalink_name'][$language_id]);
		  $slug = sanitize($permalink_name);
          if ($action == 'insert_topic') {
            $insert_sql_data = array('topics_id' => $topics_id,
                                     'language_id' => $languages[$i]['id']);
            $sql_data_array = array_merge((array)$sql_data_array, (array)$insert_sql_data);
            tep_db_perform(TABLE_TOPICS_DESCRIPTION, $sql_data_array);

            //INSERT PERMALINK

			 $check_topic = tep_db_query("SELECT * FROM permalinks WHERE language_id = '".(int)$languages[$i]['id']."' and  permalink_name='".$slug."' ");
			 if(tep_db_num_rows($check_topic) > 0){
			     $slug  = $slug."-".mt_rand(1, 100);
				tep_db_query("INSERT INTO permalinks SET  topics_id = '".$topics_id."', route = 'core/articles', language_id='".$language_id."', permalink_name='".$slug."', permalink_type = 'topics' ");
			 }else{
				tep_db_query("INSERT INTO permalinks SET  topics_id = '".$topics_id."', route = 'core/articles', language_id=1, permalink_name='".$slug."', permalink_type = 'topics' ");
			 }

          } elseif ($action == 'update_topic') {
            tep_db_perform(TABLE_TOPICS_DESCRIPTION, $sql_data_array, 'update', "topics_id = '" . (int)$topics_id . "' and language_id = '" . (int)$languages[$i]['id'] . "'");
            	$check_permal = tep_db_query("SELECT * FROM permalinks WHERE language_id = '".(int)$languages[$i]['id']."' and topics_id='".$topics_id."' and permalink_type = 'topics'");
            	if(tep_db_num_rows($check_permal) == 0 || tep_db_num_rows($check_permal) == ''){
					 $check_topic = tep_db_query("SELECT * FROM permalinks WHERE language_id = '".(int)$languages[$i]['id']."' and  permalink_name='".$slug."' ");
					 if(tep_db_num_rows($check_topic) > 0){
						 $slug  = $slug."-".mt_rand(1, 100);
						tep_db_query("INSERT INTO permalinks SET  topics_id = '".$topics_id."', route = 'core/articles', language_id='".$language_id."', permalink_name='".$slug."', permalink_type = 'topics' ");
					 }else{
						tep_db_query("INSERT INTO permalinks SET  topics_id = '".$topics_id."', route = 'core/articles', language_id=1, permalink_name='".$slug."', permalink_type = 'topics' ");
					 }
			   }

          }
        }

		// RCI for action insert
		echo $cre_RCI->get('articles', 'topicaction', false);


        if (USE_CACHE == 'true') {
          tep_reset_cache_block('topics');
        }

		  $mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
		  if ($mode == 'save') {
			tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $topics_id));
		  } else {  // save & stay
			tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $topics_id . '&action=edit_topic'));
		  }

        break;
      }
    case 'delete_topic_confirm':
      if (isset($_POST['topics_id'])) {
        $topics_id = tep_db_prepare_input($_POST['topics_id']);
        $topics = tep_get_topic_tree($topics_id, '', '0', '', true);
        $articles = array();
        $articles_delete = array();
        for ($i=0, $n=sizeof($topics); $i<$n; $i++) {
          $article_ids_query = tep_db_query("select articles_id from " . TABLE_ARTICLES_TO_TOPICS . " where topics_id = '" . (int)$topics[$i]['id'] . "'");
          while ($article_ids = tep_db_fetch_array($article_ids_query)) {
            $articles[$article_ids['articles_id']]['topics'][] = $topics[$i]['id'];
          }
        }
        reset($articles);
		foreach($articles as $key=>$value) {
          $topic_ids = '';
          for ($i=0, $n=sizeof($value['topics']); $i<$n; $i++) {
            $topic_ids .= "'" . (int)$value['topics'][$i] . "', ";
          }
          $topic_ids = substr($topic_ids, 0, -2);
          $check_query = tep_db_query("select count(*) as total from " . TABLE_ARTICLES_TO_TOPICS . " where articles_id = '" . (int)$key . "' and topics_id not in (" . $topic_ids . ")");
          $check = tep_db_fetch_array($check_query);
          if ($check['total'] < '1') {
            $articles_delete[$key] = $key;
          }
        }
        // removing topics can be a lengthy process
        tep_set_time_limit(0);
        for ($i=0, $n=sizeof($topics); $i<$n; $i++) {
          tep_remove_topic($topics[$i]['id']);
        }
        reset($articles_delete);
		foreach($articles_delete as $key=>$value) {
          tep_remove_article($key);
        }
      }
      if (USE_CACHE == 'true') {
        tep_reset_cache_block('topics');
      }
      tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath));
      break;
    case 'delete_article_confirm':
      if (isset($_POST['articles_id']) && isset($_POST['article_topics']) && is_array($_POST['article_topics'])) {
        $article_id = tep_db_prepare_input($_POST['articles_id']);
        $article_topics = $_POST['article_topics'];
        for ($i=0, $n=sizeof($article_topics); $i<$n; $i++) {
          tep_db_query("delete from " . TABLE_ARTICLES_TO_TOPICS . " where articles_id = '" . (int)$article_id . "' and topics_id = '" . (int)$article_topics[$i] . "'");
        }
        $article_topics_query = tep_db_query("select count(*) as total from " . TABLE_ARTICLES_TO_TOPICS . " where articles_id = '" . (int)$article_id . "'");
        $article_topics = tep_db_fetch_array($article_topics_query);
        if ($article_topics['total'] == '0') {
          tep_remove_article($article_id);
        }
      }
      if (USE_CACHE == 'true') {
        tep_reset_cache_block('topics');
      }
      tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath));
      break;
    case 'move_topic_confirm':
      if (isset($_POST['topics_id']) && ($_POST['topics_id'] != $_POST['move_to_topic_id'])) {
        $topics_id = tep_db_prepare_input($_POST['topics_id']);
        $new_parent_id = tep_db_prepare_input($_POST['move_to_topic_id']);
        $path = explode('_', tep_get_generated_topic_path_ids($new_parent_id));
        if (in_array($topics_id, $path)) {
          $messageStack->add_session('search', ERROR_CANNOT_MOVE_TOPIC_TO_PARENT, 'error');
          tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $topics_id));
        } else {
          tep_db_query("update " . TABLE_TOPICS . " set parent_id = '" . (int)$new_parent_id . "', last_modified = now() where topics_id = '" . (int)$topics_id . "'");
          if (USE_CACHE == 'true') {
            tep_reset_cache_block('topics');
          }
          tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $new_parent_id . '&tID=' . $topics_id));
        }
      }
      break;
    case 'move_article_confirm':
      $articles_id = tep_db_prepare_input($_POST['articles_id']);
      $new_parent_id = tep_db_prepare_input($_POST['move_to_topic_id']);
      $duplicate_check_query = tep_db_query("select count(*) as total from " . TABLE_ARTICLES_TO_TOPICS . " where articles_id = '" . (int)$articles_id . "' and topics_id = '" . (int)$new_parent_id . "'");
      $duplicate_check = tep_db_fetch_array($duplicate_check_query);
      if ($duplicate_check['total'] < 1) tep_db_query("update " . TABLE_ARTICLES_TO_TOPICS . " set topics_id = '" . (int)$new_parent_id . "' where articles_id = '" . (int)$articles_id . "' and topics_id = '" . (int)$current_topic_id . "'");
      if (USE_CACHE == 'true') {
        tep_reset_cache_block('topics');
      }
      tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $new_parent_id . '&aID=' . $articles_id));
      break;
    case 'insert_article':
    case 'update_article':
      //echo '<pre>';print_r($_FILES);exit;
      if (isset($_POST['edit_x']) || isset($_POST['edit_y'])) {
        $action = 'new_article';
      } else {
        if (isset($_GET['aID'])) $articles_id = tep_db_prepare_input($_GET['aID']);
        $articles_date_available = tep_db_prepare_input($_POST['articles_date_available']);
        $articles_date_available = ($articles_date_available != '')?date('Y-m-d', strtotime($articles_date_available)):'';
        $sql_data_array = array('articles_date_available' => $articles_date_available,
                                'articles_status' => tep_db_prepare_input($_POST['articles_status']),
                                'authors_id' => tep_db_prepare_input($_POST['authors_id']),
                                'articles_last_modified' =>'now()');

        if ($action == 'insert_article') {
          // If expected article then articles_date _added becomes articles_date_available
          if (isset($_POST['articles_date_available']) && tep_not_null($_POST['articles_date_available'])) {
            $insert_sql_data = array('articles_date_added' => tep_db_prepare_input($_POST['articles_date_available']));
          } else {
            $insert_sql_data = array('articles_date_added' => 'now()');
          }
          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
          tep_db_perform(TABLE_ARTICLES, $sql_data_array);
          $articles_id = tep_db_insert_id();
          tep_db_query("insert into " . TABLE_ARTICLES_TO_TOPICS . " (articles_id, topics_id) values ('" . (int)$articles_id . "', '" . (int)$current_topic_id . "')");
        } elseif ($action == 'update_article') {
          //$update_sql_data = array('articles_last_modified' => 'now()');
          // If expected article then articles_date _added becomes articles_date_available
          if (isset($_POST['articles_date_available']) && tep_not_null($_POST['articles_date_available'])) {
            $update_sql_data = array('articles_date_added' => tep_db_prepare_input($_POST['articles_date_available']));
            $sql_data_array = array_merge($sql_data_array, $update_sql_data);
          }


          tep_db_perform(TABLE_ARTICLES, $sql_data_array, 'update', "articles_id = '" . (int)$articles_id . "'");
        }

/*			$categories_image_tmp = '';
			$categories_file_destination = '/';
			$categories_image_tmp = new upload('categories_image');
			$categories_image_tmp->set_destination(DIR_FS_CATALOG_CATEGORIES . $categories_file_destination);
			if ($categories_image_tmp->parse() && $categories_image_tmp->save()) {
			  $categories_image_name = $categories_image_tmp->filename;
			  tep_db_query("update " . TABLE_CATEGORIES . " set categories_image = '" . $categories_image_name . "' where categories_id = '" . (int)$categories_id . "'");
			}
*/

	   //echo DIR_FS_CATALOG_ARTICLES;exit;
	   if ($articles_image = new upload('articles_image',DIR_FS_CATALOG_ARTICLES)) {
		 tep_db_query("update " . TABLE_ARTICLES . " set articles_image = '" . tep_db_input($articles_image->filename) . "'  where articles_id = '" . (int)$articles_id . "'");
	   }

        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $language_id = $languages[$i]['id'];
          $sql_data_array = array('articles_name' => tep_db_prepare_input($_POST['articles_name'][$language_id]),
                                  'articles_description' => tep_db_prepare_input($_POST['articles_description'][$language_id]),
                                  'articles_url' => tep_db_prepare_input($_POST['articles_url'][$language_id]),
                                  'articles_head_title_tag' => tep_db_prepare_input($_POST['articles_head_title_tag'][$language_id]),
                                  'articles_head_desc_tag' => tep_db_prepare_input($_POST['articles_head_desc_tag'][$language_id]),
                                  'articles_head_keywords_tag' => tep_db_prepare_input($_POST['articles_head_keywords_tag'][$language_id]));
 	          $permalink_name = tep_db_prepare_input($_POST['permalink_name'][$language_id]);
              $slug = sanitize($permalink_name);
          if ($action == 'insert_article') {
            $insert_sql_data = array('articles_id' => $articles_id,
                                     'language_id' => $language_id);
            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
            tep_db_perform(TABLE_ARTICLES_DESCRIPTION, $sql_data_array);

			if($_POST['permalink_name'][$language_id] != ''){
				 $check_art = tep_db_query("SELECT * FROM permalinks WHERE language_id = '".(int)$languages[$i]['id']."' and permalink_name='".$slug."' ");
               	 if(tep_db_num_rows($check_art) > 0){
               	    $slug = $slug."-".mt_rand(1, 100);
               	 	tep_db_query("INSERT INTO permalinks SET  articles_id = '".$articles_id."', route = 'core/articles_info', language_id='".$language_id."', permalink_name='".$slug."', permalink_type = 'articles' ");
               	 }else{
               	 	tep_db_query("INSERT INTO permalinks SET  articles_id = '".$articles_id."', route = 'core/articles_info', language_id='".$language_id."', permalink_name='".$slug."', permalink_type = 'articles' ");
			  	 }
			}

          } elseif ($action == 'update_article') {
            tep_db_perform(TABLE_ARTICLES_DESCRIPTION, $sql_data_array, 'update', "articles_id = '" . (int)$articles_id . "' and language_id = '" . (int)$language_id . "' ");
            	$check_permal = tep_db_query("SELECT * FROM permalinks WHERE language_id = '".(int)$languages[$i]['id']."' and articles_id='".$articles_id."' and permalink_type = 'articles'");
            	if(tep_db_num_rows($check_permal) == 0 || tep_db_num_rows($check_permal) == ''){
				 $check_art = tep_db_query("SELECT * FROM permalinks WHERE language_id = '".(int)$languages[$i]['id']."' and permalink_name='".$slug."' ");
               	 if(tep_db_num_rows($check_art) > 0){
               	    $slug = $slug."-".mt_rand(1, 100);
               	 	tep_db_query("INSERT INTO permalinks SET  articles_id = '".$articles_id."', route = 'core/articles_info', language_id='".$language_id."', permalink_name='".$slug."', permalink_type = 'articles' ");
               	 }else{
               	 	tep_db_query("INSERT INTO permalinks SET  articles_id = '".$articles_id."', route = 'core/articles_info', language_id='".$language_id."', permalink_name='".$slug."', permalink_type = 'articles' ");
			  	 }
			   }

          }
        }

		// RCI for action insert
		echo $cre_RCI->get('articles', 'articleaction', false);

        if (USE_CACHE == 'true') {
          tep_reset_cache_block('topics');
        }

		  $mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
		  if ($mode == 'save') {
			tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $articles_id));
		  } else {  // save & stay
			tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $articles_id . '&action=new_article'));
		  }
      }
      break;
    case 'copy_to_confirm':
      if (isset($_POST['articles_id']) && isset($_POST['topics_id'])) {
        $articles_id = tep_db_prepare_input($_POST['articles_id']);
        $topics_id = tep_db_prepare_input($_POST['topics_id']);
        if ($_POST['copy_as'] == 'link') {
          if ($topics_id != $current_topic_id) {
            $check_query = tep_db_query("select count(*) as total from " . TABLE_ARTICLES_TO_TOPICS . " where articles_id = '" . (int)$articles_id . "' and topics_id = '" . (int)$topics_id . "'");
            $check = tep_db_fetch_array($check_query);
            if ($check['total'] < '1') {
              tep_db_query("insert into " . TABLE_ARTICLES_TO_TOPICS . " (articles_id, topics_id) values ('" . (int)$articles_id . "', '" . (int)$topics_id . "')");
            }
          } else {
            $messageStack->add_session('search', ERROR_CANNOT_LINK_TO_SAME_TOPIC, 'error');
          }
        } elseif ($_POST['copy_as'] == 'duplicate') {
          $article_query = tep_db_query("select articles_date_available, authors_id from " . TABLE_ARTICLES . " where articles_id = '" . (int)$articles_id . "'");
          $article = tep_db_fetch_array($article_query);
          tep_db_query("insert into " . TABLE_ARTICLES . " (articles_date_added, articles_date_available, articles_status, authors_id) values (now(), '" . tep_db_input($article['articles_date_available']) . "', '0', '" . (int)$article['authors_id'] . "')");
          $dup_articles_id = tep_db_insert_id();
          $description_query = tep_db_query("select language_id, articles_name, articles_description, articles_url, articles_head_title_tag, articles_head_desc_tag, articles_head_keywords_tag from " . TABLE_ARTICLES_DESCRIPTION . " where articles_id = '" . (int)$articles_id . "'");
          while ($description = tep_db_fetch_array($description_query)) {
            tep_db_query("insert into " . TABLE_ARTICLES_DESCRIPTION . " (articles_id, language_id, articles_name, articles_description, articles_url, articles_head_title_tag, articles_head_desc_tag, articles_head_keywords_tag, articles_viewed) values ('" . (int)$dup_articles_id . "', '" . (int)$description['language_id'] . "', '" . tep_db_input($description['articles_name']) . "', '" . tep_db_input($description['articles_description']) . "', '" . tep_db_input($description['articles_url']) . "', '" . tep_db_input($description['articles_head_title_tag']) . "', '" . tep_db_input($description['articles_head_desc_tag']) . "', '" . tep_db_input($description['articles_head_keywords_tag']) . "', '0')");
          }
          tep_db_query("insert into " . TABLE_ARTICLES_TO_TOPICS . " (articles_id, topics_id) values ('" . (int)$dup_articles_id . "', '" . (int)$topics_id . "')");
          $articles_id = $dup_articles_id;
        }
        if (USE_CACHE == 'true') {
          tep_reset_cache_block('topics');
        }
      }
      tep_redirect(tep_href_link(FILENAME_ARTICLES, 'tPath=' . $topics_id . '&aID=' . $articles_id));
      break;
  }
}
// check if the catalog image directory exists
if (is_dir(DIR_FS_CATALOG_IMAGES)) {
  if (!is_writeable(DIR_FS_CATALOG_IMAGES)) $messageStack->add('search', ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
} else {
  $messageStack->add('search', ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
}
// check if the authors table is empty.  articles must be assigned an author to be shown in storefront.
$authors_query = tep_db_query("select * from " . TABLE_AUTHORS);
if (tep_db_num_rows($authors_query) == 0){
  $add_str = '<a href="' . tep_href_link(FILENAME_AUTHORS, '&action=new') . '">' . tep_image_button('button_new_author.gif', IMAGE_NEW_AUTHOR) . '</a>';
  $messageStack->add('search', TEXT_WARNING_NO_AUTHORS, 'warning');
} else {
  $add_str = '<a class="btn btn-success btn-sm mt-2 mb-2 ml-2" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&action=new_article') . '">' . tep_image_button('button_new_article.gif', IMAGE_NEW_ARTICLE) . '</a>';
}


include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');

   //----- new_topic / edit_topic  -----
  if ((isset($_GET['action'])) && ($_GET['action'] == 'new_topic_ACD' || $_GET['action'] == 'edit_topic_ACD')) {
    if ( isset($_GET['tID']) && (!$_POST) ) {
      $topics_query = tep_db_query("select t.topics_id, td.topics_name, td.topics_heading_title, td.topics_description, t.parent_id, t.sort_order, t.date_added, t.last_modified from " . TABLE_TOPICS . " t, " . TABLE_TOPICS_DESCRIPTION . " td where t.topics_id = '" . $_GET['tID'] . "' and t.topics_id = td.topics_id and td.language_id = '" . $languages_id . "' order by t.sort_order, td.topics_name");
      $topic = tep_db_fetch_array($topics_query);

      $tInfo = new objectInfo($topic);
    } elseif ($_POST) {
      $tInfo = new objectInfo($_POST);
      $topics_name = $_POST['topics_name'];
      $topics_heading_title = $_POST['topics_heading_title'];
      $topics_description = $_POST['topics_description'];
      $topics_url = $_POST['topics_url'];
    } else {
      $tInfo = new objectInfo(array());
    }

    $languages = tep_get_languages();

    $text_new_or_edit = ($_GET['action']=='new_topic_ACD') ? TEXT_INFO_HEADING_NEW_TOPIC : TEXT_INFO_HEADING_EDIT_TOPIC;
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo sprintf($text_new_or_edit, tep_output_generated_topic_path($current_topic_id)); ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>


  <div class="col">
    <?php
    if ($messageStack->size('articles') > 0) {
      echo $messageStack->output('articles');
    }
    ?>

<?php
echo '<form id="new_topic" name="new_topic" method="post" enctype="multipart/form-data" data-parsley-validate>';
?>
      <!-- begin button bar -->
      <div id="button-bar" class="row">
        <div class="col-9 m-b-10 w-100 pt-1 pl-0 pr-0">
            <a href="<?php echo tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . ((isset($tID) && $tID != '') ? '&tID=' . $tID : '')); ?>" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> <?php echo ((isset($cID) && empty($cID) === false) ? BUTTON_RETURN_TO_LIST :  IMAGE_CANCEL); ?></a>
            <button type="submit" onclick="updateTopic('save');" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo BUTTON_SAVE; ?></button>
            <button type="submit" onclick="updateTopic('stay');" class="btn btn-info m-r-3 btn-save-stay"><i class="fa fa-plus-square"></i> <?php echo BUTTON_SAVE_STAY; ?></button>
        </div>
		<div class="col-3 m-b-10 pt-1 pr-2">
		  <div class="btn-group pull-right dark"> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class="btn btn-white dropdown-toggle"> <span id="langDropdownTitle"><?php echo ucwords($_SESSION['language']); ?></span> <span class="caret"></span> </a>
			<ul class="dropdown-menu pull-right" id="langselector" style="left:-50px!important;">
			  <?php
			  $languages = tep_get_languages();
			  for ($i=0; $i<sizeof($languages); $i++) {
				?>
				<li id="langsel_<?php echo $languages[$i]['id']; ?>" class="langval<?php echo (($languages[$i]['id'] == $_SESSION['languages_id'])? ' active':'');?>">
				  <a aria-expanded="false" href="javascript:changeLang('page', '<?php echo $languages[$i]['name'];?>', <?php echo $languages[$i]['id']; ?>)"><?php echo '<span class="ml-2">' . $languages[$i]['name'];?></span></a>
				</li>
				<?php
			  }
			  ?>
			</ul>
		  </div>
		</div>
	  </div>
      <!-- end button bar -->

    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-articles" class="table-articles">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
<?php
    for ($i=0; $i<sizeof($languages); $i++)
    {
		$display = ($languages[$i]['id'] == $_SESSION['languages_id']) ? '' : 'display:none;';
		$required_validation = ($languages[$i]['id'] == $_SESSION['languages_id']) ? ' required ' : '';
		$lang_control = '<div class="input-group"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
?>
			 <div style="<?php echo $display; ?>" class="page-lang-pane <?php echo (($languages[$i]['id'] == $_SESSION['languages_id']) ? 'active' : '');?>" id="page-default-pane-<?php echo $languages[$i]['id'];?>">
			  <div class="ml-2 mr-2">
				<div class="main-heading"><span>Topic Info</span>
				  <div class="main-heading-footer"></div>
				</div>

				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_EDIT_TOPICS_NAME; ?><span class="required"></span></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php
					if (isset($tInfo->topics_id)) {
					  echo $lang_control.tep_draw_input_field('topics_name[' . $languages[$i]['id'] . ']', (isset($topics_name[$languages[$i]['id']]) ? stripslashes($topics_name[$languages[$i]['id']]) : tep_get_topic_name((isset($tInfo->topics_id) ? $tInfo->topics_id : 0), $languages[$i]['id'])), 'id="topics_name" class="form-control f-w-600 f-s-12 p-l-10 p-r-10" onblur="javascript:getslug('.$languages[$i]['id'].', '.$tInfo->topics_id.', this.value);"'.$required_validation).'</div>';
					} else{
					   echo $lang_control.tep_draw_input_field('topics_name[' . $languages[$i]['id'] . ']', (isset($topics_name[$languages[$i]['id']]) ? stripslashes($topics_name[$languages[$i]['id']]) : tep_get_topic_name((isset($tInfo->topics_id) ? $tInfo->topics_id : 0), $languages[$i]['id'])), 'id="topics_name" class="form-control f-w-600 f-s-12 p-l-10 p-r-10" onblur="javascript:getslug('.$languages[$i]['id'].', 0, this.value);"'.$required_validation).'</div>';
					}
					?>
				 </div>
				</div>
				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo LABEL_PERMALINK; ?><span class="required"></span></label>
				  <?php
					if (isset($tInfo->topics_id)) {
				     echo '<div class="col-xs-7 col-md-8 col-lg-6 p-r-0 meta-input">';
					 echo $lang_control.tep_draw_input_field('permalink_name[' . $languages[$i]['id'] . ']', tep_get_topic_permalink_name($tInfo->topics_id,$languages[$i]['id']), 'id="permalink_' . $languages[$i]['id'] . '" class="form-control f-w-600 f-s-12 p-l-10 p-r-10" readonly="true"'.$required_validation).'</div>';
					 echo '</div>';
					 echo '<div class="col-xs-7 col-md-8 col-lg-4 p-r-0">';
					?>
						  <a href="javascript:void(0);" class="btn btn-primary editbtn_<?php echo $languages[$i]['id'];?>" style="display:inline-block" onclick="javascript:enableEditPermalink('<?php echo $languages[$i]['id'];?>')">Edit</a>
						  <a href="javascript:void(0);" class="btn btn-primary savebtn_<?php echo $languages[$i]['id'];?>"  onclick="javascript:saveEditPermalink('<?php echo $languages[$i]['id'];?>', '<?php echo $tInfo->topics_id;?>','topics');"  style="display:none">Save</a>
						   <i class="fa fa-check" id="tickmark" aria-hidden="true" style="font-size:20px;color:#008000;display:none;"></i>
						  <img src="images/loader-30.gif" style="display:none;margin-left:5px" id="ploader"/>
					<?php
					} else{
					   echo '<div class="col-xs-7 col-md-8 col-lg-9 p-r-0">';
					   echo $lang_control.tep_draw_input_field('permalink_name[' . $languages[$i]['id'] . ']', '', 'id="permalink_' . $languages[$i]['id'] . '" class="form-control f-w-600 f-s-12 p-l-10 p-r-10" onblur="javascript:checkslug('.$languages[$i]['id'].', 0);"'.$required_validation).'</div>';
					}
					?>
				 </div>
				</div>
				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_EDIT_TOPICS_HEADING_TITLE; ?><span class="required"></span></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php echo $lang_control.tep_draw_input_field('topics_heading_title[' . $languages[$i]['id'] . ']', (isset($topics_name[$languages[$i]['id']]) ? stripslashes($topics_name[$languages[$i]['id']]) : tep_get_topic_heading_title((isset($tInfo->topics_id) ? $tInfo->topics_id : 0), $languages[$i]['id'])), 'class="form-control f-w-600 f-s-12 p-l-10 p-r-10"'.$required_validation).'</div>'; ?>
				  </div>
				</div>
				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_EDIT_TOPICS_DESCRIPTION; ?></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php echo tep_draw_textarea_field('topics_description[' . $languages[$i]['id'] . ']', 'soft', '70', '20', (isset($topics_description[$languages[$i]['id']]) ? stripslashes($topics_description[$languages[$i]['id']]) : tep_get_topic_description((isset($tInfo->topics_id) ? $tInfo->topics_id : 0), $languages[$i]['id'])), 'class="ckeditor"'); ?>
				  </div>
				</div>

               </div>
              </div>
<?php
    }
?>
	</div>
	<div class="col-md-3 col-xl-2 panel-right rounded-right light">
		<div class="sidebar-container p-4"><div class="sidebar-heading"><span><div class="text-truncate">Options</div></span></div>
		<div class="sidebar-heading-footer"></div>
			<div class="sidebar-content-container">
				<div class="form-group row mt-3">
				  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo TEXT_EDIT_SORT_ORDER; ?></label>
				  <div class="col-sm-7 p-relative"><?php echo  tep_draw_input_field('sort_order', (isset($tInfo->sort_order) ? $tInfo->sort_order : 0), 'size="2"'); ?></div>
				</div>
				<?php
				if ($cre_RCO->get('articles', 'topicaccessgroup') !== true) {
				?>
						<div class="form-group row mt-3">
						  <div class="col-sm-12">
						   <div data-toggle="popover" data-placement="top" data-html="true" data-content='<div class="text-white"><?php echo TEXT_B2B_UPSELL_POPOVER_BODY; ?></div><div class="text-center w-100"><a href="<?php echo TEXT_B2B_UPSELL_GET_B2B_URL; ?>" target="_blank" class="btn btn-warning btn-sm m-r-5 m-t-10"><?php echo TEXT_B2B_UPSELL_GET_B2B; ?></a></div>'>
							<img src="images/category-access-settings.jpg" alt="Get B2B to unlock this feature.">
						   </div>
						  </div>
						</div>
				<?php
				}
				?>
		  </div>
    </div>
	</form>
<?php
  }
  elseif ($action == 'new_article')
  {
    $parameters = array('articles_name' => '',
                       'articles_description' => '',
                       'articles_url' => '',
                       'articles_head_title_tag' => '',
                       'articles_head_desc_tag' => '',
                       'articles_head_keywords' => '',
                       'articles_id' => '',
                       'articles_date_added' => '',
                       'articles_last_modified' => '',
                       'articles_date_available' => '',
                       'articles_status' => '',
                       'authors_id' => '');

    $aInfo = new objectInfo($parameters);

    if (isset($_GET['aID']) && empty($_POST)) {
      $article_query = tep_db_query("select ad.articles_name, ad.articles_description, ad.articles_url, ad.articles_head_title_tag, ad.articles_head_desc_tag, ad.articles_head_keywords_tag, a.articles_id, a.articles_date_added,a.articles_image, a.articles_last_modified, date_format(a.articles_date_available, '%Y-%m-%d') as articles_date_available, a.articles_status, a.authors_id from " . TABLE_ARTICLES . " a, " . TABLE_ARTICLES_DESCRIPTION . " ad where a.articles_id = '" . (int)$_GET['aID'] . "' and a.articles_id = ad.articles_id and ad.language_id = '" . (int)$languages_id . "'");
      $article = tep_db_fetch_array($article_query);

      $aInfo->objectInfo($article);
    } elseif (isset($_POSTS) && tep_not_null($_POST)) {
      $aInfo->objectInfo($_POST);
      $articles_name = $_POST['articles_name'];
      $articles_description = $_POST['articles_description'];
      $articles_url = $_POST['articles_url'];
      $articles_head_title_tag = $_POST['articles_head_title_tag'];
      $articles_head_desc_tag = $_POST['articles_head_desc_tag'];
      $articles_head_keywords_tag = $_POST['articles_head_keywords_tag'];
    }

    $authors_array = array(array('id' => '', 'text' => TEXT_NONE));
    $authors_query = tep_db_query("select authors_id, authors_name from " . TABLE_AUTHORS . " order by authors_name");
    while ($authors = tep_db_fetch_array($authors_query)) {
      $authors_array[] = array('id' => $authors['authors_id'],
                                     'text' => $authors['authors_name']);
    }

    $languages = tep_get_languages();

    if (!isset($aInfo->articles_status)) $aInfo->articles_status = '1';
    switch ($aInfo->articles_status) {
      case '0': $in_status = false; $out_status = true; break;
      case '1':
      default: $in_status = true; $out_status = false;
    }
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo sprintf(TEXT_NEW_ARTICLE, tep_output_generated_topic_path($current_topic_id)); ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>


  <div class="col">
    <?php
    if ($messageStack->size('articles') > 0) {
      echo $messageStack->output('articles');
    }
    ?>

<?php
echo '<form id="new_article" name="new_article" method="post" enctype="multipart/form-data" data-parsley-validate>';
?>
      <!-- begin button bar -->
      <div id="button-bar" class="row">
        <div class="col-9 m-b-10 w-100 pt-1 pl-0 pr-0">
            <a href="<?php echo tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . ((isset($aID) && $aID != '') ? '&aID=' . $aID : '')); ?>" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> <?php echo ((isset($cID) && empty($cID) === false) ? BUTTON_RETURN_TO_LIST :  IMAGE_CANCEL); ?></a>
            <button type="submit" onclick="updateArticle('save');" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo BUTTON_SAVE; ?></button>
            <button type="submit" onclick="updateArticle('stay');" class="btn btn-info m-r-3 btn-save-stay"><i class="fa fa-plus-square"></i> <?php echo BUTTON_SAVE_STAY; ?></button>
        </div>
		<div class="col-3 m-b-10 pt-1 pr-2">
		  <div class="btn-group pull-right dark"> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class="btn btn-white dropdown-toggle"> <span id="langDropdownTitle"><?php echo ucwords($_SESSION['language']); ?></span> <span class="caret"></span> </a>
			<ul class="dropdown-menu pull-right" id="langselector" style="left:-50px!important;">
			  <?php
			  $languages = tep_get_languages();
			  for ($i=0; $i<sizeof($languages); $i++) {
				?>
				<li id="langsel_<?php echo $languages[$i]['id']; ?>" class="langval<?php echo (($languages[$i]['id'] == $_SESSION['languages_id'])? ' active':'');?>">
				  <a aria-expanded="false" href="javascript:changeLang('page', '<?php echo $languages[$i]['name'];?>', <?php echo $languages[$i]['id']; ?>)"><?php echo '<span class="ml-2">' . $languages[$i]['name'];?></span></a>
				</li>
				<?php
			  }
			  ?>
			</ul>
		  </div>
		</div>
	  </div>
      <!-- end button bar -->

    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-articles" class="table-articles">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

<?php
    for ($i=0; $i<sizeof($languages); $i++)
    {
		$display = ($languages[$i]['id'] == $_SESSION['languages_id']) ? '' : 'display:none;';
		$required_validation = ($languages[$i]['id'] == $_SESSION['languages_id']) ? ' required ' : '';
		$lang_control = '<div class="input-group"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
?>
			 <div style="<?php echo $display; ?>" class="page-lang-pane <?php echo (($languages[$i]['id'] == $_SESSION['languages_id']) ? 'active' : '');?>" id="page-default-pane-<?php echo $languages[$i]['id'];?>">
			  <div class="ml-2 mr-2">
				<div class="main-heading"><span>Article Info</span>
				  <div class="main-heading-footer"></div>
				</div>

				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_ARTICLES_NAME; ?><span class="required"></span></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php
						if (isset($aInfo->articles_id) && $aInfo->articles_id > 0) {
						  echo $lang_control.tep_draw_input_field('articles_name[' . $languages[$i]['id'] . ']', (isset($articles_name[$languages[$i]['id']]) ? $articles_name[$languages[$i]['id']] : tep_get_articles_name($aInfo->articles_id, $languages[$i]['id'])), ' id="articles_name"  onblur="javascript:arti_getslug('.$languages[$i]['id'].','.$aInfo->articles_id.', this.value);"'.$required_validation).'</div>';
						} else{
						  echo $lang_control.tep_draw_input_field('articles_name[' . $languages[$i]['id'] . ']', (isset($articles_name[$languages[$i]['id']]) ? $articles_name[$languages[$i]['id']] : tep_get_articles_name($aInfo->articles_id, $languages[$i]['id'])), ' id="articles_name"  onblur="javascript:arti_getslug('.$languages[$i]['id'].',0, this.value);"'.$required_validation).'</div>';
						}
					?>
				  </div>
				</div>
				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo LABEL_PERMALINK; ?><span class="required"></span></label>
				  <?php
					if (isset($aInfo->articles_id) && $aInfo->articles_id > 0) {
				     echo '<div class="col-xs-7 col-md-8 col-lg-6 p-r-0 meta-input">';
					 echo $lang_control.tep_draw_input_field('permalink_name[' . $languages[$i]['id'] . ']', tep_get_article_permalink_name($aInfo->articles_id), 'id="permalink_' . $languages[$i]['id'] . '" class="form-control f-w-600 f-s-12 p-l-10 p-r-10" readonly="true"'.$required_validation).'</div>';
					 echo '</div>';
					 echo '<div class="col-xs-7 col-md-8 col-lg-4 p-r-0">';
					?>
						  <a href="javascript:void(0);" class="btn btn-primary editbtn_<?php echo $languages[$i]['id'];?>" style="display:inline-block" onclick="javascript:enableEditPermalink('<?php echo $languages[$i]['id'];?>')">Edit</a>
						  <a href="javascript:void(0);" class="btn btn-primary savebtn_<?php echo $languages[$i]['id'];?>"  onclick="javascript:saveEditPermalink('<?php echo $languages[$i]['id'];?>','<?php echo $aInfo->articles_id;?>','articles');"  style="display:none">Save</a>
						   <i class="fa fa-check" id="tickmark" aria-hidden="true" style="font-size:20px;color:#008000;display:none;"></i>
						  <img src="images/loader-30.gif" style="display:none;margin-left:5px" id="ploader"/>
					<?php
					} else{
					   echo '<div class="col-xs-7 col-md-8 col-lg-9 p-r-0">';
					   echo $lang_control.tep_draw_input_field('permalink_name[' . $languages[$i]['id'] . ']', '', 'id="permalink_' . $languages[$i]['id'] . '" class="form-control f-w-600 f-s-12 p-l-10 p-r-10" onblur="javascript:arti_checkslug('.$languages[$i]['id'].', 0);"'.$required_validation).'</div>';
					}
					?>
				 </div>
				</div>
				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo TEXT_ARTICLES_DESCRIPTION; ?></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php echo tep_draw_textarea_field('articles_description[' . $languages[$i]['id'] . ']', 'soft', '70', '15', (isset($articles_description[$languages[$i]['id']]) ? $articles_description[$languages[$i]['id']] : tep_get_articles_description($aInfo->articles_id, $languages[$i]['id'])), 'class="form-control ckeditor"'); ?>
				  </div>
				</div>

				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php  echo TEXT_ARTICLES_URL . '<br><small>' . TEXT_ARTICLES_URL_WITHOUT_HTTP . '</small>'; ?></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php echo tep_draw_input_field('articles_url[' . $languages[$i]['id'] . ']', (isset($articles_url[$languages[$i]['id']]) ? $articles_url[$languages[$i]['id']] : tep_get_articles_url($aInfo->articles_id, $languages[$i]['id'])), 'size="35"'); ?>
				  </div>
				</div>

                      <!-- SEO & META TAGS start-->
                      <div class="ml-2 mr-2">
                        <div class="main-heading m-t-20"><span><?php echo HEADING_META_TAGS; ?></span>
                          <div class="main-heading-footer"></div>
                        </div>

                        <div class="form-group row mb-3 mt-3">
                          <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo LABEL_META_TITLE; ?></label>
                          <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
							<?php echo tep_draw_input_field('articles_head_title_tag[' . $languages[$i]['id'] . ']', (isset($articles_head_title_tag[$languages[$i]['id']]) ? $articles_head_title_tag[$languages[$i]['id']] : tep_get_articles_head_title_tag($aInfo->articles_id, $languages[$i]['id'])), 'size="35"'); ?>
                          </div>
                          <div class="col-xs-1 p-l-0 p-r-0 p-relative">
                            <div class="notify-container-meta-title rounded-left rounded-right"><span class="text-black"><?php echo TEXT_COPIED; ?></span></div>
                            <div id="cat-meta-title-ctc-options" class="btn-group btn-xs "> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class=" f-w-100 text-right btn btn-xs btn-white dropdown-toggle width-full"> <span class="caret"></span> </a>
                              <ul id="cat-meta-title-ctc-list" class="dropdown-menu pull-left">
                                <?php
                                for ($j=0; $j<sizeof($languages); $j++) {
                                  ?>
                                  <li><a data-lang-name="<?php echo ucwords($languages[$j]['name']); ?>" data-lang-id="<?php echo $languages[$j]['id']; ?>" aria-expanded="false" href="javascript:;"><i class="fa fa-clipboard mr-1" aria-hidden="true"></i><?php echo sprintf(TEXT_COPY_LANG_TO_CLIPBOARD, $languages[$j]['name']);?></a></li>
                                  <?php
                                }
                                ?>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row mb-3">
                          <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo LABEL_META_KEYWORDS;?></label>
                          <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
							<?php echo tep_draw_textarea_field('articles_head_keywords_tag[' . $languages[$i]['id'] . ']', 'soft', '70', '1', (isset($articles_head_keywords_tag[$languages[$i]['id']]) ? $articles_head_keywords_tag[$languages[$i]['id']] : tep_get_articles_head_keywords_tag($aInfo->articles_id, $languages[$i]['id'])), 'class="form-control"'); ?>
                          </div>
                          <div class="col-xs-1 p-l-0 p-r-0 p-relative">
                            <div id="cat-meta-keywords-ctc-options" class="notify-container-meta-keywords rounded-left rounded-right"><span class="text-black"><?php echo TEXT_COPIED; ?></span></div>
                            <div id="cat-meta-keywords-ctc-list" class="btn-group btn-xs "> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class=" f-w-100 text-right btn btn-xs btn-white dropdown-toggle width-full"> <span class="caret"></span> </a>
                              <ul class="dropdown-menu pull-left">
                                <?php
                                for ($j=0; $j<sizeof($languages); $j++) {
                                  ?>
                                  <li><a data-lang-name="<?php echo ucwords($languages[$j]['name']); ?>" data-lang-id="<?php echo $languages[$j]['id']; ?>" aria-expanded="false" href="javascript:;"><i class="fa fa-clipboard mr-1" aria-hidden="true"></i><?php echo sprintf(TEXT_COPY_LANG_TO_CLIPBOARD, $languages[$j]['name']);?></a></li>
                                  <?php
                                }
                                ?>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row mb-3">
                          <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1 pl-0"><?php echo LABEL_META_DESCRIPTION; ?></label>
                          <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
							<?php echo tep_draw_textarea_field('articles_head_desc_tag[' . $languages[$i]['id'] . ']', 'soft', '70', '1', (isset($articles_head_desc_tag[$languages[$i]['id']]) ? $articles_head_desc_tag[$languages[$i]['id']] : tep_get_articles_head_desc_tag($aInfo->articles_id, $languages[$i]['id'])), 'class="form-control"'); ?>
                          </div>
                          <div class="col-xs-1 p-l-0 p-r-0 p-relative">
                            <div id="cat-meta-desc-ctc-options" class="notify-container-meta-desc rounded-left rounded-right"><span class="text-black"><?php echo TEXT_COPIED; ?></span></div>
                            <div id="cat-meta-desc-ctc-list" class="btn-group btn-xs"> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class=" f-w-100 text-right btn btn-xs btn-white dropdown-toggle width-full"> <span class="caret"></span> </a>
                              <ul class="dropdown-menu pull-left">
                                <?php
                                for ($j=0; $j<sizeof($languages); $j++) {
                                  ?>
                                  <li><a data-lang-name="<?php echo ucwords($languages[$j]['name']); ?>" data-lang-id="<?php echo $languages[$j]['id']; ?>" aria-expanded="false" href="javascript:;"><i class="fa fa-clipboard mr-1" aria-hidden="true"></i><?php echo sprintf(TEXT_COPY_LANG_TO_CLIPBOARD, $languages[$j]['name']);?></a></li>
                                  <?php
                                }
                                ?>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- SEO & META TAGS end-->
               </div>
              </div>
<?php
    }
?>
			<hr>
			<div class="form-group row mb-3 m-t-20 p-relative">
			  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1">Articles Image:</label>
			  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
				<?php echo tep_draw_file_field('articles_image', (isset($aInfo->articles_image)?$aInfo->articles_image:'') ,'', 'class="filestyle"'); ?>
			  </div>
			</div>
			<?php
			 if($aInfo->articles_image !=''){
			 echo '<div class="form-group row mb-3 m-t-20 p-relative"><label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1">Articles Image:</label><div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input"><a href="javascript:" onclick="$(\'input[name=articles_image]\').click();" >'.tep_image(HTTP_SERVER.DIR_WS_HTTP_CATALOG.DIR_WS_ARTICLES . $aInfo->articles_image, $articles_image, '100', '100', 'class="media-object mt-2"').'</a></div></div>';
			}
			?>
		</div>
		<div class="col-md-3 col-xl-2 panel-right rounded-right light">
			<div class="sidebar-container p-4"><div class="sidebar-heading"><span><div class="text-truncate">Options</div></span></div>
			<div class="sidebar-heading-footer"></div>
				<div class="sidebar-content-container">
					<div class="form-group row mt-3">
					  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo TEXT_ARTICLES_STATUS; ?></label>
					  <div class="col-sm-7 p-relative"><?php echo  tep_draw_radio_field('articles_status', '0', $out_status) . '&nbsp;' . TEXT_ARTICLE_NOT_AVAILABLE . '&nbsp;' . tep_draw_radio_field('articles_status', '1', $in_status) . '&nbsp;' . TEXT_ARTICLE_AVAILABLE; ?></div>
					</div>

					<div class="form-group row mt-3">
					  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo TEXT_ARTICLES_DATE_AVAILABLE; ?></label>
					  <div class="col-sm-7 p-relative"><?php echo tep_draw_input_field('articles_date_available', (($aInfo->articles_date_available == '0000-00-00' || $aInfo->articles_date_available == '')?'':date('m/d/Y', strtotime($aInfo->articles_date_available))),'class="calender form-control" id="calender" data-date-format="mm/dd/yyyy"'); ?></div>
					</div>

					<div class="form-group row mt-3">
					  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo TEXT_ARTICLES_AUTHOR; ?></label>
					  <div class="col-sm-7 p-relative"><?php echo  tep_draw_pull_down_menu('authors_id', $authors_array, $aInfo->authors_id); ?></div>
					</div>
					<?php
					if ($cre_RCO->get('articles', 'accessgroup') !== true) {
					?>
							<div class="form-group row mt-3">
							  <div class="col-sm-12">
							   <div data-toggle="popover" data-placement="top" data-html="true" data-content='<div class="text-white"><?php echo TEXT_B2B_UPSELL_POPOVER_BODY; ?></div><div class="text-center w-100"><a href="<?php echo TEXT_B2B_UPSELL_GET_B2B_URL; ?>" target="_blank" class="btn btn-warning btn-sm m-r-5 m-t-10"><?php echo TEXT_B2B_UPSELL_GET_B2B; ?></a></div>'>
								<img src="images/category-access-settings.jpg" alt="Get B2B to unlock this feature.">
							   </div>
							  </div>
							</div>
					<?php
					}
					?>
			  </div>
		</div>

    </form>
<?php
  } else {
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

	<div class="col main-col">
	  <div class="row">
		<div class="col-6 pr-0 mb-2"></div>
		<div class="col-6 pr-1 pb-2">

<table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pr-2">
<?php
    echo tep_draw_form('search', FILENAME_ARTICLES, '', 'get');
    echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search');
    if (isset($_GET[tep_session_name()])) {
      echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
    }
    echo '</form>';
?>
                </td>
                <td class="pr-2">
<?php
    echo tep_draw_form('goto', FILENAME_ARTICLES, '', 'get');
    echo HEADING_TITLE_GOTO . ' ' . tep_draw_pull_down_menu('tPath', tep_get_topic_tree(), $current_topic_id, 'onChange="this.form.submit();"');
    if (isset($_GET[tep_session_name()])) {
      echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
    }
    echo '</form>';
?>
                </td>
              </tr>
            </table>
		</div>
	  </div>
	</div>

  <div class="col">
    <?php
    if ($messageStack->size('articles') > 0) {
      echo $messageStack->output('articles');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-articles" class="table-articles">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

		   <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_TOPICS_ARTICLES; ?></th>
				<th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_STATUS; ?></th>
				<th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
			  </tr>
             </thead>
<?php
    $topics_count = 0;
    $rows = 0;
    if (isset($_GET['search'])) {
      $search = tep_db_prepare_input($_GET['search']);

      $topics_query = tep_db_query("select t.topics_id, td.topics_name, t.parent_id, t.sort_order, t.date_added, t.last_modified from " . TABLE_TOPICS . " t, " . TABLE_TOPICS_DESCRIPTION . " td where t.topics_id = td.topics_id and td.language_id = '" . (int)$languages_id . "' and td.topics_name like '%" . tep_db_input($search) . "%' order by t.sort_order, td.topics_name");
    } else {
      $topics_query = tep_db_query("select t.topics_id, td.topics_name, t.parent_id, t.sort_order, t.date_added, t.last_modified from " . TABLE_TOPICS . " t, " . TABLE_TOPICS_DESCRIPTION . " td where t.parent_id = '" . (int)$current_topic_id . "' and t.topics_id = td.topics_id and td.language_id = '" . (int)$languages_id . "' order by t.sort_order, td.topics_name");
    }
    while ($topics = tep_db_fetch_array($topics_query)) {
      $topics_count++;
      $rows++;

// Get parent_id for subtopics if search
      if (isset($_GET['search'])) $tPath= $topics['parent_id'];

      if ((!isset($_GET['tID']) && !isset($_GET['aID']) || (isset($_GET['tID']) && ($_GET['tID'] == $topics['topics_id']))) && !isset($tInfo) && (substr($action, 0, 3) != 'new')) {
        $topic_childs = array('childs_count' => tep_childs_in_topic_count($topics['topics_id']));
        $topic_articles = array('articles_count' => tep_articles_in_topic_count($topics['topics_id']));

        $tInfo_array = array_merge($topics, $topic_childs, $topic_articles);
        $tInfo = new objectInfo($tInfo_array);
      }

	  $selected = (isset($tInfo) && is_object($tInfo) && ($topics['topics_id'] == $tInfo->topics_id))? true : false;
	  $col_selected = ($selected) ? ' selected' : '';
	  if ($selected) {
        echo '              <tr id="defaultSelected" class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_ARTICLES, tep_get_topic_path($topics['topics_id'])) . '\'">' . "\n";
      } else {
        echo '              <tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $topics['topics_id']) . '\'">' . "\n";
      }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLES, tep_get_topic_path($topics['topics_id'])) . '"><i class="fa fa-folder fa-lg text-warning mr-2"></i></a>&nbsp;<b>' . $topics['topics_name'] . '</b>'; ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>">&nbsp;</td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if (isset($tInfo) && is_object($tInfo) && ($topics['topics_id'] == $tInfo->topics_id) ) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>' ; } else { echo '<a href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $topics['topics_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }

    $articles_count = 0;
    if (isset($_GET['search'])) {
      $articles_query = tep_db_query("select a.articles_id, ad.articles_name, a.articles_date_added, a.articles_last_modified, a.articles_date_available, a.articles_status, a2t.topics_id,a.articles_image from " . TABLE_ARTICLES . " a, " . TABLE_ARTICLES_DESCRIPTION . " ad, " . TABLE_ARTICLES_TO_TOPICS . " a2t where a.articles_id = ad.articles_id and ad.language_id = '" . (int)$languages_id . "' and a.articles_id = a2t.articles_id and ad.articles_name like '%" . tep_db_input($search) . "%' order by ad.articles_name");
    } else {
      $articles_query = tep_db_query("select a.articles_id, ad.articles_name, a.articles_date_added, a.articles_last_modified, a.articles_date_available, a.articles_status,a.articles_image from " . TABLE_ARTICLES . " a, " . TABLE_ARTICLES_DESCRIPTION . " ad, " . TABLE_ARTICLES_TO_TOPICS . " a2t where a.articles_id = ad.articles_id and ad.language_id = '" . (int)$languages_id . "' and a.articles_id = a2t.articles_id and a2t.topics_id = '" . (int)$current_topic_id . "' order by ad.articles_name");
    }
    while ($articles = tep_db_fetch_array($articles_query)) {
      $articles_count++;
      $rows++;

// Get topics_id for article if search
      if (isset($_GET['search'])) $tPath = $articles['topics_id'];

      if ( (!isset($_GET['aID']) && !isset($_GET['tID']) || (isset($_GET['aID']) && ($_GET['aID'] == $articles['articles_id']))) && !isset($aInfo) && !isset($tInfo) && (substr($action, 0, 3) != 'new')) {
// find out the rating average from customer reviews
        $reviews_query = tep_db_query("select (avg(reviews_rating) / 5 * 100) as average_rating from " . TABLE_ARTICLE_REVIEWS . " where articles_id = '" . (int)$articles['articles_id'] . "'");
        $reviews = tep_db_fetch_array($reviews_query);
        $aInfo_array = array_merge($articles, $reviews);
        $aInfo = new objectInfo($aInfo_array);
      }

	  $selected = (isset($aInfo) && is_object($aInfo) && ($articles['articles_id'] == $aInfo->articles_id))? true : false;
	  $col_selected = ($selected) ? ' selected' : '';
      if ($selected) {
        echo '<tr  class="table-row dark selected" id="crow_'.$articles['articles_id'].'" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" id="art_'.$articles['articles_id'].'">' . "\n";
        $onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $articles['articles_id'] . '&action=article_preview&read=only') . '\'"';
      } else {
        echo '<tr class="table-row dark" id="crow_'.$articles['articles_id'].'" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" id="art_'.$articles['articles_id'].'">' . "\n";
        $onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $articles['articles_id']) . '\'"';
      }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $articles['articles_id'] . '&action=article_preview&read=only') . '"><i class="fa fa-circle-o fa-lg text-info mr-2 mr-2"></i></a>&nbsp;' . $articles['articles_name']; ?></td>
                <td class="setflag table-col dark text-center<?php echo $col_selected; ?>">
				<?php
				     $ajax_link = tep_href_link(FILENAME_ARTICLES);
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=0&aID='.$articles['articles_id'].'\', '.$articles['articles_id'].',0 )" '.(($articles['articles_status'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=1&aID='.$articles['articles_id'].'\', '.$articles['articles_id'].',1  )" '.(($articles['articles_status'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
				?>
		      </td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if (isset($aInfo) && is_object($aInfo) && ($articles['articles_id'] == $aInfo->articles_id)) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>' ; } else { echo '<a href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $articles['articles_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }

    $tPath_back = '';
    if (!isset($tPath_array)) {
      $tPath_array = array();
    }
    if (sizeof($tPath_array) > 0) {
      for ($i=0, $n=sizeof($tPath_array)-1; $i<$n; $i++) {
        if (empty($tPath_back)) {
          $tPath_back .= $tPath_array[$i];
        } else {
          $tPath_back .= '_' . $tPath_array[$i];
        }
      }
    }

    $tPath_back = (tep_not_null($tPath_back)) ? 'tPath=' . $tPath_back . '&' : '';
?>
</table>
			<table border="0" width="100%" cellspacing="0" cellpadding="0" class="data-table-foot">
                  <tr>
                    <td class="smallText"><?php echo TEXT_TOPICS . '&nbsp;' . $topics_count . '<br>' . TEXT_ARTICLES . '&nbsp;' . $articles_count; ?></td>
                    <td align="right" class="smallText"><?php if (sizeof($tPath_array) > 0) echo '<a class="btn btn-default btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_ARTICLES, $tPath_back . 'tID=' . $current_topic_id) . '">' . IMAGE_BACK . '</a>'; if (!isset($_GET['search'])) echo '<a class="btn btn-success btn-sm mt-2 mb-2 ml-2" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&action=new_topic') . '">' . IMAGE_NEW_TOPIC . '</a>' . $add_str; ?>&nbsp;</td>
                  </tr>
                </table>
                <table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <?php
                    // RCI code start
                    echo $cre_RCI->get('articles', 'listingbottom', false);
                    // RCI code eof
                    ?>
                  </tr>
                </table>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">

            <?php
            $heading = array();
            $contents = array();
            switch ($action) {
            case 'new_topic':
                $heading[] = array('text' => TEXT_INFO_HEADING_NEW_TOPIC);
                //$contents[] = array('form' => tep_draw_form('newtopic', FILENAME_ARTICLES, 'action=insert_topic&tPath=' . $tPath, 'post', 'enctype="multipart/form-data"'));
                $contents[] = array('text' => TEXT_NEW_TOPIC_INTRO);
                $topic_inputs_string = '';
                $languages = tep_get_languages();
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                  $topic_inputs_string .= '<br>' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('topics_name[' . $languages[$i]['id'] . ']');
                }
                $contents[] = array('text' => '<br>' . TEXT_TOPICS_NAME . $topic_inputs_string);
                $contents[] = array('text' => '<br>' . TEXT_SORT_ORDER . '<br>' . tep_draw_input_field('sort_order', '', 'size="2"'));
                break;
              case 'edit_topic':
                $heading[] = array('text' => TEXT_INFO_HEADING_EDIT_TOPIC);
                //$contents[] = array('form' => tep_draw_form('topics', FILENAME_ARTICLES, 'action=update_topic&tPath=' . $tPath, 'post', 'enctype="multipart/form-data"') . tep_draw_hidden_field('topics_id', $tInfo->topics_id));
                $contents[] = array('text' => TEXT_EDIT_INTRO);
                $topic_inputs_string = '';
                $languages = tep_get_languages();
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                  $topic_inputs_string .= '<br>' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('topics_name[' . $languages[$i]['id'] . ']', tep_get_topic_name($tInfo->topics_id, $languages[$i]['id']));
                }
                $contents[] = array('text' => '<br>' . TEXT_EDIT_TOPICS_NAME . $topic_inputs_string);
                $contents[] = array('text' => '<br>' . TEXT_EDIT_SORT_ORDER . '<br>' . tep_draw_input_field('sort_order', $tInfo->sort_order, 'size="2"'));
                break;
              case 'delete_topic':
                $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_TOPIC);
                $contents[] = array('form' => tep_draw_form('topics', FILENAME_ARTICLES, 'action=delete_topic_confirm&tPath=' . $tPath) . tep_draw_hidden_field('topics_id', $tInfo->topics_id));
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_DELETE_TOPIC_INTRO.'<br><br><b>' . $tInfo->topics_name . '</b></p></div></div></div>');
                if ($tInfo->childs_count > 0) $contents[] = array('text' => '<div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">' . sprintf(TEXT_DELETE_WARNING_CHILDS, $tInfo->childs_count).'</p></div></div></div>');
                if ($tInfo->articles_count > 0) $contents[] = array('text' => '<div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">' . sprintf(TEXT_DELETE_WARNING_ARTICLES, $tInfo->articles_count).'</p></div></div></div>');
                $contents[] = array('align' => 'center', 'text' => '<br><a class="btn btn-default btn-sm mt-2 mb-2 mr-2" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $tInfo->topics_id) . '">' . IMAGE_CANCEL . '</a><button class="btn btn-danger btn-sm" type="submit">' . IMAGE_DELETE . '</button>');
                break;
              case 'move_topic':
                $heading[] = array('text' => TEXT_INFO_HEADING_MOVE_TOPIC);
                $contents[] = array('form' => tep_draw_form('topics', FILENAME_ARTICLES, 'action=move_topic_confirm&tPath=' . $tPath) . tep_draw_hidden_field('topics_id', $tInfo->topics_id));
                $contents[] = array('text' => '<div class="sidebar-text">'. sprintf(TEXT_MOVE_TOPICS_INTRO, $tInfo->topics_name).'<br>' . sprintf(TEXT_MOVE, $tInfo->topics_name) . '<br>' . tep_draw_pull_down_menu('move_to_topic_id', tep_get_topic_tree(), $current_topic_id).'</div>');
                $contents[] = array('align' => 'center', 'text' => '<br><button class="btn btn-success btn-sm mt-2 mb-2 btn-submit" type="submit">' . IMAGE_MOVE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $tInfo->topics_id) . '">' . IMAGE_CANCEL . '</a>');
                break;
              case 'delete_article':
                $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_ARTICLE);
                $contents[] = array('form' => tep_draw_form('articles', FILENAME_ARTICLES, 'action=delete_article_confirm&tPath=' . $tPath) . tep_draw_hidden_field('articles_id', $aInfo->articles_id));
                $article_topics_string = '';
                $article_topics = tep_generate_topic_path($aInfo->articles_id, 'article');
                for ($i = 0, $n = sizeof($article_topics); $i < $n; $i++) {
                  $topic_path = '';
                  for ($j = 0, $k = sizeof($article_topics[$i]); $j < $k; $j++) {
                    $topic_path .= $article_topics[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
                  }
                  $topic_path = substr($topic_path, 0, -16);
                  $article_topics_string .= tep_draw_checkbox_field('article_topics[]', $article_topics[$i][sizeof($article_topics[$i])-1]['id'], true) . '&nbsp;' . $topic_path . '<br>';
                }
                $article_topics_string = substr($article_topics_string, 0, -4);
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_DELETE_ARTICLE_INTRO .'<br><b>' . $aInfo->articles_name . '</b><br>' . $article_topics_string .'</p></div></div></div>');
                $contents[] = array('align' => 'center', 'text' => '<br><a class="btn btn-default btn-sm mt-2 mb-2 mr-2" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $aInfo->articles_id) . '">' . IMAGE_CANCEL . '</a><button class="btn btn-danger btn-sm" type="submit">' . IMAGE_DELETE . '</button>');
                break;
              case 'move_article':
                $heading[] = array('text' => TEXT_INFO_HEADING_MOVE_ARTICLE );
                $contents[] = array('form' => tep_draw_form('articles', FILENAME_ARTICLES, 'action=move_article_confirm&tPath=' . $tPath) . tep_draw_hidden_field('articles_id', $aInfo->articles_id));
                $contents[] = array('text' => '<div class="sidebar-text">'. sprintf(TEXT_MOVE_ARTICLES_INTRO, $aInfo->articles_name) .'<br>' . TEXT_INFO_CURRENT_TOPICS . '<br><b>' . tep_output_generated_topic_path($aInfo->articles_id, 'article') . '</b><br>' . sprintf(TEXT_MOVE, $aInfo->articles_name) . '<br>' . tep_draw_pull_down_menu('move_to_topic_id', tep_get_topic_tree(), $current_topic_id).'</div>');
                $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-submit" type="submit">'. IMAGE_MOVE .'</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $aInfo->articles_id) . '">' . IMAGE_CANCEL . '</a>');
                break;
              case 'copy_to':
                $heading[] = array('text' => TEXT_INFO_HEADING_COPY_TO);
                $contents[] = array('form' => tep_draw_form('copy_to', FILENAME_ARTICLES, 'action=copy_to_confirm&tPath=' . $tPath) . tep_draw_hidden_field('articles_id', $aInfo->articles_id));
                $contents[] = array('text' => '<div class="sidebar-text">'.TEXT_INFO_COPY_TO_INTRO .'<br>' . TEXT_INFO_CURRENT_TOPICS . '<br>
                <b>' . tep_output_generated_topic_path($aInfo->articles_id, 'article') . '</b>
                <br>' . TEXT_TOPICS . '<br>' . tep_draw_pull_down_menu('topics_id', tep_get_topic_tree(), $current_topic_id)
                .'<br>' . TEXT_HOW_TO_COPY . '<br>' . tep_draw_radio_field('copy_as', 'link', true) . ' ' . TEXT_COPY_AS_LINK
                . '<br>' . tep_draw_radio_field('copy_as', 'duplicate') . ' ' . TEXT_COPY_AS_DUPLICATE.'</div>');
                $contents[] = array('align' => 'center', 'text' => '<br><button class="btn btn-success btn-sm mt-2 mb-2 btn-copy" type="submit">' . IMAGE_COPY . '</button><a class="btn btn-default btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $aInfo->articles_id) . '">' . IMAGE_CANCEL . '</a>');
                break;
              default:
                if ($rows > 0) {
                  if (isset($tInfo) && is_object($tInfo)) { // topic info box contents
                    $heading[] = array('text' => $tInfo->topics_name );
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a
                    				class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $tInfo->topics_id . '&action=edit_topic') . '">' . IMAGE_EDIT . '</a><a
                    				class="btn btn-grey btn-sm mt-2 mb-2 btn-move" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $tInfo->topics_id . '&action=move_topic') . '">' . IMAGE_MOVE . '</a><a
                    				class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&tID=' . $tInfo->topics_id . '&action=delete_topic') . '">' . IMAGE_DELETE . '</a></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($tInfo->date_added) . '</span></div>');
                    if (tep_not_null($tInfo->last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-1">'. TEXT_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($tInfo->last_modified) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_SUBTOPICS . ' <b>' . $tInfo->childs_count . '</b><span class="sidebar-title ml-2">' . TEXT_ARTICLES . ' <b>' . $tInfo->articles_count . '</span></div>');
                  } elseif (isset($aInfo) && is_object($aInfo)) { // article info box contents
                    $heading[] = array('text' => tep_get_articles_name($aInfo->articles_id, $languages_id));
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a
                    					class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $aInfo->articles_id . '&action=new_article') . '">' . IMAGE_EDIT . '</a><a
                    					class="btn btn-grey btn-sm mt-2 mb-2 btn-move" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $aInfo->articles_id . '&action=move_article') . '">' . IMAGE_MOVE . '</a><a
                    					class="btn btn-grey btn-sm mt-2 mb-2 btn-copyto" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $aInfo->articles_id . '&action=copy_to') . '">' . IMAGE_COPY_TO . '</a><a
                    					class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . '&aID=' . $aInfo->articles_id . '&action=delete_article') . '">' . IMAGE_DELETE . '</a></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($aInfo->articles_date_added) .'</span></div>');
                    if (tep_not_null($aInfo->articles_last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-1">'. TEXT_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($aInfo->articles_last_modified) . '</span></div>');
                    if (date('Y-m-d') < $aInfo->articles_date_available) $contents[] = array('text' => '<div class="sidebar-text mt-1">'. TEXT_DATE_AVAILABLE . '<span class="sidebar-title ml-2">' . tep_date_short($aInfo->articles_date_available) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_ARTICLES_AVERAGE_RATING . '<span class="sidebar-title ml-2">' . number_format($aInfo->average_rating, 2) . '%</span></div>');
                    if($aInfo->articles_image != ''){
                    	$contents[] = array('text' => '<div class="sidebar-img mt-3 ml-4 mr-4 mb-1">'.tep_image(HTTP_SERVER.DIR_WS_HTTP_CATALOG.DIR_WS_ARTICLES . $aInfo->articles_image, $articles_image, '100', '100', 'class="media-object mt-2"').'</div>');
                    }
                  }
                } else { // create topic/article info
                  $heading[] = array('text' => EMPTY_TOPIC);
                  $contents[] = array('text' => '<div class="sidebar-text">'.TEXT_NO_CHILD_TOPICS_OR_ARTICLES.'</div>');
                }
                break;
    }

	  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
		$box = new box;
		echo $box->showSidebar($heading, $contents);
	  }
    }
    // RCI code start
    echo $cre_RCI->get('articles', 'bottom');
    echo $cre_RCI->get('global', 'bottom');
    // RCI code eof
    ?>

          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script script type="text/javascript">

 	$('#calender').datepicker({
	    todayHighlight: true
});

function updateTopic(mode) {
  var action = '<?php echo str_replace('&amp;', '&', tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . (isset($_GET['tID']) ? '&tID=' . $_GET['tID'] : '') . '&action=' . (isset($_GET['tID']) ? 'update_topic' : 'insert_topic'))); ?>';
  // set the save mode in hidden form input
  $('<input />').attr('type', 'hidden')
      .attr('name', "mode")
      .attr('value', mode)
      .appendTo('#new_topic');

<?php
echo $lcadmin->print_action_js('addupdatetopic');
?>

  $('#new_topic').attr('action', action).submit();
}

function updateArticle(mode) {
  var action = '<?php echo str_replace('&amp;', '&', tep_href_link(FILENAME_ARTICLES, 'tPath=' . $tPath . (isset($_GET['aID']) ? '&aID=' . $_GET['aID'] : '') . '&action=' . (isset($_GET['aID']) ? 'update_article' : 'insert_article'))); ?>';
  // set the save mode in hidden form input
  $('<input />').attr('type', 'hidden')
      .attr('name', "mode")
      .attr('value', mode)
      .appendTo('#new_article');

<?php
echo $lcadmin->print_action_js('addupdatearticle');
?>

  $('#new_article').attr('action', action).submit();
}

//for articles categories i.e TOPICS
function getslug(LID,ID, TOPIC_NAME){
  if(TOPIC_NAME != ''){
	 //var permalink_name = $('#permalink_'+LID).val();
	  params = 'action=get_slug&type=topics&id='+ID+'&lid=' + LID + '&name=' + TOPIC_NAME;
		$.ajax({
			type: 'post',
			url: '<?php echo tep_href_link("ajax_common.php");?>',
			data: params,
			success: function (retval) {
				//alert('#permalink_name['+LID+']');
				if(retval == 'error'){
				}else{
				  $('#permalink_'+LID).val(retval);
			   }
			}
		});
   }
}
function checkslugs(LID, WID,ID){
	  var permalink_name = $('#permalink_'+LID).val();
		  params = 'action=check_slug&type=topics&id='+ID+'&lid=' + LID + '&wid=' + WID + '&name=' + permalink_name;
			$.ajax({
				type: 'post',
				url: '<?php echo tep_href_link("ajax_common.php");?>',
				data: params,
				success: function (retval) {
					if(retval == 'error'){
						$('#permalink_error_'+LID).html('Duplicate permalink please add a unique permalink!');
						$('#permalink_'+LID).css({border:'1px solid #FF0000', color:'#FF0000'});
						//$('#permalink_'+LID).focus();
					}else{
					  $('#permalink_'+LID).val(retval);
					  $('#permalink_'+LID).css({border:'1px solid #277421', color:'#999999'});
					  $('#permalink_error_'+LID).css('display','none');
				   }
				}
			});
}

//for articles
function arti_getslug(LID, ID, ARTICLE_NAME){
  if(ARTICLE_NAME != ''){
	  params = 'action=get_slug&type=articles&id='+ID+'&lid=' + LID + '&name=' + ARTICLE_NAME;
		$.ajax({
			type: 'post',
			url: '<?php echo tep_href_link("ajax_common.php");?>',
			data: params,
			success: function (retval) {
				//alert('#permalink_name['+LID+']');
				if(retval == 'error'){
				}else{
				  $('#permalink_'+LID).val(retval);
			   }
			}
		});
    }
}
function arti_checkslug(LID,ID){
	     var permalink_name = $('#permalink_'+LID).val();
		  params = 'action=check_slug&type=articles&id='+ID+'&lid=' + LID + '&name=' + permalink_name;
			$.ajax({
				type: 'post',
				url: '<?php echo tep_href_link("ajax_common.php");?>',
				data: params,
				success: function (retval) {
					if(retval == 'error'){
						alert("Duplicate Permalink Entry");
						$('#permalink_error_'+LID).html('Duplicate permalink please add a unique permalink!');
						$('#permalink_'+LID).css({border:'1px solid #FF0000', color:'#FF0000'});
						//$('#permalink_'+LID).focus();
					}else{
					  $('#permalink_'+LID).val(retval);
					  $('#permalink_'+LID).css({border:'1px solid #277421', color:'#999999'});
					  $('#permalink_error_'+LID).css('display','none');
				   }
				}
			});
}
function enableEditPermalink(LID){
   $('#permalink_'+LID).prop('readonly', false);
   $('.editbtn_'+LID).css('display', 'none');
   $('.savebtn_'+LID).css('display', 'inline-block');
}
function saveEditPermalink(LID,  ID, TP){
	var permalink_name = $('#permalink_'+LID).val();
	if(permalink_name == ''){
	   alert('Please enter permalink');
	   return false;
	}
	$('#ploader').css('display', 'inline-block');
	 params = 'action=update_permalink&type='+TP+'&id='+ID+'&lid=' + LID + '&name=' + permalink_name;
		$.ajax({
			type: 'post',
			url: '<?php echo tep_href_link("ajax_common.php");?>',
			data: params,
			success: function (retval) {
			//alert(retval);
			 $('#ploader').css('display', 'none');
			if(retval == 'MSG-DUPLICATE'){
				alert('Duplicate permalink please add a unique permalink!');
				$('#permalink_'+LID).css({border:'1px solid #FF0000', color:'#FF0000'});
			}else{
				alert('MSG-SUCCESS');
			    $('#permalink_'+LID).css({border:'1px solid #9fa2a5', color:'#9fa2a5'});
			    $('#permalink_'+LID).val(retval);
			    $('#permalink_'+LID).prop('readonly', true);
			    $('.editbtn_'+LID).css('display', 'inline-block');
			    $('.savebtn_'+LID).css('display', 'none');
			    $('#tickmark').css('display', 'inline-block');
			}
			}
		});
}
function changeLang(pagetype, langName, langId) {
    $('#langDropdownTitle').html(langName);
    $(".langval").removeClass("active");
    $("#langsel_"+langId).addClass("active");
	$( "."+ pagetype +"-lang-pane" ).hide( "slow" );
	$( "#"+ pagetype +"-default-pane-"+langId ).show( "slow" );
}

<?php
echo $lcadmin->print_js();
?>

</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
