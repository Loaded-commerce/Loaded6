<?php

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CREATE_ACCOUNT_SUCCESS);

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-createaccountsuccess" class="table-createaccountsuccess">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">

<!-- body_text //-->
        <div class="row">
          <div class="col-md-1 col-xl-1"></div>
          <div class="col-md-10 col-xl-10">
			<table class="table w-100 mt-2">
            <?php
            // RCO start
            if ($cre_RCO->get('createaccountsuccess', 'buttoncreateorder') !== true) {
              ?>
              <tr>
                <td class="table-col text-right" align="right"><?php echo BUTTON_TITLE1;?></td>
                <td class="table-col text-left"> 
                  <?php echo '<button type="button" class="btn btn-success btn-sm mb-2" onclick="window.location=\'' . tep_href_link(FILENAME_CREATE_ORDER, '', 'SSL') . '\'">' . IMAGE_BUTTON_CREATE_ORDER . '</button>'; ?>
                </td>
              </tr>
              <?php
            }
            // RCO eof
            ?>
            <tr>
              <td class="table-col text-right" align="right"><?php echo BUTTON_TITLE2; ?></td>
              <td class="table-col text-left">
                <?php echo '<button type="button" class="btn btn-success btn-sm mb-2" onclick="window.location=\'' . tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL') . '\'">' . IMAGE_BUTTON_CREATE_CUSTOMER . '</button>'; ?>
              </td>
            </tr>
            <tr>
              <td class="table-col text-right" align="right"><?php echo BUTTON_TITLE3; ?></td>
              <td class="table-col text-left">
                <?php echo '<button type="button" class="btn btn-success btn-sm mb-2" onclick="window.location=\'' . tep_href_link(FILENAME_DEFAULT, '', 'SSL') . '\'">' . IMAGE_BUTTON_ADMIN_HOME . '</button>'; ?>
               </td>
            </tr>
          </table>
          </div>
          <div class="col-md-1 col-xl-1"></div>
         </div>

<!-- body_text_eof //-->

          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
