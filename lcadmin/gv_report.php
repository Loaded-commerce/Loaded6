<?php
/*
  $Id: FILENAME_GV_REPORT,v 1.1.1.1 2004/03/04 23:38:36 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 - 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
  
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('gvreport') > 0) {
      echo $messageStack->output('gvreport'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-gvreport" class="table-gvreport">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CUSTOMERS; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_ORDERS_ID; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_VOUCHER_VALUE; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_DATE_PURCHASED; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
              </thead>
              <tbody>
<?php
 $gv_report_raw="select gv.unique_id, c.customers_id  from " . TABLE_CUSTOMERS . " c, " . TABLE_COUPON_GV_QUEUE . " gv where gv.customer_id = c.customers_id )";


  $gv_query_raw = "select c.customers_firstname, c.customers_lastname, gv.unique_id, gv.date_created, gv.amount, gv.order_id, gv.release_flag from " . TABLE_CUSTOMERS . " c, " . TABLE_COUPON_GV_QUEUE . " gv where (gv.customer_id = c.customers_id )";
  $gv_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $gv_query_raw, $gv_query_numrows);
  $gv_query = tep_db_query($gv_query_raw);
  while ($gv_list = tep_db_fetch_array($gv_query)) {
    if (((!$_GET['gid']) || (@$_GET['gid'] == $gv_list['unique_id'])) && (!$gInfo)) {
      $gInfo = new objectInfo($gv_list);
    }
    if ( (isset($gInfo)) && (is_object($gInfo)) && ($gv_list['unique_id'] == $gInfo->unique_id) ) {
      echo '              <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . tep_href_link(FILENAME_GV_REPORT, tep_get_all_get_params(array('gid', 'action')) . 'gid=' . $gInfo->unique_id . '&action=view') . '\'">' . "\n";
    } else {
      echo '              <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . tep_href_link(FILENAME_GV_REPORT, tep_get_all_get_params(array('gid', 'action')) . 'gid=' . $gv_list['unique_id']) . '\'">' . "\n";
    }
?>
                <td class="dataTableContent"><?php echo $gv_list['customers_firstname'] . ' ' . $gv_list['customers_lastname']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $gv_list['order_id']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $currencies->format($gv_list['amount']); ?></td>
                <td class="dataTableContent" align="right"><?php echo tep_datetime_short($gv_list['date_created']); ?></td>
                <td class="dataTableContent" align="right"><?php if ( (is_object($gInfo)) && ($gv_list['unique_id'] == $gInfo->unique_id) ) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_GV_REPORT, 'page=' . $_GET['page'] . '&gid=' . $gv_list['unique_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>
		   	 </tbody>
			</table>
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
			  <tr>
				<td class="pb-2" valign="top"><?php echo $gv_split->display_count($gv_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_GIFT_VOUCHERS); ?></td>
				<td class="pb-2" align="right"><?php echo $gv_split->display_links($gv_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
			  </tr>
			</table>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">                
<?php
if (!isset($gInfo)) {
  $gInfo = new objectInfo(array());
  $gInfo->customers_firstname = '';
  $gInfo->customers_lastname = '';
  $gInfo->unique_id = '';
  $gInfo->date_created = '';
  $gInfo->amount = '';
  $gInfo->order_id = '';
  $gInfo->release_flag = '';
}

if ($gInfo->release_flag == 'Y'){
   $release = 'Yes' ;
   }
if ($gInfo->release_flag == 'N'){
   $release = 'No' ;
   }
   

  $heading = array();
  $contents = array();
  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  switch ($action) {

    default:
	  if (isset($gInfo) && is_object($gInfo)) {
		  $heading[] = array('text' => $gInfo->unique_id . ' ' . $gInfo->customers_firstname . ' ' . $gInfo->customers_lastname );
		  if ($gInfo->release_flag == 'N'){
			 $contents[] = array('align' => 'center','text' => '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_GV_QUEUE,'&gid=' . $gInfo->unique_id,'NONSSL'). '">' . tep_image_button('button_release.gif', IMAGE_RELEASE) . '</a>');
		  }
		  $contents[] = array('text' => '<div class="sidebar-text">[' . $gInfo->unique_id . '] ' . tep_datetime_short($gInfo->date_created) . ' ' . $currencies->format($gInfo->amount) .'</div>');
		  $contents[] = array('align' => 'center','text' => '<div class="sidebar-text">'. TEXT_GV_REPORT_RELEASED . '<span class="sidebar-title ml-2">' .(isset($release) ? $release : '') .'</span></div>');
      }
      break;
   }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
	$box = new box;
	echo $box->showSidebar($heading, $contents);
  }
?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
