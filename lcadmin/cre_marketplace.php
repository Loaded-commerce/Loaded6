<?php
/*
  $Id: cre_marketplace.php,v 1.0 2007/09/20 23:38:56 datazen Exp $

  CRE Loaded, Commercial Open Source E-Commerce
  http://www.creloaded.com

  Copyright (c) 2007 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CRE_MARKETPLACE);

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-cremarketplace" class="table-cremarketplace">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">

<?php 
function cre_iframe($n,$zone) {
if($n!='' && $zone !='') {
$iframe_content .= <<<EOF
<script language='JavaScript' type='text/javascript' src='https://adserver.authsecure.com/adx.js'></script>
<script language='JavaScript' type='text/javascript'>
<!--
   if (!document.phpAds_used) document.phpAds_used = ',';
   phpAds_random = new String (Math.random()); phpAds_random = phpAds_random.substring(2,11);
   
   document.write ("<" + "script language='JavaScript' type='text/javascript' src='");
   document.write ("https://adserver.authsecure.com/adjs.php?n=" + phpAds_random);
   document.write ("&amp;what=zone:$zone");
   document.write ("&amp;exclude=" + document.phpAds_used);
   if (document.referrer)
      document.write ("&amp;referer=" + escape(document.referrer));
   document.write ("'><" + "/script>");
//-->
</script><noscript><a href='https://adserver.authsecure.com/adclick.php?n=$n' target='_blank'><img src='https://adserver.authsecure.com/adview.php?what=zone:$zone&amp;n=$n' border='0' alt=''></a></noscript>
EOF;
} else { 
$iframe_content = '';
}
return $iframe_content;
}
?>
<!-- header_eof //-->
<!-- body //-->
			<table class="table w-100 mt-2 text-right">
              <tr>
                <td class="table-col dark text-center"><!--Banner one Start--><?php echo cre_iframe('a8c53aea','52');?><!--Banner one End--></td>
              </tr>
              <tr>
                <td class="table-col dark text-center"><!--Banner two Start--><?php echo cre_iframe('ab67cba4','53');?><!--Banner two End--></td>
              </tr>
              <tr>
                <td class="table-col dark text-center"><!--Banner three Start--><?php echo cre_iframe('a601751d','54');?><!--Banner three End--></td>
              </tr>
              <tr>
                <td class="table-col dark text-center"><!--Banner four Start--><?php echo cre_iframe('a601751d','55');?><!--Banner four End--></td>
              </tr>
              <tr>
                <td class="table-col dark text-center"><!--Banner five Start--><?php echo cre_iframe('a4186fe8','56');?><!--Banner five End--></td>
              </tr>
              <tr>
                <td class="table-col dark text-center"><!--Banner six Start--><?php echo cre_iframe('a777c26a','57');?><!--Banner six End--></td>
              </tr>
              <tr>
                <td class="table-col dark text-center"><!--Banner sevem Start--><?php echo cre_iframe('ad185eea','58');?><!--Banner seven End--></td>
              </tr>
              <tr>
                <td class="table-col dark text-center"><!--Banner eight Start--><?php echo cre_iframe('a3d53437','59');?><!--Banner eight End--></td>
              </tr>              
              <tr>
                <td class="table-col dark text-center"><!--Banner nine Start--><?php echo cre_iframe('aa0357d5','60');?><!--Banner nine End--></td>
              </tr>
              <tr>
                <td class="table-col dark text-center"><!--Banner ten Start--><?php echo cre_iframe('a18b055c','61');?><!--Banner ten End--></td>
              </tr>
            </table>
            
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>            
