<?php
/*
  $Id: create_account.php,v 2.0 2008/05/05 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');

$action = isset($_GET['action'])?$_GET['action']:'';
if($action != '')
{
	switch($action) {
		case'process':
			if (count($_POST) <= 2) {
			   tep_redirect(tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'));
			 }
			include('create_account_process.php');
			break;
		case'success':
			include('create_account_success.php');
			exit();
			break;
	}
}
include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-createaccount" class="table-createaccount">
<form name="account_edit" method="post" <?php echo 'action="' . tep_href_link(FILENAME_CREATE_ACCOUNT, 'action=process', 'SSL') . '"'; ?> onSubmit="return check_form();">
<input type="hidden" name="action" value="process">
        <div class="row">
          <div class="col-md-8 col-xl-9 dark panel-left rounded-left pb-3">

		<?php
		if (isset($navigation->snapshot) && is_array($navigation->snapshot) && sizeof($navigation->snapshot) > 0) {
		?>
			<div><?php echo sprintf(TEXT_ORIGIN_LOGIN, tep_href_link(FILENAME_LOGIN, tep_get_all_get_params(), 'SSL')); ?></div>
		<?php
		}
		//$email_address = tep_db_prepare_input($_GET['email_address']);
		$account['entry_country_id'] = STORE_COUNTRY;
		$account['entry_zone_id'] = STORE_ZONE;
		require(DIR_WS_MODULES . 'account_details.php');
		?>
         <div class="row"><div class="col-md-12 col-xl-12 mb-5 text-right"><button class="btn btn-primary btn-sm mt-2" type="submit"><?php echo IMAGE_BUTTON_CONTINUE; ?></button></div></div>
    <!-- body_text_eof //-->


          </div>
          <div class="col-md-4 col-xl-3 dark panel-right rounded-right">

                    <div class="sidebar-heading mt-3">
                      <span><?php echo CATEGORY_OPTIONS; ?></span>
                    </div><div class="sidebar-heading-footer w-100"></div>
                  <div class="sidebar-content-container">
                    <div class="form-group row">
                      <label class="col-sm-5 control-label sidebar-edit mt-2 pr-0"><?php echo ENTRY_NEWSLETTER; ?><?php if(trim(ENTRY_NEWSLETTER_TEXT) != '') { echo '&nbsp;<span class="required"></span>'; } ?></label>
                      <div class="col-sm-7">
                        <div class="input-group">
						  <?php
						  if ($is_read_only) {
							if ($account['customers_newsletter'] == '1') {
							  echo ENTRY_NEWSLETTER_YES;
							} else {
							  echo ENTRY_NEWSLETTER_NO;
							}
						  } elseif ($processed) {
							if ($newsletter == '1') {
							  echo ENTRY_NEWSLETTER_YES;
							} else {
							  echo ENTRY_NEWSLETTER_NO;
							}
							echo tep_draw_hidden_field('newsletter');
						  } else {
							echo tep_draw_pull_down_menu('newsletter', $newsletter_array, (isset($account['customers_newsletter']) ? $account['customers_newsletter'] : ''));
						  }
						  ?>

                        </div>
                      </div>
                    </div>

          </div>
</form>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
	<script>
	function change_state(country_id) {
		$.get('ajax_common.php?action=getstate&page=createacc&country_id='+country_id, function(data){
			$('#state_id').html(data);
			$('#state_id select').select2();
		});
	}
	$(document).ready(function() {
		$('#countrydropdown').select2();
		$('#state_id select').select2();
		width: 'resolve'
	});
	$("#onprocesscountry").css("pointer-events","none");
	</script>
</div>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
