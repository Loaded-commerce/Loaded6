<?php
/*
  $Id: link_categories.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

// define our link functions
require(DIR_WS_FUNCTIONS . 'links.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

$error = false;
$processed = false;

if (tep_not_null($action)) {
  switch ($action) {
    case 'setflag':
      $status = tep_db_prepare_input($_GET['flag']);

      if ($status == '1') {
        tep_db_query("update " . TABLE_LINK_CATEGORIES . " set link_categories_status = '1' where link_categories_id = '" . (int)$_GET['cID'] . "'");
      } elseif ($status == '0') {
        tep_db_query("update " . TABLE_LINK_CATEGORIES . " set link_categories_status = '0' where link_categories_id = '" . (int)$_GET['cID'] . "'");
      }
      echo $_GET['flag'];exit;
      tep_redirect(tep_href_link(FILENAME_LINK_CATEGORIES, '&cID=' . $_GET['cID']));
      break;
    case 'insert':
    case 'update':
      if (isset($_POST['link_categories_id'])) $link_categories_id = tep_db_prepare_input($_POST['link_categories_id']);
      $link_categories_sort_order = tep_db_prepare_input($_POST['link_categories_sort_order']);
      $link_categories_status = ((tep_db_prepare_input($_POST['link_categories_status']) == 'on') ? '1' : '0');

      $sql_data_array = array('link_categories_sort_order' => $link_categories_sort_order,
                              'link_categories_status' => $link_categories_status);

      if ($action == 'insert') {
        $insert_sql_data = array('link_categories_date_added' => 'now()');

        $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

        tep_db_perform(TABLE_LINK_CATEGORIES, $sql_data_array);

        $link_categories_id = tep_db_insert_id();
      } elseif ($action == 'update') {
        $update_sql_data = array('link_categories_last_modified' => 'now()');

        $sql_data_array = array_merge($sql_data_array, $update_sql_data);

        tep_db_perform(TABLE_LINK_CATEGORIES, $sql_data_array, 'update', "link_categories_id = '" . (int)$link_categories_id . "'");
      }

      $languages = tep_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $link_categories_name_array = $_POST['link_categories_name'];
        $link_categories_description_array = $_POST['link_categories_description'];

        $language_id = $languages[$i]['id'];

        $sql_data_array = array('link_categories_name' => tep_db_prepare_input($link_categories_name_array[$language_id]),
                                'link_categories_description' => tep_db_prepare_input($link_categories_description_array[$language_id]));

        if ($action == 'insert') {
          $insert_sql_data = array('link_categories_id' => $link_categories_id,
                                   'language_id' => $languages[$i]['id']);

          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

          tep_db_perform(TABLE_LINK_CATEGORIES_DESCRIPTION, $sql_data_array);
        } elseif ($action == 'update') {
          tep_db_perform(TABLE_LINK_CATEGORIES_DESCRIPTION, $sql_data_array, 'update', "link_categories_id = '" . (int)$link_categories_id . "' and language_id = '" . (int)$languages[$i]['id'] . "'");
        }
      }

      if ($link_categories_image = new upload('link_categories_image', DIR_FS_CATALOG_IMAGES)) {
        tep_db_query("update " . TABLE_LINK_CATEGORIES . " set link_categories_image = '" . tep_db_input($link_categories_image->filename) . "' where link_categories_id = '" . (int)$link_categories_id . "'");
      }
      //sync link categories
	  sync_link_categories();
      tep_redirect(tep_href_link(FILENAME_LINK_CATEGORIES, '&cID=' . $link_categories_id));
      break;
    case 'delete_confirm':
      if (isset($_POST['link_categories_id'])) {
        $link_categories_id = tep_db_prepare_input($_POST['link_categories_id']);

        $link_ids_query = tep_db_query("select links_id from " . TABLE_LINKS_TO_LINK_CATEGORIES . " where link_categories_id = '" . (int)$link_categories_id . "'");

        while ($link_ids = tep_db_fetch_array($link_ids_query)) {
          tep_remove_link($link_ids['links_id']);
        }

        tep_remove_link_category($link_categories_id);
      }

      tep_redirect(tep_href_link(FILENAME_LINK_CATEGORIES));
      break;
    default:
      $link_categories_query = tep_db_query("select lc.link_categories_id, lc.link_categories_image, lc.link_categories_status, lc.link_categories_sort_order, lc.link_categories_date_added, lc.link_categories_last_modified, lcd.link_categories_name, lcd.link_categories_description from " . TABLE_LINK_CATEGORIES . " lc left join " . TABLE_LINK_CATEGORIES_DESCRIPTION . " lcd on lc.link_categories_id = lcd.link_categories_id where lcd.link_categories_id = lc.link_categories_id and lc.link_categories_id = '" . (isset($_GET['cID']) ? (int)$_GET['cID'] : 0) . "' and lcd.language_id = '" . (int)$languages_id . "'");
      $link_categories = tep_db_fetch_array($link_categories_query);

      $links_count_query = tep_db_query("select count(*) as link_categories_count from " . TABLE_LINKS_TO_LINK_CATEGORIES . " where link_categories_id = '" . (isset($_GET['cID']) ? (int)$_GET['cID'] : 0) . "'");
      $links_count = tep_db_fetch_array($links_count_query);

      //$cInfo_array = array_merge($link_categories, $links_count);
      if(!is_array($link_categories)) {
        $link_categories = array();
      }
      if(!is_array($links_count)) {
        $links_count = array();
      }
      $cInfo_array = array_merge($link_categories, $links_count);
      if(!is_array($cInfo_array)) {
        $cInfo_array = array();
      }
      $cInfo = new objectInfo($cInfo_array);
  }
}
include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
	<div class="col main-col">
	  <div class="row">
		<div class="col-9 pr-0 mb-2"></div>
		<div class="col-3 pr-1">
			<?php
			echo tep_draw_form('search', FILENAME_LINK_CATEGORIES, '', 'get');
			if (isset($_GET[tep_session_name()])) {
			  echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
			}
			?>
		     <div class="form-group row mb-2 pr-0">
			  <label for="cPath" class="hidden-xs col-sm-3 col-form-label text-center m-t-10 pr-0"><?php echo HEADING_TITLE_SEARCH; ?></label>
			  <div class="col-sm-9 p-0 dark rounded">
			   <?php echo tep_draw_input_field('search', (isset($_GET['search'])?$_GET['search']:''), 'size="12" class="form-control"'); ?>
			  </div>
			 </div>
			</form>
		</div>
	  </div>
	</div>

  <div class="col">
    <?php
    if ($messageStack->size('link_categories') > 0) {
      echo $messageStack->output('link_categories');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-linkcategories" class="table-linkcategories">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_NAME; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_STATUS; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
              </thead>
              <tbody>
                <?php
                $search = '';
                if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
                  $keywords = tep_db_input(tep_db_prepare_input($_GET['search']));
                  $search = " and lcd.link_categories_name like '%" . $keywords . "%'";
                  $link_categories_query_raw = "select lc.link_categories_id, lc.link_categories_image, lc.link_categories_status, lc.link_categories_sort_order, lc.link_categories_date_added, lc.link_categories_last_modified, lcd.link_categories_name, lcd.link_categories_description from " . TABLE_LINK_CATEGORIES . " lc left join " . TABLE_LINK_CATEGORIES_DESCRIPTION . " lcd on lc.link_categories_id = lcd.link_categories_id where lcd.language_id = '" . (int)$languages_id . "'" . $search . " order by lc.link_categories_sort_order, lcd.link_categories_name";
                } else {
                  $link_categories_query_raw = "select lc.link_categories_id, lc.link_categories_image, lc.link_categories_status, lc.link_categories_sort_order, lc.link_categories_date_added, lc.link_categories_last_modified, lcd.link_categories_name, lcd.link_categories_description from " . TABLE_LINK_CATEGORIES . " lc left join " . TABLE_LINK_CATEGORIES_DESCRIPTION . " lcd on lc.link_categories_id = lcd.link_categories_id  where lcd.language_id = '" . (int)$languages_id . "' order by lc.link_categories_sort_order, lcd.link_categories_name";
                }

                $link_categories_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $link_categories_query_raw, $link_categories_query_numrows);
                $link_categories_query = tep_db_query($link_categories_query_raw);
                while ($link_categories = tep_db_fetch_array($link_categories_query)) {
                  if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $link_categories['link_categories_id']))) && !isset($cInfo)) {
                    $links_count_query = tep_db_query("select count(*) as link_categories_count from " . TABLE_LINKS_TO_LINK_CATEGORIES . " where link_categories_id = '" . (int)$link_categories['link_categories_id'] . "'");
                    $links_count = tep_db_fetch_array($links_count_query);

                    $cInfo_array = array_merge($link_categories, $links_count);
                    $cInfo = new objectInfo($cInfo_array);
                  }
            		  $selected = (isset($cInfo) && isset($cInfo->link_categories_id) && is_object($cInfo) && ($link_categories['link_categories_id'] == $cInfo->link_categories_id))? true : false;
                  if ($selected) {
                    echo '<tr class="table-row dark selected" id="crow_'.$link_categories['link_categories_id'].'">' . "\n";
                  $onclick ='onclick="document.location.href=\'' . tep_href_link(FILENAME_LINK_CATEGORIES, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->link_categories_id . '&action=edit') . '\'"';
                  } else {
                    echo '<tr class="table-row dark" id="crow_'.$link_categories['link_categories_id'].'">' . "\n";
                  $onclick ='onclick="document.location.href=\'' . tep_href_link(FILENAME_LINK_CATEGORIES, tep_get_all_get_params(array('cID')) . 'cID=' . $link_categories['link_categories_id']) . '\'"';
                  }
                  $col_selected = ($selected) ? ' selected' : '';
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick; ?>><?php echo $link_categories['link_categories_name']; ?></td>
                  <td class="setflag table-col dark text-right<?php echo $col_selected; ?>">
                    <?php

                 /*   if ($link_categories['link_categories_status'] == '1') {
                      echo '<i class="fa fa-lg fa-check-circle text-success mr-2"></i><a href="' . tep_href_link(FILENAME_LINK_CATEGORIES, 'action=setflag&flag=0&cID=' . $link_categories['link_categories_id'], 'NONSSL') . '"><i class="fa fa-lg fa-times-circle text-secondary"></i></a>';
                    } else {
                      echo '<a href="' . tep_href_link(FILENAME_LINK_CATEGORIES, 'action=setflag&flag=1&cID=' . $link_categories['link_categories_id'], 'NONSSL') . '"><i class="fa fa-lg fa-check-circle text-secondary"></i></a>&nbsp;&nbsp;<i class="fa fa-lg fa-times-circle text-danger"></i>';
                    }
               */

					  $ajax_link = tep_href_link(FILENAME_LINK_CATEGORIES);
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=0&cID='.$link_categories['link_categories_id'].'\', '.$link_categories['link_categories_id'].',0 )" '.(($link_categories['link_categories_status'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
					  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=1&cID='.$link_categories['link_categories_id'].'\', '.$link_categories['link_categories_id'].',1  )" '.(($link_categories['link_categories_status'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';




                    ?>
                  </td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank"><?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $currency['currencies_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
			      </table>

            <div class="pagination-container ml-2 mr-2">
              <div class="results-right"><?php echo $link_categories_split->display_count($link_categories_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_LINK_CATEGORIES); ?></div>
              <div class="results-left"><?php echo  $link_categories_split->display_links($link_categories_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'info', 'x', 'y', 'cID'))); ?></div>
            </div>

            <div class="float-right mr-2 mt-3 mb-3" role="group">
              <?php
              if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
                echo '<a class="btn btn-success btn-sm mr-1" href="' . tep_href_link(FILENAME_LINK_CATEGORIES) . '">' . IMAGE_RESET . '</a><a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_LINK_CATEGORIES, 'page=' . $_GET['page'] . '&action=new') . '">' . IMAGE_NEW_CATEGORY . '</a>';
              }
              ?>
              <button class="btn btn-success btn-sm" onclick="window.location='<?php echo tep_href_link(FILENAME_LINK_CATEGORIES, 'page=' . $_GET['page'] . '&action=new') ; ?>'"><?php echo IMAGE_NEW_CATEGORY; ?></button>
            </div>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
            $heading = array();
            $contents = array();

            switch ($action) {
              case 'new':
                $heading[] = array('text' => TEXT_INFO_HEADING_NEW_LINK_CATEGORY );

                $contents[] = array('form' => tep_draw_form('new_link_categories', FILENAME_LINK_CATEGORIES, 'action=insert', 'post', 'enctype="multipart/form-data"'));
                $contents[] = array('text' => '<div class="sidebar-title">'.TEXT_NEW_LINK_CATEGORIES_INTRO.'</div>');

                $link_category_inputs_string = '';
                $languages = tep_get_languages();
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                  $link_category_inputs_string .=  tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('link_categories_name[' . $languages[$i]['id'] . ']');
                }

                $link_category_description_inputs_string = '';
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                  $link_category_description_inputs_string .=  tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;<br>' . tep_draw_textarea_field('link_categories_description[' . $languages[$i]['id'] . ']', 'soft', '40', '5','', 'class="form-control"');
                }

                $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_LINK_CATEGORIES_NAME .'<span class="required"></span></div><div>'. $link_category_inputs_string .'</div>');
                $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_LINK_CATEGORIES_DESCRIPTION .'<span class="required"></span></div><div>'. $link_category_description_inputs_string .'</div>');
                $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_LINK_CATEGORIES_IMAGE . '<span class="required"></span></div><div>' . tep_draw_file_field('link_categories_image', '', '', 'class="filestyle"').'</div>');
                $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_LINK_CATEGORIES_SORT_ORDER . '<span class="required"></span></div><div>' . tep_draw_input_field('link_categories_sort_order', '', 'size="2"').'</div>');


                //$contents[] = array('text' => '<br>' . TEXT_LINK_CATEGORIES_STATUS . '&nbsp;&nbsp;' . tep_draw_radio_field('link_categories_status', 'on', true) . ' ' . TEXT_LINK_CATEGORIES_STATUS_ENABLE . '&nbsp;&nbsp;' . tep_draw_radio_field('link_categories_status', 'off') . ' ' . TEXT_LINK_CATEGORIES_STATUS_DISABLE);

               $contents[] = array('text' => '<div class="form-label mt-3">'.TEXT_LINK_CATEGORIES_STATUS.'<span class="required"></span></div><div>'.tep_draw_radio_field('link_categories_status', 'on', true).' '.TEXT_LINK_CATEGORIES_STATUS_ENABLE.' '.tep_draw_radio_field('link_categories_status', 'off').' '.TEXT_LINK_CATEGORIES_STATUS_DISABLE.'</div>');



                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_LINK_CATEGORIES) . '">' . IMAGE_CANCEL . '</a></div>');
                break;
              case 'edit':
                $heading[] = array('text' => TEXT_INFO_HEADING_EDIT_LINK_CATEGORY );

                $contents[] = array('form' => tep_draw_form('edit_link_categories', FILENAME_LINK_CATEGORIES, 'action=update', 'post', 'enctype="multipart/form-data"') . tep_draw_hidden_field('link_categories_id', $cInfo->link_categories_id));
                $contents[] = array('text' => '<div class="sidebar-title">'.TEXT_EDIT_LINK_CATEGORIES_INTRO.'</div>');

                $link_category_inputs_string = '';
                $languages = tep_get_languages();
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                  $link_category_inputs_string .=  tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('link_categories_name[' . $languages[$i]['id'] . ']', tep_get_link_category_name($cInfo->link_categories_id, $languages[$i]['id']));
                }

                $link_category_description_inputs_string = '';
                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                  $link_category_description_inputs_string .=  tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;<br>' . tep_draw_textarea_field('link_categories_description[' . $languages[$i]['id'] . ']', 'soft', '40', '5', tep_get_link_category_description($cInfo->link_categories_id, $languages[$i]['id']), 'class="form-control"');
                }

                $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_LINK_CATEGORIES_NAME .'<span class="required"></span></div><div>'. $link_category_inputs_string.'</div>');
                $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_LINK_CATEGORIES_DESCRIPTION .'<span class="required"></span></div><div>'. $link_category_description_inputs_string.'</div>');
                $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_LINK_CATEGORIES_IMAGE . '<span class="required"></span></div><div>' . tep_draw_file_field('link_categories_image', '', '', 'class="filestyle"').'</div>');
                $contents[] = array('text' => '<div class="form-label mt-3">' . tep_info_image($cInfo->link_categories_image, $cInfo->link_categories_name) . '</div><div>' . $cInfo->link_categories_image.'</div>');
                $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_LINK_CATEGORIES_SORT_ORDER . '<span class="required"></span></div><div>' . tep_draw_input_field('link_categories_sort_order', $cInfo->link_categories_sort_order, 'size="2"').'</div>');

                //$contents[] = array('text' => '<br>' . TEXT_LINK_CATEGORIES_STATUS . '&nbsp;&nbsp;' . tep_draw_radio_field('link_categories_status', 'on', ($cInfo->link_categories_status == '1') ? true : false) . ' ' . TEXT_LINK_CATEGORIES_STATUS_ENABLE . '&nbsp;&nbsp;' . tep_draw_radio_field('link_categories_status', 'off', ($cInfo->link_categories_status == '0') ? true : false) . ' ' . TEXT_LINK_CATEGORIES_STATUS_DISABLE);

                $contents[] = array('text' => '<div class="form-label mt-3">'.TEXT_LINK_CATEGORIES_STATUS.'<span class="required"></span></div><div>'.tep_draw_radio_field('link_categories_status', 'on', ($cInfo->link_categories_status == '1') ? true : false).' '.TEXT_LINK_CATEGORIES_STATUS_ENABLE.' '.tep_draw_radio_field('link_categories_status', 'off', ($cInfo->link_categories_status == '0') ? true : false).' '.TEXT_LINK_CATEGORIES_STATUS_DISABLE.'</div>');

                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_LINK_CATEGORIES, 'cID=' . $cInfo->link_categories_id) . '">' . IMAGE_CANCEL . '</a></div>');
                break;
              case 'delete':
                $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_LINK_CATEGORY );

                $contents[] = array('form' => tep_draw_form('delete_link_categories', FILENAME_LINK_CATEGORIES, 'action=delete_confirm') . tep_draw_hidden_field('link_categories_id', $cInfo->link_categories_id));
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_DELETE_LINK_CATEGORIES_INTRO .'<br><b>' . $cInfo->link_categories_name . '</b></p></div></div></div>');
                if ($cInfo->link_categories_count > 0) $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">' . sprintf(TEXT_DELETE_WARNING_LINKS, $cInfo->link_categories_count) .'</p></div></div></div>');
                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_DELETE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_LINK_CATEGORIES, 'cID=' . $cInfo->link_categories_id) . '">' . IMAGE_CANCEL . '</a></div>');
                break;
              default:
                if (isset($cInfo) && is_object($cInfo)) {
                  $heading[] = array('text' => $cInfo->link_categories_name);

                  $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a
                  	class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_LINK_CATEGORIES, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->link_categories_id . '&action=edit') . '">' . IMAGE_EDIT . '</a><a
                  	class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_LINK_CATEGORIES, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->link_categories_id . '&action=delete') . '">' . IMAGE_DELETE . '</a></div>');

                  $contents[] = array('text' => '<div class="sidebar-text">' . tep_info_image($cInfo->link_categories_image, $cInfo->link_categories_name, HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT) . '</div><div class="sidebar-title">' . $cInfo->link_categories_image .'</div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_LINK_CATEGORY_DESCRIPTION . '</div><div class="sidebar-title">' . $cInfo->link_categories_description . '</div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_DATE_LINK_CATEGORY_CREATED . '</div><div class="sidebar-title">' . tep_date_short($cInfo->link_categories_date_added) . '</div>');
                  if (tep_not_null($cInfo->link_categories_last_modified)) {
                    $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_DATE_LINK_CATEGORY_LAST_MODIFIED . ' </div><div class="sidebar-title">' . tep_date_short($cInfo->link_categories_last_modified) . '</div>');
                  }
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_LINK_CATEGORY_COUNT . '</div><div class="sidebar-title">'  . $cInfo->link_categories_count . '</div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_LINK_CATEGORY_SORT_ORDER . '</div><div class="sidebar-title">'  . $cInfo->link_categories_sort_order . '</div>');
                }
                break;
            }

            if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
          	$box = new box;
          	echo $box->showSidebar($heading, $contents);
            }
            ?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
