<?php
/*
    Contribution Name: Manufacturer Sales Report
    Contribution Version: 2.3

Creation of this file 
Author Name: Robert Heath
Author E-Mail Address: robert@rhgraphicdesign.com
Athor Website: http://www.rhgraphicdesign.com
Donations: www.paypal.com
Donations Email: robert@rhgraphicdesign

Modifications on PHP file 
    Date: 28/05/07
    Name: Cyril Jacquenot
    What's modified?
      * fixed localizations:
        * use of currency class, to use global currency settings (�, �, $)
        * use of PHP server date/time format (fr_FR, en-EN, ...)         
      * fixed: code merging
        * before: copy/paste had been used
        * now: 
          * same code appears once
          * sql requests have completely been rewritten
          * sql requests have been speeded up
          * html code renewed (html tags are now OK) 
          * better display of pages                                       
      * added: PHP file including functions only : stats_manufacturers_sales_functions.php
      * added: some styles in "printer.css" and "stylesheet.css"
      * added: possibility to see sold products :
        * for all manufacturers                  
        * by one manufacturer                  
        * by all the customers of one manufacturer                  
        * by every customers of one manufacturer              
        * by one customer of one manufacturer
        * for each request, there is the possibility to print a simple page                          
        * when listing products, there is now a link to the product detail  
    What's need to be fixed?
      * french date format gestion with SpiffyCal
                                           
  Donations: www.paypal.com with email: cyril.jacquenot@laposte.net

Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();
  
  require(DIR_WS_FUNCTIONS . 'stats_manufacturers_sales_functions.php');

  // global variables
  $manufacturer_id = "";
  $customer_id = "";
  $manufacturer_name = "";
  $customer_name = "";

  if (isset($_GET['start_date'])) {
    $start_date = $_GET['start_date'];
  } else if (isset($_POST['start_date'])) {
    $start_date = $_POST['start_date'];
  } else {
    $start_date = (date('Y-m-01'));
  }

  if (isset($_GET['end_date'])) {
    $end_date = $_GET['end_date'];
  } else if (isset($_POST['end_date'])) {
    $end_date = $_POST['end_date'];
  } else {
    $end_date = (date('Y-m-d'));
  }
 
  // set printer-friendly toggle
  (tep_db_prepare_input($_GET['print']=='yes')) ? $print=true : $print=false;
  (tep_db_prepare_input($_GET['customer_only']=='yes')) ? $customer_only=true : $customer_only=false;
  // set inversion toggle
  
  if (!empty($_GET['mID']))    {$manufacturer_id = $_GET['mID'];}
  if (!empty($_GET['cID']))    {$customer_id = $_GET['cID'];}
  if (!empty($_GET['mName']))  {$manufacturer_name = $_GET['mName'];}
  if (!empty($_GET['cName']))  {$customer_name = $_GET['cName'];}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE_PERIOD . ": " . $start_date .  " - " . $end_date; ?> </h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

<div class="col main-col">
  <div class="row">
	<div class="col-6 pr-0 mb-2">
                  <?php echo tep_draw_form('date_range',FILENAME_STATS_MANUFACTURERS, tep_get_all_get_params(array("start_date", "end_date")), 'post'); ?> 
                    <?php echo ENTRY_STARTDATE; ?> &nbsp;
                    <input type="text" name="startDate" class="form-control" style="width:30%;display:inline!important">
                    <?php echo ENTRY_TODATE; ?> &nbsp;   
                    <input type="text" name="endDate" class="form-control" style="width:30%;display:inline!important">
                    <?php echo '<button class="btn btn-success btn-sm" type="submit">' . ENTRY_SUBMIT . '</button>'; ?>
                  </form>
	</div>
	<div class="col-6 pr-0 mb-2">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <?php echo $td_page_heading;?> 
                <td class="smallText" align="right">
                  <a class="btn btn-success btn-sm mt-2 mb-2" href="<?php echo tep_href_link(FILENAME_STATS_MANUFACTURERS,'print=yes&start_date=' . $start_date . '&end_date=' . $end_date . (tep_not_null($manufacturer_id) ? '&mID=' . $manufacturer_id : ''),'NONSSL');?>" target="print"><?php echo IMAGE_BUTTON_PRINT;?></a>
                  <?php 
                  if (isset($manufacturer_id) && tep_not_null($manufacturer_id)) {
                    echo '&nbsp; <a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_STATS_MANUFACTURERS,'start_date=' . $start_date . '&end_date=' . $end_date,'NONSSL') . '">' . IMAGE_BACK . '</a> &nbsp; ';
                  }
                  ?>
                </td>
              </tr>
            </table>
	</div>
  </div>
</div>
 
  <div class="col">   
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-statsproductspurchased" class="table-statsproductspurchased">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">
          

<?php 
$td_page_heading = "<td class='pageHeading'>";
if ($print) {$td_page_heading .= "<big>".STORE_NAME . "</big><br>";}
if (!$manufacturer_id) {
  $td_page_heading .= HEADING_TITLE; 
}
else {
  $td_page_heading .= HEADING_TITLE_REPORT_MANUFACTURER . $manufacturer_name; 
}
$td_page_heading .= "</td>";

if(!$print) {
// =======================================================================================================================================================================    
// TABLES : NOT IN PRINTING MODE
// =======================================================================================================================================================================    

} else {
// =======================================================================================================================================================================    
// TABLES : PRINTING MODE
// =======================================================================================================================================================================    

}
if (!$manufacturer_id) { 
      // =======================================================================================================================================================================    
      // mID == "" => list all the ordered products by manufacturers
      // =======================================================================================================================================================================    
      ?>
      <table class="table table-hover w-100 mt-2">
          <thead>
               <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_MANUFACTURERS_NAME; ?></th>
		          <th scope="col" class="th-col dark text-right"><?php echo TABLE_TOTAL_PRODUCTS; ?></th>
		          <th scope="col" class="th-col dark text-right"><?php echo TABLE_TOTAL_SALES; ?></th>
  		      </tr>
          <thead>
        <?php
        $manufacturers_query = tep_db_query(getMySQLraw("m"));
        while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
          $products_quantity = $manufacturers['sum_pq'];
          $final_price = $manufacturers['sum_fp'];
   
          if(!$print) {
            $url_param = 'mID=' . $manufacturers['manufacturers_id'] . '&mName=' . urlencode($manufacturers['manufacturers_name']) . "&start_date=$start_date&end_date=$end_date";
          ?>
            <tr class="dataTableRow" onClick="document.location.href='<?php echo tep_href_link(FILENAME_STATS_MANUFACTURERS, $url_param, 'NONSSL'); ?>'">
          <?php
          } else { // printing mode
          ?>
            <tr class="dataTableRow">
          <?php 
          }
          ?>
          <td class="table-col dark text-left"><?php echo $manufacturers['manufacturers_name']; ?></td>
          <td class="table-col dark text-right"><?php echo $products_quantity; ?></td>
          <td class="table-col dark text-right"><?php echo $currencies->format($final_price); ?></td>
        </tr>
        <?php
            $total_quantity = $total_quantity + $products_quantity;
            $total_sales = ($total_sales + $final_price);
            }
        ?>
        <tr>
          <td class="table-col dark text-right"><?php echo ENTRY_TOTAL; ?></td>
          <td class="table-col dark text-right"><?php echo $total_quantity; ?></td>
          <td class="table-col dark text-right"><?php echo $currencies->format($total_sales); ?></td>
        </tr>
      </table>
    <?php
   } else {
    // =======================================================================================================================================================================    
    // mID != "" => list all the ordered products for this manufacturer_id
    // =======================================================================================================================================================================

      if (!$customer_only) {
?>
      <table class="table table-hover w-100 mt-2">
        <thead>
	     <tr class="th-row">
		  <th scope="col" class="th-col dark text-left"><?php echo TABLE_CUSTOMER_NAME; ?></th>
          <th scope="col" class="th-col dark text-center"><?php echo TABLE_ORDER_PURCHASED; ?></th>
          <th scope="col" class="th-col dark text-right"><?php echo TABLE_TOTAL_PRODUCTS; ?></th>
          <th scope="col" class="th-col dark text-right"><?php echo TABLE_PRODUCT_REVENUE; ?></th>
        </tr>
        <?php
        $total_sales = 0;
        $total_quantity = 0;
        $man_customers_query = tep_db_query(getMySQLraw("c"));
        while ($man_cust_products = tep_db_fetch_array($man_customers_query)) {
          $products_quantity = $man_cust_products['sum_pq'];
          $final_price = $man_cust_products['sum_fp'];
          if (!$print) {
            $url_param = "cName=".$man_cust_products['customers_name']."&cID=".$man_cust_products['customers_id']."&mName=".urlencode($manufacturer_name)."&mID=$manufacturer_id&start_date=$start_date&end_date=$end_date";
          ?>
          
                <tr class="dataTableRow" onClick="document.location.href='<?php echo tep_href_link(FILENAME_STATS_MANUFACTURERS, $url_param, 'NONSSL'); ?>'">
                
          <?php
          } else {?>
                <tr class="dataTableRow">
          <?php
          }
          ?>
          <td class="table-col dark text-left"><?php echo $man_cust_products['customers_name']; ?></td>
          <td class="table-col dark text-center"><?php echo strftime(DATE_FORMAT_SHORT, strtotime($man_cust_products['dp'])); ?></td>
          <td class="table-col dark text-right"><?php echo $products_quantity; ?></td>
          <td class="table-col dark text-right"><?php echo $currencies->format($final_price); ?></td>
        </tr>
        <?php
            $total_quantity = $total_quantity + $products_quantity;
            $total_sales = ($total_sales + $final_price);
            }?>
        <tr class="dataTableRow">
          <td class="table-col dark text-left"><?php echo ALL_CUSTOMERS; ?></td>
          <td class="table-col dark text-right"><?php echo ENTRY_TOTAL; ?></td>
          <td class="table-col dark text-right"><?php echo $total_quantity; ?></td>
          <td class="table-col dark text-right"><?php echo $currencies->format($total_sales); ?></td>
        </tr>
        <?php
        if (!$print) {
          $url_param = "mName=".urlencode($manufacturer_name)."&mID=$manufacturer_id&start_date=$start_date&end_date=$end_date";?>
          <tr>
            <td class="table-col dark text-left" colspan="4"><?php
              echo "<a href='".tep_href_link(FILENAME_STATS_MANUFACTURERS, 'cID=all&'.$url_param, 'NONSSL')."'>".SHOW_ALL_ORDERED_PRODUCTS."</a>";?>
            </td>
          </tr>
          <tr>
            <td class="table-col dark text-left" colspan="4"><?php
              echo "<a href='".tep_href_link(FILENAME_STATS_MANUFACTURERS, 'cID=all_by_once&'.$url_param, 'NONSSL')."'>".SHOW_ALL_ORDERED_PRODUCTS_FOR_ALL_CUSTOMERS."</a>";?>
            </td>
          </tr><?php
        }?>
      </table><?php
      }

      require(DIR_WS_CLASSES . 'order.php');

      if ($customer_id) {
        if ($customer_id == "all_by_once") {
            $man_customers_list_query = tep_db_query(getMySQLraw("c"));
            while ($man_cust_list_products = tep_db_fetch_array($man_customers_list_query)) {
                $customer_id = $man_cust_list_products['customers_id'];
                echo getCustomerProductsBlock();
          }
          $customer_id = "all_by_once";
        }
        else {
          echo getCustomerProductsBlock();
        }
      }
}
?>
<!-- body_eof //-->
<!-- footer //-->
<?php 
if(!$print) {
} else {
  echo "<hr><span class='strongText'>".PRINTED_ON . "</span><span class='smallText'>" .strftime(DATE_TIME_FORMAT) . "</span>";
} 
?>
<!-- footer_eof //-->

			<div class="mb-1">&nbsp;</div>
		 </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>     