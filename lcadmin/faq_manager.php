<?php
/*
  $Id: faq_manager.php,v 1.1.1.1 2019/03/04 23:39:01 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.LoadedCommerce.com

  Copyright (c) 2019 LoadedCommerce

  Released under the GNU General Public License
*/
//300

require('includes/application_top.php');
require(DIR_WS_LANGUAGES . $language . '/faq.php');
$action = '';
if ( isset($_POST['faq_action'] ) && tep_not_null($_POST['faq_action'])) {
  $faq_action = tep_db_prepare_input($_POST['faq_action']);
} elseif ( isset($_GET['faq_action'] ) && tep_not_null($_GET['faq_action'])) {
  $faq_action = tep_db_prepare_input($_GET['faq_action']);
}
$languages = tep_get_languages();

switch($faq_action) {
  case 'setflag':
    tep_set_faq_status($_GET['faq_id'], $_GET['flag']);
	echo $_GET['flag'];exit;
    break;
	case'insert_faq':
		$status = (isset($_POST['faq_status']) && $_POST['faq_status'] == 'on')?1:0;
		$query = "INSERT INTO " . TABLE_FAQ . " SET visible='". $status ."', v_order='". $_POST['v_order'] ."', date=now()";
		tep_db_query($query);
		$fID = tep_db_insert_id();
		for ($i=0; $i<sizeof($languages); $i++)
		{
			$query = "INSERT INTO faq_description SET faq_id='". $fID ."', language_id='". $languages[$i]['id'] ."', question='". $_POST['faq_question'][$languages[$i]['id']] ."', answer='". $_POST['faq_answer'][$languages[$i]['id']] ."'";
			tep_db_query($query);
		}
		$query = tep_db_query("INSERT INTO " . TABLE_FAQ_TO_CATEGORIES . " SET faq_id='". $fID ."', categories_id='". $_POST['faq_category'] ."'");
		//tep_db_query("insert into " . TABLE_FAQ_TO_CATEGORIES . " (faq_id, categories_id) values ('" . (int)$fID . "', '" . (int)$_POST['faq_category'] . "')");

		// RCI for action insert
		echo $cre_RCI->get('faq', 'action', false);

		$messageStack->add_session('faqmanager', 'FAQ Added Successfully!!', 'success');
		$mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
		if ($mode == 'save')
		  tep_redirect(tep_href_link(FILENAME_FAQ_MANAGER, 'faq_id=' . $fID));
		else   // save & stay
		  tep_redirect(tep_href_link(FILENAME_FAQ_MANAGER, 'faq_id=' . $fID . '&faq_action=Edit'));

		break;
	case'update_faq':
		$faq_id = isset($_GET['faq_id'])?(int)$_GET['faq_id']:0;
		$status = (isset($_POST['faq_status']) && $_POST['faq_status'] == 'on')?1:0;
		if($faq_id > 0) {
			$query = "UPDATE " . TABLE_FAQ . " SET visible='". $status ."', v_order='". $_POST['v_order'] ."' WHERE faq_id='$faq_id'";
			tep_db_query($query);
			$fID = $faq_id;

			tep_db_query("DELETE FROM faq_description WHERE faq_id='". $fID ."'");
			for ($i=0; $i<sizeof($languages); $i++)
			{
				$query = "INSERT INTO faq_description SET faq_id='". $fID ."', language_id='". $languages[$i]['id'] ."', question='". $_POST['faq_question'][$languages[$i]['id']] ."', answer='". $_POST['faq_answer'][$languages[$i]['id']] ."'";
				tep_db_query($query);
			}
			tep_db_query("DELETE FROM " . TABLE_FAQ_TO_CATEGORIES . " WHERE faq_id = '" . (int)$fID . "'");
			tep_db_query("insert into " . TABLE_FAQ_TO_CATEGORIES . " (faq_id, categories_id) values ('" . (int)$fID . "', '" . (int)$_POST['faq_category'] . "')");

			// RCI for action insert
			echo $cre_RCI->get('faq', 'action', false);

			$messageStack->add_session('faqmanager', 'FAQ Updated Successfully!!', 'success');
			$mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
			if ($mode == 'save')
			  tep_redirect(tep_href_link(FILENAME_FAQ_MANAGER, 'faq_id=' . $fID));
			else   // save & stay
			  tep_redirect(tep_href_link(FILENAME_FAQ_MANAGER, 'faq_id=' . $fID . '&faq_action=Edit'));
		}
		else
			  tep_redirect(tep_href_link(FILENAME_FAQ_MANAGER));

		break;
	case'delete_faq':
		$faq_id = isset($_POST['faq_id'])?(int)$_POST['faq_id']:0;
		$fID = $faq_id;
		if($faq_id > 0) {
			tep_db_query("DELETE FROM " . TABLE_FAQ . " WHERE faq_id=$faq_id");
			tep_db_query("DELETE FROM faq_description WHERE faq_id=$faq_id");
			tep_db_query("delete from " . TABLE_FAQ_TO_CATEGORIES . " where faq_id = '" . (int)$faq_id . "'");

			// RCI for action insert
			echo $cre_RCI->get('faq', 'action', false);

			$messageStack->add_session('faqmanager', 'FAQ Deleted Successfully!!', 'success');
		}
		tep_redirect(tep_href_link(FILENAME_FAQ_MANAGER));
		break;
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
<?php
if($faq_action == 'Added' || $faq_action == 'Edit') {
	if (isset($_GET['faq_id']) && (int) $_GET['faq_id'] > 0) {
		$page_heading = 'Edit FAQ';
		$faq_id = (int)$_GET['faq_id'];
		$query = "SELECT * FROM faq WHERE faq_id=$faq_id";
		$rw_faq = tep_db_fetch_array(tep_db_query($query));
		$visible = $rw_faq['visible'];
		$active = ($visible == '1')?"checked":'';
		$v_order = $rw_faq['v_order'];
		$query = "SELECT * FROM faq_description WHERE faq_id=$faq_id";
		$rs_faq_desc = tep_db_query($query);
		while($rw_faq_desc = tep_db_fetch_array($rs_faq_desc)) {
			$faq_question[$rw_faq_desc['language_id']] = $rw_faq_desc['question'];
			$faq_answer[$rw_faq_desc['language_id']] = $rw_faq_desc['answer'];
		}

		$query = "SELECT * FROM ". TABLE_FAQ_TO_CATEGORIES ." WHERE faq_id=$faq_id";
		$rw_faq_cat = tep_db_fetch_array(tep_db_query($query));
		$faq_category = $rw_faq_cat['categories_id'];
	} else {
		$page_heading = 'New FAQ';
		$visible = 1;
		$active = "checked";
		$v_order = 1;
		$faq_category = 0;
		for ($i=0; $i<sizeof($languages); $i++)
		{
			$faq_question[$languages[$i]['id']] = '';
			$faq_answer[$languages[$i]['id']] = '';
		}
	}

?>
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo $page_heading; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>


  <div class="col">
    <?php
    if ($messageStack->size('faqmanager') > 0) {
      echo $messageStack->output('faqmanager');
    }
	echo '<form id="new_faq" name="new_faq" method="post" enctype="multipart/form-data" data-parsley-validate>';
	?>
      <!-- begin button bar -->
      <div id="button-bar" class="row">
        <div class="col-9 m-b-10 w-100 pt-1 pl-0 pr-0">
            <a href="<?php echo tep_href_link(FILENAME_FAQ_MANAGER); ?>" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> <?php echo ((isset($cID) && empty($cID) === false) ? BUTTON_RETURN_TO_LIST :  IMAGE_CANCEL); ?></a>
            <button type="submit" onclick="updateFaq('save');" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo BUTTON_SAVE; ?></button>
            <button type="submit" onclick="updateFaq('stay');" class="btn btn-info m-r-3 btn-save-stay"><i class="fa fa-plus-square"></i> <?php echo BUTTON_SAVE_STAY; ?></button>
        </div>
		<div class="col-3 m-b-10 pt-1 pr-2">
		  <div class="btn-group pull-right dark"> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class="btn btn-white dropdown-toggle"> <span id="langDropdownTitle"><?php echo ucwords($_SESSION['language']); ?></span> <span class="caret"></span> </a>
			<ul class="dropdown-menu pull-right" id="langselector" style="left:-50px!important;">
			  <?php
			  $languages = tep_get_languages();
			  for ($i=0; $i<sizeof($languages); $i++) {
				?>
				<li id="langsel_<?php echo $languages[$i]['id']; ?>" class="langval<?php echo (($languages[$i]['id'] == $_SESSION['languages_id'])? ' active':'');?>">
				  <a aria-expanded="false" href="javascript:changeLang('faq', '<?php echo $languages[$i]['name'];?>', <?php echo $languages[$i]['id']; ?>)"><?php echo '<span class="ml-2">' . $languages[$i]['name'];?></span></a>
				</li>
				<?php
			  }
			  ?>
			</ul>
		  </div>
		</div>
      </div>
      <!-- end button bar -->

    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-articles" class="table-articles">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
<?php
    for ($i=0; $i<sizeof($languages); $i++)
    {
		$display = ($languages[$i]['id'] == $_SESSION['languages_id']) ? '' : 'display:none;';
		$required_validation = ($languages[$i]['id'] == $_SESSION['languages_id']) ? ' required ' : '';
		$lang_control = '<div class="input-group"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
?>
			  <div style="<?php echo $display; ?>" class="faq-lang-pane <?php echo (($languages[$i]['id'] == $_SESSION['languages_id']) ? 'active' : '');?>" id="faq-default-pane-<?php echo $languages[$i]['id'];?>">
			  <div class="ml-2 mr-2">
				<div class="main-heading"><span>FAQ Info</span>
				  <div class="main-heading-footer"></div>
				</div>

				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo FAQ_QUESTION; ?><span class="required"></span></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php
						if (isset($aInfo->articles_id) && $aInfo->articles_id > 0) {
						  echo $lang_control. tep_draw_input_field('faq_question[' . $languages[$i]['id'] . ']', (isset($faq_question[$languages[$i]['id']]) ? $faq_question[$languages[$i]['id']] : ''), ' id="articles_name"  onblur="javascript:arti_getslug('.$languages[$i]['id'].','.$aInfo->articles_id.', this.value); "'.$required_validation).'</div>';
						} else{
						  echo $lang_control . tep_draw_input_field('faq_question[' . $languages[$i]['id'] . ']', (isset($faq_question[$languages[$i]['id']]) ? $faq_question[$languages[$i]['id']] : ''), ' id="articles_name"  onblur="javascript:arti_getslug('.$languages[$i]['id'].',0, this.value); "'.$required_validation).'</div>';
						}
					?>
				  </div>
				</div>
				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo FAQ_ANSWER; ?></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php echo tep_draw_textarea_field('faq_answer[' . $languages[$i]['id'] . ']', 'soft', '70', '15', (isset($faq_answer[$languages[$i]['id']]) ? $faq_answer[$languages[$i]['id']] : ''), 'class="form-control ckeditor"'); ?>
				  </div>
				</div>

               </div>
              </div>

<?php
    }

	$categories_array = array();
	$categories_array[] = array('id' => '', 'text' => TEXT_NO_CATEGORY);
	$categories_query = tep_db_query("select icd.categories_id, icd.categories_name from " . TABLE_FAQ_CATEGORIES_DESCRIPTION . " icd where language_id = '" . (int)$languages_id . "' order by icd.categories_name");
	while ($categories_values = tep_db_fetch_array($categories_query)) {
		$categories_array[] = array('id' => $categories_values['categories_id'], 'text' => $categories_values['categories_name']);
	}

?>
	</div>
	<div class="col-md-3 col-xl-2 panel-right rounded-right light">
		<div class="sidebar-container p-4"><div class="sidebar-heading"><span><div class="text-truncate">Options</div></span></div>
		<div class="sidebar-heading-footer"></div>
			<div class="sidebar-content-container">
				<div class="form-group row mt-3">
				  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo FAQ_STATUS; ?></label>
				  <div class="col-sm-7 p-relative">
					<input name="faq_status" <?php echo $active; ?> data-toggle="toggle" data-on="<i class='fa fa-check'></i> <?php echo TEXT_FAQ_ACTIVE; ?>" data-off="<i class='fa fa-times'></i> <?php echo TEXT_FAQ_INACTIVE; ?>" data-onstyle="success" data-offstyle="danger" type="checkbox" data-size="small">
				  </div>
				</div>
				<div class="form-group row mt-3">
				  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2">Sort Order</label>
				  <div class="col-sm-7 p-relative">
				  <?php
				  echo tep_draw_input_field('v_order', $v_order, 'size=3 maxlength=4 style="width:40%" required',false,'text',false);
				  ?>
				  </div>
				</div>

				<div class="form-group row mt-3">
				  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo ENTRY_CATEGORY; ?></label>
				  <div class="col-sm-7 p-relative"><?php echo tep_draw_pull_down_menu('faq_category', $categories_array, $faq_category); ?></div>
				</div>

				<?php /*
				if ($cre_RCO->get('faq', 'accessgroup') !== true) {
				?>
						<div class="form-group row mt-3">
						  <div class="col-sm-12">
						   <div data-toggle="popover" data-placement="top" data-html="true" data-content='<div class="text-white"><?php echo TEXT_B2B_UPSELL_POPOVER_BODY; ?></div><div class="text-center w-100"><a href="<?php echo TEXT_B2B_UPSELL_GET_B2B_URL; ?>" target="_blank" class="btn btn-warning btn-sm m-r-5 m-t-10"><?php echo TEXT_B2B_UPSELL_GET_B2B; ?></a></div>'>
							<img src="images/category-access-settings.jpg" alt="Get B2B to unlock this feature.">
						   </div>
						  </div>
						</div>
				<?php
				}*/
				?>
		  </div>
    </div>

    </form>

<?php
} else {
?>
	  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE ?></h1>
	  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

	  <div class="col">
		<?php
		if ($messageStack->size('faqmanager') > 0) {
		  echo $messageStack->output('faqmanager');
		}
		?>
		<!-- begin panel -->
		<div class="dark">
		  <!-- body_text //-->
		  <div id="table-faqmanager" class="table-faqmanager">
			<div class="row">
			  <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

			  <table class="table table-hover w-100 mt-2">
				  <thead>
				   <tr class="th-row">
					<th scope="col" class="th-col dark text-left"><?php echo FAQ_DATE;?></th>
					<th scope="col" class="th-col dark text-center"><?php echo tep_image(DIR_WS_IMAGES . 'icons/sort.gif', FAQ_SORT_BY); ?></th>
					<th scope="col" class="th-col dark text-left"><?php echo FAQ_QUESTION;?></th>
					<th scope="col" class="th-col dark text-center"><?php echo FAQ_ID;?></th>
					<th scope="col" class="th-col dark text-center"><?php echo FAQ_STATUS;?></th>
					<th scope="col" class="th-col dark text-center"><?php echo FAQ_ACTION;?></th>
				  </tr>
				 </thead>
				 <tbody>
				<?php
				$query = "SELECT * FROM faq A INNER JOIN faq_description B ON A.faq_id=B.faq_id AND B.language_id='". (int)$languages_id ."'";
				$rs_faq = tep_db_query($query);
				if (tep_db_num_rows($rs_faq) > 0) {
				  while ($faq = tep_db_fetch_array($rs_faq)) {
					  if ( ( ( ! isset($_GET['faq_id']) ) || ( isset($_GET['faq_id']) && $_GET['faq_id'] == $faq['faq_id'] )) && ( ! isset($cInfo) ) ) {
				          $cInfo = new objectInfo($faq);
					  }

	                  $selected =  (is_object($cInfo)) && ($faq['faq_id'] == $cInfo->faq_id) ? true : false;
	                  $col_selected = ($selected) ? ' selected' : '';
					  if ($selected) {
						echo '<tr class="table-row dark selected" id="crow_'.$faq['faq_id'].'">' . "\n";
					    $onclick='onclick="document.location.href=\'' . tep_href_link(FILENAME_FAQ_MANAGER, 'faq_action=Edit' . '&faq_id=' . $faq['faq_id']) . '\'"';
					  } else {
						echo '<tr class="table-row dark" id="crow_'.$faq['faq_id'].'">' . "\n";
						$del_groups_prepare .= ',\'' . $groups['admin_groups_id'] . '\'' ;
						$onclick='onclick="document.location.href=\'' . tep_href_link(FILENAME_FAQ_MANAGER, '&faq_id=' .  $faq['faq_id']) . '\'"';
					  }
					?>
					  <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo date('m/d/Y', strtotime($faq['date'])); ?></td>
					  <td class="table-col dark text-center<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo $faq['v_order'];?></td>
					  <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo $faq['question'];?></td>
					  <td class="table-col dark text-center<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo $faq['faq_id'];?></td>

					  <td class="setflag table-col dark text-center<?php echo $col_selected; ?>">
						<?php
						/*if ($faq['visible'] == 1) {
						  echo '<i class="fa fa-lg fa-check-circle text-success mr-2"></i><a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Visible&faq_id=$faq[faq_id]&visible=$faq[visible]") . '"><i class="fa fa-lg fa-times-circle text-secondary mr-2"></i></a>';
						} else {
						  echo '<i class="fa fa-lg fa-times-circle text-danger mr-2"></i><a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Visible&faq_id=$faq[faq_id]&visible=$faq[visible]") . '"><i class="fa fa-lg fa-check-circle text-secondary mr-2"></i></a>';
						}*/

						  $ajax_link = tep_href_link(FILENAME_FAQ_MANAGER);
						  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'faq_action=setflag&flag=0&faq_id='.$faq['faq_id'].'\', '.$faq['faq_id'].',0 )" '.(($faq['visible'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
						  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'faq_action=setflag&flag=1&faq_id='.$faq['faq_id'].'\', '.$faq['faq_id'].',1  )" '.(($faq['visible'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
						?>
					  </td>

					  <td class="table-col dark text-center<?php echo $col_selected; ?>">
						<?php echo '<a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=Edit&faq_id=$faq[faq_id]", 'NONSSL') . '"><i class="fa fa-edit fa-lg text-success mr-2"></i></a><a href="' . tep_href_link(FILENAME_FAQ_MANAGER, "faq_action=delete&faq_id=$faq[faq_id]", 'NONSSL') . '"><i class="fa fa-trash fa-lg text-danger mr-2"></i></a>'; ?>
					  </td>
					</tr>
					<?php
					$no++;
				  }
				} else {
				  ?>
				  <tr>
					<td colspan="7" class="table-col dark text-center"><?php echo FAQ_ALERT; ?></td>
				  </tr>
				  <?php
				}
				?>
			  </table>

			  <table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
				  <td align="right">
					<?php echo '<a class="btn btn-default btn-sm mt-2 mb-2 mr-2"  href="' . tep_href_link(FILENAME_FAQ_MANAGER, '', 'NONSSL') . '">' . IMAGE_CANCEL . '</a><a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_FAQ_MANAGER, 'faq_action=Added', 'NONSSL') . '">' . FAQ_ADD . '</a>'; ?>
				  </td>
				</tr>
				<tr>
				<?php
				// RCI code start
				echo $cre_RCI->get('faqlist', 'listingbottom');
				// RCI code eof
				?>
				</tr>
			  </table>
<?php
}
?>
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
    			  $heading = array();
    			  $contents = array();

    			  switch ($faq_action) {
    				case 'delete':
						$heading[] = array('text' => FAQ_DELETE_ID.$_GET['faq_id']);
						$contents[] = array('form' => tep_draw_form('topics', FILENAME_FAQ_MANAGER, 'faq_action=delete_faq&faq_id=' . $cInfo->faq_id) . tep_draw_hidden_field('faq_id', $cInfo->faq_id));
						$contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_DELETE_FAQ_INTRO.'<br><br><b>' . $cInfo->question . '</b></p></div></div></div>');
						$contents[] = array('align' => 'center', 'text' => '<br><a class="btn btn-default btn-block btn-sm mt-2 mr-2" href="' . tep_href_link(FILENAME_FAQ_MANAGER, 'faq_id=' . $cInfo->faq_id) . '">' . IMAGE_CANCEL . '</a><button class="btn btn-danger btn-block btn-sm" type="submit">' . IMAGE_DELETE . '</button>');
    				  break;
    				default:
    				  if (isset($cInfo) && is_object($cInfo)) {
    					$heading[] = array('text' => $cInfo->question );
					    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-success btn-block btn-sm mr-2 mt-2" href="' . tep_href_link(FILENAME_FAQ_MANAGER, 'faq_id=' . $cInfo->faq_id . '&faq_action=Edit') . '">' . IMAGE_EDIT . '</a><a
					    					class="btn btn-danger btn-block btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_FAQ_MANAGER, 'faq_id=' . $cInfo->faq_id . '&faq_action=delete') . '">' . IMAGE_DELETE . '</a><div>');
					    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 mb-2 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . $cInfo->question . '</p></div></div></div>');
    					$contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->date) . '</span></div>');
    					if (tep_not_null(tep_date_short($cInfo->last_modified))) $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->date) . '</span></div>');
    				  }

    				  break;
    			  }

            // use $box->showSidebar()
            if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
              $box = new box;
              echo $box->showSidebar($heading, $contents);
            }
            ?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script language="javascript">
function changeLang(pagetype, langName, langId) {
    $('#langDropdownTitle').html(langName);
    $(".langval").removeClass("active");
    $("#langsel_"+langId).addClass("active");
	$( "."+ pagetype +"-lang-pane" ).hide( "slow" );
	$( "#"+ pagetype +"-default-pane-"+langId ).show( "slow" );
}
function updateFaq(mode) {
  var action = '<?php echo str_replace('&amp;', '&', tep_href_link(FILENAME_FAQ_MANAGER, (isset($_GET['faq_id']) ? '&faq_id=' . $_GET['faq_id'] : '') . '&faq_action=' . (isset($_GET['faq_id']) ? 'update_faq' : 'insert_faq'))); ?>';
  // set the save mode in hidden form input
  $('<input />').attr('type', 'hidden')
      .attr('name', "mode")
      .attr('value', mode)
      .appendTo('#new_faq');

<?php
echo $lcadmin->print_action_js('addupdatefaq');
?>

  $('#new_faq').attr('action', action).submit();
}

<?php
echo $lcadmin->print_js();
?>
</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
