<?php
/*
  $Id: tax_classes.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'insert':
      $tax_class_title = tep_db_prepare_input($_POST['tax_class_title']);
      $tax_class_description = tep_db_prepare_input($_POST['tax_class_description']);

      tep_db_query("insert into " . TABLE_TAX_CLASS . " (tax_class_title, tax_class_description, date_added) values ('" . tep_db_input($tax_class_title) . "', '" . tep_db_input($tax_class_description) . "', now())");

      tep_redirect(tep_href_link(FILENAME_TAX_CLASSES));
      break;
    case 'save':
      $tax_class_id = tep_db_prepare_input($_GET['tID']);
      $tax_class_title = tep_db_prepare_input($_POST['tax_class_title']);
      $tax_class_description = tep_db_prepare_input($_POST['tax_class_description']);

      tep_db_query("update " . TABLE_TAX_CLASS . " set tax_class_id = '" . (int)$tax_class_id . "', tax_class_title = '" . tep_db_input($tax_class_title) . "', tax_class_description = '" . tep_db_input($tax_class_description) . "', last_modified = now() where tax_class_id = '" . (int)$tax_class_id . "'");

      tep_redirect(tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $tax_class_id));
      break;
    case 'deleteconfirm':
      $tax_class_id = tep_db_prepare_input($_GET['tID']);

      tep_db_query("delete from " . TABLE_TAX_CLASS . " where tax_class_id = '" . (int)$tax_class_id . "'");

      tep_redirect(tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page']));
      break;
  }
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-tax-classes" class="table-tax-classes">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_TAX_CLASSES; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody>             
                <?php
                $classes_query_raw = "select tax_class_id, tax_class_title, tax_class_description, last_modified, date_added from " . TABLE_TAX_CLASS . " order by tax_class_title";
                $classes_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $classes_query_raw, $classes_query_numrows);
                $classes_query = tep_db_query($classes_query_raw);
                while ($classes = tep_db_fetch_array($classes_query)) {
                  if ((!isset($_GET['tID']) || (isset($_GET['tID']) && ($_GET['tID'] == $classes['tax_class_id']))) && !isset($tcInfo) && (substr($action, 0, 3) != 'new')) {
                    $tcInfo = new objectInfo($classes);
                  }

                  $selected = (isset($tcInfo) && is_object($tcInfo) && ($classes['tax_class_id'] == $tcInfo->tax_class_id)) ? ' selected' : '';
                  if ($selected) {
                    echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $tcInfo->tax_class_id . '&action=edit') . '\'">' . "\n";
                  } else {
                    echo'<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $classes['tax_class_id']) . '\'">' . "\n";
                  }
                  $col_selected = ($selected) ? ' selected' : '';
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $classes['tax_class_title']; ?></td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?>">
                    <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $classes['tax_class_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                  </td>                  
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>

            <div class="pagination-container ml-2 mr-2">
              <div class="results-right"><?php echo $classes_split->display_count($classes_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_TAX_CLASSES); ?></div>
              <div class="results-left"><?php echo $classes_split->display_links($classes_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></div>
            </div>
            <?php
            if (empty($action)) {
              ?>
              <div class="float-right mr-2 mt-3 mb-3" role="group">
                <button class="btn btn-success btn-sm" onclick="window.location='<?php echo tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&action=new'); ?>'"><?php echo IMAGE_NEW_TAX_CLASS; ?></button> 
              </div>
              <?php 
            } 
            ?>            
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              switch ($action) {
                case 'new':
                  $heading[] = array('text' => TEXT_INFO_HEADING_NEW_TAX_CLASS);
                  $contents[] = array('form' => tep_draw_form('classes', FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&action=insert','post','data-parsley-validate'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_INSERT_INTRO . '</p></div></div></div>');                                           
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_CLASS_TITLE . '<span class="required"></span><span class="sidebar-title ml-2">' . tep_draw_input_field('tax_class_title', null, 'class="form-control" required') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CLASS_DESCRIPTION . '<span class="sidebar-title ml-2">' . tep_draw_input_field('tax_class_description', null, 'class="form-control"') . '</span></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-insert" type="submit">' . IMAGE_INSERT . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page']) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'edit':
                  $heading[] = array('text' => TEXT_INFO_HEADING_EDIT_TAX_CLASS);
                  $contents[] = array('form' => tep_draw_form('classes', FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $tcInfo->tax_class_id . '&action=save','post','data-parsley-validate'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_INFO_EDIT_INTRO . '</p></div></div></div>');                                           
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_CLASS_TITLE . '<span class="required"></span><span class="sidebar-title ml-2">' . tep_draw_input_field('tax_class_title', $tcInfo->tax_class_title, 'class="form-control" required') . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CLASS_DESCRIPTION . '<span class="sidebar-title ml-2">' . tep_draw_input_field('tax_class_description', $tcInfo->tax_class_description, 'class="form-control"') . '</span></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $tcInfo->tax_class_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'delete':
                  $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_TAX_CLASS);
                  $contents[] = array('form' => tep_draw_form('classes', FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $tcInfo->tax_class_id . '&action=deleteconfirm'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . sprintf(TEXT_INFO_DELETE_INTRO, '<b>' . $tcInfo->tax_class_title . '</b>') . '</p></div></div></div>');                                           
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_CONFIRM_DELETE . '</button><button type="button" class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $tcInfo->tax_class_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                default:
                  if (isset($tcInfo) && is_object($tcInfo)) {
                    $heading[] = array('text' => '<div class="text-truncate">' . $tcInfo->tax_class_title . '</div>');
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
                                    <button class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $tcInfo->tax_class_id . '&action=edit')  . '\'">' . IMAGE_EDIT . '</button>
                                    <button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_TAX_CLASSES, 'page=' . $_GET['page'] . '&tID=' . $tcInfo->tax_class_id . '&action=delete') . '\'">' . IMAGE_DELETE . '</button>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_DATE_ADDED . '<span class="sidebar-title ml-2 f-w-600">' . tep_date_short($trInfo->date_added) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_LAST_MODIFIED . '<span class="sidebar-title ml-2 f-w-600">' . tep_date_short($trInfo->last_modified) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1 mb-3">' . TEXT_INFO_CLASS_DESCRIPTION . '<span class="sidebar-title ml-2 f-w-600">' . $trInfo->tax_description . '</span></div>');
                  }
                  break;
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
