<?php
/*
  $Id: admin_members.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require_once('includes/application_top.php');

$current_boxes = DIR_FS_ADMIN . DIR_WS_BOXES;

if ( ! isset($_GET['action']) ) $_GET['action'] = '';

if ($_GET['action'] != '') {
  switch ($_GET['action']) {
    case 'member_edit':
      $admin_id = tep_db_prepare_input($_POST['admin_id']);
      $hiddenPassword = '-hidden-';
      $stored_email[] = 'NONE';

      $check_email_query = tep_db_query("select admin_email_address from " . TABLE_ADMIN . " where admin_id <> " . $admin_id . "");
      while ($check_email = tep_db_fetch_array($check_email_query)) {
        $stored_email[] = $check_email['admin_email_address'];
      }

      if (in_array($_POST['admin_email_address'], $stored_email)) {
        tep_redirect(tep_href_link(FILENAME_ADMIN_MEMBERS, 'error=email&action=edit_member&mID=' . $_GET['mID'] . setPage()));
      } else {
        $sql_data_array = array('admin_groups_id' => tep_db_prepare_input($_POST['admin_groups_id']),
                                'admin_firstname' => tep_db_prepare_input($_POST['admin_firstname']),
                                'admin_lastname' => tep_db_prepare_input($_POST['admin_lastname']),
                                'admin_email_address' => tep_db_prepare_input($_POST['admin_email_address']),
                                'admin_modified' => 'now()');

        tep_db_perform(TABLE_ADMIN, $sql_data_array, 'update', 'admin_id = \'' . $admin_id . '\'');

        tep_mail($_POST['admin_firstname'] . ' ' . $_POST['admin_lastname'], $_POST['admin_email_address'], ADMIN_EMAIL_EDIT_SUBJECT, sprintf(ADMIN_EMAIL_EDIT_TEXT, $_POST['admin_firstname'], HTTP_SERVER . DIR_WS_ADMIN, $_POST['admin_email_address'], $hiddenPassword, STORE_OWNER), STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);

        tep_redirect(tep_href_link(FILENAME_ADMIN_MEMBERS, 'mID=' . $admin_id . setPage()));
      }
      break;
  }
}

// RCI code start
echo $cre_RCI->get('administrator', 'pageaction', false);
// RCI code eof


if ( isset($_GET['gID']) ) {
  $heading_title = HEADING_TITLE_GROUPS;
} elseif ( isset($_GET['gPath']) ) {
  $heading_title = HEADING_TITLE_DEFINE;
} else {
  $heading_title = HEADING_TITLE_MEMBERS;
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo $heading_title; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-members" class="table-members">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <?php
            if ( isset($_GET['gPath']) ) {          -
              $group_name_query = tep_db_query("select admin_groups_name from " . TABLE_ADMIN_GROUPS . " where admin_groups_id = " . $_GET['gPath']);
              $group_name = tep_db_fetch_array($group_name_query);

              if ($_GET['gPath'] == 1) {
                echo tep_draw_form('defineForm', FILENAME_ADMIN_MEMBERS, 'gID=' . $_GET['gPath']);
              } elseif ($_GET['gPath'] != 1) {
                echo tep_draw_form('defineForm', FILENAME_ADMIN_MEMBERS, 'gID=' . $_GET['gPath'] . '&action=group_define', 'post', 'enctype="multipart/form-data"');
                echo tep_draw_hidden_field('admin_groups_id', $_GET['gPath']);
              }
              ?>
              <table class="table table-file-permissions w-100 mt-2">
                <thead>
                  <tr class="th-row">
                    <th scope="col" colspan="2" class="th-col dark text-left"><?php echo TABLE_HEADING_GROUPS_DEFINE; ?></th>
                  </tr>
                </thead>              
                <tbody>
                <?php
                $db_boxes_query = tep_db_query("select admin_files_id as admin_boxes_id, admin_files_name as admin_boxes_name, admin_groups_id as boxes_group_id, admin_files_is_boxes from " . TABLE_ADMIN_FILES . " where (admin_files_is_boxes = '1' or  admin_files_is_boxes = '3' ) order by sort");
                while ($group_boxes = tep_db_fetch_array($db_boxes_query)) {
                  $group_boxes_files_query = tep_db_query("select admin_files_id, admin_files_name, admin_groups_id, constant_name from " . TABLE_ADMIN_FILES . " where (admin_files_is_boxes = '0'or admin_files_is_boxes = '2') and admin_files_to_boxes = '" . $group_boxes['admin_boxes_id'] . "' order by sort");

                  $selectedGroups = $group_boxes['boxes_group_id'];
                  $groupsArray = explode(",", $selectedGroups);

                  if (in_array($_GET['gPath'], $groupsArray)) {
                    $del_boxes = array($_GET['gPath']);
                    $result = array_diff ($groupsArray, $del_boxes);
                    sort($result);
                    $checkedBox = $selectedGroups;
                    $uncheckedBox = implode (",", $result);
                    $checked = true;
                  } else {
                    $add_boxes = array($_GET['gPath']);
                    $result = array_merge ($add_boxes, $groupsArray);
                    sort($result);
                    $checkedBox = implode (",", $result);
                    $uncheckedBox = $selectedGroups;
                    $checked = false;
                  }
                  ?>
                  <tr class="table-row-boxes">
                    <?php
                    if ($group_boxes['admin_files_is_boxes'] == '1') {
                      ?>
                      <td class="col-boxes" width="23"><?php echo tep_draw_checkbox_field('groups_to_boxes[]', $group_boxes['admin_boxes_id'], $checked, '', 'id="groups_' . $group_boxes['admin_boxes_id'] . '" onClick="checkGroups(this)"'); ?></td>
                      <td class="col-boxes"><b><?php echo ucwords(substr_replace ($group_boxes['admin_boxes_name'], '', -4)) . ' ' . tep_draw_hidden_field('checked_' . $group_boxes['admin_boxes_id'], $checkedBox) . tep_draw_hidden_field('unchecked_' . $group_boxes['admin_boxes_id'], $uncheckedBox); ?></b></td>
                      <?php 
                    } else { 
                      ?>
                      <td class="col-boxes" width="23"><?php echo tep_draw_checkbox_field('groups_to_boxes[]', $group_boxes['admin_boxes_id'], $checked, '', 'id="groups_' . $group_boxes['admin_boxes_id'] . '" onClick="checkGroups(this)"'); ?></td>
                      <td class="col-boxes"><b><?php echo ucwords(substr_replace ($group_boxes['admin_boxes_name'], '', -4)) . ' ' . tep_draw_hidden_field('checked_' . $group_boxes['admin_boxes_id'], $checkedBox) . tep_draw_hidden_field('unchecked_' . $group_boxes['admin_boxes_id'], $uncheckedBox); ?></b></td>
                      <?php 
                    }
                    ?>
                  </tr>
                  <tr class="table-row">
                    <td class="table-col">&nbsp;</td>
                    <td class="table-col">
                      <table class="w-100">
                        <?php
                        //$group_boxes_files_query = tep_db_query("select admin_files_id, admin_files_name, admin_groups_id from " . TABLE_ADMIN_FILES . " where admin_files_is_boxes = '0' and admin_files_to_boxes = '" . $group_boxes['admin_boxes_id'] . "' order by admin_files_name");
                        while($group_boxes_files = tep_db_fetch_array($group_boxes_files_query)) {
                          $selectedGroups = $group_boxes_files['admin_groups_id'];
                          $groupsArray = explode(",", $selectedGroups);  

                          if (in_array($_GET['gPath'], $groupsArray)) {
                            $del_boxes = array($_GET['gPath']);
                            $result = array_diff ($groupsArray, $del_boxes);
                            sort($result);
                            $checkedBox = $selectedGroups;
                            $uncheckedBox = implode (",", $result);
                            $checked = true;
                          } else {
                            $add_boxes = array($_GET['gPath']);
                            $result = array_merge ($add_boxes, $groupsArray);
                            sort($result);
                            $checkedBox = implode (",", $result);
                            $uncheckedBox = $selectedGroups;
                            $checked = false;
                          }
						$left_menu_name = (defined($group_boxes_files['constant_name'])?constant($group_boxes_files['constant_name']):((empty($group_boxes_files['constant_name'])?$group_boxes_files['admin_files_name']:$group_boxes_files['constant_name'])));
                          ?>
                          <tr>
                            <?php
                            if ($group_boxes['admin_files_is_boxes'] == '1') {
                              ?>
                              <td class="table-col" width="20"><?php echo tep_draw_checkbox_field('groups_to_boxes[]', $group_boxes_files['admin_files_id'], $checked, '', 'id="subgroups_' . $group_boxes['admin_boxes_id'] . '" onClick="checkSub(this)"'); ?></td>
                              <td class="table-col"><?php echo $left_menu_name . ' ' . tep_draw_hidden_field('checked_' . $group_boxes_files['admin_files_id'], $checkedBox) . tep_draw_hidden_field('unchecked_' . $group_boxes_files['admin_files_id'], $uncheckedBox);?></td>
                              <?php 
                            } else {  
                              ?>
                              <td class="table-col" width="20"><?php echo tep_draw_checkbox_field('groups_to_boxes[]', $group_boxes_files['admin_files_id'], $checked, '', 'id="subgroups_' . $group_boxes['admin_boxes_id'] . '" onClick="checkSub(this)"'); ?></td>
                              <td class="table-col"><?php echo $left_menu_name . ' ' . tep_draw_hidden_field('checked_' . $group_boxes_files['admin_files_id'], $checkedBox) . tep_draw_hidden_field('unchecked_' . $group_boxes_files['admin_files_id'], $uncheckedBox);?></td>
                              <?php 
                            } 
                            ?>
                          </tr>
                          <?php
                        }
                        ?>
                      </table>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
              </table></form>
              <?php
            } elseif ( isset($_GET['gID']) ) {
              ?>
              <table class="table table-hover w-100 mt-2">
                <thead>
                  <tr class="th-row">
                    <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_GROUPS_NAME; ?></th>
                    <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $db_groups_query = tep_db_query("select * from " . TABLE_ADMIN_GROUPS . " order by admin_groups_id");
                $add_groups_prepare = '\'0\'' ;
                $del_groups_prepare = '\'0\'' ;
                $count_groups = 0;
                while ($groups = tep_db_fetch_array($db_groups_query)) {
                  $add_groups_prepare .= ',\'' . $groups['admin_groups_id'] . '\'' ;
                  if ( ( ( ! isset($_GET['gID']) ) || ( isset($_GET['gID']) && $_GET['gID'] == $groups['admin_groups_id'] ) || ( isset($_GET['gID']) && $_GET['gID'] == 'groups') ) && ( ! isset($gInfo) ) ) {
                    $gInfo = new objectInfo($groups);
                  }
                  $selected =  (is_object($gInfo)) && ($groups['admin_groups_id'] == $gInfo->admin_groups_id) ? true : false;
                  if ($selected) {
                    echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_ADMIN_MEMBERS, 'action=edit_group' . '&gID=' . $groups['admin_groups_id']) . '\'">' . "\n";
                  } else {
                    echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_ADMIN_MEMBERS, '&gID=' .  $groups['admin_groups_id']) . '\'">' . "\n";
                    $del_groups_prepare .= ',\'' . $groups['admin_groups_id'] . '\'' ;                    
                  }
                  $col_selected = ($selected) ? ' selected' : '';                  
                  ?>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $groups['admin_groups_name']; ?></td>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_ADMIN_MEMBERS, 'gID=' . $groups['admin_groups_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?></td>
                  </tr>
                  <?php
                  $count_groups++;
                }
                ?>
                </tbody>
              </table>
              <div class="pagination-container">
                <div class="results-left ml-1"><?php echo TEXT_COUNT_GROUPS . ' ' . $count_groups; ?></div>
              </div>
              <div class="float-right mr-2 mt-3 mb-3" role="group">
                <div class="d-inline">
                  <button class="btn btn-success btn-sm mr-1" onclick="window.location='<?php echo tep_href_link(FILENAME_ADMIN_MEMBERS); ?>'"><?php echo HEADING_TITLE_MEMBERS; ?></button> 
                </div>
              <?php
				if ($cre_RCO->get('administrator', 'newgroupbutton') !== true) {
              ?>
                <div class="p-relative d-inline">
                  <button class="btn btn-success btn-sm mr-1 disabled" data-container="body" data-toggle="popover" data-placement="top" data-html="true" data-content="<div class='text-white'><?php echo TEXT_PRO_UPSELL_POPOVER_BODY; ?></div><div class='text-center w-100'><a href='<?php echo TEXT_PRO_UPSELL_GET_PRO_URL; ?>' target='_blank' class='btn btn-danger btn-sm m-r-5 m-t-10'><?php echo TEXT_PRO_UPSELL_GET_PRO; ?></a></div>"><?php echo IMAGE_NEW_GROUP; ?></button> 
                  <div class="ribbon-button mr-1"><img src="assets/img/ribbon-pro.png"></div>
                </div>
              <?php
              }
              ?>  
              </div>
              <?php
            } else {
              ?>
              <table class="table table-hover w-100 mt-2">
                <thead>
                  <tr class="th-row">
                    <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_NAME; ?></th>
                    <th scope="col" class="th-col dark text-left d-none d-md-table-cell"><?php echo TABLE_HEADING_EMAIL; ?></th>
                    <th scope="col" class="th-col dark text-left d-none d-lg-table-cell"><?php echo TABLE_HEADING_GROUPS; ?></th>
                    <th scope="col" class="th-col dark text-center d-none d-xl-table-cell"><?php echo TABLE_HEADING_LOGNUM; ?></th>
                    <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $db_admin_query_raw = "select * from " . TABLE_ADMIN . " order by admin_firstname";
                  $db_admin_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $db_admin_query_raw, $db_admin_query_numrows);
                  $db_admin_query = tep_db_query($db_admin_query_raw);
                  //$db_admin_num_row = tep_db_num_rows($db_admin_query);
                  while ($admin = tep_db_fetch_array($db_admin_query)) {
                    $admin_group_query = tep_db_query("select admin_groups_name from " . TABLE_ADMIN_GROUPS . " where admin_groups_id = '" . $admin['admin_groups_id'] . "'");
                    $admin_group = tep_db_fetch_array ($admin_group_query);
                    if ($admin_group === false) $admin_group = array();
                    if ( ( ( ! isset($_GET['mID']) ) || ( isset($_GET['mID']) && $_GET['mID'] == $admin['admin_id']) ) && ( ! isset($mInfo) ) ) {
                      $mInfo_array = array_merge($admin, $admin_group);
                      $mInfo = new objectInfo($mInfo_array);
                    }                    

                    $selected =  (is_object($mInfo)) && ($admin['admin_id'] == $mInfo->admin_id) ? true : false;
                    if ($selected) {
                      echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_ADMIN_MEMBERS, 'action=edit_member' . '&mID=' . $admin['admin_id'] . setPage()) . '\'">' . "\n";
                    } else {
                      echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_ADMIN_MEMBERS, '&mID=' . $admin['admin_id'] . setPage()) . '\'">' . "\n";
                    }
                    $col_selected = ($selected) ? ' selected' : '';
                    ?>
                      <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $admin['admin_firstname']; ?>&nbsp;<?php echo $admin['admin_lastname']; ?></td>
                      <td class="table-col dark text-left d-none d-md-table-cell<?php echo $col_selected; ?>"><?php echo $admin['admin_email_address']; ?></td>
                      <td class="table-col dark text-left d-none d-lg-table-cell<?php echo $col_selected; ?>"><?php echo $admin_group['admin_groups_name']; ?></td>
                      <td class="table-col dark text-center d-none d-xl-table-cell<?php echo $col_selected; ?>"><?php echo $admin['admin_lognum']; ?></td>
                      <td class="table-col dark text-right<?php echo $col_selected; ?>">
                        <?php 
                        echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_ADMIN_MEMBERS, 'mID=' . $admin['admin_id'] . setPage()) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?></td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>

              <div class="pagination-container">
                <div class="results-right"><?php echo $db_admin_split->display_links($db_admin_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></div>
                <div class="results-left"><?php echo $db_admin_split->display_count($db_admin_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_MEMBERS); ?></div>
              </div>

              <div class="float-right mr-2 mt-3 mb-3" role="group">
              <?php
				if ($cre_RCO->get('administrator', 'newbuttons') !== true) {
              ?>
                <div class="p-relative d-inline">
                  <button class="btn btn-success btn-sm mr-1 disabled" data-container="body" data-toggle="popover" data-placement="top" data-html="true" data-content="<div class='text-white'><?php echo TEXT_PRO_UPSELL_POPOVER_BODY; ?></div><div class='text-center w-100'><a href='<?php echo TEXT_PRO_UPSELL_GET_PRO_URL; ?>' target='_blank' class='btn btn-danger btn-sm m-r-5 m-t-10'><?php echo TEXT_PRO_UPSELL_GET_PRO; ?></a></div>"><?php echo IMAGE_NEW_GROUP; ?></button> 
                  <div class="ribbon-button mr-2"><img src="assets/img/ribbon-pro.png"></div>
                </div>
 
                <div class="p-relative d-inline">
                  <button class="btn btn-success btn-sm disabled" data-container="body" data-toggle="popover" data-placement="top" data-html="true" data-content="<div class='text-white'><?php echo TEXT_PRO_UPSELL_POPOVER_BODY; ?></div><div class='text-center w-100'><a href='<?php echo TEXT_PRO_UPSELL_GET_PRO_URL; ?>' target='_blank' class='btn btn-danger btn-sm m-r-5 m-t-10'><?php echo TEXT_PRO_UPSELL_GET_PRO; ?></a></div>"><?php echo IMAGE_NEW_MEMBER; ?></button> 
                  <div class="ribbon-button"><img src="assets/img/ribbon-pro.png"></div>
                </div>
              <?php
              }
              ?>  
              </div>
              <?php
            }
            ?>
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
            $isAdmin = false;
            $heading = array();
            $contents = array();
            switch ($_GET['action']) {
              case 'edit_member':
                $heading[] = array('text' => TEXT_INFO_HEADING_EDIT);
                $contents[] = array('form' => tep_draw_form('editmember', FILENAME_ADMIN_MEMBERS, 'action=member_edit&&mID=' . $_GET['mID'] . setPage(), 'post', 'id="editmember" role="form" data-parsley-validate'));
                if ( isset($_GET['error']) ) $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><h4 class="m-0">' . TEXT_ERROR . '</h4><p class="mb-0 mt-2">' . TEXT_INFO_EMAIL_USED . '</p></div></div></div>');     
                $contents[] = array('text' => tep_draw_hidden_field('admin_id', $mInfo->admin_id));
                $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_INFO_FIRSTNAME . '<span class="required"></span></div><div>' . tep_draw_input_field('admin_firstname', $mInfo->admin_firstname, 'class="form-control" required') . '</div>');
                $contents[] = array('text' => '<div class="form-label mt-2">' . TEXT_INFO_LASTNAME . '<span class="required"></span></div><div>' . tep_draw_input_field('admin_lastname', $mInfo->admin_lastname, 'class="form-control" required') . '</div>');
                $contents[] = array('text' => '<div class="form-label mt-2">' . TEXT_INFO_EMAIL . '<span class="required"></span></div><div>' . tep_draw_input_field('admin_email_address', $mInfo->admin_email_address, 'class="form-control" type="email" required') . '</div>');
                if ($mInfo->admin_id == $_SESSION['login_id'] || $mInfo->admin_email_address == STORE_OWNER_EMAIL_ADDRESS) {
                  $isStoreAdmin = true;
                  $contents[] = array('text' => tep_draw_hidden_field('admin_groups_id', $mInfo->admin_groups_id));
                } else {
                  $isStoreAdmin = false;
                  $groups_array = array(array('id' => '0', 'text' => TEXT_NONE));
                  $groups_query = tep_db_query("select admin_groups_id, admin_groups_name from " . TABLE_ADMIN_GROUPS);
                  while ($groups = tep_db_fetch_array($groups_query)) {
                    $groups_array[] = array('id' => $groups['admin_groups_id'],
                                            'text' => $groups['admin_groups_name']);
                  }
                  $contents[] = array('text' => '<div class="form-label mt-2">' . TEXT_INFO_GROUP . '</div><div>' . tep_draw_pull_down_menu('admin_groups_id', $groups_array, $mInfo->admin_groups_id, 'class="form-control" required') . '</div>');
                }
                $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><button class="btn btn-grey btn-sm mt-2 mb-2 mr-1 btn-cancel" type="button" onclick="window.location=\'' . tep_href_link(FILENAME_ADMIN_MEMBERS, 'mID=' . $_GET['mID'] . setPage()) . '\'">' . IMAGE_CANCEL . '</button>');
                break;
              default:
               if($_GET['action'] == '') {
                if ( isset($mInfo) && is_object($mInfo) ) {              
                  $heading[]  = array('text' => TEXT_INFO_HEADING_DEFAULT);
                  $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">'. $cre_RCI->put('administrator', 'memberdeletebtn', '<button class="btn btn-success btn-sm mt-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_ADMIN_MEMBERS, 'action=edit_member&mID=' . $mInfo->admin_id . setPage())  . '\'">' . IMAGE_EDIT . '</button>'));
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_FULLNAME . '<span class="sidebar-title ml-2">' . $mInfo->admin_firstname . ' ' . $mInfo->admin_lastname . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_EMAIL . '<span class="sidebar-title ml-2">' . $mInfo->admin_email_address . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_GROUP . '<span class="sidebar-title ml-2">' . $mInfo->admin_groups_name . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_CREATED . '<span class="sidebar-title ml-2">' . $mInfo->admin_created . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_MODIFIED . '<span class="sidebar-title ml-2">' . $mInfo->admin_modified . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_LOGDATE . '<span class="sidebar-title ml-2">' . $mInfo->admin_logdate . '</span></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_LOGNUM . '<span class="sidebar-title ml-2">' . $mInfo->admin_lognum . '</span></div>');
                  $contents[] = array('text' => '<br>');
                } elseif ( isset($gInfo) && is_object($gInfo) ) {
                  $heading[] = array('text' => TEXT_INFO_HEADING_DEFAULT_GROUPS);
                  $contents[] = array('align' => 'center', 'text' => $cre_RCI->put('administrator', 'groupdeletebtn', '<button class="btn btn-success btn-sm mr-2 mt-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_ADMIN_MEMBERS, 'gID=' . $gInfo->admin_groups_id . '&action=edit_group') . '\'">' . IMAGE_EDIT . '</button>'));
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-grey btn-sm mt-2 btn-permission" onclick="window.location=\'' . tep_href_link(FILENAME_ADMIN_MEMBERS, 'gPath=' . $gInfo->admin_groups_id . '&action=define_group') . '\'">' . IMAGE_FILE_PERMISSIONS . '</button>');
                  $contents[] = array('text' => '<div class="mt-2">' . TEXT_INFO_DEFAULT_GROUPS_INTRO . '</div>');
                }
              }
            }
			// RCI code start
			$cre_RCI->get('administrator', 'pageleftcol', false);
			// RCI code eof

            if ( tep_not_null($contents) ) {
              $box = new box;
              echo $box->showSidebar($heading, $contents);
            }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
