<?php
/*
  $Id: cache.php,v 1.1.1.1 2004/03/04 23:38:12 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/   
require('includes/application_top.php');
$dir_cache = DIR_FS_CATALOG . DIR_FS_CACHE ;
$action = (isset($_GET['action']) ? $_GET['action'] : '');
if (tep_not_null($action)) {
  if ($action == 'reset') {
    tep_reset_cache_block($_GET['block']);
    $messageStack->add_session('cache', ERROR_CACHE_RESET_SUCCESS, 'success');
  }
  tep_redirect(tep_href_link(FILENAME_CACHE));
}
// check if the cache directory exists
if (is_dir($dir_cache)) {
  if (!is_writeable($dir_cache)) $messageStack->add('search', ERROR_CACHE_DIRECTORY_NOT_WRITEABLE, 'error');
} else {
  $messageStack->add('cache', ERROR_CACHE_DIRECTORY_DOES_NOT_EXIST, 'error');
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('cache') > 0) {
      echo $messageStack->output('cache'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-cache" class="table-cache">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                 <td scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CACHE; ?></td>
                 <td scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_DATE_CREATED; ?></td>
                 <td scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                </tr>
              </thead>
              <tbody> 
              <?php
              if (isset($messageStack) && is_object($messageStack)) {
                $languages = tep_get_languages();
                // get default template
                if (isset($cptemplate1) && is_array($cptemplate1) && tep_not_null($cptemplate1['template_selected'])) {
                  define('TEMPLATE_NAME', $cptemplate['template_selected']);
                } else if  (tep_not_null(DEFAULT_TEMPLATE)) {
                  define('TEMPLATE_NAME', DEFAULT_TEMPLATE);  
                } else {
                  define('TEMPLATE_NAME', 'default');
                }
                $template_query = tep_db_query("select tp.template_name from " . TABLE_TEMPLATE . " tp order by tp.template_name");
                while ($template = tep_db_fetch_array($template_query)) {
                  $template_array[] = array('template' => $template['template_name']);
                }
                for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                  if ($languages[$i]['code'] == DEFAULT_LANGUAGE) {
                    $language = $languages[$i]['directory'];
                  }
                }
                for ($i=0, $n=sizeof($cache_blocks); $i<$n; $i++) {
                  $cached_file = preg_replace('/.language/', '.' . $language, $cache_blocks[$i]['file']);
                  for ($j=0, $k=sizeof($template_array); $j<$k; $j++) {
                    $cached_file_unlink = preg_replace('/-TEMPLATE_NAME/', '-' . $template_array[$j]['template'] , $cached_file);
                    if (file_exists($dir_cache . $cached_file_unlink)) {
                      $cache_mtime = strftime(DATE_TIME_FORMAT, filemtime($dir_cache . $cached_file_unlink));
                    } else {
                      $cache_mtime = TEXT_FILE_DOES_NOT_EXIST;
                      $dir = dir($dir_cache);
                      $cache_file = (isset($cache_file) ? $cache_file : '' );
                      while ($cached_file_unlink = $dir->read()) {
                        if (isset($cache_file) && preg_match('/^/' . $cached_file, $cache_file)) {
                          $cache_mtime = strftime(DATE_TIME_FORMAT, filemtime($dir_cache . $cached_file_unlink));
                          break;
                        }
                      }
                    }
                    $dir->close();
                  }
                  ?>
                  <tr class="table-row dark" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
                    <td class="table-col dark text-left"><?php echo $cache_blocks[$i]['title']; ?></td>
                    <td class="table-col dark text-right"><?php echo $cache_mtime; ?></td>
                    <td class="table-col dark text-right"><?php echo '<a title="' . TEXT_CLEAR_CACHE . '" href="' . tep_href_link(FILENAME_CACHE, 'action=reset&block=' . $cache_blocks[$i]['code'], 'NONSSL') . '"><i class="fa fa-refresh fa-2x text-success"></i></a>'; ?>&nbsp;</td>
                  </tr>
                  <?php
                }
              }
              ?>
              </tbody>
            </table>
			      <div class="col-md-12 col-xl-12 sidebar-title mb-2"><?php echo TEXT_CACHE_DIRECTORY . ' ' . $dir_cache; ?></div>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
