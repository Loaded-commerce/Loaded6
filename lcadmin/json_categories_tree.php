<?php
require('includes/application_top.php');
function haschild($cateid){
      $category_query = tep_db_query("select c.categories_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = cd.categories_id and cd.language_id = '".(int)$languages_id."' and parent_id = '".(int)$cateid."'");
      if(tep_db_num_rows($category_query) > 0){
      	return true;
      }else{
      	return false;
      }
}
$category_tree_array = array();
$counter=0;
$category_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = cd.categories_id and cd.language_id = '".(int)$languages_id."' and parent_id = '0'");
while ($categories = tep_db_fetch_array($category_query)) {
  if(haschild($categories['categories_id'])){
	$category_tree_array[$counter] = array('id' => $categories['categories_id'], 'text' => $categories['categories_name'], 'state' => 'closed');
  }else{
	$category_tree_array[$counter] = array('id' => $categories['categories_id'], 'text' => $categories['categories_name']);
  }
  $category_query_child = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = cd.categories_id and cd.language_id = '".(int)$languages_id."' and parent_id = '".(int)$categories['categories_id']."'");
  $secondsubscounter = 0;
  while ($categories_child = tep_db_fetch_array($category_query_child)) {
	  if(haschild($categories_child['categories_id'])){
		  $category_tree_array[$counter]['children'][$secondsubscounter] = array('id' => $categories_child['categories_id'], 'text' => $categories_child['categories_name'], 'state' => 'closed');
	  }else{
		  $category_tree_array[$counter]['children'][$secondsubscounter] = array('id' => $categories_child['categories_id'], 'text' => $categories_child['categories_name']);
	  }
	  $category_query_child_2 = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = cd.categories_id and cd.language_id = '".(int)$languages_id."' and parent_id = '".(int)$categories_child['categories_id']."'");
	  $countersub = 0;
	  while ($categories_child_2 = tep_db_fetch_array($category_query_child_2)) {
		 if(haschild($categories_child_2['categories_id'])){
			$category_tree_array[$counter]['children'][$secondsubscounter]['children'][$countersub] = array('id' => $categories_child_2['categories_id'], 'text' => $categories_child_2['categories_name'], 'state' => 'closed');
		 }else{
			$category_tree_array[$counter]['children'][$secondsubscounter]['children'][$countersub] = array('id' => $categories_child_2['categories_id'], 'text' => $categories_child_2['categories_name']);
		 }
		 $countersub++;
	  }
	   $secondsubscounter++;
  }

$counter++;
}

//echo '<pre>'; print_r($category_tree_array);
$josn = json_encode($category_tree_array,1);
echo $josn;

?>