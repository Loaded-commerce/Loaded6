<?php
/*
  $Id: configuration.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
Header("Cache-control: private, no-cache");
Header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); # Past date
Header("Pragma: no-cache");
require('includes/application_top.php');

  // RCI for global and individual top
  echo $cre_RCI->get('global', 'top', false);
  echo $cre_RCI->get('configuration', 'top', false); 
  
// local dir to the template directory where you are uploading the company logo
$template_query = tep_db_query("select configuration_id, configuration_title, configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'DEFAULT_TEMPLATE'");
$template = tep_db_fetch_array($template_query);
$CURR_TEMPLATE = $template['configuration_value'] . '/';
$upload_fs_dir = DIR_FS_TEMPLATES.$CURR_TEMPLATE.DIR_WS_IMAGES;
$upload_ws_dir = DIR_WS_CATALOG_TEMPLATES.$CURR_TEMPLATE.DIR_WS_IMAGES;

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'save':
      $configuration_value = tep_db_prepare_input($_POST['configuration_value']);
      $cID = tep_db_prepare_input($_GET['cID']);
      $error = false;
      $configuration_key = tep_db_prepare_input($_POST['configuration_key']);
      // check if configuration key is admin session lifetime and greater than zero
      if ($configuration_key == 'MYSESSION_LIFETIME') {
        if ((int)$configuration_value < 60) {
          $error = true;
          $messageStack->add_session('search',CONFIG_ADMIN_SESSION_ERROR, 'error');
          tep_redirect(tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cID . '&action=edit'));
        }
      }
      // added for password length validation for pci.
      if ($configuration_key == 'ENTRY_PASSWORD_MIN_LENGTH') {
        if ((int)$configuration_value < 8) {
          $error = true;
          $messageStack->add_session('search', CONFIG_ADMIN_PASSWORD_ERROR, 'error');
          tep_redirect(tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cID . '&action=edit'));
        }
      }
      if ($error == false) {
        if (is_array($configuration_value)) {
          $configuration_value_new = '';
          foreach ($configuration_value as $value) {
            $configuration_value_new .= $value . ',';
          }
          $configuration_value_new = substr($configuration_value_new, 0, strlen($configuration_value_new) - 1);//B# 3836
          tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value_new) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");
        } else {
          tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");
        }
        tep_redirect(tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cID));
      }
      break;
  }
}
$gID = (isset($_GET['gID'])) ? $_GET['gID'] : 1;
$cfg_group_query = tep_db_query("select configuration_group_title from " . TABLE_CONFIGURATION_GROUP . " where configuration_group_id = '" . (int)$gID . "'");
$cfg_group = tep_db_fetch_array($cfg_group_query);


include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo $cfg_group['configuration_group_title']; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('currencies') > 0) {
      echo $messageStack->output('currencies'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-configuration" class="table-configuration">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                 <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CONFIGURATION_TITLE; ?></th>
                 <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CONFIGURATION_VALUE; ?></th>
                 <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
                </tr>
              <?php
              $configuration_query = tep_db_query("select configuration_id, configuration_title, configuration_value, use_function from " . TABLE_CONFIGURATION . " where configuration_group_id = '" . (int)$gID . "' order by sort_order");
              while ($configuration = tep_db_fetch_array($configuration_query)) {
                if (tep_not_null($configuration['use_function'])) {
                  $use_function = $configuration['use_function'];
                  if (preg_match('/->/', $use_function)) {
                    $class_method = explode('->', $use_function);
                    if (!isset(${$class_method[0]}) || !is_object(${$class_method[0]})) {
                      include(DIR_WS_CLASSES . $class_method[0] . '.php');
                      ${$class_method[0]} = new $class_method[0]();
                    }
                    $cfgValue = tep_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
                  } else {
                    $cfgValue = tep_call_function($use_function, $configuration['configuration_value']);
                  }
                } else {
                  $cfgValue = $configuration['configuration_value'];
                }
                if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $configuration['configuration_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
                  $cfg_extra_query = tep_db_query("select configuration_key, configuration_description, date_added, last_modified, use_function, set_function from " . TABLE_CONFIGURATION . " where configuration_id = '" . (int)$configuration['configuration_id'] . "'");
                  $cfg_extra = tep_db_fetch_array($cfg_extra_query);
                  $cInfo_array = array_merge((array)$configuration, (array)$cfg_extra);
                  $cInfo = new objectInfo($cInfo_array);
                }

                $selected = ((isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id)) ? true : false;
                if ( $selected ) {
                  if($cInfo->set_function == 'file_upload'){
                    echo '<tr class="table-row dark selected" role="row" onclick="document.location.href=\'' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . (int) $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=upload') . '\'">' . "\n";
                  } else {   
                    echo '<tr class="table-row dark selected" role="row" onclick="document.location.href=\'' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . (int) $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=edit') . '\'">' . "\n";
                  }
                } else {
                  echo '<tr class="table-row dark" role="row" onclick="document.location.href=\'' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . (int) $_GET['gID'] . '&cID=' . $configuration['configuration_id'] . '&action=edit') . '\'">' . "\n";
                }
                $col_selected = ($selected) ? ' selected' : '';
                ?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $configuration['configuration_title']; ?></td>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php 
                  if ($_GET['gID']== '450' && $configuration['configuration_title'] == 'Download Order Statuses') {
                    $s1 = tep_db_query("SELECT * FROM `orders_status` WHERE `orders_status_id` IN ( ".htmlspecialchars($cfgValue)." ) and language_id = '".$languages_id."'");
                    $s2 = '';
                    while($r1 = tep_db_fetch_array($s1)) {
                      $s2 .= $r1['orders_status_name']. ", ";
                    }
                    echo substr($s2,0,strlen($s2)-2);
                  } else {
                    echo htmlspecialchars($cfgValue); 
                  }
                ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank"><?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_CONFIGURATION, 'gid=' . (int) $_GET['gID'] . '&cID=' . $configuration['configuration_id'].'&action=edit') . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
              </tr>
              <?php
              }
              ?>   
            </table>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">            
            <?php
            $heading = array();
            $contents = array();

            switch ($action) {
              case 'edit':
                $heading[] = array('text' => $cInfo->configuration_title);
                if ($cInfo->set_function) {
                  eval('$value_field = ' . $cInfo->set_function . '"' . htmlspecialchars($cInfo->configuration_value) . '");');
                } else {
                  $value_field = tep_draw_input_field('configuration_value', $cInfo->configuration_value, 'class="form-control"');
                }
                $contents[] = array('form' => tep_draw_form('configuration', FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=save'));
               // $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
                $contents[] = array('text' => tep_draw_hidden_field('configuration_key', $cInfo->configuration_key) . '<label class="control-label sidebar-edit mt-3">' . $cInfo->configuration_title . '</label><br/><span class="sidebar-title">' . $cInfo->configuration_description . '<br/>' . $value_field .'</span>'); // VJ admin session changed
                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-submit" type="submit">' . IMAGE_UPDATE .'</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id) . '">' . IMAGE_CANCEL . '</a> </div>');
                break;
                
              default:
                if (isset($cInfo) && is_object($cInfo)) {
                  $heading[] = array('text' => $cInfo->configuration_title);
                  if ($cInfo->set_function == 'file_upload') {
                    $contents[] = array('align' => 'center', 'text' => '<a class="btn btn-success btn-sm mt-2 btn-edit" href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=upload') . '">' . IMAGE_EDIT . '</a>');
                    $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-3 mb-4 btn-edit" type="submit">'. IMAGE_EDIT.'</button>');
                  } else {
                    $contents[] = array('align' => 'center', 'text' => '<a class="btn btn-success btn-sm mt-2 btn-edit" href="' . tep_href_link(FILENAME_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=edit') . '">' . IMAGE_EDIT . '</a>');
                  }
                  $contents[] = array('text' => '<label class="control-label sidebar-edit mt-3 text-left">'.$cInfo->configuration_description.'</label>');
                  if ($cInfo->configuration_id == 6) {
                    $a = tep_db_query("select configuration_value from configuration where configuration_id='".$cInfo->configuration_id."' ");
                    $result = tep_db_fetch_array($a);
                    $value = $result['configuration_value'];

                    $s = tep_db_query("select * from zones where zone_id='".$value."' ");
                    $result1 = tep_db_fetch_array($s);
                    $s_zone_name = $result1['zone_name'];
                    $contents[] = array('text' => '<span class="sidebar-title">' . $s_zone_name.'</span>');
                  } else {
                  $contents[] = array('text' => '<span class="sidebar-title">' . $cInfo->configuration_value.'</span>');
                  }
                  $contents[] = array('text' => '<div class="sidebar-text mt-1">'.TEXT_INFO_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->date_added) . '</span></div>');
                  if (tep_not_null($cInfo->last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->last_modified) . '</span></div>');
                }
                break;
            }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>
      <?php
      // RCI for global and individual bottom
      echo $cre_RCI->get('configuration', 'bottom'); 
      echo $cre_RCI->get('global', 'bottom');  
      ?>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<!-- // for link on free shipping module --> 
<script language="javascript">
<!--
function free_shipping_module_page() {
  window.location = "<?php echo str_replace('&amp;', '&', tep_href_link(FILENAME_MODULES, 'selected_box=modules&set=shipping&module=freeshipper', 'SSL')); ?>";
}
-->
</script>      
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
