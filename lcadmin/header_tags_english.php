<?php
/*
  $Id: header_tags_controller.php,v 1.0 2005/04/08 22:50:52 hpdl Exp $
  Originally Created by: Jack York - http://www.oscommerce-solution.com
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
 
  require('includes/application_top.php');
  require_once('includes/functions/header_tags.php');
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_HEADER_TAGS_CONTROLLER);
  $filename = DIR_FS_CATALOG. DIR_WS_LANGUAGES . $language . '/header_tags.php';
  if (GetPermissions(DIR_FS_CATALOG_IMAGES) != Getpermissions($filename))
   $messageStack->add('header_tags_english', "Permissions settings for the $filename file appear to be incorrect. Change to " . Getpermissions(DIR_WS_IMAGES));  
   
  $formActive = false;
  
  /****************** READ IN FORM DATA ******************/
  $action = (isset($_POST['action']) ? $_POST['action'] : '');
  
  if (tep_not_null($action)) 
  {
      $main = array();
      $main['title'] = htmlspecialchars($_POST['main_title']);  //read in the knowns
      $main['desc'] = htmlspecialchars($_POST['main_desc']);
      $main['keyword'] = htmlspecialchars($_POST['main_keyword']);

      $formActive = true;
      $args_new = array();
      $c = 0;
      $pageCount = TotalPages($filename);
      for ($t = 0, $c = 0; $t < $pageCount; ++$t, $c += 3) //read in the unknowns
      {
         $args_new['title'][$t] = htmlspecialchars(stripslashes($_POST[$c]), ENT_QUOTES);
         $args_new['desc'][$t] = htmlspecialchars(stripslashes($_POST[$c+1]), ENT_QUOTES);
         $args_new['keyword'][$t] = htmlspecialchars(stripslashes($_POST[$c+2]), ENT_QUOTES);
        
         $boxID = sprintf("HTTA_%d", $t); 
         $args_new['HTTA'][$t] = $_POST[$boxID];
         $boxID = sprintf("HTDA_%d", $t); 
         $args_new['HTDA'][$t] = $_POST[$boxID];
         $boxID = sprintf("HTKA_%d", $t); 
         $args_new['HTKA'][$t] = $_POST[$boxID];
         $boxID = sprintf("HTCA_%d", $t); 
         $args_new['HTCA'][$t] = $_POST[$boxID];
         $boxID = sprintf("HTPA_%d", $t); 
         $args_new['HTPA'][$t] = $_POST[$boxID];
      }   
  }

  /***************** READ IN DISK FILE ******************/
  $main_title = '';
  $main_desc = '';
  $main_key = '';
  $sections = array();      //used for unknown titles
  $args = array();          //used for unknown titles
  $ctr = 0;                 //used for unknown titles
  $findTitles = false;      //used for unknown titles
  $fp = file($filename);  

  for ($idx = 0; $idx < count($fp); ++$idx)
  { 
      if (strpos($fp[$idx], "define('HEAD_TITLE_TAG_ALL'") !== FALSE)
      {
//      echo 'SEND TITLE '.$main_title.' '. ' - '.$main['title'].' - '.$formActive.'<br>';
          $main_title = GetMainArgument($fp[$idx], $main['title'], $formActive);
      } 
      else if (strpos($fp[$idx], "define('HEAD_DESC_TAG_ALL'") !== FALSE)
      {
     // echo 'SEND DESC '.$main['desc']. ' '.$formActive.'<br>';
          $main_desc = GetMainArgument($fp[$idx], $main['desc'], $formActive);
      } 
      else if (strpos($fp[$idx], "define('HEAD_KEY_TAG_ALL'") !== FALSE)
      { 
          $main_key = GetMainArgument($fp[$idx], $main['keyword'], $formActive);
          $findTitles = true;  //enable next section            
      } 
      else if ($findTitles)
      {
          if (($pos = strpos($fp[$idx], '.php')) !== FALSE) //get the section titles
          {
              $sections['titles'][$ctr] = GetSectionName($fp[$idx]);   
              $ctr++; 
          }
          else                                   //get the rest of the items in this section
          {
              if (! IsComment($fp[$idx])) // && tep_not_null($fp[$idx]))
              {
                  $c = $ctr - 1;
                  if (IsTitleSwitch($fp[$idx]))
                  {
                     if ($formActive)
                     {
                       $fp[$idx] = ChangeSwitch($fp[$idx], $args_new['HTTA'][$c]);
                     }                      
                     $args['title_switch'][$c] = GetSwitchSetting($fp[$idx]);
                     $args['title_switch_name'][$c] = sprintf("HTTA_%d",$c);                     
                  }
                  else if (IsDescriptionSwitch($fp[$idx]))
                  {
                     if ($formActive)
                     {
                       $fp[$idx] = ChangeSwitch($fp[$idx], $args_new['HTDA'][$c]);
                     } 
                     $args['desc_switch'][$c] = GetSwitchSetting($fp[$idx]);
                     $args['desc_switch_name'][$c] = sprintf("HTDA_%d",$c);  
                  }
                  if (IsKeywordSwitch($fp[$idx]))
                  {
                     if ($formActive)
                     {
                       $fp[$idx] = ChangeSwitch($fp[$idx], $args_new['HTKA'][$c]);
                     }   
                     $args['keyword_switch'][$c] = GetSwitchSetting($fp[$idx]);
                     $args['keyword_switch_name'][$c] = sprintf("HTKA_%d",$c);
                  }
                  else if (IsCatSwitch($fp[$idx]))
                  {
                     if ($formActive)
                     {
                       $fp[$idx] = ChangeSwitch($fp[$idx], $args_new['HTCA'][$c]); 
                     }  
                     $args['cat_switch'][$c] = GetSwitchSetting($fp[$idx]);
                     $args['cat_switch_name'][$c] = sprintf("HTCA_%d",$c);
                  }
                  else if (IsCatProdSwitch($fp[$idx]))     //special case - for product_info only
                  {
                     if ($formActive)
                     {
                       $fp[$idx] = ChangeSwitch($fp[$idx], $args_new['HTPA'][$c]); 
                     }  
                     $args['catprod_switch'][$c] = GetSwitchSetting($fp[$idx]);
                     $args['catprod_switch_name'][$c] = sprintf("HTPA_%d",$c);
                  }
                  else if (IsTitleTag($fp[$idx]))
                  {
                     $args['title'][$c] = GetArgument($fp[$idx], $args_new['title'][$c], $formActive);
                  } 
                  else if (IsDescriptionTag($fp[$idx])) 
                  {
                     $args['desc'][$c] = GetArgument($fp[$idx], $args_new['desc'][$c], $formActive);                   
                  }
                  else if (IsKeywordTag($fp[$idx])) 
                  {
                    $args['keyword'][$c] = GetArgument($fp[$idx], $args_new['keyword'][$c], $formActive);
                  }                                   
              }
          }
      }
  }

  /***************** WRITE THE FILE ******************/
  if ($formActive)
  {      
     WriteHeaderTagsFile($filename, $fp);  
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE_ENGLISH; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <!-- begin button bar --> 
    <div id="button-bar" class="row">
      <div class="col-9 m-b-10 w-100 pt-1 pl-0 pr-0"> 
        <button type="button" onclick="updateTags();" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo IMAGE_UPDATE; ?></button>
      </div>
      <div class="col-3 m-b-10 pt-1 pr-2">
      </div>
    </div>
    <!-- end button bar -->    
    <?php
    if ($messageStack->size('header_tags_english') > 0) {
      echo $messageStack->output('header_tags_english'); 
    }
    ?>  
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-headertagsenglish" class="table-headertagsenglish">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">
            <div class="sidebar-title mt-2">
              <div class="row">
                <div class="col p-0 mt-3 ml-2 mr-2">
                  <div class="note note-warning m-0">
                    <p class="mb-0 mt-0 f-w-400"><?php echo TEXT_ENGLISH_TAGS; ?></p>
                  </div>
                </div>
              </div>
            </div>
            <table border="0" width="100%" cellspacing="0" cellpadding="2" class="data-table">
              <!-- Begin of Header Tags -->
              <tr>
                <td align="right"><?php echo tep_draw_form('header_tags', FILENAME_HEADER_TAGS_ENGLISH, '', 'post') . tep_draw_hidden_field('action', 'process'); ?></td>
                  <tr>
                    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="20%"><label class="control-label sidebar-edit mb-0"><?php echo HEADING_TITLE_CONTROLLER_DEFAULT_TITLE; ?></label></td>
                        <td><?php echo tep_draw_input_field('main_title', tep_not_null($main_title) ? $main_title : '', 'class="form-control mb-1" maxlength="255", size="60"', false); ?> </td>
                      </tr> 
                      <tr>
                        <td><label class="control-label sidebar-edit mb-0"><?php echo HEADING_TITLE_CONTROLLER_DEFAULT_DESCRIPTION; ?></label></td>
                        <td><?php echo tep_draw_input_field('main_desc', tep_not_null($main_desc) ? $main_desc : '', 'class="form-control mb-1" maxlength="255", size="60"', false); ?> </td>
                      </tr> 
                      <tr>
                        <td><label class="control-label sidebar-edit mb-0"><?php echo HEADING_TITLE_CONTROLLER_DEFAULT_KEYWORDS; ?></label></td>
                        <td><?php echo tep_draw_input_field('main_keyword', tep_not_null($main_key) ? $main_key : '', 'class="form-control mb-1" maxlength="255", size="60"', false); ?> </td>
                      </tr> 
                 
                      <?php 
                      for ($i = 0, $id = 0; $i < count($sections['titles']); ++$i, $id += 3) { 
                        ?>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                          <td colspan="3"><table border="0" width="100%">
                            <tr>
                              <td colspan="3" width="20%">
                                <?php 
                                if(tep_not_null($sections['titles'][$i])) {
                                  ?>
                                  <span class="lead label label-warning mb-0 f-s-12"><?php echo $sections['titles'][$i]; ?></span>
                                  <?php 
                                } 
                                ?>
                              </td>
                              <td class="sidebar-title">HTTA: </td>
                              <td align="left" width="5%"><?php echo tep_draw_checkbox_field($args['title_switch_name'][$i], '', $sections['titles'][$i], '', 'class="js-switch"'); ?> </td>
                              <td class="sidebar-title">HTDA: </td>
                              <td width="5%"><?php echo tep_draw_checkbox_field($args['desc_switch_name'][$i], '', $args['desc_switch'][$i], '', 'class="js-switch mr-2"'); ?> </td>
                              <td class="sidebar-title">HTKA: </td>
                              <td width="5%"><?php echo tep_draw_checkbox_field($args['keyword_switch_name'][$i], '', $args['keyword_switch'][$i], '', 'class="js-switch mr-2"'); ?> </td>
                              <td class="sidebar-title">HTCA: </td>
                              <td width="5%"><?php echo tep_draw_checkbox_field($args['cat_switch_name'][$i], '', $args['cat_switch'][$i], '', 'class="js-switch mr-2"'); ?> </td>
                              <?php 
                              if ($sections['titles'][$i] == "product_info") { 
                                ?>
                                <td class="sidebar-title">HTPA: </td>
                                <td align="left" width="5%"><?php echo tep_draw_checkbox_field($args['catprod_switch_name'][$i], '', $args['catprod_switch'][$i], '', 'class="js-switch"'); ?> </td>
                                <?php 
                              } 
                              ?>                    
                              <td width="50%"> <a href="#modal-explain" class="btn btn-sm btn-info" data-toggle="modal"><?php echo HEADING_TITLE_CONTROLLER_EXPLAIN; ?></a></td>
                            </tr>
                          </table></td>
                        </tr>

                 



                        <tr>
                          <td colspan="3"><table border="0" width="100%">
                            <tr>
                              <td width="2%">&nbsp;</td>
                              <td width="12%"><label class="control-label sidebar-edit mb-0"><?php echo HEADING_TITLE_CONTROLLER_TITLE; ?></label></td>
                              <td><?php echo tep_draw_input_field($id, $args['title'][$i], 'class="form-control mb-1" maxlength="255", size="60"', false, 300); ?> </td>
                            </tr>
                            <tr>
                              <td width="2%">&nbsp;</td>
                              <td width="12%"><label class="control-label sidebar-edit mb-0"><?php echo HEADING_TITLE_CONTROLLER_DESCRIPTION; ?></label></td>
                              <td><?php echo tep_draw_input_field($id+1, $args['desc'][$i], 'class="form-control mb-1" maxlength="255", size="60"', false); ?> </td>
                            </tr>
                            <tr>
                              <td width="2%">&nbsp;</td>
                              <td width="12%"><label class="control-label sidebar-edit mb-0"><?php echo HEADING_TITLE_CONTROLLER_KEYWORDS; ?></label></td>
                              <td><?php echo tep_draw_input_field($id+2, $args['keyword'][$i], 'class="form-control mb-1" maxlength="255", size="60"', false); ?> </td>
                            </tr>
                          </table></td>
                        </tr>
                        <?php 
                      } 
                      ?> 
                    </table></td>
                  </tr>  
                  <tr> 
                    <td align="center"><button class="btn btn-success btn-sm mt-3 mb-3" type="submit"><?php echo IMAGE_UPDATE; ?></button></td>
                  </tr>
                </form></td>
              </tr>
              <!-- end of Header Tags -->           
            </table></td>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<!-- #modal-dialog -->
<div class="modal fade" id="modal-explain">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">What are HTTA, HTDA, HTKA and HTCA used for?</h4>
        <button type="button" class="close modal-close-fix" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>         
			</div>
			<div class="modal-body">
				<?php include_once(DIR_WS_LANGUAGES . $language . '/header_tags_popup_help.php'); ?>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-sm btn-black" data-dismiss="modal">Close</a>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){

  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small', 
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  }); 
}); 

function updateTags() {
  document.forms["header_tags"].submit();
}

// sticky menu bar
$(function () {
  var y = 60;
  $(window).on('scroll', function () {
    if (y <= $(window).scrollTop()) {
      // if so, add the fixed class
      $('#button-bar').addClass('button-bar-fixed');
    } else {
      // otherwise remove it
      $('#button-bar').removeClass('button-bar-fixed');
    }
  })
});   
</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
