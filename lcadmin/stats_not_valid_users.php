<?php
/*
  $Id: stats_inactive_user.php,v 1.2 2004/05/02 15:00:00
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2004 osCommerce

  Released under the GNU General Public License
  Created by John Wood - www.z-is.net
  Modified by Steel Shadow - rebelstyle.com

*/

 require('includes/application_top.php');

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
 ?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
 
  <div class="col">   
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-statsproductspurchased" class="table-statsproductspurchased">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">




<?php
  $go = (isset($_GET['go']) ? $_GET['go'] : '' );
  $id = (isset($_GET['id']) ? $_GET['id'] : '' );
$cust_query = tep_db_query("select customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " where customers_id = '" . $id . "'");
$cust = tep_db_fetch_array($cust_query);

if ($go == ''){
?>
      <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_ID; ?></th>
				  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_CUSTOMERS; ?></th>
				  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_EMAIL; ?></th>
				  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_ACCOUNT_CREATED; ?></th>
				  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_LAST_LOGON; ?></th>
				  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_DELETE; ?></th>
				</tr>
              </thead>
<?php
  $siu_query_raw = "select * from " . TABLE_CUSTOMERS_INFO . " ci, " . TABLE_CUSTOMERS . " c where c.customers_id = ci.customers_info_id and c.customers_validation = '0' order by c.customers_id";
  $siu_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $siu_query_raw, $siu_query_numrows );
  $siu_query = tep_db_query($siu_query_raw);
  while ($customers = tep_db_fetch_array($siu_query)) {

 ?>
      <tr> 
        <td class="table-col dark text-left"><?php echo $customers['customers_id'];?></td>
        <td class="table-col dark text-left"><?php echo $customers['customers_firstname'] . ' ' . $customers['customers_lastname'];?></td>
        <td class="table-col dark text-left"><?php echo '<a href="mailto:' . $customers['customers_email_address'] . '"><u>' . $customers['customers_email_address'] . '</u></a>' ; ?></td>
        <td class="table-col dark text-left"><?php echo tep_date_short($customers['customers_info_date_account_created']); ?></td>
        <td class="table-col dark text-left"><?php echo tep_date_short($customers['customers_info_date_of_last_logon']); ?></td>
        <td class="table-col dark text-left"><?php echo '<a href="' . tep_href_link(FILENAME_STATS_NOT_VALID_USER, 'go=delete&id=' . $customers['customers_id'] . '&page=' . $_GET['page']) .'">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>'; ?></td>
      </tr>
<?php
  }
?>
<?php
            } elseif ($_GET['go'] == 'delete')
      {
              echo '<br>' . sprintf(SURE_TO_DELETE, $cust[customers_firstname] . ' ' . $cust[customers_lastname]) . '<br><br><a href="' . tep_href_link(FILENAME_STATS_NOT_VALID_USER,  'page=' . $_GET['page'] . '&go=deleteyes&id=' . $_GET['id']) . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_STATS_NOT_VALID_USER, 'page=' . $_GET['page']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a><br><br>';
            } elseif ($_GET['go'] == 'deleteyes'){
      tep_db_query("DELETE FROM " . TABLE_CUSTOMERS . " where customers_id = '" . $_GET['id'] . "'");
      tep_db_query("DELETE FROM " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . $_GET['id'] . "'");
      tep_db_query("DELETE FROM " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . $_GET['id'] . "'");
      tep_db_query("DELETE FROM " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . $_GET['id'] . "'");
      tep_db_query("DELETE FROM " . TABLE_ADDRESS_BOOK . " where customers_id = '" . $_GET['id'] . "'");
      tep_db_query("DELETE FROM " . TABLE_PRODUCTS_NOTIFICATIONS . " where customers_id = '" . $_GET['id'] . "'");
      echo '<br>' . sprintf(SIU_CUSTOMER_DELETED, $cust[customers_firstname] . ' ' . $cust[customers_lastname]) . '<br><br><br><a href="' . tep_href_link(FILENAME_STATS_NOT_VALID_USER, 'page=' . $_GET['page']) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a><br><br>';
    }
        
?>
       </table>
    
  <?php if ($go == ''){?>   
       <table border="0" width="100%" cellspacing="0" cellpadding="0" class="data-table-foot">
          <tr>
            <td class="smallText" valign="top"><?php echo $siu_split->display_count($siu_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></td>
            <td class="smallText" align="right"><?php echo $siu_split->display_links($siu_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'info', 'x', 'y', 'cID'))); ?></td>
          </tr>
       </table>
  <?php }?> 
  

			<div class="mb-1">&nbsp;</div>
		 </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>  