<?php
/*
  $Id: gv_mail.php,v 1.1.1.1 2004/03/04 23:38:35 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
$currency_value = $currencies->currencies[$currency]['value'];
function tep_get_current_language($language_id) {
  $languages_query = tep_db_query("select languages_id, name, code, image, directory from " . TABLE_LANGUAGES . " where languages_id = '" . $language_id . "' order by sort_order");
  while ($languages = tep_db_fetch_array($languages_query)) {
    $languages_array1 = array('id' => $languages['languages_id'],
                              'name' => $languages['name'],
                              'code' => $languages['code'],
                              'image' => $languages['image'],
                              'directory' => $languages['directory']);
  }
  return $languages_array1;
}
$languages = tep_get_languages();
$cur_language = tep_get_current_language($languages_id);
$languages_name = $cur_language['name'];
$languages_image = $cur_language['image'];
$languages_directory = $cur_language['directory'];
if ( (isset($_GET['action']) && $_GET['action'] == 'send_email_to_user') && ($_POST['customers_email_address'] || $_POST['email_to']) && (!$_POST['back_x']) ) {
  switch ($_POST['customers_email_address']) {
    case '***':
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS);
      $mail_sent_to = TEXT_ALL_CUSTOMERS;
      break;
    case '**D':
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_newsletter = '1'");
      $mail_sent_to = TEXT_NEWSLETTER_CUSTOMERS;
      break;
    default:
      $customers_email_address = strtolower(tep_db_prepare_input($_POST['customers_email_address']));
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where lower(customers_email_address) = '" . tep_db_input($customers_email_address) . "'");
      $mail_info = tep_db_fetch_array($mail_query);
      $mail_to_name = $mail_info['customers_firstname'] . ' ' .  $mail_info['customers_lastname'];
      $mail_sent_to = strtolower($_POST['customers_email_address']);
      if (isset($_POST['email_to']) && $_POST['email_to'] != '') {
        $mail_sent_to = strtolower($_POST['email_to']);
      }
      break;
  }
  $from = tep_db_prepare_input($_POST['from']);
  $subject = tep_db_prepare_input($_POST['subject']);
  $_POST['amount'] = str_replace($currencies->currencies[DEFAULT_CURRENCY]['symbol_left'], '', $_POST['amount']);
  $_POST['amount'] = str_replace($currencies->currencies[DEFAULT_CURRENCY]['symbol_right'], '', $_POST['amount']);
  $_POST['amount'] = trim($_POST['amount']);
  //==============  GSR START ==============
  $coupon_type = 'G';
  if (substr($_POST['amount'], -1) == '%') $coupon_type='P';
  //==============  GSR END ==============
  if (isset($mail_sent_to) && $mail_sent_to != '') {
    if (EMAIL_USE_HTML == 'false') {
      $id1 = create_coupon_code($mail_sent_to);
      $message = tep_db_encoder(tep_db_prepare_input($_POST['message']));
      //==============  GSR START ==============
      //$message .= "\n\n" . TEXT_GV_WORTH  . $currencies->format($_POST['amount'],true,DEFAULT_CURRENCY,$currency_value) . "\n\n";
      $message .= "\n\n" . TEXT_GV_WORTH  . (($coupon_type==P) ? $_POST['amount'] ."%" : $currencies->format($_POST['amount'],true,DEFAULT_CURRENCY,$currency_value)) . "\n\n";
      //==============  GSR END ==============
      $message .= TEXT_TO_REDEEM_TEXT;
      $message .= TEXT_WHICH_IS . $id1 . TEXT_IN_CASE . "\n\n";
      $message .= TEXT_OR_VISIT . '<a href="' .  HTTP_SERVER  . DIR_WS_CATALOG . '">' . HTTP_SERVER  . DIR_WS_CATALOG  . '</a>' . TEXT_ENTER_CODE;
      $message .= TEXT_TO_REDEEM1 ;
      $message .= TEXT_REMEMBER . "\n";
    } else {
      $id1 = create_coupon_code($mail_sent_to);
      $message = tep_db_encoder(tep_db_prepare_input($_POST['message']));
      //==============  GSR START ==============
      //$message .= "\n\n" . TEXT_GV_WORTH  . $currencies->format($_POST['amount'],true,DEFAULT_CURRENCY,$currency_value) . "\n\n";
      $message .= "\n\n" . TEXT_GV_WORTH  . (($coupon_type==P) ? $_POST['amount'] ."%" : $currencies->format($_POST['amount'],true,DEFAULT_CURRENCY,$currency_value)) . "\n\n";
      //==============  GSR END ==============
      $message .= TEXT_TO_REDEEM;
      $message .= TEXT_WHICH_IS . $id1 . TEXT_IN_CASE . "\n\n";
      $message .= '<a href="' . HTTP_SERVER  . DIR_WS_CATALOG . 'gv_redeem.php' . '?gv_no='.$id1 .'">' .  HTTP_SERVER  . DIR_WS_CATALOG . 'gv_redeem.php' . '?gv_no='.$id1 . '</a>' . "\n\n";
      $message .= TEXT_OR_VISIT . '<a href="' .  HTTP_SERVER  . DIR_WS_CATALOG . '">' . HTTP_SERVER  . DIR_WS_CATALOG  . '</a>' . TEXT_ENTER_CODE;
      $message .= TEXT_TO_REDEEM1 ;
      $message .= TEXT_REMEMBER . "\n";
    }

    if (EMAIL_USE_HTML == "false") {
      $email_content = $message;
    } else {
      $email_content['text'] = strip_tags($message);
      $email_content['html'] = nl2br($message);
    }

    if($mail_to_name == '') { $mail_to_name = $mail_sent_to;}
    $sender_query = tep_db_query ("select admin_id, admin_firstname, admin_lastname from " . TABLE_ADMIN . "  where admin_id= " . $_SESSION['login_id']);
    $sender = tep_db_fetch_array($sender_query);
    $sender_name = $sender['admin_firstname'] . ' ' . $sender['admin_lastname'];
    $sender_id = $sender['admin_id'];
    if ($_POST['customers_email_address'] == '***') {
      while($tmp_db = tep_db_fetch_array($mail_query)) {
        $mail_to_name = $tmp_db['customers_firstname']. " ".$tmp_db['customers_lastname'];
        $mail_sent_to = $tmp_db['customers_email_address'];
		tep_mail($mail_to_name, $mail_sent_to, $subject, $email_content, $from, STORE_OWNER_EMAIL_ADDRESS);
      }  
    } else if ($_POST['customers_email_address'] == '**D') {
      while($tmp_db = tep_db_fetch_array($mail_query)) {
        $mail_to_name = $tmp_db['customers_firstname']. " ".$tmp_db['customers_lastname'];
        $mail_sent_to = $tmp_db['customers_email_address'];
		tep_mail($mail_to_name, $mail_sent_to, $subject, $email_content, $from, STORE_OWNER_EMAIL_ADDRESS);
      }  
    } else {
	  tep_mail($mail_to_name, $mail_sent_to, $subject, $email_content, $from, STORE_OWNER_EMAIL_ADDRESS);
    }
    // Now create the coupon email entry
    //==============  GSR START ==============
    //$insert_query = tep_db_query("insert into " . TABLE_COUPONS . " (coupon_code, coupon_type, coupon_amount, date_created) values ('" . $id1 . "', 'G', '" . $_POST['amount'] . "', now())");
    $insert_query = tep_db_query("insert into " . TABLE_COUPONS . " (coupon_code, coupon_type, coupon_amount, date_created) values ('" . $id1 . "', '" . $coupon_type . "', '" . $_POST['amount'] . "', now())");
    $insert_id = tep_db_insert_id();
    //==============  GSR END ==============
    $insert_query = tep_db_query("insert into " . TABLE_COUPON_EMAIL_TRACK . " (coupon_id, customer_id_sent, sent_firstname, emailed_to, date_sent) values ('" . $insert_id ."', '" . $sender_id . "', '" . $sender_name . "', '" . $mail_sent_to . "', now() )");
  }
  tep_redirect(tep_href_link(FILENAME_GV_MAIL, 'mail_sent_to=' . urlencode($mail_sent_to)));
}
if ( (isset($_GET['action']) && $_GET['action'] == 'preview') && (!$_POST['customers_email_address']) && (!$_POST['email_to']) ) {
  $messageStack->add('search', ERROR_NO_CUSTOMER_SELECTED, 'error');
}
if ( (isset($_GET['action']) && $_GET['action'] == 'preview') && (!$_POST['amount']) ) {
  $messageStack->add('search', ERROR_NO_AMOUNT_SELECTED, 'error');
}
if (isset($_GET['mail_sent_to']) && $_GET['mail_sent_to']) {
  $messageStack->add('search', sprintf(NOTICE_EMAIL_SENT_TO, $_GET['mail_sent_to']), 'success');
}


include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');

if (EMAIL_USE_HTML == 'true'){
  include('includes/javascript/editor.php');
  echo tep_load_html_editor();
}
?>
<script language="javascript"><!--
function disableButton(obj) {
  for (z=1; z < obj.length; z++) {
    if (obj[z].type == 'submit') {
      obj[z].disabled = true;
      break;
    }
  }
}
function editnewsletter() {
	document.mail.action = '<?php echo tep_href_link(FILENAME_GV_MAIL)?>';
	document.mail.submit();
}

--></script>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('search') > 0) {
      echo $messageStack->output('search'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-gvsent" class="table-gvsent">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">

          <?php
          if ( (isset($_GET['action']) && $_GET['action'] == 'preview') && ($_POST['customers_email_address'] || $_POST['email_to']) ) {
            switch ($_POST['customers_email_address']) {
              case '***':
                $mail_sent_to = TEXT_ALL_CUSTOMERS;
                break;
              case '**D':
                $mail_sent_to = TEXT_NEWSLETTER_CUSTOMERS;
                break;
              default:
                $mail_sent_to = strtolower($_POST['customers_email_address']);
                if (tep_not_null($_POST['email_to'])) {
                  $mail_sent_to = strtolower($_POST['email_to']);
                }
                break;
            }
            $_POST['amount'] = str_replace($currencies->currencies[DEFAULT_CURRENCY]['symbol_left'], '', $_POST['amount']);
            $_POST['amount'] = str_replace($currencies->currencies[DEFAULT_CURRENCY]['symbol_right'], '', $_POST['amount']);
            $_POST['amount'] = trim($_POST['amount']);
            //==============  GSR START ==============
            $coupon_type = 'G';
            if (substr($_POST['amount'], -1) == '%') $coupon_type='P';
            //==============  GSR END ==============
           
           echo tep_draw_form('mail', FILENAME_GV_MAIL, 'action=send_email_to_user', 'post')."\n";

			/* Re-Post all POST'ed variables */
			reset($_POST);
		    foreach($_POST as $key => $value) {
			  if (!is_array($_POST[$key])) {
				echo tep_draw_hidden_field($key, tep_db_encoder(tep_db_prepare_input($value))) . "\n";
			  }
			}
           ?>
              <table border="0" width="100%" cellpadding="2" cellspacing="2">
                <tr>
                  <td class="smallText"><b><?php echo TEXT_CUSTOMER; ?></b><br><?php echo $mail_sent_to; ?></td>
                </tr>
                <tr>
                  <td class="smallText"><b><?php echo TEXT_FROM; ?></b><br><?php echo tep_db_prepare_input($_POST['from']); ?></td>
                </tr>
                <tr>
                  <td class="smallText"><b><?php echo TEXT_SUBJECT; ?></b><br><?php echo tep_db_encoder(tep_db_prepare_input($_POST['subject'])); ?></td>
                </tr>
                <tr>
                  <td class="smallText"><b><?php echo TEXT_AMOUNT; ?></b><br><?php
                  //==============  GSR START ==============
                  if($coupon_type == 'P') {
                    echo tep_db_prepare_input($_POST['amount']); 
                  } else{
                    echo $currencies->format(nl2br(tep_db_prepare_input($_POST['amount'])),true,DEFAULT_CURRENCY,$currency_value); 
                  }
                  //echo $currencies->format(nl2br(tep_db_prepare_input($_POST['amount'])),true,DEFAULT_CURRENCY,$currency_value); 
                  //==============  GSR END ==============
                  ?>
                </td>
                </tr>
                <tr>
                  <td class="smallText"><b>Message</b><br/><?php echo tep_db_encoder(tep_db_prepare_input($_POST['message'])); ?></td>
                </tr>
				  <tr>
					<td align="right"><?php if (EMAIL_USE_HTML == 'false') { echo '<button class="btn btn-success btn-sm mt-0 mb-3 mr-2" name="back" type="button" onclick="javascript:editnewsletter()"> Edit </button>'; } ?><?php echo '<a class="btn btn-default btn-sm mt-0 mb-3 mr-2" href="' . tep_href_link(FILENAME_GV_MAIL) . '">' . IMAGE_CANCEL . '</a><button class="btn btn-success btn-sm mt-0 mb-3" type="submit">' . IMAGE_SEND_EMAIL . '</button>' ; ?></td>
				  </tr>
				  <tr>
					<td class="smallText">
					  <?php 
					  if (EMAIL_USE_HTML == 'false') {
						echo(TEXT_EMAIL_BUTTON_HTML);
					  } else {
						echo(TEXT_EMAIL_BUTTON_TEXT);
					  } 
					  ?>
					</td>
				  </tr>
				</table>
              </form>
            <?php
          } else {
            if (EMAIL_USE_HTML == 'true'){
              echo tep_insert_html_editor('message','simple','400');
            }
            
            echo tep_draw_form('mail', FILENAME_GV_MAIL, 'action=preview', 'POST','class="form-horizontal"');
            ?>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <?php
                    $customers = array();
                    $customers[] = array('id' => '', 'text' => TEXT_SELECT_CUSTOMER);
                    $customers[] = array('id' => '***', 'text' => TEXT_ALL_CUSTOMERS);
                    $customers[] = array('id' => '**D', 'text' => TEXT_NEWSLETTER_CUSTOMERS);
                    $mail_query = tep_db_query("select customers_email_address, customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " order by customers_lastname");
                    while($customers_values = tep_db_fetch_array($mail_query)) {
                      $customers[] = array('id' => $customers_values['customers_email_address'],
                                           'text' => $customers_values['customers_lastname'] . ', ' . $customers_values['customers_firstname'] . ' (' . $customers_values['customers_email_address'] . ')');
                    }
                    ?>
                    <tr>
                      <td class="control-label main-text"><?php echo TEXT_CUSTOMER; ?></td>
                      <td><?php echo tep_draw_pull_down_menu('customers_email_address', $customers, (isset($_GET['customer']) ? $_GET['customer'] : ''),'class="form-control" onchange="document.mail.email_to.value=this.value"');?></td>
                    </tr>

                    <tr>
                      <td class="control-label main-text"><?php echo TEXT_TO; ?><span class="required"></span></td>
                      <td class="control-label main-text text-left"><?php echo tep_draw_input_field('email_to',(isset($_POST['email_to'])?$_POST['email_to']:''),'required'); ?><?php echo '&nbsp;&nbsp;' . TEXT_SINGLE_EMAIL; ?></td>
                    </tr>

                    <tr>
                      <td class="control-label main-text"><?php echo TEXT_FROM; ?><span class="required"></span></td>
                      <td><?php echo tep_draw_input_field('from', (isset($_POST['from'])?$_POST['from']:STORE_OWNER_EMAIL_ADDRESS),'required'); ?></td>
                    </tr>

                    <tr>
                      <td class="control-label main-text"><?php echo TEXT_SUBJECT; ?><span class="required"></span></td>
                      <td><?php echo tep_draw_input_field('subject',(isset($_POST['subject'])?$_POST['subject']:''),'required'); ?></td>
                    </tr>

                    <tr>
                      <td class="control-label main-text" valign="top" class="main"><?php echo TEXT_AMOUNT; ?><span class="required"></span></td>
                      <td><?php echo tep_draw_input_field('amount',(isset($_POST['amount'])?$_POST['amount']:''),'required'); ?></td>
                    </tr>

                    <tr>
                      <td class="control-label main-text" valign="top" class="main"><?php echo TEXT_MESSAGE; ?><span class="required"></span></td> 
                      <td class="main"><?php echo tep_draw_textarea_field('message', 'soft', '60', '3', (isset($_POST['message'])?$_POST['message']:''), 'class="form-control" required'); ?></td>              
                    </tr>

					<tr>
					  <td colspan="2" align="center">
						<?php 
						if (EMAIL_USE_HTML == 'false') { 
						  echo '<button class="btn btn-success btn-sm mt-0 mb-3" type="submit">' . IMAGE_SEND_EMAIL . '</button>';
						} else {
						  echo '<button class="btn btn-success btn-sm mt-0 mb-3" type="submit">' . IMAGE_SEND_EMAIL . '</button>'; 
						}
						?>
					  </td>
					</tr>
	              </table>
              </form>
            <?php
          }
          ?>
          <!-- body_text_eof //-->

           </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>       