<?php
/*
  $Id: cds_configuration.php,v 1.1.1.1 2007/01/11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2006 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require(DIR_WS_FUNCTIONS . FILENAME_CDS_FUNCTIONS);

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'save':
      $configuration_value = tep_db_prepare_input($_POST['configuration_value']);
      $cID = tep_db_prepare_input($_GET['cID']);
      if (is_array($configuration_value)) {
        $configuration_value_new = '';
        foreach ($configuration_value as $value) {
          $configuration_value_new .= $value . ', ';
        }
        $configuration_value_new = substr($configuration_value_new, 0, strlen($configuration_value_new) - 2);
        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value_new) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");
      } else {
        tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");
      }
      tep_redirect(tep_href_link(FILENAME_CDS_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cID));
      break;
  }
}
$gID = (isset($_GET['gID'])) ? (int)$_GET['gID'] : 1;
$cfg_group_query = tep_db_query("SELECT configuration_group_title
                                   from " . TABLE_CONFIGURATION_GROUP . " 
                                          WHERE configuration_group_id = '" . (int)$gID . "'");
$cfg_group = tep_db_fetch_array($cfg_group_query);

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('cdsconfiguration') > 0) {
      echo $messageStack->output('cdsconfiguration'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-cdsconfiguration" class="table-cdsconfiguration">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_PAGE_TITLE; ?></th>
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_PAGE_VALUE; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_PAGE_ACTION; ?>&nbsp;</th>
              </tr>
              </thead>
              <tbody>
              <?php
              $configuration_query = tep_db_query("SELECT configuration_id, configuration_title, configuration_value, use_function from " . TABLE_CONFIGURATION . "  WHERE configuration_group_id = '" . (int)$gID . "'  ORDER BY sort_order");
              while ($configuration = tep_db_fetch_array($configuration_query)) {
                if (tep_not_null($configuration['use_function'])) {
                  $use_function = $configuration['use_function'];
                  if (preg_match('/->/', $use_function)) {
                    $class_method = explode('->', $use_function);
                    if (!is_object(${$class_method[0]})) {
                      include(DIR_WS_CLASSES . $class_method[0] . '.php');
                      ${$class_method[0]} = new $class_method[0]();
                    } 
                                        $cfgValue = tep_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
                  } else {
                    $cfgValue = tep_call_function($use_function, $configuration['configuration_value']);
                  }
                } else {
                  $cfgValue = $configuration['configuration_value'];
                } 
                                if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $configuration['configuration_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
                  $cfg_extra_query = tep_db_query("SELECT configuration_key, configuration_description, date_added, last_modified, use_function, set_function 
                                                      from " . TABLE_CONFIGURATION . " 
                                                               WHERE configuration_id = '" . (int)$configuration['configuration_id'] . "'");
                  $cfg_extra = tep_db_fetch_array($cfg_extra_query);
                  $cInfo_array = array_merge($configuration, $cfg_extra);
                  $cInfo = new objectInfo($cInfo_array);
                } 

				$selected = ((isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id))? true : false;
				$col_selected = ($selected) ? ' selected' : '';
				if ( $selected ) {
                  if ($cInfo->set_function == 'file_upload'){
                    echo '<tr id="defaultSelected" class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=upload') . '\'">' . "\n";
                  } else {
                    echo '<tr id="defaultSelected" class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=edit') . '\'">' . "\n";
                  }
                } else {
                  echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $configuration['configuration_id']) . '\'">' . "\n";
                }
                ?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $configuration['configuration_title']; ?></td>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo htmlspecialchars($cfgValue); ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if ( (isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id) ) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_CDS_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $configuration['configuration_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
              <?php
				  }
			  ?>
              </tbody>
            </table>
            
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">                
            
            <?php
            $heading = array();
            $contents = array();
            switch ($action) {
            case 'edit':
               $heading[] = array('text' => $cInfo->configuration_title);
               if ($cInfo->set_function) {
                 eval('$value_field = ' . $cInfo->set_function . '"' . htmlspecialchars($cInfo->configuration_value) . '");');
				  } else {
					$value_field = tep_draw_input_field('configuration_value', $cInfo->configuration_value);
				  }
				  $contents[] = array('form' => tep_draw_form('configuration', FILENAME_CDS_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=save'));
				  $contents[] = array('text' => '<div class="form-label mt-3">' . $cInfo->configuration_title . '</div><div class="sidebar-title">' . $cInfo->configuration_description . '<br>' . $value_field .'</div>');
				  $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mb-2 mt-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><a class="btn btn-grey btn-sm mb-2 mt-2 btn-cancel" href="' . tep_href_link(FILENAME_CDS_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id) . '">' . IMAGE_CANCEL . '</a></div>');
				break;

			default:
			  if (isset($cInfo) && is_object($cInfo)) {
				  $heading[] = array('text' => $cInfo->configuration_title);
					if ($cInfo->set_function == 'file_upload') {
					$contents[] = array('align' => 'center', 'text' => '<a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_CDS_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=upload') . '">' . IMAGE_EDIT . '</a>'.'<p>');
					  $contents[] = array('align' => 'center', 'text' => tep_image($upload_ws_dir . $cInfo->configuration_value, IMAGE_EDIT));
				  } else {
					  $contents[] = array('align' => 'center', 'text' => '<a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_CDS_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=edit') . '">' . IMAGE_EDIT . '</a>');
				  }  
				  $contents[] = array('text' => '<div class="sidebar-text">' . $cInfo->configuration_description . '</div>');
				  $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_DATE_ADDED . '</div><div class="sidebar-title">' . tep_date_short($cInfo->date_added). '</div>');
				if (tep_not_null($cInfo->last_modified)) $contents[] = array('text' => '<div class="sidebar-text">'.TEXT_INFO_LAST_MODIFIED . '</div><div class="sidebar-title">' . tep_date_short($cInfo->last_modified). '</div>');
				}
			  break;
			} 
			  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
				$box = new box;
				echo $box->showSidebar($heading, $contents);
			  }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
