<?php
if (!isset($is_read_only)) $is_read_only = false;
/*
  $Id: create_order_details.php,v 1.1.1.1 2004/03/04 23:40:22 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

function sbs_get_zone_name($country_id, $zone_id) {
  $zone_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . $country_id . "' and zone_id = '" . $zone_id . "'");
  if (tep_db_num_rows($zone_query)) {
    $zone = tep_db_fetch_array($zone_query);
    return $zone['zone_name'];
  } else {
    return (isset($default_zone) ? $default_zone : '');
  }
}
 // Returns an array with countries
function sbs_get_countries($countries_id = '', $with_iso_codes = false) {
  $countries_array = array();
  if ($countries_id) {
    if ($with_iso_codes) {
      $countries = tep_db_query("select countries_name, countries_iso_code_2, countries_iso_code_3 from " . TABLE_COUNTRIES . " where countries_id = '" . $countries_id . "' order by countries_name");
      $countries_values = tep_db_fetch_array($countries);
      $countries_array = array('countries_name' => $countries_values['countries_name'],
                               'countries_iso_code_2' => $countries_values['countries_iso_code_2'],
                               'countries_iso_code_3' => $countries_values['countries_iso_code_3']);
    } else {
      $countries = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . $countries_id . "'");
      $countries_values = tep_db_fetch_array($countries);
      $countries_array = array('countries_name' => $countries_values['countries_name']);
    }
  } else {
    $countries = tep_db_query("select countries_id, countries_name from " . TABLE_COUNTRIES . " order by countries_name");
    while ($countries_values = tep_db_fetch_array($countries)) {
      $countries_array[] = array('countries_id' => $countries_values['countries_id'],
                                 'countries_name' => $countries_values['countries_name']);
    }
  }
  return $countries_array;
}

function sbs_get_country_list($name, $selected = '', $parameters = '') {
 $countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
 $countries = sbs_get_countries();
 $size = sizeof($countries);
 for ($i=0; $i<$size; $i++) {
   $countries_array[] = array('id' => $countries[$i]['countries_id'], 'text' => $countries[$i]['countries_name']);
 }
 return tep_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
}

if (isset($account['customers_id'])) {
  tep_draw_hidden_field($account['customers_id']);
}
?>
<div class="main-container">
  <div class="ml-2 mr-2">
    <div class="note note-info"><?php echo CATEGORY_CORRECT; ?></div>
  </div>

  <div class="form-inline">
    <span class="col-3 sidebar-text text-right"><?php echo ENTRY_CUSTOMERS_ID; ?></span>
    <div class="col-9 sidebar-title">
      <?php
      if (isset($is_read_only)) {
        echo (isset($account['customers_id']) ? (int)$account['customers_id'] : '');
      } else {
        echo tep_draw_input_field('customers_id', (isset($account['customers_id']) ? $account['customers_id'] : 0), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_CUSTOMERS_ID_TEXT . '</span>';
      }
      ?>
    </div>
  </div>

  <div class="form-inline">
    <span class="col-3 sidebar-text text-right"><?php echo ENTRY_FIRST_NAME; ?></span>
    <div class="col-9 sidebar-title">
      <?php
      if (isset($is_read_only)) {
        echo (isset($account['customers_firstname']) ? $account['customers_firstname'] : '');
      } else {
        echo tep_draw_input_field('firstname', (isset($account['customers_firstname']) ? $account['customers_firstname'] : ''), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_FIRST_NAME_TEXT . '</span>';
      }
      ?>
    </div>
  </div>

  <div class="form-inline">
    <span class="col-3 sidebar-text text-right"><?php echo ENTRY_LAST_NAME; ?></span>
    <div class="col-9 sidebar-title">
      <?php
      if (isset($is_read_only)) {
        echo (isset($account['customers_lastname']) ? $account['customers_lastname'] : '');
      } else {
        echo tep_draw_input_field('lastname', (isset($account['customers_lastname']) ? $account['customers_lastname'] : ''), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_LAST_NAME_TEXT . '</span>';
      }
      ?>
    </div>
  </div>

  <div class="form-inline">
    <span class="col-3 sidebar-text text-right"><?php echo ENTRY_EMAIL_ADDRESS; ?></span>
    <div class="col-9 sidebar-title">
      <?php
      if (isset($is_read_only)) {
        echo (isset($account['customers_email_address']) ? $account['customers_email_address'] : '');
      } else {
        echo tep_draw_input_field('email_address', (isset($account['customers_email_address']) ? $account['customers_email_address'] : ''), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>';
      }
      ?>
    </div>
  </div>
  <?php
  if (ACCOUNT_COMPANY == 'true') {
    ?>
    <div class="form-inline">
      <span class="col-3 sidebar-text text-right"><?php echo ENTRY_COMPANY; ?></span>
      <div class="col-9 sidebar-title">
        <?php
        if (isset($is_read_only)) {
          echo (isset($address['entry_company']) ? $address['entry_company'] : '');
        } else {
          echo tep_draw_input_field('company', (isset($address['entry_company']) ? $address['entry_company'] : ''), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_COMPANY_TEXT . '</span>';
        }
        ?>
      </div>
    </div>
    <?php
  }
  ?>
  <div class="form-inline">
    <span class="col-3 sidebar-text text-right"><?php echo ENTRY_STREET_ADDRESS; ?></span>
    <div class="col-9 sidebar-title">
      <?php
      if (isset($is_read_only)) {
        echo (isset($address['entry_street_address']) ? $address['entry_street_address'] : '');
      } else {
        echo tep_draw_input_field('street_address', (isset($address['entry_street_address']) ? $address['entry_street_address'] : ''), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_STREET_ADDRESS_TEXT . '</span>';
      }
      ?>
    </div>
  </div>
  <?php
  if (ACCOUNT_SUBURB == 'true') {
    ?>
    <div class="form-inline">
      <span class="col-3 sidebar-text text-right"><?php echo ENTRY_COMPANY; ?></span>
      <div class="col-9 sidebar-title">
        <?php
        if (isset($is_read_only)) {
          echo (isset($address['entry_suburb']) ? $address['entry_suburb'] : '');
        } else {
          echo tep_draw_input_field('suburb', (isset($address['entry_suburb']) ? $address['entry_suburb'] : ''), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_SUBURB_TEXT . '</span>';
        }
        ?>
      </div>
    </div>
    <?php
  }
  ?>
  <div class="form-inline">
    <span class="col-3 sidebar-text text-right"><?php echo ENTRY_POST_CODE; ?></span>
    <div class="col-9 sidebar-title">
      <?php
      if (isset($is_read_only)) {
        echo (isset($address['entry_postcode']) ? $address['entry_postcode'] : '');
      } else {
        echo tep_draw_input_field('postcode', (isset($address['entry_postcode']) ? $address['entry_postcode'] : ''), 'maxlength="10" class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_POST_CODE_TEXT . '</span>';
      }
      ?>
    </div>
  </div>
  <div class="form-inline">
    <span class="col-3 sidebar-text text-right"><?php echo ENTRY_CITY; ?></span>
    <div class="col-9 sidebar-title">
      <?php
      if (isset($is_read_only)) {
        echo (isset($address['entry_city']) ? $address['entry_city'] : '');
      } else {
        echo tep_draw_input_field('city', (isset($address['entry_city']) ? $address['entry_city'] : ''), 'maxlength="10" class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_CITY_TEXT . '</span>';
      }
      ?>
    </div>
  </div>
  <?php
  if (ACCOUNT_STATE == 'true') {
    ?>
    <div class="form-inline">
      <span class="col-3 sidebar-text text-right"><?php echo ENTRY_STATE; ?></span>
      <div class="col-9 sidebar-title">
        <?php
        if ($address['entry_state'] == '') $address['entry_state'] = sbs_get_zone_name($address['entry_country_id'], $address['entry_zone_id']);
        if (isset($is_read_only)) {
          echo (isset($address['entry_state']) ? $address['entry_state'] : '');
        } else {
          echo tep_draw_input_field('state', (isset($address['entry_state']) ? $address['entry_state'] : ''), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_SUBURB_TEXT . '</span>';
        }
        ?>
      </div>
    </div>
    <?php
  }
  ?>
  <div class="form-inline">
    <span class="col-3 sidebar-text text-right"><?php echo ENTRY_COUNTRY; ?></span>
    <div class="col-9 sidebar-title">
      <?php
      if (isset($is_read_only)) {
        echo (isset($address['entry_country_id']) ? tep_get_country_name($address['entry_country_id']) : '');
      } else {
        echo tep_draw_input_field('country', (isset($address['entry_country_id']) ? tep_get_country_name($address['entry_country_id']) : ''), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_COUNTRY_TEXT . '</span>';
      }
      ?>
    </div>
  </div>
  <div class="form-inline">
    <span class="col-3 sidebar-text text-right"><?php echo ENTRY_TELEPHONE_NUMBER; ?></span>
    <div class="col-9 sidebar-title">
      <?php
      if (isset($is_read_only)) {
        echo (isset($account['customers_telephone']) ? $account['customers_telephone'] : '');
      } else {
        echo tep_draw_input_field('telephone', (isset($account['customers_telephone']) ? $account['customers_telephone'] : ''), 'class="form-control ml-2 w-50"') . '<span class="text-danger ml-2">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>';
      }
      echo tep_draw_hidden_field('gender', (isset($address['entry_gender']) ? $address['entry_gender'] : '' )) . "\n";
      echo tep_draw_hidden_field('dob', (isset($account['customers_dob']) ? $account['customers_dob'] : '' )) . "\n";
      echo tep_draw_hidden_field('fax' , (isset($address['entry_fax']) ? $address['entry_fax'] : '' )) . "\n";
      echo tep_draw_hidden_field('newsletter', (isset($account['customers_newsletter']) ? $account['customers_newsletter'] : '' )) . "\n";
      echo tep_draw_hidden_field('password', '') . "\n";
      echo tep_draw_hidden_field('confirmation', '') . "\n";
      echo tep_draw_hidden_field('zone_id', '') . "\n";
      ?>
    </div>
  </div>
  <div class="mb-2">&nbsp;</div>
</div>