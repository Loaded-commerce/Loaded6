<?php
/*
  $Id: account_details.php,v 2.0 2008/05/05 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

$is_read_only = isset($is_read_only) ? $is_read_only : false;
$error = isset($error) ? $error : false;
$newsletter_array = array(array('id' => '1',
                                'text' => ENTRY_NEWSLETTER_YES),
                          array('id' => '0',
                                'text' => ENTRY_NEWSLETTER_NO));
function sbs_get_zone_name($country_id, $zone_id) {
  $zone_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . $country_id . "' and zone_id = '" . $zone_id . "'");
  if (tep_db_num_rows($zone_query)) {
    $zone = tep_db_fetch_array($zone_query);
    return $zone['zone_name'];
  } else {
    return (isset($default_zone) ? $default_zone : '');
  }
}

// Returns an array with countries
function sbs_get_countries($countries_id = '', $with_iso_codes = false) {
  $countries_array = array();
  if ($countries_id) {
    if ($with_iso_codes) {
      $countries = tep_db_query("select countries_name, countries_iso_code_2, countries_iso_code_3 from " . TABLE_COUNTRIES . " where countries_id = '" . $countries_id . "' and country_status = 1 order by countries_name");
      $countries_values = tep_db_fetch_array($countries);
      $countries_array = array('countries_name' => $countries_values['countries_name'],
                               'countries_iso_code_2' => $countries_values['countries_iso_code_2'],
                               'countries_iso_code_3' => $countries_values['countries_iso_code_3']);
    } else {
      $countries = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . $countries_id . "' and country_status = 1 ");
      $countries_values = tep_db_fetch_array($countries);
      $countries_array = array('countries_name' => $countries_values['countries_name']);
    }
  } else {
    $countries = tep_db_query("select countries_id, countries_name from " . TABLE_COUNTRIES . " where country_status = 1 order by countries_name");
    while ($countries_values = tep_db_fetch_array($countries)) {
      $countries_array[] = array('countries_id' => $countries_values['countries_id'],
                                 'countries_name' => $countries_values['countries_name']);
    }
  }
  return $countries_array;
}

function sbs_get_country_list($name, $selected = '', $parameters = '') {
  $countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
  $countries = sbs_get_countries();
  $size = sizeof($countries);
  for ($i=0; $i<$size; $i++) {
    $countries_array[] = array('id' => $countries[$i]['countries_id'], 'text' => $countries[$i]['countries_name']);
  }
  return tep_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
}

// prepare the input fields, if the $account is availablem use it
// otherwise load the POST variables into a dummy $account
if (!isset($account)) {
  $account['customers_gender'] = isset($gender) ? $gender : '';
  $account['customers_firstname'] = isset($firstname) ? $firstname : '';
  $account['customers_lastname'] = isset($lastname) ? $lastname : '';
  $account['customers_dob'] = isset($dob) ? $dob : '';
  $account['customers_email_address'] = isset($email_address) ? $email_address : '';
  $account['entry_company'] = isset($company) ? $company : '';
  $account['entry_company_tax_id'] = isset($entry_company_tax_id) ? $entry_company_tax_id : '';
  $account['entry_street_address'] = isset($street_address) ? $street_address : '';
  $account['entry_suburb'] = isset($suburb) ? $suburb : '';
  $account['entry_postcode'] = isset($postcode) ? $postcode : '';
  $account['entry_city'] = isset($city) ? $city : '';
//$account['entry_state'] = isset($state) ? $state : '';
  $account['entry_country_id'] = isset($country) ? $country : 0;
  $account['entry_zone_id'] = isset($zone_id) ? $zone_id : 0;
  $account['customers_telephone'] = isset($telephone) ? $telephone : '';
  $account['customers_fax'] = isset($fax) ? $fax : '';
  $account['customers_newsletter'] = isset($newsletter) ? $newsletter : '1';
}
?>

<div class="sidebar-heading mt-2"><?php echo CATEGORY_PERSONAL; ?></div>
<div class="sidebar-heading-footer w-100"></div>

<table class="w-100 mb-5">
	  <?php
	  if (!isset($account['customers_gender'])) {
		$account['customers_gender'] = '';
	  }
	  if (ACCOUNT_GENDER == 'true') {
		$male = ($account['customers_gender'] == 'm') ? true : false;
		$female = ($account['customers_gender'] == 'f') ? true : false;
		?>
		<tr>
		  <td class="main-text"><?php echo ENTRY_GENDER; ?></td>
		  <td class="main-text" style="padding-top: 5px;">
			<?php
			if ($is_read_only) {
			  echo ($account['customers_gender'] == 'm') ? MALE : FEMALE;
			} elseif ($error) {
			  if ($entry_gender_error) {
				echo tep_draw_radio_field('gender', 'm', $male) . '' . MALE . '' . tep_draw_radio_field('gender', 'f', $female) . '' . FEMALE . '</td><td>' . ENTRY_GENDER_ERROR;
			  } else {
				echo ($gender == 'm') ? MALE : FEMALE;
				echo tep_draw_hidden_field('gender');
			  }
			} else {
			  echo tep_draw_radio_field('gender', 'm', $male) . '' . MALE . '' . tep_draw_radio_field('gender', 'f', $female) . '' . FEMALE . '' . ENTRY_GENDER_TEXT;
			}
			?>
		  </td>
		</tr>
		<?php
	  }
	  ?>
	  <tr>
		<td class="main-text"><?php echo ENTRY_FIRST_NAME; ?>&nbsp;<span class="required"></span></td>
		<td style="padding-top: 5px;">
		  <?php
		  if ($is_read_only) {
			echo $account['customers_firstname'];
		  } elseif ($error) {
			if ($entry_firstname_error) {
			  echo tep_draw_input_field('firstname') . '</td><td>' . ENTRY_FIRST_NAME_ERROR;
			} else {
			  echo $firstname . tep_draw_hidden_field('firstname');
			}
		  } else {
			echo tep_draw_input_field('firstname', (isset($account['customers_firstname']) ? $account['customers_firstname'] : ''), '', true) ;
		  }
		  ?>
		</td>
	  </tr>
	  <tr class="mt-5">
		<td class="main-text"><?php echo ENTRY_LAST_NAME; ?>&nbsp;<span class="required"></span></td>
		<td class="mt-3" style="padding-top: 5px;">
		  <?php
		  if ($is_read_only) {
			echo $account['customers_lastname'];
		  } elseif ($error) {
			if ($entry_lastname_error) {
			  echo tep_draw_input_field('lastname') . '</td><td>' . ENTRY_LAST_NAME_ERROR;
			} else {
			  echo $lastname . tep_draw_hidden_field('lastname');
			}
		  } else {
			echo tep_draw_input_field('lastname', (isset($account['customers_lastname']) ? $account['customers_lastname'] : ''), '', true);
		  }
		  ?>
		</td>
	  </tr>
	  <?php
	  if (ACCOUNT_DOB == 'true') {
		?>
		<tr>
		  <td class="main-text"><?php echo ENTRY_DATE_OF_BIRTH; ?></td>
		  <td style="padding-top: 5px;">
			<?php
			if ($is_read_only) {
			  echo tep_date_short($account['customers_dob']);
			} elseif ($error) {
			  if ($entry_date_of_birth_error) {
				echo tep_draw_input_field('dob') . '</td><td>' . ENTRY_DATE_OF_BIRTH_ERROR;
			  } else {
				echo $dob . tep_draw_hidden_field('dob');
			  }
			} else {
			  echo tep_draw_input_field('dob', (isset($account['customers_dob']) ? tep_date_short($account['customers_dob']) : '')) . '' . ENTRY_DATE_OF_BIRTH_TEXT;
			}
			?>
		  </td>
		</tr>
		<?php
	  }
	  ?>
	  <tr>
		<td class="main-text"><?php echo ENTRY_EMAIL_ADDRESS; ?>&nbsp;<span class="required"></span></td>
		<td style="padding-top: 5px;">
		  <?php
		  if ($is_read_only) {
			echo $account['customers_email_address'];
		  } elseif ($error) {
			if ($entry_email_address_error) {
			  echo tep_draw_input_field('email_address') . '</td><td>' . ENTRY_EMAIL_ADDRESS_ERROR;
			} elseif ($entry_email_address_check_error) {
			  echo tep_draw_input_field('email_address') . '</td><td>' . ENTRY_EMAIL_ADDRESS_CHECK_ERROR;
			} elseif ($entry_email_address_exists) {
			  echo tep_draw_input_field('email_address') . '</td><td>' . ENTRY_EMAIL_ADDRESS_ERROR_EXISTS;
			} else {
			  echo $email_address . tep_draw_hidden_field('email_address');
			}
		  } else {
			echo tep_draw_input_field('email_address', (isset($account['customers_email_address']) ? $account['customers_email_address'] : ''), '', true);
		  }
		  ?>
		</td>
	  </tr>
  <?php
  if (ACCOUNT_COMPANY == 'true') {
    ?>
    <tr>
      <td colspan="3">
		<div class="sidebar-heading mt-2"><?php echo CATEGORY_COMPANY; ?></div>
		<div class="sidebar-heading-footer w-100"></div>
	</td>
    </tr>
    <tr>
	  <td class="main-text"><?php echo ENTRY_COMPANY; ?>&nbsp;<span class="required"></span></td>
	  <td style="padding-top: 5px;">
		<?php
		if ($is_read_only) {
		  echo $account['entry_company'];
		} elseif ($error) {
		  if ($entry_company_error) {
			echo tep_draw_input_field('company') . '</td><td>' . ENTRY_COMPANY_ERROR;
		  } else {
			echo $company . tep_draw_hidden_field('company');
		  }
		} else {
		  echo tep_draw_input_field('company', $account['entry_company']);
		}
		?>
	  </td>
    </tr>
    <tr>
		  <td class="main-text">Tax ID&nbsp;<span class="required"></span></td>
		  <td style="padding-top: 5px;">
			<?php
			if ($is_read_only) {
			  echo $account['entry_company_tax_id'];
			} elseif ($error) {
			  if ($entry_company_error) {
				echo tep_draw_input_field('entry_company_tax_id') . '</td><td>' . ENTRY_COMPANY_ERROR;
			  } else {
				echo $entry_company_tax_id . tep_draw_hidden_field('entry_company_tax_id');
			  }
			} else {
			  echo tep_draw_input_field('entry_company_tax_id', $account['entry_company_tax_id']);
			}
			?>
		  </td>
    </tr>
    <?php
  }
  ?>
  <tr>
    <td class="formAreaTitle" colspan="3">
		<div class="sidebar-heading mt-2"><?php echo CATEGORY_ADDRESS; ?></div>
		<div class="sidebar-heading-footer w-100"></div></td>
  </tr>
  <tr>
		<td class="main-text"><?php echo ENTRY_STREET_ADDRESS; ?><?php if(trim(ENTRY_STREET_ADDRESS_TEXT) != '') { echo '&nbsp;<span class="required"></span>'; } ?></td>
		<td style="padding-top: 5px;">
		  <?php
		  if ($is_read_only) {
			echo $account['entry_street_address'];
		  } elseif ($error) {
			if ($entry_street_address_error) {
			  echo tep_draw_input_field('street_address') . '</td><td>' . ENTRY_STREET_ADDRESS_ERROR;
			} else {
			  echo $street_address . tep_draw_hidden_field('street_address');
			}
		  } else {
			echo tep_draw_input_field('street_address', (isset($account['entry_street_address']) ? $account['entry_street_address'] : ''), '', true);
		  }
		  ?>
		</td>
	  </tr>
	  <?php
	  if (ACCOUNT_SUBURB == 'true') {
		?>
		<tr>
		  <td class="main-text"><?php echo ENTRY_SUBURB; ?><?php if(trim(ENTRY_SUBURB_TEXT) != '') { echo '&nbsp;<span class="required"></span>'; } ?></td>
		  <td style="padding-top: 5px;">
			<?php
			if ($is_read_only) {
			  echo $account['entry_suburb'];
			} elseif ($error) {
			  if ($entry_suburb_error) {
				echo tep_draw_input_field('suburb') . '</td><td>' . ENTRY_SUBURB_ERROR;
			  } else {
				echo $suburb . tep_draw_hidden_field('suburb');
			  }
			} else {
			  echo tep_draw_input_field('suburb', (isset($account['entry_suburb']) ? $account['entry_suburb'] : '')) ;
			}
			?>
		  </td>
		</tr>
		<?php
	  }
	  ?>
	  <tr>
		<td class="main-text"><?php echo ENTRY_POST_CODE; ?><?php if(trim(ENTRY_POST_CODE_TEXT) != '') { echo '&nbsp;<span class="required"></span>'; } ?></td>
		<td style="padding-top: 5px;">
		  <?php
		  if ($is_read_only) {
			echo $account['entry_postcode'];
		  } elseif ($error) {
			if ($entry_post_code_error) {
			  echo tep_draw_input_field('postcode','','maxlength="10"') . '</td><td>' . ENTRY_POST_CODE_ERROR;
			} else {
			  echo $postcode . tep_draw_hidden_field('postcode');
			}
		  } else {
			echo tep_draw_input_field('postcode', (isset($account['entry_postcode']) ? $account['entry_postcode'] : ''),'maxlength="10"', true) ;
		  }
		  ?>
		</td>
	  </tr>
	  <tr>
		<td class="main-text"><?php echo ENTRY_CITY; ?><?php if(trim(ENTRY_CITY_TEXT) != '') { echo '&nbsp;<span class="required"></span>'; } ?></td>
		<td style="padding-top: 5px;">
		  <?php
		  if ($is_read_only) {
			echo $account['entry_city'];
		  } elseif ($error) {
			if ($entry_city_error) {
			  echo tep_draw_input_field('city') . '</td><td>' . ENTRY_CITY_ERROR;
			} else {
			  echo $city . tep_draw_hidden_field('city');
			}
		  } else {
			echo tep_draw_input_field('city', (isset($account['entry_city']) ? $account['entry_city'] : ''), '', true) ;
		  }
		  ?>
		</td>
	  </tr>
	  <?php
	  if (ACCOUNT_STATE == 'true') {
		?>
		<tr>
		  <td class="main-text"><?php echo ENTRY_STATE; ?><?php if(trim(ENTRY_STATE_TEXT) != '') { echo '&nbsp;<span class="required"></span>'; } ?></td>
		  <td style="padding-top: 5px;" id="state_id">
				<?php
 					  $selectedcountry = isset($_POST['country'])?$_POST['country']:STORE_COUNTRY;
					  if (!isset($country)) $country = 0;
					  if (!isset($zone_id)) $zone_id = 0;
					  $statename = sbs_get_zone_name($selectedcountry, $zone_id);
					  $process = true;
					  $entry_state_has_zones = true;
					  if ($process == true) {
					  $zones_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$selectedcountry . "' and zone_status = 1 order by zone_name");
					  $entry_state_has_zones = (tep_db_num_rows($zones_query) > 0);
						  if ($entry_state_has_zones == true) {
							$zones_array = array();
							$selected_zone = '';
							$selected_zone_code = '';
							$zones_array[] = array('id' => '', 'text' => 'Select State');
							$state = $address['zone_id'];
							$zones_query = tep_db_query("select * from " . TABLE_ZONES . " where zone_country_id = '" . (int)$selectedcountry . "' and zone_status = 1  order by zone_name");
							while ($zones_values = tep_db_fetch_array($zones_query)) {
							  $zones_array[] = array('id' => $zones_values['zone_name'], 'text' => $zones_values['zone_name']);
							   if (strtolower($state) == strtolower($zones_values['zone_code']) || strtolower($state) == strtolower($zones_values['zone_name']) || $state == $zones_values['zone_id']) {
								$selected_zone = $zones_values['zone_name'];
								$selected_zone_code = $zones_values['zone_code'];
							  }
							}
							if ($error) {
								echo tep_draw_hidden_field('zone_id') . tep_draw_hidden_field('state',$statename) . $statename;
							}else{
								echo tep_draw_pull_down_menu('state', $zones_array, $selected_zone, 'required id="state" class="form-control"');
							}
						  } else {
							if ($error) {
								if($entry_state_error){
									echo tep_draw_input_field('state', '', '', true) .tep_draw_hidden_field('zone_id'). ENTRY_STATE_ERROR;
								}else{
									echo tep_draw_input_field('state', '', '', true) .tep_draw_hidden_field('zone_id');
								}
							}else{
								echo tep_draw_input_field('state', $entry_state, 'required id="state" class="form-control"' );
							}
						  }
						} else {
							if ($error) {
								echo tep_draw_hidden_field('zone_id') . tep_draw_hidden_field('state') . $statename ;
							}else{
								echo tep_draw_input_field('state', $entry_state, 'id="state" required class="form-control"');
							}
						}
				?>
		  </td>
		</tr>
		<?php
	  }
	  ?>
	  <tr>
		<td class="main-text" style="width:250px;"><?php echo ENTRY_COUNTRY; ?><?php if(trim(ENTRY_COUNTRY_TEXT) != '') { echo '&nbsp;<span class="required"></span>'; } ?></td>
		<td style="padding-top: 5px;">
		  <?php
		  $account['entry_country_id'] = STORE_COUNTRY;
		  if ($is_read_only) {
			echo tep_get_country_name($account['entry_country_id']);
		  } elseif ($error) {
			if ($entry_country_error) {
			  echo sbs_get_country_list('country') . '</td><td>' . ENTRY_COUNTRY_ERROR;
			} else {
			  echo sbs_get_country_list('country','','id="onprocesscountry" readonly');
			}
		  } else {
			echo sbs_get_country_list('country', $account['entry_country_id'],'id="countrydropdown" style="width:100%" onchange="javascript:change_state(this.value);"') ;
		  }
		  ?>
		</td>
	</tr>
	<tr>
	<td class="formAreaTitle" colspan="3">
	<div class="sidebar-heading mt-2"><?php echo CATEGORY_CONTACT; ?></div>
	<div class="sidebar-heading-footer w-100"></div></td>
	</tr>
	<tr>
		<td class="main-text"><?php echo ENTRY_TELEPHONE_NUMBER; ?><?php if(trim(ENTRY_TELEPHONE_NUMBER_TEXT) != '') { echo '&nbsp;<span class="required"></span>'; } ?></td>
		<td style="padding-top: 5px;">
		  <?php
		  if ($is_read_only) {
			echo $account['customers_telephone'];
		  } elseif ($error) {
			if ($entry_telephone_error) {
			  echo tep_draw_input_field('telephone') . '</td><td>' . ENTRY_TELEPHONE_NUMBER_ERROR;
			} else {
			  echo $telephone . tep_draw_hidden_field('telephone');
			}
		  } else {
			echo tep_draw_input_field('telephone', (isset($account['customers_telephone']) ? $account['customers_telephone'] : ''), '', true) ;
		  }
		  ?>
		</td>
	  </tr>
	  <tr>
		<td class="main-text"><?php echo ENTRY_FAX_NUMBER; ?></td>
		<td style="padding-top: 5px;">
		  <?php
		  if ($is_read_only) {
			echo $account['customers_fax'];
		  } elseif ($error) {
			if ($entry_fax_error) {
			  echo tep_draw_input_field('fax');
			} else {
			  echo $fax . tep_draw_hidden_field('fax');
			}
		  } else {
			echo tep_draw_input_field('fax', (isset($account['customers_fax']) ? $account['customers_fax'] : ''));
		  }
		  ?>
		</td>
	</tr>
<?php
  echo tep_draw_hidden_field('password', tep_create_hard_pass());
  echo tep_draw_hidden_field('send_password', '1');
?>
</table>
