<?php
$arr_boxes_page['index'] = 'index';
$arr_boxes_page['banner_manager'] = 'marketing';
$arr_boxes_page['banner_statistics'] = 'marketing';
$arr_boxes_page['specials'] = 'marketing';
$arr_boxes_page['newsletters'] = 'marketing';
$arr_boxes_page['mailbeez'] = 'marketing';

$arr_boxes_page['pages'] = 'information';
$arr_boxes_page['cds_configuration'] = 'information';
$arr_boxes_page['cds_backup_restore'] = 'information';
$arr_boxes_page['faq_manager'] = 'information';
$arr_boxes_page['faq_categories'] = 'information';
$arr_boxes_page['define_mainpage'] = 'information';

$arr_boxes_page['categories'] = 'catalog';
$arr_boxes_page['product_edit'] = 'catalog';
$arr_boxes_page['products_attributes'] = 'catalog';
$arr_boxes_page['manufacturers'] = 'catalog';
$arr_boxes_page['reviews'] = 'catalog';
$arr_boxes_page['shopbyprice'] = 'catalog';
$arr_boxes_page['xsell_products'] = 'catalog';
$arr_boxes_page['featured'] = 'catalog';
$arr_boxes_page['products_expected'] = 'catalog';
$arr_boxes_page['product_files'] = 'catalog';



$arr_boxes_page['orders'] = 'customers';
$arr_boxes_page['create_order'] = 'customers';
$arr_boxes_page['edit_orders'] = 'customers';
$arr_boxes_page['create_order_admin'] = 'customers';
$arr_boxes_page['create_account_process'] = 'customers';
$arr_boxes_page['create_account_success'] = 'customers';
$arr_boxes_page['paypal'] = 'customers';
$arr_boxes_page['customers'] = 'customers';
$arr_boxes_page['create_account'] = 'customers';
$arr_boxes_page['cre_marketplace'] = 'customers';
$arr_boxes_page['add_order_product'] = 'customers';
$arr_boxes_page['create_order_shipping'] = 'customers';
$arr_boxes_page['create_order_payment'] = 'customers';
$arr_boxes_page['get_loaded'] = 'customers';

$arr_boxes_page['coupon_admin'] = 'gv_admin';
$arr_boxes_page['gift_voucher_report'] = 'gv_admin';
$arr_boxes_page['gv_report'] = 'gv_admin';
$arr_boxes_page['gv_queue'] = 'gv_admin';
$arr_boxes_page['gv_mail'] = 'gv_admin';
$arr_boxes_page['gv_sent'] = 'gv_admin';

$arr_boxes_page['admin_members'] = 'administrator';
$arr_boxes_page['admin_groups'] = 'administrator';
$arr_boxes_page['admin_account'] = 'administrator';
$arr_boxes_page['admin_files'] = 'administrator';

$arr_boxes_page['languages'] = 'localization';
$arr_boxes_page['currencies'] = 'localization';
$arr_boxes_page['define_language'] = 'localization';
$arr_boxes_page['orders_status'] = 'localization';

$arr_boxes_page['countries'] = 'taxes';
$arr_boxes_page['zones'] = 'taxes';
$arr_boxes_page['geo_zones'] = 'taxes';
$arr_boxes_page['tax_classes'] = 'taxes';
$arr_boxes_page['tax_rates'] = 'taxes';

$arr_boxes_page['links'] = 'links';
$arr_boxes_page['link_categories'] = 'links';
$arr_boxes_page['links_contact'] = 'links';

$arr_boxes_page['configuration'] = 'configuration';

$arr_boxes_page['modules'] = 'modules';

$arr_boxes_page['backup_mysql'] = 'tools';
$arr_boxes_page['backup'] = 'tools';
$arr_boxes_page['cache'] = 'tools';
$arr_boxes_page['edit_textdata'] = 'tools';
$arr_boxes_page['email_subjects'] = 'tools';
$arr_boxes_page['mail'] = 'tools';
$arr_boxes_page['server_info'] = 'tools';
$arr_boxes_page['whos_online'] = 'tools';

$arr_boxes_page['header_tags_english'] = 'design_controls';
$arr_boxes_page['header_tags_fill_tags'] = 'design_controls';
$arr_boxes_page['header_tags_controller'] = 'design_controls';
$arr_boxes_page['template_admin'] = 'design_controls';
$arr_boxes_page['template_configuration'] = 'design_controls';
$arr_boxes_page['infobox_configuration'] = 'design_controls';
$arr_boxes_page['branding_manager'] = 'design_controls';

$arr_boxes_page['easypopulate_export'] = 'data';
$arr_boxes_page['easypopulate_import'] = 'data';
$arr_boxes_page['easypopulate_basic_export'] = 'data';
$arr_boxes_page['easypopulate_basic_import'] = 'data';
$arr_boxes_page['feature_not_present'] = 'data';
$arr_boxes_page['data_help'] = 'data';
$arr_boxes_page['data'] = 'data';

$arr_boxes_page['stats_products_viewed'] = 'reports';
$arr_boxes_page['stats_products_purchased'] = 'reports';
$arr_boxes_page['stats_articles_viewed'] = 'reports';
$arr_boxes_page['stats_wishlist'] = 'reports';
$arr_boxes_page['stats_customers'] = 'reports';
$arr_boxes_page['stats_not_valid_users'] = 'reports';
$arr_boxes_page['stats_customers_orders'] = 'reports';
$arr_boxes_page['stats_sales_report2'] = 'reports';
$arr_boxes_page['stats_daily_products_sales_report'] = 'reports';
$arr_boxes_page['stats_manufacturers_sales'] = 'reports';
$arr_boxes_page['stats_products_notifications'] = 'reports';
$arr_boxes_page['stats_low_stock'] = 'reports';
$arr_boxes_page['stats_monthly_sales'] = 'reports';

$arr_boxes_page['articles'] = 'articles';
$arr_boxes_page['articles_config'] = 'articles';
$arr_boxes_page['authors'] = 'articles';
$arr_boxes_page['article_reviews'] = 'articles';
$arr_boxes_page['articles_xsell'] = 'articles';
?>
