<?php
/*
  $Id: cdspages_pages_sidebarcatbuttons.php, v 1.0.0.0 2008/02/12 maestro

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2006 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
  Author: Michael Hogan, modified: maestro
*/

  global $cInfo;
  $cPath = (isset($_GET['cPath'] ) && tep_not_null($_GET['cPath'])) ? $_GET['cPath'] : 0;

  $rci = '<a class="btn btn-grey btn-sm mt-2 mb-2 mr-2 btn-viewincatalog" target="_blank" href="' . tep_catalog_href_link('pages.php', 'CDpath=' . $cInfo->ID) . '">View in Catalog</a>';

?>