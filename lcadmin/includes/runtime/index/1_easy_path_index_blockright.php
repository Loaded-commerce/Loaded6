<?php
/*
  $Id: 1_easy_path_index_blockleft.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
if (defined('ADMIN_BLOCKS_EASY_PATH') && ADMIN_BLOCKS_EASY_PATH == 'true'){
  ?>
    <!-- begin panel -->
    <div class="col-lg-12 panel panel-inverse" style="">
      <div class="panel-heading">
        <h4 class="panel-title"><?php cre_index_block_title(BLOCK_TITLE_EASY_PATH); ?></h4>
      </div>
      <div class="panel-body">


      </div>
    </div>
  <!-- end easypath -->
  <?php
}
?>