<?php
global $directory_array, $file_extension;

$arr_available_addons = json_decode(AVAILABLE_ADDONS_jSON);
foreach($arr_available_addons as $indv_addons) {
	$module_directory = DIR_FS_CATALOG.'addons/'. $indv_addons .'/catalog/includes/modules/addons/';
	$module_lang_directory = DIR_FS_CATALOG.'addons/'. $indv_addons .'/catalog/includes/languages/english/modules/addons/';
	if(is_dir($module_directory)) {
	  if ($dir = @dir($module_directory)) {
		while ($file = $dir->read()) {
		  if (!is_dir($module_directory . $file)) {
			if (substr($file, strrpos($file, '.')) == $file_extension) {
				$mod_class = substr($file, 0, strrpos($file, '.'));
				$directory_array[] = array('mod_path'=>'addons_'.substr($indv_addons, 3), 'mod_file'=>$module_directory . $file, 'mod_lang_file'=> $module_lang_directory .$file, 'mod_class'=>$mod_class);

			}
		  }
		}
		sort($directory_array);
		$dir->close();
	  }
   }
}
?>
