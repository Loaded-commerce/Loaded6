<?php
/*
  LoadedCommerce, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2019 Loaded Commerce

  Released under the GNU General Public License
*/

// Define the webserver and path parameters
// * DIR_FS_* = Filesystem directories (local/physical)
// * DIR_WS_* = Webserver directories (virtual/URL)

  require_once('../lc_config.php');

  define('HTTP_CATALOG_SERVER', HTTP_SERVER);
  define('HTTPS_CATALOG_SERVER', HTTPS_SERVER);
  define('HTTPS_ADMIN_SERVER', HTTPS_SERVER);
  define('ENABLE_SSL_CATALOG', 'false'); // secure webserver for catalog module
  define('DIR_WS_HTTP_ADMIN',  DIR_WS_HTTP_CATALOG.'lcadmin/');
  define('DIR_WS_HTTPS_ADMIN',  DIR_WS_HTTP_CATALOG.'lcadmin/');

  define('DIR_FS_DOCUMENT_ROOT', DIR_FS_CATALOG); // where the pages are located on the server
  define('DIR_FS_ADMIN', DIR_FS_CATALOG.'lcadmin/'); // absolute path required
  define('DIR_WS_CATALOG', DIR_WS_HTTP_CATALOG); // absolute path required

  define('DIR_WS_CATALOG_IMAGES', DIR_WS_CATALOG . 'images/');
  define('DIR_WS_CATALOG_LANGUAGES', DIR_WS_CATALOG . 'includes/languages/');
  define('DIR_WS_CATALOG_PRODUCTS', DIR_WS_CATALOG_IMAGES . 'products/');
  define('DIR_FS_CATALOG_LANGUAGES', DIR_FS_CATALOG . 'includes/languages/');
  define('DIR_FS_CATALOG_IMAGES', DIR_FS_CATALOG . 'images/');
  define('DIR_FS_CATALOG_PRODUCTS', DIR_FS_CATALOG_IMAGES . 'products/');
  define('DIR_FS_CATALOG_CATEGORIES', DIR_FS_CATALOG_IMAGES . 'categories/');
  define('DIR_FS_CATALOG_MANUFACTURERS', DIR_FS_CATALOG_IMAGES . 'manufacturers/');
  define('DIR_FS_CATALOG_PAGES', DIR_FS_CATALOG_IMAGES . 'pages/');
  define('DIR_FS_CATALOG_BANNERS', DIR_FS_CATALOG_IMAGES . 'banners/');
  define('DIR_FS_CATALOG_OTHERS', DIR_FS_CATALOG_IMAGES . 'others/');
  define('DIR_FS_CATALOG_VIDEOS', DIR_FS_CATALOG_IMAGES . 'videos/');
  define('DIR_FS_CATALOG_ARTICLES', DIR_FS_CATALOG_IMAGES . 'articles/');
  define('DIR_FS_CATALOG_DEFAULT', DIR_FS_CATALOG_IMAGES . 'default/');
  define('DIR_WS_CATALOG_DEFAULT', DIR_WS_CATALOG_IMAGES . 'default/');
  define('DIR_FS_CATALOG_MODULES', DIR_FS_CATALOG . 'includes/modules/');
  define('DIR_FS_BACKUP', DIR_FS_ADMIN . 'backups/');

// Added for Templating
  define('DIR_FS_CATALOG_MAINPAGE_MODULES', DIR_FS_CATALOG_MODULES . 'mainpage_modules/');
  define('DIR_WS_CATALOG_TEMPLATES', DIR_WS_CATALOG . 'templates/');
  define('DIR_FS_TEMPLATES', DIR_FS_CATALOG . 'templates/');

?>