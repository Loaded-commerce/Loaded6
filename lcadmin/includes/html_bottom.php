      <!-- scroll to top btn -->
      <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    </div>

	<script type="text/javascript" src="../templates/default/library/fancyBox/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="../templates/default/library/fancyBox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <?php
    if (file_exists(DIR_FS_ADMIN . 'assets/js/general.js.php')) include(DIR_FS_ADMIN . 'assets/js/general.js.php');
    ?>
    <script>
      $(document).ready(function() {
        App.init();
    <?php if(basename($_SERVER['PHP_SELF']) == 'index.php') { ?>
        ChartNvd3.init();
	<?php } ?>
		$('.class-date').datepicker({
			todayHighlight: true,
			autoclose: true,
			format: 'yyyy-mm-dd'
		});
        if (window.location.pathname.indexOf('index.php')>-1) DashboardV2.init();
      });
		function assign_product_to_file(fID)
		{
			$.fancybox.open({
					href : '<?php echo tep_href_link('fdm_search_product.php')?>&fID='+fID,
					type : 'iframe',
					width: '900',
					height: '600px',
					padding : 5,
					modal : true,
					showCloseButton : true,
					autoScale : true,
					openEffect: 'elastic',
					afterShow : function() {
						$('.fancybox-skin').append('<a title="Close" class="fancybox-item fancybox-close" href="javascript:jQuery.fancybox.close();"></a>');
					}
				});
		}

		function assign_file_to_product(pID)
		{
			$.fancybox.open({
					href : '<?php echo tep_href_link('fdm_search_file.php')?>&pID='+pID,
					type : 'iframe',
					width: '900',
					height: '600px',
					padding : 5,
					modal : true,
					showCloseButton : true,
					autoScale : true,
					openEffect: 'elastic',
					afterShow : function() {
						$('.fancybox-skin').append('<a title="Close" class="fancybox-item fancybox-close" href="javascript:jQuery.fancybox.close();"></a>');
					}
				});
		}

    </script>
    <?php if(basename($_SERVER['PHP_SELF']) == 'index.php'){?>
	<script type="text/javascript">
		var blue = '#348fe2', blueLight	= '#5da5e8', blueDark = '#1993E4', aqua = '#49b6d6', aquaLight = '#6dc5de', aquaDark = '#3a92ab', green	= '#00acac', greenLight	= '#33bdbd', greenDark	= '#008a8a', orange	= '#f59c1a', orangeLight = '#f7b048', orangeDark = '#c47d15', dark = '#2d353c', grey = '#b6c2c9', purple = '#727cb6', purpleLight = '#8e96c5', purpleDark = '#5b6392', red = '#ff5b57';
		var handleBarChart = function() {
			"use strict";
			var barChartData = [{
				key: 'Cumulative Return',
				values: [
					<?php
						for($i = 1; $i <= 7; $i++) {
							$ts = strtotime($year.'W'.$week.$i);
							$curntweek = date("m-d-Y", $ts);
							$dateweek = date("Y-m-d", $ts);
							$strings = array( 'blue','blueLight','blueDark','aqua','aquaLight','aquaDark','green','greenLight','greenDark','orange','orangeLight','orangeDark','dark','grey','purple','purpleLight','purpleDark','red');
							$key = array_rand($strings);
					?>
							{ 'label' : '<?php echo $curntweek; ?>', 'value' : '<?php echo get_ordertotal_bydate($dateweek); ?>', 'color' : <?php echo $strings[$key];?> },
					<?php } ?>
						]
			}];
			nv.addGraph(function() {
				var barChart = nv.models.discreteBarChart()
					.x(function(d) { return d.label })
					.y(function(d) { return d.value * 1 })
					.showValues(true)
					.rotateLabels(0)
					.duration(250);

					barChart.yAxis.axisLabel("Total Orders Amount USD");
					barChart.xAxis.axisLabel('Week Days');

					d3.select('#nv-bar-chart-weekly').append('svg').datum(barChartData).call(barChart);
					nv.utils.windowResize(barChart.update);

					return barChart;
			});
		}

		var handlePieAndDonutChart = function() {
			"use strict";

			var pieChartData = [
				{ 'label': 'Subscribed', 'value' : <?php echo $totalsubscribed; ?>, 'color': red },
				{ 'label': 'UnSubscribed', 'value' : <?php echo $totalunsubscribed; ?>, 'color': orange }
			];

			nv.addGraph(function() {
				var pieChart = nv.models.pieChart()
				  .x(function(d) { return d.label })
				  .y(function(d) { return d.value })
				  .showLabels(true)
				  .labelThreshold(.05);

				d3.select('#nv-pie-chart').append('svg')
					.datum(pieChartData)
					.transition().duration(350)
					.call(pieChart);

				return pieChart;
			});


			nv.addGraph(function() {
			  var chart = nv.models.pieChart()
				  .x(function(d) { return d.label })
				  .y(function(d) { return d.value })
				  .showLabels(true)
				  .labelThreshold(.05)
				  .labelType("percent")
				  .donut(true)
				  .donutRatio(0.35);

				d3.select('#nv-donut-chart').append('svg')
					.datum(pieChartData)
					.transition().duration(350)
					.call(chart);

				return chart;
			});
		};

		var ChartNvd3 = function () {
			"use strict";
			return {
				init: function () {
					handleBarChart();
					handlePieAndDonutChart();
				}
			};
		}();
	</script>
	<?php } ?>
	<script type="text/javascript">
	function setStatus(actiontype,ajaxlink,params,ID,newstatus ){
	//alert(params);
	        var statustype = 'activated';
	        var icontype = "<i class='fa fa-lg fa-check-circle text-success' style='color:#F2FA03 !important'></i>";
			if(parseInt(newstatus) == '0'){
			    var statustype = 'deactivated';
			    var icontype = "<i class='fa fa-lg fa-times-circle text-danger'></i>";

			}
	         // params = 'action='+actiontype+'&flag='+newstatus+'&cID=' + ID;
	          //alert(params);
	          var active_id = '#crow_'+ID+ ' .'+actiontype+' .sactive';
	          var deactive_id = '#crow_'+ID+ ' .'+actiontype+' .sdeactive';
	          var message = icontype+'  Status has been '+statustype+' successfully!';
			$.ajax({
				type: 'get',
				url: ajaxlink,
				data: params,
				success: function (retval) {
				    if(parseInt(retval) == 0){
				       $(active_id).css('display', 'none');
				       $(deactive_id).css('display', 'block');

				    }
				    if(parseInt(retval) == 1){
				       $(active_id).css('display', 'block');
				       $(deactive_id).css('display', 'none');
				    }
				    $.gritter.add({
						text:message,
						sticky:false,time:"2500"});

				}
			});
	}


	function selectProduct(products_id)
	{
	//alert(products_id);
	params = 'pID='+products_id;
	$('.ac_results').css('display', 'none');

		$.ajax({
			type:'post',
			data: params,
			url: '<?php echo tep_href_link("ajax_common.php","action=add_ajax_product");?>',
			success:function(retval){
			//alert(retval);
			 $('#replacediv').html(retval);
				   $("#search_data").autocomplete("autocomplete_product_search.php", {
					  selectFirst: true,
					  fun: 'selectCurrent2',
				   });

		   }
		});
	}

</script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
  </body>
</html>