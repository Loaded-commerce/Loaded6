<?php
class lcadmin {
  public $common_js = array();
  public $common_action_js = array();
  public $common_document_ready_js = array();
  public $append_sql = '';

  public function __construct($products_id=0) {
    global $languages_id;

  }  //end of __construct

  public function set_js_document_ready($jscode) {
  	$this->common_document_ready_js[] = $jscode;
  }

  public function get_js_document_ready() {
  	return $this->common_document_ready_js;
  }

  public function print_document_ready_js() {
	$arr_docready_js = $this->common_document_ready_js;
	$str_docready_js = '';
	foreach($arr_docready_js as $indv_js) {
		$str_docready_js .= $indv_js."\n";
	}
	return $str_docready_js;
  }

  public function set_js($jscode) {
  	$this->common_js[] = $jscode;
  }

  public function get_js() {
  	return $this->common_js;
  }

  public function print_js() {
	$arr_js = $this->common_js;
	$str_js = '';
	foreach($arr_js as $indv_js) {
		$str_js .= $indv_js."\n";
	}
	return $str_js;
  }

  public function set_action_js($action='default', $jscode) {
  	$this->common_action_js[$action][] = $jscode;
  }

  public function get_action_js($action='default') {
  	return $this->common_action_js[$action];
  }

  public function print_action_js($action='default') {
	$arr_js = isset($this->common_action_js[$action]) ? $this->common_action_js[$action] : array();
	$str_js = '';
	foreach($arr_js as $indv_js) {
		$str_js .= $indv_js."\n";
	}
	return $str_js;
  }

  public function get_append_query() {
	  return $this->append_sql;
  }

  public function customer_group_id() {
	  return $this->customer_group_id;
  }

  public function customer_access_group_id() {
	return $this->customer_access_group_id;
  }

  public static function install_addon($addon_code) {
	  
	if(!empty($addon_code)) {
		$arr_addon_name = (array) json_decode(ADDONS_INSTALLED, true);
		array_push($arr_addon_name, $addon_code);

		//Insert the code in addons table
		tep_db_query("DELETE FROM lcaddons WHERE code='$addon_code'");		
		$query = "INSERT INTO lcaddons SET code='$addon_code', status=1, date_installed='". date('Y-m-d') ."'";
		tep_db_query($query);

		//Check if any changes in DB
		$db_file_name = DIR_FS_DOCUMENT_ROOT.'addons/'. $addon_code .'/reference_db.xml';
		if(file_exists($db_file_name))
		{
			//restructure the table
			require_once('includes/classes/tableProcessor.php');
			$tblProcessor = new tableProcessor();

			// call the method to encode the current table
			$currTable = $tblProcessor->getTableStructure();

			// call the method to encode the reference table
			$refTable = file_get_contents($db_file_name);

			// now what is needed to conver the current table into the same structure as the reference table
			$diffTable = $tblProcessor->diffTableStructure($currTable, $refTable, false, false);

			unset($refTable);

			$action_results = $tblProcessor->applyTableChanges($diffTable);
			$result = array();
			foreach ($action_results as $result) {
				if ($result['success'] == 'FALSE') {
				  $result['_RESULT'] = array('code' => 'error',
											  'action' => 'install '.$addon_code,
											  'msg' => $result['sql'] . '<br>' . $result['msg']
											 );
				}
			}
		}

		//check and update if any config changes
		$config_file_name = DIR_FS_DOCUMENT_ROOT.'addons/'. $addon_code .'/reference_sql.xml';
		if(file_exists($config_file_name))
		{
			$xml=simplexml_load_file($config_file_name);
			$arr_sql = $xml->sql;
			foreach($arr_sql as $sql) {
				if(trim($sql) != '')
					tep_db_query($sql);
			}
		}
		$query = "UPDATE configuration SET configuration_value='". json_encode($arr_addon_name) ."' WHERE configuration_key='ADDONS_INSTALLED'";
		tep_db_query($query);
	}

  }

  public static function remove_addon($addon_code) {
	  $arr_addon_name = (array) json_decode(ADDONS_INSTALLED, true);
	  if (($key = array_search($addon_code, $arr_addon_name)) !== false) {
		unset($arr_addon_name[$key]);
	  }
	  $query = "UPDATE configuration SET configuration_value='". json_encode($arr_addon_name) ."' WHERE configuration_key='ADDONS_INSTALLED'";
	  tep_db_query($query);
	  tep_db_query("DELETE FROM lcaddons WHERE code='$addon_code'");		
  }
}
?>
