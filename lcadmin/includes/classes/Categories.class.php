<?php
class Categories {
	public static function getCategoryName($cPath) {
		global $languages_id;

		if ($cPath == '') {
			$cPath == 0;
		} else {
			$parts = explode("_", $cPath);

      if (count($parts) == 1) {
      	$id = $parts[0];
      } else {
      	array_pop($parts);
      	$id = end($parts);
      }
		}

		$cQuery = tep_db_query("SELECT categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " WHERE categories_id = '" . $id . "' and language_id = '" . $languages_id . "'");
		$cData = tep_db_fetch_array($cQuery);
    
    if ($id == 0) {
    	return 'Root Category';
    } else {
		  return (isset($cData['categories_name'])) ? $cData['categories_name'] : 'Root Category';       
		}
	}
}
?>