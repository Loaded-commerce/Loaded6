<?php
/*
  $Id: split_page_results.php,v 1.1.1.1 2004/03/04 23:39:49 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  class splitPageResults {
    function __construct(&$current_page_number, $max_rows_per_page, &$sql_query, &$query_num_rows) {
      if (empty($current_page_number)) $current_page_number = 1;
      $sql_query = strtolower($sql_query);
      $pos_to = strlen($sql_query);
      $pos_from = strpos($sql_query, ' from', 0);

      $pos_group_by = strpos($sql_query, ' group by', $pos_from);
      if (($pos_group_by < $pos_to) && ($pos_group_by != false)) $pos_to = $pos_group_by;

      $pos_having = strpos($sql_query, ' having', $pos_from);
      if (($pos_having < $pos_to) && ($pos_having != false)) $pos_to = $pos_having;

      $pos_order_by = strpos($sql_query, ' order by', $pos_from);
      if (($pos_order_by < $pos_to) && ($pos_order_by != false)) $pos_to = $pos_order_by;

      $reviews_count_query = tep_db_query("select count(*) as total " . substr($sql_query, $pos_from, ($pos_to - $pos_from)));
      $reviews_count = tep_db_fetch_array($reviews_count_query);
      $query_num_rows = $reviews_count['total'];

      $num_pages = ceil($query_num_rows / $max_rows_per_page);
      if ($current_page_number > $num_pages) {
        $current_page_number = $num_pages;
      }
      $offset = ($max_rows_per_page * ($current_page_number - 1));
      //newer version of mysql can not handle neg number in limit, temp fix
      if ($offset < '0'){
         $offset = '1';
         }
      $sql_query .= " limit " . $offset . ", " . $max_rows_per_page;
    }

    function display_links($query_numrows, $max_rows_per_page, $max_page_links, $current_page_number, $parameters = '', $page_name = 'page') {
      global $PHP_SELF;

      if ( tep_not_null($parameters) && (substr($parameters, -1) != '&') ) $parameters .= '&';

// calculate number of pages needing links
      $num_pages = ceil($query_numrows / $max_rows_per_page);

      $pages_array = array();
      for ($i=1; $i<=$num_pages; $i++) {
        $pages_array[] = array('id' => $i, 'text' => $i);
      }

      if ($num_pages > 1) {
      	$call_page_name = basename($PHP_SELF);
      	$route_param = '';
      	if($call_page_name == 'addons.php') {
      		$route_param = 'routes='.$_GET['routes'];
      		if($parameters != '')
      			$parameters .= '&routes='.$_GET['routes'].'&';
      		else	
      			$parameters .= 'routes='.$_GET['routes'].'&';
      	}
        $display_links = tep_draw_form('pages', basename($PHP_SELF), $route_param, 'get');
        $display_links .= '<div class="input-group" style="width:120px;">';

        if ($current_page_number > 1) {
          $display_links .= '<span class="input-group-addon"><a class="m-t-4" href="' . tep_href_link(basename($PHP_SELF), $parameters . $page_name . '=' . ($current_page_number - 1), 'NONSSL') . '" class="m-t-5 mr-2"><i class="fa fa-angle-double-left"></i></a></span>';
        } else {
          $display_links .= '<span class="input-group-addon disabled"><a class="m-t-4" href="javascript:;"><i class="fa fa-angle-double-left text-secondary"></i></a></span>';
        }

        $display_links .= tep_draw_pull_down_menu($page_name, $pages_array, $current_page_number, 'onChange="this.form.submit();" class="form-control d-inline" style="width:60px;"');

        if (($current_page_number < $num_pages) && ($num_pages != 1)) {
          $display_links .= '<span class="input-group-addon"><a href="' . tep_href_link(basename($PHP_SELF), $parameters . $page_name . '=' . ($current_page_number + 1), 'NONSSL') . '" class="splitPageLink"><i class="fa fa-angle-double-right"></i></a></span>';
        } else {
          $display_links .= '<span class="input-group-addon disabled"><a href="javascript:;"><i class="fa fa-angle-double-right text-secondary"></i></a></span>';          
        }

        if ($parameters != '') {
          if (substr($parameters, -1) == '&') $parameters = substr($parameters, 0, -1);
          $pairs = explode('&', $parameters);
		  foreach($pairs as $pair) {
            list($key,$value) = explode('=', $pair);
            $display_links .= tep_draw_hidden_field(rawurldecode($key), rawurldecode($value));
          }
        }

        if (SID) $display_links .= tep_draw_hidden_field(tep_session_name(), tep_session_id());

        $display_links .= '  </div>';
        $display_links .= '</form>';
      } else {
        $display_links = sprintf(TEXT_RESULT_PAGE, $num_pages, $num_pages);
      }

      return $display_links;
    }

    function display_count($query_numrows, $max_rows_per_page, $current_page_number, $text_output) {
      $to_num = ($max_rows_per_page * $current_page_number);
      if ($to_num > $query_numrows) $to_num = $query_numrows;
      $from_num = ($max_rows_per_page * ($current_page_number - 1));
      if ($to_num == 0) {
        $from_num = 0;
      } else {
        $from_num++;
      }

      return sprintf($text_output, $from_num, $to_num, $query_numrows);
    }
  }
?>
