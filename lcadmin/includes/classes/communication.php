<?php
/*
  $Id: communication.php,v 1.1.1.2 2020/06/04 23:37:57 devidash Exp $

  Loaded Commerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce

  Released under the GNU General Public License
*/

  class communication{

	public function __construct() {
		global $language;
	}  //end of __construct

	public static function sendToHostGet($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}

	public static function sendToHostPost($url, $data, $headers = array())
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_HTTPHEADER => $headers
		));

		$body = curl_exec($curl);
		$message = curl_error($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$info = curl_getinfo($ch);
		curl_close($curl);

		return $body;
	}
	
}