<?php
/*
  $Id: lc_admin_cache.php,v 1.1.1.2 2020/06/04 23:37:57 devidash Exp $

  Loaded Commerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce

  Released under the GNU General Public License
*/

  class lc_admin_cache{

	public function __construct() {
		global $language;
	}  //end of __construct

	public static function clean_all_cache()
	{
		//Refresh the left menu cache
		tep_db_query("TRUNCATE TABLE cache_data");
	}

	public static function clean_leftmenu_cache()
	{
		//Refresh the left menu cache
		tep_db_query("DELETE FROM cache_data WHERE cache_code LIKE'LEFT_MENU_DATA_%'");
	}

	public static function clean_cache($params)
	{

	}
	
}