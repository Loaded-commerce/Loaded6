<?php
class Products {
  public static function getAll($format = 'list', $exclude_pID = '') {
    global $languages_id;

    $products_query = tep_db_query("select pd.*, p.* from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "'");

    $result = array();
    switch ($format) {
      case 'list' :
        while($products = tep_db_fetch_array($products_query)) {

          if ($exclude_pID != '' && $exclude_pID == $products['products_id']) continue;

          $result[] = array('id' => $products['products_id'], 'text' => $products['products_name']);
        }

        break;

      default:
        while($products = tep_db_fetch_array($products_query)) {
          $result[] = $products;
        }
    }

    return $result;
  }

  public static function getProductsInCategory($category_id, $include_children = true) {
    global $languages_id;

    $products_query = tep_db_query("SELECT p.products_id, pd.products_name, p.products_quantity, p.products_image, p.products_price, p.products_date_added, p.products_last_modified, p.products_date_available, p.products_status, p.products_model, p.products_sku, p2c.categories_id
                                    FROM " . TABLE_PRODUCTS . " p,
                                         " . TABLE_PRODUCTS_DESCRIPTION . " pd,
                                         " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c
                                    WHERE p.products_id = pd.products_id
                                      and pd.language_id = " . (int)$languages_id . "
                                      and p.products_id = p2c.products_id
                                      and p2c.categories_id = " . (int)$category_id . "
                                      and p.products_parent_id = 0
                                    ORDER BY pd.products_name");

    $result = array();
    while ($products = tep_db_fetch_array($products_query)) {
  //    $pArr[] = $products;
      $result[] = array('id' => $products['products_id'], 'text' => $products['products_name']);
    }

    return $result;
  }

}
?>