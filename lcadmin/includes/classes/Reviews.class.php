<?php
class Reviews {
  public static function get($rID) {
    global $languages_id;

    //$rID = tep_db_prepare_input($_GET['rID']);
    $reviews_query = tep_db_query("SELECT r.reviews_id, r.products_id, r.customers_name, r.date_added, r.last_modified, r.reviews_read, rd.reviews_text, r.reviews_rating 
                                     from " . TABLE_REVIEWS . " r, 
                                          " . TABLE_REVIEWS_DESCRIPTION . " rd 
                                   WHERE r.reviews_id = '" . (int)$rID . "' 
                                     and r.reviews_id = rd.reviews_id");
    $reviews = tep_db_fetch_array($reviews_query);
    $products_query = tep_db_query("SELECT products_image 
                                      from " . TABLE_PRODUCTS . " 
                                    WHERE products_id = '" . (int)$reviews['products_id'] . "'");
    $products = tep_db_fetch_array($products_query);
    $products_name_query = tep_db_query("SELECT products_name 
                                           from " . TABLE_PRODUCTS_DESCRIPTION . " 
                                         WHERE products_id = '" . (int)$reviews['products_id'] . "' 
                                           and language_id = '" . (int)$languages_id . "'");
    $products_name = tep_db_fetch_array($products_name_query);
    $rInfo_array = array_merge((array)$reviews, (array)$products, (array)$products_name);
    $rInfo = new objectInfo($rInfo_array);

    return $rInfo;
  }
}
?>