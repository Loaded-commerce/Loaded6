<?php
/*
  $Id: manufacturers.php,v 1.2 2004/03/05 00:36:41 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Manufacturers');

define('TABLE_HEADING_MANUFACTURERS', 'Manufacturers');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_HEADING_NEW_MANUFACTURER', 'New Manufacturer');
define('TEXT_HEADING_EDIT_MANUFACTURER', 'Edit Manufacturer');
define('TEXT_HEADING_DELETE_MANUFACTURER', 'Delete Manufacturer');

define('TEXT_MANUFACTURERS', 'Manufacturers:');
define('TEXT_DATE_ADDED', 'Date Added:');
define('TEXT_LAST_MODIFIED', 'Last Modified:');
define('TEXT_PRODUCTS', 'Products:');

define('TEXT_NEW_INTRO', 'Please fill out the following information for the new manufacturer');
define('TEXT_EDIT_INTRO', 'Please make any necessary changes');

define('TEXT_MANUFACTURERS_NAME', 'Name:');
define('TEXT_MANUFACTURERS_IMAGE', 'Image:');
define('TEXT_MANUFACTURERS_URL', 'URL:');
define('TEXT_MANUFACTURERS_HTC_TITLE', 'HTC Title:');
define('TEXT_MANUFACTURERS_HTC_DESC', 'HTC Description:');
define('TEXT_MANUFACTURERS_HTC_KEYWORDS', 'HTC Keywords:');

define('TEXT_DELETE_INTRO', 'Are you sure you want to delete <b>%s</b>?');
define('TEXT_DELETE_IMAGE', 'Delete image?');
define('TEXT_DELETE_PRODUCTS', '<b>Delete all products from this manufacturer</b>? (includes product reviews, products on special and upcoming products)');
define('TEXT_DELETE_WARNING_PRODUCTS', 'There are <b>%s</b> products still linked to this manufacturer!');

define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Can not write to <b>%s</b>. Please check user permissions.');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Directory <b>%s</b> does not exist.');
define('EMPTY_MANUFACTURERS_ERROR_TEXT', 'Please enter the manufacturer name');
?>
