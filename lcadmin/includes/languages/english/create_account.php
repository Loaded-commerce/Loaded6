<?php
/*    
  $Id: create_account.php,v 2.0 2008/05/05 00:36:41 datazen Exp $    
  
  LoadedCommerce, Commerical Open Source eCommerce
  http://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce
  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Create an Account');
define('HEADING_TITLE', 'Add a New Customer');
define('HEADING_NEW', 'Order Process');
define('TEXT_ORIGIN_LOGIN', '<font color="#FF0000"><small><b>NOTE:</b></font></small> If you already have an account with us, please login at the <a href="%s"><u>login page</u></a>.');
define('TEXT_VISITOR_CART', '<font color="#FF0000"><b> We want to get your order to you as quickly as possible! We need the following information to process your order efficiently.</b></font>');
define('TEXT_RETURN_CUSTOMER', '<font color="#FF0000">Already have a profile? Please login!</font>');
define('NAVBAR_NEW_TITLE', 'Order Process');
define('TEXT_PRIVACY', '<font color="#FF0000"><b>Haven\'t bought from us before? Ordering is easy. Start by filling in all required fields, and then clicking the \'continue\' button. </b>');
define('ENTRY_CUSTOMERS_PAYMENT_SET', 'Set payment modules for the customer');
define('ENTRY_CUSTOMERS_PAYMENT_DEFAULT', 'Use settings from Group or Configuration');
define('ENTRY_CUSTOMERS_PAYMENT_SET_EXPLAIN', 'If you choose <b><i>Set payment modules for the customer</i></b> but do not check any of the boxes, default settings (Group settings or Configuration) will still be used.');
define('ENTRY_CUSTOMERS_SHIPPING_SET', 'Set shipping modules for the customer');
define('ENTRY_CUSTOMERS_SHIPPING_DEFAULT', 'Use settings from Group or Configuration');
define('ENTRY_CUSTOMERS_SHIPPING_SET_EXPLAIN', 'If you choose <b><i>Set shipping modules for the customer</i></b> but do not check any of the boxes, default settings (Group settings or Configuration) will still be used.');
define('ENTRY_SEND_PASSWORD', 'Send Password to Customer: ');

define('EMAIL_SUBJECT', 'Welcome to ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Dear Mr. ' . (isset($_POST['lastname']) ? stripslashes($_POST['lastname']) : '') . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Dear Ms. ' . (isset($_POST['lastname']) ? stripslashes($_POST['lastname']) : '') . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Dear ' . (isset($_POST['firstname']) ? stripslashes($_POST['firstname']) : '') . ',' . "\n\n");
define('EMAIL_WELCOME', 'We welcome you to <b>' . STORE_NAME . '</b>.' . "\n\n");
define('EMAIL_TEXT', 'You can now take part in the <b>various services</b> we have to offer you. Some of these services include:' . "\n\n" . '<li><b>Permanent Cart</b> - Any products added to your online cart remain there until you remove them, or check them out.' . "\n" . '<li><b>Address Book</b> - We can now deliver your products to another address other than yours! This is perfect to send birthday gifts direct to the birthday-person themselves.' . "\n" . '<li><b>Order History</b> - View your history of purchases that you have made with us.' . "\n" . '<li><b>Products Reviews</b> - Share your opinions on products with our other customers.' . "\n\n");
define('EMAIL_CONTACT', 'For help with any of our online services, please email us: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '<b>Note:</b> This email address was given to us by one of our customers. If you did not signup for a free member account, please send an Email to us: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n");
define('EMAIL_PASS_1', 'Your password for this account is ');
define('EMAIL_PASS_2', ', keep it in a safe place. (Please note: Your password is case sensitive.)');

define('ENTRY_CUSTOMERS_PAYMENT_SET', 'Set payment modules for the customer');
define('ENTRY_CUSTOMERS_PAYMENT_DEFAULT', 'Use settings from Group or Configuration');
define('ENTRY_CUSTOMERS_PAYMENT_SET_EXPLAIN', 'If you choose <b><i>Set payment modules for the customer</i></b> but do not check any of the boxes, default settings (Group settings or Configuration) will still be used.');
define('ENTRY_CUSTOMERS_SHIPPING_SET', 'Set shipping modules for the customer');
define('ENTRY_CUSTOMERS_SHIPPING_DEFAULT', 'Use settings from Group or Configuration');
define('ENTRY_CUSTOMERS_SHIPPING_SET_EXPLAIN', 'If you choose <b><i>Set shipping modules for the customer</i></b> but do not check any of the boxes, default settings (Group settings or Configuration) will still be used.');

?>