<?php
/*
  $Id: packingslip.php,v 1.3 2004/03/13 15:09:11 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
define('HEADING_TITLE', 'Packing Slip');
define('TABLE_HEADING_COMMENTS', 'Comments');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Model');
define('TABLE_HEADING_PRODUCTS', 'Products');
define('ENTRY_SOLD_TO', 'SOLD TO:');
define('ENTRY_SHIP_TO', 'SHIP TO:');
define('ENTRY_PHONE', 'Phone:');
define('ENTRY_EMAIL', 'E-Mail:');
define('ENTRY_ORDER_NUMBER', 'Order Number:');
define('BUTTON_PRINT', 'Print');
define('TEXT_THANK_YOU', 'THANK YOU FOR YOUR BUSINESS');
define('TEXT_ORDER_INFO', 'ORDER INFORMAITON');
?>