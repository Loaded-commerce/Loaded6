<?php
/*
  $Id: header_tags_popup_help.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
?>
<div class="popup-container">
  <p>Header Tags comes with a default set of tags. You can create your own set of tags for each page (it comes with some set up, like for index and product pages).</p>

  <ul>
    <li>HT = Header Tags</li>
    <li>T  = Title</li>
    <li>A  = All</li> 
    <li>D  = Description</li>
    <li>K  = Keywords</li>
    <li>C  = Categories *</li>
    <li>P  = Categories and Products **</li>
  </ul> 

  <p><b>* Note:</b> The HTCA option only works for the index and product_info pages. For the index page, it causes the category name to be displayed in the title. For the product_info page, if it is checked, the text in the boxes in Text Control will be appended to the title, description and keywords, respectively.</p>

  <p><b>** Note:</b> The HTPA option only works for the product_info page.  If it is checked, the name of the category the product is in will be added to the title and meta tags.</p>

  <p>If HTTA is set on (checked), then it says display the Header Tags Title All (default title plus the one you set up).</p>

  <p>So if you have the option checked, both titles will be displayed.  Let's say your title is Mysite and the default title is osCommerce.</p>

  <ul>
    <li>With HTTA on, the title is Mysite Loaded Commmerce</li>
    <li>With HTTA off, the title is Mysite</li>
  </ul>

  <p>If the name of the section is in <font color="red">red</font>, it means that that file does not have the required Header Tags code installed in it. See the Install_Catalog.txt file for instructions on how to do this.</p>

</div>