<?php
/* $Id$ 
osCommerce, Open Source E-Commerce Solutions 
http://www.oscommerce.com 
Copyright (c) 2002 osCommerce 

Released under the GNU General Public License 
xsell.php
Original Idea From Isaac Mualem im@imwebdesigning.com <mailto:im@imwebdesigning.com> 
Complete Recoding From Stephen Walker admin@snjcomputers.com
*/ 

define('CROSS_SELL_SUCCESS', 'Cross sell items successfully updated for <span class="f-w-600">%s</span>');
define('SORT_CROSS_SELL_SUCCESS', 'Sort order successfully updated for product #'.(isset($_GET['add_related_product_ID']) ? $_GET['add_related_product_ID'] : 0));
define('HEADING_TITLE', 'Cross-Sell Products');
define('TABLE_HEADING_PRODUCT_ID', 'ID');
define('TABLE_HEADING_PRODUCT_MODEL', 'Model');
define('TABLE_HEADING_PRODUCT_NAME', 'Name');
define('TABLE_HEADING_CURRENT_SELLS', 'Current Cross-Sells');
define('TABLE_HEADING_CROSS_SELLS', 'Cross-Sells');
//define('TABLE_HEADING_UPDATE_SELLS', 'Update Cross-Sells');
define('TABLE_HEADING_PRODUCT_IMAGE', 'Image');
define('TABLE_HEADING_PRODUCT_PRICE', 'Price');
define('TABLE_HEADING_CROSS_SELL_THIS', 'Cross-Sell?');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_EDIT_SELLS', 'Edit');
define('TEXT_SORT', 'Prioritize');
define('TEXT_SETTING_SELLS', 'Setting Cross-Sells for');
define('TEXT_PRODUCT_ID', 'Product Id');
define('TEXT_MODEL', 'Model');
define('TABLE_HEADING_PRODUCT_SORT', 'Sort Order');
define('TEXT_NO_IMAGE', 'No Image');
define('TEXT_CROSS_SELL', 'Cross-Sell');
define('TEXT_PRODUCTS_MODEL', 'Model:');
define('TEXT_INFO_EDIT_XSELL', 'Click the Edit button to edit cross-sells for <b>%s</b>.');
define('TEXT_DISPLAY_NUMBER_OF_XSELL', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> Products)');

?>
