<?php
/*
  $Id: categories.php,v 1.2 2004/03/05 00:36:41 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
define('TEXT_EDIT_PRICE_GROUP','Please use the base price in the &quot;Quantity Price Breaks &quot;');
define('TEXT_CATEGORIES_IMAGE_BROWSE','Browse your local computer and upload image. OR');
define('TEXT_CATEGORIES_IMAGE_TYPE','Type in known image name on your server');
define('TEXT_PARENT_PRODUCT_WARNING','This has been added as a Subproduct ');
define('TEXT_PARENT_PRODUCT_WARNING2','Click here');
define('TEXT_PARENT_PRODUCT_WARNING3',' to View the Parent Product');
define('TEXT_CATEGORIES_IMAGE_REMOVE_SHORT', '<b>Remove</b> this Image from this Category?');
define('TEXT_CATEGORIES_IMAGE_DELETE_SHORT', '<b>Delete</b> this Image from the Server (Permanent!)?');
define('TEXT_EXISTING_IMAGE','Existing Image');
define('TEXT_NO_IMAGE','No image assigned');
define('TEXT_UPLOAD_IMAGE','Upload a new image');
define('TEXT_UPLOAD_DESTINATION','Select Upload Folder');
define('TEXT_FILE_NAME_IMAGE','Image file name: ');
define('TEXT_SELECT_SHOW_IMAGE','Image on server');
define('TEXT_SELECT_DIRECTORY_BROWSE','Select a directory');
define('TEXT_BROWSE_CATEGORIES_IMAGE','Browse Categories image');
define('TEXT_SELECT_DIRECTORY_ROOT','/ ');
define('TEXT_SELECT_IMAGE_SERVER','Select image from server ');
define('TEXT_SELECT_IMAGE_DIR','Image Location on the Server');
define('TEXT_SELECT_CATEGORIES_IMAGE','Select an existing image');
define('TEXT_IMAGE_DIRECTORY','Select Image Directory');
define('TEXT_SELECT_EXISTING_IMAGE','Select an existing image');
define('TEXT_SELECT_DIRECTORY_UPLOAD','Select directory to upload');
define('TEXT_PRODUCT_IMAGE_NAME','Image Name:  ');
define('TEXT_UPLOAD_PRODUCTS_IMAGE','Upload Image');
define('TEXT_NOT_FOUND_IMAGE','No image found on server');
define('TEXT_PRODUCTS_IMAGE_NAME','Image Name: ');
define('TEXT_PRODUCTS_IMAGE_MEDIUM','Product Image');
define('TEXT_PRODUCTS_IMAGE_MEDIUM_DESC', 'Image for catalog & description pages');
define('TEXT_PRODUCTS_IMAGE_NOTE', 'Small Image');
define('TEXT_PRODUCTS_IMAGE_NOTE_DESC', 'Thumbnail image for product list pages');
define('TEXT_PRODUCTS_IMAGE_LARGE', 'Pop-up Image');
define('TEXT_PRODUCTS_IMAGE_LARGE_DESC', 'Large image for pop-up window pages');
define('TEXT_PRODUCTS_IMAGE_LINKED', '<u>Store Product/s Sharing this Image =</u>');
define('TEXT_PRODUCTS_IMAGE_REMOVE', '<b>Remove</b> this Image from this Product?');
define('TEXT_PRODUCTS_IMAGE_DELETE', '<b>Delete</b> this Image from the Server (Permanent!)?');
define('TEXT_PRODUCTS_IMAGE_REMOVE_SHORT', '<b>Remove</b> this Image from this Product?');
define('TEXT_PRODUCTS_IMAGE_DELETE_SHORT', '<b>Delete</b> this Image from the Server (Permanent!)?');
define('TEXT_PRODUCTS_IMAGE_TH_NOTICE', '<b>SM = Small Images.</b> If a "SM" image is used<br>(Alone) NO Pop-up window link is created, the "SM"<br> will be placed directly under the products<br>description. If used in conjunction with an <br>"XL" image on the right, a Pop-up Window Link<br> is created and the "XL" image will be<br>shown in a Pop-up window.<br><br>');
define('TEXT_PRODUCTS_IMAGE_XL_NOTICE', '<b>XL = Large Images.</b> Used for the Pop-up image<br><br><br>');
define('TEXT_PRODUCTS_IMAGE_ADDITIONAL', 'Additional Images:');
define('TEXT_PRODUCTS_IMAGE_SM_1', 'SM Image 1:');
define('TEXT_PRODUCTS_IMAGE_XL_1', 'XL Image 1:');
define('TEXT_PRODUCTS_IMAGE_SM_2', 'SM Image 2:');
define('TEXT_PRODUCTS_IMAGE_XL_2', 'XL Image 2:');
define('TEXT_PRODUCTS_IMAGE_SM_3', 'SM Image 3:');
define('TEXT_PRODUCTS_IMAGE_XL_3', 'XL Image 3:');
define('TEXT_PRODUCTS_IMAGE_SM_4', 'SM Image 4:');
define('TEXT_PRODUCTS_IMAGE_XL_4', 'XL Image 4:');
define('TEXT_PRODUCTS_IMAGE_SM_5', 'SM Image 5:');
define('TEXT_PRODUCTS_IMAGE_XL_5', 'XL Image 5:');
define('TEXT_PRODUCTS_IMAGE_SM_6', 'SM Image 6:');
define('TEXT_PRODUCTS_IMAGE_XL_6', 'XL Image 6:');
define('HEADING_TITLE', 'Categories / Products');
define('HEADING_TITLE_SEARCH', 'Search:');
define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_CATEGORIES_PRODUCTS', 'Categories / Products');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');
define('TEXT_NEW_PRODUCT', 'New Product in %s');
define('TEXT_CATEGORIES', 'Categories:');
define('TEXT_SUBCATEGORIES', 'Subcategories:');
define('TEXT_PRODUCTS', 'Products:');
define('TEXT_PRODUCTS_PRICE_INFO', 'Price:');
define('TEXT_PRODUCTS_TAX_CLASS', 'Tax Class:');
define('TEXT_PRODUCTS_AVERAGE_RATING', 'Average Rating:');
define('TEXT_PRODUCTS_QUANTITY_INFO', 'Quantity:');
define('TEXT_DATE_ADDED', 'Date Added:');
define('TEXT_DELETE_IMAGE', 'Delete Image');
define('TEXT_DATE_AVAILABLE', 'Date Available:');
define('TEXT_LAST_MODIFIED', 'Last Modified:');
define('TEXT_NO_CHILD_CATEGORIES_OR_PRODUCTS', 'Please insert a new category or product in this level.');
define('TEXT_PRODUCT_MORE_INFORMATION', 'For more information, please visit this products <a href="http://%s" target="blank"><u>webpage</u></a>.');
define('TEXT_PRODUCT_DATE_ADDED', 'This product was added to our catalog on %s.');
define('TEXT_PRODUCT_DATE_AVAILABLE', 'This product will be in stock on %s.');
define('TEXT_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_EDIT_CATEGORIES_ID', 'Category ID:');
define('TEXT_EDIT_CATEGORIES_NAME', 'Category Name:');
define('TEXT_EDIT_CATEGORIES_IMAGE', 'Category Image:');
define('TEXT_CHANGE_CATEGORIES_IMAGE', 'Change or New Category Image:');
define('TEXT_SELECT_CATEGORIES_IMAGE_DIR','Select an existing Image Directory');
define('TEXT_EDIT_SORT_ORDER', 'Sort Order:');
define('TEXT_EDIT_CATEGORIES_HEADING_TITLE', 'Heading Title:');
define('TEXT_EDIT_CATEGORIES_DESCRIPTION', 'Description:');
define('TEXT_EDIT_CATEGORIES_TITLE_TAG', 'Title Meta Tag :');
define('TEXT_EDIT_CATEGORIES_DESC_TAG', 'Description Meta Tag :');
define('TEXT_EDIT_CATEGORIES_KEYWORDS_TAG', 'Key Word Meta Tag :');
define('TEXT_INFO_COPY_TO_INTRO', 'Please choose a new category you wish to copy this product to');
define('TEXT_INFO_CURRENT_CATEGORIES', 'Current Categories:');
define('TEXT_INFO_HEADING_NEW_CATEGORY', 'New Category in %s');
define('TEXT_INFO_HEADING_EDIT_CATEGORY', 'Edit Category : %s ');
define('TEXT_INFO_HEADING_DELETE_CATEGORY', 'Delete Category');
define('TEXT_INFO_HEADING_MOVE_CATEGORY', 'Move Category');
define('TEXT_INFO_HEADING_DELETE_PRODUCT', 'Delete Product');
define('TEXT_INFO_HEADING_MOVE_PRODUCT', 'Move Product');
define('TEXT_INFO_HEADING_COPY_TO', 'Copy To');
define('TEXT_INFO_HEADING_EDIT_PRODUCT', 'Edit Product : %s ');
define('TEXT_DELETE_CATEGORY_INTRO', 'Are you sure you want to delete category <b>%s</b>?');
define('TEXT_DELETE_PRODUCT_INTRO', 'Are you sure you want to delete product <b>%s</b>?');
define('TEXT_DELETE_WARNING_CHILDS', 'There are %s child categories still linked to this category. Deleting this category will delete all child categories and products within.');
define('TEXT_DELETE_WARNING_PRODUCTS', 'There are %s products still linked to this category. Deleting this category will delete all products within.');

define('TEXT_MOVE', 'Move <b>%s</b> to:');
define('TEXT_NEW_CATEGORY_INTRO', 'Please fill out the following information for the new category');
define('TEXT_CATEGORIES_NAME', 'Category Name:');
define('TEXT_CATEGORIES_IMAGE', 'Category Image:');
define('TEXT_SORT_ORDER', 'Sort Order:');
define('TEXT_PRODUCTS_STATUS', 'Status:');
define('TEXT_PRODUCTS_DATE_AVAILABLE', 'Date Avail:');
define('TEXT_PRODUCT_AVAILABLE', 'In Stock');
define('TEXT_PRODUCT_NOT_AVAILABLE', 'Out of Stock');
define('TEXT_PRODUCTS_MANUFACTURER', 'Manufacturer:');
define('TEXT_PRODUCTS_NAME', 'Name:');
define('TEXT_PRODUCTS_DESCRIPTION', 'Description:');
define('TEXT_PRODUCTS_QUANTITY', 'Quantity:');
define('TEXT_PRODUCTS_MODEL', 'Model:');
define('TEXT_PRODUCTS_SKU', 'Sku:');
define('TEXT_PRODUCTS_IMAGE', 'Image:');
define('TEXT_PRODUCTS_URL', 'URL:');
define('TEXT_PRODUCTS_URL_WITHOUT_HTTP', '<small>(without http://)</small>');
define('TEXT_PRODUCTS_PRICE_NET', 'Net Price:');
define('TEXT_PRODUCTS_PRICE_GROSS', 'Gross Price:');
define('TEXT_PRODUCTS_WEIGHT', 'Weight:');
define('TEXT_PRODUCTS_IMAGE_MAIN','Image Options');
define('EMPTY_CATEGORY', 'Empty Category');
define('TEXT_PRODUCTS_EXTRA_FIELDS','Extra Fields');
define('TEXT_HOW_TO_COPY', 'Copy Method :');
define('TEXT_COPY_AS_LINK', 'Link product');
define('TEXT_COPY_AS_DUPLICATE', 'Duplicate product');
define('ERROR_CANNOT_LINK_TO_SAME_CATEGORY', 'Error: Can not link products in the same category.');
define('ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT', 'Error: Category cannot be moved into child category.');
define('TEXT_PRODUCT_METTA_INFO', 'Meta Tags');
define('TEXT_PRODUCTS_PAGE_TITLE', 'Page Title:');
define('TEXT_PRODUCTS_HEADER_DESCRIPTION', 'Meta Description:');
define('TEXT_PRODUCTS_KEYWORDS', 'Meta Keywords :');
define('IMAGE_EDIT_ATTRIBUTES', 'Edit Product Attributes');
// mod for sub products
define('MAX_PRODUCT_SUB_ROWS', '2');
define('TEXT_SUB_PRODUCT','Inventory Control Options (Sub Products)');
define('TEXT_SUB_PRODUCT_DELETE','Delete');
define('TEXT_SUB_PRODUCT_NAME','Name');
define('TEXT_SUB_PRODUCT_PRICE','Price');
define('TEXT_SUB_PRODUCT_MODEL','Model');
define('TEXT_SUB_PRODUCT_QTY','Qty');
define('TEXT_SUB_PRODUCT_WEIGHT','Weight');
define('TEXT_SUB_PRODUCT_IMAGE','Image');
define('TEXT_SUB_PRODUCT_NOTE_1','* additional subproducts available upon insert');
define('TEXT_SUB_PRODUCT_NOTE_2','* zero quantity disables the subproduct');
define('TEXT_SUB_PRODUCT_SUBPRODUCT','This Product is a sub product');
// mod for sppc and qty price breaks
define('TEXT_PRODUCTS_PRICE_GRP', 'Quantity Price Breaks');
define('TEXT_PRODUCTS_PRICE', 'Retail Price:');
define('TEXT_PRODUCTS_GROUPS', 'Groups:');
define('TEXT_PRODUCTS_BASE', 'Base');
define('TEXT_PRODUCTS_ABOVE', 'Above');
define('TEXT_PRODUCTS_QTY', 'Qty');
define('TEXT_PRODUCTS_QTY_BLOCKS', 'Quantity Blocks:');
define('TEXT_PRODUCTS_QTY_BLOCKS_INFO', '(can only order in blocks of X quantity)');
define('TEXT_PRODUCTS_SPPP_NOTE', 'Note that if a field is filled, but the checkbox is unchecked no price will be inserted.<br>If a price is already inserted in the database, but the checkbox unchecked it will be removed from the database.');
define('TEXT_PRODUCTS_QTY_DISCOUNT', '10');
// mod for sppc and qty price breaks eof
define('PRODUCT_LIST_MODEL','Product Model');
define('PRODUCT_LIST_NAME','Product Name');
define('PRODUCT_LIST_MANUFACTURER','Manufacturer');
define('PRODUCT_LIST_PRICE','Price');
define('TEXT_PRODUCT_IMAGES', 'Product Images');
define('TEXT_EXTRA_FIELDS', 'Extra Fields');
define('TEXT_EXTRA_IMAGES', 'Extra Images');
define('TEXT_ACTIVE_ATTRIBUTES', 'Active Attributes');
define('IMAGE_COPY_ATTRIBUTES', 'Copy Attributes');
define('TEXT_COPY_ATTRIBUTES_TO_ANOTHER_PRODUCT', 'Copy Attributes to another product');
define('TEXT_COPYING_ATTRIBUTES_FROM', 'Copying Attributes from');
define('TEXT_COPYING_ATTRIBUTES_TO', 'Copying Attributes to');
define('TEXT_DELETE_ALL_ATTRIBUTE', 'Delete ALL Attributes and Downloads before copying');
define('TEXT_OTHERWISE', 'Otherwise ...');
define('TEXT_DUPLICATE_ATTRIBUTES_SKIPPED', 'Duplicate Attributes should be skipped');
define('TEXT_DUPLICATE_ATTRIBUTES_OVERWRITTEN', 'Duplicate Attributes should be overwritten');
define('TEXT_COPY_ATTRIBUTES_WITH_DOWNLOADS', 'Copy Attributes with Downloads');
define('TEXT_SELECT_PRODUCT_FOR_DISPLAY', 'Select a product for display');
define('TEXT_COPY_PRODUCT_ATTRIBUTES_TO_CATEGORY', 'Copy Product Attributes to Category ...');
define('TEXT_COPY_PRODUCT_ATTRIBUTES_FROM_PRODUCT_ID', 'Copy Product Attributes from Product ID');
define('TEXT_COPYING_TO_ALL_PRODUCTS_IN_CATEGORY_ID', 'Copying to all products in Category ID');
define('TEXT_CATEGORY_NAME', 'Category Name: ');
define('TEXT_SELECT_PRODUCT_TO_DISPLAY_ATTRIBUTES', 'Select a product to display attributes');
define('TEXT_PRODUCTS_BLURB', 'Products Blurb:');
// Dimensional UPS begin
define('TEXT_PRODUCTS_HEIGHT', 'Height:');
define('TEXT_PRODUCTS_LENGTH', 'Length:');
define('TEXT_PRODUCTS_WIDTH', 'Width:');
define('TEXT_PRODUCTS_READY_TO_SHIP', 'Ready to ship:');
define('TEXT_PRODUCTS_READY_TO_SHIP_SELECTION', 'Product can be shipped in its own container.');
// Dimensional UPS end
define('TEXT_PRODUCTS_GROUP_ACCESS','Access Group');
define('CAT_GUEST', ' Guest');
define('CAT_SHOW', ' Show');
define('CAT_HIDE', ' Hide');
define('TEXT_PUSH_SAVE_OPTION','Save Options');
define('TEXT_DO_NOT_PUSH','Do not Push Access');
define('TEXT_PUSH_SUB_CATEGORIES','Push access to all sub categories');
define('TEXT_PUSH_SUB_CATEGORIES_PRODUCTS','Push access to all sub categories and products');
define('TEXT_PUSH_WARNING','Warning : may time out if this is a top level and you have a large number of products and categories');
define('TEXT_EXISTING_CATEGORIES_IMAGE','Existing Image');
define('TEXT_ACCESS', 'All');
define('TEXT_SHOW_IN_NAV', 'Show In Navigation');
define('TABLE_HEADING_SORT_ORDER', 'Sort');
define('IMAGE_UPDATE_SORT_ORDER', 'Update Sort Order');
define('TEXT_GENERAL_OPTIONS', 'Options');

define('TEXT_LIST_ATTRIBUTES', 'List Attributes');
define('TEXT_ATTRIBUTES_NAMES_HELPER', '<a href="' . tep_href_link('quick_attributes_popup.php','look_it_up=%s&my_languages_id=' . $languages_id ) . '" onclick="NewWindow2(this.href,\'name2\',\'700\',\'400\',\'yes\');return false;">List Attributes for pID: </a>');
define('TEXT_BUTTON_COPY_ATTRIBUTES', 'Copy Attributes');
define('TEXT_ATTRIBUTES_COPY_TO', 'Select a category to copy attributes to ');
define('TEXT_ATTRIBUTES_PRODUCT_LOOKUP','<a href="' . 'quick_products_popup.php' . '" onclick="NewWindow(this.href,\'name\',\'700\',\'500\',\'yes\');return false;">Pop-Up Product ID# Search</a>');

define('TEXT_TAB_PANE_NEW_FILE', 'New File');
define('TEXT_TAB_PANE_EXISTING_FILE', 'Existing File');
define('TEXT_CATEGORIES_IMAGE_DIR', 'Image Dir: ');
define('TEXT_CATEGORIES_IMAGE_FILE', 'Image File: ');
// multi-vendor shipping
define('TEXT_VENDOR', 'Vendor:');
define('TEXT_PRODUCTS_VENDORS', 'Product Vendors:');
define('TEXT_VENDORS_PRODUCT_PRICE_BASE', 'Vendor Price (Base):');
define('TEXT_VENDORS_PROD_COMMENTS', 'Vendor Comments or Special Instructions:');
define('TEXT_VENDORS_PROD_ID', 'Vendor Item Number:');
define('TEXT_VENDORS_PRODUCT_PRICE_INFO', 'Vendors Price:');
// multi-vendor shipping //eof

define('TEXT_QTY_BASED_HISTORY', 'Quantity Based on History: ');
define('TEXT_PRODUCTS_QTY_DAYS', 'Days');
define('TEXT_PRODUCTS_QTY_YEARS', 'Years');

//product notification
define('PRODUCT_NOTIFCATION_EMAIL_SUBJECT','Notification: %s');
define('PRODUCT_NOTIFCATION_EMAIL_CUSTOMER','Hello %s, '."\n\n");
define('PRODUCT_NOTIFCATION_EMAIL_START', 'You have Requested to be updated as to changes concerning the following product: . "\n" .  "%s" ' ."\n\n");
define('PRODUCT_NOTIFCATION_EMAIL_BODY','To get more information on the changes made to them please log into our online store at:' . "\n" . ' %s ' . "\n\n\n");
define('PRODUCT_NOTIFCATION_EMAIL_END','Please feel free to contact us should you have any questions:' ."\n" .  nl2br(STORE_NAME_ADDRESS) . "\n" . STORE_OWNER_EMAIL_ADDRESS . "\n\n");
define('PRODUCT_NOTIFCATION_EMAIL_SUCCESS','Product Notification sent to: %s');

//Margin Reports & MSRP
define('TEXT_PRODUCTS_COST_INFO', 'Cost: ');
define('TEXT_PRODUCTS_PROFIT_INFO', 'Profit:');
define('TEXT_PRODUCTS_PRICE_COST', 'Products Cost:');
define('TEXT_PRODUCTS_MSRP', 'MSRP');
define('TEXT_SUB_PRODUCT_MSRP', 'MSRP');
define('TEXT_PRODUCTS_MSRP2', 'MSRP: ');
define('TEXT_PRODUCTS_PRICE_MSRP', 'MSRP Price:');
define('TEXT_PRODUCTS_OUR_PRICE', 'Our Price: ');
define('TEXT_PRODUCTS_SALE', 'SalePrice: ');
define('TEXT_PRODUCTS_SAVINGS', 'You Save: ');
define('TEXT_PRODUCTS_PRICE_MSRP', 'Products MSRP:');
define('TEXT_PRODUCTS_MSRP_INFO', 'MSRP Info:');

// DZ added for new template
/*
 * CATEGORY ADD/EDIT
 */
define('LABEL_IMAGE', 'Image:');
define('LABEL_PAGE_HEADING', 'Page Heading:');

/*
 * PRODUCT ADD/EDIT
 */
define('HEADING_CATALOG', 'Catalog');
define('HEADING_COPY_ATTRIBUTES', 'Copy Attributes');

define('HEADING_COPY_TO', 'Copy To');
define('HEADING_DELETE_PRODUCT', 'Delete Product');
define('HEADING_EXTRA_FIELDS', 'Extra Fields');
define('HEADING_IMAGES', 'Images');
define('HEADING_INVENTORY', 'Inventory');
define('HEADING_META_TAGS', 'Meta Tags');
define('HEADING_MOVE_CATEGORY', 'Move Category');
define('HEADING_MOVE_PRODUCT', 'Move Product');
define('HEADING_PRICING_RULES', 'Pricing Rules');
define('HEADING_SUBPRODUCTS', 'Sub Products');
define('HEADING_USER_ACCESS_SETTINGS', 'User Access Settings');

define('BUTTON_RETURN_TO_LIST', 'Return to List');
define('BUTTON_VIEW_IN_CATALOG', 'View in Catalog');

define('LABEL_BASE_PRICE', 'Base Price:');
define('LABEL_COPY_TO_INTRO', 'Copy this product to:');

define('LABEL_CURRENT_CATEGORY', 'Current Category:');
define('LABEL_DATE_AVAILABLE', 'Available:');
define('LABEL_DELETE', 'Delete:');
define('LABEL_DESCRIPTION', 'Description:');
define('LABEL_ENABLE', 'Enable:');
define('LABEL_FEATURED_PRODUCTS', 'Featured:');
define('LABEL_GOTO', 'Go To:');
define('LABEL_GROUP_PRICE_OVERRIDES', 'Customer Group Pricing Overrides:');
define('LABEL_ITEM_COST', 'Item Cost:');
define('LABEL_LARGE_IMAGE', 'Large Image:');
define('LABEL_LISTING_BLURB', 'Listing Blurb:');
define('LABEL_MAIN_IMAGE', 'Main Image:');
define('LABEL_META_DESCRIPTION', 'Meta Description:');
define('LABEL_META_KEYWORDS', 'Meta Keywords:');
define('LABEL_META_TITLE', 'Title Meta Tag:');
define('LABEL_MODEL', 'Model:');
define('LABEL_MOVE_INTRO', 'Select the category for <b>%s</b> to reside in:');
define('LABEL_MSRP', 'MSRP:');
define('LABEL_NAME', 'Name:');
define('LABEL_MANUFACTURER','Manufacturer:');
define('LABEL_PRICE_WITH_TAX', 'Price w/Tax:');
define('LABEL_QTY_PRICE_BREAKS', 'Quantity Price Breaks:');
define('LABEL_QUANTITY', 'Quantity:');
define('LABEL_RESTRICT_ACCESS', 'Restrict Access:');
define('LABEL_SKU', 'SKU:');
define('LABEL_SORT_ORDER', 'Sort Order:');
define('LABEL_SPECIAL_PRICE', 'Special Price:');
define('LABEL_SPECIAL_PRICE_PERCENTAGE', 'Special Price Percentage:');
define('LABEL_STATUS', 'Status:');
define('LABEL_TAX_CLASS', 'Tax Class:');
define('LABEL_THUMBNAIL_IMAGE', 'Thumbnail Image:');
define('LABEL_UNLINK', 'Unlink:');
define('LABEL_URL', 'URL:');
define('LABEL_VENDOR', 'Vendor:');
define('LABEL_VENDOR_NOTE', 'Vendor Note:');
define('LABEL_WEIGHT', 'Weight:');
define('LABEL_PERMALINK', 'Permalink:');

define('OPTION_SELECT_VENDOR', 'Select Vendor');

define('TABLE_SUBPRODUCT_NAME', 'Sub Product Name');
define('TABLE_SUBPRODUCT_MODEL', 'Model');
define('TABLE_SUBPRODUCT_PRICE', 'Price');
define('TABLE_SUBPRODUCT_ACTION', 'Action');

define('TEXT_COPY_LANG_TO_CLIPBOARD', 'Copy %s to Clipboard');
define('TEXT_CHOOSE_FILE', 'Click to select');

define('INFO_CREATE_NEW_CATEGORY_TITLE', 'Creating a New Category in ');
define('INFO_CREATE_NEW_CATEGORY_TEXT', 'After saving, enter into this category to add products or more categories by clicking on the listing entry.');
define('INFO_CREATE_NEW_PRODUCT_TITLE', 'Creating a New Product in ');
define('INFO_CREATE_NEW_PRODUCT_TEXT', 'You can control when this product becomes visible in the catalog by using the status value.');
define('WARNING_ITEM_INACTIVE_TITLE', 'Item is inactive');
define('WARNING_ITEM_INACTIVE_TEXT', 'This item will not appear on the catalog because it is marked inactive.');
define('WARNING_ITEM_OUT_OF_STOCK_TITLE', 'Item is out of stock');
define('WARNING_ITEM_OUT_OF_STOCK_TEXT', 'This item may not appear on the catalog as it\'s quantity on hand is at or below 0.');

/*
 * COPY ATTRIBUTES MODAL
 */
define('HEADING_COPY_PRODUCT_ATTRIBUTES', 'Copy Product Attributes');

define('TEXT_OTHERWISE', 'Otherwise ...');
define('LABEL_COPY_PRODUCT_ATTRIBUTES_FROM', 'Copying attributes from (source):');
define('LABEL_COPY_PRODUCT_ATTRIBUTES_TO', 'Copying attributes to (target):');
define('LABEL_DELETE_ALL_ATTRIBUTES', 'Clear ALL attributes and downloads from target before copying?');
define('LABEL_SKIP_DUPLICATE_ATTRIBUTES', '-OR- Skip duplicate attributes?');
define('LABEL_OVERWRITE_DUPLICATE_ATTRIBUTES', '-OR- Overwrite duplicate attributes?');
define('LABEL_INCLUDE_DOWNLOAD_ATTRIBUTES', 'Include download attributes?');
define('LABEL_COPY_ALL_PRODUCTS_TO_CATEGORY', 'Copying attributes to ALL products within category:');

define('ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE', 'The catalog images directory is not writeable: <b>' . DIR_FS_CATALOG_IMAGES . '</b>');
define('ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'The catalog images directory does not exist: <b>' . DIR_FS_CATALOG_IMAGES . '</b>');
define('WARNING_CANNOT_COPY_TO_SAME_PRODUCT', 'Did not copy from <b>%s</b> to <b>%s</b> becuase it is the same product.');
define('WARNING_NO_ATTRIBUTES_FOUND', 'No attributes were found for <b>%s</b> therefore no copy was made.');
define('WARNING_TARGET_DOES_NOT_EXIST', 'Target product <b>%s<b> was not found therefore no copy was made.');
define('SUCCESS_ATTRIBUTES_COPIED', 'Product attributes for <b>%s</b> successfully copied to <b>%s</b>.');

define('TABLE_HEADING_TOP_NAV_STATUS', 'Top NAV Status');
define('TABLE_HEADING_INFOBOX_STATUS', 'Infobox Status');
define('TABLE_HEADING_CATEGORIES_MODEL', 'Products Model');

define('HEADING_SELECT_CATEGORIES', 'Categories');
define('SELECT_CATEGORIES', 'Select Categories:');
?>
