<?php
/*
  $Id: countries.php,v 1.2 2004/03/05 00:36:41 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Countries');

define('TABLE_HEADING_COUNTRY_NAME', 'Country');
define('TABLE_HEADING_COUNTRY_CODE_2', 'ISO-2');
define('TABLE_HEADING_COUNTRY_CODE_3', 'ISO-3');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes.');
define('TEXT_INFO_COUNTRY_NAME', 'Name:');
define('TEXT_INFO_COUNTRY_CODE_2', 'ISO Code (2):');
define('TEXT_INFO_COUNTRY_CODE_3', 'ISO Code (3):');
define('TEXT_INFO_ADDRESS_FORMAT', 'Address Format:');
define('TEXT_INFO_INSERT_INTRO', 'Please enter the new country information.');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete %s country?');
define('TEXT_INFO_HEADING_NEW_COUNTRY', 'New Country');
define('TEXT_INFO_HEADING_EDIT_COUNTRY', 'Edit Country');
define('TEXT_INFO_HEADING_DELETE_COUNTRY', 'Delete Country');
define('TABLE_HEADING_COUNTRY_STATUS', 'Country Status');
define('TABLE_HEADING_COUNTRY_ALLOWED', 'Country Allowed');
?>
