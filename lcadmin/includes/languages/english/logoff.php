<?php
/*
 $Id: login.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Administrator Log Out');
define('TEXT_MAIN', 'You have been successfully logged-off from the <span class="text-white">Admin</span> area. It is safe to leave the computer now.');
define('TEXT_VISIT_CATALOG', 'Visit Catalog');
define('TEXT_RETURN_TO_ADMIN', 'Return to Admin');
?>