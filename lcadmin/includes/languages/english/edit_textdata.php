<?php
/*

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define ('HEADING_TITLE', 'Edit Language Defines');

define('BUTTON_TEXT_SAVE_CHANGES', 'Save Changes');
define('BUTTON_TEXT_RESTORE_FILE', 'Restore File');
define('BUTTON_TEXT_RETURN_MAIN_EDIT', 'Return to Main Edit');
define('TEXT_RETURN_MAIN', 'Return to File List');

define('ERROR_TEXT_FILE_LOCKED', "ERROR: File '%s' is not writable");
define('ERROR_TEXT_BACKUP_SUCC', "Back up of file : '%s' : Successful!");

define('TEXT_HIDE_NAVIGATION', 'Hide Navigation');
define('TEXT_SHOW_NAVIGATION', 'Show Navigation');

define('TEXT_HELP_HELP', 'Quick Help (Edit Mode)');
define('TEXT_HELP_HELP1', '1. Click on the <span class="text-success">File Name</span> to edit.');
define('TEXT_HELP_HELP2', '2. When finished editing, click on <span class="text-success">Save</span>.');
define('TEXT_HELP_HELP3', '3. Once saved, click on <span class="text-success">Restore</span>.');
define('TEXT_HELP_HELP4', '4. Click on insert to add a new define to a language file. (Insert not functional at this time)');
define('TEXT_HELP_HELP5', '4. Click <span class="text-success">Return</span> to go to main screen.');

define('TEXT_DEFINE_LABEL', 'Define Label');
define('TEXT_DEFINE_TEXT', 'Display Text');
define('TEXT_RESTORE_FILE', 'Saved file : ');
define('TEXT_EDIT_FILE', 'Editing file: ');
define('TEXT_SAVE_FILE', 'Restored file : ');
define('TEXT_SET_LANGUAGE', 'Choose language: ');
define('TEXT_SET_ADMIN_CAT', 'Choose site: ');

define('TABLE_HEADING_FILE_TYPE', 'File type');
define('TABLE_HEADING_FILE_NAME', 'File name');
define('TABLE_HEADING_FILE_ACTION', 'Action');

define('TEXT_MIXED_CONSTANT', '(This variable is mixed please use advanced edtor to edit this define.)');
define('TEXT_ADV_EDITOR', 'Full Page Editor<br><small>For Advanced Users Only</small>');


define('TEXT_LANGUGES', 'languages');
define('TEXT_MSG_1', 'No matching defines found');
define('TEXT_LIST_DIR_IN', 'List of directories:');
define('TEXT_LIST_FILE_IN', 'List of files:');

define('TEXT_ADMIN_DIR', 'Admin');
define('TEXT_CAT_DIR', 'Catalog');
define('TEXT_OPTIONS', 'Options');
define('TEXT_DIRECTORY', 'Directory');

define('ERROR_CANNOT_RESTORE_FILE', 'Cannot restore file, one paramiter is missing');
define('ERROR_TEMP_FILE_NOT_CREATED', 'The temp file creation failed.');

define('MODAL_HELP_TITLE', 'Language Editor Help');

define('MODAL_HELP_INTRO', '<div class="lead">What is this?</div>
<p>This script allows you to quickly edit almost all displayed text in your Loaded Copmmerce language files.</p>
<p>Text is stored in two places, the database and language define files. Language Define Editor is entended to edit the language define file within the /languages directory or a sub directory. All language defines files should reside in the /languages directory.</p>
<div class="lead">Specific Task Help</div>
<ol>
  <li>Identifying the text to edit.</li>
  <ul>
    <li>Note the file name in the browser url box.</li>
    <li>Next note the text you want to change.</li>
  </ul>
  <li>Finding the correct file:</li>
  <ul>
    <li>Go to Tools > Define Languages.</li>
    <li>Select the directory the define file resides in.</li>
    <li>Click on the file name of the file.</li>
  </ul>
  <li>Searching for files:</li>
  <ul>
    <li>Go to Tools > Define Languages.</li>
    <li>Select the directory the define file resides in.</li>
    <li>In the search box type in all or part of the file name without the .php</li>
    <li>This will bring up a list of files that contain all or part of the text you type in the search box. Click on the file name you wish to edit.</li>
  </ul>
</ol>
<p>You can also go directly to a particular file and modify the text in that file.</p>
<div class="lead">What to do if you have a problem</div>
<p>Language define files have variables in them. If for some reason one of these get changed you can always revert back to the file before editng by clicking on restore. A restore button will apear after you save your edits. click on the "Restore File" button at the top of the page. The file will then take on the contents it had prior to the last change. You should always check your changes between each save.</p>');

define('MODAL_HELP_SEARCH', '<div class="lead">How search works</div>
<p>You can search one of two ways depending on which edit screen you are on. You can search for file names within the Languages define file. Search will detect which screen you are on either the directory/file screen, or the Language Define Edit Screen. It will switch the between file and define search logic automatically for you.</p>
<div class="lead">Where are the define files located?</div>
<ol>
  <li>includes\languages ~ In this directory all of the base define files reside. A base define file is common to more then one group of output screen.</li>
  <li>includes\languages\(Langauge Name) ~ this is where the bulk of the language defines reside.</li>
  <li>includes\languages\(Langauge Name)\images ~ Unique images that are language dependent.</li>
  <li>includes\languages\(Langauge Name)\modules ~ Specific to files that are in the includes/modules directory.</li>
  <li>includes\languages\(Langauge Name)\modules\order_total ~ Specific to files that are in the includes/modules/order_total directory.</li>
  <li>includes\languages\(Langauge Name)\modules\payment ~ Specific to files that are in the includes/modules/payment directory.</li>
  <li>includes\languages\(Langauge Name)\modules\shipping~ Specific to files that are in the includes/modules/shipping directory.</li>
</ol>
<div class="lead">Searching for a file name</div>
<ol>
  <li>Go to Tools > Define Languages.</li>
  <li>Select the directory the define file resides in.</li>
  <li>In the search box type in all or part of the file name without the .php</li>
  <li>This will bring up a list of files that contain all or part of the text you type in the search box. Click on the file name you wish to edit.</li>
</ol>');

define('MODAL_HELP_EDIT', '<div class="lead">Edit Language Defines</div>
<p>Once you have navigated to the correct file to edit you can then edit the language defines.</p>
<div class="lead">Specific Task Help.</div>
<ol>
  <li>Identifying the text to edit.</li>
  <ul>
    <li>Note the file name in the browser url box.</li>
    <li>Next note the text you want to change.</li>
  </ul>
  <li>Finding the correct file:</li>
  <ul>
    <li>Go to tools > Define Languages.</li>
    <li>Select the directory the define file resides in.</li>
    <li>Click on the file name of the file.</li>
  </ul>
</ol>
<div class="lead">Editing Language Defines</div>
<ol>
  <li>Type in the new text
   <li>Click on save next to the input box you just typed in.</li>
   <li>Double check your changes by viewing them in the store or admin.</li>
   <li>If they did not work or are incorrect, you can restore the previous text and change again in the editor.</li>
</ol>
<p class="f-w-600">Note: you can only sabe changes for one input box at a time.</p>
<div class="lead">Restoring Language Defines</div>
<ol>
  <li>Once you have saved the define your changes you can double check that it is correct.</li>
  <li>If it is not then you can click on the restore button to revert back to the previous langauge define.</li>
</ol>');

define('MODAL_HELP_SAVE', '<div class="lead">Save Your Work</div>
<p>Once you have finished editing a define, click on the save button next to the define. This will save the edit you just made. You will return to the define edit screen so you can check the changes. If for some reason it is not correct you can click on the restore button and this will revert the file back to what it was before the save.</p>
<div class="lead">Specific Task Help</div>
<ol>
  <li>Saving a new define.</li>
  <ul>
    <li>Note the file name in the browser url box.</li>
    <li>Next note the text you want to change.</li>
  </ul>
  <li>Finding the correct file:</li>
  <ul>
    <li>Go to tools > Define Languages.</li>
    <li>Select the directory the define file resides in.</li>
    <li>Click on the file name of the file.</li>
  </ul>
  <li>Searching for files:</li>
  <ul>
    <li>Go to tools > Define Languages.</li>
    <li>Select the directory the define file resides in.</li>
    <li>In the search box type in all or part of the file name without the .php</li>
    <li>This will bring up a list of files that contain all or part of the text you type in the search box. click on the file name you wish to edit.</li>
  </ul>
</ol>');

define('MODAL_HELP_RESTORE', '

<div class="lead">Restore Last Copy</div>

<p>Text is stored in two places, the database and language defines files. Language Define Editor is entended to edit the language define file within the /languages directory or a sub directory. All language defines files should reside in the /languages directory.</p>

<p>Language define files have variables in them. If for some reason one of these get changed you can always revert back to the file before editng by clicking on the Restore button. A restore button will apear after you save your edits. Click on the "Restore File" button at the top of the page. The file will then take on the contents it had prior to the last change. You should always check your changes between each save.</p>');
?>