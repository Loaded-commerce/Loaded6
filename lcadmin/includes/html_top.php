<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <title><?php echo TITLE; ?></title>
    <!-- favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/img/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/img/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/img/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Required meta tags -->
    <meta content="Create Great Looking Mobile Responsive E-commerce Sites. Designed not just for the challenges of today, but tomorrow. Open source. Unlimited Free Edition." name="description" />
    <meta content="Loaded Commerce" name="author" />
    <?php
    // themes: black, blue, default, red, orange, purple
    if (!isset($_SESSION['theme'])) $_SESSION['theme'] = (defined('ADMIN_THEME') && in_array(ADMIN_THEME, array("red","blue","default","black","purple","orange","green"))) ? ADMIN_THEME : 'default';
    if (isset($_GET['theme']) && in_array($_GET['theme'], array("red","blue","default","black","purple","orange","green"))) {
      $_SESSION['theme'] = $_GET['theme'];
    }
    // modes: dark, light
    $_SESSION['theme_mode'] = getThemeMode();

    function getThemeMode() {
      $query = tep_db_query("SELECT * FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'ADMIN_THEME_MODE'");
      $cArr = tep_db_fetch_array($query);

      return $cArr['configuration_value'];
    }
    ?>
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- link href="assets/plugins/bootstrap4/css/bootstrap.min.css" rel="stylesheet" / -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css?v=<?php echo rand(1,999); ?>" rel="stylesheet" />
    <link href="assets/css/animate.min.css?v=<?php echo rand(1,978); ?>" rel="stylesheet" />
    <link href="assets/css/style-bs4.css?v=<?php echo rand(1,997); ?>" rel="stylesheet" />
    <link href="assets/css/style.css?v=<?php echo rand(1,987); ?>" rel="stylesheet" />
    <link href="assets/css/style-responsive.css?v=<?php echo rand(1,987); ?>" rel="stylesheet" />
    <link href="assets/css/dark.css?v=<?php echo rand(1,987); ?>" rel="stylesheet" />
    <link href="assets/css/light.css?v=<?php echo rand(1,987); ?>" rel="stylesheet" />
    <link href="assets/css/theme/<?php echo $_SESSION['theme']; ?>.css" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="assets/plugins/jquery-jvectormap/jquery-jvectormap.css" rel="stylesheet" />
    <!-- link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" / -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.css" rel="stylesheet" />
    <!-- link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" / -->
    <link href="assets/plugins/morris/morris.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="assets/plugins/switchery/switchery.css" rel="stylesheet" />
    <link href="assets/plugins/jstree/dist/themes/default/style.css" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->
    <link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="assets/plugins/pace/pace.min.js"></script>
	<script type="text/javascript" src="includes/javascript/jscolor.js"></script>
    <!-- script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="assets/plugins/tether/js/tether.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- script src="assets/plugins/bootstrap4/js/bootstrap.min.js"></script -->
    <!--[if lt IE 9]>
      <script src="assets/crossbrowserjs/html5shiv.js"></script>
      <script src="assets/crossbrowserjs/respond.min.js"></script>
      <script src="assets/crossbrowserjs/excanvas.min.js"></script>
    <![endif]-->
    <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
    <!-- ================== END BASE JS ================== -->

    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="assets/plugins/morris/raphael.min.js"></script>
    <script src="assets/plugins/morris/morris.js"></script>
    <script src="assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="assets/plugins/bootstrap-sweetalert/sweetalert.js"></script>
    <script src="assets/plugins/parsley/dist/parsley.js"></script>
    <script src="assets/plugins/jstree/dist/jstree.js"></script>
    <script src="assets/plugins/switchery/switchery.js"></script>
    <script src="assets/plugins/bootstrap-filestyle/bootstrap-filestyle.js"></script>
    <!-- script src="assets/plugins/gritter/js/jquery.gritter.js"></script -->
    <script src="assets/plugins/flot/jquery.flot.min.js"></script>
    <script src="assets/plugins/flot/jquery.flot.time.min.js"></script>
    <script src="assets/plugins/flot/jquery.flot.resize.min.js"></script>
    <script src="assets/plugins/flot/jquery.flot.pie.min.js"></script>
    <script src="assets/plugins/sparkline/jquery.sparkline.js"></script>
    <script src="assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
    <script src="assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
    <!-- script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script -->
    <script src="assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
    <script src="assets/plugins/gritter/js/jquery.gritter.js"></script>
    <!--script src="assets/js/dashboard.js"></script -->
    <script src="assets/js/dashboard-v2.js"></script>
    <script src="assets/js/apps.js"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->

	<?php if(basename($_SERVER['PHP_SELF']) == 'index.php'){?>
	<!-- ================== START GRAPH JS ================== -->
	<script src="assets/plugins/d3charts/d3.min.js"></script>
	<link href="assets/plugins/d3charts/nv.d3.css" rel="stylesheet" />
	<script src="assets/plugins/d3charts/nv.d3.js"></script>
	<script src="assets/plugins/d3charts/chart-d3.demo.js"></script>

	<!-- ================== END GRAPH JS ================== -->
	<?php } ?>

	<?php if(basename($_SERVER['PHP_SELF']) == 'categories.php'){?>
	<!-- ================== START EASY UI TREE JS ================== -->
	<script src="assets/plugins/jquery-easyuitree/jquery.easyui.min.js"></script>
	<link href="assets/plugins/jquery-easyuitree/easyui.css" rel="stylesheet" />
	<link href="assets/plugins/jquery-easyuitree/icon.css" rel="stylesheet" />
	<!-- ================== START EASY UI TREE JS ================== -->
	<?php } ?>

	<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
	<script src="assets/plugins/select2/dist/js/select2.min.js"></script>
	<script language="javascript" src="includes/javascript/general.js"></script>
	<?php
	$js_page_name = 'includes/javascript/'.str_replace('.php','.js.php', basename($_SERVER['PHP_SELF']));
	if(file_exists($js_page_name)) { require_once($js_page_name); } ?>

  </head>
  <body>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
    <!-- end #page-loader -->

<?php if($menu_position == 'sidebar') { ?>
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
<?php } elseif($menu_position == 'top-menu') {?>
    <div id="page-container" class="page-container fade page-without-sidebar page-header-fixed page-with-top-menu">
<?php } ?>