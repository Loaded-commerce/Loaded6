<?php
/*
  Loaded Commerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce

  Released under the GNU General Public License
*/
// define the project version
if(!defined('INSTALLED_VERSION_TYPE'))
	define('INSTALLED_VERSION_TYPE', 'Community Edition');
define('INSTALLED_VERSION_TYPE_CODE', 'CE');
define('INSTALLED_VERSION_MAJOR', '6');
define('INSTALLED_VERSION_MINOR', '6');
define('INSTALLED_PATCH', '0');
if(!defined('INSTALLED_VERSION_REVISION'))
	define('INSTALLED_VERSION_REVISION', '');
define('INSTALLED_VERSION', INSTALLED_VERSION_MAJOR . '.' . INSTALLED_VERSION_MINOR . '.' . INSTALLED_PATCH . '.' . INSTALLED_VERSION_REVISION);
define('PROJECT_VERSION', 'Loaded Commerce CE v' . INSTALLED_VERSION);
?>
