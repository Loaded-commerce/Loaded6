<?php
/*
  $Id: column_left.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

function tep_configuration_menu() {

  $config_data = array();
  $configuration_groups_query = tep_db_query("select configuration_group_id as cgID, configuration_group_title as cgTitle from " . TABLE_CONFIGURATION_GROUP . " where visible = '1' order by sort_order");
  while ($configuration_groups = tep_db_fetch_array($configuration_groups_query)) {
	$config_data[] = array('constant_name'=>$configuration_groups['cgTitle'], 'admin_files_id'=>$configuration_groups['cgID'], 'admin_files_name'=>FILENAME_CONFIGURATION, 'query_string'=>'gID=' . $configuration_groups['cgID'],);
  }
  return $config_data;
}
?>
<div id="<?php echo $menu_position; ?>" class="<?php echo $menu_position; ?>">
<?php if($menu_position == 'sidebar') { ?>
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <ul class="nav">
          <li class="nav-profile">
            <!--div class="image">
              <a href="javascript:;"><i class="fa fa-user"></i></a>
            </div -->
            <div class="info"> <a target="_blank" href="<?php echo tep_catalog_href_link();?>" style="text-decoration: none; color: #fff;">
              <?php echo STORE_NAME ;?>
             <small>[View Store]</small></a>
            </div>
          </li>
        </ul>
        <!-- begin sidebar nav -->
<?php } ?>
        <ul class="nav nav-list">
            <!-- sidebar minify button -->
            <li class="parent_index">
                <a href="<?php echo tep_href_link(FILENAME_DEFAULT,'','SSL');?>"><i class="fa fa-laptop"></i> <span>Dashboard</span></a>
            </li>
            <?php
              if(isset($_GET['boxPath'])) {
              	$_SESSION['sessBoxPath'] = $_GET['boxPath'];
              } else {
	              if(basename($_SERVER['PHP_SELF']) == 'index.php')
	              	$_SESSION['sessBoxPath'] = 'index_index';
              }
              
			  if ( $_SESSION['login_groups_id'] != 1 )
			  	$cache_constnt_name = 'LEFT_MENU_DATA_'.$_SESSION['login_groups_id'];
			  else
			  	$cache_constnt_name = 'LEFT_MENU_DATA_ADMIN';
			  	
			  $rs_cache_data = tep_db_query("select * from cache_data where cache_code='$cache_constnt_name'");
			  if(tep_db_num_rows($rs_cache_data) > 0) {
			  	$rw_child_data = tep_db_fetch_array($rs_cache_data);
			  	$arr_left_menu = json_decode(stripslashes($rw_child_data['cache_data']), 1);
			  	$left_menu_parent = $arr_left_menu['left_menu_parent'];
			  	$left_menu_childs = $arr_left_menu['left_menu_childs'];
			  } else {
				  $left_menu_parent = array();
				  $left_menu_childs = array();
				  if ( $_SESSION['login_groups_id'] != 1 )
					  $installed_boxes_query = tep_db_query("select * from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $_SESSION['login_groups_id'] . "', admin_groups_id) AND status=1 AND visible=1 order by sort");
				  else 
					  $installed_boxes_query = tep_db_query("select * from " . TABLE_ADMIN_FILES . " where status=1 AND visible=1 order by sort");
				  while($db_boxes = tep_db_fetch_array($installed_boxes_query)) {
					$display_permission = 1;
					if ( $_SESSION['login_groups_id'] != 1 ) {
						$arr_groups_id = array_map('trim', explode(',', $db_boxes['admin_groups_id']));
						if(!in_array($_SESSION['login_groups_id'], $arr_groups_id))
							$display_permission = 0;
					} 

					if($display_permission) {
						if($db_boxes['admin_files_is_boxes']) {
							$left_menu_parent[] = $db_boxes;
							if(!empty($db_boxes['function_name'])) {
								$arr_function_data = $db_boxes['function_name']();
								foreach($arr_function_data as $function_data) {
									$left_menu_childs[$db_boxes['admin_files_id']][] = $function_data;
								}
							}
						} else
							$left_menu_childs[$db_boxes['admin_files_to_boxes']][] = $db_boxes;
					  }
				  }
				  $arr_left_menu = array('left_menu_parent'=>$left_menu_parent, 'left_menu_childs'=>$left_menu_childs);
				  
				  tep_db_query("DELETE FROM cache_data WHERE cache_code='$cache_constnt_name'");
				  $query = "INSERT INTO cache_data SET cache_code='$cache_constnt_name', cache_data='". addslashes(json_encode($arr_left_menu)) ."'";
				  tep_db_query($query);
			  }
			  
			  $active_box = 0;
			  $active_section = 0;
			  foreach($left_menu_parent as $db_boxes) {
				echo '<li class="parent_' . $db_boxes['admin_files_id'] . ' has-sub">
						<a href="javascript:;">
						  <i class="fa ' . $db_boxes['fa_icon_name'] . '"></i>
						  <b class="caret pull-right"></b>
						  <span>' . (defined($db_boxes['constant_name'])?constant($db_boxes['constant_name']):$db_boxes['constant_name']) . '</span>
						</a>
					<ul id="parent_' . $db_boxes['admin_files_id'] . 'Menu" class="sub-menu">';
					foreach($left_menu_childs[$db_boxes['admin_files_id']] as $left_sub_menus) {
						if($left_sub_menus['admin_files_name'] == basename($_SERVER['PHP_SELF'])) {
							$active_box = $db_boxes['admin_files_id'];
							$active_section = $left_sub_menus['admin_files_id'];
						}

						$left_menu_name = (defined($left_sub_menus['constant_name'])?constant($left_sub_menus['constant_name']):$left_sub_menus['constant_name']);
						$left_menu_file_name = (defined($left_sub_menus['admin_files_name'])?constant($left_sub_menus['admin_files_name']):$left_sub_menus['admin_files_name']);
						if(empty(trim($left_sub_menus['admin_files_name'])))
							echo '<div class="menuBoxSubhead">&nbsp;&nbsp;'. $left_menu_name .'</div>';
						else
							echo '<li class="child_'. $left_sub_menus['admin_files_id'] .'"><a href="'. tep_href_link($left_menu_file_name, 'boxPath='. $db_boxes['admin_files_id'].'_'.$left_sub_menus['admin_files_id'] .'&'.$left_sub_menus['query_string']) .'">'. $left_menu_name .'</a></li>';
					}
				echo '</ul>';
			  }


            /*
                $menu_active = '';
                $box_files_list = array(
                    array('administrator','administrator.php', BOX_HEADING_ADMINISTRATOR, 'fa-user'),
                    array('configuration', 'configuration.php', BOX_HEADING_CONFIGURATION,'fa-gear'),
                    array('catalog','catalog.php', BOX_HEADING_CATALOG,'fa-tasks'),
                    array('customers', 'customers.php' , BOX_HEADING_CUSTOMERS,'fa-users'),
                    array('marketing', 'marketing.php', BOX_HEADING_MARKETING,'fa-signal'),
                    array('gv_admin', 'gv_admin.php' , BOX_HEADING_GV_ADMIN,'fa-gift'),
                    array('reports', 'reports.php' , BOX_HEADING_REPORTS,'fa-bar-chart-o'),
                    array('data', 'data.php' , BOX_HEADING_DATA,'fa-database'),
                    array('information', 'information.php', BOX_HEADING_INFORMATION,'fa-info'),
                    array('articles', 'articles.php' , BOX_HEADING_ARTICLES,'fa-book'),
                    array('design_controls' , 'design_controls.php' , BOX_HEADING_DESIGN_CONTROLS,'fa-archive'),
                    array('links', 'links.php' , BOX_HEADING_LINKS,'fa-external-link-square'),
                    array('modules', 'modules.php' , BOX_HEADING_MODULES,'fa-cubes'),
                    array('taxes', 'taxes.php' , BOX_HEADING_LOCATION_AND_TAXES,'fa-bank'),
                    array('localization', 'localization.php' , BOX_HEADING_LOCALIZATION,'fa-language'),
                    array('tools','tools.php',BOX_HEADING_TOOLS,'fa-wrench'),
                );
				// RCI include RCI menus
				$cre_RCI->get('boxes', 'menu');
				

                foreach($box_files_list as $item_menu) {

                    // NOTE: Menu "selected" logic moved to javascript includes/column_left.php

                    if (tep_admin_check_boxes($item_menu[1]) == true) {

                        echo '<li class="' . $item_menu[0] . ' has-sub">
                                <a href="javascript:;">
                                  <i class="fa ' . $item_menu[3] . '"></i>
                                  <b class="caret pull-right"></b>
                                  <span>' . $item_menu[2] . '</span>
                                </a>';
						if(file_exists(DIR_WS_BOXES . $item_menu[1]))
                        	require(DIR_WS_BOXES . $item_menu[1]);
						elseif(file_exists($item_menu[1]))
                        	require($item_menu[1]);

                    }
                }
                */
                ?>
<?php if($menu_position == 'sidebar') { ?>
            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->
<?php
}
if($menu_position == 'top-menu') {
?>
			<li class="menu-control menu-control-left">
				<a href="#" data-click="prev-menu"><i class="fa fa-angle-left"></i></a>
			</li>
			<li class="menu-control menu-control-right">
				<a href="#" data-click="next-menu"><i class="fa fa-angle-right"></i></a>
			</li>
        </ul>
        <!-- end sidebar nav -->
</div>
<?php  } ?>
<script>
$(document).ready(function() {
  // remove all active
  $('.nav-list li.active').removeClass('active');
  // add active class to current page
  var currentPage = '<?php echo str_replace(".php", "", basename($_SERVER['PHP_SELF'])); ?>';
  // menu mapping
//alert(currentPage);
<?php
if(isset($_GET['BoxPath']) && trim($_GET['BoxPath']) != '') 
	$active_box_path = trim($_GET['BoxPath']);
elseif(isset($_SESSION['sessBoxPath']) && trim($_SESSION['sessBoxPath']) != '')
	$active_box_path = trim($_SESSION['sessBoxPath']);
else
	$active_box_path = 'index_index';

$arr_box_path = explode('_', $_SESSION['sessBoxPath']);
$active_box = $arr_box_path[0];
$active_section = $arr_box_path[1];
?>
  var box = 'parent_<?php echo $active_box; ?>';
  $('.' + box).addClass('active');

  // set sub menu active
  var section = 'child_<?php echo $active_section; ?>';
  var action = '<?php echo (isset($_GET['action'])?$_GET['action']:''); ?>';

  // set default if no parameters
  if (section == '') section = currentPage;
  // set active
  if (section) $('.' + section).addClass('active');

});
</script>
