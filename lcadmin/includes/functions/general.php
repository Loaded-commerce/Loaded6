<?php
/*
  $Id: general.php,v 1.2.0.0 2020/06/29 23:38:03 ccwjr Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce
  Copyright (c) 2008 CRE Loaded

  Released under the GNU General Public License
*/

// This function is used to a JIT load of classes
function creClassLoader($className) {
  if (file_exists(DIR_FS_CLASSES . $className . '.php')) {
    include DIR_FS_CLASSES . $className . '.php';
  }
}
spl_autoload_register('creClassLoader');  // register it

// this function will record all PHP messages to prevent
// potential exposure of seecure details
function _exception_handler($severity, $message, $filepath, $line) {
  // We don't bother with "strict" notices since they will fill up
  // the log file with information that isn't normally very
  // helpful.  For example, if you are running PHP 5 and you
  // use version 4 style class functions (without prefixes
  // like "public", "private", etc.) you'll get notices telling
  // you that these have been deprecated.
  if ($severity == E_STRICT) return;

  $msg = '['.date("D M j G:i:s Y").'] [IP:'.$_SERVER['REMOTE_ADDR'].'] [URI:'.$_SERVER['REQUEST_URI'].'] Severity: '.$severity.' --> '.$message. ' '.$filepath.' '.$line;

  $fp = fopen(ERROR_MESSAGE_LOG, 'a');
  flock($fp, LOCK_EX);
  fwrite($fp, $msg . "\r\n");
  fwrite($fp, "\r\n");
  flock($fp, LOCK_UN);
  fclose($fp);
}

// this function is used to log as part of the PHP error log
// any additional messages that should not be presented on the
// web page, but are needed for debugging.
function _error_handler($message) {
  // capture the track back report
  // the actual passed message is ignored since
  // it will be included as part of the track back
  ob_start();
  debug_print_backtrace();
  $traceback = ob_get_clean();
  $msg = '['.date("D M j G:i:s Y").'] [IP:'.$_SERVER['REMOTE_ADDR'].'] [URI:'.$_SERVER['REQUEST_URI'].'] -->';

  $fp = @fopen(ERROR_MESSAGE_LOG, 'a');
  flock($fp, LOCK_EX);
  fwrite($fp, $msg . "\r\n");
  fwrite($fp, $traceback . "\r\n");
  fwrite($fp, "\r\n");
  flock($fp, LOCK_UN);
  fclose($fp);
}

// Stop from parsing any further PHP code
  function tep_exit() {
   tep_session_close();
   exit();
  }


// Redirect to another page or site
  function tep_redirect($url) {
    global $cre_RCI, $redirect_url;
    $redirect_url = $url;
    // do a call to the monitor redirect handler
    $cre_RCI->get('monitor', 'redirect', false);

    if ( (strstr($url, "\n") != false) || (strstr($url, "\r") != false) ) {
      tep_redirect(tep_href_link(FILENAME_DEFAULT, '', 'NONSSL', false));
    }

    if ( (ENABLE_SSL == 'true') && (getenv('HTTPS') == 'on') ) { // We are loading an SSL page
      if (substr($url, 0, strlen(HTTP_SERVER . DIR_WS_HTTP_CATALOG)) == HTTP_SERVER . DIR_WS_HTTP_CATALOG) { // NONSSL url
        $url = HTTPS_SERVER . DIR_WS_HTTPS_CATALOG . substr($url, strlen(HTTP_SERVER . DIR_WS_HTTP_CATALOG)); // Change it to SSL
      }
    }
    $url =  str_replace("&amp;", "&", $url);
    header('Location: ' . $url);

   tep_exit();
  }

//Admin begin
////
//Check login and file access
function tep_admin_check_login() {
  global $PHP_SELF, $navigation;
  if ( ! isset($_SESSION['login_id']) && basename($PHP_SELF) != FILENAME_LOGOFF ) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  } else {
    // if they are in group 1, let them do whatever they need to do
    if ( $_SESSION['login_groups_id'] != 1 ) {
      $filename = basename( $_SERVER['PHP_SELF'] );
      if ($filename != FILENAME_DEFAULT && $filename != FILENAME_FORBIDEN && $filename != FILENAME_LOGOFF && $filename != FILENAME_ADMIN_ACCOUNT && $filename != FILENAME_POPUP_IMAGE && $filename != 'packingslip.php' && $filename != 'invoice.php' && $filename != 'ajax_common.php' && $filename != 'addons.php' && $filename != 'print_order.php' && substr($filename, 0, 12) != 'autocomplete' && substr($filename, 0, 6) != 'modal_') {
        $db_file_query = tep_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $_SESSION['login_groups_id'] . "', admin_groups_id) and admin_files_name = '" . $filename . "' AND status=1");
        if (!tep_db_num_rows($db_file_query)) {
          tep_redirect(tep_href_link(FILENAME_FORBIDEN));
        }
      }
    }
  }
}

////
//Return 'true' or 'false' value to display boxes and files in index.php and column_left.php
function tep_admin_check_boxes($filename, $boxes='') {
  $is_boxes = 1;
  if ($boxes == 'sub_boxes') {
    $is_boxes = 0;
  }
  // by pass the check for top level admins
  if ( $_SESSION['login_groups_id'] != 1 ) {
    $dbquery = tep_db_query("select admin_files_id from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $_SESSION['login_groups_id'] . "', admin_groups_id) and admin_files_is_boxes = '" . $is_boxes . "' and admin_files_name = '" . $filename . "'");

    $return_value = false;
    if (tep_db_num_rows($dbquery)) {
      $return_value = true;
    }
  } else {
    $return_value = true;
  }
  return $return_value;
}

////
//Return files stored in box that can be accessed by user
function tep_admin_files_boxes ($filename = '', $sub_box_name = '', $connection = 'NONSSL', $parameters = '', $space = '') {
  global $request_type;
  $sub_boxes = '';

  if ($filename != '' ){
  // by pass the check for top level admins

    // Recommended
	parse_str($parameters, $output);
    //$li_id = str_replace("section=", "", substr($parameters, strpos($parameters, "section=")));
    $li_id = $output['section'];

    if ( $_SESSION['login_groups_id'] != 1 ) {
      $dbquery = tep_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $_SESSION['login_groups_id'] . "', admin_groups_id) and admin_files_is_boxes = '0' and admin_files_name = '" . $filename . "'");
      if (tep_db_num_rows($dbquery)) {
        $sub_boxes = '<li class="' . $li_id . '"><a href="' . tep_href_link($filename, $parameters, $connection) . '">' . $sub_box_name . '</a></li>' . "\n";
      } else if ( (tep_db_num_rows($dbquery)) || ($connection == '') ) {
        $sub_boxes = '<li class="' . $li_id . '"><a href="' . tep_href_link($filename, $parameters, $connection) . '">' . $sub_box_name . '</a></li>' . "\n";
      }
    } else {
      $sub_boxes = '<li class="' . $li_id . '"><a href="' . tep_href_link($filename, $parameters, $connection) . '">' . $sub_box_name . '</a></li>' . "\n";
    }
  } else  if ($filename == ''){
    $sub_boxes = '<div class="menuBoxSubhead"><nobr>' . $sub_box_name . '</nobr></div>';
  } else {
    $sub_boxes = '<div class="menuBoxSubhead"><nobr>' . $sub_box_name . '</nobr></div>';
  }
  return $sub_boxes;
}

////
//Get selected file for index.php
function tep_selected_file($filename) {
  $randomize = FILENAME_ADMIN_ACCOUNT;

  $dbquery = tep_db_query("select admin_files_id as boxes_id from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $_SESSION['login_groups_id'] . "', admin_groups_id) and admin_files_is_boxes = '1' and admin_files_name = '" . $filename . "'");
  if (tep_db_num_rows($dbquery)) {
    $boxes_id = tep_db_fetch_array($dbquery);
    $randomize_query = tep_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $_SESSION['login_groups_id'] . "', admin_groups_id) and admin_files_is_boxes = '0' and admin_files_to_boxes = '" . $boxes_id['boxes_id'] . "'");
    if (tep_db_num_rows($randomize_query)) {
      $file_selected = tep_db_fetch_array($randomize_query);
      $randomize = $file_selected['admin_files_name'];
    }
  }
  return $randomize;
}
//Admin end


////
// Parse the data used in the html tags to ensure the tags will not break
  function tep_parse_input_field_data($data, $parse) {
    return strtr(trim($data), $parse);
  }

  function tep_output_string($string, $translate = false, $protected = false) {
    $string = str_replace('\\', '', $string);
    if ($protected == true) {
      $new_string = str_replace('&amp;', '&', $string);  // so corruption will not occur
      return htmlspecialchars($new_string);
    } else {
      if ($translate == false) {
        return tep_parse_input_field_data($string, array('"' => '&quot;'));
      } else {
        return tep_parse_input_field_data($string, $translate);
      }
    }
  }

  function tep_output_string_protected($string) {
    return tep_output_string($string, false, true);
  }

  function tep_sanitize_string($string) {
    $string = preg_replace('/ +/', ' ', $string);

    return preg_replace("/[<>]/", '_', $string);
  }
 //taken from wordpress
 function seems_utf8($str) {
     $length = strlen($str);
     for ($i=0; $i < $length; $i++) {
         $c = ord($str[$i]);
         if ($c < 0x80) $n = 0; # 0bbbbbbb
         elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
         elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
         elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
         elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
         elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
         else return false; # Does not match any model
         for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
             if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                 return false;
         }
     }
     return true;
 }
function _truncate_post_slug( $slug, $length = 200 ) {
	if ( strlen( $slug ) > $length ) {
			$decoded_slug = urldecode( $slug );
			if ( $decoded_slug === $slug )
			  $slug = substr( $slug, 0, $length );
			else
			  $slug = utf8_uri_encode( $decoded_slug, $length );
	}
	return rtrim($slug, '-' );
}
  function tep_customers_name($customers_id) {
    $customers = tep_db_query("select customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customers_id . "'");
    $customers_values = tep_db_fetch_array($customers);

    return $customers_values['customers_firstname'] . ' ' . $customers_values['customers_lastname'];
  }

  function tep_get_path($current_category_id = '') {
    global $cPath_array;

    if ($current_category_id == '') {
      $cPath_new = implode('_', $cPath_array);
    } else {
      if (sizeof($cPath_array) == 0) {
        $cPath_new = $current_category_id;
      } else {
        $cPath_new = '';
        $last_category_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$cPath_array[(sizeof($cPath_array)-1)] . "'");
        $last_category = tep_db_fetch_array($last_category_query);

        $current_category_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$current_category_id . "'");
        $current_category = tep_db_fetch_array($current_category_query);

        if ($last_category['parent_id'] == $current_category['parent_id']) {
          for ($i = 0, $n = sizeof($cPath_array) - 1; $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        } else {
          for ($i = 0, $n = sizeof($cPath_array); $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        }

        $cPath_new .= '_' . $current_category_id;

        if (substr($cPath_new, 0, 1) == '_') {
          $cPath_new = substr($cPath_new, 1);
        }
      }
    }

    return 'cPath=' . $cPath_new;
  }

  function tep_get_all_get_params($exclude_array = '') {
    if ($exclude_array == '') $exclude_array = array();

    $get_url = '';
    $exclude_array[] = 'routes';

    reset($_GET);
    foreach($_GET as $key=>$value) {
      if (($key != tep_session_name()) && ($key != 'error') && (!in_array($key, $exclude_array))) $get_url .= $key . '=' . $value . '&';
    }

    return $get_url;
  }

  function tep_date_long($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || ($raw_date == '') ) return false;

    $year = (int)substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    return strftime(DATE_FORMAT_LONG, mktime($hour, $minute, $second, $month, $day, $year));
  }

////
// Output a raw date string in the selected locale date format
// $raw_date needs to be in this format: YYYY-MM-DD HH:MM:SS
// NOTE: Includes a workaround for dates before 01/01/1970 that fail on windows servers
  function tep_date_short($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || ($raw_date == '') ) return false;

    $year = substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
      return date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
      return preg_replace('/2037' . '$/', $year, date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, 2037)));
    }

  }

  function tep_datetime_short($raw_datetime) {
    if ( ($raw_datetime == '0000-00-00 00:00:00') || ($raw_datetime == '') ) return false;

    $year = (int)substr($raw_datetime, 0, 4);
    $month = (int)substr($raw_datetime, 5, 2);
    $day = (int)substr($raw_datetime, 8, 2);
    $hour = (int)substr($raw_datetime, 11, 2);
    $minute = (int)substr($raw_datetime, 14, 2);
    $second = (int)substr($raw_datetime, 17, 2);

    return strftime(DATE_TIME_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
  }

  function tep_get_category_tree($parent_id = '0', $spacing = '', $exclude = '', $category_tree_array = '', $include_itself = false) {
    global $languages_id;

    if (!is_array($category_tree_array)) $category_tree_array = array();
    if ( (sizeof($category_tree_array) < 1) && ($exclude != '0') ) $category_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    if ($include_itself) {
      $category_query = tep_db_query("select cd.categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " cd where cd.language_id = '" . (int)$languages_id . "' and cd.categories_id = '" . (int)$parent_id . "'");
      $category = tep_db_fetch_array($category_query);
      $category_tree_array[] = array('id' => $parent_id, 'text' => $category['categories_name']);
    }

    $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "' and c.parent_id = '" . (int)$parent_id . "' order by c.sort_order, cd.categories_name");
    while ($categories = tep_db_fetch_array($categories_query)) {
      if ($exclude != $categories['categories_id']) $category_tree_array[] = array('id' => $categories['categories_id'], 'text' => $spacing . $categories['categories_name']);
      $category_tree_array = tep_get_category_tree($categories['categories_id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $category_tree_array);
    }

    return $category_tree_array;
  }

  function tep_draw_products_pull_down($name, $parameters = '', $exclude = '') {
    global $currencies, $languages_id;

    if ($exclude == '') {
      $exclude = array();
    }

    $select_string = '<select name="' . $name . '"';

    $select_string .= ' class="form-control" ';

    if ($parameters) {
      $select_string .= ' ' . $parameters;
    }

    $select_string .= '>';

    $products_query = tep_db_query("SELECT p.products_id, pd.products_name, p.products_price
                                    FROM " . TABLE_PRODUCTS . " p,
                                         " . TABLE_PRODUCTS_DESCRIPTION . " pd
                                    WHERE p.products_id = pd.products_id
                                      and pd.language_id = " . (int)$languages_id . "
                                      and (p.products_status = 1
                                          or (p.products_status <> 1 and p.products_parent_id <> 0))
                                      and p.products_model <> 'GIFT'
                                    ORDER BY products_name");
    while ($products = tep_db_fetch_array($products_query)) {
      if (!in_array($products['products_id'], $exclude)) {
        $select_string .= '<option value="' . $products['products_id'] . '">' . $products['products_name'] . ' (' . $currencies->format($products['products_price']) . ')</option>';
      }
    }

    $select_string .= '</select>';

    return $select_string;
  }

  function tep_options_name($options_id) {
    global $languages_id;

    $options = tep_db_query("select products_options_name from " . TABLE_PRODUCTS_OPTIONS_TEXT . " where products_options_text_id = '" . (int)$options_id . "' and language_id = '" . (int)$languages_id . "'");
    $options_values = tep_db_fetch_array($options);

    return $options_values['products_options_name'];
  }

  function tep_values_name($values_id) {
    global $languages_id;

    $values = tep_db_query("select products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " where products_options_values_id = '" . (int)$values_id . "' and language_id = '" . (int)$languages_id . "'");
    $values_values = tep_db_fetch_array($values);

    return $values_values['products_options_values_name'];
  }

  function tep_info_image($image, $alt, $width = '', $height = '') {
      global $request_type;
      $fs_image = DIR_FS_CATALOG.$image;
    if (tep_not_null($image) && (file_exists($fs_image)) ) {
      $image = tep_image((($request_type == 'SSL') ? HTTPS_CATALOG_SERVER : HTTP_SERVER )  . DIR_WS_HTTP_CATALOG . $image, $alt, $width, $height);
    } else {
      $image = VISUAL_IMAGE_NONEXISTENT;
    }

    return $image;
  }

  function tep_catalog_path() {
	global $request_type;
	$path = (($request_type == 'SSL') ? HTTPS_CATALOG_SERVER : HTTP_SERVER )  . DIR_WS_HTTP_CATALOG;
	return $path;
  }

  function tep_break_string($string, $len, $break_char = '-') {
    $l = 0;
    $output = '';
    for ($i=0, $n=strlen($string); $i<$n; $i++) {
      $char = substr($string, $i, 1);
      if ($char != ' ') {
        $l++;
      } else {
        $l = 0;
      }
      if ($l > $len) {
        $l = 1;
        $output .= $break_char;
      }
      $output .= $char;
    }

    return $output;
  }

  function tep_get_country_name($country_id) {
    $country_query = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$country_id . "'");

    if (!tep_db_num_rows($country_query)) {
      return $country_id;
    } else {
      $country = tep_db_fetch_array($country_query);
      return $country['countries_name'];
    }
  }

  function tep_get_zone_name($country_id, $zone_id, $default_zone) {
    $zone_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' and zone_id = '" . (int)$zone_id . "'");
    if (tep_db_num_rows($zone_query)) {
      $zone = tep_db_fetch_array($zone_query);
      return $zone['zone_name'];
    } else {
      return $default_zone;
    }
  }

function tep_array_to_string($array, $exclude = '', $equals = '=', $separator = '&') {
    if (!is_array($exclude)) $exclude = array();

    $get_string = '';
    if (sizeof($array) > 0) {
      foreach($array as $key=>$value) {
        if ( (!in_array($key, $exclude)) && ($key != 'x') && ($key != 'y') ) {
          $get_string .= $key . $equals . $value . $separator;
        }
      }
      $remove_chars = strlen($separator);
      $get_string = substr($get_string, 0, -$remove_chars);
    }

    return $get_string;
  }

  function tep_not_null($value) {
    if (is_array($value)) {
      if (sizeof($value) > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      if ( (is_string($value) || is_int($value)) && ($value != '') && ($value != 'NULL') && (strlen(trim($value)) > 0)) {
        return true;
      } else {
        return false;
      }
    }
  }

  function tep_browser_detect($component) {
    global $HTTP_USER_AGENT;

    return stristr($HTTP_USER_AGENT, $component);
  }

  function tep_tax_classes_pull_down($parameters, $selected = '') {
    $select_string = '<select ' . $parameters . '>';
    $classes_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
    while ($classes = tep_db_fetch_array($classes_query)) {
      $select_string .= '<option value="' . $classes['tax_class_id'] . '"';
      if ($selected == $classes['tax_class_id']) $select_string .= ' selected="selected"';
      $select_string .= '>' . $classes['tax_class_title'] . '</option>';
    }
    $select_string .= '</select>';

    return $select_string;
  }

  function tep_geo_zones_pull_down($parameters, $selected = '') {
    $select_string = '<select ' . $parameters . '>';
    $zones_query = tep_db_query("select geo_zone_id, geo_zone_name from " . TABLE_GEO_ZONES . " order by geo_zone_name");
    while ($zones = tep_db_fetch_array($zones_query)) {
      $select_string .= '<option value="' . $zones['geo_zone_id'] . '"';
      if ($selected == $zones['geo_zone_id']) $select_string .= ' selected="selected"';
      $select_string .= '>' . $zones['geo_zone_name'] . '</option>';
    }
    $select_string .= '</select>';

    return $select_string;
  }

  function tep_get_geo_zone_name($geo_zone_id) {
    $zones_query = tep_db_query("select geo_zone_name from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$geo_zone_id . "'");

    if (!tep_db_num_rows($zones_query)) {
      $geo_zone_name = $geo_zone_id;
    } else {
      $zones = tep_db_fetch_array($zones_query);
      $geo_zone_name = $zones['geo_zone_name'];
    }

    return $geo_zone_name;
  }

  function tep_address_format($address_format_id, $address, $html, $boln, $eoln) {
    $address_format_query = tep_db_query("select address_format as format from " . TABLE_ADDRESS_FORMAT . " where address_format_id = '" . (int)$address_format_id . "'");
    $address_format = tep_db_fetch_array($address_format_query);

    $company = (isset($address['company']) ? tep_output_string_protected($address['company']) : '');
    if (isset($address['firstname']) && tep_not_null($address['firstname'])) {
      $firstname = tep_output_string_protected($address['firstname']);
      $lastname = tep_output_string_protected($address['lastname']);
    } elseif (isset($address['name']) && tep_not_null($address['name'])) {
      $firstname = tep_output_string_protected($address['name']);
      $lastname = '';
    } else {
      $firstname = '';
      $lastname = '';
    }
    $street = tep_output_string_protected($address['street_address']);
    $suburb = tep_output_string_protected($address['suburb']);
    $city = tep_output_string_protected($address['city']);
    $state = tep_output_string_protected($address['state']);
    if (isset($address['country_id']) && tep_not_null($address['country_id'])) {
      $country = tep_get_country_name($address['country_id']);

      if (isset($address['zone_id']) && tep_not_null($address['zone_id'])) {
        $state = tep_get_zone_code($address['country_id'], $address['zone_id'], $state);
      }
    } elseif (isset($address['country']) && tep_not_null($address['country'])) {
      $country = tep_output_string_protected($address['country']);
    } else {
      $country = '';
    }
    $postcode = tep_output_string_protected($address['postcode']);
    $zip = $postcode;
    $telephone = isset($address['telephone']) ? tep_output_string_protected($address['telephone']) : '';

    if ($html) {
// HTML Mode
      $HR = '<hr>';
      $hr = '<hr>';
      if ( ($boln == '') && ($eoln == "\n") ) { // Values not specified, use rational defaults
        $CR = '<br>';
        $cr = '<br>';
        $eoln = $cr;
      } else { // Use values supplied
        $CR = $eoln . $boln;
        $cr = $CR;
      }
    } else {
// Text Mode
      $CR = $eoln;
      $cr = $CR;
      $HR = '----------------------------------------';
      $hr = '----------------------------------------';
    }

    $statecomma = '';
    $streets = $street;
    if ($suburb != '') $streets = $street . $cr . $suburb;
    if ($country == '') $country = tep_output_string_protected($address['country']);
    if ($state != '') $statecomma = $state . ', ';

    $fmt = $address_format['format'];
    eval("\$address = \"$fmt\";");

    if ( (ACCOUNT_COMPANY == 'true') && (tep_not_null($company)) ) {
      $address = $company . $cr . $address;
    }

    if (tep_not_null($telephone)) {
      $address .= $cr . $telephone;
    }

    return $address;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // Function    : tep_get_zone_code
  //
  // Arguments   : country           country code string
  //               zone              state/province zone_id
  //               def_state         default string if zone==0
  //
  // Return      : state_prov_code   state/province code
  //
  // Description : Function to retrieve the state/province code (as in FL for Florida etc)
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////
  function tep_get_zone_code($country, $zone, $def_state) {

    $state_prov_query = tep_db_query("select zone_code from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and zone_id = '" . (int)$zone . "'");

    if (!tep_db_num_rows($state_prov_query)) {
      $state_prov_code = $def_state;
    }
    else {
      $state_prov_values = tep_db_fetch_array($state_prov_query);
      $state_prov_code = $state_prov_values['zone_code'];
    }

    return $state_prov_code;
  }

  function tep_get_uprid($prid, $params) {
    $uprid = $prid;
    if ( (is_array($params)) && (!strstr($prid, '{')) ) {
      foreach($params as $option=>$value) {
        $uprid = $uprid . '{' . $option . '}' . $value;
      }
    }

    return $uprid;
  }

  function tep_get_prid($uprid) {
    $pieces = explode('{', $uprid);

    return $pieces[0];
  }

  function tep_get_languages() {
    $languages_query = tep_db_query("select languages_id, name, code, image, directory from " . TABLE_LANGUAGES . " order by sort_order");
    while ($languages = tep_db_fetch_array($languages_query)) {
      $languages_array[] = array('id' => $languages['languages_id'],
                                 'name' => $languages['name'],
                                 'code' => $languages['code'],
                                 'image' => $languages['image'],
                                 'directory' => $languages['directory']);
    }

    return $languages_array;
  }

  function tep_get_orders_status_name($orders_status_id, $language_id = '') {
    global $languages_id;

    if (!$language_id) $language_id = $languages_id;
    $orders_status_query = tep_db_query("select orders_status_name from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . (int)$orders_status_id . "' and language_id = '" . (int)$language_id . "'");
    $orders_status = tep_db_fetch_array($orders_status_query);

    return $orders_status['orders_status_name'];
  }

  function tep_get_orders_status() {
    global $languages_id;

    $orders_status_array = array();
    $orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' order by orders_status_id");
    while ($orders_status = tep_db_fetch_array($orders_status_query)) {
      $orders_status_array[] = array('id' => $orders_status['orders_status_id'],
                                     'text' => $orders_status['orders_status_name']);
    }

    return $orders_status_array;
  }

  function tep_get_products_name($product_id, $language_id = 0) {
    global $languages_id;

    if ($language_id == 0) $language_id = $languages_id;
    $product_query = tep_db_query("select products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);
    $product['products_name'] = tep_db_decoder($product['products_name']);
    $product['products_name'] = tep_db_output($product['products_name']);

    return $product['products_name'];
  }

  function tep_get_infobox_file_name($infobox_id, $language_id = 0) {
    global $languages_id;

    if ($language_id == 0) $language_id = $languages_id;
    $infobox_query = tep_db_query("select infobox_file_name from " . TABLE_INFOBOX_CONFIGURATION . " where infobox_id = '" . (int)$infobox_id . "' and language_id = '" . (int)$language_id . "'");
    $infobox = tep_db_fetch_array($infobox_query);

    return $infobox['infobox_file_name'];
  }

  function tep_get_products_description($product_id, $language_id) {
    $product_query = tep_db_query("select products_description from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_description'];
  }

  function tep_get_products_url($product_id, $language_id) {
    $product_query = tep_db_query("select products_url from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_url'];
  }

  function tep_get_category_name($category_id, $language_id) {
    $category_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_name'];
  }

  function tep_get_category_htc_title($category_id, $language_id) {
    $category_query = tep_db_query("select categories_head_title_tag from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_head_title_tag'];
  }

  function tep_get_category_htc_desc($category_id, $language_id) {
    $category_query = tep_db_query("select categories_head_desc_tag from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_head_desc_tag'];
  }

  function tep_get_category_htc_keywords($category_id, $language_id) {
    $category_query = tep_db_query("select categories_head_keywords_tag  from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_head_keywords_tag '];
  }

  function tep_get_products_head_title_tag($product_id, $language_id) {
    $product_query = tep_db_query("select products_head_title_tag from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_head_title_tag'];
  }

  function tep_get_products_head_desc_tag($product_id, $language_id) {
    $product_query = tep_db_query("select products_head_desc_tag from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_head_desc_tag'];
  }

  function tep_get_products_head_keywords_tag($product_id, $language_id) {
    $product_query = tep_db_query("select products_head_keywords_tag from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_head_keywords_tag'];
  }

  function tep_get_manufacturer_htc_title($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_htc_title_tag from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_htc_title_tag'];
  }

  function tep_get_manufacturer_htc_desc($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_htc_desc_tag from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_htc_desc_tag'];
  }

  function tep_get_manufacturer_htc_keywords($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_htc_keywords_tag from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_htc_keywords_tag'];
  }

////
// Return the manufacturers URL in the needed language
// TABLES: manufacturers_info
  function tep_get_manufacturer_url($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_url from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_url'];
  }

////
// Wrapper for class_exists() function
// This function is not available in all PHP versions so we test it before using it.
  function tep_class_exists($class_name) {
    if (function_exists('class_exists')) {
      return class_exists($class_name, false);
    } else {
      return true;
    }
  }

////
// Count how many products exist in a category
// TABLES: products, products_to_categories, categories
  function tep_products_in_category_count($categories_id, $include_deactivated = false) {
    $products_count = 0;

    if ($include_deactivated) {
      $products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = p2c.products_id and p2c.categories_id = '" . (int)$categories_id . "'");
    } else {
      $products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = p2c.products_id and p.products_status = '1' and p2c.categories_id = '" . (int)$categories_id . "'");
    }

    $products = tep_db_fetch_array($products_query);

    $products_count += $products['total'];

    $childs_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$categories_id . "'");
    if (tep_db_num_rows($childs_query)) {
      while ($childs = tep_db_fetch_array($childs_query)) {
        $products_count += tep_products_in_category_count($childs['categories_id'], $include_deactivated);
      }
    }

    return $products_count;
  }

////
// Count how many subcategories exist in a category
// TABLES: categories
  function tep_childs_in_category_count($categories_id) {
    $categories_count = 0;

    $categories_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$categories_id . "'");
    while ($categories = tep_db_fetch_array($categories_query)) {
      $categories_count++;
      $categories_count += tep_childs_in_category_count($categories['categories_id']);
    }

    return $categories_count;
  }

////
// Returns an array with countries
// TABLES: countries
  function tep_get_countries($default = '') {
    $countries_array = array();
    if ($default) {
      $countries_array[] = array('id' => '',
                                 'text' => $default);
    }
    $countries_query = tep_db_query("select countries_id, countries_name from " . TABLE_COUNTRIES . " where country_status = 1 order by countries_name");
    while ($countries = tep_db_fetch_array($countries_query)) {
      $countries_array[] = array('id' => $countries['countries_id'],
                                 'text' => $countries['countries_name']);
    }

    return $countries_array;
  }

////
// return an array with country zones
  function tep_get_country_zones($country_id) {
    $zones_array = array();
    $zones_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' and zone_status = 1 order by zone_name");
    while ($zones = tep_db_fetch_array($zones_query)) {
      $zones_array[] = array('id' => $zones['zone_id'],
                             'text' => $zones['zone_name']);
    }

    return $zones_array;
  }

  function tep_prepare_country_zones_pull_down($country_id = '') {
// preset the width of the drop-down for Netscape
    $pre = '';
    if ( (!tep_browser_detect('MSIE')) && (tep_browser_detect('Mozilla/4')) ) {
      for ($i=0; $i<45; $i++) $pre .= '&nbsp;';
    }

    $zones = tep_get_country_zones($country_id);

    if (sizeof($zones) > 0) {
      $zones_select = array(array('id' => '', 'text' => PLEASE_SELECT));
      $zones = array_merge($zones_select, $zones);
    } else {
      $zones = array(array('id' => '', 'text' => TYPE_BELOW));
// create dummy options for Netscape to preset the height of the drop-down
      if ( (!tep_browser_detect('MSIE')) && (tep_browser_detect('Mozilla/4')) ) {
        for ($i=0; $i<9; $i++) {
          $zones[] = array('id' => '', 'text' => $pre);
        }
      }
    }

    return $zones;
  }

////
// Get list of address_format_id's
  function tep_get_address_formats() {
    $address_format_query = tep_db_query("select address_format_id from " . TABLE_ADDRESS_FORMAT . " order by address_format_id");
    $address_format_array = array();
    while ($address_format_values = tep_db_fetch_array($address_format_query)) {
      $address_format_array[] = array('id' => $address_format_values['address_format_id'],
                                      'text' => $address_format_values['address_format_id']);
    }
    return $address_format_array;
  }

////
// Alias function for Store configuration values in the Administration Tool
  function tep_cfg_pull_down_country_list($country_id) {
    return tep_draw_pull_down_menu('configuration_value', tep_get_countries(), $country_id);
  }

  function tep_cfg_pull_down_zone_list($zone_id) {
    return tep_draw_pull_down_menu('configuration_value', tep_get_country_zones(STORE_COUNTRY), $zone_id);
  }

  function tep_cfg_pull_down_timezone_list($timezone_id) {
    $timezones_array = array();
    $timezone_identifiers = DateTimeZone::listIdentifiers();
    foreach ($timezone_identifiers as $tz) {
      $timezones_array[$tz] = 0;
    }
    ksort($timezones_array);
    unset($timezone_identifiers);

    $timezone_abbreviations = DateTimeZone::listAbbreviations();
    foreach ($timezone_abbreviations as $abr => $tzs) {
      foreach ($tzs as $tz) {
        if ($tz['timezone_id'] == '') continue;
        if (isset($timezones_array[$tz['timezone_id']])) {
          $timezones_array[$tz['timezone_id']] = $tz['offset'];
        }
      }
    }
    unset($timezone_abbreviations);

    $timezones = array();
    foreach($timezones_array as $tz => $offset) {
      if ($offset == 0) {
        $hours = 0;
        $mins = 0;
      } else {
        $mins = 0;
        $hours = floor(abs($offset) / 3600);
        $remaining_seconds = abs($offset) - ($hours * 3600);
        if ($offset < 0) $hours = $hours * -1;
        if ($remaining_seconds > 0) $mins = floor($remaining_seconds / 60);
      }
      $timezones[] = array('id' => $tz,
                           'text' => $tz . ' GMT ' . sprintf('%+02d:%02d', $hours, $mins));
    }

    return tep_draw_pull_down_menu('configuration_value', $timezones, $timezone_id);
  }

  function tep_cfg_pull_down_tax_classes($tax_class_id, $key = '') {
    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
    $tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
    while ($tax_class = tep_db_fetch_array($tax_class_query)) {
      $tax_class_array[] = array('id' => $tax_class['tax_class_id'],
                                 'text' => $tax_class['tax_class_title']);
    }

    return tep_draw_pull_down_menu($name, $tax_class_array, $tax_class_id);
  }
//++++ QT Pro: Begin Changed code
////
// Function to build menu of available class files given a file prefix
// Used for configuring plug-ins for product information attributes
  function tep_cfg_pull_down_class_files($prefix, $current_file) {
    $d=DIR_FS_CATALOG . DIR_WS_CLASSES;
    $function_directory = dir ($d);

    while (false !== ($function = $function_directory->read())) {
      if (preg_match('/^'.$prefix.'(.+)\.php$/',$function,$function_name)) {
          $file_list[]=array('id'=>$function_name[1], 'text'=>$function_name[1]);
      }
    }
    $function_directory->close();

    return tep_draw_pull_down_menu('configuration_value', $file_list, $current_file);
  }

//++++ QT Pro: End Changed Code
////
// Function to read in text area in admin
 function tep_cfg_textarea($text) {
    return tep_draw_textarea_field('configuration_value', false, 35, 5, $text, 'class="form-control"');
  }

  function tep_cfg_get_zone_name($zone_id) {
    $zone_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_id = '" . (int)$zone_id . "'");

    if (!tep_db_num_rows($zone_query)) {
      return $zone_id;
    } else {
      $zone = tep_db_fetch_array($zone_query);
      return $zone['zone_name'];
    }
  }

// Function use: to create a color selector for html colors
  function tep_cfg_select_color_html($feild) {
$output  = '<tr> ';
$output .=    '<td bgcolor="#ffffff" valign="top">';
$output .=      '<input type="Text" name="configuration_value" value="' . $feild . '"> ' ;;
$output .=      '<a href="javascript:TCP.popup(document.forms[\'configuration\'].elements[\'configuration_value\'], 0)">' . tep_image_button('button_insert.gif', TEXT_HEADING_CHANGE_COLOR);
$output .=      '<br><br>'. TEXT_HEADING_INPUT_COLOR . '&nbsp;<span style= "{ border-width: medium ; border-style:solid ; border-color: #000000 ;  padding: .5em ; background-color: rgb(' . $feild . '); border-color: #000000 ; border-width: thin; padding: .5em ;} ">&nbsp;&nbsp;&nbsp;</span>';
$output .=     '</td>';
$output .=  '</tr>';

    return $output ;
  }

// Function use: to create a color selector for RGB colors
  function tep_cfg_select_color_rgb($feild) {
$output  = '<tr> ';
$output .=    '<td bgcolor="#ffffff" valign="top">';
$output .=      '<input type="Text" name="configuration_value" value="' . $feild . '"> ' ;
$output .=      '<a href="javascript:TCP_DEC.popup(document.forms[\'configuration\'].elements[\'configuration_value\'], 0)">' . tep_image_button('button_insert.gif', TEXT_HEADING_CHANGE_COLOR);
$output .=      '<br><br>'. TEXT_HEADING_INPUT_COLOR . '&nbsp;<span style= "{ border-width: medium ; border-style:solid ; border-color: #000000 ;  padding: .5em ; background-color: rgb(' . $feild . '); border-color: #000000 ; border-width: thin; padding: .5em ;} ">&nbsp;&nbsp;&nbsp;</span>';
$output .=     '</td>';
$output .=  '</tr>';

    return $output ;
  }

////
// Function to build menu of available font files
// Used for configuring plug-ins for product information attributes
  function tep_cfg_pull_down_font_files() {
     $d= DIR_FS_CATALOG . 'includes/fonts/' ;
     $name_select = VVC_FONTS;
  if ($handle1 = opendir($d)) {
      while (false !== ($file1 = readdir($handle1))) {
         if( (is_file($d . $file1)) && ($file1 != '..' || $file1 != '.' ) ){
         $file1 = str_replace(".ttf", "", $file1);
         $font_list1[] = array('id'=>$file1, 'text'=>$file1);
         }
     }
    closedir($handle1);
    }
return tep_draw_pull_down_menu('configuration_value', $font_list1, $name_select);
  }

////
// Sets the status of a banner
  function tep_set_banner_status($banners_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_BANNERS . " set status = '1', date_status_change = now() where banners_id = '" . $banners_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_BANNERS . " set status = '0', date_status_change = now() where banners_id = '" . $banners_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the status of a product
  function tep_set_product_status($products_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_PRODUCTS . " set products_status = '1', products_last_modified = now() where products_id = '" . (int)$products_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_PRODUCTS . " set products_status = '0', products_last_modified = now() where products_id = '" . (int)$products_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the status of a product on special
  function tep_set_specials_status($specials_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_SPECIALS . " set status = '1', expires_date = NULL, date_status_change = NULL where specials_id = '" . (int)$specials_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_SPECIALS . " set status = '0', date_status_change = now() where specials_id = '" . (int)$specials_id . "'");
    } else {
      return -1;
    }
  }

  function tep_set_faq_status($faq_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_FAQ . " set visible = '1' where faq_id = '" . $faq_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_FAQ . " set visible = '0' where faq_id = '" . $faq_id . "'");
    } else {
      return -1;
    }
  }






////
// Sets timeout for the current script.
// Cant be used in safe mode.
  function tep_set_time_limit($limit) {
    if (!get_cfg_var('safe_mode')) {
      set_time_limit($limit);
    }
  }

////
// Alias function for Store configuration values in the Administration Tool
  function tep_cfg_select_option($select_array, $key_value, $key = '') {
    $string = '';

    for ($i=0, $n=sizeof($select_array); $i<$n; $i++) {
      $name = ((tep_not_null($key)) ? 'configuration[' . $key . ']' : 'configuration_value');

      $string .= '<br><input type="radio" name="' . $name . '" value="' . $select_array[$i] . '"';

      if ($key_value == $select_array[$i]) $string .= ' CHECKED';

      $string .= '> ' . $select_array[$i];
    }

    return $string;
  }

////
// Alias function for module configuration keys
  function tep_mod_select_option($select_array, $key_name, $key_value) {
    reset($select_array);
    foreach($select_array as $key=>$value) {
      if (is_int($key)) $key = $value;
      $string .= '<br><input type="radio" name="configuration[' . $key_name . ']" value="' . $key . '"';
      if ($key_value == $key) $string .= ' CHECKED';
      $string .= '> ' . $value;
    }

    return $string;
  }

////
// Retreive server information
  function tep_get_system_information() {
    $db_query = tep_db_query("select now() as datetime");
    $db = tep_db_fetch_array($db_query);

    list($system, $host, $kernel) = preg_split('/[\s,]+/', @exec('uname -a'), 5);

    return array('date' => tep_datetime_short(date('Y-m-d H:i:s')),
                 'system' => $system,
                 'kernel' => $kernel,
                 'host' => $host,
                 'ip' => gethostbyname($host),
                 'uptime' => @exec('uptime'),
                 'http_server' => $_SERVER['SERVER_SOFTWARE'],
                 'php' => PHP_VERSION,
                 'zend' => (function_exists('zend_version') ? zend_version() : ''),
                 'db_server' => DB_SERVER,
                 'db_ip' => gethostbyname(DB_SERVER),
                 'db_version' => 'MySQL ' . (function_exists('mysql_get_server_info') ? mysql_get_server_info() : ''),
                 'db_date' => tep_datetime_short($db['datetime']));
  }

  function tep_generate_category_path($id, $from = 'category', $categories_array = '', $index = 0) {
    global $languages_id;

    if (!is_array($categories_array)) $categories_array = array();

    if ($from == 'product') {
      $categories_query = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$id . "'");
      while ($categories = tep_db_fetch_array($categories_query)) {
        if ($categories['categories_id'] == '0') {
          $categories_array[$index][] = array('id' => '0', 'text' => TEXT_TOP);
        } else {
          $category_query = tep_db_query("select cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$categories['categories_id'] . "' and c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "'");
          $category = tep_db_fetch_array($category_query);
          $categories_array[$index][] = array('id' => $categories['categories_id'], 'text' => $category['categories_name']);
          if ( (tep_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = tep_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
          $categories_array[$index] = array_reverse($categories_array[$index]);
        }
        $index++;
      }
    } elseif ($from == 'category') {
      $category_query = tep_db_query("select cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$id . "' and c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "'");
      $category = tep_db_fetch_array($category_query);
      $categories_array[$index][] = array('id' => $id, 'text' => $category['categories_name']);
      if ( (tep_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = tep_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
    }

    return $categories_array;
  }

  function tep_output_generated_category_path($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = tep_generate_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -16) . '<br>';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -4);
    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }

  function tep_get_generated_category_path_ids($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = tep_generate_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['id'] . '_';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -1) . '<br>';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -4);
    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;
    // reverse the order
    $new_calculated_category_path_string = '';
    $path_array = explode('&nbsp;&gt;&nbsp;', $calculated_category_path_string);
    rsort($path_array);
    foreach($path_array as $value) {
      $new_calculated_category_path_string .= $value . '&nbsp;&gt;&nbsp;';
    }
    $new_calculated_category_path_string = substr($new_calculated_category_path_string, 0, -16);

    return $new_calculated_category_path_string;
  }

  function tep_remove_category($category_id) {
    $category_image_query = tep_db_query("select categories_image from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    $category_image = tep_db_fetch_array($category_image_query);

    $duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_CATEGORIES . " where categories_image = '" . tep_db_input($category_image['categories_image']) . "'");
    $duplicate_image = tep_db_fetch_array($duplicate_image_query);

    if ($duplicate_image['total'] < 2) {
      if (file_exists(DIR_FS_CATALOG_IMAGES . $category_image['categories_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $category_image['categories_image']);
      }
    }
	tep_db_query("delete from " . TABLE_PERMALINK . " where categories_id = '" . (int)$category_id . "'");
    tep_db_query("delete from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    tep_db_query("delete from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");

    if (USE_CACHE == 'true') {
      tep_reset_cache_block('categories');
      tep_reset_cache_block('also_purchased');
    }
  }

  function tep_remove_product($product_id) {
    $product_image_query = tep_db_query("select products_image from " . TABLE_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
    $product_image = tep_db_fetch_array($product_image_query);

    $duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " where products_image = '" . tep_db_input($product_image['products_image']) . "'");
    $duplicate_image = tep_db_fetch_array($duplicate_image_query);

    if ($duplicate_image['total'] < 2) {
      if (file_exists(DIR_FS_CATALOG_IMAGES . $product_image['products_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $product_image['products_image']);
      }
    }

    tep_db_query("delete from " . TABLE_PERMALINK . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where products_id = '" . (int)$product_id . "'");
    //tep_db_query("delete from " . TABLE_PRODUCTS_TO_PRODUCTS_EXTRA_FIELDS . " where products_id = " . (int)$product_id);
    $product_reviews_query = tep_db_query("select reviews_id from " . TABLE_REVIEWS . " where products_id = '" . (int)$product_id . "'");
    while ($product_reviews = tep_db_fetch_array($product_reviews_query)) {
      tep_db_query("delete from " . TABLE_REVIEWS_DESCRIPTION . " where reviews_id = '" . (int)$product_reviews['reviews_id'] . "'");
    }
    tep_db_query("delete from " . TABLE_REVIEWS . " where products_id = '" . (int)$product_id . "'");
    //addition 12.15.08
    tep_db_query("delete from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_FEATURED . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_ARTICLES_XSELL . " where xsell_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_XSELL . " where products_id = '" . (int)$product_id . "'"); //if product has xsell
    tep_db_query("delete from " . TABLE_PRODUCTS_XSELL . " where xsell_id = '" . (int)$product_id . "'");   //if is xsell for other products


    if (USE_CACHE == 'true') {
      tep_reset_cache_block('categories');
      tep_reset_cache_block('also_purchased');
    }
  }

  function tep_remove_order($order_id, $restock) {
    if ($restock == 'on') {
      $order_query = tep_db_query("select products_id, products_quantity from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$order_id . "'");
      while ($order = tep_db_fetch_array($order_query)) {
        tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity + " . $order['products_quantity'] . ", products_ordered = products_ordered - " . $order['products_quantity'] . " where products_id = '" . (int)$order['products_id'] . "'");
      }
    }

    //begin PayPal_Shopping_Cart_IPN 2.8 DMG
    // the file may no longer exist, so check for it
    if (file_exists(DIR_FS_CATALOG_MODULES . 'payment/paypal/functions/general.func.php')) {
      include_once(DIR_FS_CATALOG_MODULES . 'payment/paypal/functions/general.func.php');
      paypal_remove_order($order_id);
    }
    //end PayPal_Shopping_Cart_IPN
    tep_db_query("delete from " . TABLE_ORDERS . " where orders_id = '" . (int)$order_id . "'");
    tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$order_id . "'");
    tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_id = '" . (int)$order_id . "'");
    tep_db_query("delete from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_id = '" . (int)$order_id . "'");
    tep_db_query("delete from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$order_id . "'");
  }

  function tep_reset_cache_block($cache_block) {
    global $cache_blocks;
$dir_cache = DIR_FS_CATALOG . DIR_FS_CACHE ;
   // get default template
   if (tep_not_null($cptemplate1['template_selected'])) {
  define('TEMPLATE_NAME', $cptemplate['template_selected']);
    }else if  (tep_not_null(DEFAULT_TEMPLATE)){
  define('TEMPLATE_NAME', DEFAULT_TEMPLATE);
    } else {
    define('TEMPLATE_NAME', 'default');
    }
     $template_query = tep_db_query("select template_name from " . TABLE_TEMPLATE . "  order by template_name");
      while ($template = tep_db_fetch_array($template_query)) {
        $template_array=array(template=>template_name);
        }


    for ($i=0, $n=sizeof($cache_blocks); $i<$n; $i++) {
      if ($cache_blocks[$i]['code'] == $cache_block) {
        if ($cache_blocks[$i]['multiple']) {
          if ($dir = @opendir($dir_cache)) {
            while ($cache_file = readdir($dir)) {
              $cached_file = $cache_blocks[$i]['file'];
              $languages = tep_get_languages();
              for ($j=0, $k=sizeof($languages); $j<$k; $j++) {
                $cached_file_unlink = preg_replace('/-language/', '-' . $languages[$j]['directory'], $cached_file);

                              for ($j=0, $k=sizeof($template_array); $j<$k; $j++) {
                    $cached_file_unlink = preg_replace('/-TEMPLATE_NAME/', '-' . $template_array[$j]['template_name'] , $cached_file);


                if (preg_match('/^' . $cached_file_unlink . '/', $cache_file)) {
                  @unlink($dir_cache . $cache_file);
                  }
                }
              }
            }
            closedir($dir);
          }
        } else {
          $cached_file = $cache_blocks[$i]['file'];
          $languages = tep_get_languages();
          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $cached_file = preg_replace('/-language/', '-' . $languages[$i]['directory'], $cached_file);
            @unlink(DIR_FS_CACHE . $cached_file);
          }
        }
        break;
      }
    }
  }

  function tep_get_file_permissions($mode) {
// determine type
    if ( ($mode & 0xC000) == 0xC000) { // unix domain socket
      $type = 's';
    } elseif ( ($mode & 0x4000) == 0x4000) { // directory
      $type = 'd';
    } elseif ( ($mode & 0xA000) == 0xA000) { // symbolic link
      $type = 'l';
    } elseif ( ($mode & 0x8000) == 0x8000) { // regular file
      $type = '-';
    } elseif ( ($mode & 0x6000) == 0x6000) { //bBlock special file
      $type = 'b';
    } elseif ( ($mode & 0x2000) == 0x2000) { // character special file
      $type = 'c';
    } elseif ( ($mode & 0x1000) == 0x1000) { // named pipe
      $type = 'p';
    } else { // unknown
      $type = '?';
    }

// determine permissions
    $owner['read']    = ($mode & 00400) ? 'r' : '-';
    $owner['write']   = ($mode & 00200) ? 'w' : '-';
    $owner['execute'] = ($mode & 00100) ? 'x' : '-';
    $group['read']    = ($mode & 00040) ? 'r' : '-';
    $group['write']   = ($mode & 00020) ? 'w' : '-';
    $group['execute'] = ($mode & 00010) ? 'x' : '-';
    $world['read']    = ($mode & 00004) ? 'r' : '-';
    $world['write']   = ($mode & 00002) ? 'w' : '-';
    $world['execute'] = ($mode & 00001) ? 'x' : '-';

// adjust for SUID, SGID and sticky bit
    if ($mode & 0x800 ) $owner['execute'] = ($owner['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x400 ) $group['execute'] = ($group['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x200 ) $world['execute'] = ($world['execute'] == 'x') ? 't' : 'T';

    return $type .
           $owner['read'] . $owner['write'] . $owner['execute'] .
           $group['read'] . $group['write'] . $group['execute'] .
           $world['read'] . $world['write'] . $world['execute'];
  }

  function tep_remove($source) {
    global $messageStack, $tep_remove_error;

    if (isset($tep_remove_error)) $tep_remove_error = false;

    if (is_dir($source)) {
      $dir = dir($source);
      while ($file = $dir->read()) {
        if ( ($file != '.') && ($file != '..') ) {
          if (is_writeable($source . '/' . $file)) {
            tep_remove($source . '/' . $file);
          } else {
            $messageStack->add('search', sprintf(ERROR_FILE_NOT_REMOVEABLE, $source . '/' . $file), 'error');
            $tep_remove_error = true;
          }
        }
      }
      $dir->close();

      if (is_writeable($source)) {
        rmdir($source);
      } else {
        $messageStack->add('search', sprintf(ERROR_DIRECTORY_NOT_REMOVEABLE, $source), 'error');
        $tep_remove_error = true;
      }
    } else {
      if (is_writeable($source)) {
        unlink($source);
      } else {
        $messageStack->add('search', sprintf(ERROR_FILE_NOT_REMOVEABLE, $source), 'error');
        $tep_remove_error = true;
      }
    }
  }

////
// Output the tax percentage with optional padded decimals
  function tep_display_tax_value($value, $padding = TAX_DECIMAL_PLACES) {
    if (strpos($value, '.')) {
      $loop = true;
      while ($loop) {
        if (substr($value, -1) == '0') {
          $value = substr($value, 0, -1);
        } else {
          $loop = false;
          if (substr($value, -1) == '.') {
            $value = substr($value, 0, -1);
          }
        }
      }
    }

    if ($padding > 0) {
      if ($decimal_pos = strpos($value, '.')) {
        $decimals = strlen(substr($value, ($decimal_pos+1)));
        for ($i=$decimals; $i<$padding; $i++) {
          $value .= '0';
        }
      } else {
        $value .= '.';
        for ($i=0; $i<$padding; $i++) {
          $value .= '0';
        }
      }
    }

    return $value;
  }

  function tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name=STORE_OWNER, $from_email_address=STORE_OWNER_EMAIL_ADDRESS, $extraInfo=array()) {
    if (SEND_EMAILS != 'true') return false;

	//PHPMailer Object
	$mail = new PHPMailer;

	$mail->SMTPDebug = 0;                                                                        // Enable verbose debug output
	$mail->Encoding = "base64";
	$mail->CharSet = "UTF-8";
	if (EMAIL_TRANSPORT == 'smtp') {
		$mail->IsSMTP(); // send via SMTP
		$mail->Host = EMAIL_SMTP_HOST_SERVER; // SMTP servers
		if(trim(EMAIL_SMTP_PORT_SERVER) != '') {
			$mail->Port = EMAIL_SMTP_PORT_SERVER; // SMTP server Port
			if(EMAIL_SMTP_PORT_SERVER == 587)
				$mail->SMTPSecure = 'tls';
			elseif(EMAIL_SMTP_PORT_SERVER == 465)
				$mail->SMTPSecure = 'ssl';
		}
		if(trim(EMAIL_SMTP_HELO_SERVER) != '')
			$mail->Helo = EMAIL_SMTP_HELO_SERVER; // Helo Name
		$mail->SMTPAuth = true; // turn on SMTP authentication
		$mail->Username = EMAIL_SMTP_USERNAME; // SMTP username
		$mail->Password = EMAIL_SMTP_PASSWORD; // SMTP password
	}

	//From email address and name
	$mail->From = $from_email_address;
	$mail->FromName = $from_email_name;

	//To address and name
	$mail->addAddress($to_email_address, $to_name);

	//Address to which recipient will reply
	$mail->addReplyTo($from_email_address, $from_email_name);

	//CC and BCC
	if(isset($extraInfo['cc']) && trim($extraInfo['cc']) != '')
		$mail->addCC(trim($extraInfo['cc']));
	if(isset($extraInfo['bcc']) && trim($extraInfo['bcc']) != '')
		$mail->addBCC(trim($extraInfo['bcc']));


	$mail->Subject = $email_subject;
	if(is_array($email_text)) {
		//Send HTML or Plain Text email
		$mail->isHTML(EMAIL_USE_HTML);

		$mail->Body = $email_text['html'];
		$mail->AltBody = $email_text['text'];
	} else {
		if(isHtml($email_text))
			$mail->isHTML(true);
		else
			$mail->isHTML(false);
		$mail->Body = $email_text;
	}

	if(!$mail->send()) {
		//echo EMAIL_SMTP_HOST_SERVER;
		//print_r($mail->ErrorInfo);
		return "Mailer Error: " . $mail->ErrorInfo;
	} else {
		return "Message has been sent successfully";
	}
  }

  function isHtml($data) {
	$matches = array();
	preg_match('/<\s?[^\>]*\/?\s?>/i', $data, $matches);
	return count($matches);
  }

  function tep_get_tax_class_title($tax_class_id) {
    if ($tax_class_id == '0') {
      return TEXT_NONE;
    } else {
      $classes_query = tep_db_query("select tax_class_title from " . TABLE_TAX_CLASS . " where tax_class_id = '" . (int)$tax_class_id . "'");
      $classes = tep_db_fetch_array($classes_query);

      return $classes['tax_class_title'];
    }
  }

  function tep_banner_image_extension() {
    if (function_exists('imagetypes')) {
      if (imagetypes() & IMG_PNG) {
        return 'png';
      } elseif (imagetypes() & IMG_JPG) {
        return 'jpg';
      } elseif (imagetypes() & IMG_GIF) {
        return 'gif';
      }
    } elseif (function_exists('imagecreatefrompng') && function_exists('imagepng')) {
      return 'png';
    } elseif (function_exists('imagecreatefromjpeg') && function_exists('imagejpeg')) {
      return 'jpg';
    } elseif (function_exists('imagecreatefromgif') && function_exists('imagegif')) {
      return 'gif';
    }

    return false;
  }

////
// Wrapper function for round() for php3 compatibility
  function tep_round($value, $precision) {
    return round($value, $precision);
  }

////
// Add tax to a products price
  function tep_add_tax($price, $tax) {
    global $currencies;

    if (DISPLAY_PRICE_WITH_TAX == 'true') {
      return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']) + tep_calculate_tax($price, $tax);
    } else {
      return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
    }
  }

// Calculates Tax rounding the result
  function tep_calculate_tax($price, $tax) {
    global $currencies;

    return tep_round($price * $tax / 100, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
  }

////
// Returns the tax rate for a zone / class
// TABLES: tax_rates, zones_to_geo_zones
  function tep_get_tax_rate($class_id, $country_id = -1, $zone_id = -1) {

    if ( ($country_id == -1) && ($zone_id == -1) ) {
      if ( ! isset($_SESSION['customer_id']) ) {
        $country_id = STORE_COUNTRY;
        $zone_id = STORE_ZONE;
      } else {
        $country_id = $_SESSION['customer_country_id'];
        $zone_id = $_SESSION['customer_zone_id'];
      }
    }

    $tax_query = tep_db_query("select sum(tax_rate) as tax_rate from " . TABLE_TAX_RATES . " tr left join " . TABLE_ZONES_TO_GEO_ZONES . " za ON tr.tax_zone_id = za.geo_zone_id left join " . TABLE_GEO_ZONES . " tz ON tz.geo_zone_id = tr.tax_zone_id WHERE (za.zone_country_id IS NULL OR za.zone_country_id = '0' OR za.zone_country_id = '" . (int)$country_id . "') AND (za.zone_id IS NULL OR za.zone_id = '0' OR za.zone_id = '" . (int)$zone_id . "') AND tr.tax_class_id = '" . (int)$class_id . "' GROUP BY tr.tax_priority");
    if (tep_db_num_rows($tax_query)) {
      $tax_multiplier = 0;
      while ($tax = tep_db_fetch_array($tax_query)) {
        $tax_multiplier += $tax['tax_rate'];
      }
      return $tax_multiplier;
    } else {
      return 0;
    }
  }

////
// Returns the tax rate for a tax class
// TABLES: tax_rates
  function tep_get_tax_rate_value($class_id) {
    $tax_query = tep_db_query("select sum(tax_rate) as tax_rate from " . TABLE_TAX_RATES . " where tax_class_id = '" . (int)$class_id . "' group by tax_priority");
    if (tep_db_num_rows($tax_query)) {
      $tax_multiplier = 0;
      while ($tax = tep_db_fetch_array($tax_query)) {
        $tax_multiplier += $tax['tax_rate'];
      }
      return $tax_multiplier;
    } else {
      return 0;
    }
  }

  function tep_call_function($function, $parameter, $object = '') {
    if (!is_object($object)) {
      return call_user_func($function, $parameter);
    } else {
      return call_user_func(array($object, $function), $parameter);
    }
  }

  function tep_get_zone_class_title($zone_class_id) {
    if ($zone_class_id == '0') {
      return TEXT_NONE;
    } else {
      $classes_query = tep_db_query("select geo_zone_name from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$zone_class_id . "'");
      $classes = tep_db_fetch_array($classes_query);

      return $classes['geo_zone_name'];
    }
  }

  function tep_cfg_pull_down_zone_classes($zone_class_id, $key = '') {
    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $zone_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
    $zone_class_query = tep_db_query("select geo_zone_id, geo_zone_name from " . TABLE_GEO_ZONES . " order by geo_zone_name");
    while ($zone_class = tep_db_fetch_array($zone_class_query)) {
      $zone_class_array[] = array('id' => $zone_class['geo_zone_id'],
                                  'text' => $zone_class['geo_zone_name']);
    }

    return tep_draw_pull_down_menu($name, $zone_class_array, $zone_class_id);
  }

  function tep_cfg_pull_down_order_statuses($order_status_id, $key = '') {
    global $languages_id;

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $statuses_array = array(array('id' => '0', 'text' => TEXT_DEFAULT));
    $statuses_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' order by orders_status_name");
    while ($statuses = tep_db_fetch_array($statuses_query)) {
      $statuses_array[] = array('id' => $statuses['orders_status_id'],
                                'text' => $statuses['orders_status_name']);
    }

    return tep_draw_pull_down_menu($name, $statuses_array, $order_status_id);
  }

  function tep_get_order_status_name($order_status_id, $language_id = '') {
    global $languages_id;

    if ($order_status_id < 1) return TEXT_DEFAULT;

    if (!is_numeric($language_id)) $language_id = $languages_id;

    $status_query = tep_db_query("select orders_status_name from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . (int)$order_status_id . "' and language_id = '" . (int)$language_id . "'");
    $status = tep_db_fetch_array($status_query);

    return $status['orders_status_name'];
  }

////
// Return a random value
  function tep_rand($min = null, $max = null) {
    static $seeded;

    if (!$seeded) {
      mt_srand((double)microtime()*1000000);
      $seeded = true;
    }

    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }

// nl2br() prior PHP 4.2.0 did not convert linefeeds on all OSs (it only converted \n)
  function tep_convert_linefeeds($from, $to, $string) {
    return str_replace($from, $to, $string);
  }

  function tep_string_to_int($string) {
    return (int)$string;
  }

////
// Parse and secure the cPath parameter values
  function tep_parse_category_path($cPath) {
// make sure the category IDs are integers
    $cPath_array = array_map('tep_string_to_int', explode('_', $cPath));

// make sure no duplicate category IDs exist which could lock the server in a loop
    $tmp_array = array();
    $n = sizeof($cPath_array);
    for ($i=0; $i<$n; $i++) {
      if (!in_array($cPath_array[$i], $tmp_array)) {
        $tmp_array[] = $cPath_array[$i];
      }
    }

    return $tmp_array;
  }
// Alias function for array of configuration values in the Administration Tool
  function tep_cfg_select_multioption($select_array, $key_value, $key = '') {
    $string = '';
    for ($i=0; $i<sizeof($select_array); $i++) {
      $name = (($key) ? 'configuration[' . $key . '][]' : 'configuration_value');
      $string .= '<br><input type="checkbox" name="' . $name . '" value="' . $select_array[$i] . '"';
      $key_values = explode( ", ", $key_value);
      if ( in_array($select_array[$i], $key_values) ) $string .= 'CHECKED';
      $string .= '> ' . $select_array[$i];
    }
    return $string;
  }

//create a select list to display list of themes available for selection
  function tep_cfg_pull_down_template_list($template_id, $key = '') {
    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $template_query = tep_db_query("select template_id, template_name from " . TABLE_TEMPLATE . " order by template_name");
    while ($template = tep_db_fetch_array($template_query)) {
      $template_array[] = array('id' => $template['template_name'],
                                 'text' => $template['template_name']);
    }

    return tep_draw_pull_down_menu($name, $template_array, $template_id);
  }


// BOF: WebMakers.com Added: Downloads Controller
require(DIR_WS_FUNCTIONS . 'downloads_controller.php');
// EOF: WebMakers.com Added: Downloads Controller

// Contact US Email Subjects : DMG
// PassionSeed Contact Us Email Subject begin
  function tep_get_email_subjects_list($subjects_array = '') {
    if (!is_array($subjects_array)) $subjects_array = array();

    $subjects_query = tep_db_query("select email_subjects_id, email_subjects_name from " . TABLE_EMAIL_SUBJECTS . " order by email_subjects_name");
    while ($subjects = tep_db_fetch_array($subjects_query)) {
      $subjects_array[] = array('id' => $subjects['email_subjects_name'], 'text' => $subjects['email_subjects_name']);
    }

    return $subjects_array;
  }
// PassionSeed Contact Us Email Subject end
function tep_get_pay_method($pay_methods_id, $language_id = '') {
    global $languages_id;

    if (!$language_id) $language_id = $languages_id;
    $pay_method_query1 = tep_db_query("select pay_method from " . TABLE_ORDERS_PAY_METHODS . " where pay_methods_id = '" . (int)$pay_methods_id . "' and pay_method_language = '" . (int)$language_id . "'");
    $pay_method1 = tep_db_fetch_array($pay_method_query1);

    return $pay_method1['pay_method'];
  }
function tep_get_ship_method($ship_methods_id, $language_id = '') {
    global $languages_id;

    if (!$language_id) $language_id = $languages_id;
    $ship_method_query1 = tep_db_query("select ship_method from " . TABLE_ORDERS_SHIP_METHODS . " where ship_methods_id = '" . (int)$ship_methods_id . "' and ship_method_language = '" . (int)$language_id . "'");
    $ship_method1 = tep_db_fetch_array($ship_method_query1);

    return $ship_method1['ship_method'];
  }

  function tep_get_ip_address() {
    if (isset($_SERVER)) {
      if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
      } else {
        $ip = $_SERVER['REMOTE_ADDR'];
      }
    } else {
      if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
      } elseif (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
      } else {
        $ip = getenv('REMOTE_ADDR');
      }
    }

    return $ip;
  }


function tep_get_box_heading($infobox_id, $languages_id) {
    $configuration_query12 = tep_db_query("select box_heading from " . TABLE_INFOBOX_HEADING . " where infobox_id = '" . (int)$infobox_id . "' and languages_id = '" . (int)$languages_id . "'");
    $configuration12 = tep_db_fetch_array($configuration_query12);

    return $configuration12['box_heading'];
  }

  function tep_html_noquote($string) {
  $string=str_replace('&#39;', '', $string);
  $string=str_replace("'", "", $string);
  $string=str_replace('"', '', $string);
  $string=preg_replace("/\\r\\n|\\n|\\r/", "<br>", $string);
  return $string;

}

  //function to get order status for configurations.
  function tep_get_orders_status_selection($key_value, $key = '') {
    $string = '';
    $key_values = explode(',', $key_value);
    $orders_status_array = array();
    $orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$_SESSION['languages_id'] . "' order by orders_status_id");
    while ($orders_status = tep_db_fetch_array($orders_status_query)) {
      $orders_status_array[] = array ('id' => $orders_status['orders_status_id'],
                                      'name' => $orders_status['orders_status_name']);
    }
    for ($i=0; $i<(sizeof($orders_status_array)); $i++) {
      $name = (($key) ? 'configuration[' . $key . '][]' : 'configuration_value[]');
      $string .= '<br><input type="checkbox" name="' . $name . '" value="' . $orders_status_array[$i]['id'] . '"';
      if (in_array($orders_status_array[$i]['id'], $key_values)) $string .= ' checked="checked"';
      $string .= '> ' . $orders_status_array[$i]['name'];
    }
    return $string;
  }


  //function to show title and tooltip of index boxes.
  function cre_index_block_title($title, $url='', $tip='') {
    $content = '';
    $content .= $title;
    if(isset($url) && $url !=''){
      $content .= ' (<a href="' . $url . '">' . TEXT_MANAGE . '</a>) ';
    }
    if(isset($tip) && $tip !='' && defined('ADMIN_BLOCKS_SHOW_TIP') && ADMIN_BLOCKS_SHOW_TIP == 'true'){
      $content .= ' <a class="helpLink" href="?" onMouseover="showhint(\'' . $tip . '\', this, event, \'250px\'); return false">[?]</a>';
    }
    echo $content;
  }

  //generate password
  function randomize() {
    $salt = "ABCDEFGHIJKLMNOPQRSTUVWXWZabchefghjkmnpqrstuvwxyz0123456789";
    srand((double)microtime()*1000000);
    $i = 0;
    $pass = '';
    while ($i <= ENTRY_PASSWORD_MIN_LENGTH) {
      $num = rand() % strlen($salt);
      $tmp = substr($salt, $num, 1);
      $pass .= $tmp;
      $i++;
    }
    return $pass;
  }

  // set page value
  function setPage($separator = true) {
    $amp = ($separator === true) ? '&' : '';
    return ((isset($_GET['page'])) ? $amp . 'page=' . preg_replace('/[^0-9]/', '', $_GET['page']) : '');
  }

  // Sets the status of a category
  function tep_set_category_status($categories_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_CATEGORIES . " set categories_status = '1', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_CATEGORIES . " set categories_status = '0', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
    } else {
      return -1;
    }
  }
  // Sets the status of a topnav
  function tep_set_topnav_status($categories_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_CATEGORIES . " set show_in_top_nav = '1', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_CATEGORIES . " set show_in_top_nav = '0', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
    } else {
      return -1;
    }
  }
  // Sets the status of category for sidenav
  function tep_set_sidenav_status($categories_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_CATEGORIES . " set show_in_side_nav = '1', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_CATEGORIES . " set show_in_side_nav = '0', last_modified = now() where categories_id = '" . (int)$categories_id . "'");
    } else {
      return -1;
    }
  }
  //set country status
  function tep_set_country_status($countries_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set country_status = '1' where countries_id = '" . (int)$countries_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set country_status = '0' where countries_id = '" . (int)$countries_id . "'");
    } else {
      return -1;
    }
  }
    //set zone status
    function tep_set_zone_status($zone_id, $status) {
      if ($status == '1') {
        return tep_db_query("update " . TABLE_ZONES . " set zone_status = '1' where zone_id = '" . (int)$zone_id . "'");
      } elseif ($status == '0') {
        return tep_db_query("update " . TABLE_ZONES . " set zone_status = '0' where zone_id = '" . (int)$zone_id . "'");
      } else {
        return -1;
      }
  }
  //set Review approve status
  function tep_set_review_approve_status($reviews_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_REVIEWS . " set isapproved = '1' where reviews_id = '" . (int)$reviews_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_REVIEWS . " set isapproved = '0' where reviews_id = '" . (int)$reviews_id . "'");
    } else {
      return -1;
    }
  }
  //set country allowed in website status
  function tep_set_country_allowed($countries_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set country_allowed = '1' where countries_id = '" . (int)$countries_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set country_allowed = '0' where countries_id = '" . (int)$countries_id . "'");
    } else {
      return -1;
    }
  }
  //for graph reports functions
	function get_ordertotal_bydate($data, $params = '', $filter = ''){
		$time=strtotime($data);
		$month=date("F",$time);
		$year=date("Y",$time);
		$current_month = date('m');
		$current_year = date('Y');
		$append_query = '';

		if($filter!=''){
			$append_query .= $filter;
		}

		if($params == 'monthly'){
			echo "SELECT SUM(OT.value) AS alltotal, O.`date_purchased` FROM orders O INNER JOIN orders_total OT ON O.orders_id = OT.`orders_id` WHERE OT.`class` = 'ot_total' AND MONTH(O.`date_purchased`)  = '".$data."' AND YEAR(O.`date_purchased`) = '".$current_year."' $append_query ";
			$date_query = tep_db_query("SELECT SUM(OT.value) AS alltotal, O.`date_purchased` FROM orders O INNER JOIN orders_total OT ON O.orders_id = OT.`orders_id` WHERE OT.`class` = 'ot_total' AND MONTH(O.`date_purchased`)  = '".$data."' AND YEAR(O.`date_purchased`) = '".$current_year."' $append_query ");
		}elseif($params == 'yearly'){
			$date_query = tep_db_query("SELECT SUM(OT.value) AS alltotal, O.`date_purchased` FROM orders O INNER JOIN orders_total OT ON O.orders_id = OT.`orders_id` WHERE OT.`class` = 'ot_total' AND YEAR(O.`date_purchased`) = '".$data."' $append_query ");
		}else{
			$date_query = tep_db_query("SELECT SUM(OT.value) AS alltotal, O.`date_purchased` FROM orders O INNER JOIN orders_total OT ON O.orders_id = OT.`orders_id` WHERE OT.`class` = 'ot_total'  AND DATE(O.`date_purchased`) = '".$data."' $append_query ");
		}
		$row_query = tep_db_fetch_array($date_query);
		if($row_query['alltotal'] > 0){
			return $row_query['alltotal'];
		}else{
			return '0';
		}
	}

  function newslettersubscribed($type='', $param=''){
  		if($param == 'total'){
  			$select_sub = tep_db_query("Select count(*) as TOTALSUB from customers ");
  		}else{
  			$select_sub = tep_db_query("Select count(*) as TOTALSUB from customers where customers_newsletter = '".$type."' ");
  		}
  		$row = tep_db_fetch_array($select_sub);
  		return $row['TOTALSUB'];
  }
  function get_order_count(){
	 $count_order = 0;
	 $query = "select count(*) as number_of_order from " . TABLE_ORDERS . " ";
	 $query_rw = tep_db_query($query);
	 $count_rw = tep_db_fetch_array($query_rw);
	 $count_order = $count_rw['number_of_order'];
	 return $count_order;
  }
  function get_order_register(){
  	 $count_order = 0;
  	 $query = "select count(*) as number_of_register from customers";
  	 $query_rw = tep_db_query($query);
  	 $count_rw = tep_db_fetch_array($query_rw);
  	 $count_order = $count_rw['number_of_register'];
  	 return $count_order;
  }
  function get_newsletter_subscriber(){
	 $count_order = 0;
	 $query = "select count(*) as number_of_newsltr from newsletters";
	 $query_rw = tep_db_query($query);
	 $count_rw = tep_db_fetch_array($query_rw);
	 $count_order = $count_rw['number_of_newsltr'];
	 return $count_order;
  }
  function get_last_month_newsletter(){
    $current_year = date('Y');
    $current_month = date('M');
    $cur_month = date("m",strtotime($current_month));
    $previus_month = date("F", strtotime("last month"));
    $pre_month = date("m",strtotime($previus_month));

	 $query = "SELECT COUNT(date_added) AS last_neswletter FROM newsletters WHERE YEAR(date_added)='".$current_year."' AND MONTH(date_added)='".$pre_month."'";
	 $query_rw = tep_db_query($query);
	 $last_month = tep_db_fetch_array($query_rw);
	 $last_month_newsltr = $last_month['last_neswletter'];
	 return $last_month_newsltr;
  }
  function get_total_newsletter(){
     $current_year = date('Y');
     $current_month = date('M');
     $cur_month = date("m",strtotime($current_month));
     $previus_month = date("F", strtotime("last month"));
     $pre_month = date("m",strtotime($previus_month));

	 //previus month
	 $query = "SELECT COUNT(date_added) AS last_neswletter FROM newsletters WHERE YEAR(date_added)='".$current_year."' AND MONTH(date_added)='".$pre_month."'";
	 $query_rw = tep_db_query($query);
	 $last_month = tep_db_fetch_array($query_rw);
	 //current month
	 $curquery = "SELECT COUNT(date_added) AS current_neswletter FROM newsletters WHERE YEAR(date_added)='".$current_year."' AND MONTH(date_added)='".$cur_month."'";
	 $curquery_rw = tep_db_query($curquery);
	 $cuurent_month = tep_db_fetch_array($curquery_rw);

	 $sum = (int)$cuurent_month['current_neswletter'] + (int)$last_month['last_neswletter'];
	 $last_month_neswletter = ($last_month['last_neswletter'] > 0)?$last_month['last_neswletter']:1;
	 $percentage = ($cuurent_month['current_neswletter'] - $last_month['last_neswletter']) / $last_month_neswletter;
	 //echo $percentage;
	 $toatal_order = $percentage * 100;
     return $toatal_order;
  }
 function get_last_month_order(){
	$current_year = date('Y');
	$current_month = date('M');
    $cur_month = date("m",strtotime($current_month));
    $previus_month = date("F", strtotime("last month"));
    $pre_month = date("m",strtotime($previus_month));

	 $query = "SELECT SUM(products_quantity) AS total FROM `orders_products` op INNER JOIN orders o ON o.orders_id = op.orders_id WHERE YEAR(date_purchased)='".$current_year."' AND MONTH(date_purchased)='".$pre_month."'";
  	 $query_row = tep_db_query($query);
  	 $cart_row = tep_db_fetch_array($query_row);
  	 $cart_order = $cart_row['total'];
  	 return $cart_order;
  }
 function get_current_month_order(){
	$current_year = date('Y');
	$current_month = date('M');
    $cur_month = date("m",strtotime($current_month));
    $previus_month = date("F", strtotime("last month"));
    $pre_month = date("m",strtotime($previus_month));

	 $query = "SELECT SUM(products_quantity) AS total FROM `orders_products` op INNER JOIN orders o ON o.orders_id = op.orders_id WHERE YEAR(date_purchased)='".$current_year."' AND MONTH(date_purchased)='".$cur_month."'";
	 $query_row = tep_db_query($query);
	 $cart_row = tep_db_fetch_array($query_row);
	 $cart_order = $cart_row['total'];
  	 return $cart_order;
 }
 function getlast_month_customer(){
   $current_year = date('Y');
   $current_month = date('M');
   $query = "SELECT SUM(customers_info_number_of_logons) AS total FROM customers_info WHERE YEAR(customers_info_date_account_created)='".$current_year."' AND MONTH(customers_info_date_account_created)='".$pre_month."' ";
   $cust_rw = tep_db_query($query);
   $cust_com_row = tep_db_fetch_array($cust_rw);
   $cust_total = $cust_com_row['total'];
   return  $cust_total;
 }
 function get_currentmonth_customer(){
  $current_year = date('Y');
  $current_month = date('M');
  $cur_month = date("m",strtotime($current_month));

  $query = "SELECT SUM(customers_info_number_of_logons) AS total FROM customers_info WHERE YEAR(customers_info_date_account_created)='".$current_year."' AND MONTH(customers_info_date_account_created)='".$cur_month."' ";
  $cust_rw = tep_db_query($query);
  $cust_com_row = tep_db_fetch_array($cust_rw);
  $cust_total = $cust_com_row['total'];
  return  $cust_total;
 }
 function get_totalpercent_customer(){
	  $current_year = date('Y');
	  $current_month = date('M');
	  $cur_month = date("m",strtotime($current_month));
	  $previus_month = date("F", strtotime("last month"));
	  $pre_month = date("m",strtotime($previus_month));

	 //last month
	  $query = "SELECT SUM(customers_info_number_of_logons) AS last_total FROM customers_info WHERE YEAR(customers_info_date_account_created)='".$current_year."' AND MONTH(customers_info_date_account_created)='".$pre_month."' ";
	  $custl_rw = tep_db_query($query);
	  $cust_last_row = tep_db_fetch_array($custl_rw);
	 //current month
	  $query = "SELECT SUM(customers_info_number_of_logons) AS current_total FROM customers_info WHERE YEAR(customers_info_date_account_created)='".$current_year."' AND MONTH(customers_info_date_account_created)='".$cur_month."' ";
	  $custc_rw = tep_db_query($query);
	  $cust_curr_row = tep_db_fetch_array($custc_rw);

	  $sum = (int)$cust_curr_row['current_total'] + (int)$cust_last_row['last_total'];
	  $lastmonth_total = ($cust_last_row['last_total'] > 0)?$cust_last_row['last_total']:1;
	  $percentage = ($cust_curr_row['current_total'] - $cust_last_row['last_total']) / $lastmonth_total;
	 //echo $percentage;
	  $toatal_order = $percentage * 100;
	  return number_format((float)$toatal_order, 2, '.', '');
 }
function get_order_percent(){
	$current_year = date('Y');
	$current_month = date('M');
	$cur_month = date("m",strtotime($current_month));
	$previus_month = date("F", strtotime("last month"));
	$pre_month = date("m",strtotime($previus_month));
	//current months completed order
	$current_totalorder_query = "SELECT SUM(products_quantity) AS currentmonth_total FROM `orders_products` op INNER JOIN orders o ON o.orders_id = op.orders_id WHERE YEAR(date_purchased)='".$current_year."' AND MONTH(date_purchased)='".$cur_month."'";
	$totalorder_query_row = tep_db_query($current_totalorder_query);
	$current_ordertotal = tep_db_fetch_array($totalorder_query_row);
	//current months incompleted order
	$current_incomplete_cart = "SELECT SUM(customers_basket_quantity) AS currentmonth_incomplete FROM customers_basket WHERE YEAR(customers_basket_date_added)='".$current_year."' AND MONTH(customers_basket_date_added)='".$cur_month."' ";
	$incomplete_row = tep_db_query($current_incomplete_cart);
	$current_incomplete = tep_db_fetch_array($incomplete_row);
	//last months completed order
	$last_totalorder_query = "SELECT SUM(products_quantity) AS lastmonth_total FROM `orders_products` op INNER JOIN orders o ON o.orders_id = op.orders_id WHERE YEAR(date_purchased)='".$current_year."' AND MONTH(date_purchased)='".$pre_month."'";
	$lastmonth_totalorder_row = tep_db_query($last_totalorder_query);
	$lastmonth_totalorder = tep_db_fetch_array($lastmonth_totalorder_row);

	$current_order_product_sum = (int)$current_ordertotal['currentmonth_total'] + (int)$lastmonth_totalorder['lastmonth_total'];
	$lastmonth_totalorder = ($lastmonth_totalorder['lastmonth_total'] > 0)?$lastmonth_totalorder['lastmonth_total']:1;
	$percentage = ($current_ordertotal['currentmonth_total'] - $lastmonth_totalorder['lastmonth_total']) / $lastmonth_totalorder;
	$toatal_order = $percentage * 100;
	return $toatal_order;
}
 function add_tracking_url($shipping_type,$tracking_nr){
	if($tracking_nr != '')
	{
		$link = '';
		if($shipping_type == 'ups' || $shipping_type == 'upsxml') {
		   $link = 'http://wwwapps.ups.com/WebTracking/track?trackNums='.$tracking_nr.'&track.x=Track';
		}elseif($shipping_type == 'fedex' || $shipping_type == 'fedexwebservices'){
			$link = 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber='.$tracking_nr.'&cntry_code=us';
		}else {
		   $link = 'https://tools.usps.com/go/TrackConfirmAction.action?tLabels='.$tracking_nr;
		}
		if($link == ''){
		   return $tracking_nr;
		}else{
		   return $tracking_nr_cmt = '<a href=\''.$link.'\' target=\'_blank\'>'.$tracking_nr.'</a>';
		}
	}
 }
function post_curl($post_url, $postdata)
{
	$ch = curl_init();
	$postvars = '';
	foreach($postdata as $key=>$value) {
		$postvars .= $key . "=" . $value . "&";
	}
	$url = $post_url;
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_POST, 1);                //0 for a get request
	curl_setopt($ch, CURLOPT_POSTFIELDS,$postvars);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,3);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($ch);
	curl_close ($ch);
	return $response;
}

 function sanitize($title) {
	 $title = sanitize_title_extra($title);
     return $title;
}
function sanitize_title_extra( $title ) {
    $friendlyURL = $title;
    $friendlyURL = preg_replace('/&([a-z]{1,2})(?:acute|lig|grave|ring|tilde|uml|cedil|caron);/i','\1',$friendlyURL);
    $friendlyURL = html_entity_decode($friendlyURL,ENT_COMPAT, "UTF-8");
    $friendlyURL = preg_replace('/[^a-z0-9-._]+/i', '-', $friendlyURL);
    $friendlyURL = preg_replace('/-+/', '-', $friendlyURL);
    $friendlyURL = str_replace("'", '', $friendlyURL);
    $friendlyURL = trim($friendlyURL, '-');
    $friendlyURL = strtolower($friendlyURL);
    $friendlyURL = rtrim($friendlyURL,'.');
    return $friendlyURL;
}
function check_permalink($permalink_name, $language_id) {
    $permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$permalink_name . "' and language_id = '" . (int)$language_id . "'");
    if(tep_db_num_rows ($permalink_query) > 0){
      $permalink_name = 1;
    }else{
		$permalink_name = 0;
	}

    return $permalink_name;
 }
function check_dup_permalink($checktype, $checkid=0, $pname, $language_id) {
	$web_id = ($web_id > 0)?$web_id:1;
	$language_id = ($language_id > 0)?$language_id:1;
    $status = false;
    $i=0;
    while ($status === false) {
		if($i > 0)
			$pname .= '-'.$i;
		$query = "select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$pname . "' and language_id = '" . (int)$language_id . "'";
		if($checkid > 0)
			$query .= ' and '.$checktype."!='". $checkid ."'";
		$permalink_query = tep_db_query($query);
		if(tep_db_num_rows($permalink_query) === 0) {
            $status = true;
            return $pname;
		}
		$i++;
	}
}
function tep_get_products_permalink_name($products_id, $language_id=1) {
   $permalink_name = '';
   if($products_id > 0){
    $permalink_query = tep_db_query("select permalink_name from " . TABLE_PERMALINK . " where products_id = '" . (int)$products_id . "' and language_id = '" . (int)$language_id . "'");
    if(tep_db_num_rows ($permalink_query) > 0){
      $permalink = tep_db_fetch_array($permalink_query);
      $permalink_name = $permalink['permalink_name'];
    }
   }
    return $permalink_name;
}
 function tep_get_permalink_name($category_id, $language_id) {
	$permalink_name = '';
    if($category_id > 0){
    $permalink_query = tep_db_query("select permalink_name from " . TABLE_PERMALINK . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    if(tep_db_num_rows ($permalink_query) > 0){
      $permalink = tep_db_fetch_array($permalink_query);
      $permalink_name = $permalink['permalink_name'];
     }
    }
    return $permalink_name;
  }
   function checking_update_category_permalink($permalink_name, $categories_id, $language_id) {
        $permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$permalink_name . "' and language_id = '" . (int)$language_id . "' and categories_id != '" . (int)$categories_id . "'");
        if(tep_db_num_rows ($permalink_query) > 0){
          $permalink_name = 1;
        }else{
    		$permalink_name = 0;
    	}

        return $permalink_name;
  }
function tep_pages_get_permalink_name($pages_id, $language_id) {
	if($pages_id > 0) {
		$permalink_query = tep_db_query("select permalink_name from " . TABLE_PERMALINK . " where pages_id = '" . (int)$pages_id . "' and language_id = '" . (int)$language_id . "'");
		if(tep_db_num_rows ($permalink_query) > 0){
		  $permalink = tep_db_fetch_array($permalink_query);
		  $permalink_name = $permalink['permalink_name'];
		}else{
			$permalink_name = '';
		}
	}
	else
	$permalink_name = '';

    return $permalink_name;
}
function tep_pages_categories_permalink_name($pages_categories_id, $language_id) {
	if($pages_categories_id > 0) {
		$permalink_query = tep_db_query("select permalink_name from " . TABLE_PERMALINK . " where pages_categories_id = '" . (int)$pages_categories_id . "' and language_id = '" . (int)$language_id . "'");
		if(tep_db_num_rows ($permalink_query) > 0){
		  $permalink = tep_db_fetch_array($permalink_query);
		  $permalink_name = $permalink['permalink_name'];
		}else{
			$permalink_name = '';
		}
	}
	else
	$permalink_name = '';

    return $permalink_name;
}
function tep_manufacture_permalink_name($manufacturers_id) {
	if($manufacturers_id > 0) {
		$permalink_query = tep_db_query("select permalink_name from " . TABLE_PERMALINK . " where manufacturers_id = '" . (int)$manufacturers_id . "'");
		if(tep_db_num_rows ($permalink_query) > 0){
		  $permalink = tep_db_fetch_array($permalink_query);
		  $permalink_name = $permalink['permalink_name'];
		}else{
			$permalink_name = '';
		}
	}
	else
	$permalink_name = '';

    return $permalink_name;
}

  function tep_get_topic_permalink_name($topics_id) {
   global $languages_id;
   $permalink_name = '';
   if($topics_id > 0){
    $permalink_query = tep_db_query("select permalink_name from " . TABLE_PERMALINK . " where topics_id = '" . (int)$topics_id . "' and language_id = '" . (int)$languages_id . "' ");
    if(tep_db_num_rows ($permalink_query) > 0){
      $permalink = tep_db_fetch_array($permalink_query);
      $permalink_name = $permalink['permalink_name'];
    }
   }
    return $permalink_name;
  }
  function tep_get_article_permalink_name($articles_id) {
   global $languages_id;
   $permalink_name = '';
   if($articles_id > 0){
    $permalink_query = tep_db_query("select permalink_name from " . TABLE_PERMALINK . " where articles_id = '" . (int)$articles_id . "' and language_id = '" . (int)$languages_id . "' ");
    if(tep_db_num_rows ($permalink_query) > 0){
      $permalink = tep_db_fetch_array($permalink_query);
      $permalink_name = $permalink['permalink_name'];
    }
   }
    return $permalink_name;
  }
function get_cat_name_lang($categories_id, $language_id){
	$query = tep_db_query("SELECT CD.categories_name FROM categories C INNER JOIN categories_description CD ON C.categories_id = CD.categories_id WHERE CD.categories_id = '".(int)$categories_id."' and CD.language_id = '".(int)$language_id."'");
	if(tep_db_num_rows($query) > 0){
		$row = tep_db_fetch_array($query);
		return $row['categories_name'];
	}else{
		return '';
	}
}

function get_prod_name_lang($products_id, $language_id){
	$query = tep_db_query("SELECT PD.products_name FROM products P INNER JOIN products_description PD  ON P.products_id = PD.products_id WHERE PD.products_id = '".(int)$products_id."' and PD.language_id = '".(int)$language_id."' order by PD.products_id desc");
	if(tep_db_num_rows($query) > 0){
		$row = tep_db_fetch_array($query);
		return $row['products_name'];
	}else{
		return '';
	}
}
  function check_manufacture_permalink($permalink_name) {
    $permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$permalink_name . "'");
    if(tep_db_num_rows ($permalink_query) > 0){
      $permalink_name = 1;
    }else{
		$permalink_name = 0;
	}

    return $permalink_name;
  }
  function checking_update_manufacture_permalink($permalink_name, $manufacturers_id) {

    $permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$permalink_name . "'  and manufacturers_id != '" . (int)$manufacturers_id . "'");
    if(tep_db_num_rows ($permalink_query) > 0){
      $permalink_name = 1;
    }else{
		$permalink_name = 0;
	}

    return $permalink_name;
  }






function get_permalink_name($id,$type)
{
		switch($type)
		{
			case"category":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where categories_id = '". (int) $id ."' ");
			break;
			case"product":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where products_id = '". (int) $id ."' ");
			break;
			case"page_category":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where pages_categories_id = '". (int) $id ."' ");
			break;
			case"page":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where pages_id = '". (int) $id ."' ");
			break;
			case"manufacturer":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where manufacturers_id = '". (int) $id ."' ");
			break;
			case"link_category":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where link_categories_id = '". (int) $id ."' ");
			break;
			case"faq_category":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where faq_categories_id = '". (int) $id ."' ");
			break;
			case"album_gallery":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where albums_id = '". (int) $id ."' ");
			break;
			case"form_builder":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where form_id = '". (int) $id ."' ");
			break;
			case"articles_category":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where topics_id = '". (int) $id ."' ");
			break;
			case"articles_info":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where articles_id = '". (int) $id ."' ");
			break;
			case"testimonialcat":
				$permalink_query = tep_db_query("SELECT permalink_name FROM permalinks where testimonial_cat_id = '". (int) $id ."' ");
			break;
	    }
		if(tep_db_num_rows($permalink_query) > 0){
			$row_perma = tep_db_fetch_array($permalink_query);
			$page_name = $row_perma['permalink_name'];
		}


	return $page_name;
}

 function tep_cfg_colorpicker($colorcode, $field_name=''){
	return tep_draw_input_field('configuration['. $field_name .']', $colorcode, 'class="form-control jscolor {required:false} jscolor-active" autocomplete="off"');
}

function tep_cfg_rangerpicker($value, $field_name=''){
	echo '<div id="slidecontainer"> <input type="range" min="1" max="10" value="'.$value.'" class="slider" id="myRange" style="max-width:50%"> <p> Transparency level at <span id="demo"></span> %</p> <input type="hidden" id="demo2" name="configuration['. $field_name .']" value="'.$value.'" readonly=""> </div>';
}

function tep_get_assigned_categories($pID){
	global $languages_id;
	$categories = array();
	$p2c_query = tep_db_query("select categories_id from products_to_categories where products_id = '".(int)$pID."' ");
	while($row = tep_db_fetch_array($p2c_query)){
		$categories[] = $row['categories_id'];
	}
	$categories_id = implode(',',$categories);
	if($categories_id != ''){
		return $categories_id;
	}else{
		return false;
	}
}

include(DIR_WS_FUNCTIONS . 'wysiwyg_editor.php');
include(DIR_WS_FUNCTIONS . 'permalinks_function.php');
include_once(DIR_FS_CATALOG .'includes/functions/common.php'); //Common functions used in both front and admin
?>
