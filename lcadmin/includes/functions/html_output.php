<?php
/*
  $Id: html_output.php,v 1.1.1.1 2004/03/04 23:39:54 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

////
// The HTML href link wrapper function
  function tep_href_link($page = '', $parameters = '', $connection = 'NONSSL') {
    global $request_type, $session_started, $SID, $spider_flag, $arr_addon_pages;

    $arr_available_addons = json_decode(AVAILABLE_ADDONS_jSON);
	foreach($arr_available_addons as $indv_addons) {
	  $addon_mod = substr($indv_addons, 3);
	  if(is_array($arr_addon_pages)) {
	    if(is_array($arr_addon_pages[$addon_mod])) {
	      if(in_array($page, $arr_addon_pages[$addon_mod])) {
		    $parameters = (trim($parameters) == '')?'routes='. $addon_mod .'/'.substr($page, 0, -4):'routes='. $addon_mod .'/'.substr($page, 0, -4).'&'.$parameters;
		    $page = 'addons.php';
	      }
	    }
	  }
	}
    if ($page == '') {
      die('<font color="#ff0000">'.UNABLE_TO_DETERMINE_PAGE_LINK.'tep_href_link(\'' . $page . '\', \'' . $parameters . '\', \'' . $connection . '\')</b>');
    }
    if ($connection == 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_ADMIN;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == 'true') {
        // A bug in the install routines cause the DIR_WS_ADMIN to be incorrectly set in the configuration file
        // This is a generated value.  To by pass this issue, DIR_WS_HTTPS_ADMIN will be used
        $link = HTTPS_SERVER . DIR_WS_HTTPS_ADMIN;
      } else {
        $link = HTTP_SERVER . DIR_WS_HTTP_ADMIN;
      }
    } else {
      die('<font color="#ff0000">'.UNABLE_TO_DETERMINE_CONNECTION_METHOD_ON_PAGE_LINK.'tep_href_link(\'' . $page . '\', \'' . $parameters . '\', \'' . $connection . '\')</b>');
    }
    if ($parameters == '') {
      $link = $link . $page ;
    } else {
      $link = $link . $page . '?' . tep_output_string($parameters, false, true) ;
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);
    
    $link = str_replace('&amp;', '&', $link);
    return $link;
  }

  function tep_catalog_href_link($page = 'index.php', $parameters = '', $connection = 'NONSSL') {

    global $request_type, $session_started, $SID, $spider_flag, $arr_addon_pages;
    $req_page = $page;
    parse_str(str_ireplace('&amp;', '&', $parameters), $output_param);
    $output = $output_param;
    if(isset($output_param['rt']))
    {
  	  unset($output_param['rt']);
	  $parameters = http_build_query($output_param);
    }
    $arr_available_addons = json_decode(AVAILABLE_ADDONS_jSON);
    $addons_page = 0;
    foreach($arr_available_addons as $indv_addons) {
   	  $addon_mod = substr($indv_addons, 3);
      $arr_mod_files = is_array($arr_addon_pages[$addon_mod])?$arr_addon_pages[$addon_mod]:array();
      if(in_array($page, $arr_mod_files)) {
    	$parameters = (trim($parameters) == '')?'rt='. $addon_mod .'/'.substr($page, 0, -4):'rt='. $addon_mod .'/'.substr($page, 0, -4).'&'.$parameters;
    	$page_mod = substr($page, 0, -4);
	    $addons_page = 1;
      }
    }

  	if(!$addons_page) {
    	$parameters = (trim($parameters) == '')?'rt=core/'.substr($page, 0, -4):'rt=core/'.substr($page, 0, -4).'&'.$parameters;
    	$page_mod = substr($page, 0, -4);
  	}
	$page = 'index.php';

	if (lc_check_constant_val('MODULE_ADDONS_ULTIMATESEO_STATUS',  'True'))
	{
		$friendly_name = '';
		if(isset($output_param['products_id']))
			$friendly_name = get_permalink_name($output['products_id'],'product');
		elseif(isset($output['cPath'])) {
			$arr_cat_path = explode('_', $output['cPath']);
			$friendly_name = '';
			if(count($arr_cat_path) > 0){
				$cat_id = end($arr_cat_path);
				$friendly_name = (($friendly_name == '')? get_permalink_name($cat_id, 'category'): $friendly_name.'/'.get_permalink_name($cat_id, 'category'));
			}
		}
		elseif(isset($output['manufacturers_id']) && $output['manufacturers_id'] > 0) 
			$friendly_name =get_permalink_name($output['manufacturers_id'], 'manufacturer');
		elseif(isset($output['forms_id']) && $output['forms_id'] > 0)
			$friendly_name = 'forms/'.get_permalink_name($output['forms_id'], 'form_builder');
		elseif(isset($output['pID']))
			$friendly_name = get_permalink_name($output['pID'], 'page');
		elseif(isset($output['lPath']))
			$friendly_name = 'links/'.get_permalink_name($output['lPath'], 'link_category');
		elseif(isset($output['cID']))
			$friendly_name = 'faq/'.get_permalink_name($output['cID'], 'faq_category');
		elseif(isset($output['tPath']))
			$friendly_name = 'articles/'.get_permalink_name($output['tPath'], 'articles_category');
		elseif(isset($output['articles_id']))
			$friendly_name = 'articles/'.get_permalink_name($output['articles_id'], 'articles_info');
		elseif(isset($output['testcategories_id']))
			$friendly_name = 'customer-testimonials/'.get_permalink_name($output['testcategories_id'], 'testimonialcat');
		elseif(isset($output['testimonial_id']))
			$friendly_name = 'customer-testimonials/tmid-'.$output['testimonial_id'];
		elseif(isset($output['lPath']))
			$friendly_name.= '/'.get_permalink_name($output['lPath'], 'link_category');
		elseif(isset($output['CDpath']))
			$friendly_name = get_permalink_name($output['CDpath'], 'page_category');
		else {
		    parse_str(str_ireplace('&amp;', '&', $parameters), $indv_param);
			$def_route = $indv_param['rt'];
			 $query = "SELECT * FROM default_permalink WHERE route='$def_route'";
			$rw_def = tep_db_fetch_array(tep_db_query($query));
			if(trim($rw_def['permalink_name']) != '')
				$friendly_name = trim($rw_def['permalink_name']);
		}

		if(isset($output['order_id']) && $output['order_id'] > 0)
			$friendly_name_param = '/oid-'. (int) $output['order_id'];
		elseif(isset($output['tlid']) && $output['tlid'] != '')
			$friendly_name_param = '/tlid-'.$output['tlid'];
		elseif(isset($output['fID']) && $output['fID'] != '')
			$friendly_name_param = '/fid-'.$output['fID'];
		elseif(isset($output['pid']) && $output['pid'] > 0)
			$friendly_name_param = '/pid-'.$output['pid'];
		elseif(isset($output['page']))
			$friendly_name_param = '/page-'.$output['page'];
		elseif(isset($output['sub']) && $output['sub'] != '') 
			$friendly_name_param = '/sub-'.$output['sub'];
		elseif(isset($output['error_message']) && $output['error_message'] != '')
			$friendly_name_param = '/ERROR-'.$output['error_message'];
		elseif(isset($output['success_message']) && $output['success_message'] != '')
			$friendly_name_param = '/SUCCESS-'.$output['success_message'];
		elseif(isset($output['info_message']) && $output['info_message'] != '')
			$friendly_name_param = '/WARNING-'.$output['info_message'];
		elseif(isset($output['edit']))
			$friendly_name_param = '/edit-'.$output['edit'];
		elseif(isset($output['delete']))
			$friendly_name_param = '/delete-'.$output['delete'];
		elseif(isset($output['action']) && $output['action'] == 'deleteconfirm')
			$friendly_name_param = '/deleteconfirm';
		elseif($req_page == 'product_reviews_write.php')	
			$friendly_name_param = '/write-your-review';
		elseif(isset($output['authors_id']) && (int) $output['authors_id'] > 0)
			$friendly_name_param = '/aid-'. (int) $output['authors_id'];
		elseif($req_page == 'article_reviews.php')	
			$friendly_name_param = '/article-reviews';
		elseif($req_page == 'article_reviews_write.php')	
			$friendly_name_param = '/write-your-article-review';
		elseif($req_page == 'article_reviews_info.php')	
			$friendly_name_param = '/arid-'. (int) $output['reviews_id'];

		
		$found_friendly_name = 0;
		if(trim($friendly_name) != '') {
			$parameters = '';
			$page = $friendly_name.$friendly_name_param;
			$found_friendly_name = 1;
		}
		$seo_params = '';
		$allowed_actions = array('buy_now', 'buy_quote', 'send', 'edit_quote', 'add_wishlist', 'add_product', 'edit_quote', 'emptycart', 'update_product', 'process');
		if(isset($output['action']) && in_array($output['action'], $allowed_actions))
			$seo_params = ($seo_params != '')?$seo_params."&action=".$output['action']:"action=".$output['action'];

		
		$allowed_append_vars = array('keywords', 'popup', 'subspopup', 'show_limit', 'sort', 'display', 'qID', 'quotes_id');
		foreach($allowed_append_vars as $output_key) {
			if(isset($output[$output_key]))
				$seo_params = ($seo_params != '')?$seo_params."&action=".$output['action']:"action=".$output['action'];
		}
		
		if($found_friendly_name) {
			$parameters = $seo_params;
		}

	}
    parse_str(str_ireplace('&amp;', '&', $parameters), $final_param);
    if(count($final_param) == 1 && (isset($final_param['rt']) && $final_param['rt'] == 'core/index')) {
    	$parameters = '';
    	$page = '';
    }
    else {
		if (!tep_not_null($page)) {
		  die('</td></tr></table></td></tr></table><br><br><font color="#ff0000">' . TEP_HREF_LINK_ERROR1);
		}
    }
	
    if ($connection == 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_HTTP_CATALOG;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == 'true') {
        $link = HTTPS_SERVER . DIR_WS_HTTPS_CATALOG;
      } else {
        $link = HTTP_SERVER . DIR_WS_HTTP_CATALOG;
      }
    } else {
      die('</td></tr></table></td></tr></table><br><br><font color="#ff0000">' . TEP_HREF_LINK_ERROR2);
    }
    if (tep_not_null($parameters)) {
      while ( (substr($parameters, -5) == '&amp;') ) $parameters = substr($parameters, 0, strlen($parameters)-5);
      $link .= $page . '?' . tep_output_string($parameters, false, true);
      $separator = '&amp;';
    } else {
      $link .= $page;
      $separator = '?';
    }

    $link = str_replace('&amp;', '&', $link);
    return $link;
  }

////
// The HTML image wrapper function
  function tep_image($src, $alt = '', $width = '', $height = '', $params = '') {
    $image = '<img src="' . $src . '" border="0" alt="' . tep_output_string($alt) . '"';
    if ($alt) {
      $image .= ' title=" ' . $alt . ' "';
    }
    if ($width) {
      $image .= ' width="' . $width . '"';
    }
    if ($height) {
      $image .= ' height="' . $height . '"';
    }
    if ($params) {
      $image .= ' ' . $params;
    }
    $image .= '>';

    return $image;
  }

////
// The HTML form submit button wrapper function
// Outputs a button in the selected language
  function tep_image_submit($image='', $value = '-AltValueError-', $params = '') {
    global $language;
    $width = strlen(html_entity_decode($value)) * CSS_BUTTON_CHAR_WIDTH;
    $width = (int)$width;
    //if ($width < CSS_BUTTON_MIN_WIDTH) $style = ' style="width: ' . CSS_BUTTON_MIN_WIDTH . 'px;"';
    $style = '';//' style="width: ' . CSS_SUBMIT_BUTTON_WIDTH . 'px;"';
    $cssJS = 'onmouseover="$(this).addClassName(\'cssButtonSubmitOver\')" onmouseout="$(this).removeClassName=(\'cssButtonSubmitOver\')" onmousedown="$(this).addClassName(\'cssButtonSubmitActive\')" onmouseup="$(this).removeClassName(\'cssButtonSubmitActive\')"';
    $image_submit = '<input name="' . $value . '" class="' . CSS_BUTTON_SUBMIT_NORMAL_STYLE . '" ' . $cssJS . ' type="submit" value=" ' . $value . ' " ' . $params . $style . '>';
    return $image_submit;
  }

////
// Output a function button in the selected language
  function tep_image_button($image='', $value = '-AltValueError-', $params = '') {
    global $language;
    $width = strlen(html_entity_decode($value)) * CSS_BUTTON_CHAR_WIDTH;
    $width = (int)$width;
    //if ($width < CSS_BUTTON_MIN_WIDTH) $style = ' style="width: ' . CSS_BUTTON_MIN_WIDTH . 'px;"';
    $style = '';//' style="width: ' . CSS_IMAGE_BUTTON_WIDTH . 'px;"'; 
    $cssJS = 'onmouseover="$(this).addClassName(\'cssButtonOver\')" onmouseout="$(this).removeClassName(\'cssButtonOver\')" onmousedown="$(this).addClassName(\'cssButtonActive\')" onmouseup="$(this).removeClassName(\'cssButtonActive\')"';
    $image = '<span class="cssButton" ' . $cssJS . $params . $style . ' ><div style="text-align: center;">' . $value . '</div></span>';
    return $image;
  }

////
// The HTML form submit button wrapper function
// Outputs a button in the selected language
  function tep_icon_submit($image, $alt = '', $parameters = '') {
    global $language;

    $image_submit = '<input type="image" src="' . tep_output_string(DIR_WS_IMAGES .'icons/' . $image) . '" border="0" alt="' . tep_output_string($alt) . '"';

    if (tep_not_null($alt)) $image_submit .= ' title=" ' . tep_output_string($alt) . ' "';

    if (tep_not_null($parameters)) $image_submit .= ' ' . $parameters;

    $image_submit .= '>';

    return $image_submit;
  }

////
// Draw a 1 pixel black line
  function tep_black_line() {
    return tep_image(DIR_WS_IMAGES . 'pixel_black.gif', '', '100%', '1');
  }

////
// Output a separator either through whitespace, or with an image
  function tep_draw_separator($image = 'pixel_black.gif', $width = '100%', $height = '1') {
    return tep_image(DIR_WS_IMAGES . $image, '', $width, $height);
  }

////
// javascript to dynamically update the states/provinces list when the country is changed
// TABLES: zones
  function tep_js_zone_list($country, $form, $field) {
    $countries_query = tep_db_query("select distinct zone_country_id from " . TABLE_ZONES . " order by zone_country_id");
    $num_country = 1;
    $output_string = '';
    while ($countries = tep_db_fetch_array($countries_query)) {
      if ($num_country == 1) {
        $output_string .= '  if (' . $country . ' == "' . $countries['zone_country_id'] . '") {' . "\n";
      } else {
        $output_string .= '  } else if (' . $country . ' == "' . $countries['zone_country_id'] . '") {' . "\n";
      }

      $states_query = tep_db_query("select zone_name, zone_id from " . TABLE_ZONES . " where zone_country_id = '" . $countries['zone_country_id'] . "' order by zone_name");

      $num_state = 1;
      while ($states = tep_db_fetch_array($states_query)) {
        if ($num_state == '1') $output_string .= '    ' . $form . '.' . $field . '.options[0] = new Option("' . PLEASE_SELECT . '", "");' . "\n";
        $output_string .= '    ' . $form . '.' . $field . '.options[' . $num_state . '] = new Option("' . $states['zone_name'] . '", "' . $states['zone_id'] . '");' . "\n";
        $num_state++;
      }
      $num_country++;
    }
    $output_string .= '  } else {' . "\n" .
                      '    ' . $form . '.' . $field . '.options[0] = new Option("' . TYPE_BELOW . '", "");' . "\n" .
                      '  }' . "\n";

    return $output_string;
  }

////
// Output a form
  function tep_draw_form($name, $action, $parameters = '', $method = 'post', $params = '', $connection = 'NONSSL') {
	global $arr_addon_pages;

    $form = '<form name="' . tep_output_string($name) . '" action="';

    if (tep_not_null($parameters)) 
      $form_action = tep_href_link($action, $parameters, $connection);
    else 
      $form_action = tep_href_link($action, '', $connection);
    
    $url_query_string = parse_url($form_action, PHP_URL_QUERY);
	parse_str($url_query_string, $output_qs);
    
    $form .= $form_action;
    $form .= '" method="' . tep_output_string($method) . '"';
    if (tep_not_null($params)) {
      $form .= ' ' . $params;
    }
    $form .= '>';
    if($method == 'get') {
		if(isset($output_qs['routes'])) {
			$form .= "\n".tep_draw_hidden_field('routes', $output_qs['routes']);
		}
		else{
			parse_str($parameters, $output_param);
			foreach($output_param as $param_key=>$param_val) {
				$form .= "\n".tep_draw_hidden_field($param_key, $param_val);
			}
		}
	}

    return $form;
  }

////
// Output a form input field
  function tep_draw_input_field($name, $value = '', $parameters = '', $required = false, $type = 'text', $reinsert_value = true) {
    $field = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '"';

    if($type == 'file')
    	$field .= ' class="filestyle" ';
    elseif($type == 'text')	
    	$field .= ' class="form-control" ';
    if (isset($GLOBALS[$name]) && ($reinsert_value == true) && is_string($GLOBALS[$name])) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    } elseif (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    }
    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    if ($required == true) $field .= ' required';
    $field .= '>';

    //if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }

////
// Output a form password field
  function tep_draw_password_field($name, $value = '', $required = false, $parameters = 'maxlength="40" class="form-control" autocomplete="off"') {
    $field = tep_draw_input_field($name, '', $parameters, $required, 'password', false);

    return $field;
  }


////
// Output a form filefield
 function tep_draw_file_field($name, $size = '25', $required = false, $parameters = '') {
    $field = tep_draw_input_field($name, '', 'size="' . $size . '" ' . $parameters, $required, 'file');

    return $field;
  }





//Admin begin
////
// Output a selection field - alias function for tep_draw_checkbox_field() and tep_draw_radio_field()
//  function tep_draw_selection_field($name, $type, $value = '', $checked = false, $compare = '') {
//    $selection = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '"';
//
//    if (tep_not_null($value)) $selection .= ' value="' . tep_output_string($value) . '"';
//
//    if ( ($checked == true) || (isset($GLOBALS[$name]) && is_string($GLOBALS[$name]) && ($GLOBALS[$name] == 'on')) || (isset($value) && isset($GLOBALS[$name]) && (stripslashes($GLOBALS[$name]) == $value)) || (tep_not_null($value) && tep_not_null($compare) && ($value == $compare)) ) {
//      $selection .= ' CHECKED';
//    }
//
//    $selection .= '>';
//
//    return $selection;
//  }
//
////
// Output a form checkbox field
//  function tep_draw_checkbox_field($name, $value = '', $checked = false, $compare = '') {
//    return tep_draw_selection_field($name, 'checkbox', $value, $checked, $compare);
//  }
//
////
// Output a form radio field
//  function tep_draw_radio_field($name, $value = '', $checked = false, $compare = '') {
//    return tep_draw_selection_field($name, 'radio', $value, $checked, $compare);
//  }
////
// Output a selection field - alias function for tep_draw_checkbox_field() and tep_draw_radio_field()
  function tep_draw_selection_field($name, $type, $value = '', $checked = false, $compare = '', $parameter = '') {
    $selection = '<input type="' . $type . '" name="' . $name . '"';
    if ($value != '') {
      $selection .= ' value="' . $value . '"';
    }
    if ( ($checked == true) || (isset($GLOBALS[$name]) && $GLOBALS[$name] == 'on') || ($value && (isset($GLOBALS[$name]) && $GLOBALS[$name] == $value)) || ($value && ($value == $compare)) ) {
      $selection .= ' CHECKED';
    }
    if ($parameter != '') {
      $selection .= ' ' . $parameter;
    }
    $selection .= '>';

    return $selection;
  }

////
// Output a form checkbox field
  function tep_draw_checkbox_field($name, $value = '', $checked = false, $compare = '', $parameter = '') {
    return tep_draw_selection_field($name, 'checkbox', $value, $checked, $compare, $parameter);
  }


////
// Output a form radio field
  function tep_draw_radio_field($name, $value = '', $checked = false, $compare = '', $parameter = '') {
    return tep_draw_selection_field($name, 'radio', $value, $checked, $compare, $parameter);
  }
//Admin end

////
// Output a form textarea field
  function tep_draw_textarea_field($name, $wrap, $width, $height, $text = '', $parameters = '', $reinsert_value = true) {
    // the wrap is removed because it is not W3C standard and creates problem in IE
    $field = '<textarea name="' . tep_output_string($name) . '" cols="' . tep_output_string($width) . '" rows="' . tep_output_string($height) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= stripslashes($GLOBALS[$name]);
    } elseif (tep_not_null($text)) {
      $field .= $text;
    }

    $field .= '</textarea>';

    return $field;
  }

////
// Output a form hidden field
  function tep_draw_hidden_field($name, $value = '', $parameters = '') {
    $field = '<input type="hidden" name="' . tep_output_string($name) . '"';

    if (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    } elseif (isset($GLOBALS[$name]) && is_string($GLOBALS[$name])) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }

////
// Hide form elements
  function tep_hide_session_id() {
    return tep_draw_hidden_field(tep_session_name(), tep_session_id());
  }

////
// Output a form pull down menu
  function tep_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false) {
    $field = '<select class="form-control" name="' . tep_output_string($name) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if (empty($default) && isset($GLOBALS[$name])) $default = stripslashes($GLOBALS[$name]);

    for ($i=0, $n=sizeof($values); $i<$n; $i++) {
      $field .= '<option value="' . (isset($values[$i]['id']) ? tep_output_string($values[$i]['id']) : '') . '"';
      if (isset($values[$i]['id']) && $default == $values[$i]['id']) {
        $field .= ' selected="selected"';
      }

      $field .= '>' . tep_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }
?>
