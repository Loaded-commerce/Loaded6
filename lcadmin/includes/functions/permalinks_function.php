<?php
function sync_faq_categories(){
	$count_faq = 0;
	$faq_query = tep_db_query("SELECT FC.* from ".TABLE_FAQ_CATEGORIES." F INNER JOIN ".TABLE_FAQ_CATEGORIES_DESCRIPTION." FC ON F.categories_id = FC.categories_id");
	while($faq = tep_db_fetch_array($faq_query)){
		$categories_name = $faq['categories_name'];
		$check_faq = tep_db_query("SELECT * FROM permalinks WHERE faq_categories_id = '".(int)$faq['categories_id']."'");
		if(tep_db_num_rows($check_faq) <= 0){
				$slug =  sanitize($categories_name);
				$permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$slug . "'");
				if(tep_db_num_rows ($permalink_query) > 0){
					$suffix = 2;
					$slug = _truncate_post_slug( $slug, 200 - ( strlen( $suffix ) + 1 ) ) . "-$suffix";
				}
				$permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$slug . "'");
				if(tep_db_num_rows ($permalink_query) > 0){
					$suffix = 2;
					$slug = _truncate_post_slug( $slug, 200 - ( strlen( $suffix ) + 1 ) ) .'-'. mt_rand(1,100);

				}

				$sql_data_array = array('faq_categories_id' => $faq['categories_id'],
								  'filename' => 'faq.php',
								  'route' => 'core/faq',
								  'permalink_name' => $slug,
								  'permalink_type' => 'faq_category');


				tep_db_perform(TABLE_PERMALINK, $sql_data_array);
				$count_faq++;
		}
	}//faq sync end
}
function sync_link_categories(){
	/*******Link categories_description sync start **************/
	$count_linkcat = 0;
	$linkcat_query = tep_db_query("SELECT FC.* from ".TABLE_LINK_CATEGORIES." F INNER JOIN ".TABLE_LINK_CATEGORIES_DESCRIPTION." FC ON F.link_categories_id = FC.link_categories_id");
	while($linkcat = tep_db_fetch_array($linkcat_query)){
		$link_categories_name = $linkcat['link_categories_name'];
		$check_linkcat = tep_db_query("SELECT * FROM permalinks WHERE link_categories_id = '".(int)$linkcat['link_categories_id']."'");
		if(tep_db_num_rows($check_linkcat) <= 0){
				$slug =  sanitize($link_categories_name);
				$permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$slug . "'");
				if(tep_db_num_rows ($permalink_query) > 0){
					$suffix = 2;
					$slug = _truncate_post_slug( $slug, 200 - ( strlen( $suffix ) + 1 ) ) . "-$suffix";
				}
				$permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$slug . "'");
				if(tep_db_num_rows ($permalink_query) > 0){
					$suffix = 2;
					$slug = _truncate_post_slug( $slug, 200 - ( strlen( $suffix ) + 1 ) ) .'-'. mt_rand(1,100);

				}

				$sql_data_array = array('link_categories_id' => $linkcat['link_categories_id'],
								  'filename' => 'links.php',
								  'route' => 'core/links',
								  'permalink_name' => $slug,
								  'permalink_type' => 'link_category');


				tep_db_perform(TABLE_PERMALINK, $sql_data_array);
				$count_linkcat++;
		}
	}//linkcat sync end
}
//Manufactures
function sync_manufactures(){
	$count_manuf = 0;
	$manuf_query = tep_db_query("SELECT * from ".TABLE_MANUFACTURERS."");
	while($manuf = tep_db_fetch_array($manuf_query)){
		$manufacturers_name = $manuf['manufacturers_name'];
			$check_manuf = tep_db_query("SELECT * FROM permalinks WHERE manufacturers_id = '".(int)$manuf['manufacturers_id']."' and language_id = '".(int)$language_id."'");
			if(tep_db_num_rows($check_manuf) <= 0){

					$slug =  sanitize($manufacturers_name);
					$permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$slug . "' and language_id = '" . (int)$language_id . "'");
					if(tep_db_num_rows ($permalink_query) > 0){
						$suffix = 2;
						$slug = _truncate_post_slug( $slug, 200 - ( strlen( $suffix ) + 1 ) ) . "-$suffix";
					}
					$permalink_query = tep_db_query("select permalink_id from " . TABLE_PERMALINK . " where permalink_name = '" .$slug . "' and language_id = '" . (int)$language_id . "'");
					if(tep_db_num_rows ($permalink_query) > 0){
						$suffix = 2;
						$slug = _truncate_post_slug( $slug, 200 - ( strlen( $suffix ) + 1 ) ) .'-'. mt_rand(1,100);
					}

					$sql_data_array = array('manufacturers_id' => $manuf['manufacturers_id'],
										  'language_id' => $language_id,
										  'route' => 'core/index',
										  'permalink_name' => $slug,
										  'permalink_type' => 'manufacturer');

					tep_db_perform(TABLE_PERMALINK, $sql_data_array);
					$count_manuf++;
			}
	   }//while
}


?>