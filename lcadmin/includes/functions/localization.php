<?php
/*
  $Id: localization.php,v 1.1.1.1 2004/03/04 23:39:55 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  function quote_currencyconverterapi_currency($code, $base = DEFAULT_CURRENCY) {

    if ($base != $code) {
      $result = file('https://free.currencyconverterapi.com/api/v5/convert?q=' . $base . '_' . $code . '&compact=ultra');
      $key = $base . '_' . $code;
      $rateArr = (isset($result[0])) ? json_decode($result[0], true) : array();
      $rate = (isset($rateArr[$key])) ? $rateArr[$key] : 0;
    } else {
      $rate = 1;
    }

    if ($rate > 0) {
      return $rate;
    } else {
      return false;
    }
  }

  function quote_1forge_currency($code, $base = DEFAULT_CURRENCY) {

    // get your APi key - https://1forge.com/register
    $api_key = 'm2nsaKjczaAvaKpNm3ZtbALge9i7j88F';

    if ($base != $code) {
      $result = file('https://forex.1forge.com/1.0.3/convert?from=' . $base . '&to=' . $code . '&quantity=1&api_key=' . $api_key);
      $data = (isset($result[0])) ? json_decode($result[0], true) : array();
      $rate = (isset($data['value'])) ? $data['value'] : 0;
    } else {
      $rate = 1;
    }

    if ($rate > 0) {
      return $rate;
    } else {
      return false;
    }
  }
?>
