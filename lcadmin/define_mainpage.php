<?php
/*
  $Id: define_mainpage.php,v 1.1 2008/06/11 00:18:17 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');
$_GET['filename'] = 'mainpage.php';
$action = (isset($_GET['action']) ? $_GET['action'] : '');
$lngdir = (isset($_GET['lngdir']) ? $_GET['lngdir'] : '');
switch ($action) {
  case 'save':
    if ( ($lngdir) && ($_GET['filename']) ) {
      //if ($_GET['filename'] == $language . '.php') {
      //  $file = DIR_FS_CATALOG_LANGUAGES . $_GET['filename'];
      //} else {
        $file = DIR_FS_CATALOG_LANGUAGES . $_GET['lngdir'] . '/' . $_GET['filename'];
      //}
      if (file_exists($file)) {
        if (file_exists(DIR_FS_CATALOG_LANGUAGES . $_GET['lngdir'] . '/bak' . $_GET['filename'])) {
          @unlink(DIR_FS_CATALOG_LANGUAGES . $_GET['lngdir'] . '/bak' . $_GET['filename']);
        }
        @rename($file, DIR_FS_CATALOG_LANGUAGES . $_GET['lngdir'] . '/bak' . $_GET['filename']);
        $new_file = fopen($file, 'w');
        $file_contents = stripslashes($_POST['file_contents']);
        fwrite($new_file, $file_contents, strlen($file_contents));
        fclose($new_file);
      }
      tep_redirect(tep_href_link(FILENAME_DEFINE_MAINPAGE, 'lngdir=' . $_GET['lngdir']));
    }
    break;
}
if (!$lngdir) $lngdir = $language;
$languages_array = array();
$languages = tep_get_languages();
$lng_exists = false;
for ($i=0; $i<sizeof($languages); $i++) {
  if ($languages[$i]['directory'] == $lngdir) $lng_exists = true;
  $languages_array[] = array('id' => $languages[$i]['directory'],
                             'text' => $languages[$i]['name']);
}
if (!$lng_exists) $_GET['lngdir'] = $language;


include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');

echo tep_load_html_editor();
echo tep_insert_html_editor('file_contents');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('definemainpage') > 0) {
      echo $messageStack->output('definemainpage'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-definemainpage" class="table-definemainpage">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">


		<table border="0" width="100%" cellspacing="0" cellpadding="2">
          <?php
          if ( ($lngdir) && ($_GET['filename']) ) {
            if ($_GET['filename'] == $language . '.php') {
              $file = DIR_FS_CATALOG_LANGUAGES . $_GET['filename'];
            } else {
              $file = DIR_FS_CATALOG_LANGUAGES . $lngdir . '/' . $_GET['filename'];
            }
            if (file_exists($file)) {
              $file_array = @file($file);
              $file_contents = @implode('', $file_array);
              $file_writeable = true;
              if (!is_writeable($file)) {
                $file_writeable = false;
                $messageStack->reset();
                $messageStack->add('mainpage', sprintf(ERROR_FILE_NOT_WRITEABLE, $file), 'error');
                if ($messageStack->size('mainpage') > 0) {
                  echo $messageStack->output('mainpage');
                }    
              }
              ?>
              <tr>
                <?php echo tep_draw_form('language', FILENAME_DEFINE_MAINPAGE, 'lngdir=' . $lngdir . '&filename=' . $_GET['filename'] . '&action=save'); ?>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="sidebar-title"><b><?php echo $_GET['filename']; ?></b></td>
                  </tr>
                  <tr>
                    <td class="sidebar-title"><?php echo tep_draw_textarea_field('file_contents', 'soft', '80', '20', $file_contents,' style="width: 100%" class="ckeditor"', (($file_writeable) ? '' : 'readonly')); ?></td>
                  </tr>
                  <tr>
                    <td align="right">
                      <?php 
                      if ($file_writeable) { 
                        echo '<a class="btn btn-default btn-sm mt-2 mb-2 mr-2" href="' . tep_href_link(FILENAME_DEFINE_MAINPAGE, 'lngdir=' . $lngdir) . '">' .  IMAGE_CANCEL . '</a><button class="btn btn-success btn-sm" type="submit">' . IMAGE_SAVE . '</button>'; 
                      } else { 
                        echo '<a class="btn btn-default btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_DEFINE_MAINPAGE, 'lngdir=' . $lngdir) . '">' . IMAGE_BACK . '</a>'; 
                      } 
                      ?>
                    </td>
                  </tr>
                </table></td>
                </form>
              </tr>
              <?php
            } else {
              ?>
              <tr>
                <td class="text-danger"><b><?php echo TEXT_FILE_DOES_NOT_EXIST; ?></b></td>
              </tr>
              <tr>
                <td><?php echo '<a class="btn btn-default btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_DEFINE_MAINPAGE, 'lngdir=' . $_GET['lngdir']) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
              </tr>
              <?php
            }
          } else {
            $filename = $lngdir . '.php';
            ?>
            <tr>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="smallText"><a href="<?php echo tep_href_link(FILENAME_DEFINE_MAINPAGE, 'lngdir=' . $_GET['lngdir'] . '&filename=' . $filename); ?>"><b><?php echo $filename; ?></b></a></td>
                  <?php
                  $dir = dir(DIR_FS_CATALOG_LANGUAGES . $lngdir);
                  $left = false;
                  if ($dir) {
                    $file_extension = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], '.'));
                    while ($file = $dir->read()) {
                      if (substr($file, strrpos($file, '.')) == $file_extension) {
                        echo '<td class="smallText"><a href="' . tep_href_link(FILENAME_DEFINE_MAINPAGE, 'lngdir=' . $lngdir . '&filename=' . $file) . '">' . $file . '</a></td>' . "\n";
                        if (!$left) {
                          echo '</tr>' . "\n" . '<tr>' . "\n";
                        }
                        $left = !$left;
                      }
                    }
                    $dir->close();
                  }
                  ?>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td align="right"><?php echo '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_FILE_MANAGER, 'current_path=' . DIR_FS_CATALOG_LANGUAGES . $_GET['lngdir']) . '">' . IMAGE_FILE_MANAGER . '</a>'; ?></td>
            </tr>
            <?php
          }
          ?>
        </table>
        
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
