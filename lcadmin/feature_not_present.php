<?php
/*
  $Id: validate_New.php,v 1.1.1.1 2004/03/04 23:38:20 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License

 THIS IS BETA - Use at your own risk!
  Step-By-Step Manual Order Entry Verion 0.5
  Customer Entry through Admin
*/

  require('includes/application_top.php');

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> Featured Not Present</h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
 
  <div class="col">   
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-featurednopresent" class="table-featurednopresent">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">




  <table border="0" cellpadding="0" cellspacing="0" style="margin: 1em auto; width: 600px;" align="center">
    <tr>
       <td></td>
       <td style="padding-bottom: 1em;" align="left"><a href="http://store.loadedcommerce.com" target="_blank"><img border="0" src="images/window-logo.png" /></a></td>
       <td></td>
    </tr>
     <tr>
       <td class="box-top-left">&nbsp;</td>
       <td class="box-top">&nbsp;</td>
       <td class="box-top-right" style="width: 48px;" >&nbsp;</td>
     </tr>
     <!--
     <tr>
        <td class="box-left">&nbsp;</td>
        <td style="background: #f0f8fc url(images/window-right.png) repeat-y right;" width="600" colspan="2">
          <img src="images/window-pro.png" align="right" /><img src="images/window-available.png" align="right" style="margin-right: 4px;" />
        </td>
     </tr>
     -->
     <tr>
        <td class="box-left">&nbsp;</td>
        <td class="box-content" valign="top">
          <div class="sidebar-title" style="padding: 1em 0; line-height: 1.8em; font-size: 14px;">
          <p>
          This feature is not available with your current Loaded Commerce product.  If you'd like to upgrade, you'll get additional features, support, and flexibility.</p><p><a href="http://www.creloaded.com/products/shoppingcarts/creloadedpro/" target="_blank">Loaded Commerce Professional</a> and <a href="http://www.creloaded.com/creloaded/products/shoppingcarts/creloadedb2b/" target="_blank">Loaded Commerce Professional B2B</a> include a complete Content Management System, automatic URL search engine optimization and additional email and phone support.  Plus, they have increased capabilities with additional features such as one page checkout and abandoned cart recovery.
          </p>
          </div>
        </td> 
        <td class="box-right" style="width: 48px;">&nbsp;</td>
      </tr>
     <tr>
        <td class="box-left">&nbsp;</td>
        <td align="right" class="box-content" valign="bottom">
        <div style="padding-top: 1em;"><?php echo '<a class="btn btn-success btn-sm mt-2 mb-2" href="http://store.loadedcommerce.com" target="_blank">Learn More</a>'; ?>
        </div>
        </td>
        <td class="box-right" style="width: 48px;">&nbsp;</td>
     </tr>
     <tr>
       <td class="box-bottom-left">&nbsp;</td>
       <td class="box-bottom">&nbsp;</td>
       <td class="box-bottom-right" style="width: 48px;">&nbsp;</td>
     </tr>
  </table>
      
    </td>
    <!-- body_text_eof //-->
  </tr>
</table>

		 </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>