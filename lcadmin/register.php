<?php
/*
  $Id: login.php,v 1.2 2004/03/05 00:36:41 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGIN);
$error = (isset($_SESSION['validate_error']) && $_SESSION['validate_error'] == true) ? true : false;
$error_message = ($error == true) ? $_SESSION['validate_error']['error_message'] : '';
$action = (isset($_GET['action']) && ($_GET['action'] != '')) ? $_GET['action'] : ''; 
$_SESSION['force_registration'] = true;
$paction = (isset($_POST['action']) && ($_POST['action'] != '')) ? $_POST['action'] : '';
$serial = (isset($_POST['serial']) && ($_POST['serial'] != '')) ? tep_db_prepare_input($_POST['serial']) : '';

if ($paction == 'processfree') {

	$postdata['fname'] = tep_db_prepare_input($_POST['fname']);
	$postdata['lname'] = tep_db_prepare_input($_POST['lname']);
	$postdata['email'] = tep_db_prepare_input($_POST['email']);
	$postdata['webserver'] = HTTP_SERVER;
	$postdata['ssl_webserver'] = HTTPS_SERVER;
	$postdata['cookie_domain'] = HTTP_COOKIE_DOMAIN;
	$postdata['ssl_cookie_domain'] = HTTPS_COOKIE_DOMAIN;
	$postdata['metadata'] = json_encode($_SERVER);

	$data = post_curl('https://api.loadedcommerce.com/lc6_addon_api/?action=activatefree', $postdata);
	
}

if (($paction == 'validate') || $serial && !$error) {
  // check for blank serial
    if ($serial == '') {
    $error = true;
    $error_message = TEXT_ERROR_BLANK_SERIAL;    
  }
  // validation logic  
  if (!$error) {

	  // validation logic  
	  if (!$error) {
		$components_query = tep_db_query("SELECT serial_1, validation_product, status from " . TABLE_COMPONENTS);
		if (tep_db_num_rows($components_query) <= 0) {
			$_SESSION['new_registration'] = true;
		}
		// instantiate the serial class
		require_once(DIR_WS_CLASSES . 'sss_verify.php');
		$sss = new sss_verify;
		$verify_array = $sss->verifySerial($serial);
		if ($verify_array['verified'] == true) {
		  if (isset($_SESSION['new_registration']) && $_SESSION['new_registration'] == true) {
			$sss->installversion();
			unset($_SESSION['new_registration']);
			if (isset($_SESSION['force_registration'])) unset($_SESSION['force_registration']);
			$_SESSION['verify_array'] = $verify_array;
			//tep_redirect(FILENAME_REGISTER . '', '', 'SSL');
		    $success_message = $verify_array['error_message'];   
		  } else {
			$_SESSION['continue'] = true;
			//tep_redirect(FILENAME_REGISTER, '', 'SSL');  
		    $success_message = $verify_array['error_message'];   
		  }
		} else {
		  $error = true;
		  $success_message = $verify_array['error_message'];   
		}
		
		if($success_message == '') {
		  $error = true;
		  $success_message = $verify_array['error_message'];   
		}
	  }
  }
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
  <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="assets/img/favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="assets/img/favicons/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="assets/img/favicons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">    
  
  <meta charset="utf-8" />
  <title><?php echo TITLE; ?> | Logoff Page</title>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta content="Create Great Looking Mobile Responsive E-commerce Sites. Designed not just for the challenges of today, but tomorrow. Open source. Unlimited Free Edition." name="description" />
  <meta content="Loaded Commerce" name="author" />
  <?php
  // themes: black, blue, default, red, orange, purple
  $theme = (defined('ADMIN_THEME') && ADMIN_THEME != '') ? ADMIN_THEME : 'default';
  ?>  
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
  <link href="assets/plugins/bootstrap4/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
  <link href="assets/css/animate.min.css" rel="stylesheet" />
  <link href="assets/css/style-bs4.min.css" rel="stylesheet" />
  <link href="assets/css/style.css" rel="stylesheet" />
  <link href="assets/css/style-responsive.min.css" rel="stylesheet" />
  <link href="assets/css/theme/<?php echo $theme; ?>.css" rel="stylesheet" id="theme" />
</head>
<body> 
<style>
.login .login-content {
    padding: 40px 40px 20px 40px;
}
</style>
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->
<div class="login-cover">
  <div class="login-cover-image"><img src="assets/img/login-bg/bg-1.jpg" data-id="login-cover-image" alt="" /></div>
  <div class="login-cover-bg"></div>
</div>
<!-- begin #page-container -->
<div id="page-container" class="fade">
  <!-- begin logoff -->
  <div class="login login-v2" data-pageload-addclass="animated fadeIn">
    <!-- begin brand -->
      <div class="login-header">
        <div class="login-brand clearfix">
          <a href="index.php" class="navbar-brand">
            <span class="login-brand-logo"></span>
            <span class="login-brand-text"> Loaded Commerce </span>
            <small class="login-brand-slogan"><i><?php echo INSTALLED_VERSION_TYPE; ?></i></small>
          </a>
        </div>

        <div class="icon">
          <i class="fa fa-sign-in"></i>
        </div>
      </div>
      <!-- end brand -->

<?php
if(isset($_GET['register']) && $_GET['register'] == 'free')
{
?>
      <div class="login-content pt-3">
        <div class="login-content-heading mt-1 mb-2"><?php echo HEADING_TITLE_FREE_REGISTER; ?></div>        
            <?php
          echo tep_draw_form('login', FILENAME_REGISTER, 'action=processfree', 'post', 'class="margin-bottom-0"', 'SSL') . tep_draw_hidden_field("action","processfree");
          if (!isset($success_message) && !isset($_SESSION['password_forgotten'])){
            ?>                        
            <div class="m-t-10">
              <div class="login-user-message mb-3"><?php echo TEXT_REGISTER_FREE_USER_MESSAGE;?></div>
            </div>
            <div class="form-group m-b-20">
              <input name="fname" id="fname" type="text" class="form-control input-lg" placeholder="Enter First Name" required />
            </div>
            <div class="form-group m-b-20">
              <input name="lname" id="lname" type="text" class="form-control input-lg" placeholder="Enter Last Name" required />
            </div>
            <div class="form-group m-b-20">
              <input name="email" id="email" type="text" class="form-control input-lg" placeholder="Enter Your Email" required />
            </div>
            <div class="checkbox m-b-20">
              <button class="btn btn-success btn-block btn-lg" type="submit"><?php echo TEXT_FREE_PRODUCT_REGISTRATION; ?></button>
            </div>
            <?php 
          } else {
            ?>
            <div class="m-t-20">
              <?php echo $success_message; ?>
            </div>
            <?php
          }
          ?>
        </form>
      </div>
        <!-- end login -->
<?php
}
else
{
?>
      <div class="login-content pt-3">
        <div class="login-content-heading mt-1 mb-2"><?php echo HEADING_TITLE_REGISTER; ?></div>        
            <div class="checkbox m-b-20">
              <a href="<?php echo tep_href_link(FILENAME_REGISTER, 'register=free')?>"><button class="btn btn-primary btn-block btn-lg" type="button"><?php echo TEXT_ACTIVATE_FREE_FEATURE; ?></button></a>
              <div style="text-align:center; padding-top:10px;font-size:16px;font-weight:bold;color:#ffffff">-OR-</div>
            </div>

            <?php echo tep_draw_form('login', FILENAME_REGISTER, 'action=process', 'post', 'class="margin-bottom-0"', 'SSL') . tep_draw_hidden_field("action","process");
          if (isset($_SESSION['password_forgotten'])) {
            ?>
            <div class="form-group m-b-20"><?php echo TEXT_FORGOTTEN_FAIL; ?></div>
            <?php
            $success_message = '';
          } elseif (isset($success_message)) {
            $success_message = '<div class="login-user-message mb-4 mt-3">' . $success_message . '</div>';
            $success_message .= '<div class="checkbox m-b-20"><button class="btn btn-success btn-block btn-lg" onClick="location.href=\'' . HTTP_CATALOG_SERVER . DIR_WS_HTTP_CATALOG . '\'">' . TEXT_VISIT_CATALOG . '</button><button class="btn btn-primary btn-block btn-lg" type="button" onClick="document.location=\'' . tep_href_link(FILENAME_LOGIN, '' , 'SSL') . '\'">' . TEXT_BACK_TO_LOGIN . '</button>';
          } else {
            if (isset($info_message)) {
                echo '<div class="row errmsg"><div class="col p-0 mb-1 mt-1 ml-2 mr-2"><div class="note note-danger m-0"><h4 class="m-0">' . TEXT_ERROR . '</h4><p class="mb-0 mt-2">' . $info_message . tep_draw_hidden_field('log_times', $log_times) . '</p></div></div></div>';     
              echo tep_draw_hidden_field('log_times', '0');

            } else {
              echo tep_draw_hidden_field('log_times', '0');
            }
          }

          if (!isset($success_message) && !isset($_SESSION['password_forgotten'])){
            ?>                        
            <div class="m-t-10">
              <div class="login-user-message mb-3"><?php echo TEXT_REGISTER_USER_MESSAGE;?></div>
            </div>
            <div class="form-group m-b-20">
              <input name="serial" id="serial" type="text" class="form-control input-lg" placeholder="Enter Product Serial" />
            </div>
            <div class="checkbox m-b-20">
              <button class="btn btn-success btn-block btn-lg" type="submit"><?php echo TEXT_PRODUCT_REGISTRATION; ?></button>
              <button class="btn btn-primary btn-block btn-lg" type="button" onClick="window.open('https://store.loadedcommerce.com/loaded-commerce-pro-p-315.html');"><?php echo TEXT_GET_PRO; ?></button>
            </div>
            <?php 
          } else {
            ?>
            <div class="m-t-20">
              <?php echo $success_message; ?>
            </div>
            <?php
          }
          ?>
        </form>
      </div>
        <!-- end login -->
<?php
}
?>
      <div class="p-relative clearfix"><p class="login-brand-version"><i>v<?php echo INSTALLED_VERSION; ?></i></p></div>
  </div>
  <!-- end logoff -->
</div>
<!-- end page container -->
<script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="assets/plugins/tether/js/tether.min.js"></script>
<script src="assets/plugins/bootstrap4/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
    <script src="assets/crossbrowserjs/html5shiv.js"></script>
    <script src="assets/crossbrowserjs/respond.min.js"></script>
    <script src="assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/js/apps.js"></script>
<script>
$(document).ready(function() {
  App.init();
  setTimeout(function(){ $('.errmsg').delay(3000).fadeOut('slow'); }, 5000);
});
</script>
<?php
  require('includes/application_bottom.php');
?>
</body>
</html>
