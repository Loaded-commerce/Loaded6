<?php
/*
  $Id: mail.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');
$vself = 0;
if ( ($action == 'send_email_to_user') && isset($_POST['customers_email_address']) && !isset($_POST['back_x']) ) {
  switch ($_POST['customers_email_address']) {
    case '***':
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS);
      $mail_sent_to = TEXT_ALL_CUSTOMERS;
      break;
    case '**D':
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_newsletter = '1'");
      $mail_sent_to = TEXT_NEWSLETTER_CUSTOMERS;
      break;
    case '***D':
      //$mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_newsletter = '1'");
      $mail_sent_to = TEXT_NEWSLETTER_SELF;
      $vself = 1;
      break;
    default:
      $customers_email_address = strtolower(tep_db_prepare_input($_POST['customers_email_address']));
      $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where lower(customers_email_address) = '" . tep_db_input($customers_email_address) . "'");
      $mail_sent_to = strtolower($_POST['customers_email_address']);
  }

  $from = tep_db_prepare_input($_POST['from']);

  $subject = tep_db_prepare_input($_POST['email_subject']);

  $message = tep_db_prepare_input($_POST['message']);

  if (HTML_WYSIWYG_DISABLE_EMAIL == 'Disable') {
    $email_content = $message;
  } else {
    $email_content['text'] = strip_tags($message);
    $email_content['html'] = nl2br($message);
  }

  if($vself == 1) {
	  tep_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, EMAIL_SUBJECT, $email_content, $from, STORE_OWNER_EMAIL_ADDRESS);
  } else {
    while ($mail = tep_db_fetch_array($mail_query)) {
	  tep_mail($mail['customers_firstname'] . ' ' . $mail['customers_lastname'], $mail['customers_email_address'], EMAIL_SUBJECT.$subject, $email_content, $from, STORE_OWNER_EMAIL_ADDRESS);
    }
  }

  tep_redirect(tep_href_link(FILENAME_MAIL, 'mail_sent_to=' . urlencode($mail_sent_to)));
}

if ( ($action == 'preview') && !isset($_POST['customers_email_address']) ) {
  $messageStack->add('search', ERROR_NO_CUSTOMER_SELECTED, 'error');
}

if (isset($_GET['mail_sent_to'])) {
  $messageStack->add('search', sprintf(NOTICE_EMAIL_SENT_TO, $_GET['mail_sent_to']), 'success');
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-languages" class="table-languages">
        <div class="row">
          <div class="col-12 dark panel-left rounded-left rounded-right">

            <?php
            if ( ($action == 'preview') && isset($_POST['customers_email_address']) ) {
              switch ($_POST['customers_email_address']) {
                case '***':
                  $mail_sent_to = TEXT_ALL_CUSTOMERS;
                  break;
                case '**D':
                  $mail_sent_to = TEXT_NEWSLETTER_CUSTOMERS;
                  break;
                case '***D':
                  $mail_sent_to = TEXT_NEWSLETTER_SELF;
                  break;
                default:
                  $mail_sent_to = strtolower($_POST['customers_email_address']);
                  break;
              }
             
              echo tep_draw_form('mail', FILENAME_MAIL, 'action=send_email_to_user'); ?>


              <div class="form-group">
                <label class="main-text"><?php echo TEXT_CUSTOMER; ?></label>
                <span class="main-title"><?php echo $mail_sent_to; ?></span>
              </div>

                <tr>
                  <td class="smallText"><b><?php echo TEXT_CUSTOMER; ?></b><br><?php echo $mail_sent_to; ?></td>
                </tr>
                <tr>
                  <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="smallText"><b><?php echo TEXT_FROM; ?></b><br><?php echo htmlspecialchars(stripslashes($_POST['from'])); ?></td>
                </tr>
                <tr>
                  <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="smallText"><b><?php echo TEXT_SUBJECT; ?></b><br><?php echo htmlspecialchars(stripslashes($_POST['email_subject'])); ?></td>
                </tr>
                <tr>
                  <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="smallText"><b><?php echo TEXT_MESSAGE; ?></b><br><?php if (HTML_WYSIWYG_DISABLE_EMAIL == 'Enable') { echo (stripslashes($_POST['message'])); } else { echo htmlspecialchars(stripslashes($_POST['message'])); } ?></td>
                </tr>
                <tr>
                  <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td>
                  <?php
                  /* Re-Post all POST'ed variables */
                    reset($_POST);
					foreach($_POST as $key => $value) {
                      if (!is_array($_POST[$key])) {
                        echo tep_draw_hidden_field($key, htmlspecialchars(stripslashes($value)));
                      }
                    }
                  ?>
                  </td>
                </tr>
                <tr>
                  <td class="smallText">
                  <?php 
                    if (HTML_WYSIWYG_DISABLE_EMAIL == 'Disable') {
                      echo tep_image_submit('button_back.gif', IMAGE_BACK, 'name="back"');
                    } 
                    if (HTML_WYSIWYG_DISABLE_EMAIL == 'Disable') {
                      echo(TEXT_EMAIL_BUTTON_HTML);
                    } else { 
                      echo(TEXT_EMAIL_BUTTON_TEXT);
                    } 
                  ?>
                  </td>
                </tr>
                <tr>
                  <td align="right"><?php echo '<a class="btn btn-default btn-sm mt-2 mb-2 mr-2" href="' . tep_href_link(FILENAME_MAIL) . '">' . IMAGE_CANCEL . '</a> <button class="btn btn-success btn-sm" type="submit">' . IMAGE_SEND_EMAIL . '</button>'; ?></td>
                </tr>
              </table>
              </form>
              <?php
            } else {
              echo tep_draw_form('mail', FILENAME_MAIL, 'action=preview');

                $customers = array();
                $customers[] = array('id' => '', 'text' => TEXT_SELECT_CUSTOMER);
                $customers[] = array('id' => '***D', 'text' => TEXT_NEWSLETTER_SELF);
                $customers[] = array('id' => '***', 'text' => TEXT_ALL_CUSTOMERS);
                $customers[] = array('id' => '**D', 'text' => TEXT_NEWSLETTER_CUSTOMERS);
                $mail_query = tep_db_query("select customers_email_address, customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " order by customers_lastname");
                while($customers_values = tep_db_fetch_array($mail_query)) {
                  $customers[] = array('id' => $customers_values['customers_email_address'],
                                       'text' => $customers_values['customers_lastname'] . ', ' . $customers_values['customers_firstname'] . ' (' . $customers_values['customers_email_address'] . ')');
                }
                ?>
                <div class="ml-2 mr-2 mt-4">
                  <div class="form-inline mt-3">
                    <label for="customers_email_address" class="col-1 col-form-label main-text mr-2"><?php echo TEXT_CUSTOMER; ?></label>
                    <?php echo tep_draw_pull_down_menu('customers_email_address', $customers, (isset($_GET['customer']) ? $_GET['customer'] : ''), 'class="form-control"');?>
                  </div> 

                  <div class="form-inline mt-2">
                    <label for="from" class="col-1 col-form-label main-text mr-2"><?php echo TEXT_FROM; ?></label>
                    <?php echo tep_draw_input_field('from', STORE_OWNER_EMAIL_ADDRESS, 'class="form-control w-50"');?>
                  </div> 

                  <div class="form-inline mt-2">
                    <label for="email_subject" class="col-1 col-form-label main-text mr-2"><?php echo TEXT_SUBJECT; ?></label>
                    <?php echo tep_draw_input_field('email_subject', null, 'class="form-control w-50"');?>
                  </div>                 

                  <div class="form-inline mt-2">
                    <label for="email_subject" class="col-1 col-form-label main-text mr-2"><?php echo TEXT_MESSAGE; ?></label>
                    <?php echo tep_draw_textarea_field('message', 'soft', '60', '15', null, 'class="ckeditor"');?>
                  </div>  

                  <div class="col-10 button-container text-center">
                    <button type="submit" class="btn btn-success btn-sm mt-3 mb-4"><?php echo IMAGE_SEND_EMAIL; ?></button> 
                  </div>

                </div>
              </form>
              <?php
            }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function() {
  setTimeout(function(){ $('#cke_message').addClass('w-75'); }, 500);
});
</script>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>