<?php
/*
  $Id: cds_page_manager.php,v 1.0.0.0 2007/02/27 13:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2007 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
$is_62 = (INSTALLED_VERSION_MAJOR == 6 && INSTALLED_VERSION_MINOR == 2) ? true : false;
require(DIR_WS_FUNCTIONS . 'cds_functions.php');

if ($messageStack->size('header') > 0) {
  echo $messageStack->output('header');
}
$error_text = (isset($_SESSION['error_text'])) ? $_SESSION['error_text'] : '';
// determine cPath
$cPath = '0';
if ( isset($_POST['cPath'] ) && tep_not_null($_POST['cPath'] ) ) {
  $cPath = $_POST['cPath'];
} elseif ( isset($_GET['cPath'] ) && tep_not_null($_GET['cPath'])) {
  $cPath = $_GET['cPath'];
}
if ( tep_not_null($cPath) ) {
  $cPath_array = tep_pages_parse_categories_path($cPath);
  $cPath = implode('_', $cPath_array);
  $current_categories_id = $cPath_array[(sizeof($cPath_array)-1)];
} else {
  $current_categories_id = 0;
}

// clean variables
$cID = '';
if ( isset($_POST['cID'] ) && tep_not_null($_POST['cID'])) {
  $cID = (int)$_POST['cID'];
} elseif ( isset($_GET['cID'] ) && tep_not_null($_GET['cID'])) {
  $cID = (int)$_GET['cID'];
}
$pID = '';
if ( isset($_POST['pID'] ) && tep_not_null($_POST['pID'])) {
  $pID = (int)$_POST['pID'];
} elseif ( isset($_GET['pID'] ) && tep_not_null($_GET['pID'])) {
  $pID = (int)$_GET['pID'];
}
$action = '';
if ( isset($_POST['action'] ) && tep_not_null($_POST['action'])) {
  $action = tep_db_prepare_input($_POST['action']);
} elseif ( isset($_GET['action'] ) && tep_not_null($_GET['action'])) {
  $action = tep_db_prepare_input($_GET['action']);
}

$messageStack;
switch ($action) {

  case 'setflag_category':
    $status = tep_db_prepare_input($_GET['flag']);
    if ($status == '1') {
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . "
                    SET categories_status = '1'
                    WHERE categories_id = '" . (int)$cID . "'");
    } elseif ($status == '0') {
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . "
                    SET categories_status = '0'
                    WHERE categories_id = '" . (int)$cID . "'");
    }
   echo $_GET['flag'];exit;
tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cID));
    break;

  case 'setmenu_category':
    $status = tep_db_prepare_input($_GET['flag']);
    if ($status == '1') {
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . "
                    SET categories_in_menu = '1'
                    WHERE categories_id = '" . (int)$cID . "'");
    } elseif ($status == '0') {
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . "
                    SET categories_in_menu = '0'
                    WHERE categories_id = '" . (int)$cID . "'");
    }
     echo $_GET['flag'];exit;

    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cID));
    break;

  case 'setlisting_category':
    $status = tep_db_prepare_input($_GET['flag']);
    if ($status == '1') {
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . "
                    SET categories_in_pages_listing = '2'
                    WHERE categories_id = '" . (int)$cID . "'");
    } elseif ($status == '0') {
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . "
                    SET categories_in_pages_listing = '0'
                    WHERE categories_id = '" . (int)$cID . "'");
    }
        echo ($_GET['flag'] == 1)? '1':'0';exit;
    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cID));
    break;

  case 'edit_category':
    $categories_query = tep_db_query("SELECT ic.categories_id, ic.categories_status, ic.categories_image, ic.category_heading_title_image, ic.category_header_banner, ic.categories_sort_order, ic.categories_url_override, ic.categories_attach_product, ic.categories_url_override_target, ic.category_append_cdpath, ic.categories_sub_category_view, ic.categories_listing_content_mode, ic.categories_listing_columns, ic.categories_in_menu, ic.categories_in_pages_listing, ic.categories_language_saving_option, ic.categories_template, icd.categories_name, icd.categories_description
                                        from " . TABLE_CDS_CATEGORIES . " ic
                                      LEFT JOIN " . TABLE_CDS_CATEGORIES_DESCRIPTION . " icd
                                        on ic.categories_id = icd.categories_id
                                      WHERE ic.categories_id = '" . (int)$cID . "'
                                        and icd.language_id = '" . (int)$languages_id . "'");
    if ($categories = tep_db_fetch_array($categories_query)) {
      $cInfo = new objectInfo($categories);
    }
    break;

  case 'insert_category':
  case 'update_category':
    //echo '<pre>';print_r($_POST);exit;
    $categories_sort_order = tep_db_prepare_input($_POST['categories_sort_order']);
    $category_append_cdpath = tep_db_prepare_input($_POST['category_append_cdpath']);
    $categories_url_override = tep_db_prepare_input($_POST['categories_url_override']);
    $categories_url_override_target = tep_db_prepare_input($_POST['categories_url_override_target']);
    //$categories_attach_product = tep_db_prepare_input($_POST['categories_attach_product']);
    $categories_attach_product = tep_db_prepare_input($_POST['products_id']);
    //echo $categories_attach_product;exit;
    $categories_status = isset($_POST['categories_status']) ? tep_db_prepare_input($_POST['categories_status']) : 'off';
    if ($categories_status == 'on') $categories_status = 1;
    if ($categories_status == 'off') $categories_status = 0;

    $categories_sub_category_view =((tep_db_prepare_input($_POST['categories_sub_category_view']) == 'on') ? '1' : '0');
    $categories_listing_content_mode =((tep_db_prepare_input($_POST['categories_listing_content_mode']) == 'on') ? '1' : '0');
    $categories_listing_columns = tep_db_prepare_input($_POST['categories_listing_columns']);
    $categories_language_saving_option =((tep_db_prepare_input($_POST['categories_language_saving_option']) == 'on') ? '1' : '0');
    $categories_in_menu = tep_db_prepare_input($_POST['category_show_link']);
    $categories_in_pages_listing = tep_db_prepare_input($_POST['category_show_link1']);
    $categories_template = tep_db_prepare_input($_POST['categories_template']);
    $sql_data_array = array('categories_sort_order' => $categories_sort_order,
                            'categories_url_override' => $categories_url_override,
                            'categories_url_override_target' =>$categories_url_override_target,
                            'category_append_cdpath'=>$category_append_cdpath,
                            'categories_attach_product' => $categories_attach_product,
                            'categories_status' => $categories_status,
                            'categories_sub_category_view' => $categories_sub_category_view,
                            'categories_listing_content_mode' =>$categories_listing_content_mode,
                            'categories_listing_columns' => $categories_listing_columns ,
                            'categories_language_saving_option' =>$categories_language_saving_option,
                            'categories_in_menu'=>$categories_in_menu,
                            'categories_template' => $categories_template,
                            'categories_in_pages_listing'=>$categories_in_pages_listing);

    if ($action == 'insert_category') {
      $insert_sql_data = array('categories_parent_id' => $current_categories_id,
                               'categories_date_added' => 'now()');
      $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
      tep_db_perform(TABLE_CDS_CATEGORIES, $sql_data_array);
      $cID = tep_db_insert_id();
    } elseif ($action == 'update_category') {
      //print_r($_POST);exit;
      $update_sql_data = array('categories_last_modified' => 'now()');
      $sql_data_array = array_merge($sql_data_array, $update_sql_data);
      tep_db_perform(TABLE_CDS_CATEGORIES, $sql_data_array, 'update', "categories_id = '" . (int)$cID . "'");
    }
    $languages = tep_get_languages();
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $categories_name_array = $_POST['categories_name'];
      $categories_heading_array = $_POST['categories_heading'];
      $categories_description_array = $_POST['categories_description'];
      $categories_blurb_array = $_POST['categories_blurb'];
      $categories_meta_keywords_array = $_POST['categories_meta_keywords'];
      $categories_meta_title_array = $_POST['categories_meta_title'];
      $categories_meta_keywords_array = $_POST['categories_meta_keywords'];
      $categories_meta_description_array = $_POST['categories_meta_description'];
      $language_id = $languages[$i]['id'];
      $language_code = $languages[$i]['code'];
            $lang_default = (defined('DEFAULT_LANGUAGE') && DEFAULT_LANGUAGE != '') ? DEFAULT_LANGUAGE : 'en';
            if (($categories_name_array[$language_id] == '') && ($language_code == $lang_default)) {
                $_SESSION['error_text'] = TEXT_CDS_ERROR_MENU_NAME;
        tep_redirect(FILENAME_PAGES . '?cPath=' . $cPath . '&cID=' . $cID . '&action=edit_category');
          }
      $sql_data_array = array('categories_name' => tep_db_prepare_input($categories_name_array[$language_id]),
                              'categories_heading' => tep_db_prepare_input($categories_heading_array[$language_id]),
                              'categories_description' => tep_db_prepare_input($categories_description_array[$language_id]),
                              'categories_blurb' => tep_db_prepare_input($categories_blurb_array[$language_id]),
                              'categories_meta_keywords'=>tep_db_prepare_input($categories_meta_keywords_array[$language_id]),
                              'categories_meta_title' => tep_db_prepare_input($categories_meta_title_array[$language_id]),
                              'categories_meta_keywords' => tep_db_prepare_input($categories_meta_keywords_array[$language_id]),
                              'categories_meta_description' => tep_db_prepare_input($categories_meta_description_array[$language_id]) );

      if ($action == 'insert_category') {
        $insert_sql_data = array('categories_id' => $cID,
                                 'language_id' => $languages[$i]['id']);
        $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
        tep_db_perform(TABLE_CDS_CATEGORIES_DESCRIPTION, $sql_data_array);

			//checking existing same permalink name
			$permalink_name = tep_db_prepare_input(tep_db_encoder($_POST['permalink_name'][$language_id]));
			$checking_existing_permalink = check_permalink($permalink_name, $language_id);
			if($checking_existing_permalink > 0)
			 $permalink_name = tep_db_prepare_input(tep_db_encoder($_POST['permalink_name'][$language_id])).'-'.rand(1,20);
			 $permalink_name = check_dup_permalink('pages_id', $pID, $permalink_name, 1);
			 if($_POST['permalink_name'][$language_id] == ''){
				 $permalink_name =  sanitize($categories_name_array[$language_id]);
				 $permalink_name = check_dup_permalink('pages_categories_id', $cID, $permalink_name, 1);
				 if (is_numeric($permalink_name)){
					$permalink_name = 'pr-'.$permalink_name;
				 }
			 }
			 if($permalink_name != ''){
				 $insert_permalink_data = array('pages_categories_id' => $cID,
									 'language_id' => $language_id,
									 'permalink_type' => 'page_category',
									 'permalink_name' => $permalink_name);
				 tep_db_perform(TABLE_PERMALINK, $insert_permalink_data);
			 }

      } elseif ($action == 'update_category') {
        tep_db_perform(TABLE_CDS_CATEGORIES_DESCRIPTION, $sql_data_array, 'update', "categories_id = '" . (int)$cID . "' and language_id = '" . (int)$languages[$i]['id'] . "'");

			 //checking permalink table
			$default_page_category_name = tep_db_prepare_input(tep_db_encoder($categories_name_array[$language_id]));
			$perma_query = tep_db_query("SELECT * FROM ". TABLE_PERMALINK ." WHERE  pages_categories_id = '$cID' and language_id = '$language_id'");
			if(tep_db_num_rows($perma_query) > 0) {
				 $permalink_name = tep_db_prepare_input(tep_db_encoder($_POST['permalink_name'][$language_id]));
				 $permalink_name = sanitize($permalink_name);
				 if($permalink_name == ''){
					$permalink_name = sanitize($default_page_category_name).'-'.$cID;
				 }
				 $update_permalink_data = array('permalink_type' => 'page_category','permalink_name' => $permalink_name);
				 tep_db_perform(TABLE_PERMALINK, $update_permalink_data, 'update', 'pages_categories_id = ' . (int)$cID . ' and language_id = ' . (int)$language_id);
			}else{
				 if($_POST['permalink_name'][$language_id] == ''){
					 $slug = sanitize($default_page_category_name).'-'.$cID;
				 }else{
					$slug = sanitize($_POST['permalink_name'][$language_id]);
				 }
					 $insert_permalink_data = array('pages_categories_id' => $cID,
										 'language_id' => $language_id,
										 'permalink_type' => 'page_category',
										 'permalink_name' => $slug);
					tep_db_perform(TABLE_PERMALINK, $insert_permalink_data);
			}



      }
    }

    //Category Images Upload
    if (isset($_FILES['categories_image']) && tep_not_null($_FILES['categories_image']['name'])) {
      $categories_image = new upload('categories_image', DIR_FS_CATALOG_IMAGES);
      $sql=("UPDATE " . TABLE_CDS_CATEGORIES . " SET categories_image = '" . tep_db_input($categories_image->filename) . "' WHERE categories_id = '" . (int)$cID . "'");
      tep_db_query($sql);
    }
    if (isset($_FILES['category_heading_title_image']) && tep_not_null($_FILES['category_heading_title_image']['name'])) {
      $category_heading_title_image = new upload('category_heading_title_image', DIR_FS_CATALOG_IMAGES);
      $sql = ("UPDATE  " . TABLE_CDS_CATEGORIES . " SET category_heading_title_image = '" . tep_db_input($category_heading_title_image->filename) . "' WHERE categories_id = '" . (int)$cID . "'");
    tep_db_query($sql);
    }
    if (isset($_FILES['category_header_banner']) && tep_not_null($_FILES['category_header_banner']['name'])) {
      $category_header_banner = new upload('category_header_banner',DIR_FS_CATALOG_IMAGES );
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . " SET category_header_banner = '" . tep_db_input($category_header_banner->filename) . "' WHERE categories_id = '" . (int)$cID . "'");
    }
    // Category Images Remove and Delete part.
    if ($_POST['unlink_image_med'] == 'yes') {
      $category_heading_title_image = '';
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . " SET category_heading_title_image = '" .($category_heading_title_image) . "' WHERE categories_id = '" . (int)$cID . "'");
    }
    if ($_POST['delete_image_med'] == 'yes') {
      unlink(DIR_FS_CATALOG_IMAGES . $_POST['catagory_heading_previous_image']);
      $category_heading_title_image = '';
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . " SET category_heading_title_image = '" . ( $category_heading_title_image ) . "' WHERE categories_id = '" . (int)$cID . "'");
    }
    if ($_POST['unlink_image'] == 'yes') {
      $categories_image = '';
      $sql=("UPDATE " . TABLE_CDS_CATEGORIES . " SET categories_image = '" . tep_db_input($categories_image) . "' WHERE categories_id = '" . (int)$cID . "'");
      tep_db_query($sql);
    }
    if ( $_POST['delete_image'] == 'yes' ) {
      unlink(DIR_FS_CATALOG_IMAGES . $_POST['catagory_previous_image']);
      $categories_image = '';
      $sql=("UPDATE " . TABLE_CDS_CATEGORIES . " SET categories_image = '" . tep_db_input($categories_image) . "' WHERE categories_id = '" . (int)$cID . "'");
        tep_db_query($sql);
    }
    if ($_POST['unlink_image_lrg'] == 'yes') {
      $category_header_banner = '';
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . " SET category_header_banner = '" . tep_db_input($category_header_banner) . "' WHERE categories_id = '" . (int)$cID . "'");
    }
    if ($_POST['delete_image_lrg'] == 'yes') {
      unlink(DIR_FS_CATALOG_IMAGES . $_POST['catagory_banner_previous_image']);
      $category_header_banner = '';
      tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . " SET category_header_banner = '" . tep_db_input($category_header_banner) . "' WHERE categories_id = '" . (int)$cID . "'");
    }

	// RCI for action insert
	echo $cre_RCI->get('pages', 'categoryaction', false);


    if($action == 'new_category'){
		$messageStack->add_session('pages', 'Pages Category Added Successfully!!', 'success');
	}else{
		$messageStack->add_session('pages', 'Pages Category Updated Successfully!!', 'success');
	}

	  $mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
	  if ($mode == 'save') {
	    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cID));
	  } else {  // save & stay
	    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cID . '&action=edit_category'));
	  }

    break;

  case 'deleteconfirm_category':
    if (tep_not_null($cID)) {
      $categories = tep_pages_get_categories_tree($cID, '', '0', '', true);
      $pages = array();
      $pages_delete = array();
      for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
        $pages_ids_query = tep_db_query("SELECT pages_id
                                           from " . TABLE_CDS_PAGES_TO_CATEGORIES . "
                                         WHERE categories_id = '" . (int)$categories[$i]['id'] . "'");
        while ($pages_ids = tep_db_fetch_array($pages_ids_query)) {
          $pages[$pages_ids['pages_id']]['categories'][] = $categories[$i]['id'];
        }
      }
      reset($pages);
	  foreach($pages as $key => $value) {
        $categories_ids = '';
        for ($i=0, $n=sizeof($value['categories']); $i<$n; $i++) {
          $categories_ids .= "'" . (int)$value['categories'][$i] . "', ";
        }
        $categories_ids = substr($categories_ids, 0, -2);
        $check_query = tep_db_query("SELECT count(*) as total
                                                from " . TABLE_CDS_PAGES_TO_CATEGORIES . "
                                             WHERE pages_id = '" . (int)$key . "'
                                               and categories_id not in (" . $categories_ids . ")");
        $check = tep_db_fetch_array($check_query);
        if ($check['total'] < '1') {
          $pages_delete[$key] = $key;
        }
      }
      // make sure script doesn't time out
      tep_set_time_limit(0);
      for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
        tep_pages_remove_category($categories[$i]['id']);
      }
      reset($pages_delete);
	  foreach($pages_delete as $key => $value) {
        tep_pages_remove_page($key);
      }
    }
    $messageStack->add_session('Category Page Deleted Successfully', 'success');
    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath));
    break;

  case 'moveconfirm_category':
    if ( isset($_POST['cID']) && ($_POST['cID'] != $_POST['move_to_category_id']) ) {
      $categories_id = tep_db_prepare_input($_POST['cID']);
      $new_parent_id = tep_db_prepare_input($_POST['move_to_category_id']);
      $path = explode('_', tep_get_generated_pages_category_path_ids($new_parent_id));
      if (in_array($categories_id, $path)) {
        $messageStack->add_session(ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT, 'error');
        tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $categories_id));
      } else {
        tep_db_query("UPDATE " . TABLE_CDS_CATEGORIES . "
                              SET categories_parent_id = '" . (int)$new_parent_id . "', categories_last_modified = now()
                              WHERE categories_id = '" . (int)$categories_id . "'");
        tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $new_parent_id . '&cID=' . $categories_id));
      }
    }
    break;

  case 'moveconfirm_page':
    $pages_id = tep_db_prepare_input($_POST['pID']);
    $categories_id = tep_db_prepare_input($_POST['ID']);
    $new_parent_id = tep_db_prepare_input($_POST['move_to_category_id']);
    tep_db_query("UPDATE " . TABLE_CDS_PAGES_TO_CATEGORIES . "
                          SET categories_id = '" . (int)$new_parent_id . "'
                          WHERE pages_id = '" . (int)$pID . "'");

    $menuname_query = tep_db_query("SELECT pages_menu_name
                                                           from " . TABLE_CDS_PAGES_DESCRIPTION . "
                                                        WHERE pages_id = '" . (int)$pages_id . "'");
    $menu_name = tep_db_fetch_array($menuname_query);
    $menu_name = $menu_name['pages_menu_name'];
    $pages_menu_name_array = array($menu_name);
    $separated = implode('_',explode(' ',$pages_menu_name_array[0]));

    $sql_category_name = tep_db_query("SELECT categories_name
                                                            from " . TABLE_CDS_CATEGORIES_DESCRIPTION . "
                                                          WHERE categories_id = " . (int)$categories_id . "
                                                            and language_id= " . $languages_id);

    $category_name=tep_db_fetch_array($sql_category_name);
    $cat_name = $category_name['categories_name'];
    $separated_cat_name = strtolower($cat_name);

    $sql=( "SELECT *
                 from  " . TABLE_LANGUAGES . "");
    $sql_res = tep_db_query($sql);
    while ($result= tep_db_fetch_array($sql_res)) {
      $lang_directory = $result['directory'];
      $string_lang = strtolower( $lang_directory );
      $menu_name = strtolower($menu_name);
      $pages_menu_name_array = array($menu_name);
      $separated = strtolower(implode('_', explode(' ', $pages_menu_name_array[0])));

      $sql_file =("UPDATE " . TABLE_CDS_PAGES_DESCRIPTION. "
                      SET pages_file = ('" .$separated_cat_name . '_' . $separated . '_' . $dup_pages_id . '.php'. "')
                      WHERE pages_id='".$dup_pages_id."'");
      tep_db_query($sql_file);
    }
    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $new_parent_id . '&pID=' . $pID));
    break;

  case 'copyconfirm_page':
    if ( isset($_POST['pID']) ) {
      $pages_id = tep_db_prepare_input($_POST['pID']);
      $categories_id = tep_db_prepare_input($_POST['ID']);
      if ( $_POST['copy_as']  ==  'link')  {
        if ( $categories_id != $current_category_id ) {
          $check_query = tep_db_query("SELECT count(*) as total
                                         from " . TABLE_CDS_PAGES_TO_CATEGORIES . "
                                       WHERE pages_id = '" . (int)$pages_id . "'
                                         and categories_id = '" . (int)$categories_id . "'");
          $check = tep_db_fetch_array($check_query);
          if ( $check['total'] < '1' ) {
            $next_sort_value = cre_get_next_sort_value();
            tep_db_query("INSERT into " . TABLE_CDS_PAGES_TO_CATEGORIES . " (pages_id, categories_id, page_sort_order) VALUES ('" . (int)$pages_id . "', '" . (int)$categories_id . "', '" . (int)$next_sort_value . "')");
          }
        } else {
          $messageStack->add_session(ERROR_CANNOT_LINK_TO_SAME_CATEGORY, 'error');
        }
      } elseif ($_POST['copy_as'] == 'duplicate') {
        $page_query = tep_db_query("SELECT pages_image, pages_date_added, pages_date_modified, pages_author, pages_status, pages_sort_order, pages_in_menu, pages_in_page_listing, pages_url, pages_append_cdpath, pages_url_target, pages_attach_product, pages_group_access
                                      from " . TABLE_CDS_PAGES . "
                                    WHERE pages_id = '" . (int)$pages_id . "'");;
        $page = tep_db_fetch_array($page_query);
        tep_db_query("INSERT into " . TABLE_CDS_PAGES . " (pages_image, pages_date_added, pages_date_modified, pages_author, pages_status, pages_sort_order, pages_in_menu, pages_in_page_listing, pages_url, pages_append_cdpath, pages_url_target, pages_attach_product, pages_group_access)
                      VALUES ('" . $page['pages_image'] . "', '" . date("Y-m-d") . "', '" . date("Y-m-d") . "', '" . $page['pages_author'] . "', '0', '" . (int)$page['pages_sort_order'] . "', '" . (int)$page['pages_in_menu'] . "','" . (int)$page['pages_in_page_listing'] . "', '" . $page['pages_url'] . "', '" . $page['pages_append_cd_path'] . "', '" . $page['pages_url_target'] . "', '" . (int)$page['pages_attach_product'] . "', '" . $page['pages_group_access'] . "')");
        $dup_pages_id = tep_db_insert_id();

        $description_query = tep_db_query ("SELECT pages_id, language_id, pages_title, pages_meta_title, pages_meta_keywords, pages_meta_description, pages_blurb, pages_body, pages_menu_name, pages_file
                                             from  " . TABLE_CDS_PAGES_DESCRIPTION . "
                                           WHERE pages_id = '" . (int)$pages_id . "'");

        while ($description = tep_db_fetch_array($description_query)) {
          tep_db_query("INSERT into " . TABLE_CDS_PAGES_DESCRIPTION . " (pages_id, language_id, pages_title, pages_meta_title, pages_meta_keywords, pages_meta_description, pages_blurb, pages_body, pages_menu_name)
                        VALUES ('" . (int)$dup_pages_id . "', '" . (int)$description['language_id'] . "', '" . tep_db_input($description['pages_title']) . "','" . tep_db_input($description['pages_meta_title']) . "', '" . tep_db_input($description['pages_meta_keywords']) . "', '" . tep_db_input($description['pages_meta_description']) . "', '" . tep_db_input($description['pages_blurb']) . "', '" . tep_db_input($description['pages_body']) . "', '" . tep_db_input($description['pages_menu_name']) . "')");
        }

        $next_sort_value = (isset($cPath)) ? cre_get_next_sort_value() : 10;
        tep_db_query("INSERT into " . TABLE_CDS_PAGES_TO_CATEGORIES . " (pages_id, categories_id, page_sort_order)
                      VALUES ('" . (int)$dup_pages_id . "', '" . (int)$categories_id . "', '" . (int)$next_sort_value . "')");

        $menuname_query = tep_db_query("SELECT pages_menu_name
                                          from " . TABLE_CDS_PAGES_DESCRIPTION . "
                                        WHERE pages_id = '" . (int)$pages_id . "'");
        $menu_name = tep_db_fetch_array($menuname_query);
        $menu_name = $menu_name['pages_menu_name'];
        $pages_menu_name_array = array($menu_name);
        $separated = implode('_',explode(' ',$pages_menu_name_array[0]));

        $sql_category_name = tep_db_query("SELECT categories_name
                                             from " . TABLE_CDS_CATEGORIES_DESCRIPTION . "
                                           WHERE categories_id = " . (int)$categories_id . "
                                             and language_id= " . $languages_id);
        $category_name=tep_db_fetch_array($sql_category_name);
        $cat_name = $category_name['categories_name'];
        $separated_cat_name = strtolower($cat_name);

        $sql=( "SELECT *
                  from  " . TABLE_LANGUAGES . "");
        $sql_res = tep_db_query($sql);
        while ($result= tep_db_fetch_array($sql_res)) {
          $lang_directory = $result['directory'];
          $string_lang = strtolower( $lang_directory );
          $menu_name = strtolower($menu_name);
          $pages_menu_name_array = array($menu_name);
          $separated = strtolower(implode('_', explode(' ', $pages_menu_name_array[0])));

          $sql_file =("UPDATE " . TABLE_CDS_PAGES_DESCRIPTION. "
                          SET pages_file = ('" .$separated_cat_name . '_' . $separated . '_' . $dup_pages_id . '.php'. "')
                          WHERE pages_id='".$dup_pages_id."'");
          tep_db_query($sql_file);
        }
      }
    }
    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $categories_id . '&pID=' . $pages_id));
    break;

  case 'setflag_page':
    $flag = (int)$_GET['flag'];
    if ( ($flag == '0') || ($flag == '1') ) {
      if ( tep_not_null($pID) ) {
        tep_db_query("UPDATE " . TABLE_CDS_PAGES . "
                      SET pages_status = '" . $flag . "'
                      WHERE pages_id = '" . (int)$pID . "'");
      }
    }
    echo $_GET['flag'];exit;
    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pID));
    break;

  case 'setmenu_page':
    $flag = (int)$_GET['flag'];
    if ( ($flag == '0') || ($flag == '1') ) {
      if ( tep_not_null($pID) ) {
        tep_db_query("UPDATE " . TABLE_CDS_PAGES . "
                      SET pages_in_menu = '" . $flag . "'
                      WHERE pages_id = '" . (int)$pID . "'");
      }
    }
    echo $_GET['flag'];exit;
    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pID));
    break;

  case 'setlisting_page':
    $flag = (int)$_GET['flag'];
    if ( ($flag == '0') || ($flag == '2') ) {
      if ( tep_not_null($pID) ) {
        tep_db_query("UPDATE " . TABLE_CDS_PAGES . "
                      SET pages_in_page_listing = '" . $flag . "'
                      WHERE pages_id = '" . (int)$pID . "'");
      }
    }
    echo ($_GET['flag'] == 2)? '1':'0';exit;
    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pID));
    break;

  case 'edit_page':
    $pages_query = tep_db_query("SELECT ip.pages_id, ip.pages_image,ip.pages_header_banner, ip.pages_date_added, ip.pages_date_modified, ip.pages_status, ip.pages_in_menu, ip.pages_in_page_listing, ip.pages_attach_product, ip2c.page_sort_order, ipd.pages_title, ipd.pages_blurb ,ipd.pages_file
                                   from " . TABLE_PAGES_TO_CATEGORIES . " ip2c,
                                          " . TABLE_CDS_PAGES . " ip
                                 LEFT JOIN " . TABLE_CDS_PAGES_DESCRIPTION . " ipd
                                   on ip.pages_id = ipd.pages_id
                                 WHERE ipd.language_id = '" . (int)$languages_id . "'
                                   and ip2c.pages_id = '" . (int)$pID . "'
                                   and ip.pages_id = '" . (int)$pID . "'");
    if ($pages = tep_db_fetch_array($pages_query)) {
      $pInfo = new objectInfo($pages);
    }
   break;

  case 'insert_page':
  case 'update_page':
    //print_r($_POST);
    $languages = tep_get_languages();
    $pages_menu_name_array = tep_db_prepare_input($_POST['pages_menu_name']);
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $language_id = (int)$languages[$i]['id'];
      $language_code = $languages[$i]['code'];
      $lang_default = (defined('DEFAULT_LANGUAGE') && DEFAULT_LANGUAGE != '') ? DEFAULT_LANGUAGE : 'en';
      if (($pages_menu_name_array[$language_id] == '') && ($language_code == $lang_default)) {
        $_SESSION['error_text'] = TEXT_CDS_ERROR_MENU_NAME;
        tep_redirect(tep_href_link(FILENAME_PAGES, 'cPath=' . $cPath . '&action=new_page'));
      }
    }
    $pages_title_array = tep_db_prepare_input($_POST['pages_title']);

    $pages_category = isset($_POST['pages_category']) ? (int)$_POST['pages_category'] : 0;
    $pages_blurb_array = tep_db_prepare_input($_POST['pages_blurb']);
    $pages_body_array = tep_db_prepare_input($_POST['pages_body']);
    $pages_meta_title_array = tep_db_prepare_input($_POST['pages_meta_title']);
    $pages_meta_keywords_array = tep_db_prepare_input($_POST['pages_meta_keywords']);
    $pages_meta_description_array = tep_db_prepare_input($_POST['pages_meta_description']);

    $pages_status = isset($_POST['pages_status']) ? tep_db_prepare_input($_POST['pages_status']) : 'off';
    if ($pages_status == 'on') $pages_status = 1;
    if ($pages_status == 'off') $pages_status = 0;

    $pages_sort_order = tep_not_null($_POST['pages_sort_order']) ? (int)$_POST['pages_sort_order'] : 0 ;
    $pages_in_menu = isset($_POST['pages_in_menu']) ? (int)tep_db_prepare_input($_POST['pages_in_menu']) : 0;
    $pages_show_link_in_listing = tep_db_prepare_input($_POST['pages_show_link_in_listing']);
    $pages_attach_product = (int)tep_db_prepare_input($_POST['pages_attach_product']);
    $sql_data_array = array('pages_status' => $pages_status,
                            'pages_sort_order' => $pages_sort_order,
                            'pages_in_menu' => (int)$pages_in_menu,
                            'pages_in_page_listing' => $pages_show_link_in_listing,
                            'pages_attach_product' => $pages_attach_product
                           );
    if ( $action == 'update_page' ) {
      $sql_data_array['pages_date_modified'] = 'now()';
      tep_db_perform(TABLE_CDS_PAGES, $sql_data_array, 'update', "pages_id = '" . (int)$pID . "'");
    } else {
      $sql_data_merge = array('pages_date_added' => 'now()' );
            $sql_data_array = array_merge($sql_data_array, $sql_data_merge);
      tep_db_perform(TABLE_CDS_PAGES, $sql_data_array);
      $pID = tep_db_insert_id();
    }

    // upload image
    if (isset($_POST['unlink_image']) && $_POST['unlink_image'] == 'yes' ) {
      $pages_image = '';
     $sql=("UPDATE " . TABLE_CDS_PAGES . " SET pages_image = '" . $pages_image . "' WHERE pages_id = '" . (int)$pID . "'");
      tep_db_query($sql);
    }
    if (isset($_POST['delete_image']) && $_POST['delete_image'] == 'yes') {
      @unlink(DIR_FS_CATALOG_IMAGES . $_POST['pages_previous_image']);
      $pages_image = '';
     $sql=("UPDATE " . TABLE_CDS_PAGES . " SET pages_image = '" . $pages_image . "' WHERE pages_id = '" . (int)$pID . "'");
      tep_db_query($sql);
    }
    if (isset($_FILES['pages_image']) && tep_not_null($_FILES['pages_image']['name'])) {
      $pages_image = new upload('pages_image', DIR_FS_CATALOG_IMAGES);
      if($pages_image->filename) {
        tep_db_query("UPDATE " . TABLE_CDS_PAGES . "
                      SET pages_image = '" . $pages_image->filename . "'
                      WHERE pages_id = '" . (int)$pID . "'");
      }
    } elseif (isset($_POST['pages_image']) && tep_not_null($_POST['pages_image'])) {
      tep_db_query("UPDATE " . TABLE_CDS_PAGES . "
                      SET pages_image = '" . tep_db_prepare_input($_POST['pages_image']) . "'
                      WHERE pages_id = '" . (int)$pID . "'");
    }
    //Banner Image upload
    if (isset($_POST['unlink_banner_image']) && $_POST['unlink_banner_image'] == 'yes' ) {
      $pages_header_banner = '';
     $sql=("UPDATE " . TABLE_CDS_PAGES . " SET pages_header_banner = '" . $pages_header_banner . "' WHERE pages_id = '" . (int)$pID . "'");
      tep_db_query($sql);
    }
    if (isset($_POST['delete_banner_image']) && $_POST['delete_banner_image'] == 'yes') {
      @unlink(DIR_FS_CATALOG_IMAGES . $_POST['pages_previous_image']);
      $pages_header_banner = '';
     $sql=("UPDATE " . TABLE_CDS_PAGES . " SET pages_header_banner = '" . $pages_header_banner . "' WHERE pages_id = '" . (int)$pID . "'");
      tep_db_query($sql);
    }


    if (isset($_FILES['pages_header_banner']) && tep_not_null($_FILES['pages_header_banner']['name'])) {
      $pages_banner_image = new upload('pages_header_banner', DIR_FS_CATALOG_IMAGES);
      if($pages_banner_image->filename) {
        tep_db_query("UPDATE " . TABLE_CDS_PAGES . "
                      SET pages_header_banner = '" . $pages_banner_image->filename . "'
                      WHERE pages_id = '" . (int)$pID . "'");
      }
    } elseif (isset($_POST['pages_header_banner_previous']) && tep_not_null($_POST['pages_header_banner_previous'])) {
      tep_db_query("UPDATE " . TABLE_CDS_PAGES . "
                      SET pages_header_banner = '" . tep_db_prepare_input($_POST['pages_header_banner_previous']) . "'
                      WHERE pages_id = '" . (int)$pID . "'");
    }

    // update description tables
    $sql_data_array = array();
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $language_id = (int)$languages[$i]['id'];
      $language_code = $languages[$i]['code'];
      $lang_default = (defined('DEFAULT_LANGUAGE') && DEFAULT_LANGUAGE != '') ? DEFAULT_LANGUAGE : 'en';
      $sql_data_array = array('pages_title' => $pages_title_array[$language_id],
                              'pages_menu_name' => $pages_menu_name_array[$language_id],
                              'pages_blurb' => $pages_blurb_array[$language_id],
                              'pages_body' => $pages_body_array[$language_id],
                              'pages_meta_title' => $pages_meta_title_array[$language_id],
                              'pages_meta_keywords' => $pages_meta_keywords_array[$language_id],
                              'pages_meta_description' => $pages_meta_description_array[$language_id]);
      if ( $action == 'insert_page' ) {
        $sql_data_array['pages_id'] = (int)$pID;
        $sql_data_array['language_id'] = (int)$language_id;
        tep_db_perform( TABLE_CDS_PAGES_DESCRIPTION, $sql_data_array );

			//checking existing same permalink name
			$permalink_name = tep_db_prepare_input(tep_db_encoder($_POST['permalink_name'][$language_id]));
			$checking_existing_permalink = check_permalink($permalink_name, $language_id);
			if($checking_existing_permalink > 0)
			 $permalink_name = tep_db_prepare_input(tep_db_encoder($_POST['permalink_name'][$language_id])).'-'.rand(1,20);
			 $permalink_name = check_dup_permalink('pages_id', $pID, $permalink_name, 1);
			 if($_POST['permalink_name'][$language_id] == ''){
				 $permalink_name =  sanitize($pages_menu_name_array[$language_id]);
				 $permalink_name = check_dup_permalink('pages_id', $pID, $permalink_name, 1);
				 if (is_numeric($permalink_name)){
					$permalink_name = 'pr-'.$permalink_name;
				 }
			 }
			 if($permalink_name != ''){
				 $insert_permalink_data = array('pages_id' => $pID,
									 'language_id' => $language_id,
									 'route' => 'core/pages',
									 'permalink_type' => 'page',
									 'permalink_name' => $permalink_name);
				 tep_db_perform(TABLE_PERMALINK, $insert_permalink_data);
			 }
      } else {
        tep_db_perform( TABLE_CDS_PAGES_DESCRIPTION, $sql_data_array, 'update', "pages_id = '" . (int)$pID . "' and language_id = '" . (int)$language_id . "'");
            //checking permalink table
            $default_page_name = tep_db_prepare_input(tep_db_encoder($pages_menu_name_array[$language_id]));
			$perma_query = tep_db_query("SELECT * FROM ". TABLE_PERMALINK ." WHERE  pages_id = '$pID' and language_id = '$language_id'");
			if(tep_db_num_rows($perma_query) > 0) {
				 $permalink_name = tep_db_prepare_input(tep_db_encoder($_POST['permalink_name'][$language_id]));
				 $permalink_name = sanitize($permalink_name);
				 if($permalink_name == ''){
				 	$permalink_name = sanitize($default_product_name).'-'.$pID;
				 }
				 $update_permalink_data = array('permalink_type' => 'page','permalink_name' => $permalink_name);
				 tep_db_perform(TABLE_PERMALINK, $update_permalink_data, 'update', 'pages_id = ' . (int)$pID . ' and language_id = ' . (int)$language_id);
			}else{
				 if($_POST['permalink_name'][$language_id] == ''){
					 $slug = sanitize($default_page_name).'-'.$pID;
				 }else{
					$slug = sanitize($_POST['permalink_name'][$language_id]);
			     }

					 $insert_permalink_data = array('pages_id' => $pID,
										 'language_id' => $language_id,
										 'route' => 'core/pages',
										 'permalink_type' => 'page',
										 'permalink_name' => $slug);
					tep_db_perform(TABLE_PERMALINK, $insert_permalink_data);


			}
      }
    }

    // update category info
    if ( $action == 'update_page') {
      $sql_update_page = ("UPDATE " . TABLE_CDS_PAGES_TO_CATEGORIES . "
                           SET categories_id = '" . (int)$current_category_id . "', page_sort_order = '" . (int)$pages_sort_order . "'
                           WHERE pages_id = '" . (int)$pID . "'");
      tep_db_query($sql_update_page);
    } else if( $action == 'insert_page' ) {
      $sql_page_list =("INSERT into " . TABLE_CDS_PAGES_TO_CATEGORIES . " (pages_id, categories_id, page_sort_order)
                        VALUES ('" . (int)$pID . "', '" . (int)$current_category_id . "', '" . (int)$pages_sort_order . "')");
      tep_db_query($sql_page_list);
    }

    //Creating New ACF file If not exist on Update Page.
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $pagename = tep_pages_get_auxillary_file( $pID, $languages[$i]['id'] );
      $sql=("SELECT *
               from  " .TABLE_LANGUAGES. "");
      $sql_res =tep_db_query($sql);
      while ( $result=tep_db_fetch_array($sql_res) ) {
        $lang_directory = $result['directory'];
        $ourFilename = DIR_FS_DOCUMENT_ROOT . DIR_WS_LANGUAGES . $lang_directory . '/pages/' . $pagename;
        if (!file_exists($ourFilename) ) {
          $sql_file =("UPDATE " . TABLE_CDS_PAGES_DESCRIPTION . "
                        SET pages_file = ('" . $pagename . "')
                        WHERE pages_id='".$pID."'");
          tep_db_query($sql_file);
        }
      }
    }

    if ( $action == 'insert_page' ) {
      $cPath = isset($_POST['cPath']) ? $_POST['cPath'] : '';
      if ($cPath) {
        $findme   = '_';
        $pos = strpos($cPath, $findme);
        if (!$pos) {
          $sql_category_name = tep_db_query("SELECT categories_name
                                               from " . TABLE_CDS_CATEGORIES_DESCRIPTION . "
                                             WHERE categories_id = ". $cPath . " and language_id= " . $languages_id);

          $category_name=tep_db_fetch_array($sql_category_name);
          $cat_name = $category_name['categories_name'];
        } else {
          $var = explode('_', $cPath);
          $var1 = array();
          for($i=0; $i < sizeof($var); $i++) {
            $sql_category_name = tep_db_query("SELECT categories_name
                                                 from " . TABLE_CDS_CATEGORIES_DESCRIPTION . "
                                               WHERE categories_id = " . $var[$i] . " and language_id= " . $languages_id);

            $category_name=tep_db_fetch_array($sql_category_name);
            $cat_name = $category_name['categories_name'];
            $var1[]=$cat_name;
          }
        }
      }
      if (isset($var1)) {
        $separated = implode('_',  $var1 );
      } else {
        $separated = isset($cat_name) ? $cat_name:'';
      }
      $separated_cat_name = cre_clean_filename(strtolower(implode('_', explode( ' ', $separated))));
      $pages_menu_name_array = $_POST['pages_menu_name'];
      $separated = implode('_', explode(' ', $pages_menu_name_array[1]));
      $sql=("SELECT * from  " . TABLE_LANGUAGES . "");

      $sql_res = tep_db_query($sql);
      while ( $result=tep_db_fetch_array($sql_res) ) {
        $lang_directory = $result['directory'];
        $string_lang = strtolower($lang_directory);
        $pages_menu_name_array = $_POST['pages_menu_name'];
        $final_separated  = implode('_', explode( ' ', $pages_menu_name_array[1]));
        $separated = cre_clean_filename(strtolower($final_separated));
        if ($cPath) {
          $ourFilename = DIR_FS_DOCUMENT_ROOT . DIR_WS_LANGUAGES.$string_lang . '/' . 'pages/' . $separated_cat_name . '_' . $separated . '_' . $pID . '.php';
        } else {
          $ourFilename = DIR_FS_DOCUMENT_ROOT . DIR_WS_LANGUAGES.$string_lang. '/' . 'pages/' . $separated . '_' . $pID .'.php';
        }
        if ($cPath) {
          $sql_file =("UPDATE " . TABLE_CDS_PAGES_DESCRIPTION. "
                       SET pages_file = ('" .$separated_cat_name . '_' . $separated . '_' . $pID . '.php'. "')
                       WHERE pages_id='".$pID."'");
        } else {
          $sql_file =(" UPDATE " . TABLE_CDS_PAGES_DESCRIPTION. "
                        SET pages_file = ('" .$separated . '_' . $pID .'.php'. "')
                        WHERE pages_id='".$pID."'");
        }
        tep_db_query($sql_file);
      }
      // update cds_acf_pages.txt file
      include_once(FILENAME_CDS_ACF_PAGES);
    }

    if ( $action == 'update_page' )  {
      $rename_check = isset($_POST['renamecheck']) ? tep_db_prepare_input( $_POST['renamecheck'] ) : '';
      if ( $rename_check == '1') {
        if ($cPath) {
          $separated_cat_name = cre_clean_filename(cre_get_cat_name($cPath));
        }
        $languages = tep_get_languages();
        for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
          $language_id = (int)$languages[$i]['id'];
          $lang_directory = $result['directory'];
          $pID = isset($_GET['pID']) ? (int)$_GET['pID'] : 0;
          $pagename = tep_pages_get_auxillary_file( $pID,  $languages[$i]['id'] );
          $final_separated = explode ( '_', $pagename);
          $acf_rename = trim($_POST['acf_newname']);
          $separated_filename = implode('_', explode(' ', $acf_rename));
          $separated = cre_clean_filename(strtolower($separated_filename));
          $sql = ("SELECT *
                        from  " . TABLE_LANGUAGES . "");
          $sql_res = tep_db_query($sql);
          while ( $result = tep_db_fetch_array($sql_res) ) {
            $lang_directory = $result['directory'];
            $ourFilename = DIR_FS_DOCUMENT_ROOT . DIR_WS_LANGUAGES . $lang_directory . '/pages/' . $pagename;
            $changed_file = DIR_FS_DOCUMENT_ROOT . DIR_WS_LANGUAGES . $lang_directory . '/' . 'pages/' . $separated_cat_name . '_' . $separated . '_' . $pID . '.php';
            if (file_exists($ourFilename)) {
              rename ($ourFilename, $changed_file);
            }
            $sql_file =("UPDATE " . TABLE_CDS_PAGES_DESCRIPTION. "
                            SET pages_file = ('" . $separated_cat_name . '_' . $separated . '_' . $pID . '.php'. "')
                            WHERE pages_id='" . $pID."'");
            tep_db_query($sql_file);
          }
        }
      } // if $rename_check

    }
    // update cds_acf_pages.txt file
    include_once(FILENAME_CDS_ACF_PAGES);


	// RCI for action insert
	echo $cre_RCI->get('pages', 'action', false);

    if($action == 'new_page'){
    	$messageStack->add_session('pages','Pages Added Successfully','success');
	}else{
		$messageStack->add_session('pages','Pages Updated Successfully','success');
	}

	  $mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
	  if ($mode == 'save') {
	    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pID));
	  } else {  // save & stay
	    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pID . '&action=edit_page'));
	  }
    break;

  case 'deleteconfirm_page':
    tep_pages_remove_page($pID);
    $messageStack->add_session('Pages Deleted Successfully', 'success');
    tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pID));
    break;

  case 'update_sort' :
    $page_sort_order = isset($_POST['page_sort_order']) ? $_POST['page_sort_order'] : array();
    $category_sort_order = isset($_POST['category_sort_order']) ? $_POST['category_sort_order'] : array();
    if ($page_sort_order) {
	  foreach($page_sort_order as $key => $value) {
        $sql_update_sort =("UPDATE " . TABLE_CDS_PAGES_TO_CATEGORIES. "
                            SET  page_sort_order = $value
                            WHERE  pages_id = $key");

        tep_db_query($sql_update_sort);
      }
    }
	foreach($category_sort_order as $key => $value) {
      $sql_update_sort =("UPDATE " . TABLE_CDS_CATEGORIES . "
                          SET categories_sort_order = $value
                          WHERE categories_id = $key");

      tep_db_query($sql_update_sort);
    }
    break;
  }

  // check if the pages/ directory exists
  $sql=("SELECT *
           from  " . TABLE_LANGUAGES . "");
  $sql_res =tep_db_query($sql);
  while ( $result=tep_db_fetch_array($sql_res) ) {
    $lang_directory = $result['directory'];
    $ourFilename=DIR_FS_DOCUMENT_ROOT . DIR_WS_LANGUAGES .$lang_directory. '/' . 'pages/';
    if (is_dir($ourFilename)) {
      if (!is_writeable($ourFilename) ) $messageStack->add(ERROR_CATALOG_PAGE_DIRECTORY_NOT_WRITEABLE, 'error');
    } else {
      $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
    }
  }
  // check if the catalog image directory exists
  if (is_dir(DIR_FS_CATALOG_IMAGES)) {
    if (!is_writeable(DIR_FS_CATALOG_IMAGES ) ) $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
  } else {

    $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<!-- End Tabs -->
<?php include('includes/javascript/editor.php');?>
<SCRIPT LANGUAGE="JavaScript">
  <!-- Begin
  function popUp(URL) {
    day = new Date();
    id = day.getTime();
    eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=500,height=600');");
  }
  function trim(str) {
    return str.replace(/^\s+|\s+$/g,"");
  }
  // End -->
</script>
<?php
if ( $action == 'edit_page' || $action == 'update_page' || $action == 'new_page' || $action == 'insert_page')
{
	if ($action == 'edit_page' || $action == 'update_page') {
	  $form_action = 'update_page' . '&pID=' . $pID;
	} else {
	  $form_action = 'insert_page';
	}
	if (isset($_GET['pID']) ) {
	  $form_action_text = 'Update';
	  $form_action_button =  tep_image_submit('button_quick_save.gif',$form_action_text,'name="Operation" onClick="document.pressed=this.value" VALUE="'.$form_action_text.'"');
	} else {
	  $form_action_text = 'Save';
	  $form_action_button = tep_image_submit('button_quick_save.gif',$form_action_text,'name="Operation" onClick="document.pressed=this.value" VALUE="'.$form_action_text.'"');
	}
	$form_action = (isset($_GET['pID'])) ? 'update_page&pID=' . (int)$_GET['pID'] : 'insert_page';

	for ( $i=0; $i < sizeof($languages); $i++) {
		$page_menu_elements .= '$F( "pages_menu_name[' . $languages[$i]['id'] . ']" )=="" || ';
	}

	$categories_array = array();
	$categories_array[] = array('id' => '', 'text' => TEXT_NO_CATEGORY);
	$categories_query = tep_db_query("SELECT icd.categories_id, icd.categories_name from " . TABLE_CDS_CATEGORIES_DESCRIPTION . " icd  WHERE language_id = '" . (int)$languages_id . "' order by icd.categories_name");
	while ( $categories_values = tep_db_fetch_array($categories_query) ) {
	  $categories_array[] = array('id' => $categories_values['categories_id'], 'text' => $categories_values['categories_name']);
	}

	$title = ( $action=='new_page' ) ? HEADING_TITLE_NEW_PAGES : HEADING_TITLE_EDIT_PAGES;
?>

<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo $title; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

	<form id="new_page" name="new_page" method="post" enctype="multipart/form-data" data-parsley-validate>
		  <div id="button-bar" class="row mb-1">
			<div class="col-9">
					<a href="JavaScript:history.back();" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> Back </a>
					<button type="submit" onclick="updatePage('save');" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo BUTTON_SAVE; ?></button>
					<button type="submit" onclick="updatePage('stay');" class="btn btn-info m-r-3 btn-save-stay"><i class="fa fa-plus-square"></i> <?php echo BUTTON_SAVE_STAY; ?></button>
			</div>
			<div class="col-3 m-b-10 pt-1 pr-2">
			  <div class="btn-group pull-right dark"> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class="btn btn-white dropdown-toggle"> <span id="langDropdownTitle"><?php echo ucwords($_SESSION['language']); ?></span> <span class="caret"></span> </a>
				<ul class="dropdown-menu pull-right" id="langselector" style="left:-50px!important;">
				  <?php
				  $languages = tep_get_languages();
				  for ($i=0; $i<sizeof($languages); $i++) {
					?>
					<li id="langsel_<?php echo $languages[$i]['id']; ?>" class="langval<?php echo (($languages[$i]['id'] == $_SESSION['languages_id'])? ' active':'');?>">
					  <a aria-expanded="false" href="javascript:changeLang('page', '<?php echo $languages[$i]['name'];?>', <?php echo $languages[$i]['id']; ?>)"><?php echo '<span class="ml-2">' . $languages[$i]['name'];?></span></a>
					</li>
					<?php
				  }
				  ?>
				</ul>
			  </div>
			</div>
		  </div>

  <div class="col">
    <?php
    if ($messageStack->size('pages') > 0) {
      echo $messageStack->output('pages');
    }
  $title = (isset($pInfo->pages_title)) ? $pInfo->pages_title : HEADING_TITLE_NEW_PAGES;
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-pages" class="table-pages">
        <div class="row">
          <div class="col-md-8 col-xl-9 dark panel-left rounded-left">
		   <div class="main-heading"><span><?php echo $title; ?></span>
			<div class="main-heading-footer"></div>
		   </div>
            <?php
            // Load Editor
            $pages_body_elements = '';
            $pages_blurb_elements = '';
            for ( $i=0; $i < sizeof($languages); $i++) {
              $pages_body_elements .= 'pages_body[' . $languages[$i]['id'] . '],';
              $pages_blurb_elements .= 'pages_blurb[' . $languages[$i]['id'] . '],';
            }
            if (CDS_WYSIWYG_ON_PAGE_BODY == 'Enabled') {
              echo tep_insert_html_editor($pages_body_elements,'advanced','500');
            }
            if (CDS_WYSIWYG_ON_PAGE_BLURB == 'Enabled') {
              echo tep_insert_html_editor($pages_blurb_elements,'simple','200');
            }

              $this_id = (isset($pInfo)) ? $pInfo->pages_id : '';
              for ($i=0; $i < sizeof($languages); $i++) {
                $acf_file_exists = false;
				$display = ($languages[$i]['id'] == $_SESSION['languages_id']) ? '' : 'display:none;';
				$page_id = ($this_id == '')? 0: $this_id;
				$required_validation = ($languages[$i]['id'] == $_SESSION['languages_id']) ? ' required ' : '';
				$lang_control = '<div class="input-group"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
			?>
				<div style="<?php echo $display; ?>" class="page-lang-pane <?php echo (($languages[$i]['id'] == $_SESSION['languages_id']) ? 'active' : '');?>" id="page-default-pane-<?php echo $languages[$i]['id'];?>">
                  		<table border="0" cellspacing="0" cellpadding="4" width="100%">
							<tr>
							  <td class="control-label main-text"><?php echo ENTRY_TITLE; ?></td>
							  <td class="main"><?php echo $lang_control . tep_draw_input_field('pages_title[' . $languages[$i]['id'] . ']', tep_pages_get_page_title($this_id, $languages[$i]['id']),"size='50'") .'</div>'; ?></td>
							  <td class="main"></td>
							</tr>
							<tr>
							  <td class="control-label main-text"><?php echo ENTRY_MENU_NAME; ?></td>
							  <td class="main"><?php echo $lang_control . tep_draw_input_field('pages_menu_name[' . $languages[$i]['id'] . ']', tep_pages_get_menu_name($this_id, $languages[$i]['id']), ' id="pages_menu_name[' . $languages[$i]['id'] . ']" '. $required_validation .'  onblur="javascript:getpageslug('.$languages[$i]['id'].','.$page_id.', this.value);" ').'</div>' ?></td>
							  <td class="main" width="40%"><span class="required"></span><?php echo '&nbsp;<span class="errorText">&nbsp;' . $error_text . '</span>'; if (isset($_SESSION['error_text'])) unset($_SESSION['error_text']); ?></td>
							</tr>
							<tr>
							  <td class="control-label main-text"><?php echo LABEL_PERMALINK; ?></td>
							  <td class="main">
								 <?php  echo $lang_control . tep_draw_input_field('permalink_name[' . $languages[$i]['id'] . ']',((isset($_GET['pID']))?(tep_pages_get_permalink_name($page_id, $languages[$i]['id'])): ''), 'class="form-control f-w-600 f-s-12 p-l-10 p-r-10" '. $required_validation .' onblur="javascript:checkpagelug('.$languages[$i]['id'].', '.$page_id.');" id="permalink_' . $languages[$i]['id'] . '"').'</div>'; ?>

							  </td>
							  <td class="main" width="40%"><span class="required"></span><?php echo '&nbsp;<span class="errorText">&nbsp;' . $error_text . '</span>'; if (isset($_SESSION['error_text'])) unset($_SESSION['error_text']); ?> <span id="permalink_error_<?php  echo $languages[$i]['id'];?>" style="color:#FF0000;font-size:10px;"></span></td>
							</tr>

							<tr>
							  <td class="control-label main-text" valign="top"><?php echo ENTRY_BLURB; ?></td>
							  <td class="main" colspan="2"><?php echo cre_cds_draw_textarea_field('pages_blurb[' . $languages[$i]['id'] . ']', 'soft', '100%', 3, stripslashes(tep_pages_get_page_blurb($this_id, $languages[$i]['id'])), 'class="form-control"'); ?></td>
							</tr>
							<tr>
							  <td class="control-label main-text" valign="top"><?php echo ENTRY_BODY; ?></td>
							  <td class="main" colspan="2"><?php echo cre_cds_draw_textarea_field('pages_body[' . $languages[$i]['id'] . ']', 'soft', '100%', 25, stripslashes(tep_pages_get_page_body($this_id, $languages[$i]['id'])), 'class="ckeditor"'); ?></td>
							</tr>
							<tr>
							  <td class="control-label main-text" valign="top"><?php echo ENTRY_META_TITLE; ?></td>
							  <td class="main" colspan="2"><?php echo tep_draw_textarea_field('pages_meta_title[' . $languages[$i]['id'] . ']', 'soft', '15', '1', tep_pages_get_page_meta_title($this_id, $languages[$i]['id']),'style="width: 100%" class="form-control"'); ?></td>
							</tr>
							<tr>
							  <td class="control-label main-text" valign="top"><?php echo ENTRY_META_DESCRIPTION; ?></td>
							  <td class="main" colspan="2"><?php echo tep_draw_textarea_field('pages_meta_description[' . $languages[$i]['id'] . ']', 'soft', '15', '1', tep_pages_get_page_meta_description($this_id, $languages[$i]['id']),'style="width: 100%" class="form-control"');?></td>
							</tr>
							<tr>
							  <td class="control-label main-text" valign="top"><?php echo ENTRY_META_KEYWORDS; ?></td>
							  <td class="main" colspan="2"><?php echo tep_draw_textarea_field('pages_meta_keywords[' . $languages[$i]['id'] . ']', 'soft', '15', '1', tep_pages_get_page_meta_keywords ($this_id, $languages[$i]['id']), 'style="width: 100%" class="form-control"'); ?></td>
							</tr>
					  </table>
                        <?php
							if( $action=='edit_page' ) {
							  ?>
							  <div class="main-heading"><span><?php echo TEXT_AUXILIARY_CONTENT_FILE; ?></span>
								<div class="main-heading-footer"></div>
							  </div>
                            <table width="100%" cellspacing="0" cellpadding="2" border="0">
                              <tr>
                                <?php
                                $file_name = DIR_FS_DOCUMENT_ROOT . DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/pages/' . tep_pages_get_auxillary_file($this_id, $languages[$i]['id']);
                                $file_location =  DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/pages/';
                                if ( tep_pages_get_auxillary_file($this_id, $languages[$i]['id']) == '' )  {
                                  $cds_aux_file = 'Upload files';
                                } else if (file_exists($file_name) ) {
                                  $acf_file_exists = true;
                                  $cds_aux_file = '<a href="javascript:popUp(\'' . tep_href_link(FILENAME_CDS_POPUP,'action=view'.'&pID='.(int)$pID.'&language_id='.$languages[$i]['id']) . '\')">' . tep_image(DIR_WS_IMAGES . '/icons/preview.gif', TEXT_PREVIEW) . '<b>' . tep_pages_get_auxillary_file($this_id, $languages[$i]['id']) .'</b></a>';
                                } else {
                                  $cds_aux_file = '<span class="errorText">' . sprintf(TEXT_ACF_FILE_MISSING,tep_pages_get_auxillary_file($this_id, $languages[$i]['id']) ).'</span><br><span class="smallText">' . TEXT_ACF_FILE_PLEASE_UPLOAD . '<b>' . $file_location . '</b></span>';
                                }
                                echo '<td valign="middle" align="center" class ="control-label main-text text-center">'. $cds_aux_file .'</td>';
                                $pID = isset($_GET['pID']) ? (int)$_GET['pID'] : '';
                                $pageid = 0;
                                $pageid = tep_pages_get_auxillary_file($this_id, $languages[$i]['id']);
                                $pagename = "";
                                if ( $pageid != 0 ) {
                                  $pagename=explode ( '.',$pageid );
                                  $pagename=$pagename[0];
                                  $pos=strpos($pagename, "_" , 1);
                                  $pagename=substr($pagename, $pos + 1);
                                }
                                $pageid=explode('_',$pageid);
                                ?>
                              </tr>
                            </table>
					<?php
						}
						echo '</div>';
					}
					?>


                    <!-- IMAGES start-->
                    <div class="ml-2 mr-2">
                      <?php
                      $pages_image = 'no_image.png';
                      $pages_header_banner = 'no_image.png';

                      if (isset($pInfo)) {
                        if ($pInfo->pages_image != '') $pages_image = $pInfo->pages_image;
                        if ($pInfo->pages_header_banner != '') $pages_header_banner = $pInfo->pages_header_banner;

                      }
                      ?>
                      <div class="main-heading m-t-20"><span></span><?php echo PAGES_IMAGES; ?></span>
                        <div class="main-heading-footer"></div>
                      </div>
                      <div class="form-group row mt-3 mb-3">
                        <label class="col-md-2 control-label main-text mt-2 p-0"><?php echo TEXT_PAGES_THUMBNAIL_IMAGE; ?></label>
                        <div class="col-md-10">
                          <div class="medi border rounded">
                            <div class="lc-border p-10">
                              <a onclick="$('#pages_image').click();" class="media-left" style="min-width:<?php echo SMALL_IMAGE_WIDTH; ?>px;" href="javascript:;"><?php echo tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $pages_image, $pages_image, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'class="media-object mt-2"'); ?></a>
                              <div class="media-body mr-2 mb-0">
                                <?php
                                echo '<div class="col d-sm-inline p-0 mb-2">' . $pages_image . tep_draw_hidden_field('pages_previous_image', $pInfo->pages_image) . '</div>';
                                if ($pages_image != 'no_image.png') {
                                  ?>
                                  <div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_DELETE; ?></label><!--<input type="checkbox" name="delete_image" class="js-switch js-delete-1">-->
                                  <?php
										$checked = '';
										if (isset($_POST['delete_image']) && $_POST['delete_image'] == '' ) {
										  $checked = "checked";
										} else {
										  if (!isset($_POST['delete_image'])) {
											$checked = "checked";
										  }
										}
										echo tep_draw_checkbox_field('delete_image','yes', '', '', 'class="js-switch js-delete-1"');
									?>
                                  </div>
                                  <div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_UNLINK; ?></label><!--<input type="checkbox" name="unlink_image" class="js-switch js-unlink-1">-->
                                  <?php
										$checked = '';
										if (isset($_POST['unlink_image']) && $_POST['unlink_image'] == '' ) {
										  $checked = "checked";
										} else {
										  if (!isset($_POST['unlink_image'])) {
											$checked = "checked";
										  }
										}
										echo tep_draw_checkbox_field('unlink_image','yes', '', '', 'class="js-switch js-unlink"');
									?>
                                  </div>
                                  <?php
                                }
                                ?>
                                <script>
                                  function openFileInput() {

                                  }
                                </script>
                                <div class="fine-input-container mt-2 mb-3">
                                  <input type="file" class="filestyle" id="pages_image" name="pages_image" placeholder="<?php echo TEXT_CHOOSE_FILE; ?>" /><?php echo tep_draw_hidden_field('pages_image_previous', $pages_image); ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
					</div>
                    <!-- IMAGES end-->






                    <!-- Page Banner Image start-->
                      <div class="form-group row mt-3 mb-3">
                        <label class="col-md-2 control-label main-text mt-2 p-0"><?php echo ENTRY_CATEGORY_PAGE_BANNER_IMAGE; ?></label>
                        <div class="col-md-10">
                          <div class="media border rounded">
                            <div class="lc-border p-10">
                              <a onclick="$('#pages_header_banner').click();" class="media-left" style="min-width:<?php echo SMALL_IMAGE_WIDTH; ?>px;" href="javascript:;"><?php echo tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $pages_header_banner, $pages_header_banner, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'class="media-object mt-2"'); ?></a>
                              <div class="media-body mr-2 mb-0">
                              <?php
                                echo '<div class="col d-sm-inline p-0 mb-2">' . $pages_header_banner . '</div>';
                                if ($pages_header_banner != 'no_image.png') {
                                  ?>
                                  <div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_DELETE; ?></label><input type="checkbox" name="delete_banner_image" value="yes" class="js-switch js-delete-3"></div>
                                  <div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_UNLINK; ?></label><input type="checkbox" name="unlink_banner_image" value="yes" class="js-switch js-unlink-3"></div>
                                  <?php
                                }
                                ?>
                                <div class="fine-input-container mt-2 mb-3">
                                  <input type="file" class="filestyle" id="pages_header_banner" name="pages_header_banner" placeholder="<?php echo TEXT_CHOOSE_FILE; ?>" /><?php echo tep_draw_hidden_field('pages_header_banner_previous', $pages_header_banner); ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>












                     <!-- Page Banner Image End-->
                 <div class="clearfix"></div>
                <script>
                  function enabledisablerename(renamecheck) {
                    if ( renamecheck.checked )
                      document.new_product.acf_newname.disabled=false;
                    else
                      document.new_product.acf_newname.disabled=true;
                  }
                </script>
                <?php
                 if ( $action == 'edit_page' ) {
                  $pagename = "";
                  $pageid = $pInfo->pages_file;
                  $pagename = explode('.', $pageid);
                  $pagename = $pagename[0];
                  $length = strlen($pagename);
                  $pos = strrpos($pagename, "_");
                  $pagename = substr($pagename, 0, ($length-($length-$pos)));
                  if (isset($cPath)) {
                    $cat_name = cre_get_cat_name($cPath);
                  }
                  if (!isset($cat_name)) {
                    $cat_name = '';
                  }
                  $cat_name .= '_';
                  $pagename = preg_replace("/$cat_name/i", '', $pagename);
                          }
                        ?>
			</div>
			<div class="col-md-4 col-xl-3 dark panel-right rounded-right">
				<div class="sidebar-container p-4">
					<div class="sidebar-heading"><span><?php echo DISPLAY_MODES;  ?></span></div>
					<div class="sidebar-heading-footer"></div>
					<div class="sidebar-row text-left">
					  <div class="sidebar-content-container">


					  <div class="form-group row mt-3">
						<label class="col-sm-5 control-label sidebar-edit mt-2 pl-3 pr-0"><?php echo ENTRY_STATUS; ?></label>
						<div class="col-sm-7">
							<?php
							if (isset($pInfo->pages_status)) {
							  if ($pInfo->pages_status == 1) {
								$active = "checked";
								$inactive = "";
							  } else {
								$active = "";
								$inactive = "checked";
							  }
							} else {
							  if  (CDS_DEFAULT_PAGE_STATUS == 'Active') {
								$active = "checked";
								$inactive = "";
							  } else {
								$active = "";
								$inactive = "checked";
							  }
							}
							//echo tep_draw_radio_field('pages_status', 1 ,$active) . ' ' . TEXT_PAGES_ACTIVE . '&nbsp;&nbsp;' . tep_draw_radio_field('pages_status', 0 , $inactive) . ' ' . TEXT_PAGES_INACTIVE;
							?>
	                        <input name="pages_status" <?php echo $active; ?> data-toggle="toggle" data-on="<i class=\'fa fa-check\'></i> <?php echo TEXT_PAGES_ACTIVE; ?>" data-off="<i class=\'fa fa-times\'></i> <?php echo TEXT_PAGES_INACTIVE; ?>" data-onstyle="success" data-offstyle="danger" type="checkbox" data-size="small">
						</div>
					  </div>
					  <div class="form-group row mt-3">
						<label class="col-sm-5 control-label sidebar-edit mt-2 pl-3 pr-0"><?php echo ENTRY_SORT_ORDER; ?></label>
						<div class="col-sm-7">
							<?php
							if (!isset($pInfo)) {
							  // find last sort order value and increment by 10
							  $value = isset($cPath) ? cre_get_next_sort_value() : 10;
							} else {
							  $value = (int)$pInfo->page_sort_order;
							}
							echo tep_draw_input_field('pages_sort_order', $value, 'size="4"');
							?>
						</div>
					  </div>
					  <div class="form-group row mt-3">
						<label class="col-sm-5 control-label sidebar-edit mt-2 pl-3 pr-0"><?php echo TEXT_SHOW_LINK; ?></label>
						<div class="col-sm-7 sidebar-text">
							<?php
							if ($action == 'edit_page') {
							  $in_menu = ($pInfo->pages_in_menu == 1 ) ? 'checked' : '';
							  $in_listing = ($pInfo->pages_in_page_listing == 2  ) ? 'checked' : '';
							} else {
							  $in_menu = (CDS_DEFAULT_PAGE_IN_MENU == 'On') ? 'checked' : '';
							  $in_listing = (CDS_DEFAULT_PAGE_IN_LISTING == 'On') ? 'checked' : '';
							}
							echo tep_draw_checkbox_field('pages_in_menu', 1 , $in_menu) . '&nbsp;&nbsp;' . TEXT_IN_MENU . '<br/>' .
								 tep_draw_checkbox_field('pages_show_link_in_listing', 2 , $in_listing) . '&nbsp;&nbsp;' . TEXT_IN_PAGE_LISTING;
							?>
						</div>
					  </div>
					  <div class="form-group row mt-3">
						<label class="col-sm-12 control-label sidebar-edit mt-2 pl-3 pr-0 text-left"><?php echo TEXT_ATTACH_PRODUCT; ?></label>
					  </div>
					  <div class="form-group row">
						<div class="col-sm-12">
						<?php
							/*$product_query =tep_db_query ("SELECT p.products_id, pd.products_name
																			  from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd
																			WHERE p.products_id = pd.products_id
																			  and pd.language_id = '" . $languages_id . "'
																			ORDER BY pd.products_name");

							$product_list = array(array( 'id' => '', 'text' => TEXT_SELECT_PRODUCT));
							while ($product = tep_db_fetch_array($product_query)) {
							  $product_list[] = array('id' => $product['products_id'],
																			  'text' => $product['products_name']
																);
							}
							if (isset($pInfo->pages_attach_product)) {
							  echo tep_draw_pull_down_menu('pages_attach_product', $product_list, $pInfo->pages_attach_product);
							} else {
							  echo tep_draw_pull_down_menu('pages_attach_product', $product_list);
							} */
					  ?>

						  <div id="replacediv" class="well" style="padding:0px;margin-bottom: 0px;border: none;">
							  <div id="search_section">
								 <?php echo ((isset($pInfo->pages_attach_product))? '<input type="text" name="cname" class="form-control" id="pages_data" value="'.$pInfo->pages_attach_product.'" style="">' :'<input type="text" class="form-control custom-search cust-input ac_input" id="pages_data" autocomplete="off" name="cname" value="" height="50px;" style="" placeholder="Search Product...." style="margin-top:10px;">'); ?>
								 <?php  if (isset($pInfo->pages_attach_product)) {
									echo tep_draw_hidden_field('pages_attach_product', $pInfo->pages_attach_product);
								  } else {
									echo tep_draw_hidden_field('pages_attach_product', 0);
								  } ?>
							  </div>
						  </div>
						</div>
					  </div>
						<?php
						if ($cre_RCO->get('pages', 'accessgroup') !== true) {
						?>
								<div class="form-group row mt-3">
								  <div class="col-sm-12">
								   <div data-toggle="popover" data-placement="top" data-html="true" data-content='<div class="text-white"><?php echo TEXT_B2B_UPSELL_POPOVER_BODY; ?></div><div class="text-center w-100"><a href="<?php echo TEXT_B2B_UPSELL_GET_B2B_URL; ?>" target="_blank" class="btn btn-warning btn-sm m-r-5 m-t-10"><?php echo TEXT_B2B_UPSELL_GET_B2B; ?></a></div>'>
									<img src="images/category-access-settings.jpg" alt="Get B2B to unlock this feature.">
								   </div>
								  </div>
								</div>
						<?php
						}
                        ?>

					  </div>
				    </div>
				</div>
			</div>
          </form>
<?php
}
elseif ($action == 'edit_category' || $action == 'update_category' || $action == 'new_category' || $action == 'insert_category') {
       if ($action == 'edit_category' || $action == 'update_category') {
                    $form_action = 'update_category' . '&cID=' . (int)$cInfo->categories_id;
                } else {
                    $form_action = 'insert_category';
                }
                $pages_category_query = tep_db_query("select icd.categories_name, icd.categories_heading, ic.categories_image from " . TABLE_CDS_CATEGORIES_DESCRIPTION . " icd, " . TABLE_CDS_CATEGORIES . " ic where ic.categories_id = icd.categories_id and icd.categories_id = '" . (int)$cID . "' and icd.language_id = '" . (int)$languages_id . "'");
                $pages_category = tep_db_fetch_array($pages_category_query);
                $cname = ($cID == '') ? ("") : (" : " . $pages_category['categories_name']);
                $title = ($action=='new_category') ? HEADING_TITLE_NEW_PAGES_CATEGORY : HEADING_TITLE_EDIT_PAGES_CATEGORY;
                $image_string = tep_not_null($pages_category['categories_image']) ? tep_image(DIR_WS_CATALOG_IMAGES . $pages_category['categories_image'], $pages_category['categories_name'], PAGES_IMAGE_WIDTH, PAGES_IMAGE_HEIGHT) : '&nbsp;';
              for ($i=0; $i < sizeof($languages); $i++) {
                $categories_name_elements .= '$F( "categories_name[' . $languages[$i]['id'] . ']" )=="" || ';
              }

			$title = (isset($cInfo)) ? $pages_category['categories_heading'] : HEADING_TITLE_NEW_PAGES_CATEGORY;
?>
			<div id="content" class="content p-relative">
			  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo $title; ?></h1>
			  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
			<?php
			echo '<form id="form_page_category" name="pages" method="post" enctype="multipart/form-data" data-parsley-validate>';
			?>

		      <div id="button-bar" class="row mb-1">
				<div class="col-9">
					<a href="JavaScript:history.back();" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> Back </a>
					<button type="submit" onclick="updatePageCategory('save');" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo BUTTON_SAVE; ?></button>
					<button type="submit" onclick="updatePageCategory('stay');" class="btn btn-info m-r-3 btn-save-stay"><i class="fa fa-plus-square"></i> <?php echo BUTTON_SAVE_STAY; ?></button>
				</div>
				<div class="col-3 m-b-10 pt-1 pr-2">
				  <div class="btn-group pull-right dark"> <a aria-expanded="false" href="javascript:;" data-toggle="dropdown" class="btn btn-white dropdown-toggle"> <span id="langDropdownTitle"><?php echo ucwords($_SESSION['language']); ?></span> <span class="caret"></span> </a>
					<ul class="dropdown-menu pull-right" id="langselector" style="left:-50px!important;">
					  <?php
					  $languages = tep_get_languages();
					  for ($i=0; $i<sizeof($languages); $i++) {
						?>
						<li id="langsel_<?php echo $languages[$i]['id']; ?>" class="langval<?php echo (($languages[$i]['id'] == $_SESSION['languages_id'])? ' active':'');?>">
						  <a aria-expanded="false" href="javascript:changeLang('pageCategory', '<?php echo $languages[$i]['name'];?>', <?php echo $languages[$i]['id']; ?>)"><?php echo '<span class="ml-2">' . $languages[$i]['name'];?></span></a>
						</li>
						<?php
					  }
					  ?>
					</ul>
				  </div>
				</div>
			  </div>


			  <div class="col">
				<?php
				if ($messageStack->size('pages') > 0) {
				  echo $messageStack->output('pages');
				}
				?>
				<!-- begin panel -->
				<div class="dark">
				  <!-- body_text //-->
				  <div id="table-pages" class="table-pages">
					<div class="row">
					  <div class="col-md-8 col-xl-9 dark panel-left rounded-left">

                        <div class="main-heading"><span><?php echo $title;  ?></span>
                          <div class="main-heading-footer"></div>
                        </div>

						<?php
                                                    // editor functions
                          $category_elements = '';
                          $category_blurb_elements = '';
							for ($i=0; $i < sizeof($languages); $i++) {
								$category_elements .= 'categories_description[' . $languages[$i]['id'] . '],';
								$category_blurb_elements .= 'categories_blurb[' . $languages[$i]['id'] . '],';
							}
                          if (CDS_WYSIWYG_ON_CATEGORY_BODY == 'Enabled') {
                            echo tep_insert_html_editor($category_elements,'advanced','500');
                          }
                          if (CDS_WYSIWYG_ON_CATEGORY_BLURB == 'Enabled') {
                            echo tep_insert_html_editor($category_blurb_elements,'simple','200');
                          }
                            $this_id = (isset($cInfo)) ? $cInfo->categories_id : '';
                                                    // editor functions eof

							for ($i=0; $i < sizeof($languages); $i++)
							{
		                    	$display = ($languages[$i]['id'] == $_SESSION['languages_id']) ? '' : 'display:none;';
			                    $required_validation = ($languages[$i]['id'] == $_SESSION['languages_id']) ? ' required ' : '';
								$lang_control = '<div class="input-group"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
							?>
								<div style="<?php echo $display; ?>" class="pageCategory-lang-pane <?php echo (($languages[$i]['id'] == $_SESSION['languages_id']) ? 'active' : '');?>" id="pageCategory-default-pane-<?php echo $languages[$i]['id'];?>">
										<table width="100%"  border="0" cellspacing="0" cellpadding="4">
											<tr>
											 <td class="control-label main-text"><?php echo TEXT_PAGES_CATEGORY_HEADING; ?></td>
											   <td class="main"><?php echo $lang_control . tep_draw_input_field ('categories_heading[' . $languages[$i]['id'] . ']',tep_pages_get_category_heading($this_id, $languages[$i]['id']),'size="50"').'</div>'; ?> </td>
											   <td class="main"></td>
											</tr>
											<tr>
											   <td class="control-label main-text"><?php echo TEXT_PAGES_CATEGORIES_NAME; ?></td>
											   <td class="main"><?php echo $lang_control . tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']', tep_pages_get_category_name($this_id, $languages[$i]['id']), ' id="categories_name[' . $languages[$i]['id'] . ']" onblur="javascript:getpagescategoryslug('.$languages[$i]['id'].','.((isset($this_id) && $this_id >0)? $this_id:0) .', this.value);"') .'</div>';  ?></td>
											   <td class="main" width="30%"><?php echo '&nbsp;<span class="errorText">*&nbsp;' . $error_text . '</span>';  if (isset($_SESSION['error_text'])) unset($_SESSION['error_text']); ?></td>
											</tr>
											<tr>
											   <td class="control-label main-text"><?php echo 'Permalink:'; ?></td>
											   <td class="main"><?php  echo $lang_control . tep_draw_input_field('permalink_name[' . $languages[$i]['id'] . ']',((isset($this_id))?(tep_pages_categories_permalink_name($this_id, $languages[$i]['id'])): ''), 'class="form-control f-w-600 f-s-12 p-l-10 p-r-10" '. $required_validation .' onblur="javascript:checkpagescategoryslug('.$languages[$i]['id'].', '.$this_id.');" id="permalink_' . $languages[$i]['id'] . '"') .'</div>'; ?></td>
											   <td class="main" width="30%"><?php echo '&nbsp;<span class="errorText">*&nbsp;' . $error_text . '</span>';  if (isset($_SESSION['error_text'])) unset($_SESSION['error_text']); ?></td>
											</tr>
											<tr>
											   <td class="control-label main-text" valign="top"><?php echo ENTRY_BLURB; ?></td>
											   <td class="main" colspan="2"><?php echo cre_cds_draw_textarea_field('categories_blurb[' . $languages[$i]['id'] . ']', 'soft', '100%', 3, stripslashes(tep_pages_get_category_blurb($this_id, $languages[$i]['id'])), 'class="form-control"'); ?> </td>
											</tr>
											<tr>
											   <td class="control-label main-text" valign="top"><?php echo TEXT_PAGES_CATEGORIES_DESCRIPTION; ?></td>
											   <td class="main" colspan="2"><?php echo cre_cds_draw_textarea_field('categories_description[' . $languages[$i]['id'] . ']', 'soft', '100%', 25, stripslashes(tep_pages_get_category_description($this_id, $languages[$i]['id'])), 'class="ckeditor"'); ?> </td>
											</tr>
											<tr>
											   <td class="control-label main-text" valign="top"><?php echo ENTRY_META_TITLE; ?></td>
											   <td class="main" colspan="2"><?php echo tep_draw_textarea_field('categories_meta_title[' . $languages[$i]['id'] . ']', 'soft', '15', '1', tep_pages_get_category_meta_title($this_id, $languages[$i]['id']),'style="width: 100%" class="form-control"'); ?> </td>
											</tr>
											<tr>
											   <td class="control-label main-text" valign="top"><?php echo ENTRY_META_DESCRIPTION; ?></td>
											   <td class="main" colspan="2"><?php echo tep_draw_textarea_field('categories_meta_description[' . $languages[$i]['id'] . ']', 'soft', '15', '1', tep_pages_get_category_meta_description($this_id, $languages[$i]['id']),'style="width: 100%" class="form-control"'); ?> </td>
											</tr>
											<tr>
											   <td class="control-label main-text" valign="top"><?php echo ENTRY_META_KEYWORDS; ?></td>
											   <td class="main" colspan="2"><?php echo tep_draw_textarea_field('categories_meta_keywords[' . $languages[$i]['id'] . ']', 'soft', '15', '1', tep_pages_get_meta_keyword_category ($this_id, $languages[$i]['id']), 'style="width: 100%" class="form-control"'); ?> </td>
											</tr>
										</table>
									</div>
									<?php
									}
									?>


                    <!-- IMAGES start-->
                    <div class="ml-2 mr-2">
                      <?php
                      $categories_image = 'no_image.png';
                      $category_heading_title_image = 'no_image.png';
                      $category_header_banner = 'no_image.png';
                      if (isset($cInfo)) {
                        if ($cInfo->categories_image != '') $categories_image = $cInfo->categories_image;
                        if ($cInfo->category_heading_title_image != '') $category_heading_title_image = $cInfo->category_heading_title_image;
                        if ($cInfo->category_header_banner != '') $category_header_banner = $cInfo->category_header_banner;
                      }
                      ?>
                      <div class="main-heading m-t-20"><span></span><?php echo TEXT_CATEGORY_IMAGES; ?></span>
                        <div class="main-heading-footer"></div>
                      </div>
                      <div class="form-group row mt-3 mb-3">
                        <label class="col-md-2 control-label main-text mt-2 p-0"><?php echo TEXT_PAGES_CATEGORIES_IMAGE; ?></label>
                        <div class="col-md-10">
                          <div class="medi border rounded">
                            <div class="lc-border p-10">
                              <a onclick="$('#categories_image').click();" class="media-left" style="min-width:<?php echo SMALL_IMAGE_WIDTH; ?>px;" href="javascript:;"><?php echo tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $categories_image, $categories_image, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'class="media-object mt-2"'); ?></a>
                              <div class="media-body mr-2 mb-0">
                                <?php
                                echo '<div class="col d-sm-inline p-0 mb-2">' . $categories_image . '</div>';
                                if ($categories_image != 'no_image.png') {
                                  ?>

 								<div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_DELETE; ?></label><input type="checkbox" name="delete_image" value="yes" class="js-switch js-delete-1">
                                  </div>
                                  <?php echo $categories_image;?>
                                  <?php echo $_POST['delete_image'];?>
                                  <div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_UNLINK; ?></label><input type="checkbox" name="unlink_image" value="yes" class="js-switch js-unlink-1">
                                  </div>
                                  <?php
                                }
                                ?>
                                <script>
                                  function openFileInput() {

                                  }
                                </script>
                                <div class="fine-input-container mt-2 mb-3">
                                  <input type="file" class="filestyle" id="categories_image" name="categories_image" placeholder="<?php echo TEXT_CHOOSE_FILE; ?>" /><?php echo tep_draw_hidden_field('categories_image_previous', $categories_image); ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row mt-3 mb-3">
                        <label class="col-md-2 control-label main-text mt-2 p-0"><?php echo ENTRY_HEADING_TITLE_IMAGE; ?></label>
                        <div class="col-md-10">
                          <div class="media border rounded">
                            <div class="lc-border p-10">
                              <a onclick="$('#category_heading_title_image').click();" class="media-left" style="min-width:<?php echo SMALL_IMAGE_WIDTH; ?>px;" href="javascript:;"><?php echo tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $category_heading_title_image, $category_heading_title_image, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'class="media-object mt-2"'); ?></a>
                              <div class="media-body mr-2 mb-0">
                                <?php
                                echo '<div class="col d-sm-inline p-0 mb-2">' . $category_heading_title_image . '</div>';
                                if ($category_heading_title_image != 'no_image.png') {
                                  ?>
                                  <div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_DELETE; ?></label><input type="checkbox" name="delete_image_med" value="yes" class="js-switch js-delete-2"></div>
                                  <div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_UNLINK; ?></label><input type="checkbox" name="unlink_image_med" value="yes" class="js-switch js-unlink-2"></div>
                                  <?php
                                }
                                ?>
                                <div class="fine-input-container mt-2 mb-3">
                                  <input type="file" class="filestyle" id="category_heading_title_image" name="category_heading_title_image" placeholder="<?php echo TEXT_CHOOSE_FILE; ?>" /><?php echo tep_draw_hidden_field('category_heading_title_image_previous', $category_heading_title_image); ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group row mt-3 mb-3">
                        <label class="col-md-2 control-label main-text mt-2 p-0"><?php echo ENTRY_CATEGORY_PAGE_BANNER_IMAGE; ?></label>
                        <div class="col-md-10">
                          <div class="media border rounded">
                            <div class="lc-border p-10">
                              <a onclick="$('#category_header_banner').click();" class="media-left" style="min-width:<?php echo SMALL_IMAGE_WIDTH; ?>px;" href="javascript:;"><?php echo tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $category_header_banner, $category_header_banner, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'class="media-object mt-2"'); ?></a>
                              <div class="media-body mr-2 mb-0">
                              <?php
                                echo '<div class="col d-sm-inline p-0 mb-2">' . $category_header_banner . '</div>';
                                if ($category_header_banner != 'no_image.png') {
                                  ?>
                                  <div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_DELETE; ?></label><input type="checkbox" name="delete_image_lrg" value="yes" class="js-switch js-delete-3"></div>
                                  <div class="col d-sm-inline p-0"><label class="control-label ml-3 mr-2 main-text"><?php echo LABEL_UNLINK; ?></label><input type="checkbox" name="unlink_image_lrg" value="yes" class="js-switch js-unlink-3"></div>
                                  <?php
                                }
                                ?>
                                <div class="fine-input-container mt-2 mb-3">
                                  <input type="file" class="filestyle" id="category_header_banner" name="category_header_banner" placeholder="<?php echo TEXT_CHOOSE_FILE; ?>" /><?php echo tep_draw_hidden_field('products_image_rg_previous', $category_header_banner); ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
					</div>
                    <!-- IMAGES end-->

			</div>
			<div class="col-md-4 col-xl-3 dark panel-right rounded-right">
				<div class="sidebar-container p-4">
					<div class="sidebar-heading"><span><?php echo DISPLAY_MODES;  ?></span></div>
					<div class="sidebar-heading-footer"></div>
					<div class="sidebar-row text-left">
					  <div class="sidebar-content-container">

						<div class="form-group row mt-3">
						  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer"><?php echo TEXT_PAGES_CATEGORIES_STATUS; ?></label>
						  <div class="col-sm-7 p-relative">
							<?php
                                   if (isset($cInfo->categories_status)) {
                                     if ($cInfo->categories_status == 1) {
                                       $active = "checked";
                                       $inactive = "";
                                     } else {
                                       $active = "";
                                       $inactive = "checked";
                                     }
                                   } else {
                                     if  (CDS_DEFAULT_CATEGORY_STATUS == 'Active') {
                                       $active = "checked";
                                       $inactive = "";
                                     } else {
                                       $active = "";
                                       $inactive = "checked";
                                     }
                                   }
                                  // echo tep_draw_radio_field('categories_status', 'on', $active) . ' ' . TEXT_PAGES_ACTIVE . '&nbsp;&nbsp;' . tep_draw_radio_field('categories_status', 'off', $inactive) . ' ' . TEXT_PAGES_INACTIVE;
                             ?>
	                        <input name="categories_status" <?php echo $active; ?> data-toggle="toggle" data-on="<i class=\'fa fa-check\'></i> <?php echo TEXT_PAGES_ACTIVE; ?>" data-off="<i class=\'fa fa-times\'></i> <?php echo TEXT_PAGES_INACTIVE; ?>" data-onstyle="success" data-offstyle="danger" type="checkbox" data-size="small">
						  </div>
						</div>
						<div class="form-group row mt-3">
						  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer mt-2">Sort Order:</label>
						  <div class="col-sm-7 p-relative">
                                   <?php
                                         // find last sort order value and increment by 10
                                         if (!isset($cInfo->categories_sort_order)) {
                                     $value = (isset($cPath)) ? cre_get_next_sort_value() : 10;
                                   } else {
                                     $value = (isset($cInfo->categories_sort_order)) ? (int)$cInfo->categories_sort_order : 10;
                                   }
                                                        echo tep_draw_input_field('categories_sort_order', $value, 'size="2"'); ?>
						  </div>
						</div>
						<div class="form-group row mt-3">
						  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer"><?php echo PAGES_SUB_CATEGORY_VIEW; ?></label>
						  <div class="col-sm-7 p-relative sidebar-text"><?php
                                                                              $nested = '';
                                                                              $flat = '';
                                       if (isset($cInfo)) {
                                     if ($cInfo->categories_sub_category_view == 1 ) {
                                                             $nested = 'checked';
                                     } else {
                                                            $flat = 'checked';
                                                          }
                                   } else {
                                      $nested = 'checked';
                                   }
                                   echo tep_draw_radio_field('categories_sub_category_view', 'on', $nested) . ' ' . TEXT_PAGES_NESTED . '<br/>' .
                                   tep_draw_radio_field('categories_sub_category_view', 'off', $flat ) . ' ' . TEXT_PAGES_FLAT; ?></div>
						</div>
						<div class="form-group row mt-3">
						  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer"><?php echo PAGES_LISTING_COLUMNS; ?></label>
						  <div class="col-sm-7 p-relative sidebar-text">
						  <?php
								$one = '';
								$two = '';
								$three = '';
								$four = '';
								if (isset($cInfo->categories_listing_columns)) {
									if ( $cInfo->categories_listing_columns == 1 ) {
										$one = 'checked';
									} elseif( $cInfo->categories_listing_columns == 2 ) {
										$two = 'checked';
									} elseif( $cInfo->categories_listing_columns == 3 ) {
										$three = 'checked';
									} elseif($cInfo->categories_listing_columns == 4 ) {
										$four = 'checked';
									} else {
										$two = 'checked';
									}
								} else {
								  $two = 'checked';
								}
							echo tep_draw_radio_field('categories_listing_columns', '1', $one) . ' ' . '1' .'&nbsp;('. WITH_DESC_LEFT .')'. '<br/>' .
								 tep_draw_radio_field('categories_listing_columns', '2', $two) . ' ' . '1' . '<br/>' .
								 tep_draw_radio_field('categories_listing_columns', '3', $three) . ' ' . '2' . '<br/>' .
								 tep_draw_radio_field('categories_listing_columns', '4', $four ) . ' ' . '3';
							?>
							</div>
						</div>
						<div class="form-group row mt-3">
						  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer"><?php echo TEXT_SHOW_LINK; ?></label>
						  <div class="col-sm-7 p-relative sidebar-text">
                            <?php
                            if ($action == 'edit_category') {
                                         $in_menu = (isset($cInfo->categories_in_menu) && $cInfo->categories_in_menu == 1) ? 'checked' : '';
                                         $in_listing = (isset($cInfo->categories_in_pages_listing) && $cInfo->categories_in_pages_listing == 2) ? 'checked' : '';
                            } else {
                              $in_menu = (CDS_DEFAULT_CATEGORY_IN_MENU == 'On') ? 'checked' : '';
                              $in_listing = (CDS_DEFAULT_CATEGORY_IN_LISTING == 'On') ? 'checked' : '';
                            }
                            echo tep_draw_checkbox_field('category_show_link','1', $in_menu) . ' ' . TEXT_IN_MENU . '<br/>' .
                            	 tep_draw_checkbox_field('category_show_link1','2', $in_listing) . ' ' .TEXT_IN_CATEGORY_LISTING;
                            ?>
						  </div>
						</div>
						<div class="form-group row mt-3">
						  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer"><?php echo TEXT_PAGE_CATEGORY_URL_OVERRIDE; ?></label>
						  <div class="col-sm-7 p-relative"><?php echo tep_draw_input_field('categories_url_override', (isset($cInfo->categories_url_override) ? $cInfo->categories_url_override : ''), 'size="60"'); ?></div>
						</div>
						<div class="form-group row mt-3">
						  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer"><?php echo TEXT_APPEND_CATEGORY; ?></label>
						  <div class="col-sm-7 p-relative">
							<?php
							$checked = '';
							if (isset($cInfo->category_append_cdpath) && $cInfo->category_append_cdpath == 1 ) {
							  $checked = "checked";
							} else {
							  if (!isset($cInfo)) {
								$checked = "checked";
							  }
							}
							echo tep_draw_checkbox_field('category_append_cdpath','1', $checked, '', 'class="js-switch"'); ?>
						  </div>
						</div>
						<div class="form-group row mt-3">
						  <label class="col-sm-5 control-label sidebar-edit pl-3 pr-0 c-pointer"><?php echo TEXT_OVERRIDE_TARGET; ?></label>
						  <div class="col-sm-7 p-relative"><?php echo tep_draw_input_field('categories_url_override_target', (isset($cInfo->categories_url_override_target) ? $cInfo->categories_url_override_target : ''), 'size="30"'); ?></div>
						</div>
						<div class="form-group row mt-3">
						  <label class="col-sm-12 control-label sidebar-edit pl-3 pr-0 c-pointer mt-2 text-left"><?php echo TEXT_ATTACH_PRODUCT; ?></label>
						</div>
						<div class="form-group row">
						  <div class="col-sm-12 p-relative">
                            <?php
                            $product_query =tep_db_query ("SELECT p.products_id, pd.products_name
                                                             from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd
                                                           WHERE p.products_id = pd.products_id
                                                             and pd.language_id = '" . $languages_id . "'
                                                           ORDER BY pd.products_name");

                            $product_list = array(array( 'id' => '', 'text' => TEXT_SELECT_PRODUCT));
                            while ($product = tep_db_fetch_array($product_query)) {
                              $product_list[] = array('id' => $product['products_id'],
                                                      'text' => $product['products_name']
                                                     );
                            }

                            //print_r ($product_list['products_name']);

                          /* if (isset($cInfo->categories_attach_product)) {
                              echo tep_draw_pull_down_menu('categories_attach_product', $product_list, $cInfo->categories_attach_product);
                            } else {
                              echo tep_draw_pull_down_menu('categories_attach_product', $product_list);
                            }*/
                            $categories_attach_product = $cInfo->categories_attach_product;
                           ?>

						   <div id="replacediv" class=" well" style="padding:0px;margin-bottom: 0px;border: none;">
							  <div id="search_section">
								 <?php echo ((isset($cInfo->categories_attach_product))? '<input type="text" name="categories_attach_product" class="form-control" id="search_data" value="'.tep_get_products_name($cInfo->categories_attach_product).'" >' :'<input type="text" class="form-control custom-search cust-input ac_input" id="search_data" autocomplete="off" name="categories_attach_product" value="" height="50px;"  placeholder="Search Product...." style="">'); ?>
							  </div>
						  </div>
						 </div>
					</div>
						<?php
						if ($cre_RCO->get('pages', 'cataccessgroup') !== true) {
						?>
								<div class="form-group row mt-3">
								  <div class="col-sm-12">
								   <div data-toggle="popover" data-placement="top" data-html="true" data-content='<div class="text-white"><?php echo TEXT_B2B_UPSELL_POPOVER_BODY; ?></div><div class="text-center w-100"><a href="<?php echo TEXT_B2B_UPSELL_GET_B2B_URL; ?>" target="_blank" class="btn btn-warning btn-sm m-r-5 m-t-10"><?php echo TEXT_B2B_UPSELL_GET_B2B; ?></a></div>'>
									<img src="images/category-access-settings.jpg" alt="Get B2B to unlock this feature.">
								   </div>
								  </div>
								</div>
						<?php
						}

                        //RCI start
                        echo $cre_RCI->get('pages', 'displayoptions');
                        //RCI end
                        ?>
					  </div>
					</div>
				</div>
			</div>
		</form>
<?php
} else {

	echo tep_draw_form('search', FILENAME_CDS_PAGE_MANAGER );
	echo tep_draw_hidden_field(tep_session_name(), tep_session_id());
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo ($cname != '') ? $cname : HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
	<div class="col main-col">
	  <div class="row">
		<div class="col-6 pr-0 mb-2"></div>
		<div class="col-3 pr-1">
          <?php echo tep_draw_form('search', FILENAME_CDS_PAGE_MANAGER);?>
           <div class="form-group row mb-2 pr-0">
            <label for="cPath" class="hidden-xs col-sm-3 col-form-label text-center m-t-10 pr-0"><?php echo HEADING_TITLE_SEARCH; ?></label>
            <div class="col-sm-9 p-0 dark rounded">
              <?php echo tep_draw_input_field('search'); ?>
            </div>
          </div>
          <?php
          if (isset($_GET[tep_session_name()])) {
            echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
          }
          echo '</form>';
          ?>

		</div>
		<div class="col-3 pr-1">

          <?php echo tep_draw_form('goto', FILENAME_CDS_PAGE_MANAGER, '', 'get'); ?>
           <div class="form-group row mb-2 pr-0">
            <label for="cPath" class="hidden-xs col-sm-3 col-form-label text-center m-t-10 pr-0"><?php echo HEADING_TITLE_GOTO; ?></label>
            <div class="col-sm-9 p-0 dark rounded">
              <?php echo tep_draw_pull_down_menu('cPath', tep_get_pages_category_tree(), $current_categories_id, 'onChange="this.form.submit();"'); ?>
            </div>
          </div>
          <?php
          if (isset($_GET[tep_session_name()])) {
            echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
          }
          echo '</form>';
          ?>

		</div>
	  </div>
	</div>


  <div class="col">
    <?php
    if ($messageStack->size('pages') > 0) {
      echo $messageStack->output('pages');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-pages" class="table-pages">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

                            <?php
                            $pages_cid = 0;
                            if (tep_not_null($cPath)) {
                                $cPath_array = tep_pages_parse_categories_path($cPath);
                                $cPath = implode('_', $cPath_array);
                                $pages_cid = $cPath_array[(sizeof($cPath_array)-1)];
                            }
                            $pages_category_query = tep_db_query("SELECT icd.categories_name, ic.categories_image, ic.categories_url_override, ic.categories_url_override
                                                                               from " . TABLE_CDS_CATEGORIES_DESCRIPTION . " icd, " . TABLE_CDS_CATEGORIES . " ic
                                                                             WHERE ic.categories_id = icd.categories_id
                                                                               and icd.categories_id = '" . (int)$pages_cid . "'
                                                                               and icd.language_id = '" . (int)$languages_id . "'");

                 $pages_category = tep_db_fetch_array($pages_category_query);
                 $current_folder_icon = ($pages_category['categories_url_override'] != '') ? 'cds_override_folder_icon.gif' : 'cds_current_folder_icon.gif';
                 $cname = ($pages_cid == '') ? ("") : ("<table><tr><td>" . tep_image(DIR_WS_ICONS . $current_folder_icon, ICON_FOLDER) . '</td><td class="pageHeading">' . $pages_category['categories_name'] . '</td></tr></table>');
                 $url_override_text = ($pages_category['categories_url_override'] != '') ? '(url override: ' .  '<a href="' . $pages_category['categories_url_override'] . '" target="_blank">' . $pages_category['categories_url_override'] . '</a>)' : '';
                            ?>
					            <table class="table table-hover w-100 mt-2">
									  <thead>
									   <tr class="th-row">
										<th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_PAGES_CATEGORIES; ?></th>
                                        <th scope="col" class="th-col center text-center"><?php echo TABLE_HEADING_STATUS; ?></th>
                                        <th scope="col" class="th-col center text-center"><?php echo TABLE_HEADING_MENU; ?></th>
                                        <th scope="col" class="th-col center text-center"><?php echo TABLE_HEADING_LISTING; ?></th>
                                        <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_PRODUCT; ?></th>
                                        <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_SORT_ORDER; ?></th>
                                        <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
                                    </tr>                                    <thead>
                                    <tbody class="row_page_position">

                                    <?php
                                    $search = '';
                                    $search1 = '';
                                    if (isset($_POST['search']) && tep_not_null($_POST['search'])) {
                                        $keywords = tep_db_input(tep_db_prepare_input($_POST['search']));
                                        $search = " and icd.categories_name like '%" . $keywords . "%'";
                                        $search1 ="and pd.pages_title like '%" . $keywords . "%'";
                                        if (tep_not_null($cPath) ) {
                                            $cPath_array = tep_pages_parse_categories_path($cPath);
                                            $cPath = implode('_', $cPath_array);
                                            $current_categories_id = $cPath_array[(sizeof($cPath_array)-1)];
                                        } else {
                                            $current_categories_id = 0;
                                        }
$sql_listing = " SELECT ic.categories_id as 'ID', ic.categories_parent_id as 'CID', ic.categories_sort_order as 'Sort', ic.categories_status as 'Status', IF(icd.categories_name IS NULL OR icd.categories_name = '', icd.categories_heading, icd.categories_name) as 'Title', ic.categories_image as 'Image', icd.categories_description as 'body', ic.categories_date_added as 'date_added',ic.categories_last_modified as 'date_modified',ic.categories_url_override as 'URL' , 'c' as 'type', ic.categories_in_menu as 'Menu', ic.categories_in_pages_listing as 'Listing', ic.categories_attach_product as 'products_id'
                      from " . TABLE_CDS_CATEGORIES . " ic
                  LEFT JOIN " . TABLE_CDS_CATEGORIES_DESCRIPTION . " icd
                      on ic.categories_id = icd.categories_id
                  WHERE  icd.language_id = '" . (int)$languages_id . "'" . $search . "
                  UNION
                  SELECT p.pages_id as 'ID', p2c.categories_id as 'CID', p2c.page_sort_order as 'Sort', p.pages_status as  'Status', pd.pages_title as 'Title', p.pages_image as 'Image',  pd.pages_body as 'body',  p.pages_date_added as 'date_added', p.pages_date_modified as 'date_modified',p.pages_url as 'URL' , 'p' as 'type', p.pages_in_menu as 'Menu', p.pages_in_page_listing as 'Listing', p.pages_attach_product as 'products_id'
                      from " . TABLE_CDS_PAGES . " p,
                                " . TABLE_CDS_PAGES_DESCRIPTION . "  pd,
                                " . TABLE_CDS_PAGES_TO_CATEGORIES . " p2c
                  WHERE p.pages_id = pd.pages_id
                      and pd.language_id ='" . (int)$languages_id . "'" . $search1 . "
                      and p.pages_id = p2c.pages_id
                  ORDER BY Sort, Title";
} else {
$sql_listing = " SELECT ic.categories_id as 'ID', ic.categories_parent_id as 'CID', ic.categories_sort_order as 'Sort', ic.categories_status as 'Status', IF(icd.categories_name IS NULL OR icd.categories_name = '', icd.categories_heading, icd.categories_name) as 'Title',ic.categories_image as 'Image', icd.categories_description as 'body',  ic.categories_date_added as 'date_added', ic.categories_last_modified as 'date_modified', ic.categories_url_override as 'URL' ,'c' as 'type', ic.categories_in_menu as 'Menu', ic.categories_in_pages_listing as 'Listing' , ic.categories_attach_product as 'products_id'
        from " . TABLE_CDS_CATEGORIES . " ic
    LEFT JOIN " . TABLE_CDS_CATEGORIES_DESCRIPTION . " icd
        on ic.categories_id = icd.categories_id
    WHERE ic.categories_parent_id = '" . (int)$current_categories_id . "'
        and icd.language_id = '" . (int)$languages_id . "'
    UNION
    SELECT p.pages_id as 'ID', p2c.categories_id as 'CID', p2c.page_sort_order as 'Sort', p.pages_status as  'Status', pd.pages_title as 'Title',p.pages_image as 'Image', pd.pages_blurb as 'body', p.pages_date_added as 'date_added', p.pages_date_modified as 'date_modified' ,p.pages_url as 'URL', 'p' as 'type' , p.pages_in_menu as 'Menu', p.pages_in_page_listing as 'Listing', p.pages_attach_product as 'products_id'
        from " . TABLE_CDS_PAGES . " p,
                  " . TABLE_CDS_PAGES_DESCRIPTION . "  pd,
                  " . TABLE_CDS_PAGES_TO_CATEGORIES . " p2c
    WHERE p.pages_id = pd.pages_id
        and pd.language_id ='" . (int)$languages_id . "'
        and p.pages_id = p2c.pages_id
        and p2c.categories_id ='" . (int)$current_categories_id . "'
    ORDER BY Sort, Title";
                                    }

  $listing_query = tep_db_query($sql_listing);
  $category_count = 0;
  $cnt = 0;
  $page_count = 0;  $mixpc = 0;
  echo (tep_draw_form('categories_sort', FILENAME_CDS_PAGE_MANAGER, 'action=update_sort&cPath=' . $cPath));
  while ( $categories = tep_db_fetch_array ( $listing_query ) ) {
      if (isset($_POST['search'])) $cPath = $categories['CID'];
      //print("<br>categories-type : ".$categories['type']."<br>");
      if ($categories['type'] == 'c') {
          $category_count++;		  $mixpc = 1;
      }
      if ($categories['type'] == 'p') {
          $page_count++;
      }
      $cnt++;
      if ( (!isset($_GET['pID']) && !isset($_GET['cID']) || (isset($_GET['pID']) && ($_GET['pID'] == $categories['ID']))) && !isset($pInfo) && !isset($cInfo) && (substr($action, 0, 3) != 'new') && $categories['type'] == 'p') {
          $pInfo = new objectInfo($categories);
      }
      if ( ( !isset($_GET['cID'] ) && !isset( $_GET['pID']) || ( isset($_GET['cID']) && ( $_GET['cID'] == $categories['ID']) ) ) && !isset($cInfo) && (substr($action, 0, 3) != 'new') && $categories['type'] == 'c') {
          $categories_count = array('categories_count' => tep_pages_get_categories_count($categories['ID']));
          $pages_count = array('pages_count' => tep_pages_get_pages_count($categories['ID']));
          $cInfo_array = array_merge($categories, $categories_count, $pages_count);
          $cInfo = new objectInfo($cInfo_array);
      }
      if ($categories['type'] == 'c') {

		  $selected = (isset($cInfo) && is_object($cInfo) && ($categories['ID'] == $cInfo->ID))? true : false;
		  $col_selected = ($selected) ? ' selected' : '';

          if ($selected) {
              echo ' <tr class="toprow table-row dark selected" id="crow_'.$categories['ID'].'" rowid="'.$categories['ID'].'"> ' . "\n";
              echo '<td class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, tep_pages_get_categories_path($categories['ID'])) . '\'">';
          } else {
              echo '<tr class="toprow table-row dark" id="crow_'.$categories['ID'].'" rowid="'.$categories['ID'].'">' . "\n";
              echo '<td class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $categories['ID']) . '\'">';
          }
      } else if($categories['type'] == 'p')  {

		  $selected = (isset($pInfo) && is_object($pInfo) && ($categories['ID'] == $pInfo->ID))? true : false;
		  $col_selected = ($selected) ? ' selected' : '';
          if ($selected) {
              echo ' <tr  class="productrow table-row dark selected" id="crow_'.$categories['ID'].'" rowid="'.$categories['ID'].'"> ' . "\n";
              echo '<td class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID . '&action=edit_page') . '\'">';
          } else {
              echo '<tr class="productrow table-row dark" id="crow_'.$categories['ID'].'" rowid="'.$categories['ID'].'">' . "\n";
              echo '<td class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $categories['ID']) . '\'">';
          }
      }
      if ($categories['type'] == 'c') {
          if ($categories['URL']) {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, tep_pages_get_categories_path($categories['ID'])) . '"><i class="fa fa-folder fa-lg text-info mr-2"></i></a>&nbsp;<b>' . $categories['Title'] . '</b>';
          } else {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, tep_pages_get_categories_path($categories['ID'])) . '"><i class="fa fa-folder fa-lg text-warning mr-2"></i></a>&nbsp;<b>' . $categories['Title'] . '</b>';
          }
      } else if($categories['type']=='p') {
          echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $categories['ID'] . '&action=new_page_preview&read=only') . '"><i class="fa fa-circle-o fa-lg text-info mr-2 mr-2"></i></a>&nbsp;' . $categories['Title'];
      }
      echo "</td>";
      if ($categories['type'] == 'c') {
          if (isset($cInfo) && is_object($cInfo) && ($categories['ID'] == $cInfo->ID) ) {
              echo '<td class="setflag_category table-row dark selected text-center">';
          } else {
              echo '<td class="setflag_category table-row dark text-center">';
          }
      } else if ($categories['type'] == 'p') {
          if (isset($pInfo) && is_object($pInfo) && ($categories['ID'] == $pInfo->ID)) {
              echo '<td class="setflag_page table-row dark selected text-center">';
          } else {
              echo '<td class="setflag_page table-row dark text-center">';
          }
      }
      if ($categories['type'] == 'c') {

  				         $ajax_link = tep_href_link(FILENAME_CDS_PAGE_MANAGER);
				         echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag_category\',\''.$ajax_link.'\',\'action=setflag_category&flag=0&cID='.$categories['ID'].'\', '.$categories['ID'].',0 )" '.(($categories['Status'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
 				         echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag_category\',\''.$ajax_link.'\',\'action=setflag_category&flag=1&cID='.$categories['ID'].'\', '.$categories['ID'].',1  )" '.(($categories['Status'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
      } else if ($categories['type'] == 'p') {
			 echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag_page\',\''.$ajax_link.'\',\'action=setflag_page&flag=0&pID='.$categories['ID'].'\', '.$categories['ID'].',0 )" '.(($categories['Status'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
			 echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag_page\',\''.$ajax_link.'\',\'action=setflag_page&flag=1&pID='.$categories['ID'].'\', '.$categories['ID'].',1  )" '.(($categories['Status'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
      }
      echo '</td>';
      if ($categories['type'] == 'c') {
          if (isset($cInfo) && is_object($cInfo) && ($categories['ID'] == $cInfo->ID) ) {
              echo '<td class="setmenu_category table-row dark selected text-center">';
             //onclick =' onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, tep_pages_get_categories_path($categories['ID'])) . '\'"';
          } else {
              echo '<td class="setmenu_category table-row dark text-center">';
           //onclick ='onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $categories['ID']) . '\'"';
          }
      } else if ($categories['type'] == 'p') {
          if (isset($pInfo) && is_object($pInfo) && ($categories['ID'] == $pInfo->ID)) {
              echo '<td class="setmenu_page table-row dark selected text-center">';
              //onclick ='onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID . '&action=edit_page') . '\'"';
          } else {
              echo '<td class="setmenu_page table-row dark text-center">';

      //onclick ='onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $categories['ID']) . '\'"';
      }
      }
      if ($categories['type'] == 'c') {

     /*     if ($categories['Menu'] == '1') {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=setmenu_category&flag=0&cID=' . $categories['ID'] . '&cPath=' . $cPath, 'NONSSL') . '"><i class="fa fa-bars fa-lg text-success mr-2"></i></a>';
          } else {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=setmenu_category&flag=1&cID=' . $categories['ID'] . '&cPath=' . $cPath, 'NONSSL') . '"><i class="fa fa-bars fa-lg text-muted mr-2"></i></a>';
          } */

			 echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setmenu_category\',\''.$ajax_link.'\',\'action=setmenu_category&flag=0&cID='.$categories['ID'].'\', '.$categories['ID'].',0 )" '.(($categories['Menu'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-bars fa-lg text-success mr-2"></i></a>';
			 echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setmenu_category\',\''.$ajax_link.'\',\'action=setmenu_category&flag=1&cID='.$categories['ID'].'\', '.$categories['ID'].',1  )" '.(($categories['Menu'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-bars fa-lg text-muted mr-2"></i></a>';


      } else if ($categories['type'] == 'p') {
       /*   if ($categories['Menu'] == '1') {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=setmenu_page&flag=0&pID=' . $categories['ID'] . '&cPath=' . $cPath, 'NONSSL') . '"><i class="fa fa-bars fa-lg text-success mr-2"></i></a>';
          } else {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=setmenu_page&flag=1&pID=' . $categories['ID'] . '&cPath=' . $cPath, 'NONSSL') . '"><i class="fa fa-bars fa-lg text-muted mr-2"></i></a>';
          } */

 			 echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setmenu_page\',\''.$ajax_link.'\',\'action=setmenu_page&flag=0&pID='.$categories['ID'].'\', '.$categories['ID'].',0 )" '.(($categories['Menu'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-bars fa-lg text-success mr-2"></i></a>';
			 echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setmenu_page\',\''.$ajax_link.'\',\'action=setmenu_page&flag=1&pID='.$categories['ID'].'\', '.$categories['ID'].',1  )" '.(($categories['Menu'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-bars fa-lg text-muted mr-2"></i></a>';

      }
      echo "</td>";
      if ($categories['type'] == 'c') {
          if (isset($cInfo) && is_object($cInfo) && ($categories['ID'] == $cInfo->ID) ) {
              echo '<td class="setlisting_category table-row dark selected text-center" >';
              //$onclick ='onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, tep_pages_get_categories_path($categories['ID'])) . '\'"';
          } else {
              echo '<td class="setlisting_category table-row dark text-center" >';
             // $onclick ='onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $categories['ID']) . '\'"';
          }
      } else if ($categories['type'] == 'p') {
          if (isset($pInfo) && is_object($pInfo) && ($categories['ID'] == $pInfo->ID)) {
              echo '<td class="setlisting_page table-row dark selected text-center">';
          //$onclick =' onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID . '&action=edit_page') . '\'"';
          } else {
              echo '<td class="setlisting_page table-row dark text-center">';
        //$onclick ='onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $categories['ID']) . '\'"';
          }
      }

      if ($categories['type'] == 'c') {


       /*   if ($categories['Listing'] == '2') {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=setlisting_category&flag=0&cID=' . $categories['ID'] . '&cPath=' . $cPath, 'NONSSL') . '"><i class="fa fa-bars fa-lg text-success mr-2"></i></a>';
          } else {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=setlisting_category&flag=1&cID=' . $categories['ID'] . '&cPath=' . $cPath, 'NONSSL') . '"><i class="fa fa-bars fa-lg text-muted mr-2"></i></a>';
          }
        */

 			 echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setlisting_category\',\''.$ajax_link.'\',\'action=setlisting_category&flag=0&cID='.$categories['ID'].'\', '.$categories['ID'].',0 )" '.(($categories['Listing'] == '1' || $categories['Listing'] == '2')? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-bars fa-lg text-success mr-2"></i></a>';
			 echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setlisting_category\',\''.$ajax_link.'\',\'action=setlisting_category&flag=1&cID='.$categories['ID'].'\', '.$categories['ID'].',1  )" '.(($categories['Listing'] == '0')? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-bars fa-lg text-muted mr-2"></i></a>';




      } elseif ($categories['type'] == 'p') {

         /*if ($categories['Listing'] == '2') {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=setlisting_page&flag=0&pID=' . $categories['ID'] . '&cPath=' . $cPath, 'NONSSL') . '"><i class="fa fa-bars fa-lg text-success mr-2"></i></a>';
          } else {
              echo '<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=setlisting_page&flag=2&pID=' . $categories['ID'] . '&cPath=' . $cPath, 'NONSSL') . '"><i class="fa fa-bars fa-lg text-muted mr-2"></i></a>';
          } */

 			echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setlisting_page\',\''.$ajax_link.'\',\'action=setlisting_page&flag=0&pID='.$categories['ID'].'\', '.$categories['ID'].',0 )" '.(($categories['Listing'] == 2)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-bars fa-lg text-success mr-2"></i></a>';
			 echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setlisting_page\',\''.$ajax_link.'\',\'action=setlisting_page&flag=2&pID='.$categories['ID'].'\', '.$categories['ID'].',2  )" '.(($categories['Listing'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-bars fa-lg text-muted mr-2"></i></a>';


      }
      echo '</td>';
      if ($categories['type'] == 'c') {
          if (isset($cInfo) && is_object($cInfo) && ($categories['ID'] == $cInfo->ID) ) {
              echo '<td class="table-row dark selected text-center" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, tep_pages_get_categories_path($categories['ID'])) . '\'">';
          } else {
              echo '<td class="table-row dark text-center" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $categories['ID']) . '\'">';
          }
      } else if ($categories['type'] == 'p') {
          if (isset($pInfo) && is_object($pInfo) && ($categories['ID'] == $pInfo->ID)) {
              echo '<td class="table-row dark selected text-right" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID . '&action=edit_page') . '\'">';
          } else {
              echo '<td class="table-row dark text-right" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $categories['ID']) . '\'">';
          }
      }
      if ($categories['products_id'] != '0') {
        $products_model = tep_db_fetch_array(tep_db_query("select products_model from " . TABLE_PRODUCTS . " where products_id = '" . $categories['products_id'] . "'"));
        if (tep_not_null($products_model['products_model'])) {
          echo $products_model['products_model'];
        } else {
          echo substr(tep_get_products_name($categories['products_id'], $languages_id), 0, 16);
        }
      }
                  echo '</td>';
   $debug =  (file_exists('debug.txt')) ? $categories['Sort'] : '';
                  if ($categories['type'] == 'c') {
     $name = 'category_sort_order[' . $categories['ID'] . ']';
                      if (isset($cInfo) && is_object($cInfo) && ($categories['ID'] == $cInfo->ID) ) {
                          echo'<td class="table-row dark selected text-right">' . $debug; ?>
                     <?php   /* <input type="text" class="form-control" size="2" tabindex="<?php echo (10 * ($cnt)); ?>" name="<?php echo $name; ?>" value="<?php echo (10 * ($cnt)); ?>"> */ ?>                        <i class="fa fa-arrows fa-lg text-success"></i>
                          <?php
        echo '</td>';
                      } else { echo'<td class="table-row dark text-right">' . $debug; ?>
                         <?php /*   <input type="text"  class="form-control" size="2" tabindex="<?php echo (10 * ($cnt)); ?>" name="<?php echo $name; ?>" value="<?php echo (10 * ($cnt)); ?>" > */ ?>                        	<i class="fa fa-arrows fa-lg text-success"></i>

                          <?php
                      }
                  } else if($categories['type']=='p') {
                      if (isset($pInfo) && is_object($pInfo) && isset($pages['pages_id']) && ($pages['pages_id'] == $pInfo->pages_id)) {
                          echo'<td class="table-row dark selected text-right">' . $debug; ?>
                       <?php  /* <input type="text" class="form-control" size="2" tabindex="<?php echo (10 * ($cnt)); ?>" name="page_sort_order[<?php echo $categories['ID']; ?>]" value="<?php echo (10 * ($cnt));?>"> */ ?>                        <i class="fa fa-arrows fa-lg text-success"></i>
                          <?php
                          echo '</td>';
                      } else { echo'<td class="table-row dark text-right">' . $debug; ?>
                                              <?php /*  <input class="form-control" type="text" size="2" tabindex="<?php echo (10 * ($cnt)); ?>" name="page_sort_order[<?php echo $categories['ID']; ?>]" value="<?php echo (10 * ($cnt)); ?>"> */ ?>                        						<?php if($mixpc == 0){?><i class="fa fa-arrows fa-lg text-success"></i><?php } ?>

                                                <?php
                                                echo  '</td>';
                                              }
                                        }
                                        if ($categories['type'] == 'c') {
                                            if (isset($cInfo) && is_object($cInfo) && ($categories['ID'] == $cInfo->ID) ) {
                                                echo '<td class="table-row dark selected text-right" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, tep_pages_get_categories_path($categories['ID'])) . '\'"><a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $categories['ID'] . '&action=edit_category') . '"><i class="fa fa-edit fa-lg text-success mr-2"></i></a><i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i></td>';
                                            } else  {
                                                echo '<td class="table-row dark text-right" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $categories['ID']) . '\'"><a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $categories['ID']) . '">'.'<a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $categories['ID'] .  '&action=edit_category') . '"><i class="fa fa-edit fa-lg text-success mr-2"></i></a><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a></td>';
                                            }
                                        } else if (($categories['type']=='p')) {
                                            if (isset($pInfo) && is_object($pInfo) && ($categories['ID'] == $pInfo->ID)) {
                                                echo '<td class="table-row dark selected text-right" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID . '&action=edit_page') . '\'"><a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $categories['ID'] . '&action=edit_page') . '"><i class="fa fa-edit fa-lg text-success mr-2"></i></a><i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i></td>';
                                            } else {
                                                echo  '<td class="table-row dark text-right" onclick="document.location.href=\'' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $categories['ID']) . '\'"><a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $categories['ID']. '&action=edit_page') . '"><i class="fa fa-edit fa-lg text-success mr-2"></i></a><a href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $categories['ID']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a></td>';
                                            }
                                        }
                                        echo "</td></tr> ";
                                    } // end while
                                    echo "</form>";
                                    $cPath_back = '';
                                    if (sizeof($cPath_array) > 0) {
                                        for ($i=0, $n=sizeof($cPath_array)-1; $i<$n; $i++) {
                                            if (empty($cPath_back)) {
                                                $cPath_back .= $cPath_array[$i];
                                            } else {
                                                $cPath_back .= '_' . $cPath_array[$i];
                                            }
                                        }
                                    }
                                    $cPath_back = (tep_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
                                    ?>
                                    </table>
                                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                            <tr>
                                                <td class="smallText"><?php echo TEXT_CATEGORIES_COUNT . '&nbsp;' . $category_count . '<br>' . TEXT_PAGES_COUNT . '&nbsp;' . $page_count; ?></td>
                                                <td align="right" class="smallText"><?php
                                   					if ((isset($_GET['cPath']) || isset($_POST['cPath'])) && sizeof($cPath_array) > 0) echo '<a class="btn btn-default btn-sm mt-2 mb-2 mr-2" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, $cPath_back . 'cID=' . $current_categories_id) . '">' . IMAGE_BACK . '</a>&nbsp;';
                                                    if (!isset($_GET['search']))                      								if($mixpc == 0){
                                                    	echo '<a class="btn btn-success btn-sm mt-2 mb-2 mr-2" href="javascript:void(0)" onclick="javascript:updatePagesSortOrder('.$current_category_id.')">' . IMAGE_UPDATE_SORT  . '</a>';                                                    }else{
                                                        echo '<a class="btn btn-success btn-sm mt-2 mb-2 mr-2" href="javascript:void(0)" onclick="javascript:updateSortOrder()">' . IMAGE_UPDATE_SORT  . '</a>';
                                                    }
                                                    echo '<a class="btn btn-success btn-sm mt-2 mb-2 mr-2" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&action=new_category') . '">' . IMAGE_NEW_CATEGORY . '</a><a class="btn btn-success btn-sm mt-2 mb-2 mr-2" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&action=new_page') . '">' . IMAGE_NEW_PAGE . '</a>'; ?>
                                                </td>
                                            </tr>

                                        </table>


          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
                            <?php
                            $heading = array();
                            $contents = array();

                            switch ($action) {

                            case 'delete_category':
                                $heading[] = array('text' => TEXT_PAGES_HEADING_DELETE_PAGES_CATEGORY );
                                $contents[] = array('form' => tep_draw_form('categories_delete', FILENAME_CDS_PAGE_MANAGER, 'action=deleteconfirm_category&cPath=' . $cPath) . tep_draw_hidden_field('cID', $cInfo->ID));

                                $delete_message = TEXT_DELETE_PAGES_CATEGORIES_INTRO .'<br><b>' . $cInfo->Title . '</b>';
                                if (isset($cInfo->categories_page_count) && $cInfo->categories_page_count > 0)
                                	$delete_message .= '<br>' . sprintf(TEXT_DELETE_WARNING_PAGES, $cInfo->categories_page_count);

                                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">' . $delete_message . '</p></div></div></div>');
                                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2 mr-2"><button class="btn btn-danger btn-block btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_DELETE . '</button> <a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cInfo->ID) . '">' . IMAGE_CANCEL . '</a></div>');
                                break;

                            case 'delete_page':
                                $heading[] = array('text' => TEXT_PAGES_HEADING_DELETE_PAGE );
                                $contents[] = array('form' => tep_draw_form('pages_delete', FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cPath=' . $cPath . '&pID=' . $pInfo->ID . '&action=deleteconfirm_page'));
                                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_DELETE_INTRO . '<br><br><b>' . $pInfo->Title . '</b></p></div></div></div>');
                                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2 mr-2"><button class="btn btn-danger btn-block btn-sm mt-2" type="submit">' . IMAGE_DELETE . '</button> <a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel"  href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID) . '">' . IMAGE_CANCEL . '</a></div>');
                                break;

                            case 'move_category':
                                $heading[] = array('text' => TEXT_PAGES_HEADING_MOVE_PAGES_CATEGORY );
                                $contents[] = array('form' => tep_draw_form('categories_move', FILENAME_CDS_PAGE_MANAGER, 'action=moveconfirm_category&cPath=' . $cPath) . tep_draw_hidden_field('cID', $cInfo->ID));
                                $contents[] = array('text' => '<div class="sidebar-title mb-2">'.sprintf(TEXT_MOVE_PAGES_CATEGORIES_INTRO, $cInfo->Title).'</div>');
                                $contents[] = array('text' => '<br>' . sprintf(TEXT_MOVE, $cInfo->Title) . '</div><div class="sidebar-title">' . tep_draw_pull_down_menu('move_to_category_id', tep_get_pages_category_tree(), $current_category_id));
                                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2 mr-2"><button class="btn btn-success btn-sm mt-2 mb-2" type="submit">' . IMAGE_MOVE . '</button> <a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cInfo->ID) . '">' . IMAGE_CANCEL . '</a></div>');
                                break;

                            case 'move_page':
                                $heading[] = array('text' => TEXT_INFO_HEADING_MOVE_PAGES );
                                $contents[] = array('form' => tep_draw_form('pages_move', FILENAME_CDS_PAGE_MANAGER, 'action=moveconfirm_page&cPath=' . $cPath) . tep_draw_hidden_field('pID', $pInfo->ID));
                                $contents[] = array('text' => '<div class="sidebar-title mb-2">'.sprintf(TEXT_MOVE_PAGES_INTRO, $pInfo->Title).'</div>');
                                $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENT_PAGES_CATEGORIES . '</div><div class="sidebar-title">' . tep_output_generated_pages_category_path($pInfo->ID, 'page') .'</div>');
                                $contents[] = array('text' => '<div class="sidebar-text mt-2">' . sprintf(TEXT_MOVE, $pInfo->Title) . '</div><div class="sidebar-title">' . tep_draw_pull_down_menu('move_to_category_id', tep_get_pages_category_tree(), $current_category_id).'</div>');
                                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2 mr-2"><button class="btn btn-success btn-sm mt-2 mb-2" type="submit">' . IMAGE_MOVE . '</button> <a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID) . '">' . IMAGE_CANCEL . '</a></div>');
                                break;

                            case 'copy_to':
                                $heading[] = array('text' => TEXT_INFO_HEADING_COPY_TO_PAGES );
                                $contents[] = array('form' => tep_draw_form('pages_copy', FILENAME_CDS_PAGE_MANAGER, 'action=copyconfirm_page&cPath=' . $cPath) . tep_draw_hidden_field('pID', $pInfo->ID));
                                $contents[] = array('text' => '<div class="sidebar-title mb-2">'.TEXT_INFO_COPY_TO_PAGES_INTRO.'</div>');
                                $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_CURRENT_PAGES_CATEGORIES . '</div><div class="sidebar-title">' . tep_output_generated_pages_category_path($pInfo->ID, 'page') . '</b></div>');
                                $contents[] = array('text' => '<div class="sidebar-text mt-2">' . sprintf(TEXT_COPY, $pInfo->Title) . '</div><div class="sidebar-title">' . tep_draw_pull_down_menu('ID', tep_get_pages_category_tree(), $current_category_id).'</div>');
                                $contents[] = array('text' => '<div class="sidebar-text mt-2">' . TEXT_PAGES_HOW_TO_COPY . '</div><div class="sidebar-title">' . tep_draw_radio_field('copy_as', 'link', true) . ' ' . TEXT_PAGES_COPY_AS_LINK . '<br>' . tep_draw_radio_field('copy_as', 'duplicate') . ' ' . TEXT_COPY_AS_DUPLICATE.'</div>');
                                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2 mr-2"><button class="btn btn-success btn-sm mt-2 mb-2" type="submit">' . IMAGE_COPY . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID) . '">' . IMAGE_CANCEL . '</a></div>');
                                break;

                            default:
                                if ((isset($cInfo) && is_object($cInfo)) && ($cInfo->type == 'c')) {
                                    $heading[] = array('text' => $cInfo->Title );
                                    $returned_rci = $cre_RCI->get('pages', 'sidebarcatbuttons');
                                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2 mr-2"><a
                                    					class="btn btn-success btn-sm mt-2 mb-2 mr-2 btn-edit" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cInfo->ID . '&action=edit_category') . '">' . IMAGE_EDIT . '</a><a
                                    					class="btn btn-grey btn-sm mt-2 mb-2 mr-2 btn-move" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cInfo->ID . '&action=move_category') . '">' . IMAGE_MOVE . '</a>'. $returned_rci .'<a
                                    					class="btn btn-danger btn-sm mt-2 mb-2 mr-2 btn-delete" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cInfo->ID . '&action=delete_category') . '">' . IMAGE_DELETE . '</a></div>');
                                    $contents[] = array('text' => '<div class="sidebar-title text-center">' . tep_info_image($cInfo->Image, $cInfo->Title, SMALL_IMAGE_WIDTH, SMALL_IMAGE_WIDTH) . '<br>' . $cInfo->Image .'</div>');
                                    if (!$cInfo->URL) {
                                        $contents[] = array('text' => '<div class="sidebar-title text-left">' . TEXT_PAGES_CATEGORY_COUNT . ' '  . $cInfo->pages_count.'</div>');
                                        $contents[] = array('text' => '<div class="sidebar-title text-left">' . TEXT_CATEGORIES_COUNT . ' '  . $cInfo->categories_count .'</div>');
                                    } else {
                                        $contents[] = array('text' => '<div class="sidebar-text text-left">' . TEXT_CDS_URL_OVERRIDE . '</div><div class="sidebar-title">' . $cInfo->URL .'</div>');
                                        $contents[] = array('text' => '<div class="sidebar-text text-left">' . TEXT_DATE_PAGES_CATEGORY_CREATED . '</div><div class="sidebar-title">' . tep_date_short($cInfo->date_added) .'</div>');
                                    }
                                } elseif((isset($pInfo) && is_object($pInfo)) && ($pInfo->type == 'p')) {
                                    $heading[] = array('text' => $pInfo->Title );
                                    //RCI start
                                      $returned_rci = $cre_RCI->get('pages', 'sidebarbuttons');
                                    //RCI eof
                                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2 mr-2"><a
                                    						class="btn btn-success btn-sm mt-2 mb-2 mr-2 btn-edit" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID . '&action=edit_page') . '">' . IMAGE_EDIT . '</a><a
                                    						class="btn btn-grey btn-sm mt-2 mb-2 mr-2 btn-move" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID . '&action=move_page') . '">' . IMAGE_MOVE  . '</a><a
                                    						class="btn btn-grey btn-sm mt-2 mb-2 mr-2 btn-default btn-copyto" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID. '&action=copy_to') . '">' . IMAGE_COPY_TO . '</a>'. $returned_rci .'<a
                                    						class="btn btn-danger btn-sm mt-2 mb-2 mr-2 btn-delete" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&pID=' . $pInfo->ID . '&action=delete_page') . '">' . IMAGE_DELETE . '</a>
                                    						</div>');
                                    $contents[] = array('align' => 'center', 'text' => '<div class="sidebar-title text-center">' . tep_info_image($pInfo->Image, $pInfo->Title, SMALL_IMAGE_WIDTH, SMALL_IMAGE_WIDTH).'</div>');
                                    $contents[] = array('text' => '<div class="sidebar-title text-center">' . $pInfo->body.'</div>');
                                    $contents[] = array('text' => '<div class="sidebar-title text-text">' . TEXT_DATE_PAGES_CREATED . '</div><div class="sidebar-title">' . tep_date_short($pInfo->date_added).'</div>');
                                } else {
                                    if (!isset($cInfo->Title)) {
                                        $heading[] = array('text' => '&nbsp;');
                                        if (($page_count == 0) && ($category_count == 0)) {
                                            $contents[] = array('text' => TEXT_NO_CHILD_CATEGORIES_OR_PAGES);
                                        } else {
										   if (($categories['type']=='p') && ($categories['ID'] != '')) {
											 tep_redirect(tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'pID=' . $categories['ID']));
										   }
                                           $contents[] = array('text' => TEXT_CDS_NO_CATEGORIES_OR_PAGES_SELECTED);                                       }
                                    } else {
                                        $heading[] = array('text' => $cInfo->Title );
                                        $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a
                                        				class="btn btn-success btn-sm mt-2 mb-2 mr-2 btn-edit" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cInfo->ID . '&action=edit_category') . '">' . IMAGE_EDIT . '</a><a
                                        				class="btn btn-grey btn-sm mt-2 mb-2 mr-2 btn-move" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cInfo->ID . '&action=move_category') . '">' . IMAGE_MOVE . '</a><a
                                        				class="btn btn-danger btn-sm mt-2 mb-2 mr-2 btn-delete" href="' . tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'cPath=' . $cPath . '&cID=' . $cInfo->ID . '&action=delete_category') . '">' . IMAGE_DELETE . '</a></div>');
                                        $contents[] = array('text' => '<div class="sidebar-title text-center">' . tep_info_image($cInfo->Image, $cInfo->Title, SMALL_IMAGE_WIDTH, SMALL_IMAGE_WIDTH) . '<br>' . $cInfo->Image.'</div>');
                                        $contents[] = array('text' => '<div class="sidebar-text text-left">' . TEXT_DATE_PAGES_CATEGORY_CREATED . '</div><div class="sidebar-title text-left">' . tep_date_short($cInfo->date_added).'</div>');
                                    }
                                }
                                break;
                            }

							  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
								$box = new box;
								echo $box->showSidebar($heading, $contents);
							  }
                            ?>
              <?php
          }
          ?>
        <!-- body_text_eof //-->
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script type="text/javascript" src="includes/javascript/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<script src="includes/javascript/jquery.autocomplete_pos.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="includes/javascript/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery.autocomplete.css" />
<script>
$(document).ready(function(){

  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small',
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  });
});
function updatePageCategory (mode)
{
	var action = '<?php echo str_replace('&amp;', '&', tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=' . $form_action . '&cPath=' . $cPath)); ?>';
	// set the save mode in hidden form input
	$('<input />').attr('type', 'hidden')
	.attr('name', "mode")
	.attr('value', mode)
	.appendTo('#form_page_category');

	<?php
	echo $lcadmin->print_action_js('addupdatepagescategory');
	?>

	$('#form_page_category').attr('action', action).submit();

}

function updatePage (mode)
{
	var action = '<?php echo str_replace('&amp;', '&', tep_href_link(FILENAME_CDS_PAGE_MANAGER, 'action=' . $form_action . '&cPath=' . $cPath)); ?>';
	// set the save mode in hidden form input
	$('<input />').attr('type', 'hidden')
	.attr('name', "mode")
	.attr('value', mode)
	.appendTo('#new_page');

	<?php
	echo $lcadmin->print_action_js('addupdatepages');
	?>

	$('#new_page').attr('action', action).submit();

}


function getpageslug(LID, ID, PAGE_NAME){
    if(PAGE_NAME != ''){
	var permalink_name = $('#permalink_'+LID).val();
	 var page_name = PAGE_NAME.replace(/&/g, "and");
	  params = 'action=get_slug&type=page&id='+ID+'&lid=' + LID + '&name=' + page_name +'&pname='+permalink_name;
		$.ajax({
			type: 'post',
			url: '<?php echo tep_href_link("ajax_common.php");?>',
			data: params,
			success: function (retval) {
				//alert('#permalink_name['+LID+']');
				if(retval == 'error'){
				}else{
				  $('#permalink_'+LID).val(retval);
			   }
			}
		});
	}
}

function checkpagelug(LID,ID){
	  var permalink_name = $('#permalink_'+LID).val();
		  params = 'action=check_slug&type=page&id='+ID+'&lid=' + LID + '&name=' + permalink_name;
			$.ajax({
				type: 'post',
				url: '<?php echo tep_href_link("ajax_common.php");?>',
				data: params,
				success: function (retval) {
					if(retval == 'error'){
						$('#permalink_error_'+LID).html('Duplicate permalink please add a unique permalink!');
						$('#permalink_'+LID).css({border:'1px solid #FF0000', color:'#FF0000'});
						//$('#permalink_'+LID).focus();
						$('#validate').val('0');
					}else{
					  $('#permalink_'+LID).val(retval);
					  $('#permalink_'+LID).css({border:'1px solid #277421', color:'#999999'});
					  $('#permalink_error_'+LID).css('display','none');
					  $('#validate').val('1');
				   }
				}
			});
}
function getpagescategoryslug(LID, ID, CAT_PAGE_NAME){
    if(CAT_PAGE_NAME != ''){
	var permalink_name = $('#permalink_'+LID).val();
	 var cat_page_name = CAT_PAGE_NAME.replace(/&/g, "and");
	  params = 'action=get_slug&type=page_category&id='+ID+'&lid=' + LID + '&name=' + cat_page_name +'&pname='+permalink_name;
		$.ajax({
			type: 'post',
			url: '<?php echo tep_href_link("ajax_common.php");?>',
			data: params,
			success: function (retval) {
				//alert('#permalink_name['+LID+']');
				if(retval == 'error'){
				}else{
				  $('#permalink_'+LID).val(retval);
			   }
			}
		});
	}
}
function checkpagescategoryslug(LID,ID){
	  var permalink_name = $('#permalink_'+LID).val();
		  params = 'action=check_slug&type=page_category&id='+ID+'&lid=' + LID + '&name=' + permalink_name;
			$.ajax({
				type: 'post',
				url: '<?php echo tep_href_link("ajax_common.php");?>',
				data: params,
				success: function (retval) {
					if(retval == 'error'){
						$('#permalink_error_'+LID).html('Duplicate permalink please add a unique permalink!');
						$('#permalink_'+LID).css({border:'1px solid #FF0000', color:'#FF0000'});
						//$('#permalink_'+LID).focus();
						$('#validate').val('0');
					}else{
					  $('#permalink_'+LID).val(retval);
					  $('#permalink_'+LID).css({border:'1px solid #277421', color:'#999999'});
					  $('#permalink_error_'+LID).css('display','none');
					  $('#validate').val('1');
				   }
				}
			});
}

$('#enablesiteid').change(function() {
	if($(this).val()!=''){
		$(".show_push_option").show();
	}
})
$('#updateallstores').change(function() {
	if($(this).val()!=''){
		$(".show_push_option").show();
	}
})

function categorycheck() {
	if(strTrim(document.getElementById('category_name').value) == "")
	{
		alert ('Please enter the Category name');
		return false;
	}
}
function enableEditPermalink(LID){
   $('#permalink_'+LID).prop('readonly', false);
   $('.editbtn_'+LID).css('display', 'none');
   $('.savebtn_'+LID).css('display', 'inline-block');
}
function saveEditPermalink(LID,ID, TP){
	var permalink_name = $('#permalink_'+LID).val();
	if(permalink_name == ''){
	   alert('Please enter permalink');
	   return false;
	}
	$('#ploader').css('display', 'inline-block');
	 params = 'action=update_permalink&type='+TP+'&id='+ID+'&lid=' + LID + '&name=' + permalink_name;
		$.ajax({
			type: 'post',
			url: '<?php echo tep_href_link("ajax_common.php");?>',
			data: params,
			success: function (retval) {
			//alert(retval);
			 $('#ploader').css('display', 'none');
			if(retval == 'MSG-DUPLICATE'){
				alert('Duplicate permalink please add a unique permalink!');
				$('#permalink_'+LID).css({border:'1px solid #FF0000', color:'#FF0000'});
			}else{
				alert('MSG-SUCCESS');
			    $('#permalink_'+LID).css({border:'1px solid #9fa2a5', color:'#9fa2a5'});
			    $('#permalink_'+LID).val(retval);
			    $('#permalink_'+LID).prop('readonly', true);
			    $('.editbtn_'+LID).css('display', 'inline-block');
			    $('.savebtn_'+LID).css('display', 'none');
			    $('#tickmark').css('display', 'inline-block');
			}

			}
		});
}

$(document).ready(function() {
//Code for the Auto Complete for pos
   $("#pages_data").autocomplete("autocomplete_pages_search.php", {
	  selectFirst: true,
	  fun: 'selectCurrent2'
   });

	$('.iframe-btn').fancybox({
	'width'		: 900,
	'height'	: 600,
	'type'		: 'iframe',
	'autoScale'    	: false,
	'autoSize':     false
	});
});

$(document).ready(function() {
//Code for the Auto Complete for pos
   $("#search_data").autocomplete("autocomplete_search.php", {
	  selectFirst: true,
	  fun: 'selectCurrent2'
   });

	$('.iframe-btn').fancybox({
	'width'		: 900,
	'height'	: 600,
	'type'		: 'iframe',
	'autoScale'    	: false,
	'autoSize':     false
	});
});




function selectpage_product(products_id)
{
//alert(products_id);
params = 'pID='+products_id;
$('.ac_results').css('display', 'none');

	$.ajax({
		type:'post',
		data: params,
		url: '<?php echo tep_href_link("ajax_common.php","action=add_ajax_pages_product");?>',
		success:function(retval){
		//alert(retval);
         $('#replacediv').html(retval);
			   $("#pages_data").autocomplete("autocomplete_product_search.php", {
				  selectFirst: true,
				  fun: 'selectCurrent2',
			   });

       }
	});
}
function changeLang(pagetype, langName, langId) {
    $('#langDropdownTitle').html(langName);
    $(".langval").removeClass("active");
    $("#langsel_"+langId).addClass("active");
	$( "."+ pagetype +"-lang-pane" ).hide( "slow" );
	$( "#"+ pagetype +"-default-pane-"+langId ).show( "slow" );
}function updateSortOrder(){
     var selectedData = new Array();
     $('.row_page_position .toprow').each(function() {
	 selectedData.push($(this).attr("rowid"));
     });
    //alert(selectedData);
     updatepagesCategorySortOrder(selectedData);

}
function updatePagesSortOrder(pageid){
     var selectedData = new Array();
     $('.row_page_position .productrow').each(function() {
	 selectedData.push($(this).attr("rowid"));
     });
   // alert(selectedData);
     updatePagesSortOrderAjax(selectedData, pageid);
}

$( ".row_page_position" ).sortable({
 delay: 150
});
function updatepagesCategorySortOrder(data) {
      var requestURL = '<?php echo tep_href_link("ajax_common.php", "action=setpagescategorysorting");?>';
        $.ajax({
            url:requestURL,
            type:'post',
            data:{position:data},
            success:function(response){
             $.gritter.add({ text:'Sort order updated successfully!', sticky:false,time:"2500"});
            }
        })
}
function updatePagesSortOrderAjax(data, pageid) {
      var requestURL = '<?php echo tep_href_link("ajax_common.php", "action=setpagesorting");?>';
        $.ajax({
            url:requestURL,
            type:'post',
            data:{position:data, "category_id":pageid},
            success:function(response){
             $.gritter.add({ text:'Sort order updated successfully!', sticky:false,time:"2500"});
            }
        })
}
<?php
echo $lcadmin->print_js();
?>
</script>
<style>
#search_section .ac_results{width:220px !important;}
</style>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
