<?php
/*
  $Id: featured.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
  case 'insert':
    $expires_date = (trim($_POST['expires_date']) == '' || trim($_POST['expires_date']) == '0000-00-00')?'0000-00-00' : date('Y-m-d', strtotime($_POST['expires_date']));
    tep_db_query("UPDATE " . TABLE_PRODUCTS . " SET featured=1, featured_expires='" . $expires_date . "' WHERE products_id='". $_POST['products_id'] ."'");
    $featured_id = $_POST['products_id'];
    tep_redirect(tep_href_link(FILENAME_FEATURED, 'page=' . $_GET['page']. '&sID=' . $featured_id));
    break;
  case 'deleteconfirm':
    $featured_id = tep_db_prepare_input($_GET['sID']);
    tep_db_query("UPDATE " . TABLE_PRODUCTS . " SET featured=0, featured_expires=null WHERE products_id=$featured_id");
    tep_redirect(tep_href_link(FILENAME_FEATURED, 'page=' . $_GET['page']));
    break;
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-languages" class="table-languages">
        <div class="row">

            <?php
            if ( isset($_GET['action']) && (($_GET['action'] == 'new') || ($_GET['action'] == 'edit')) ) {
              $form_action = 'insert';
              if ( ($_GET['action'] == 'edit') && ($_GET['sID']) ) {
                $form_action = 'update';
                $product_query = tep_db_query("SELECT p.products_id, pd.products_name, p.featured_expires
                                                 from " . TABLE_PRODUCTS . " p,
                                                      " . TABLE_PRODUCTS_DESCRIPTION . " pd
                                               WHERE p.products_id = pd.products_id
                                                 and pd.language_id = '" . $languages_id . "'
                                                 and p.products_id = '" . $_GET['sID'] . "'
                                               ORDER BY pd.products_name");
                $product = tep_db_fetch_array($product_query);
                $sInfo = new objectInfo($product);
              } else {
                $sInfo = new objectInfo(array());
              }
              ?>
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">
			<div class="main-heading m-t-20"><span>Add/Edit Featured Product</span>
			  <div class="main-heading-footer"></div>
			</div>
              <form name="new_feature" <?php echo 'action="' . tep_href_link(FILENAME_FEATURED, tep_get_all_get_params(array('action', 'info', 'sID')) . 'action=' . $form_action, 'NONSSL') . '"'; ?> method="post" class="form-inline"><?php if ($form_action == 'update') echo tep_draw_hidden_field('products_id', $_GET['sID']); ?>
                <table border="0" cellspacing="5" cellpadding="2" class="data-table mt-3">
                  <tr>
                    <td class="main"><?php echo TEXT_FEATURED_PRODUCT; ?>&nbsp;</td>
                    <td class="main">
                    <?php
                     /*echo (isset($sInfo->products_name) ? $sInfo->products_name : tep_draw_products_pull_down('products_id', 'class="form-control"', $featured_array));
                      if (isset($sInfo->products_price)) {
                        echo tep_draw_hidden_field('products_price', $sInfo->products_price);
                      } else {
                        echo tep_draw_hidden_field('products_price', 0);
                      } */
                      ?>
                      <div id="replacediv" class=" well" style="padding:0px;margin-bottom: 0px;border: none;">
						  <div id="search_section">
							 <?php echo ((isset($sInfo->products_name))? '<input type="text" name="cname" class="form-control" id="featured_data" value="'.$sInfo->products_name.'" style="width:500px;">' :'<input type="text" class="form-control custom-search cust-input ac_input" id="featured_data" autocomplete="off" name="cname" value="" height="50px;" style="width:500px;" placeholder="Search Product...." style="margin-top:10px;">'); ?>
							 <?php  if (isset($sInfo->products_price)) {
								echo tep_draw_hidden_field('products_price', $sInfo->products_price);
							  } else {
								echo tep_draw_hidden_field('products_price', 0);
							  } ?>
						  </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="main"><?php echo TEXT_FEATURED_EXPIRES_DATE; ?>&nbsp;</td>
                    <td class="main"><?php echo tep_draw_input_field('expires_date',(($sInfo->featured_expires == '' || $sInfo->featured_expires == '0000-00-00')?'':date('m/d/Y', strtotime($sInfo->featured_expires))),'class="calender form-control" id="calender" data-date-format="mm/dd/yyyy"'); ?></td>
                  </tr>
				  <tr>
					<td align="center" colspan="2"><br><?php echo '<a class="btn btn-default btn-sm mt-2 mb-2 mr-2" href="' . tep_href_link(FILENAME_FEATURED, 'page=' . $_GET['page'] . '&sID=' . (isset($_GET['sID']) ? $_GET['sID'] : 0)) . '">' . IMAGE_CANCEL . '</a>' . (($form_action == 'insert') ? '<button class="btn btn-success btn-sm" type="submit">'. IMAGE_INSERT .'</button>' : '<button class="btn btn-success btn-sm" type="submit">'. IMAGE_UPDATE .'</button>'); ?>
					</td>
				  </tr>
                </table>
              </form>
              <?php
            } else {
              ?>
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
              <table class="table table-hover w-100 mt-2">
                <thead>
                  <tr class="th-row">
                    <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_PRODUCTS; ?></th>
                    <th scope="col" class="th-col dark text-left">&nbsp;</th>
                    <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_SORT; ?></th>
                    <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                  </tr>
                </thead>
                <tbody class="row_featured_position">
                  <?php
                  $featured_query_raw = "SELECT p.products_id, p.products_image, pd.products_name, p.featured, p.featured_expires, p.products_status
                                           from " . TABLE_PRODUCTS . " p,
                                                " . TABLE_PRODUCTS_DESCRIPTION . " pd
                                         WHERE p.products_id = pd.products_id
                                           and pd.language_id = '" . $languages_id . "'
                                           and p.featured=1
                                         ORDER BY p.featured_sort";
                  $featured_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $featured_query_raw, $featured_query_numrows);
                  $featured_query = tep_db_query($featured_query_raw);
                  while ($featured = tep_db_fetch_array($featured_query)) {

                    if ( ((!isset($_GET['sID'])) || ($_GET['sID'] == $featured['products_id'])) && (!isset($sInfo)) ) {
	                    $sInfo = new objectInfo($featured);
                    }
                    $selected = (isset($sInfo) && (is_object($sInfo)) && ($featured['products_id'] == $sInfo->products_id)) ? ' selected' : '';
                    if ($selected) {
                      echo '<tr class="toprow table-row dark selected" id="crow_'.$featured['products_id'].'" rowid="'.$featured['products_id'].'">' . "\n";
                      $onclick = ' onclick="document.location.href=\'' . tep_href_link(FILENAME_FEATURED, 'page=' . $_GET['page'] . '&sID=' . $sInfo->products_id . '&action=edit') . '\'"';
                    } else {
                      echo '<tr class="toprow table-row dark" id="crow_'.$featured['products_id'].'" rowid="'.$featured['products_id'].'">' . "\n";
                      $onclick = ' onclick="document.location.href=\'' . tep_href_link(FILENAME_FEATURED, 'page=' . $_GET['page'] . '&sID=' . $featured['products_id']) . '\'"';
                    }
                    $col_selected = ($selected) ? ' selected' : '';
                    ?>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"<?php echo $onclick; ?>><?php echo $featured['products_name']; ?></td>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"<?php echo $onclick; ?>>&nbsp;</td>
                    <td class="table-col dark text-left<?php echo $col_selected; ?>"><i class="fa fa-arrows fa-lg text-success"></i></td>
                    <td class="table-col dark text-right<?php echo $col_selected; ?>">
                      <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_FEATURED, 'page=' . $_GET['page'] . '&sID=' . $featured['products_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                    </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>

              <div class="pagination-container ml-2 mr-2">
                <div class="results-right"><?php echo $featured_split->display_count($featured_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_FEATURED); ?></div>
                <div class="results-left"><?php echo $featured_split->display_links($featured_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></div>
              </div>

              <div class="float-right mr-3 mt-3 mb-3" role="group">
                <a class="btn btn-success btn-sm mb-2 ml-2 pull-right" href="javascript:void(0)" onclick="javascript:updateSortOrder()">Update Sort Order</a>
                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#myfeaturedModal"><?php echo TEXT_CREATE; ?></button>
              </div>
              <?php
            // RCI code start
            echo $cre_RCI->get('featured', 'bottom');
            // RCI code eof
            ?>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              switch ($action) {
                case 'delete':
                  $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_FEATURED );
                  $contents[] = array('form' => tep_draw_form('featured', FILENAME_FEATURED, 'page=' . $_GET['page'] . '&sID=' . $sInfo->products_id . '&action=deleteconfirm'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0 fw-400">' . sprintf(TEXT_INFO_DELETE_INTRO, $sInfo->products_name) . '</div></div></div>');
                  $contents[] = array('align' => 'center', 'text' => '
                  <button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_CONFIRM_DELETE . '</button>
                  <button class="btn btn-grey btn-sm mt-3 mb-2 btn-cancel" type="button" onclick="window.location=\'' . tep_href_link(FILENAME_FEATURED, 'page=' . $_GET['page'] . '&sID=' . $sInfo->products_id) . '\'">' . IMAGE_CANCEL . '</button>
                  ');
                  break;
                default:
                  if (isset($sInfo) && is_object($sInfo)) {
                    $heading[] = array('text' => '<div class="text-truncate">' . $sInfo->products_name . '</div>');
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
                                    <button class="btn btn-success btn-sm mt-2 mb-2 btn-sidebar btn-edit" onclick="editfeatured('.$sInfo->products_id.','.$_GET['page'].')">' . IMAGE_EDIT . '</button>
                                    <button class="btn btn-danger btn-sm mt-2 mb-2 btn-sidebar btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_FEATURED, 'page=' . $_GET['page'] . '&sID=' . $sInfo->products_id . '&action=delete')  . '\'">' . IMAGE_DELETE . '</button>');
                    $contents[] = array('align' => 'center', 'text' => '<div class="sidebar-img mt-3 well ml-4 mr-4 mb-1">' . tep_info_image($sInfo->products_image, $sInfo->products_name, 100, 100) . '</div><div class="sidebar-text mb-0 mt-0">' . $cInfo->categories_image . '</div><div class="sidebar-text mt-0 mb-3">(' . HEADING_IMAGE_WIDTH . 'x' . ((HEADING_IMAGE_HEIGHT == '') ? HEADING_IMAGE_WIDTH : HEADING_IMAGE_HEIGHT) . ')</div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_EXPIRES_DATE . '<span class="sidebar-title ml-2">' . tep_date_short($sInfo->featured_expires) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1 mb-3">' . TEXT_INFO_STATUS_CHANGE . '<span class="sidebar-title ml-2">' . tep_date_short($sInfo->date_status_change) . '</span></div>');
                  }
                  break;
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            }
            ?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script type="text/javascript" src="includes/javascript/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>
<script src="includes/javascript/jquery.autocomplete_pos.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="includes/javascript/fancyBox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery.autocomplete.css" />
<script src="assets/plugins/eModal/eModal.min.js"></script>
<script script type="text/javascript">


$(function () {
	$('#calender').datepicker({
		todayHighlight: true
	});
});

$(document).ready(function() {
//Code for the Auto Complete for pos
   $("#featured_data").autocomplete("autocomplete_featured_search.php", {
	  selectFirst: true,
	  fun: 'selectCurrent2'
   });

	$('.iframe-btn').fancybox({
	'width'		: 900,
	'height'	: 600,
	'type'		: 'iframe',
	'autoScale'    	: false,
	'autoSize':     false
	});
});

function selectProduct(products_id)
{
//alert(products_id);
params = 'pID='+products_id;
$('.ac_results').css('display', 'none');

	$.ajax({
		type:'post',
		data: params,
		url: '<?php echo tep_href_link("ajax_common.php");?>&action=add_ajax_featured_product',
		success:function(retval){
		//alert(retval);
         $('#replacediv').html(retval);
			   $("#featured_data").autocomplete("autocomplete_product_search.php", {
				  selectFirst: true,
				  fun: 'selectCurrent2',
			   });

       }
	});
}
function updateSortOrder(){
     var selectedData = new Array();
     $('.row_featured_position .toprow').each(function() {
	 selectedData.push($(this).attr("rowid"));
     });
    //alert(selectedData);
     updatefeaturedSortOrder(selectedData);

}
$( ".row_featured_position" ).sortable({
 delay: 150
});
function updatefeaturedSortOrder(data) {
      var requestURL = '<?php echo tep_href_link("ajax_common.php", "action=setfeaturedsorting");?>';
        $.ajax({
            url:requestURL,
            type:'post',
            data:{position:data},
            success:function(response){
             $.gritter.add({ text:'Sort order updated successfully!', sticky:false,time:"2500"});
            }
        })
}
/***** modal edit in special ****/
  function editfeatured(id,page) {
  	if(page == 0 || page == ''){
  		page = 1;
  	}
	var params = {
		buttons: false,
		loadingHtml: '<span class="fa fa-circle-o-notch fa-spin fa-3x text-primary"></span><span class="h4">Loading</span>',
		title: 'Edit Featured Product',
		url: './ajax/modal_edit_featured_products.php?action=edit&page='+page+'&sID='+id,
	};

	return eModal.ajax(params);
}
</script>

 <!-- The Modal -->
  <div class="modal" id="myfeaturedModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header"  style="display:block ruby;">
          <h4 class="modal-title">Add Featured Product</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">
          	  <?php $form_action = 'insert'; ?>
              <form name="new_feature" <?php echo 'action="' . tep_href_link(FILENAME_FEATURED, tep_get_all_get_params(array('action', 'info', 'sID')) . 'action=' . $form_action, 'NONSSL') . '"'; ?> method="post" class="form-inline"><?php if ($form_action == 'update') echo tep_draw_hidden_field('products_id', $_GET['sID']); ?>
                <table border="0" cellspacing="5" cellpadding="2" class="data-table mt-3">
                  <tr>
                    <td class="main"><?php echo TEXT_FEATURED_PRODUCT; ?>&nbsp;</td>
                    <td class="main">
                      <div id="replacediv" class=" well" style="padding:0px;margin-bottom: 0px;border: none;">
						  <div id="search_section">
							 <input type="text" class="form-control custom-search cust-input ac_input" id="featured_data" autocomplete="off" name="cname" value="" height="50px;" style="width:300px;" placeholder="Search Product...." style="margin-top:10px;">
						  </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="main"><?php echo TEXT_FEATURED_EXPIRES_DATE; ?>&nbsp;</td>
                    <td class="main"><?php echo tep_draw_input_field('expires_date',(($sInfo->featured_expires == '' || $sInfo->featured_expires == '0000-00-00')?'':date('m/d/Y', strtotime($sInfo->featured_expires))),'class="calender form-control" id="calender" data-date-format="mm/dd/yyyy"'); ?></td>
                  </tr>
				  <tr>
					<td align="center" colspan="2"><br><?php echo '<button type="button" class="btn btn-default btn-sm mt-2 mb-2 mr-2" data-dismiss="modal">'.IMAGE_CANCEL.'</button>' . (($form_action == 'insert') ? '<button class="btn btn-success btn-sm" type="submit">'. IMAGE_INSERT .'</button>' : '<button class="btn btn-success btn-sm" type="submit">'. IMAGE_UPDATE .'</button>'); ?>
					</td>
				  </tr>
                </table>
              </form>
            </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
        </div>

      </div>
    </div>
  </div>

<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');

?>
