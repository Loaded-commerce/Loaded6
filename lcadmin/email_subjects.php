<?php
/*
  $Id: email_subjects.php, v1 07/11/2005

  Copyright (c) 2005 PassionSeed
  http://PassionSeed.com

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'insert':
    case 'save':
      if (isset($_GET['gID'])) $email_subjects_id = tep_db_prepare_input($_GET['gID']);
      $email_subjects_name = tep_db_prepare_input($_POST['email_subjects_name']);
      $email_subjects_category = tep_db_prepare_input($_POST['email_subjects_category']);

      $sql_data_array = array('email_subjects_name' => $email_subjects_name,
                              'email_subjects_category' => $email_subjects_category);

      if ($action == 'insert') {
        $insert_sql_data = array('date_added' => 'now()');

        $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

        tep_db_perform(TABLE_EMAIL_SUBJECTS, $sql_data_array);
        $email_subjects_id = tep_db_insert_id();
      } elseif ($action == 'save') {
        $update_sql_data = array('last_modified' => 'now()');

        $sql_data_array = array_merge($sql_data_array, $update_sql_data);

        tep_db_perform(TABLE_EMAIL_SUBJECTS, $sql_data_array, 'update', "email_subjects_id = '" . (int)$email_subjects_id . "'");
      }


      if (USE_CACHE == 'true') {
        tep_reset_cache_block('email_subjects');
      }

      tep_redirect(tep_href_link(FILENAME_EMAIL_SUBJECTS, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'gID=' . $email_subjects_id));
      break;
    case 'deleteconfirm':
      $email_subjects_id = tep_db_prepare_input($_GET['gID']);

      tep_db_query("delete from " . TABLE_EMAIL_SUBJECTS . " where email_subjects_id = '" . (int)$email_subjects_id . "'");

      if (USE_CACHE == 'true') {
        tep_reset_cache_block('email_subjects');
      }

      tep_redirect(tep_href_link(FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page']));
      break;
  }
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('currencies') > 0) {
      echo $messageStack->output('currencies'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-emailsubjects" class="table-emailsubjects">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
              <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_EMAIL_SUBJECTS; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_EMAIL_SUBJECTS_CATEGORY; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
              </thead>
              <tbody>
                <?php
                $email_subjects_query_raw = "select email_subjects_id, email_subjects_name, email_subjects_category, date_added, last_modified from " . TABLE_EMAIL_SUBJECTS . " order by email_subjects_name";
                $email_subjects_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $email_subjects_query_raw, $email_subjects_query_numrows);
                $email_subjects_query = tep_db_query($email_subjects_query_raw);
                while ($email_subjects = tep_db_fetch_array($email_subjects_query)) {
                  if ((!isset($_GET['gID']) || (isset($_GET['gID']) && ($_GET['gID'] == $email_subjects['email_subjects_id']))) && !isset($mInfo) && (substr($action, 0, 3) != 'new')) {
                    $mInfo_array = array_merge($email_subjects);
                    $mInfo = new objectInfo($mInfo_array);
                  }

                  $selected = (isset($mInfo) && is_object($mInfo) && ($email_subjects['email_subjects_id'] == $mInfo->email_subjects_id)) ? ' selected' : '';
                  if ($selected) {
                    echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $email_subjects['email_subjects_id'] . '&action=edit') . '\'">' . "\n";
                  } else {
                    echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $email_subjects['email_subjects_id']) . '\'">' . "\n";
                  }
              	  $col_selected = ($selected) ? ' selected' : '';
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $email_subjects['email_subjects_name']; ?></td>
                  <td class="table-col dark text-center<?php echo $col_selected; ?>" align="center"><?php echo (($email_subjects['email_subjects_category'] == '1') ? 'Admin' : 'Customer'); ?></td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?> d-none d-lg-table-cell col-blank"><?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_CURRENCIES, 'page=' . $_GET['page'] . '&cID=' . $currency['currencies_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                  </tr>
                  <?php
                }
                ?>            
			        </tbody>
			      </table>

            <div class="pagination-container ml-2 mr-2">
              <div class="results-right"><?php echo $email_subjects_split->display_count($currency_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CURRENCIES); ?></div>
              <div class="results-left"><?php echo  $email_subjects_split->display_links($currency_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></div>
            </div>

            <div class="float-right mr-2 mt-3 mb-3" role="group">
              <?php 
			        if (empty($action)) {
				        echo '<a class="btn btn-success btn-sm" href="' . tep_href_link(FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $mInfo->email_subjects_id . '&action=new') . '">' . IMAGE_INSERT . '</a>';
              }
              ?>
            </div>                         

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
            $heading = array();
            $contents = array();

            switch ($action) {
              case 'new':
                $heading[] = array('text' => TEXT_HEADING_NEW_EMAIL_SUBJECT);
                $email_subjects_category = (isset($email_subjects_category) ? $email_subjects_category : '');
                $contents[] = array('form' => tep_draw_form('email_subjects', FILENAME_EMAIL_SUBJECTS, 'action=insert', 'post', 'enctype="multipart/form-data"'));
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_NEW_INTRO . '</p></div></div></div>');                                           
                $contents[] = array('text' => '<label class="control-label sidebar-edit mt-2">' . TEXT_EMAIL_SUBJECTS_NAME . '</label>' . tep_draw_input_field('email_subjects_name', null, 'class="form-control"'));
                $contents[] = array('text' => '<label class="control-label sidebar-edit mt-2">' . TEXT_EMAIL_SUBJECTS_CATEGORY . '</label>' . tep_draw_pull_down_menu('email_subjects_category',  array(array("id" => "1", "text" => "Admin"), array("id" => "2", "text" => "Customer")), $email_subjects_category, 'class="form-control"'));
                $languages = tep_get_languages();
                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $_GET['gID']) . '">' . IMAGE_CANCEL . '</a></div>');
                break;
              case 'edit':
                $heading[] = array('text' => TEXT_HEADING_EDIT_EMAIL_SUBJECT);
                $email_subjects_category = (isset($email_subjects_category) ? $email_subjects_category : '');
                $contents[] = array('form' => tep_draw_form('email_subjects', FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $mInfo->email_subjects_id . '&action=save', 'post', 'enctype="multipart/form-data"'));
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_EDIT_INTRO . '</p></div></div></div>');                                           
                $contents[] = array('text' => '<label class="control-label sidebar-edit mt-2">' . TEXT_EMAIL_SUBJECTS_NAME . '</label>' . tep_draw_input_field('email_subjects_name', $mInfo->email_subjects_name, 'class="form-control"'));
                $contents[] = array('text' => '<label class="control-label sidebar-edit mt-2">' . TEXT_EMAIL_SUBJECTS_CATEGORY . '</label>' . tep_draw_pull_down_menu('email_subjects_category',  array(array("id" => "1", "text" => "Admin"), array("id" => "2", "text" => "Customer")), $mInfo->email_subjects_category, 'class="form-control"'));
                $languages = tep_get_languages();
                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $mInfo->email_subjects_id) . '">' . IMAGE_CANCEL . '</a></div>');
                break;               
              case 'delete':
                $heading[] = array('text' => TEXT_HEADING_DELETE_EMAIL_SUBJECT );
                $contents[] = array('form' => tep_draw_form('email_subjects', FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $mInfo->email_subjects_id . '&action=deleteconfirm'));
                $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0 fw-400">' . sprintf(TEXT_DELETE_INTRO, $mInfo->email_subjects_name) . '</div></div></div>');
                $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_CONFIRM_DELETE.'</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $mInfo->email_subjects_id) . '">' . IMAGE_CANCEL . '</a></div>');
                break;
              default:
                if (isset($mInfo) && is_object($mInfo)) {
                  $heading[] = array('text' => '<div class="text-truncate">' . $mInfo->email_subjects_name . '</div>');
                  $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $mInfo->email_subjects_id . '&action=edit') . '">' . IMAGE_EDIT . '</a> <a class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_EMAIL_SUBJECTS, 'page=' . $_GET['page'] . '&gID=' . $mInfo->email_subjects_id . '&action=delete') . '">' . IMAGE_DELETE . '</a></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($mInfo->date_added) . '</span></div>');
                  if (tep_not_null($mInfo->last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-1">'.TEXT_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($mInfo->last_modified). '</span></div>');
                }
                break;
            }

          	if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
          		$box = new box;
          		echo $box->showSidebar($heading, $contents);
          	}
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
