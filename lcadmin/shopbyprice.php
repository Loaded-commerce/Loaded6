<?php
/*
  $Id: shopbyprice.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

 require('includes/application_top.php');

if (!defined('MODULE_SHOPBYPRICE_RANGES')) define('MODULE_SHOPBYPRICE_RANGES', 0);
if (!defined('MODULE_SHOPBYPRICE_RANGE')) define('MODULE_SHOPBYPRICE_RANGE', '');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (!isset($_GET['oID'])) {
  $oid = 1;
} else {
  $oid = $_GET['oID'];
}

$error_message = '';

if ($action == 'save') {
  if ($oid == 1) {
    $sbp_ranges = tep_db_prepare_input($_POST['sbp_ranges']);
    if (is_numeric($sbp_ranges)) {
      tep_db_query('update ' . TABLE_CONFIGURATION . ' set configuration_value = "' . (int)$sbp_ranges . '" where configuration_key = "MODULE_SHOPBYPRICE_RANGES" ');
      tep_redirect(tep_href_link(FILENAME_SHOPBYPRICE, tep_get_all_get_params(array('action'))));
      
    } else {
      $error_message .= TEXT_EDIT_ERROR_RANGES;
    }
    tep_db_query('update ' . TABLE_CONFIGURATION . ' set configuration_value = "' . $_POST['configuration_value'] . '" where configuration_key = "MODULE_SHOPBYPRICE_OVER" ');

    if ($error_message != '') {
      $action = 'edit';
    }
  } else {
    $sbp_input_array = $_POST['sbp_range'];
    $sbp_array[0] = tep_db_prepare_input($sbp_input_array[0]);
    for ($i = 1, $ii = MODULE_SHOPBYPRICE_RANGES; $i < $ii; $i++) {
      $sbp_array[$i] = tep_db_prepare_input($sbp_input_array[$i]);
      if (! is_numeric($sbp_array[$i])) {
        $error_message .= TEXT_EDIT_ERROR_NUMERIC;
      } elseif ($sbp_array[$i] <= $sbp_array[$i - 1]) {
        $error_message .= TEXT_EDIT_ERROR_RANGE;
      }
    }
    if ($error_message == '') {
      $serial_array = serialize($sbp_array);
      $text = tep_db_input($serial_array);
      tep_db_query('update ' . TABLE_CONFIGURATION . ' set configuration_value = "' . $text . '" where configuration_key = "MODULE_SHOPBYPRICE_RANGE" ');
      tep_redirect(tep_href_link(FILENAME_SHOPBYPRICE, tep_get_all_get_params(array('action'))));
      
    } else {
      $action = 'edit';
    }
  }
}

$sbp_array = unserialize(MODULE_SHOPBYPRICE_RANGE);

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-languages" class="table-languages">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_OPTIONS; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody>            
                <?php
                if ($oid == 1) {
                  echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=1&action=edit') . '\'">' . "\n";
                } else {
                  echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=1') . '\'">' . "\n";
                }
                $col_selected1 = ($oid == 1) ? ' selected' : '';
                ?>
                <td class="table-col dark text-left<?php echo $col_selected1; ?>"><?php echo TEXT_INFO_OPTION_1; ?></td>
                <td class="table-col dark text-right<?php echo $col_selected1; ?>">
                  <?php echo ($oid == 1) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=1') . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                </td>
                </tr>
                <?php
                if ($oid == 2) {
                  echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=2&action=edit') . '\'">' . "\n";
                } else {
                  echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=2') . '\'">' . "\n";
                }
                $col_selected2 = ($oid == 2) ? ' selected' : '';
                ?>
                <td class="table-col dark text-left<?php echo $col_selected2; ?>"><?php echo TEXT_INFO_OPTION_2; ?></td>
                <td class="table-col dark text-right<?php echo $col_selected2; ?>">
                  <?php echo ($oid == 2) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=2') . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                </td>
                </tr>
              </tbody>
            </table> 

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              switch ($action) {
                case 'edit':
                  if ($oid == 1) {
                    $heading[] = array('text' => TEXT_EDIT_HEADING_OPTIONS);
                    $contents[] = array('form' => tep_draw_form('sbp_options', FILENAME_SHOPBYPRICE, 'oID=1&action=save'));
                    if ($error_message != '') {
                      $contents[] = array('text' => '<div class="alert-error row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><h4 class="m-0">' . TEXT_ERROR . '</h4>' . $error_message . '</div></div></div>');
                    }
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0 f-w-400">' . TEXT_EDIT_OPTIONS_INTRO . '</div></div></div>');                     
                    $contents[] = array('text' => '<div class="form-label mt-3 mb-1">' . TEXT_INFO_RANGES . '</div><div>' . tep_draw_input_field('sbp_ranges', MODULE_SHOPBYPRICE_RANGES, 'class="form-control"') . '</div>');
                    $contents[] = array('text' => '<div class="form-group mt-3">
                                                     <label class="form-label mt-0 mb-0 pl-2 pr-0">' . TEXT_INFO_OVER . '</label>
                                                     <span class="ml-2">' . tep_draw_checkbox_field('sbp_over', $spb_over, null, null, 'class="js-switch" checked') . '</span>
                                                   </div>');
                    $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><button class="btn btn-grey btn-sm mt-2 mb-2 mr-2 btn-cancel" type="button" onclick="window.location=\'' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=1') . '\'">' . IMAGE_CANCEL . '</button>');              
                  } elseif (MODULE_SHOPBYPRICE_RANGES > 0) {
                    $heading[] = array('text' => TEXT_EDIT_HEADING_RANGE);
                    $contents[] = array('form' => tep_draw_form('sbp_options', FILENAME_SHOPBYPRICE, 'oID=2&action=save'));
                    if ($error_message != '') {
                      $contents[] = array('text' => '<div class="alert-error row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><h4 class="m-0">' . TEXT_ERROR . '</h4>' . $error_message . '</div></div></div>');
                    }
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0 f-w-400">' . TEXT_EDIT_RANGE_INTRO . '</div></div></div>');                     

                    $contents[] = array('text' => '<div class="form-label mt-3 mb-1">' . TEXT_INFO_UNDER . '</div><div>' . tep_draw_input_field('sbp_range[0]', $sbp_array[0], 'class="form-control"') . '</div>');
                    for ($i = 1, $ii = MODULE_SHOPBYPRICE_RANGES; $i < $ii; $i++) {
                      $contents[] = array('text' => '<div class="form-label mt-1 mb-1">' . TEXT_INFO_TO . '</div><div>' . tep_draw_input_field('sbp_range['.$i.']', $sbp_array[$i], 'class="form-control"') . '</div>');
                    }
                    $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-update" type="submit">' . IMAGE_UPDATE . '</button><button class="btn btn-grey btn-sm mt-2 mb-2 mr-2 btn-cancel" type="button" onclick="window.location=\'' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=2') . '\'">' . IMAGE_CANCEL . '</button>');              
                  }
                  break;
                default:
                  if ($oid == 1) {
                    $heading[] = array('text' => TEXT_EDIT_HEADING_OPTIONS);
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0 f-w-400">' . TEXT_INFO_OPTIONS_DESCRIPTION . '</div></div></div>');                     
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=1&action=edit')  . '\'">' . IMAGE_EDIT . '</button></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_INFO_RANGES . '<span class="sidebar-title ml-2">' . MODULE_SHOPBYPRICE_RANGES . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1 mb-3">' . TEXT_INFO_OVER . '<span class="sidebar-title ml-2">' . MODULE_SHOPBYPRICE_OVER . '</span></div>');
                  } else {
                    $heading[] = array('text' => TEXT_EDIT_HEADING_RANGE);
                    if (! MODULE_SHOPBYPRICE_RANGES > 0) {
                      $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0 f-w-400">' . TEXT_INFO_ZERORANGE . '</div></div></div>');
                    } elseif (MODULE_SHOPBYPRICE_RANGE == '') {
                      $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0 f-w-400">' . TEXT_INFO_NORANGE . '</div></div></div>');                     
                      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=2&action=edit')  . '\'">' . IMAGE_EDIT . '</button></div>');
                    } else {
                      $sbp_string = '';
                      for ($i = 1, $ii = count($sbp_array); $i < $ii; $i++) {
                         $sbp_string .= '<div>' . TEXT_INFO_TO . '<b>' . $sbp_array[$i] . '</b></div>';
                      }
                      if (MODULE_SHOPBYPRICE_OVER) {
                        $sbp_string .= '<div><b>' . $sbp_array[$i-1] . '</b>' . TEXT_INFO_ABOVE . '</div>';
                      }                      
                      $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2 f-w-400"><div class="note note-info m-0"><div>' . TEXT_INFO_UNDER . '<b>' . $sbp_array[0] . '</b>' . '</div>' . $sbp_string . '</div></div></div>');                     
                      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-3"><button class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_SHOPBYPRICE, 'oID=2&action=edit')  . '\'">' . IMAGE_EDIT . '</button></div>');
                    }
                  }
                  break;
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function(){

  // fade any error messages
  $('.alert-error').delay(6000).fadeOut('slow');

  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small', 
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  });  
});  
</script>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>