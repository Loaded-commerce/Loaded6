<?php
/*
  $Id: banner_manager.php,v 1.1.1.1 2004/03/04 23:38:12 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  require(DIR_WS_CLASSES . 'file_select.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  $banner_extension = tep_banner_image_extension();

  if (tep_not_null($action)) {
    switch ($action) {
      case 'setflag':
        if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
          tep_set_banner_status($_GET['bID'], $_GET['flag']);

          $messageStack->add_session('search', SUCCESS_BANNER_STATUS_UPDATED, 'success');
        } else {
          $messageStack->add_session('search', ERROR_UNKNOWN_STATUS_FLAG, 'error');
        }

        tep_redirect(tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $_GET['bID']));
        break;
      case 'insert':
      case 'update':
        //print_r($_POST);exit;
        //echo DIR_FS_CATALOG_BANNERS;exit;
      	$banners_id = tep_db_prepare_input($_POST['banners_id']);
        $banners_title = tep_db_prepare_input($_POST['banners_title']);
        $banners_url = tep_db_prepare_input($_POST['banners_url']);
        $new_banners_group = tep_db_prepare_input($_POST['new_banners_group']);
        $banners_group = (empty($new_banners_group)) ? tep_db_prepare_input($_POST['banners_group']) : $new_banners_group;
        $banners_html_text = tep_db_prepare_input($_POST['banners_html_text']);
        $banners_image_local = tep_db_prepare_input($_POST['banners_image_local']);
        $banners_image_target = tep_db_prepare_input($_POST['banners_image_target']);
        $db_image_location = '';
        $expires_date = tep_db_prepare_input($_POST['expires_date']);

        $expires_impressions = tep_db_prepare_input($_POST['expires_impressions']);
        $date_scheduled = tep_db_prepare_input($_POST['date_Scheduled']);

        $banner_error = false;
        if (empty($banners_title)) {
          $messageStack->add('search', ERROR_BANNER_TITLE_REQUIRED, 'error');
          $banner_error = true;
        }

        if (empty($banners_group)) {
          $messageStack->add('search', ERROR_BANNER_GROUP_REQUIRED, 'error');
          $banner_error = true;
        }

		if($_FILES['banners_image']['name'] != '' ){
			$banners_image_local = '';
		}

          if (empty($banners_image_local)) {
            $banners_image = new upload('banners_image');
            $banners_image->set_destination(DIR_FS_CATALOG_BANNERS . $banners_image_target);
            if ( ($banners_image->parse() == false) || ($banners_image->save() == false) ) {
              $banner_error = true;
            }
          }


        if ($banner_error == false) {
          $db_image_location = (tep_not_null($banners_image_local)) ? $banners_image_local : $banners_image_target . $banners_image->filename;
          $sql_data_array = array('banners_title' => $banners_title,
                                  'banners_url' => $banners_url,
                                  'banners_image' => $db_image_location,
                                  'banners_group' => $banners_group,
                                  'banners_html_text' => $banners_html_text
                                  );
          if ($expires_date != '') $sql_data_array['expires_date'] = $expires_date;
          if ($date_scheduled != '') $sql_data_array['date_scheduled'] = $date_scheduled;
          if ($expires_impressions != '') $sql_data_array['expires_impressions'] = $expires_impressions;




          if ($action == 'insert') {
                $insert_sql_data = array('date_added' => 'now()',
                                         'status' => '1');

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

           tep_db_perform(TABLE_BANNERS, $sql_data_array);

            $banners_id = tep_db_insert_id();

            $messageStack->add_session('search', SUCCESS_BANNER_INSERTED, 'success');
          } elseif ($action == 'update') {
            tep_db_perform(TABLE_BANNERS, $sql_data_array, 'update', "banners_id = '" . (int)$banners_id . "'");
            $messageStack->add_session('search', SUCCESS_BANNER_UPDATED, 'success');
          }

          if (tep_not_null($expires_date)) {
            list($day, $month, $year) = explode('/', $expires_date);

            $expires_date = $year .
                            ((strlen($month) == 1) ? '0' . $month : $month) .
                            ((strlen($day) == 1) ? '0' . $day : $day);

            tep_db_query("update " . TABLE_BANNERS . " set expires_date = '" . tep_db_input($expires_date) . "', expires_impressions = null where banners_id = '" . (int)$banners_id . "'");
          } elseif (tep_not_null($expires_impressions)) {
            tep_db_query("update " . TABLE_BANNERS . " set expires_impressions = '" . tep_db_input($expires_impressions) . "', expires_date = null where banners_id = '" . (int)$banners_id . "'");
          }

          if (tep_not_null($date_scheduled)) {
            list($day, $month, $year) = explode('/', $date_scheduled);

            $date_scheduled = $year .
                              ((strlen($month) == 1) ? '0' . $month : $month) .
                              ((strlen($day) == 1) ? '0' . $day : $day);

            tep_db_query("update " . TABLE_BANNERS . " set status = '0', date_scheduled = '" . tep_db_input($date_scheduled) . "' where banners_id = '" . (int)$banners_id . "'");
          }

          tep_redirect(tep_href_link(FILENAME_BANNER_MANAGER, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'bID=' . $banners_id));
        } else {
          $action = 'new';
        }
        break;
      case 'deleteconfirm':
        $banners_id = tep_db_prepare_input($_GET['bID']);

        if (isset($_POST['delete_image']) && ($_POST['delete_image'] == 'on')) {
          $banner_query = tep_db_query("select banners_image from " . TABLE_BANNERS . " where banners_id = '" . (int)$banners_id . "'");
          $banner = tep_db_fetch_array($banner_query);

          if (is_file(DIR_FS_CATALOG_IMAGES . $banner['banners_image'])) {
            if (is_writeable(DIR_FS_CATALOG_IMAGES . $banner['banners_image'])) {
              unlink(DIR_FS_CATALOG_IMAGES . $banner['banners_image']);
            } else {
              $messageStack->add_session('search', ERROR_IMAGE_IS_NOT_WRITEABLE, 'error');
            }
          } else {
            $messageStack->add_session('search', ERROR_IMAGE_DOES_NOT_EXIST, 'error');
          }
        }

        tep_db_query("delete from " . TABLE_BANNERS . " where banners_id = '" . (int)$banners_id . "'");
        tep_db_query("delete from " . TABLE_BANNERS_HISTORY . " where banners_id = '" . (int)$banners_id . "'");

        if (function_exists('imagecreate') && tep_not_null($banner_extension)) {
          if (is_file(DIR_WS_IMAGES . 'graphs/banner_infobox-' . $banners_id . '.' . $banner_extension)) {
            if (is_writeable(DIR_WS_IMAGES . 'graphs/banner_infobox-' . $banners_id . '.' . $banner_extension)) {
              unlink(DIR_WS_IMAGES . 'graphs/banner_infobox-' . $banners_id . '.' . $banner_extension);
            }
          }

          if (is_file(DIR_WS_IMAGES . 'graphs/banner_yearly-' . $banners_id . '.' . $banner_extension)) {
            if (is_writeable(DIR_WS_IMAGES . 'graphs/banner_yearly-' . $banners_id . '.' . $banner_extension)) {
              unlink(DIR_WS_IMAGES . 'graphs/banner_yearly-' . $banners_id . '.' . $banner_extension);
            }
          }

          if (is_file(DIR_WS_IMAGES . 'graphs/banner_monthly-' . $banners_id . '.' . $banner_extension)) {
            if (is_writeable(DIR_WS_IMAGES . 'graphs/banner_monthly-' . $banners_id . '.' . $banner_extension)) {
              unlink(DIR_WS_IMAGES . 'graphs/banner_monthly-' . $banners_id . '.' . $banner_extension);
            }
          }

          if (is_file(DIR_WS_IMAGES . 'graphs/banner_daily-' . $banners_id . '.' . $banner_extension)) {
            if (is_writeable(DIR_WS_IMAGES . 'graphs/banner_daily-' . $banners_id . '.' . $banner_extension)) {
              unlink(DIR_WS_IMAGES . 'graphs/banner_daily-' . $banners_id . '.' . $banner_extension);
            }
          }
        }

        $messageStack->add_session('search', SUCCESS_BANNER_REMOVED, 'success');

        tep_redirect(tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page']));
        break;
    }
  }

// check if the graphs directory exists
  $dir_ok = false;
  if (function_exists('imagecreate') && tep_not_null($banner_extension)) {
    if (is_dir(DIR_WS_IMAGES . 'graphs')) {
      if (is_writeable(DIR_WS_IMAGES . 'graphs')) {
        $dir_ok = true;
      } else {
        $messageStack->add('search', ERROR_GRAPHS_DIRECTORY_NOT_WRITEABLE, 'error');
      }
    } else {
      $messageStack->add('search', ERROR_GRAPHS_DIRECTORY_DOES_NOT_EXIST, 'error');
    }
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<?php
  if ($action == 'new') {
    $form_action = 'insert';

    $parameters = array('expires_date' => '',
                        'date_scheduled' => '',
                        'banners_title' => '',
                        'banners_url' => '',
                        'banners_group' => '',
                        'banners_image' => '',
                        'banners_html_text' => '',
                        'expires_impressions' => '');

    $bInfo = new objectInfo($parameters);

    if (isset($_GET['bID'])) {
      $form_action = 'update';

      $bID = tep_db_prepare_input($_GET['bID']);

      $banner_query = tep_db_query("select banners_title, banners_url, banners_image, banners_group, banners_html_text, status, date_format(date_scheduled, '%d/%m/%Y') as date_scheduled, date_format(expires_date, '%d/%m/%Y') as expires_date, expires_impressions, date_status_change from " . TABLE_BANNERS . " where banners_id = '" . (int)$bID . "'");
      $banner = tep_db_fetch_array($banner_query);

      $bInfo->objectInfo($banner);
    } elseif (tep_not_null($_POST)) {
      $bInfo->objectInfo($_POST);
    }

    $groups_array = array();
    $groups_query = tep_db_query("select distinct banners_group from " . TABLE_BANNERS . " order by banners_group");
    while ($groups = tep_db_fetch_array($groups_query)) {
      $groups_array[] = array('id' => $groups['banners_group'], 'text' => $groups['banners_group']);
    }

/*    $images_array = array();
    $images_query = tep_db_query("select distinct banners_image from ". TABLE_BANNERS . " order by banners_image");
    while($image = tep_db_fetch_array($images_query)){
    	$images_array[] = array('id' => basename($image['banners_image']), 'text' => basename($image['banners_image']));
    }*/
          // array used by the DirSelect class
          $ImageLocations['base_dir'] = DIR_FS_CATALOG_IMAGES . 'banners/';
          $ImageLocations['base_url'] = DIR_WS_CATALOG_IMAGES . 'banners/';
          $manage_image = new DirSelect($ImageLocations);
          $image_dir = $manage_image->getDirs();
          unset($image_dir);

          $image_file = $manage_image->getFiles('');
          $file_list = '<option value=""> -- Select Banner Image -- </option>';
          foreach ($image_file[1] as $relative => $fullpath) {
            //  echo $relative . '<br>';
              if($relative == $bInfo->banners_image){
                  $file_list .= '<option selected value="' . rawurlencode($relative) . '">' . $relative . '</option>';
              } else {
                  $file_list .= '<option value="' . rawurlencode($relative) . '">' . $relative . '</option>';
              }
          }
          unset($image_file);

        unset($manage_image, $relative, $fullpath);
?>
<style>
.filebannerimage{
	position: absolute;
	top: 11%;
	left: 38%;
	width: 27%;
}
</style>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <?php
    if ($messageStack->size('bannermanager') > 0) {
      echo $messageStack->output('bannermanager');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-bannermanager" class="table-bannermanager">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left">

		<?php echo tep_draw_form('new_banner', FILENAME_BANNER_MANAGER, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'action=' . $form_action, 'post', 'enctype="multipart/form-data"'); if ($form_action == 'update') echo tep_draw_hidden_field('banners_id', $bID); ?>
        <table border="0" cellspacing="0" cellpadding="5" width="100%">
          <tr>
            <td class="control-label main-text"><?php echo TEXT_BANNERS_TITLE; ?><span class="required"></span></td>
            <td><?php echo tep_draw_input_field('banners_title', $bInfo->banners_title, 'class="form-control", required'); ?></td>
          </tr>
          <tr>
            <td class="control-label main-text"><?php echo TEXT_BANNERS_URL; ?></td>
            <td><?php echo tep_draw_input_field('banners_url', $bInfo->banners_url); ?></td>
          </tr>
          <tr>
            <td class="control-label main-text" valign="mid"><?php echo TEXT_BANNERS_GROUP; ?></td>
            <td class="control-label main-text text-left">
            	<div class="row"><div class="col-md-6" style="padding-right:0"><?php echo tep_draw_pull_down_menu('banners_group', $groups_array, $bInfo->banners_group,'style="max-width:50%;display:inline-block;"') . '&nbsp;&nbsp;' . TEXT_BANNERS_NEW_GROUP . '</div> <div class="col-md-6" style="padding-left:0">' . tep_draw_input_field('new_banners_group', '', 'style="display:inline;"', ((sizeof($groups_array) > 0) ? false : true)); ?></div></div>
            </td>
          </tr>
          <tr>
            <td class="control-label main-text" valign="mid"><?php echo TEXT_BANNERS_IMAGE; ?></td>
            <td class="control-label main-text text-left">
            	<div class="row"><div class="col-md-6" style="padding-right:0"><select name="banners_image_local" style="max-width:50%; display: inline-block !important;" class="form-control"><?php echo $file_list; ?></select><?php echo /*tep_draw_pull_down_menu('banners_image_local', $images_array, $bInfo->banners_image,'style="max-width:50%;display:inline-block;"') .*/ '&nbsp;&nbsp;' . TEXT_BANNERS_IMAGE_LOCAL . '</div> <div class="col-md-6" style="padding-left:0">' . tep_draw_file_field('banners_image', '', '', 'class="filestyle" style="max-width:42%;display:inline-block;"'); ?></div></div>
            </td>
          </tr>
<?php
if(trim($bInfo->banners_image) != '') {
?>
          <tr>
             <td class="control-label main-text" valign="top"></td>
             <td class="control-label main-text text-left" valign="top">
             	<?php echo tep_info_image(DIR_WS_BANNERS.$bInfo->banners_image, $bInfo->banners_image, '280', '150','style="object-fit:contain;" class="img-thumbnail"'); ?>
             </td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php } ?>
          <tr>
            <td valign="top" class="control-label main-text"><?php echo TEXT_BANNERS_HTML_TEXT; ?></td>
            <td><?php echo tep_draw_textarea_field('banners_html_text', 'soft', '60', '5', $bInfo->banners_html_text, 'class="ckeditor"'); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="control-label main-text"><?php echo TEXT_BANNERS_SCHEDULED_AT; ?><br><small><!-- (dd/mm/yyyy) --><?php echo DATE_FORMAT;?></small></td>
            <td valign="top"><?php echo tep_draw_input_field('date_Scheduled','','class="calender form-control" id="calender" data-date-format="yyyy-mm-dd"'); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td valign="top" class="control-label main-text"><?php echo TEXT_BANNERS_EXPIRES_ON; ?><br><small><!-- (dd/mm/yyyy) --><?php echo DATE_FORMAT;?></small></td>
            <td class="control-label main-text text-left"><?php echo tep_draw_input_field('expires_date','','class="calender form-control" id="calender1" data-date-format="yyyy-mm-dd"').'' . TEXT_BANNERS_OR_AT . '<br>' . tep_draw_input_field('expires_impressions',$bInfo->expires_impressions, 'class="form-control"   maxlength="7" size="7"') . ' ' . TEXT_BANNERS_IMPRESSIONS; ?></td>
          </tr>
        </table>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="control-label main-text text-left"></td>
            <td class="control-label main-text" align="right" valign="top" nowrap="nowrap"><?php echo (($form_action == 'insert') ? '<button class="btn btn-success btn-sm" type="submit">' . IMAGE_INSERT . '</button>' : '<button class="btn btn-success btn-sm" type="submit">' . IMAGE_UPDATE . '</button>'). '&nbsp;&nbsp;<a class="btn btn-default btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_BANNER_MANAGER, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . (isset($_GET['bID']) ? 'bID=' . $_GET['bID'] : '')) . '">' . IMAGE_CANCEL . '</a>'; ?></td>
          </tr>
        </table>

			<div class="sidebar-title mt-2">
              <div class="row">
                <div class="col p-0 mt-3 ml-2 mr-2">
                  <div class="note note-warning m-0">
                    <p class="mb-0 mt-0 f-w-400"><?php echo TEXT_BANNERS_BANNER_NOTE; ?></p>
                  </div>
                </div>
              </div>
            </div>
			      <hr>
            <div class="sidebar-title mt-2">
              <div class="row">
                <div class="col p-0 mt-0 ml-2 mr-2">
                  <div class="note note-info m-0">
                    <p class="mb-0 mt-0 f-w-400"><?php echo TEXT_BANNERS_INSERT_NOTE; ?></p>
                  </div>
                </div>
              </div>
            </div>
			      <hr>
            <div class="sidebar-title mt-2">
              <div class="row">
                <div class="col p-0 mt-0 ml-2 mr-2">
                  <div class="note note-warning m-0">
                    <p class="mb-0 mt-0 f-w-400"><?php echo TEXT_BANNERS_EXPIRCY_NOTE; ?></p>
                  </div>
                </div>
              </div>
            </div>
			      <hr>
            <div class="sidebar-title mt-2">
              <div class="row">
                <div class="col p-0 mt-0 ml-2 mr-2">
                  <div class="note note-info m-0">
                    <p class="mb-0 mt-0 f-w-400"><?php echo TEXT_BANNERS_SCHEDULE_NOTE; ?></p>
                  </div>
                </div>
              </div>
            </div>

      </form>
<?php
  } else {
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <?php
    if ($messageStack->size('bannermanager') > 0) {
      echo $messageStack->output('bannermanager');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-bannermanager" class="table-bannermanager">
        <div class="row">
          <div class="col-md-8 col-xl-9 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left" colspan="2"><?php echo TABLE_HEADING_BANNERS; ?></th>
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_GROUPS; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_STATISTICS; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
<?php
    $banners_query_raw = "select banners_id, banners_title, banners_image, banners_group, status, expires_date, expires_impressions, date_status_change, date_scheduled, date_added from " . TABLE_BANNERS . " order by banners_title, banners_group";
    $banners_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $banners_query_raw, $banners_query_numrows);
    $banners_query = tep_db_query($banners_query_raw);
    while ($banners = tep_db_fetch_array($banners_query)) {
      $info_query = tep_db_query("select sum(banners_shown) as banners_shown, sum(banners_clicked) as banners_clicked from " . TABLE_BANNERS_HISTORY . " where banners_id = '" . (int)$banners['banners_id'] . "'");
      $info = tep_db_fetch_array($info_query);

      if ((!isset($_GET['bID']) || (isset($_GET['bID']) && ($_GET['bID'] == $banners['banners_id']))) && !isset($bInfo) && (substr($action, 0, 3) != 'new')) {
        $bInfo_array = array_merge($banners, $info);
        $bInfo = new objectInfo($bInfo_array);
      }

      $banners_shown = ($info['banners_shown'] != '') ? $info['banners_shown'] : '0';
      $banners_clicked = ($info['banners_clicked'] != '') ? $info['banners_clicked'] : '0';

	  $selected = (isset($bInfo) && is_object($bInfo) && ($banners['banners_id'] == $bInfo->banners_id))? true : false;
	  $col_selected = ($selected) ? ' selected' : '';
      if ($selected) {
        echo '              <tr id="defaultSelected" class="table-row dark selected">' . "\n";
      } else {
        echo '              <tr class="table-row dark">' . "\n";
      }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>" style="width:20px;"><?php echo '<a href="javascript:openpopup(\'' . $banners['banners_id'] . '\')">' . tep_image(DIR_WS_IMAGES . 'icon_popup.gif', 'View Banner') . '</a>'; ?></td>
                <td class="table-col dark text-left<?php echo $col_selected; ?>" onclick="document.location.href='<?php echo tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id']); ?>'"><?php echo $banners['banners_title']; ?></td>
                <td class="table-col dark text-left<?php echo $col_selected; ?>" onclick="document.location.href='<?php echo tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id']); ?>'"><?php echo $banners['banners_group']; ?></td>
                <td class="table-col dark text-center<?php echo $col_selected; ?>" onclick="document.location.href='<?php echo tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id']); ?>'"><?php echo $banners_shown . ' / ' . $banners_clicked; ?></td>
                <td class="table-col dark text-center<?php echo $col_selected; ?>" onclick="document.location.href='<?php echo tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id']); ?>'">
<?php
      if ($banners['status'] == '1') {
        echo '<i class="fa fa-lg fa-check-circle text-success mr-2"></i> <a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id'] . '&action=setflag&flag=0') . '"><i class="fa fa-lg fa-times-circle text-secondary mr-2"></i></a>';
      } else {
        echo '<a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id'] . '&action=setflag&flag=1') . '"><i class="fa fa-lg fa-times-circle text-danger mr-2"></i></a><i class="fa fa-lg fa-check-circle text-secondary mr-2"></i>';
      }
?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo '<a href="' . tep_href_link(FILENAME_BANNER_STATISTICS, 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id']) . '">' . tep_image(DIR_WS_ICONS . 'chart_line.png', ICON_STATISTICS) . '</a>&nbsp;'; if (isset($bInfo) && is_object($bInfo) && ($banners['banners_id'] == $bInfo->banners_id)) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
        </table>
        		<table border="0" width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $banners_split->display_count($banners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_BANNERS); ?></td>
                    <td class="smallText" align="right"><?php echo $banners_split->display_links($banners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                  <tr>
                    <td align="right" colspan="2"><?php echo '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'action=new') . '">' . IMAGE_NEW_BANNER . '</a>'; ?></td>
                  </tr>
                </table>

          </div>
          <div class="col-md-4 col-xl-3 dark panel-right rounded-right">
<?php
  $heading = array();
  $contents = array();
  switch ($action) {
    case 'delete':
      $banner_image = '';
      $heading[] = array('text' => $bInfo->banners_title);

      $contents[] = array('form' => tep_draw_form('banners', FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $bInfo->banners_id . '&action=deleteconfirm'));
      if ($bInfo->banners_image) $banner_image = '<br>' . tep_draw_checkbox_field('delete_image', 'on', true) . ' ' . TEXT_INFO_DELETE_IMAGE;
      $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'. TEXT_INFO_DELETE_INTRO .'<b>' . $bInfo->banners_title . '</b>'. $banner_image .'</p></div></div></div>');
      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-danger btn-sm" type="submit">' . IMAGE_DELETE . '</button><a class="btn btn-default btn-sm mr-2 ml-2" href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $_GET['bID']) . '">' . IMAGE_CANCEL . '</a></div>');
      break;
    default:
      if (is_object($bInfo)) {
        $heading[] = array('text' => $bInfo->banners_title);

        $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-success btn-sm mr-2" href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $bInfo->banners_id . '&action=new') . '">' . IMAGE_EDIT . '</a><a class="btn btn-danger btn-sm mr-2" href="' . tep_href_link(FILENAME_BANNER_MANAGER, 'page=' . $_GET['page'] . '&bID=' . $bInfo->banners_id . '&action=delete') . '">' . IMAGE_DELETE . '</a></div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_BANNERS_DATE_ADDED . '</div><div class="sidebar-title">' . tep_date_short($bInfo->date_added).'</div>');

        if ( (function_exists('imagecreate')) && ($dir_ok) && ($banner_extension) ) {
          $banner_id = $bInfo->banners_id;
          $days = '3';
          include(DIR_WS_INCLUDES . 'graphs/banner_infobox.php');
          $contents[] = array('align' => 'center', 'text' => '<div class="sidebar-text">' . tep_image(DIR_WS_IMAGES . 'graphs/banner_infobox-' . $banner_id . '.' . $banner_extension).'</div>');
        } else {
          include(DIR_WS_FUNCTIONS . 'html_graphs.php');
          $contents[] = array('align' => 'center', 'text' => '<div class="sidebar-text">' . tep_banner_graph_infoBox($bInfo->banners_id, '3') .'</div>');
        }

        $contents[] = array('text' => tep_image(DIR_WS_IMAGES . 'graph_hbar_blue.gif', 'Blue', '5', '5') . ' ' . TEXT_BANNERS_BANNER_VIEWS . '<br>' . tep_image(DIR_WS_IMAGES . 'graph_hbar_red.gif', 'Red', '5', '5') . ' ' . TEXT_BANNERS_BANNER_CLICKS);

        if ($bInfo->date_scheduled) $contents[] = array('text' => '<div class="sidebar-text">' . sprintf(TEXT_BANNERS_SCHEDULED_AT_DATE, tep_date_short($bInfo->date_scheduled)) .'</div>');

        if ($bInfo->expires_date) {
          $contents[] = array('text' => '<div class="sidebar-text">' . sprintf(TEXT_BANNERS_EXPIRES_AT_DATE, tep_date_short($bInfo->expires_date)) .'</div>');
        } elseif ($bInfo->expires_impressions) {
          $contents[] = array('text' => '<div class="sidebar-text">' . sprintf(TEXT_BANNERS_EXPIRES_AT_IMPRESSIONS, $bInfo->expires_impressions) .'</div>');
        }

        if ($bInfo->date_status_change) $contents[] = array('text' => '<div class="sidebar-text">' . sprintf(TEXT_BANNERS_STATUS_CHANGE, tep_date_short($bInfo->date_status_change)).'</div>');
      }
      break;
  }

	  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
		$box = new box;
		echo $box->showSidebar($heading, $contents);
	  }

  }
?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Banner Image </h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
function openpopup(par)
{
    $('.modal-body').load('<?php echo tep_href_link(FILENAME_POPUP_IMAGE, '1=1'); ?>&banner='+par,function(){
        $('#myModal').modal({show:true});
    });
}

$('#calender').datepicker({

	todayHighlight: true
});
$('#calender1').datepicker({

	    todayHighlight: true
});

</script>

<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
