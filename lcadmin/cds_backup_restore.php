<?php
/*
  $Id: cds_backup_restore.php,v 1.1.1.1 2007/01/11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2007 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
$is_62 = (INSTALLED_VERSION_MAJOR == 6 && INSTALLED_VERSION_MINOR == 2) ? true : false;

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'forget':
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key = 'DB_LAST_RESTORE'");
      $messageStack->add_session('search', SUCCESS_LAST_RESTORE_CLEARED, 'success');
      tep_redirect(tep_href_link(FILENAME_CDS_BACKUP_RESTORE));
      break;

    case 'backupnow':
      tep_set_time_limit(0);
      $backup_file = 'cds_db_' . DB_DATABASE . '-' . date('YmdHis') . '.sql';
      $fp = fopen(DIR_FS_BACKUP . $backup_file, 'w');
      $schema = '# CRE Loaded, Commercial Open Source E-Commerce' . "\n" .
                '# http://www.creloaded.com' . "\n" .
                '#' . "\n" .
                '# CDS Database Backup For ' . STORE_NAME . "\n" .
                '# Copyright (c) ' . date('Y') . ' ' . STORE_OWNER . "\n" .
                '#' . "\n" .
                '# Database: ' . DB_DATABASE . "\n" .
                '# Database Server: ' . DB_SERVER . "\n" .
                '#' . "\n" .
                '# Backup Date: ' . date(PHP_DATE_TIME_FORMAT) . "\n\n";
                
      $schema .= 'delete from configuration where configuration_group_id = 480 or configuration_group_id = 481;' . "\n";
      fputs($fp, $schema);
            
      // dump only specific configuration data
      $rows_query = tep_db_query("select * from configuration where configuration_group_id = 480 or configuration_group_id = 481");
      while ($rows = tep_db_fetch_array($rows_query)) {     
        $schema = 'insert into configuration values (';
        reset($rows);
		foreach($rows as $key=>$value) {
          if (!isset($value)) {
            $schema .= 'NULL, ';
          } elseif (tep_not_null($value)) {
            $row = addslashes($value);
            $row = preg_replace("/\n#/", "\n".'\#', $row);
            $schema .= '\'' . $row . '\', ';
          } else {
            $schema .= '\'\', ';
          }
        }
        $schema = preg_replace('/, $/', '', $schema) . ');' . "\n\n";
        fputs($fp, $schema);
      }               
            
      $table_array = array('pages', 'pages_categories', 'pages_categories_description', 'pages_description', 'pages_to_categories');
      $tables_query = tep_db_query('show tables');
      while ($tables = tep_db_fetch_array($tables_query)) {
        $table = current($tables);
        if (!in_array($table, $table_array)) {
          continue;
        }
        $schema = 'drop table if exists ' . $table . ';' . "\n" .
                        'create table ' . $table . ' (' . "\n";

        $table_list = array();
        $fields_query = tep_db_query("show fields from " . $table);
        while ($fields = tep_db_fetch_array($fields_query)) {
          $table_list[] = $fields['Field'];
          $schema .= '  ' . $fields['Field'] . ' ' . $fields['Type'];
          if (strlen($fields['Default']) > 0) {
            if ($fields['Default'] == 'CURRENT_TIMESTAMP') {
              $schema .= ' default ' . $fields['Default'] . '';
            } else {
              $schema .= ' default \'' . $fields['Default'] . '\'';
            }
          }
          if ($fields['Null'] != 'YES') $schema .= ' not null';
          if (isset($fields['Extra'])) $schema .= ' ' . $fields['Extra'];
          $schema .= ',' . "\n";
        }
        $schema = preg_replace("/,\n$/", '', $schema);

        // add the keys
        $index = array();
        $keys_query = tep_db_query("show keys from " . $table);
        while ($keys = tep_db_fetch_array($keys_query)) {
          $kname = $keys['Key_name'];
          if (!isset($index[$kname])) {
            $index[$kname] = array('unique' => !$keys['Non_unique'],
                                              'columns' => array());
          }
          $index[$kname]['columns'][] = $keys['Column_name'];
        }
		foreach($index as $kname=>$info) {
          $schema .= ',' . "\n";
          $columns = implode($info['columns'], ', ');
          if ($kname == 'PRIMARY') {
            $schema .= '  PRIMARY KEY (' . $columns . ')';
          } elseif ($info['unique']) {
            $schema .= '  UNIQUE ' . $kname . ' (' . $columns . ')';
          } else {
            $schema .= '  KEY ' . $kname . ' (' . $columns . ')';
          }
        }
        $schema .= "\n" . ');' . "\n\n";
        fputs($fp, $schema);

        // dump the data
        $rows_query = tep_db_query("select " . implode(',', $table_list) . " from " . $table);
        while ($rows = tep_db_fetch_array($rows_query)) {
          $schema = 'insert into ' . $table . ' (' . implode(', ', $table_list) . ') values (';
          reset($table_list);
		  foreach($table_list as $i) {
            if (!isset($rows[$i])) {
              $schema .= 'NULL, ';
            } elseif (tep_not_null($rows[$i])) {
              $row = addslashes($rows[$i]);
              $row = preg_replace("/\n#/", "\n".'\#', $row);
              $schema .= '\'' . $row . '\', ';
            } else {
              $schema .= '\'\', ';
            }
          }
          $schema = preg_replace('/, $/', '', $schema) . ');' . "\n\n";
          fputs($fp, $schema);
        }         
      }
      fclose($fp);

      if (isset($_POST['download']) && ($_POST['download'] == 'yes')) {
        switch ($_POST['compress']) {
          case 'gzip':
            exec(LOCAL_EXE_GZIP . ' ' . DIR_FS_BACKUP . $backup_file);
            $backup_file .= '.gz';
            break;
          case 'zip':
            exec(LOCAL_EXE_ZIP . ' -j ' . DIR_FS_BACKUP . $backup_file . '.zip ' . DIR_FS_BACKUP . $backup_file);
            unlink(DIR_FS_BACKUP . $backup_file);
            $backup_file .= '.zip';
        }
        header('Content-type: application/x-octet-stream');
        header('Content-disposition: attachment; filename=' . $backup_file);
        readfile(DIR_FS_BACKUP . $backup_file);
        unlink(DIR_FS_BACKUP . $backup_file);
        exit;
      } else {
        switch ($_POST['compress']) {
          case 'gzip':
            exec(LOCAL_EXE_GZIP . ' ' . DIR_FS_BACKUP . $backup_file);
            break;
          case 'zip':
            exec(LOCAL_EXE_ZIP . ' -j ' . DIR_FS_BACKUP . $backup_file . '.zip ' . DIR_FS_BACKUP . $backup_file);
            unlink(DIR_FS_BACKUP . $backup_file);
        }
        $messageStack->add_session('search', SUCCESS_DATABASE_SAVED, 'success');
      }
      tep_redirect(tep_href_link(FILENAME_CDS_BACKUP_RESTORE));
      break;

    case 'restorenow':
    case 'restorelocalnow':
      tep_set_time_limit(0);
      if ($action == 'restorenow') {
        $read_from = $_GET['file'];
        if (file_exists(DIR_FS_BACKUP . $_GET['file'])) {
          $restore_file = DIR_FS_BACKUP . $_GET['file'];
          $extension = substr($_GET['file'], -3);
          if ( ($extension == 'sql') || ($extension == '.gz') || ($extension == 'zip') ) {
            switch ($extension) {
              case 'sql':
                $restore_from = $restore_file;
                $remove_raw = false;
                break;
              case '.gz':
                $restore_from = substr($restore_file, 0, -3);
                exec(LOCAL_EXE_GUNZIP . ' ' . $restore_file . ' -c > ' . $restore_from);
                $remove_raw = true;
                break;
              case 'zip':
                $restore_from = substr($restore_file, 0, -4);
                exec(LOCAL_EXE_UNZIP . ' ' . $restore_file . ' -d ' . DIR_FS_BACKUP);
                $remove_raw = true;
            }
            if (isset($restore_from) && file_exists($restore_from) && (filesize($restore_from) > 15000)) {
              $fd = fopen($restore_from, 'rb');
              $restore_query = fread($fd, filesize($restore_from));
              fclose($fd);
            }
          }
        }
      } elseif ($action == 'restorelocalnow') {
        $sql_file = new upload('sql_file');
        if ($sql_file->parse() == true) {
          $restore_query = fread(fopen($sql_file->tmp_filename, 'r'), filesize($sql_file->tmp_filename));
          $read_from = $sql_file->filename;
        }
      }
      if (isset($restore_query)) {
        $sql_array = array();
        $sql_length = strlen($restore_query);
        $pos = strpos($restore_query, ';');
        for ($i=$pos; $i<$sql_length; $i++) {
          if ($restore_query[0] == '#') {
            $restore_query = ltrim(substr($restore_query, strpos($restore_query, "\n")));
            $sql_length = strlen($restore_query);
            $i = strpos($restore_query, ';')-1;
            continue;
          }
          if ($restore_query[($i+1)] == "\n") {
            for ($j=($i+2); $j<$sql_length; $j++) {
              if (trim($restore_query[$j]) != '') {
                $next = substr($restore_query, $j, 6);
                if ($next[0] == '#') {
                  // find out where the break position is so we can remove this line (#comment line)
                  for ($k=$j; $k<$sql_length; $k++) {
                    if ($restore_query[$k] == "\n") break;
                  }
                  $query = substr($restore_query, 0, $i+1);
                  $restore_query = substr($restore_query, $k);
                  // join the query before the comment appeared, with the rest of the dump
                  $restore_query = $query . $restore_query;
                  $sql_length = strlen($restore_query);
                  $i = strpos($restore_query, ';')-1;
                  continue 2;
                }
                break;
              }
            }
            if ($next == '') { // get the last insert query
              $next = 'insert';
            }
            if ( (preg_match('/create/i', $next)) || (preg_match('/insert/i', $next)) || (preg_match('/drop t/i', $next)) ) {
              $next = '';
              $sql_array[] = substr($restore_query, 0, $i);
              $restore_query = ltrim(substr($restore_query, $i+1));
              $sql_length = strlen($restore_query);
              $i = strpos($restore_query, ';')-1;
            }
          }
        }
        for ($i=0, $n=sizeof($sql_array); $i<$n; $i++) {
          tep_db_query($sql_array[$i]);
        }
        if (isset($remove_raw) && ($remove_raw == true)) {
          unlink($restore_from);
        }
        $messageStack->add_session('search', SUCCESS_DATABASE_RESTORED, 'success');
      }
      tep_redirect(tep_href_link(FILENAME_CDS_BACKUP_RESTORE));
      break;

    case 'download':
      $extension = substr($_GET['file'], -3);
      if ( ($extension == 'zip') || ($extension == '.gz') || ($extension == 'sql') ) {
        if ($fp = fopen(DIR_FS_BACKUP . $_GET['file'], 'rb')) {
          $buffer = fread($fp, filesize(DIR_FS_BACKUP . $_GET['file']));
          fclose($fp);
          header('Content-type: application/x-octet-stream');
          header('Content-disposition: attachment; filename=' . $_GET['file']);
          echo $buffer;
          exit;
        }
      } else {
        $messageStack->add('search', ERROR_DOWNLOAD_LINK_NOT_ACCEPTABLE, 'error');
      }
      break;

    case 'deleteconfirm':
      if (strstr($_GET['file'], '..')) tep_redirect(tep_href_link(FILENAME_CDS_BACKUP_RESTORE));
      tep_remove(DIR_FS_BACKUP . $_GET['file']);
      if (!$tep_remove_error) {
        $messageStack->add_session('search', SUCCESS_BACKUP_DELETED, 'success');
        tep_redirect(tep_href_link(FILENAME_CDS_BACKUP_RESTORE));
      }
      break;
    }
  }

  // check if the backup directory exists
  $dir_ok = false;
  if (is_dir(DIR_FS_BACKUP)) {
    if (is_writeable(DIR_FS_BACKUP)) {
      $dir_ok = true;
    } else {
      $messageStack->add('search', ERROR_BACKUP_DIRECTORY_NOT_WRITEABLE, 'error');
    }
  } else {
    $messageStack->add('search', ERROR_BACKUP_DIRECTORY_DOES_NOT_EXIST, 'error');
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('cdsbackuprestore') > 0) {
      echo $messageStack->output('cdsbackuprestore'); 
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-cdsbackuprestore" class="table-cdsbackuprestore">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_TITLE; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_FILE_DATE; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_FILE_SIZE; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
             </thead>
             <tbody>
              <?php
              if ($dir_ok == true) {
                $dir = dir(DIR_FS_BACKUP);
                $contents = array();
                while ($file = $dir->read()) {
                  if (!is_dir(DIR_FS_BACKUP . $file) && strtolower(substr($file, 0, 4)) == 'cds_') {
                    $contents[] = $file;
                  }
                }
                sort($contents);
                for ($i=0, $n=sizeof($contents); $i<$n; $i++) {
                  $entry = $contents[$i];
                  $check = 0;
                  if ((!isset($_GET['file']) || (isset($_GET['file']) && ($_GET['file'] == $entry))) && !isset($buInfo) && ($action != 'backup') && ($action != 'restorelocal')) {
                    $file_array['file'] = $entry;
                    $file_array['date'] = date(PHP_DATE_TIME_FORMAT, filemtime(DIR_FS_BACKUP . $entry));
                    $file_array['size'] = number_format(filesize(DIR_FS_BACKUP . $entry)) . BYTES;
                    switch (substr($entry, -3)) {
                      case 'zip': $file_array['compression'] = 'ZIP'; break;
                      case '.gz': $file_array['compression'] = 'GZIP'; break;
                      default: $file_array['compression'] = TEXT_NO_EXTENSION; break;
                    }
                    $buInfo = new objectInfo($file_array);
                  }

				  $selected = (isset($buInfo) && is_object($buInfo) && ($entry == $buInfo->file))? true : false;
				  $col_selected = ($selected) ? ' selected' : '';
                  if ($selected) {
                    echo '<tr id="defaultSelected" class="table-row dark selected">' . "\n";
                    $onclick_link = 'file=' . $buInfo->file . '&action=restore';
                  } else {
                     echo '<tr class="table-row dark">' . "\n";
                     $onclick_link = 'file=' . $entry;
                  }
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" onclick="document.location.href='<?php echo tep_href_link(FILENAME_CDS_BACKUP_RESTORE, $onclick_link); ?>'"><?php echo '<a href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'action=download&file=' . $entry) . '"><i class="fa fa-download fa-lg text-success mr-2"></i></a>&nbsp;' . $entry; ?></td>
                  <td class="table-col dark text-center<?php echo $col_selected; ?>" onclick="document.location.href='<?php echo tep_href_link(FILENAME_CDS_BACKUP_RESTORE, $onclick_link); ?>'"><?php echo date(PHP_DATE_TIME_FORMAT, filemtime(DIR_FS_BACKUP . $entry)); ?></td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?>" onclick="document.location.href='<?php echo tep_href_link(FILENAME_CDS_BACKUP_RESTORE, $onclick_link); ?>'"><?php echo number_format(filesize(DIR_FS_BACKUP . $entry)); ?> <!-- bytes --><?php echo BYTES;?></td>
                  <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if (isset($buInfo) && is_object($buInfo) && ($entry == $buInfo->file)) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'file=' . $entry) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
                </tr>
                <?php
                }
                $dir->close();
              }
              ?>
             </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="data-table-foot">
              <tr>
                <td class="sidebar-title" colspan="3"><?php echo TEXT_BACKUP_DIRECTORY . ' ' . DIR_FS_BACKUP; ?></td>
                <td align="right" class="smallText"><?php if ( ($action != 'backup') && (isset($dir)) ) echo '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'action=backup') . '">' . IMAGE_BACKUP . '</a>'; if ( ($action != 'restorelocal') && isset($dir) ) echo '&nbsp;&nbsp;<a class="btn btn-warning btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'action=restorelocal') . '">' . IMAGE_RESTORE . '</a>'; ?></td>
              </tr>
              <?php
              if (defined('DB_LAST_RESTORE')) {
                ?>
                <tr>
                  <td class="sidebar-title" colspan="4"><?php echo TEXT_LAST_RESTORATION . ' ' . DB_LAST_RESTORE . ' <a href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'action=forget') . '">' . TEXT_FORGET . '</a>'; ?></td>
                </tr>
                <?php
              }
              ?>
            </table>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">                
            
            <?php
            $heading = array();
            $contents = array();

            switch ($action) {
              case 'backup':
              $heading[] = array('text' => TEXT_INFO_HEADING_NEW_BACKUP );
              $contents[] = array('form' => tep_draw_form('backup', FILENAME_CDS_BACKUP_RESTORE, 'action=backupnow'));
              $contents[] = array('text' => '<div class="sidebar-title">'.TEXT_INFO_NEW_BACKUP .'</div>');
              $contents[] = array('text' => '<div class="sidebar-title">' . tep_draw_radio_field('compress', 'no', true) . ' ' . TEXT_INFO_USE_NO_COMPRESSION .'</div>');
              if (file_exists(LOCAL_EXE_GZIP)) $contents[] = array('text' => '<div class="sidebar-title">' . tep_draw_radio_field('compress', 'gzip') . ' ' . TEXT_INFO_USE_GZIP .'</div>');
              if (file_exists(LOCAL_EXE_ZIP)) $contents[] = array('text' => '<div class="sidebar-title">'.tep_draw_radio_field('compress', 'zip') . ' ' . TEXT_INFO_USE_ZIP .'</div>');
              if ($dir_ok == true) {
                $contents[] = array('text' => '<div class="sidebar-title">' . tep_draw_checkbox_field('download', 'yes') . ' ' . TEXT_INFO_DOWNLOAD_ONLY . '*</div><div class="text-danger">*' . TEXT_INFO_BEST_THROUGH_HTTPS .'</div>');
              } else {
                $contents[] = array('text' => '<div class="sidebar-title">' . tep_draw_radio_field('download', 'yes', true) . ' ' . TEXT_INFO_DOWNLOAD_ONLY . '*</div><div class="text-danger">*' . TEXT_INFO_BEST_THROUGH_HTTPS .'</div>');
              }
              $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-backup" type="submit">' . IMAGE_BACKUP . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE) . '">' . IMAGE_CANCEL . '</a></div>');
              break;

            case 'restore':
              $heading[] = array('text' => $buInfo->date );
              $contents[] = array('text' => '<div class="sidebar-title">'.tep_break_string(sprintf(TEXT_INFO_RESTORE, DIR_FS_BACKUP . (($buInfo->compression != TEXT_NO_EXTENSION) ? substr($buInfo->file, 0, strrpos($buInfo->file, '.')) : $buInfo->file), ($buInfo->compression != TEXT_NO_EXTENSION) ? TEXT_INFO_UNPACK : ''), 35, ' ').'</div>');
              $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-warning btn-sm mt-2 mb-2 btn-restore" href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'file=' . $buInfo->file . '&action=restorenow') . '">' . IMAGE_RESTORE . '</a><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'file=' . $buInfo->file) . '">' . IMAGE_CANCEL . '</a></div>');
              break;

            case 'restorelocal':
              $heading[] = array('text' => TEXT_INFO_HEADING_RESTORE_LOCAL);
              $contents[] = array('form' => tep_draw_form('restore', FILENAME_CDS_BACKUP_RESTORE, 'action=restorelocalnow', 'post', 'enctype="multipart/form-data"'));
              $contents[] = array('text' => '<div class="sidebar-title">'.TEXT_INFO_RESTORE_LOCAL . '<br><br>' . TEXT_INFO_BEST_THROUGH_HTTPS.'</div>');
              $contents[] = array('text' => '<div class="sidebar-title">' . tep_draw_file_field('sql_file', '', '', 'class="filestyle"').'</div>');
              $contents[] = array('text' => '<div class="sidebar-title">' .TEXT_INFO_RESTORE_LOCAL_RAW_FILE.'</div>');
              $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-warning btn-sm mt-2 mb-2 btn-restore" type="submit">' . IMAGE_RESTORE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE) . '">' . IMAGE_CANCEL . '</a></div>');
              break;

            case 'delete':
              $heading[] = array('text' => $buInfo->date);
              $contents[] = array('form' => tep_draw_form('delete', FILENAME_CDS_BACKUP_RESTORE, 'file=' . $buInfo->file . '&action=deleteconfirm'));
              $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_DELETE_INTRO.'<br><b>' . $buInfo->file . '</b></p></div></div></div>');
			  $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_DELETE . '</button> <a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'file=' . $buInfo->file) . '">' . IMAGE_CANCEL . '</a></div>');
			  break;
			default:
				if (isset($buInfo) && is_object($buInfo)) {
					$heading[] = array('text' => $buInfo->date );
					$contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a class="btn btn-warning btn-sm mr-2" href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'file=' . $buInfo->file . '&action=restore') . '">' . IMAGE_RESTORE . '</a> <a class="btn btn-danger btn-sm mr-2" href="' . tep_href_link(FILENAME_CDS_BACKUP_RESTORE, 'file=' . $buInfo->file . '&action=delete') . '">' . IMAGE_DELETE . '</a></div>');
					$contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_DATE . '</div><div class="sidebar-title">' . $buInfo->date . '</div>');
					$contents[] = array('text' => '<div class="sidebar-text">'.TEXT_INFO_SIZE . '</div><div class="sidebar-title">' . $buInfo->size . '</div>');
					$contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_COMPRESSION . '</div><div class="sidebar-title">' . $buInfo->compression . '</div>');
				}
			  break;
                      }             
			  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
				$box = new box;
				echo $box->showSidebar($heading, $contents);
			  }
		?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
