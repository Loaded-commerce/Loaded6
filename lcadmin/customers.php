<?php
/*
  $Id: customers.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

// RCI for global and individual top
echo $cre_RCI->get('global', 'top', false);
echo $cre_RCI->get('customers', 'top', false);

$action = (isset($_GET['action']) ? $_GET['action'] : '');

$error = false;
$processed = false;

// RCI for adding processes after $_GET['action']
echo $cre_RCI->get('customers', 'process', false);

if(isset($_GET['Customer'])) {
  //$account_query = tep_db_query("select * from " . TABLE_CUSTOMERS . " where customers_id = '" . $_GET['Customer'] . "'");
  $account_query = tep_db_query("select c.*,ab.entry_telephone as customers_telephone from " . TABLE_CUSTOMERS . " c, ".TABLE_ADDRESS_BOOK." ab where c.customers_id = '" . $_GET['Customer'] . "' and c.customers_id = ab.customers_id and ab.address_book_id = c.customers_default_address_id");

  $account = tep_db_fetch_array($account_query);
  $customer = $account['customers_id'];
  $address_query = tep_db_query("select ab.* from " . TABLE_CUSTOMERS . " c, ".TABLE_ADDRESS_BOOK." ab where c.customers_id = '" . $_GET['Customer'] . "' and c.customers_id = ab.customers_id and ab.address_book_id = c.customers_default_address_id");
  $address = tep_db_fetch_array($address_query);
}

if (tep_not_null($action)) {
  switch ($action) {
    case 'update':
      $customers_id = tep_db_prepare_input($_GET['cID']);
      $customers_firstname = tep_db_prepare_input($_POST['customers_firstname']);
      $customers_lastname = tep_db_prepare_input($_POST['customers_lastname']);
      $customers_email_address = strtolower(tep_db_prepare_input($_POST['customers_email_address']));
      $customers_telephone = tep_db_prepare_input($_POST['customers_telephone']);
      $customers_fax = tep_db_prepare_input($_POST['customers_fax']);
      $customers_newsletter = tep_db_prepare_input($_POST['customers_newsletter']);
      $customers_emailvalidated = tep_db_prepare_input($_POST['customers_emailvalidated']);
      $customers_gender = tep_db_prepare_input($_POST['customers_gender']);
      $customers_dob = tep_db_prepare_input($_POST['customers_dob']);
      $customers_voucher_amount = tep_db_prepare_input($_POST['customers_voucher_amount']);
      $customers_selected_template = tep_db_prepare_input($_POST['customers_selected_template']);
      $default_address_id = tep_db_prepare_input($_POST['default_address_id']);
      $entry_street_address = tep_db_prepare_input($_POST['entry_street_address']);
      $entry_suburb = tep_db_prepare_input($_POST['entry_suburb']);
      $entry_postcode = tep_db_prepare_input($_POST['entry_postcode']);
      $entry_city = tep_db_prepare_input($_POST['entry_city']);
      $entry_country_id = tep_db_prepare_input($_POST['entry_country_id']);
      $entry_company = tep_db_prepare_input($_POST['entry_company']);
      $entry_company_tax_id = tep_db_prepare_input($_POST['entry_company_tax_id']);
      $entry_state = tep_db_prepare_input($_POST['entry_state']);
      if (isset($_POST['entry_zone_id'])) $entry_zone_id = tep_db_prepare_input($_POST['entry_zone_id']);
      if (strlen($customers_firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $error = true;
        $entry_firstname_error = true;
      } else {
        $entry_firstname_error = false;
      }
      if (strlen($customers_lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;
        $entry_lastname_error = true;
      } else {
        $entry_lastname_error = false;
      }
      if (ACCOUNT_DOB == 'true') {
        if (checkdate(substr(tep_date_raw($customers_dob), 4, 2), substr(tep_date_raw($customers_dob), 6, 2), substr(tep_date_raw($customers_dob), 0, 4))) {
          $entry_date_of_birth_error = false;
        } else {
          $error = true;
          $entry_date_of_birth_error = true;
        }
      }
      if (strlen($customers_email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
        $error = true;
        $entry_email_address_error = true;
      } else {
        $entry_email_address_error = false;
      }
      if (!tep_validate_email($customers_email_address)) {
        $error = true;
        $entry_email_address_check_error = true;
      } else {
        $entry_email_address_check_error = false;
      }
      if (strlen($entry_street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
        $error = true;
        $entry_street_address_error = true;
      } else {
        $entry_street_address_error = false;
      }
      if (strlen($entry_postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
        $error = true;
        $entry_post_code_error = true;
      } else {
        $entry_post_code_error = false;
      }
      if (strlen($entry_city) < ENTRY_CITY_MIN_LENGTH) {
        $error = true;
        $entry_city_error = true;
      } else {
        $entry_city_error = false;
      }
      if ($entry_country_id == false) {
        $error = true;
        $entry_country_error = true;
      } else {
        $entry_country_error = false;
      }
      if (ACCOUNT_STATE == 'true') {
        if ($entry_country_error == true) {
          $entry_state_error = true;
        } else {
          $zone_id = 0;
          $entry_state_error = false;
          $check_query = tep_db_query("SELECT count(*) as total
                                         from " . TABLE_ZONES . "
                                       WHERE zone_country_id = '" . (int)$entry_country_id . "'");
          $check_value = tep_db_fetch_array($check_query);
          $entry_state_has_zones = ($check_value['total'] > 0);
          if ($entry_state_has_zones == true) {
            $zone_query = tep_db_query("SELECT zone_id
                                          from " . TABLE_ZONES . "
                                        WHERE zone_country_id = '" . (int)$entry_country_id . "'
                                          and zone_name = '" . tep_db_input($entry_state) . "'");
            if (tep_db_num_rows($zone_query) == 1) {
              $zone_values = tep_db_fetch_array($zone_query);
              $entry_zone_id = $zone_values['zone_id'];
            } else {
              $error = true;
              $entry_state_error = true;
            }
          } else {
            if ($entry_state == false) {
              $error = true;
              $entry_state_error = true;
            }
          }
        }
      }
      if (strlen($customers_telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
        $error = true;
        $entry_telephone_error = true;
      } else {
        $entry_telephone_error = false;
      }
      $check_email = tep_db_query("SELECT customers_email_address
                                     from " . TABLE_CUSTOMERS . "
                                   WHERE lower(customers_email_address) = '" . tep_db_input($customers_email_address) . "'
                                     and customers_id != '" . (int)$customers_id . "'");
      if (tep_db_num_rows($check_email)) {
        $error = true;
        $entry_email_address_exists = true;
      } else {
        $entry_email_address_exists = false;
      }
      if ($error == false) {
        $sql_data_array = array('customers_firstname' => $customers_firstname,
                                'customers_lastname' => $customers_lastname,
                                'customers_email_address' => $customers_email_address,
                                'customers_validation' => $customers_emailvalidated,
                                'customers_selected_template' => $customers_selected_template,
                                'customers_newsletter' => $customers_newsletter);
        if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $customers_gender;
        if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($customers_dob);

        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "'");
        tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customers_id . "'");
        if($customers_voucher_amount > 0) {
	        $query = "SELECT * FROM ". TABLE_COUPON_GV_CUSTOMER ." WHERE customer_id='". (int)$customers_id ."'";
	        $rs_voucher = tep_db_query($query);
	        if(tep_db_num_rows($rs_voucher) > 0)
	        	tep_db_query("update ".TABLE_COUPON_GV_CUSTOMER." set amount = " . $customers_voucher_amount . " where customer_id = " . (int)$customers_id . "");
	        else
	        	tep_db_query("INSERT INTO ".TABLE_COUPON_GV_CUSTOMER." set amount = " . $customers_voucher_amount . ", customer_id = '" . (int)$customers_id . "'");

		}

        if ($entry_zone_id > 0) $entry_state = '';
        $sql_data_array = array('entry_firstname' => $customers_firstname,
                                'entry_lastname' => $customers_lastname,
                                'entry_company' => $entry_company,
                                'entry_company_tax_id' => $entry_company_tax_id,
                                'entry_email_address' => $customers_email_address,
                                'entry_telephone' => $customers_telephone,
                                'entry_fax' => $customers_fax,
                                'entry_street_address' => $entry_street_address,
                                'entry_postcode' => $entry_postcode,
                                'entry_city' => $entry_city,
                                'entry_country_id' => $entry_country_id);

        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $entry_suburb;
        if (ACCOUNT_STATE == 'true') {
          if ($entry_zone_id > 0) {
            $sql_data_array['entry_zone_id'] = $entry_zone_id;
            $sql_data_array['entry_state'] = '';
          } else {
            $sql_data_array['entry_zone_id'] = '0';
            $sql_data_array['entry_state'] = $entry_state;
          }
        }
        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "' and address_book_id = '" . (int)$default_address_id . "'");
      } else if ($error == true) {
        $cInfo = new objectInfo($_POST);
        $processed = true;
      }

      // RCI for action update
      echo $cre_RCI->get('customers', 'action', false);

      if ($error !== true) {
        $messageStack->add_session('customers', sprintf(NOTICE_CUSTOMER_UPDATED, $customers_id), 'success');
		$mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
		if ($mode == 'save') {
	        tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers_id));
		} else {  // save & stay
	        tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'action=edit&cID=' . $customers_id));
		}
      }
      break;

    case 'deleteconfirm':
      $customers_id = tep_db_prepare_input($_GET['cID']);
      if (isset($_POST['delete_reviews']) && ($_POST['delete_reviews'] == 'on')) {
        $reviews_query = tep_db_query("select reviews_id from " . TABLE_REVIEWS . " where customers_id = '" . (int)$customers_id . "'");
        while ($reviews = tep_db_fetch_array($reviews_query)) {
          tep_db_query("delete from " . TABLE_REVIEWS_DESCRIPTION . " where reviews_id = '" . (int)$reviews['reviews_id'] . "'");
        }

        tep_db_query("delete from " . TABLE_REVIEWS . " where customers_id = '" . (int)$customers_id . "'");
      } else {
        tep_db_query("update " . TABLE_REVIEWS . " set customers_id = null where customers_id = '" . (int)$customers_id . "'");
      }
      tep_db_query("delete from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where customer_id = '" . (int)$customers_id . "'");

      // RCI for action deleteconfirm
      echo $cre_RCI->get('customers', 'action', false);
      $messageStack->add_session('customers', sprintf(NOTICE_CUSTOMER_DELETE, $customers_id), 'warning');
      tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action'))));
      break;

    default:
      $customers_query = tep_db_query("select c.customers_id, c.customers_gender, c.customers_firstname, c.customers_lastname, c.customers_dob, c.customers_email_address, a.entry_company,a.entry_company_tax_id, a.entry_street_address, a.entry_suburb, a.entry_postcode, a.entry_city, a.entry_state, a.entry_zone_id, a.entry_country_id, a.entry_telephone as customers_telephone , a.entry_fax as customers_fax, c.customers_newsletter, c.customers_default_address_id, c.customers_validation, c.customers_selected_template from " . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " a on c.customers_default_address_id = a.address_book_id where a.customers_id = c.customers_id and c.customers_id = '" . (int)$_GET['cID'] . "'");

      $module_directory = DIR_FS_CATALOG_MODULES . 'payment/';
      $ship_module_directory = DIR_FS_CATALOG_MODULES . 'shipping/';
      $file_extension = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], '.'));
      $directory_array = array();
      if ($dir = @dir($module_directory)) {
      while ($file = $dir->read()) {
        if (!is_dir($module_directory . $file)) {
          if (substr($file, strrpos($file, '.')) == $file_extension) {
            $directory_array[] = $file; // array of all the payment modules present in includes/modules/payment
          }
        }
      }
      sort($directory_array);
      $dir->close();
      }

      $ship_directory_array = array();
      if ($dir = @dir($ship_module_directory)) {
        while ($file = $dir->read()) {
          if (!is_dir($ship_module_directory . $file)) {
            if (substr($file, strrpos($file, '.')) == $file_extension) {
              $ship_directory_array[] = $file; // array of all shipping modules present in includes/modules/shipping
            }
          }
        }
        sort($ship_directory_array);
        $dir->close();
      }
      $customers = tep_db_fetch_array($customers_query);
      $cInfo = new objectInfo($customers);
      $customers_voucher_amount_query = tep_db_query("select * from  ".TABLE_COUPON_GV_CUSTOMER." where customer_id = ".(int)$_GET['cID']."");
      $customers_voucher_amount_res =  tep_db_fetch_array($customers_voucher_amount_query);
      $i_customers_voucher_amount = $customers_voucher_amount_res['amount'];
      $cInfo->customers_voucher_amount = number_format($i_customers_voucher_amount,2);
      // RCI call for action default
      echo $cre_RCI->get('customers', 'action', false);
  } // end switch
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col main-col">
    <?php
    if ($messageStack->size('customers') > 0) {
      echo $messageStack->output('customers');
    }
    ?>
      <!-- begin button bar -->
      <div id="button-bar" class="row">
        <div class="col-9 m-b-10 w-100 pt-1 pl-0 pr-0">
          <?php
          if ($action == 'edit') {
            ?>
            <a href="<?php echo tep_href_link(FILENAME_CUSTOMERS, ((isset($cInfo->customers_id) && $cInfo->customers_id != '') ? 'cID=' . $cInfo->customers_id : '') . ((isset($pID) && $pID != '') ? '&pID=' . $pID : '')); ?>" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> <?php echo ((isset($cID) && empty($cID) === false) ? BUTTON_RETURN_TO_LIST :  IMAGE_CANCEL); ?></a>
            <button type="submit" onclick="updateCustomer('save');" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo BUTTON_SAVE; ?></button>
            <button type="submit" onclick="updateCustomer('stay');" class="btn btn-info m-r-3 btn-save-stay"><i class="fa fa-plus-square"></i> <?php echo BUTTON_SAVE_STAY; ?></button>
            <?php
          }
          ?>
        </div>
        <div class="col-3 m-b-10 pt-1 pr-2"></div>
      </div>
      <!-- end button bar -->

    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-languages" class="table-languages">
        <div class="row">
            <!-- begin panel -->
		<?php
		if ($action == 'edit' || $action == 'update') {

			$newsletter_array = array(array('id' => '0', 'text' => ENTRY_NEWSLETTER_NO),
									  array('id' => '1', 'text' => ENTRY_NEWSLETTER_YES));

			$emailvalidated_array = array(array('id' => '0', 'text' => ENTRY_EMAILVALIDATE_NO),
										  array('id' => '1', 'text' => ENTRY_EMAILVALIDATE_YES));
		?>
          <div class="col-md-7 col-xl-8 dark panel-left rounded-left pb-3">
		<?php
          echo tep_draw_form('customers', FILENAME_CUSTOMERS, tep_get_all_get_params(array('action')) . 'action=update', 'post', 'id="edit_customer" data-parsley-validate') ;
          echo tep_draw_hidden_field('default_address_id', (isset($cInfo->customers_default_address_id) ? $cInfo->customers_default_address_id : ''));
		?>
			<div class="ml-2 mr-2">
			  <div class="main-heading m-t-20"><span><?php echo CATEGORY_PERSONAL; ?></span>
				<div class="main-heading-footer"></div>
			  </div>
            <?php
            if (ACCOUNT_GENDER == 'true') {
              ?>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_GENDER; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                  <?php
                  if ($error == true) {
                  if (isset($entry_gender_error) && $entry_gender_error == true) {
                    echo tep_draw_radio_field('customers_gender', 'm', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('customers_gender', 'f', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . ENTRY_GENDER_ERROR;
                  } else {
                    echo ($cInfo->customers_gender == 'm') ? MALE : FEMALE;
                    echo tep_draw_hidden_field('customers_gender');
                  }
                } else {
                  echo tep_draw_radio_field('customers_gender', 'm', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('customers_gender', 'f', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . FEMALE;
                }
                ?>
				</div>
			  </div>
              <?php
            }
            ?>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_FIRST_NAME; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                <?php
                if ($error == true) {
                  if (isset($entry_firstname_error) && $entry_firstname_error == true) {
                    echo tep_draw_input_field('customers_firstname', $cInfo->customers_firstname, 'maxlength="32" required') . '&nbsp;' . ENTRY_FIRST_NAME_ERROR;
                  } else {
                    echo '<div class="p-t-5">'.$cInfo->customers_firstname . tep_draw_hidden_field('customers_firstname').'</div>';
                  }
                } else {
                  echo tep_draw_input_field('customers_firstname', $cInfo->customers_firstname, 'maxlength="32" required', true);
                }
                ?>
                </div>
			  </div>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_LAST_NAME; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                <?php
                if ($error == true) {
                  if (isset($entry_lastname_error) && $entry_lastname_error == true) {
                    echo tep_draw_input_field('customers_lastname', $cInfo->customers_lastname, 'maxlength="32"') . '&nbsp;' . ENTRY_LAST_NAME_ERROR;
                  } else {
                    echo '<div class="m-t-5">'. $cInfo->customers_lastname . tep_draw_hidden_field('customers_lastname').'</div>';
                  }
                } else {
                  echo tep_draw_input_field('customers_lastname', $cInfo->customers_lastname, 'maxlength="32"', true);
                }
                ?>
                </div>
			  </div>
            <?php
            if (ACCOUNT_DOB == 'true') {
              ?>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_DATE_OF_BIRTH; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                  <?php
                  if ($error == true) {
                    if (isset($entry_date_of_birth_error) && $entry_date_of_birth_error == true) {
                      echo tep_draw_input_field('customers_dob', tep_date_short($cInfo->customers_dob), 'maxlength="10"') . '&nbsp;' . ENTRY_DATE_OF_BIRTH_ERROR;
                    } else {
                      echo '<div class="m-t-5">'. $cInfo->customers_dob . tep_draw_hidden_field('customers_dob') .'</div>';
                    }
                  } else {
                    echo tep_draw_input_field('customers_dob', tep_date_short($cInfo->customers_dob), 'maxlength="10"', true);
                  }
                  ?>
                </div>
			  </div>
              <?php
            }
            ?>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_EMAIL_ADDRESS; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                <?php
                if ($error == true) {
                  if (isset($entry_email_address_error) && $entry_email_address_error == true) {
                    echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR;
                  } elseif ($entry_email_address_check_error == true) {
                    echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_CHECK_ERROR;
                  } elseif ($entry_email_address_exists == true) {
                    echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR_EXISTS;
                  } else {
                    echo '<div class="m-t-5">'. $customers_email_address . tep_draw_hidden_field('customers_email_address') .'</div>';
                  }
                } else {
                  echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96"', true);
                }
                ?>
                </div>
			  </div>
        <?php
        if (ACCOUNT_COMPANY == 'true') {
          ?>
			  <div class="main-heading m-t-20"><span><?php echo CATEGORY_COMPANY; ?></span>
				<div class="main-heading-footer"></div>
			  </div>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_COMPANY; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                  <?php
                  if ($error == true) {
                    if (isset($entry_company_error) && $entry_company_error == true) {
                      echo tep_draw_input_field('entry_company', $cInfo->entry_company, 'maxlength="32"') . '&nbsp;' . ENTRY_COMPANY_ERROR;
                    } else {
                      echo '<div class="m-t-5">'. $cInfo->entry_company . tep_draw_hidden_field('entry_company') .'</div>';
                    }
                  } else {
                    echo tep_draw_input_field('entry_company', $cInfo->entry_company, 'maxlength="32"');
                  }
                  ?>
                </div>
			  </div>

			   <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1">Tax ID</label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php
					if ($error == true) {
					  if (isset($entry_company_error) && $entry_company_error == true) {
						echo tep_draw_input_field('entry_company_tax_id', $cInfo->entry_company_tax_id, 'maxlength="32"') . '&nbsp;' . ENTRY_COMPANY_ERROR;
					  } else {
						echo '<div class="m-t-5">'. $cInfo->entry_company_tax_id . tep_draw_hidden_field('entry_company_tax_id') .'</div>';
					  }
					} else {
					  echo tep_draw_input_field('entry_company_tax_id', $cInfo->entry_company_tax_id, 'maxlength="32"');
					}
					?>
				  </div>
			  </div>

          <?php
        }
        ?>
			  <div class="main-heading m-t-20"><span><?php echo CATEGORY_ADDRESS; ?></span>
				<div class="main-heading-footer"></div>
			  </div>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_STREET_ADDRESS; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                <?php
                if ($error == true) {
                  if (isset($entry_street_address_error) && $entry_street_address_error == true) {
                    echo tep_draw_input_field('entry_street_address', $cInfo->entry_street_address, 'maxlength="64"') . '&nbsp;' . ENTRY_STREET_ADDRESS_ERROR;
                  } else {
                    echo '<div class="m-t-5">'. $cInfo->entry_street_address . tep_draw_hidden_field('entry_street_address') .'</div>';
                  }
                } else {
                  echo tep_draw_input_field('entry_street_address', $cInfo->entry_street_address, 'maxlength="64"', true);
                }
                ?>
                </div>
			  </div>
            <?php
            if (ACCOUNT_SUBURB == 'true') {
              ?>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_SUBURB; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                  <?php
                  if ($error == true) {
                    if (isset($entry_suburb_error) && $entry_suburb_error == true) {
                      echo tep_draw_input_field('suburb', $cInfo->entry_suburb, 'maxlength="32"') . '&nbsp;' . ENTRY_SUBURB_ERROR;
                    } else {
                      echo '<div class="m-t-5">'. $cInfo->entry_suburb . tep_draw_hidden_field('entry_suburb') .'</div>';
                    }
                  } else {
                    echo tep_draw_input_field('entry_suburb', $cInfo->entry_suburb, 'maxlength="32"');
                  }
                  ?>
                </div>
			  </div>
              <?php
            }
            ?>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_POST_CODE; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                <?php
                if ($error == true) {
                  if (isset($entry_post_code_error) && $entry_post_code_error == true) {
                    echo tep_draw_input_field('entry_postcode', $cInfo->entry_postcode, 'maxlength="10"') . '&nbsp;' . ENTRY_POST_CODE_ERROR;
                  } else {
                    echo '<div class="m-t-5">'. $cInfo->entry_postcode . tep_draw_hidden_field('entry_postcode') .'</div>';
                  }
                } else {
                  echo tep_draw_input_field('entry_postcode', $cInfo->entry_postcode, 'maxlength="10"', true);
                }
                ?>
                </div>
			  </div>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_CITY; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                <?php
                if ($error == true) {
                  if (isset($entry_city_error) && $entry_city_error == true) {
                    echo tep_draw_input_field('entry_city', $cInfo->entry_city, 'maxlength="32"') . '&nbsp;' . ENTRY_CITY_ERROR;
                  } else {
                    echo '<div class="m-t-5">'. $cInfo->entry_city . tep_draw_hidden_field('entry_city') .'</div>';
                  }
                } else {
                  echo tep_draw_input_field('entry_city', $cInfo->entry_city, 'maxlength="32"', true);
                }
                ?>
                </div>
			  </div>
            <?php
 			$selectedcountry = isset($_POST['entry_country_id'])?$_POST['entry_country_id']:((isset($cInfo->entry_country_id) && $cInfo->entry_country_id > 0)?$cInfo->entry_country_id:STORE_COUNTRY);
            if (ACCOUNT_STATE == 'true') {
              ?>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_STATE; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input" id="state_id">
                  <?php
						 if (ACCOUNT_STATE == 'true') {
						  $entry_state = tep_get_zone_name($selectedcountry, (isset($cInfo->entry_zone_id) ? $cInfo->entry_zone_id : 0), $cInfo->entry_state);
						  $process = true;
						  $entry_state_has_zones = true;
						  if ($process == true) {
						  $zones_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$selectedcountry . "' and zone_status = 1 order by zone_name");
						  $entry_state_has_zones = (tep_db_num_rows($zones_query) > 0);
							  if ($entry_state_has_zones == true) {
								$zones_array = array();
								$selected_zone = '';
								$selected_zone_code = '';
								$zones_array[] = array('id' => '', 'text' => 'Select State');
								$state = $address['zone_id'];
								$zones_query = tep_db_query("select * from " . TABLE_ZONES . " where zone_country_id = '" . (int)$selectedcountry . "' and zone_status = 1 order by zone_name");
								while ($zones_values = tep_db_fetch_array($zones_query)) {
								  $zones_array[] = array('id' => $zones_values['zone_name'], 'text' => $zones_values['zone_name']);
								   if (strtolower($state) == strtolower($zones_values['zone_code']) || strtolower($state) == strtolower($zones_values['zone_name']) || $state == $zones_values['zone_id']) {
									$selected_zone = $zones_values['zone_name'];
									$selected_zone_code = $zones_values['zone_code'];
								  }
								}
								if ($error == true) {
									echo '<div class="m-t-5">'. $entry_state . tep_draw_hidden_field('entry_zone_id') . tep_draw_hidden_field('entry_state') .'</div>';
								}else{
									echo  tep_draw_pull_down_menu('entry_state', $zones_array, $selected_zone, 'id="state" class="form-control"');
								}
							  } else {
								if ($error == true) {
									echo '<div class="m-t-5">'. $entry_state . tep_draw_hidden_field('entry_zone_id') . tep_draw_hidden_field('entry_state') .'</div>';
								}else{
									echo tep_draw_input_field('entry_state', $entry_state, 'id="state" class="form-control"' );
								}
							  }
							} else {
								if ($error == true) {
									echo '<div class="m-t-5">'. $entry_state . tep_draw_hidden_field('entry_zone_id') . tep_draw_hidden_field('entry_state') .'</div>';
								}else{
							  		echo  tep_draw_input_field('entry_state', $entry_state, 'id="state" class="form-control"');
							  	}
							}
						}
                  ?>
                </div>
			  </div>
              <?php
            }
            ?>
			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_COUNTRY; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                <?php
                if ($error == true) {
                  if ($entry_country_error == true) {
                    echo tep_draw_pull_down_menu('entry_country_id', tep_get_countries(), $selectedcountry) . '&nbsp;' . ENTRY_COUNTRY_ERROR;
                  } else {
                    echo '<div class="m-t-5">'. tep_get_country_name($selectedcountry) . tep_draw_hidden_field('entry_country_id') .'</div>';
                  }
                } else {
                  echo tep_draw_pull_down_menu('entry_country_id', tep_get_countries(), $selectedcountry,'id="countrydropdown" onchange="javascript:change_state(this.value);"');
                }
                ?>
                </div>
			  </div>

			  <div class="main-heading m-t-20"><span><?php echo CATEGORY_CONTACT; ?></span>
				<div class="main-heading-footer"></div>
			  </div>

			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_TELEPHONE_NUMBER; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                <?php
                if ($error == true) {
                  if ($entry_telephone_error == true) {
                    echo tep_draw_input_field('customers_telephone', $cInfo->customers_telephone, 'maxlength="32"') . '&nbsp;' . ENTRY_TELEPHONE_NUMBER_ERROR;
                  } else {
                    echo '<div class="m-t-5">'. $cInfo->customers_telephone . tep_draw_hidden_field('customers_telephone') .'</div>';
                  }
                } else {
                  echo tep_draw_input_field('customers_telephone', $cInfo->customers_telephone, 'maxlength="32"', true);
                }
                ?>
                </div>
			  </div>

			  <div class="form-group row mb-3 mt-3">
				<label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_FAX_NUMBER; ?></label>
				<div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
                <?php
                if ($processed == true) {
                  echo $cInfo->customers_fax . tep_draw_hidden_field('customers_fax');
                } else {
                  echo '<div class="m-t-5">'. tep_draw_input_field('customers_fax', $cInfo->customers_fax, 'maxlength="32"') .'</div>';
                }
                ?>
                </div>
			  </div>
			 <?php
			  // RCI for inserting info inside the form at bottom
			  echo $cre_RCI->get('customers', 'bottominsideform');
			 ?>
        </div>
        <?php
			// RCI for inserting info outside the form at bottom
			echo $cre_RCI->get('customers', 'bottomoutsideform');
            } else {
              ?>
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left pb-3">
              <div class="table-responsive">
                <table id="products-table" class="table table-invoice">
                  <thead>
                    <tr>
                      <?php
                      $listing = isset($_GET['listing']) ? $_GET['listing'] : '';
                      switch ($listing) {
                        case "id-asc":
                          $order = "c.customers_id";
                          break;
                        case "firstname":
                          $order = "c.customers_firstname";
                          break;
                        case "firstname-desc":
                          $order = "c.customers_firstname DESC";
                          break;
                        case "company":
                          $order = "a.entry_company, c.customers_lastname";
                          break;
                        case "company-desc":
                          $order = "a.entry_company DESC,c .customers_lastname DESC";
                          break;
                        case "lastname":
                          $order = "c.customers_lastname, c.customers_firstname";
                          break;
                        case "lastname-desc":
                          $order = "c.customers_lastname DESC, c.customers_firstname";
                          break;
                        default:
                          $order = "c.customers_id DESC";
                      }
                      if (ACCOUNT_COMPANY == 'true') {
                      ?>
                      <td class="th-col dark">
                        <?php echo ENTRY_COMPANY; ?>
                        <a href="<?php echo tep_href_link(FILENAME_CUSTOMERS,'listing=company'); ?>"><span class="fa fa-toggle-up ml-1 text-success"></span>
                        <a href="<?php echo tep_href_link(FILENAME_CUSTOMERS,'listing=company-desc'); ?>"><span class="fa fa-toggle-down ml-1 text-success"></span></a>
                      </td>
                      <?php } ?>
                      <td class="th-col dark">
                        <?php echo TABLE_HEADING_LASTNAME; ?>
                        <a href="<?php echo tep_href_link(FILENAME_CUSTOMERS,'listing=lastname'); ?>"><span class="fa fa-toggle-up ml-1 text-success"></span>
                        <a href="<?php echo tep_href_link(FILENAME_CUSTOMERS,'listing=lastname-desc'); ?>"><span class="fa fa-toggle-down ml-1 text-success"></span></a>
                      </td>
                      <td class="th-col dark">
                        <?php echo TABLE_HEADING_FIRSTNAME; ?>
                        <a href="<?php echo tep_href_link(FILENAME_CUSTOMERS,'listing=firstname'); ?>"><span class="fa fa-toggle-up ml-1 text-success"></span>
                        <a href="<?php echo tep_href_link(FILENAME_CUSTOMERS,'listing=firstname-desc'); ?>"><span class="fa fa-toggle-down ml-1 text-success"></span></a>
                      </td>
                      <td class="th-col dark text-right">
                        <?php echo TABLE_HEADING_ACCOUNT_CREATED; ?>
                        <a href="<?php echo tep_href_link(FILENAME_CUSTOMERS,'listing=id-asc'); ?>"><span class="fa fa-toggle-up ml-1 text-success"></span>
                        <a href="<?php echo tep_href_link(FILENAME_CUSTOMERS,'listing=id-desc'); ?>"><span class="fa fa-toggle-down ml-1 text-success"></span></a>
                      </td>
                      <td class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $search = '';
                    $keywords = isset($_GET['search']) ? tep_db_input(tep_db_prepare_input($_GET['search'])) : '';
                    if (isset($_POST['search'])) {
                      $keywords =  tep_db_input(tep_db_prepare_input($_POST['search']));
                      // this is added to ba able to pass the value along to the next statement
                      $_GET['search'] = $keywords;
                    }
                    if ($keywords != '') {
                      // $search = "where c.customers_lastname like '%" . $keywords . "%' or c.customers_firstname like '%" . $keywords . "%' or c.customers_email_address like '%" . $keywords . "%'";
                      $search = "where c.customers_lastname like '%" . $keywords . "%' or c.customers_firstname like '%" . $keywords . "%' or lower(c.customers_email_address) like '%" . $keywords . "%'  or (a.entry_company) like '%" . $keywords . "%' ";
                    }
                    $customers_query_raw = "select c.customers_id, c.customers_lastname, c.customers_firstname, c.customers_email_address, a.entry_country_id, a.entry_company from " . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " a on c.customers_id = a.customers_id and c.customers_default_address_id = a.address_book_id " . $search . " order by $order";
                    $info = array();
                    $customers = array();
                    $customers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $customers_query_raw, $customers_query_numrows);
                    $customers_query = tep_db_query($customers_query_raw);
                    while ($customers = tep_db_fetch_array($customers_query)) {
                      $info_query = tep_db_query("select customers_info_date_account_created as date_account_created, customers_info_date_account_last_modified as date_account_last_modified, customers_info_date_of_last_logon as date_last_logon, customers_info_number_of_logons as number_of_logons from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . $customers['customers_id'] . "'");
                      $info = tep_db_fetch_array($info_query);
                      if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $customers['customers_id']))) && !isset($cInfo)) {
                        $country_query = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$customers['entry_country_id'] . "'");
                        if ( !$country = tep_db_fetch_array($country_query) ) {
                          $country = array();
                        }
                        $reviews_query = tep_db_query("select count(*) as number_of_reviews from " . TABLE_REVIEWS . " where customers_id = '" . (int)$customers['customers_id'] . "'");
                        if ( !$reviews = tep_db_fetch_array($reviews_query) ) {
                          $reviews = array();
                        }
                        $customer_info = array_merge((array)$country, (array)$info, (array)$reviews);
                        $cInfo_array = array_merge((array)$customers, (array)$customer_info);
                        $cInfo = new objectInfo($cInfo_array);
                      }
                      $selected = (isset($cInfo) && is_object($cInfo) && ($customers['customers_id'] == $cInfo->customers_id)) ? true : false;
                      if ($selected) {
                        echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=edit') . '\'">' . "\n";
                      } else {
                        echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID')) . 'cID=' . $customers['customers_id']) . '\'">' . "\n";
                      }
                      $col_selected = ($selected) ? ' selected' : '';
                      if (ACCOUNT_COMPANY == 'true') {
                      ?>
                      <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php
                        if (strlen($customers['entry_company']) > 16 ) {
                          print ("<acronym title=\"".$customers['entry_company']."\">".substr($customers['entry_company'], 0, 16)."&#160;</acronym>");
                        } else {
                          echo $customers['entry_company'];
                        } ?>
                      </td>
                      <?php } ?>
                      <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php
                        if (strlen($customers['customers_lastname']) > 15 ) {
                          print ("<acronym title=\"".$customers['customers_lastname']."\">".substr($customers['customers_lastname'], 0, 15)."&#160;</acronym>");
                        } else {
                          echo $customers['customers_lastname'];
                        } ?>
                      </td>
                      <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php
                        if (strlen($customers['customers_firstname']) > 15 ) {
                          print ("<acronym title=\"".$customers['customers_firstname']."\">".substr($customers['customers_firstname'], 0, 15)."&#160;</acronym>");
                        } else {
                          echo $customers['customers_firstname'];
                        } ?>
                      </td>
                      <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo tep_date_short($info['date_account_created']); ?></td>
                      <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo '<a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers['customers_id'] . '&action=edit') . '"><i class="fa fa-edit fa-lg text-success"></i></a>'; ?>
                        <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID')) . 'cID=' . $customers['customers_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                      </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>

              <div class="pagination-container">
                <div class="results-right"><?php echo $customers_split->display_links($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'info', 'x', 'y', 'cID'))); ?></div>
                <div class="results-left"><?php echo $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></div>
              </div>
              <?php
              // RCI for inserting code in the bottom of the listing
              echo $cre_RCI->get('customers', 'listingbottom');
              // RCI code eof
            }
            ?>
          </div>
          <?php
			if ($action == 'edit' || $action == 'update')
				echo '<div class="col-md-5 col-xl-4 dark panel-right rounded-right">'."\n";
			else
				echo '<div class="col-md-3 col-xl-2 dark panel-right rounded-right">'."\n";
              $heading = array();
              $contents = array();

              switch ($action) {
                case 'edit':
                case 'update':
                $newsletter_text = '';
                $customers_emailvalidated_text = '';
                if ($processed == true) {
                  $newsletter_text = '<div style="margin-top:8px;">';
                  if ($cInfo->customers_newsletter == '1') {
                    $newsletter_text .= ENTRY_NEWSLETTER_YES;
                  } else {
                    $newsletter_text .= ENTRY_NEWSLETTER_NO;
                  }
                  $newsletter_text .= tep_draw_hidden_field('customers_newsletter') .'</div>';
                } else {
                  $newsletter_text = tep_draw_pull_down_menu('customers_newsletter', $newsletter_array, (($cInfo->customers_newsletter == '1') ? '1' : '0'));
                }
                  if(ACCOUNT_EMAIL_CONFIRMATION=='true')  {
                      $customers_emailvalidated_text = tep_draw_pull_down_menu('customers_emailvalidated', $emailvalidated_array, (isset($cInfo->customers_validation) ? $cInfo->customers_validation : ''));
                  } else {
                      $customers_emailvalidated_text =  TEXT_EMAIL_VALIDATE_FEATURE . tep_draw_hidden_field('customers_emailvalidated', $cInfo->customers_validation);
                  }
		        $returned_rci = $cre_RCI->get('customers', 'dataextension');
                $voucher_text = '';
                if ($processed == true) {
                  $voucher_text = '<div style="margin-top:8px;">$' . $cInfo->customers_voucher_amount . tep_draw_hidden_field('customers_voucher_amount') .'</div>';
                } else {
                  $voucher_text = '<span class="input-group-addon text-white bg-blue-lighter p-5">' . $currencies->get_symbol_left(DEFAULT_CURRENCY) . '</span>'. tep_draw_input_field('customers_voucher_amount', $cInfo->customers_voucher_amount, 'class="form-control"');
                }

              if ($processed != true) {
                $existing_templates_query = tep_db_query("select * from ".TABLE_TEMPLATE." order by template_id");
                $existing_templates_array[] = array("id" => '', "text" => "&#160; NONE &#160;");
                while ($existing_templates =  tep_db_fetch_array($existing_templates_query)) {
                  $existing_templates_array[] = array("id" => $existing_templates['template_name'], "text" => "&#160;".$existing_templates['template_name']."&#160;");
                }
              } // end if ($processed != true )

                $customers_selected_template_text = '';
                if ($processed == true) {
                  $customers_selected_template_text = '<div style="margin-top:8px;">' . (($cInfo->customers_selected_template == '')?'None':'') . tep_draw_hidden_field('customers_selected_template') .'</div>';
                } else {
                  if(tep_not_null($cInfo->customers_selected_template )) {
                    $customers_selected_template_text = tep_draw_pull_down_menu('customers_selected_template', $existing_templates_array, $cInfo->customers_selected_template);
                  } else {
                    $customers_selected_template_text = tep_draw_pull_down_menu('customers_selected_template', $existing_templates_array, '');
                  }
                }

                $customers_leftinsideform = $cre_RCI->get('customers', 'leftinsideform');

                $heading[] = array('text' => CATEGORY_OPTIONS);
                $contents[] = array('text' => '
                  <div class="sidebar-content-container">
                    <div class="form-group row">
                      <label class="col-sm-5 control-label sidebar-edit mt-2 pr-0">' . ENTRY_NEWSLETTER . '</label>
                      <div class="col-sm-7">
                        <div class="input-group">' . $newsletter_text . '</div>
                      </div>
                    </div>
                    '. $returned_rci .'
                    <div class="sidebar-heading mt-3">
                      <span>' . SYSTEM_INFORMATION . '</span>
                    </div><div class="sidebar-heading-footer w-100"></div>
                    <div class="form-group row mt-3">
                      <label class="col-sm-5 control-label sidebar-edit mt-2 pr-0">' . ENTRY_CUSTOMERS_VOUCHER_AMOUNT . '</label>
                      <div class="col-sm-7">
                        <div class="input-group">' . $voucher_text . '</div>
                      </div>
                    </div>
                    <div class="form-group row mt-3">
                      <label class="col-sm-5 control-label sidebar-edit mt-2 pr-0">' . ENTRY_CUSTOMERS_TEMPLATE_NAME . '</label>
                      <div class="col-sm-7">
                        <div class="input-group"> '. $customers_selected_template_text .'</div>
                      </div>
                    </div>'. $customers_leftinsideform);
				if ($cre_RCO->get('customers', 'accessgroup') !== true) {
					$contents[] = array('text' => '
						<div class="form-group row mt-3">
						  <div class="col-sm-12">
						   <div data-toggle="popover" data-placement="top" data-html="true" data-content=\'<div class="text-white">'. TEXT_B2B_UPSELL_POPOVER_BODY .'</div><div class="text-center w-100"><a href="'. TEXT_B2B_UPSELL_GET_B2B_URL .'" target="_blank" class="btn btn-warning btn-sm m-r-5 m-t-10">'. TEXT_B2B_UPSELL_GET_B2B .'</a></div>\'>
							<img src="images/category-access-settings.jpg" alt="Get B2B to unlock this feature.">
						   </div>
						  </div>
						</div>');
				}

                  break;
                case 'confirm':
                  $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_CUSTOMER);
                  $contents[] = array('form' => tep_draw_form('customers', FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=deleteconfirm'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . sprintf(TEXT_DELETE_INTRO, $cInfo->customers_firstname . ' ' . $cInfo->customers_lastname) . '</div></div></div>');
                  if (isset($cInfo->number_of_reviews) && ($cInfo->number_of_reviews) > 0) $contents[] = array('align' => 'center', 'text' => '<div class="col d-sm-inline mt-3 p-0"><label class="control-label ml-3 mr-2 mt-4 main-text">' . sprintf(TEXT_DELETE_REVIEWS, $cInfo->number_of_reviews) . '</label><input type="checkbox" name="delete_reviews" class="js-switch js-default" checked></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button type="button" class="btn btn-default btn-sm mt-2 mb-2 btn-sidebar" onclick="window.location=\'' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id) . '\'">' . IMAGE_CANCEL . '</button><button class="btn btn-danger btn-sm mt-3 mb-4" type="submit">' . IMAGE_CONFIRM_DELETE . '</button>');
                  break;

                default:
                  if (isset($cInfo) && is_object($cInfo)) {
                    $heading[] = array('text' => '[' . $cInfo->customers_id . '] ' . $cInfo->customers_firstname . ' ' . $cInfo->customers_lastname);
                    //RCI customer sidebar buttons top
                    $returned_rci = $cre_RCI->get('customers', 'sidebarbuttons');
                    $_SESSION['adminlogin_customer'] = $cInfo->customers_id;
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-3 mb-2">
                                   <button class="btn btn-primary btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=edit')  . '\'">' . IMAGE_EDIT . '</button>
                                   <button class="btn btn-grey btn-sm mt-2 mb-2 btn-sidebar btn-orders" onclick="window.location=\'' . tep_href_link(FILENAME_ORDERS, 'cID=' . $cInfo->customers_id)  . '\'">' . IMAGE_ORDERS . '</button>
                                   <button class="btn btn-grey btn-sm mt-2 mb-2 btn-sidebar btn-sendemail" onclick="window.location=\'' . tep_href_link(FILENAME_MAIL, 'selected_box=tools&customer=' . $cInfo->customers_email_address) . '\'">' . IMAGE_EMAIL . '</button>
								   <button class="btn btn-grey btn-sm mt-2 mb-2 btn-sidebar btn-createorder" onclick="window.location=\'' . tep_href_link(FILENAME_CREATE_ORDER, 'Customer=' . $cInfo->customers_id)  . '\'">' . IMAGE_BUTTON_CREATE_ORDER . '</button>
                                   <button class="btn btn-grey btn-sm mt-2 mb-2 btn-sidebar btn-loginascustomer" onclick="window.open(\'' . HTTP_SERVER . DIR_WS_CATALOG . 'index.php?rt=core/adminlogin&loginkey='. tep_session_id() . '\', \'_blank\')">' . IMAGE_LOGIN_AS_CUSTOMER . '</button>
									'. ( (ACCOUNT_EMAIL_CONFIRMATION == 'true')?'<button class="btn btn-grey btn-sm mt-2 mb-2 btn-sidebar btn-resendvalidation" onclick="window.location=\'' . tep_href_link(FILENAME_VALIDATE_NEW, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=confirm')  . '\'">' . IMAGE_BUTTON_RESEND_VALIDATION . '</button>':'' ) .'
                                    '.  $returned_rci .'
                                   <button class="btn btn-danger btn-sm mt-2 mb-2 btn-sidebar btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=confirm') . '\'">' . IMAGE_DELETE . '</button>
                                   ');

                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_DATE_ACCOUNT_CREATED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->date_account_created) . '</span></div>');
                    if (tep_not_null($cInfo->date_account_last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_DATE_ACCOUNT_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($cInfo->date_account_last_modified) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_DATE_LAST_LOGON . '<span class="sidebar-title ml-2">'  . tep_date_short($cInfo->date_last_logon) . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_NUMBER_OF_LOGONS . '<span class="sidebar-title ml-2">' . $cInfo->number_of_logons . '</span></div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_INFO_COUNTRY . '<span class="sidebar-title ml-2">' . (isset($cInfo->countries_name) ? $cInfo->countries_name : '') . '</span></div>');
                    //RCI customer sidebar buttons bottom
                    $returned_rci = $cre_RCI->get('customers', 'sidebarbottom');
                    $contents[] = array('text' => $returned_rci);

                    if ((isset($_POST['search']) && tep_not_null($_POST['search'])) || (isset($_GET['search']) && tep_not_null($_GET['search']))) {
                      $contents[] = array('align' => 'center', 'text' => '<div class="mt-3 mb-2">
                                            <button class="btn btn-success mr-1" onclick="window.location=\'' . tep_href_link(FILENAME_CUSTOMERS) . '\'">' . IMAGE_RESET . '</button></div>');
                    }
                  }

              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }

            ?>
          </div>
        </div>
      </div>
    <?php if (isset($action) && ($action == 'edit' || $action == 'update')) {
      ?>
      </form>
      <?php
    }
    ?>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function() {
  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small',
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  });
});

function selectCustomer(id) {
  alert(id);
}
function updateCustomer(mode) {
  var action = '<?php echo tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action')) . 'action=update'); ?>';
  // set the save mode in hidden form input
  $('<input />').attr('type', 'hidden')
      .attr('name', "mode")
      .attr('value', mode)
      .appendTo('#edit_customer');
<?php
echo $lcadmin->print_action_js('updatecustomer');
?>

  $('#edit_customer').attr('action', action).submit();
}
function change_state(country_id) {
	$.get('ajax_common.php?action=getstate&country_id='+country_id, function(data){
		$('#state_id').html(data);
		$('#state_id select').select2();
	});
}
$(document).ready(function() {
    $('#countrydropdown').select2();
    $('#state_id select').select2();
});

<?php
echo $lcadmin->print_js();
?>

</script>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
