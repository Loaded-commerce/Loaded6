<?php
/*
  Id: stats_products_notifications.php,v 1.1 2003/05/16 00:10:05 ft01189 Exp 

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
  
  Contribution by Radu Manole, radu_manole@hotmail.com
  
  
  Added to CRE Loaded 6.2
  
  Last Modified Date : $Date$
  Last Modified By : $Author$
   
*/
  require('includes/application_top.php');

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE ?> </h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">   
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-statsproductspurchased" class="table-statsproductspurchased">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded">


<?php
// show customers for a product
$action = (isset($_GET['action']) ? $_GET['action']: '');
$pID = (isset($_GET['pID']) ? $_GET['pID']: '');
$page = (isset($_GET['page']) ? $_GET['page']: '');
$rows = (isset($rows) ? $rows : '');

if ($action == 'show_customers' && (int)$pID) {
  $products_id = (int)$pID;
?>

		<table class="table table-hover w-100 mt-2">
         <thead>
	      <tr class="th-row">
			<th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_NUMBER; ?></th>
			<th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_NAME; ?></th>
			<th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_EMAIL; ?></th>
			<th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_DATE; ?></th>
		  </tr>
         </thead>
<?php
  $cpage = (isset($_GET['cpage']) ? $_GET['cpage'] : '');
  if ($cpage > 1) $rows = $cpage * MAX_DISPLAY_SEARCH_RESULTS - MAX_DISPLAY_SEARCH_RESULTS;

    $customers_query_raw = "select c.customers_firstname, c.customers_lastname, c.customers_email_address, pn.date_added
                            from " . TABLE_CUSTOMERS . " c, " . TABLE_PRODUCTS_NOTIFICATIONS . " pn 
                            where c.customers_id = pn.customers_id and pn.products_id = '" . $products_id . "' 
                            order by c.customers_firstname, c.customers_lastname";

    $customers_split = new splitPageResults($_GET['cpage'], MAX_DISPLAY_SEARCH_RESULTS, $customers_query_raw, $customers_query_numrows);
    $customers_query = tep_db_query($customers_query_raw);
  
    while ($customers = tep_db_fetch_array($customers_query)) {
      $rows++;

      if (strlen($rows) < 2) {
        $rows = '0' . $rows;
      }
?>
              <tr>
                <td class="table-col dark text-left"><?php echo $rows; ?>.</td>
                <td class="table-col dark text-left"><?php echo $customers['customers_firstname'] . ' ' . $customers['customers_lastname']; ?></td>
                <td class="table-col dark text-left"><?php echo '<a href="' . tep_href_link(FILENAME_MAIL, 'selected_box=tools&customer=' . $customers['customers_email_address'], 'NONSSL') . '">' . $customers['customers_email_address'] . '</a>'; ?>&nbsp;</td>
                <td class="table-col dark text-left"><?php echo $customers['date_added']; ?>&nbsp;</td>
              </tr>
<?php
    }
?>
            </table>
            <table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['cpage'], 'Displaying <b>%s</b> to <b>%s</b> (of <b>%s</b> customers)' , '', 'cpage'); ?></td>
                <td class="smallText" align="right"><?php echo $customers_split->display_links($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['cpage'], tep_get_all_get_params(array('cpage')), 'cpage'); ?></td>
              </tr>
            </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right"><?php echo '<a class="btn btn-success btn-sm mt-2 mb-2" href="' . tep_href_link(FILENAME_STATS_PRODUCTS_NOTIFICATIONS, tep_get_all_get_params(array('action', 'pID', 'cpage'))) . '">' . IMAGE_BACK . '</a>'; ?></td>
        </tr>
      </table>
<?php
// default
} else {

?>
		<table class="table table-hover w-100 mt-2">
         <thead>
	      <tr class="th-row">
			<th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_NUMBER; ?></th>
			<th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_PRODUCTS; ?></th>
			<th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_COUNT; ?>&nbsp;</th>
		  </tr>
<?php
  if ($page > 1) $rows = $page * MAX_DISPLAY_SEARCH_RESULTS - MAX_DISPLAY_SEARCH_RESULTS;

    $products_notifications_query_raw = "select count(pn.products_id) as count_notifications, pn.products_id, pd.products_name 
                                   from " . TABLE_PRODUCTS_NOTIFICATIONS . " pn, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_CUSTOMERS . " c
                                         where pn.products_id = pd.products_id and pd.language_id = '" . $languages_id . "' 
                                         and pn.customers_id = c.customers_id 
                                         group by pn.products_id order by count_notifications desc";
    // fix numrows
    $products_count_query = tep_db_query($products_notifications_query_raw);

    $products_notifications_split = new splitPageResults($page, MAX_DISPLAY_SEARCH_RESULTS, $products_notifications_query_raw,     $products_notifications_query_numrows);     
    $products_notifications_query = tep_db_query($products_notifications_query_raw);
    $products_notifications_query_numrows = tep_db_num_rows($products_count_query);
  
    while ($products = tep_db_fetch_array($products_notifications_query)) {
      $rows++;

      if (strlen($rows) < 2) {
        $rows = '0' . $rows;
      }
?>
              <tr onclick="document.location.href='<?php echo tep_href_link(FILENAME_STATS_PRODUCTS_NOTIFICATIONS, 'action=show_customers&pID=' . $products['products_id'] . '&page=' . $page, 'NONSSL'); ?>'">
                <td class="table-col dark text-left"><?php echo $rows; ?>.</td>
                <td class="table-col dark text-left"><?php echo '<a href="' . tep_href_link(FILENAME_STATS_PRODUCTS_NOTIFICATIONS, 'action=show_customers&pID=' . $products['products_id'] . '&page=' . $page, 'NONSSL') . '">' . $products['products_name'] . '</a>'; ?></td>
                <td class="table-col dark text-center"><?php echo $products['count_notifications']; ?>&nbsp;</td>
              </tr>
<?php
  }
?>
            </table>
            <table border="0" width="100%" cellspacing="0" cellpadding="0" class="data-table-foot">
              <tr>
                <td class="smallText" valign="top"><?php echo $products_notifications_split->display_count($products_notifications_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $page, TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
                <td class="smallText" align="right"><?php echo $products_notifications_split->display_links($products_notifications_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $page); ?></td>
              </tr>
            </table>
<?php
} // end else
?>



			<div class="mb-1">&nbsp;</div>
		 </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>     