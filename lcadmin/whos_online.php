<?php
/*
  $Id: whos_online.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

$xx_mins_ago = (time() - 900);

require('includes/application_top.php');

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

// remove entries that have expired
tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where time_last_click < '" . $xx_mins_ago . "'");

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">     
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-whos-online" class="table-whos-online">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_ONLINE; ?></th>
                  <th scope="col" class="th-col dark text-left d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_CUSTOMER_ID; ?></th>
                  <th scope="col" class="th-col dark text-left d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_FULL_NAME; ?></th>
                  <th scope="col" class="th-col dark text-left d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_IP_ADDRESS; ?></th>
                  <th scope="col" class="th-col dark text-left d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_ENTRY_TIME; ?></th>
                  <th scope="col" class="th-col dark text-left d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_LAST_CLICK; ?></th>
                  <th scope="col" class="th-col dark text-left d-none d-lg-table-cell col-blank"><?php echo TABLE_HEADING_LAST_PAGE_URL; ?></th>
                </tr>
              </thead>
              <tbody>             
                <?php
                $whos_online_query = tep_db_query("select customer_id, full_name, ip_address, time_entry, time_last_click, last_page_url, session_id from " . TABLE_WHOS_ONLINE);
                while ($whos_online = tep_db_fetch_array($whos_online_query)) {
                  $time_online = (time() - $whos_online['time_entry']);
                  if ((!isset($GET['info']) || (isset($GET['info']) && ($GET['info'] == $whos_online['session_id']))) && !isset($info)) {
                    $info = new ObjectInfo($whos_online);
                  }
                  $selected = (isset($info) && ($whos_online['session_id'] == $info->session_id)) ? true : false;
                  if ($selected) {
                    echo '<tr class="table-row dark selected">' . "\n";
                  } else {
                    echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_WHOS_ONLINE, tep_get_all_get_params(array('info', 'action')) . 'info=' . $whos_online['session_id']) . '\'">' . "\n";
                  }
                  $col_selected = ($selected) ? ' selected' : '';                  
                  ?>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo gmdate('H:i:s', $time_online); ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" align="center"><?php echo $whos_online['customer_id']; ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $whos_online['full_name']; ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" align="center"><?php echo $whos_online['ip_address']; ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo date('H:i:s', $whos_online['time_entry']); ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" align="center"><?php echo date('H:i:s', $whos_online['time_last_click']); ?></td>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php if (preg_match('/^(.*)lcsid=[A-Z0-9,-]+[&]*(.*)/i', $whos_online['last_page_url'], $array)) { echo $array[1] . $array[2]; } else { echo $whos_online['last_page_url']; } ?>&nbsp;</td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td class="table-col dark text-left<?php echo $col_selected; ?>" colspan="7"><?php echo sprintf(TEXT_NUMBER_OF_CUSTOMERS, tep_db_num_rows($whos_online_query)); ?></td>
                </tr>
              </tbody>
            </table>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              if (isset($info)) {
                $heading[] = array('text' => TABLE_HEADING_SHOPPING_CART);
                $contents[] = array('text' => '<span class="smallText">&nbsp;</span>');
                if ( $info->customer_id > 0 ) {
                  $products_query = tep_db_query("select cb.customers_basket_quantity, cb.products_id, pd.products_name from " . TABLE_CUSTOMERS_BASKET . " cb, " . TABLE_PRODUCTS_DESCRIPTION . " pd where cb.customers_id = '" . (int)$info->customer_id . "' and cb.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "'");

                  if ( tep_db_num_rows($products_query) ) {
                    $shoppingCart = new shoppingCart();

                    while ( $products = tep_db_fetch_array($products_query) ) {
                      $contents[] = array('text' => '<span class="sidebar-title">' . $products['customers_basket_quantity'] . ' x ' . $products['products_name'] . '</span>');

                      $attributes = array();

                      if ( strpos($products['products_id'], '{') !== false ) {
                        $combos = array();
                        preg_match_all('/(\{[0-9]+\}[0-9]+){1}/', $products['products_id'], $combos);

                        foreach ( $combos[0] as $combo ) {
                          $att = array();
                          preg_match('/\{([0-9]+)\}([0-9]+)/', $combo, $att);

                          $attributes[$att[1]] = $att[2];
                        }
                      }

                      $shoppingCart->add_cart(tep_get_prid($products['products_id']), $products['customers_basket_quantity'], $attributes);
                    }

                    $contents[] = array('text' => '<hr>');
                    $contents[] = array('align' => 'right', 'text'  => '<span class="sidebar-title mb-2 f-s-16">' . TEXT_SHOPPING_CART_SUBTOTAL . ' ' . $currencies->format($shoppingCart->show_total()) . '</span>');
                  } else {
                    $contents[] = array('text' => '&nbsp;');
                  }
                } else {
                  $contents[] = array('text' => 'N/A');
                }
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>