<?php
/*
  $Id: gv_sent.php,v 1.1.1.1 2004/03/04 23:38:36 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 - 2003 osCommerce

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <?php
    if ($messageStack->size('gvsent') > 0) {
      echo $messageStack->output('gvsent');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-gvsent" class="table-gvsent">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_SENDERS_NAME; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_VOUCHER_VALUE; ?></th>
                <th scope="col" class="th-col dark text-center"><?php echo TABLE_HEADING_VOUCHER_CODE; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_DATE_SENT; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
              </thead>
              <tbody>
<?php
  $gv_query_raw = "select et.unique_id, c.coupon_amount, c.coupon_code, c.coupon_id, et.sent_firstname, et.sent_lastname, et.customer_id_sent, et.emailed_to, et.date_sent, c.coupon_id from " . TABLE_COUPONS . " c, " . TABLE_COUPON_EMAIL_TRACK . " et where c.coupon_id = et.coupon_id";
  $gv_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $gv_query_raw, $gv_query_numrows);
  $gv_query = tep_db_query($gv_query_raw);
  $gid = (isset($_GET['gid']) ? $_GET['gid'] : '');
  $uID = (isset($_GET['uID']) ? $_GET['uID'] : '');
  while ($gv_list = tep_db_fetch_array($gv_query)) {
    if ($gid == $gv_list['coupon_id'] && !isset($gInfo)) {
      $gInfo = new objectInfo($gv_list);
    }

	$selected = ((isset($gInfo) && is_object($gInfo)) && ($uID == $gv_list['unique_id']))? true : false;
	$col_selected = ($selected) ? ' selected' : '';
    if ($selected) {
      echo '              <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . tep_href_link('gv_sent.php', tep_get_all_get_params(array('gid', 'action')) . 'gid=' . $gInfo->coupon_id . '&uID=' . $gv_list['unique_id'] . '&action=edit') . '\'">' . "\n";
    } else {
      echo '              <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . tep_href_link('gv_sent.php', tep_get_all_get_params(array('gid', 'action','uID')) . 'gid=' . $gv_list['coupon_id'] . '&uID=' . $gv_list['unique_id']) . '\'">' . "\n";
    }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $gv_list['sent_firstname'] . ' ' . $gv_list['sent_lastname']; ?></td>
                <td class="table-col dark text-center<?php echo $col_selected; ?>"><?php echo $currencies->format($gv_list['coupon_amount']); ?></td>
                <td class="table-col dark text-center<?php echo $col_selected; ?>"><?php echo $gv_list['coupon_code']; ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo tep_date_short($gv_list['date_sent']); ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if ( (is_object($gInfo)) && ($gv_list['coupon_id'] == $gInfo->coupon_id) ) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_GV_SENT, 'page=' . $_GET['page'] . '&gid=' . $gv_list['coupon_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>            </table>
				<table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $gv_split->display_count($gv_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_GIFT_VOUCHERS); ?></td>
                    <td class="smallText" align="right"><?php echo $gv_split->display_links($gv_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table>


          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
<?php
if (isset($gInfo) && is_object($gInfo) && $gInfo->coupon_id > 0) {
  $heading = array();
  $contents = array();
  $heading[] = array('text' => '[' . $gInfo->coupon_id . '] ' . ' ' . $currencies->format($gInfo->coupon_amount));
  $coupon_info_query = tep_db_query("select c.coupon_amount, c.coupon_code, c.coupon_id, et.customer_id_sent, et.emailed_to, et.date_sent, c.coupon_id from " . TABLE_COUPONS . " c, " . TABLE_COUPON_EMAIL_TRACK . " et where c.coupon_id = et.coupon_id and et.unique_id = '" . $uID . "'");
  $coupon_info = tep_db_fetch_array(  $coupon_info_query );
  $redeem_query = tep_db_query("select * from " . TABLE_COUPON_REDEEM_TRACK . " where coupon_id = '" . $coupon_info['coupon_id'] . "' and customer_id = '" . $coupon_info['customer_id_sent'] . "'");
  $redeemed = 'No';
  if (tep_db_num_rows($redeem_query) > 0) $redeemed = 'Yes';
  $contents[] = array('text' => '<div class="sidebar-text">'. TEXT_INFO_SENDERS_ID . '<span class="sidebar-title ml-2">' . $coupon_info['customer_id_sent'] .'</span></div>');
  $contents[] = array('text' => '<div class="sidebar-text">'. TEXT_INFO_AMOUNT_SENT . '<span class="sidebar-title ml-2">' . $currencies->format($coupon_info['coupon_amount']) .'</span></div>');
  $contents[] = array('text' => '<div class="sidebar-text">'. TEXT_INFO_DATE_SENT . '<span class="sidebar-title ml-2">' . tep_date_short($coupon_info['date_sent']) .'</span></div>');
  $contents[] = array('text' => '<div class="sidebar-text">'. TEXT_INFO_VOUCHER_CODE . '<span class="sidebar-title ml-2">' . $coupon_info['coupon_code'] .'</span></div>');
  $contents[] = array('text' => '<div class="sidebar-text">'. TEXT_INFO_EMAIL_ADDRESS . '<span class="sidebar-title ml-2">' . $coupon_info['emailed_to'] .'</span></div>');
  if ($redeemed=='Yes') {
    $redeem = tep_db_fetch_array($redeem_query);
    $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_ORDER_ID . '<span class="sidebar-title ml-2">' . $redeem['order_id'] .'</span></div>');
    $contents[] = array('text' => '<div class="sidebar-text">'. TEXT_INFO_DATE_REDEEMED . '<span class="sidebar-title ml-2">' . tep_date_short($redeem['redeem_date']) .'</span></div>');
    $contents[] = array('text' => '<div class="sidebar-text">'. TEXT_INFO_IP_ADDRESS . '<span class="sidebar-title ml-2">' . $redeem['redeem_ip'] .'</span></div>');
    $contents[] = array('text' => '<div class="sidebar-text">'. TEXT_INFO_CUSTOMERS_ID . '<span class="sidebar-title ml-2">' . $redeem['customer_id'] .'</span></div>');
  } else {
    $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_INFO_NOT_REDEEMED .'</div>');
  }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
	$box = new box;
	echo $box->showSidebar($heading, $contents);
  }
}
?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
