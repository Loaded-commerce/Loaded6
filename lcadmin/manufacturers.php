<?php
/*
  $Id: manufacturers.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
  switch ($action) {
    case 'insert':
    case 'save':
      if (isset($_GET['mID'])) $manufacturers_id = tep_db_prepare_input($_GET['mID']);
      $manufacturers_name = tep_db_prepare_input($_POST['manufacturers_name']);
      $permalink_name = tep_db_prepare_input(tep_db_encoder($_POST['permalink_name']));
      $sql_data_array = array('manufacturers_name' => $manufacturers_name);
      if ($action == 'insert') {
        $insert_sql_data = array('date_added' => 'now()');
        $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
        tep_db_perform(TABLE_MANUFACTURERS, $sql_data_array);
        $manufacturers_id = tep_db_insert_id();
      } elseif ($action == 'save') {
        $update_sql_data = array('last_modified' => 'now()');
        $sql_data_array = array_merge($sql_data_array, $update_sql_data);
        tep_db_perform(TABLE_MANUFACTURERS, $sql_data_array, 'update', "manufacturers_id = '" . (int)$manufacturers_id . "'");
      }
      $manufacturers_image = new upload('manufacturers_image', DIR_FS_CATALOG_IMAGES);
      if (!empty($manufacturers_image->filename)) {
        tep_db_query("update " . TABLE_MANUFACTURERS . " set manufacturers_image = '" . $manufacturers_image->filename . "' where manufacturers_id = '" . (int)$manufacturers_id . "'");
      }
      $languages = tep_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $manufacturers_url_array = $_POST['manufacturers_url'];
        $manufacturers_htc_title_array = $_POST['manufacturers_htc_title_tag'];
        $manufacturers_htc_desc_array = $_POST['manufacturers_htc_desc_tag'];
        $manufacturers_htc_keywords_array = $_POST['manufacturers_htc_keywords_tag'];
        $language_id = $languages[$i]['id'];
        $sql_data_array = array('manufacturers_url' => tep_db_prepare_input($manufacturers_url_array[$language_id]),
                                'manufacturers_htc_title_tag' => tep_db_prepare_input($manufacturers_htc_title_array[$language_id]),
                                'manufacturers_htc_desc_tag' => tep_db_prepare_input($manufacturers_htc_desc_array[$language_id]),
                                'manufacturers_htc_keywords_tag' => tep_db_prepare_input($manufacturers_htc_keywords_array[$language_id])
                               );
        if ($action == 'insert') {
          $insert_sql_data = array('manufacturers_id' => $manufacturers_id,
                                   'languages_id' => $language_id);
          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
          tep_db_perform(TABLE_MANUFACTURERS_INFO, $sql_data_array);
			//checking exist permalink or not
			$checking_existing_permalink = check_manufacture_permalink($permalink_name);
			if($checking_existing_permalink > 0)
			  $permalink_name = $permalink_name.'-'.mt_rand(1, 100);
			 $insert_permalink_data = array('manufacturers_id' => $manufacturers_id,
			                     'language_id' => $language_id,
								 'route' => 'core/index',
								 'permalink_type' => 'manufacturer',
								 'permalink_name' => $permalink_name);
			 tep_db_perform(TABLE_PERMALINK, $insert_permalink_data);

        } elseif ($action == 'save') {
          tep_db_perform(TABLE_MANUFACTURERS_INFO, $sql_data_array, 'update', "manufacturers_id = '" . (int)$manufacturers_id . "' and languages_id = '" . (int)$language_id . "'");

					/*******check and insert or update permalink data start ********/
					$checking_existing_permalink = checking_update_manufacture_permalink($permalink_name,$manufacturers_id);
					if($checking_existing_permalink > 0)
					  $permalink_name = $permalink_name.'-'.mt_rand(1, 100);

				   $check_perma_query = tep_db_query("SELECT * FROM ".TABLE_PERMALINK." WHERE manufacturers_id = '" . (int)$manufacturers_id . "'");
				   if(tep_db_num_rows($check_perma_query) > 0){
					   //updating permalink data
						$update_permalink_data = array('permalink_name' => $permalink_name);
						//tep_db_perform(TABLE_PERMALINK, $update_permalink_data, 'update', "manufacturers_id = '" . (int)$manufacturers_id . "'");

				   }else {
						//inserting permalink data
						$insert_permalink_data = array('manufacturers_id' => $manufacturers_id,
						                               'language_id' => $language_id,
						                          	   'route' => 'core/index',
						                          	   'permalink_type' => 'manufacturer',
													   'permalink_name' => $permalink_name);
						tep_db_perform(TABLE_PERMALINK, $insert_permalink_data);

				   }
				   /*******check and insert or update permalink data end ********/
        }
      }
      if (USE_CACHE == 'true') {
        tep_reset_cache_block('manufacturers');
      }
      tep_redirect(tep_href_link(FILENAME_MANUFACTURERS, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'mID=' . $manufacturers_id));
      break;
    case 'deleteconfirm':
      $manufacturers_id = tep_db_prepare_input($_GET['mID']);
      if (isset($_POST['delete_image']) && ($_POST['delete_image'] == 'on')) {
        $manufacturer_query = tep_db_query("select manufacturers_image from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$manufacturers_id . "'");
        $manufacturer = tep_db_fetch_array($manufacturer_query);
        $image_location = DIR_FS_DOCUMENT_ROOT . DIR_WS_CATALOG_IMAGES . $manufacturer['manufacturers_image'];
        if (file_exists($image_location)) @unlink($image_location);
      }
      tep_db_query("delete from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$manufacturers_id . "'");
      tep_db_query("delete from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturers_id . "'");
      if (isset($_POST['delete_products']) && ($_POST['delete_products'] == 'on')) {
        $products_query = tep_db_query("select products_id from " . TABLE_PRODUCTS . " where manufacturers_id = '" . (int)$manufacturers_id . "'");
        while ($products = tep_db_fetch_array($products_query)) {
          tep_remove_product($products['products_id']);
        }
      } else {
        tep_db_query("update " . TABLE_PRODUCTS . " set manufacturers_id = '' where manufacturers_id = '" . (int)$manufacturers_id . "'");
      }
      if (USE_CACHE == 'true') {
        tep_reset_cache_block('manufacturers');
      }
      tep_redirect(tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page']));
      break;
  }
}

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-languages" class="table-languages">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

            <table class="table table-hover w-100 mt-2">
              <thead>
                <tr class="th-row">
                  <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_MANUFACTURERS; ?></th>
                  <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
                </tr>
              </thead>
              <tbody>
              <?php
              $manufacturers_query_raw = "SELECT m.manufacturers_id, m.manufacturers_name, m.manufacturers_image, m.date_added, m.last_modified,
                                                 mi.manufacturers_htc_title_tag, mi.manufacturers_htc_desc_tag, mi.manufacturers_htc_keywords_tag
                                          FROM " . TABLE_MANUFACTURERS . " m,
                                               " .  TABLE_MANUFACTURERS_INFO . " mi
                                          WHERE m.manufacturers_id = mi.manufacturers_id
                                            and mi.languages_id = " . $languages_id . "
                                          ORDER BY m.manufacturers_name";
              $manufacturers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $manufacturers_query_raw, $manufacturers_query_numrows);
              $manufacturers_query = tep_db_query($manufacturers_query_raw);
              while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
                if ((!isset($_GET['mID']) || (isset($_GET['mID']) && ($_GET['mID'] == $manufacturers['manufacturers_id']))) && !isset($mInfo) && (substr($action, 0, 3) != 'new')) {
                  $manufacturer_products_query = tep_db_query("SELECT count(*) as products_count
                                                                 from " . TABLE_PRODUCTS . "
                                                               WHERE manufacturers_id = '" . (int)$manufacturers['manufacturers_id'] . "'");
                  $manufacturer_products = tep_db_fetch_array($manufacturer_products_query);
                  $mInfo_array = array_merge((array)$manufacturers, (array)$manufacturer_products);
                  $mInfo = new objectInfo($mInfo_array);
                }
                $this_id = (isset($mInfo)) ? $mInfo->manufacturers_id : '';
                $selected = (isset($mInfo) && is_object($mInfo) && ($manufacturers['manufacturers_id'] == $mInfo->manufacturers_id)) ? ' selected' : '';
                if ($selected) {
                  echo '<tr class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $manufacturers['manufacturers_id'] . '&action=edit') . '\'">' . "\n";
                } else {
                  echo '<tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $manufacturers['manufacturers_id']) . '\'">' . "\n";
                }
                $col_selected = ($selected) ? ' selected' : '';
                ?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo $manufacturers['manufacturers_name']; ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>">
                  <?php echo ($selected) ? '<i class="fa fa-long-arrow-right fa-lg text-success"></i>' : '<a href="' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $manufacturers['manufacturers_id']) . '"><i class="fa fa-info-circle fa-lg text-muted"></i></a>'; ?>
                </td>
                </tr>
                <?php
              }
              ?>
              </tbody>
            </table>

            <div class="pagination-container ml-2 mr-2">
              <div class="results-right"><?php echo $manufacturers_split->display_count($manufacturers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS); ?></div>
              <div class="results-left"><?php echo $manufacturers_split->display_links($manufacturers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></div>
            </div>
            <?php
            if (empty($action)) {
              ?>
              <div class="float-right mr-2 mt-0 mb-3" role="group">
                <button class="btn btn-success btn-sm btn-create" onclick="window.location='<?php echo tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . (isset($mInfo->manufacturers_id) ? $mInfo->manufacturers_id : 0) . '&action=new'); ?>'"><?php echo TEXT_CREATE; ?></button>
              </div>
              <?php
            }
            ?>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
            <?php
              $heading = array();
              $contents = array();

              switch ($action) {
                case 'new':
                  $heading[] = array('text' => TEXT_HEADING_NEW_MANUFACTURER);
                  $contents[] = array('form' => tep_draw_form('manufacturers', FILENAME_MANUFACTURERS, 'action=insert', 'post', 'onSubmit="return check_form();" enctype="multipart/form-data"'));

                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_NEW_INTRO . '</p></div></div></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-2">' . TEXT_MANUFACTURERS_NAME . '<span class="required"></span>' . tep_draw_input_field('manufacturers_name', null, 'class="form-control mt-1" id="manufacturers_name" required onblur="javascript:getmanufactureslug(0,this.value);"') . '</div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-2">' . Permalink . '<span class="required"></span>' . tep_draw_input_field('permalink_name', null, 'class="form-control mt-1" required onblur="javascript:checkmanufactureslug(0);" id="permalink"') . '</div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_IMAGE . '</div><input type="file" class="filestyle mt-1" id="manufacturers_image" name="manufacturers_image" />');

                  $manufacturer_inputs_string = '';
                  $languages = tep_get_languages();
                  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                    $manufacturer_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' .
                      tep_draw_input_field('manufacturers_url[' . $languages[$i]['id'] . ']', null, 'class="form-control"') . '</div>';
                    $manufacturer_htc_title_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' .
                      tep_draw_input_field('manufacturers_htc_title_tag[' . $languages[$i]['id'] . ']', null, 'class="form-control"') . '</div>';
                    $manufacturer_htc_desc_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' .
                      tep_draw_input_field('manufacturers_htc_desc_tag[' . $languages[$i]['id'] . ']', null, 'class="form-control"') . '</div>';
                    $manufacturer_htc_keywords_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' .
                      tep_draw_input_field('manufacturers_htc_keywords_tag[' . $languages[$i]['id'] . ']', null, 'class="form-control"') . '</div>';
                  }
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_URL . '</div>' . $manufacturer_inputs_string);
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_HTC_TITLE . '</div>' . $manufacturer_htc_title_inputs_string);
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_HTC_DESC . '</div>' . $manufacturer_htc_desc_inputs_string);
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_HTC_KEYWORDS . '</div>' . $manufacturer_htc_keywords_inputs_string);
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE . '</button><button class="btn btn-grey btn-sm mt-2 mb-2 mr-2 btn-cancel" type="button" onclick="window.location=\'' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $_GET['mID']) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'edit':
                  $heading[] = array('text' => TEXT_HEADING_EDIT_MANUFACTURER);
                  $contents[] = array('form' => tep_draw_form('manufacturers', FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=save', 'post', 'onSubmit="return check_form();" enctype="multipart/form-data" data-parsley-validate'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-info m-0"><p class="mb-0 mt-0 f-w-400">' . TEXT_EDIT_INTRO . '</p></div></div></div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-2">' . TEXT_MANUFACTURERS_NAME . '<span class="required"></span>' . tep_draw_input_field('manufacturers_name', $mInfo->manufacturers_name, 'class="form-control mt-1" id="manufacturers_name" required onblur="javascript:getmanufactureslug('.$this_id.',this.value);"') . '</div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-2">' . Permalink . '<span class="required"></span>' . tep_draw_input_field('permalink_name',((isset($_GET['mID']))?(tep_manufacture_permalink_name($this_id)): ''),'class="form-control mt-1" required onblur="javascript:checkmanufactureslug('.$this_id.');" id="permalink"') . '</div>');
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_IMAGE . '</div><input type="file" class="filestyle mt-1" id="manufacturers_image" name="manufacturers_image" />');

                  $manufacturer_inputs_string = '';
                  $languages = tep_get_languages();
                  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                    $manufacturer_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' .
                      tep_draw_input_field('manufacturers_url[' . $languages[$i]['id'] . ']', tep_get_manufacturer_url($mInfo->manufacturers_id, $languages[$i]['id']), 'class="form-control"') . '</div>';
                    $manufacturer_htc_title_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' .
                      tep_draw_input_field('manufacturers_htc_title_tag[' . $languages[$i]['id'] . ']', tep_get_manufacturer_htc_title($mInfo->manufacturers_id, $languages[$i]['id']), 'class="form-control"') . '</div>';
                    $manufacturer_htc_desc_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' .
                      tep_draw_input_field('manufacturers_htc_desc_tag[' . $languages[$i]['id'] . ']', tep_get_manufacturer_htc_desc($mInfo->manufacturers_id, $languages[$i]['id']), 'class="form-control"') . '</div>';
                    $manufacturer_htc_keywords_inputs_string .= '<div class="input-group mb-3">
                      <span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' .
                      tep_draw_input_field('manufacturers_htc_keywords_tag[' . $languages[$i]['id'] . ']', tep_get_manufacturer_htc_keywords($mInfo->manufacturers_id, $languages[$i]['id']), 'class="form-control"') . '</div>';
                  }
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_URL . '</div>' . $manufacturer_inputs_string);
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_HTC_TITLE . '</div>' . $manufacturer_htc_title_inputs_string);
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_HTC_DESC . '</div>' . $manufacturer_htc_desc_inputs_string);
                  $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-1">' . TEXT_MANUFACTURERS_HTC_KEYWORDS . '</div>' . $manufacturer_htc_keywords_inputs_string);
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE . '</button><button class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" type="button" onclick="window.location=\'' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $mInfo->manufacturers_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                case 'delete':
                  $heading[] = array('text' => TEXT_HEADING_DELETE_MANUFACTURER);
                  $contents[] = array('form' => tep_draw_form('manufacturers', FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=deleteconfirm'));
                  $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . sprintf(TEXT_DELETE_INTRO, $mInfo->manufacturers_name) . '</p></div></div></div>');
                  if ($mInfo->products_count > 0) {
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . sprintf(TEXT_DELETE_WARNING_PRODUCTS, $mInfo->products_count) . '</p></div></div></div>');
                    $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0 f-w-400">' . tep_draw_checkbox_field('delete_products', null, null, null, 'class="js-switch js-switch-small"') . ' ' . TEXT_DELETE_PRODUCTS . '</p></div></div></div>');
                  }
                  $contents[] = array('text' => '<div class="col d-sm-inline p-0"><label class="control-label mt-3 ml-3 mr-2 mb-3 main-text">' . TEXT_DELETE_IMAGE . '</label><input type="checkbox" name="delete_image" class="js-switch" checked></div>');
                  $contents[] = array('align' => 'center', 'text' => '<button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_CONFIRM_DELETE . '</button><button type="button" class="btn btn-grey btn-sm mr-2 mt-2 mb-2 btn-cancel" onclick="window.location=\'' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $mInfo->manufacturers_id) . '\'">' . IMAGE_CANCEL . '</button>');
                  break;
                default:
                  if (isset($mInfo) && is_object($mInfo)) {
                    $heading[] = array('text' => $mInfo->manufacturers_name);
                    $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2">
                                    <button class="btn btn-success btn-sm mt-2 mb-2 btn-edit" onclick="window.location=\'' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=edit')  . '\'">' . IMAGE_EDIT . '</button>
                                    <button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" onclick="window.location=\'' . tep_href_link(FILENAME_MANUFACTURERS, 'page=' . $_GET['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=delete') . '\'">' . IMAGE_DELETE . '</button>');
                    $returned_rci = $cre_RCI->get('manufacturers', 'msidebarbuttons');
                    $contents[] = array('align' => 'center', 'text' => $returned_rci);
                    $contents[] = array('text' => '<div class="sidebar-text mt-3">' . TEXT_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($mInfo->date_added) . '</span></div>');
                    if (tep_not_null($mInfo->last_modified)) $contents[] = array('text' => '<div class="sidebar-text mt-1">' . TEXT_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($mInfo->last_modified) . '</span></div>');
                    $contents[] = array('align' => 'center', 'text' => '<div class="sidebar-img mt-3 well ml-4 mr-4 mb-1">' . tep_info_image($mInfo->manufacturers_image, $mInfo->manufacturers_name,100,100) . '</div><div class="sidebar-text mb-0 mt-0">' . $cInfo->categories_image . '</div>');
                    $contents[] = array('text' => '<div class="sidebar-text mt-3 mb-3">' . TEXT_PRODUCTS . '<span class="sidebar-title ml-2">' . $mInfo->products_count . '</span></div>');
                  }
                  break;
              }

              // use $box->showSidebar()
              if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                $box = new box;
                echo $box->showSidebar($heading, $contents);
              }
            ?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<script>
$(document).ready(function(){

  // fade any error messages
  $('.alert-error').delay(6000).fadeOut('slow');

  //  instantiate checkbox switches
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  elems.forEach(function(html) {
    var switchery = new Switchery(html, { size: 'small',
                                          color: '#ff4044',
                                          secondaryColor: '#a8acb1' });
  });
});
function getmanufactureslug(ID, MANUFACTURE_NAME){
    if(MANUFACTURE_NAME != ''){
	var permalink_name = $('#permalink').val();
	 var manufacturers_name = MANUFACTURE_NAME.replace(/&/g, "and");
	  params = 'action=get_slug&type=manufacturer&id='+ID+'&name=' + manufacturers_name +'&pname='+permalink_name;
		$.ajax({
			type: 'post',
			url: '<?php echo tep_href_link("ajax_common.php");?>',
			data: params,
			success: function (retval) {
				//alert('#permalink_name['+LID+']');
				//alert (retval);
				if(retval == 'error'){
				}else{
				  $('#permalink').val(retval);
			   }
			}
		});
	}
}
function checkmanufactureslug(ID){
	  var permalink_name = $('#permalink').val();
		  params = 'action=check_slug&type=manufacturer&id='+ID+'&name=' + permalink_name;
			$.ajax({
				type: 'post',
				url: '<?php echo tep_href_link("ajax_common.php");?>',
				data: params,
				success: function (retval) {
					if(retval == 'error'){
						$('#permalink_error').html('Duplicate permalink please add a unique permalink!');
						$('#permalink').css({border:'1px solid #FF0000', color:'#FF0000'});
						//$('#permalink').focus();
						$('#validate').val('0');
					}else{
					  $('#permalink').val(retval);
					  $('#permalink').css({border:'1px solid #277421', color:'#999999'});
					  $('#permalink_error').css('display','none');
					  $('#validate').val('1');
				   }
				}
			});
}
</script>
<!-- body_eof //-->
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>