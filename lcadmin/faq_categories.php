<?php
/*
  $Id: faq_categories.php,v 1.1.1.1 2004/03/04 23:38:42 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

// define functions
require(DIR_WS_FUNCTIONS . 'faq.php');

// RCI code start
echo $cre_RCI->get('global', 'top', false);
echo $cre_RCI->get('faqcategories', 'top', false);
// RCI code eof

// clean variables
$cID = '';
if (isset($_POST['cID']) && tep_not_null($_POST['cID'])) {
  $cID = (int)$_POST['cID'];
} elseif (isset($_GET['cID']) && tep_not_null($_GET['cID'])) {
  $cID = (int)$_GET['cID'];
}

$action = '';
if (isset($_POST['action']) && tep_not_null($_POST['action'])) {
  $action = tep_db_prepare_input($_POST['action']);
} elseif (isset($_GET['action']) && tep_not_null($_GET['action'])) {
  $action = tep_db_prepare_input($_GET['action']);
}

$error = false;
$processed = false;

switch ($action) {
 case 'setflag':
   $status = tep_db_prepare_input($_GET['flag']);

   if ($status == '1') {
     tep_db_query("update " . TABLE_FAQ_CATEGORIES . " set categories_status = '1' where categories_id = '" . (int)$cID . "'");
   } elseif ($status == '0') {
     tep_db_query("update " . TABLE_FAQ_CATEGORIES . " set categories_status = '0' where categories_id = '" . (int)$cID . "'");
   }
	echo $status;exit;
   tep_redirect(tep_href_link(FILENAME_FAQ_CATEGORIES, '&cID=' . $cID));
   break;
 case 'insert':
 case 'update':
   $categories_sort_order = tep_db_prepare_input($_POST['categories_sort_order']);
   $categories_status = ((tep_db_prepare_input($_POST['categories_status']) == 'on') ? '1' : '0');
   $categories_fav_icon = ((tep_db_prepare_input($_POST['categories_image']) == '') ? '1' : '0');


   $sql_data_array = array('categories_sort_order' => $categories_sort_order,
         'categories_status' => $categories_status);

   if ($action == 'insert') {
     $insert_sql_data = array('categories_date_added' => 'now()');

     $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

     tep_db_perform(TABLE_FAQ_CATEGORIES, $sql_data_array);

     $cID = tep_db_insert_id();
   } elseif ($action == 'update') {
     $update_sql_data = array('categories_last_modified' => 'now()');

     $sql_data_array = array_merge($sql_data_array, $update_sql_data);

     tep_db_perform(TABLE_FAQ_CATEGORIES, $sql_data_array, 'update', "categories_id = '" . (int)$cID . "'");
   }

   $languages = tep_get_languages();
   for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
     $categories_name_array = $_POST['categories_name'];
     $categories_description_array = $_POST['categories_description'];

     $language_id = $languages[$i]['id'];

     $sql_data_array = array('categories_name' => tep_db_prepare_input($categories_name_array[$language_id]),
           'categories_description' => tep_db_prepare_input($categories_description_array[$language_id]));

     if ($action == 'insert') {
       $insert_sql_data = array('categories_id' => $cID,
        'language_id' => $languages[$i]['id']);

       $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

       tep_db_perform(TABLE_FAQ_CATEGORIES_DESCRIPTION, $sql_data_array);
     } elseif ($action == 'update') {
       tep_db_perform(TABLE_FAQ_CATEGORIES_DESCRIPTION, $sql_data_array, 'update', "categories_id = '" . (int)$cID . "' and language_id = '" . (int)$languages[$i]['id'] . "'");
     }
   }
   if ($categories_image = new upload('categories_image', DIR_FS_CATALOG_OTHERS)) {
      $categories_fav_icon = (($categories_image->filename == '') ? '0' : '1');
      //echo $categories_fav_icon;exit;
     tep_db_query("update " . TABLE_FAQ_CATEGORIES . " set categories_image = '" . tep_db_input($categories_image->filename) . "'  where categories_id = '" . (int)$cID . "'");
   }
	sync_faq_categories();

	// RCI for action insert
	echo $cre_RCI->get('faq', 'cataction', false);

   tep_redirect(tep_href_link(FILENAME_FAQ_CATEGORIES, '&cID=' . $cID));
   break;
 case 'delete_confirm':
   if (tep_not_null($cID)) {
     $faq_ids_query = tep_db_query("select faq_id from " . TABLE_FAQ_TO_CATEGORIES . " where categories_id = '" . (int)$cID . "'");

     while ($faq_ids = tep_db_fetch_array($faq_ids_query)) {
       tep_faq_remove_faq($faq_ids['faq_id']);
     }

     tep_faq_remove_category($cID);
   }
	// RCI for action insert
	echo $cre_RCI->get('faq', 'cataction', false);

   tep_redirect(tep_href_link(FILENAME_FAQ_CATEGORIES));
   break;
}


include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

      <div class="row mr-3">
        <div class="col-9"></div>
        <div class="col-3 pr-0">
          <?php echo tep_draw_form('goto', FILENAME_FAQ_CATEGORIES, '', 'get');?>
           <div class="form-group row mb-2 pr-0">
            <label for="cPath" class="hidden-xs col-sm-3 col-form-label text-center m-t-10 pr-0"><?php echo HEADING_TITLE_SEARCH; ?></label>
            <div class="col-sm-9 p-0 dark rounded">
              <?php echo tep_draw_input_field('search'); ?>
            </div>
          </div>
          <?php
          if (isset($_GET[tep_session_name()])) {
            echo tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]);
          }
          echo '</form>';
          ?>
        </div>
      </div>

  <div class="col">
    <?php
    if ($messageStack->size('faqcategories') > 0) {
      echo $messageStack->output('faqcategories');
    }
    ?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-faqcategories" class="table-faqcategories">
        <div class="row">
          <div class="col-md-9 col-xl-10 dark panel-left rounded-left">
            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_NAME; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_STATUS; ?></th>
                <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?></th>
              </tr>
             <thead>
<?php
    $search = '';
    if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
      $keywords = tep_db_input(tep_db_prepare_input($_GET['search']));
      $search = " and icd.categories_name like '%" . $keywords . "%'";

      $categories_query_raw = "select ic.categories_id, ic.categories_image, ic.categories_status, ic.categories_sort_order, ic.categories_date_added, ic.categories_last_modified, icd.categories_name, icd.categories_description from " . TABLE_FAQ_CATEGORIES . " ic left join " . TABLE_FAQ_CATEGORIES_DESCRIPTION . " icd on ic.categories_id = icd.categories_id where icd.language_id = '" . (int)$languages_id . "'" . $search . " order by ic.categories_sort_order, icd.categories_name";
    } else {
      $categories_query_raw = "select ic.categories_id, ic.categories_image, ic.categories_status, ic.categories_sort_order, ic.categories_date_added, ic.categories_last_modified, icd.categories_name, icd.categories_description from " . TABLE_FAQ_CATEGORIES . " ic left join " . TABLE_FAQ_CATEGORIES_DESCRIPTION . " icd on ic.categories_id = icd.categories_id where icd.language_id = '" . (int)$languages_id . "' order by ic.categories_sort_order, icd.categories_name";
    }

    $categories_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $categories_query_raw, $categories_query_numrows);
    $categories_query = tep_db_query($categories_query_raw);
    while ($categories = tep_db_fetch_array($categories_query)) {
      if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $categories['categories_id']))) && !isset($cInfo)) {
        $faq_count_query = tep_db_query("select count(*) as categories_faq_count from " . TABLE_FAQ_TO_CATEGORIES . " where categories_id = '" . (int)$categories['categories_id'] . "'");
        $faq_count = tep_db_fetch_array($faq_count_query);

        $cInfo_array = array_merge($categories, $faq_count);
        $cInfo = new objectInfo($cInfo_array);
      }

		$selected = (isset($cInfo) && is_object($cInfo) && ($categories['categories_id'] == $cInfo->categories_id))? true : false;
		$col_selected = ($selected) ? ' selected' : '';
      if ($selected) {
        echo '          <tr  class="table-row dark selected" id="crow_'.$categories['categories_id'].'">' . "\n";
        $onclick = ' onclick="document.location.href=\'' . tep_href_link(FILENAME_FAQ_CATEGORIES, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->categories_id . '&action=edit') . '\'"';

      } else {
        echo '          <tr class="table-row dark" id="crow_'.$categories['categories_id'].'">' . "\n";
        $onclick = 'onclick="document.location.href=\'' . tep_href_link(FILENAME_FAQ_CATEGORIES, tep_get_all_get_params(array('cID')) . 'cID=' . $categories['categories_id']) . '\'"';
      }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>" <?php echo $onclick;?>><?php echo $categories['categories_name']; ?></td>
                <td class="setflag table-col dark text-right<?php echo $col_selected; ?>">
				<?php
				  $ajax_link = tep_href_link(FILENAME_FAQ_CATEGORIES);
				  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=0&cID='.$categories['categories_id'].'\', '.$categories['categories_id'].',0 )" '.(($categories['categories_status'] == 1)? 'style="display:block"':'style="display:none"').' class="sactive"><i class="fa fa-lg fa-check-circle text-success"></i></a>';
				  echo '<a href="javascript:void(0);" onclick="javascript:setStatus(\'setflag\',\''.$ajax_link.'\',\'action=setflag&flag=1&cID='.$categories['categories_id'].'\', '.$categories['categories_id'].',1  )" '.(($categories['categories_status'] == 0)? 'style="display:block"':'style="display:none"').' class="sdeactive"><i class="fa fa-lg fa-times-circle text-danger"></i></a>';
				?>
				</td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if (isset($cInfo) && is_object($cInfo) && ($categories['categories_id'] == $cInfo->categories_id)) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>'; } else { echo '<a href="' . tep_href_link(FILENAME_FAQ_CATEGORIES, tep_get_all_get_params(array('cID')) . 'cID=' . $categories['categories_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>            </table>
            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $categories_split->display_count($categories_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_FAQ_CATEGORIES); ?></td>
                    <td class="smallText" align="right"><?php echo $categories_split->display_links($categories_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'pages', 'x', 'y', 'cID'))); ?></td>
                  </tr>
                  <tr>
                    <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <?php
                    // RCI code start
                    echo $cre_RCI->get('faqcategories', 'listingbottom');
                    // RCI code eof
                    ?>
                  </tr>
                </table></td>
              </tr>
                  <tr>
<?php
    if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
?>
                    <td align="right" colspan="2"><?php echo '<a class="btn btn-default btn-sm mr-2" href="' . tep_href_link(FILENAME_FAQ_CATEGORIES) . '">' . IMAGE_RESET . '</a><a class="btn btn-success btn-sm mr-2" href="' . tep_href_link(FILENAME_FAQ_CATEGORIES, 'page=' . $_GET['page'] . '&action=new') . '">' . IMAGE_NEW_CATEGORY . '</a>'; ?></td>
<?php
    } else {
?>
                    <td align="right" colspan="2"><?php echo '<a class="btn btn-success btn-sm mr-2" href="' . tep_href_link(FILENAME_FAQ_CATEGORIES, 'page=' . $_GET['page'] . '&action=new') . '">' . IMAGE_NEW_CATEGORY . '</a>'; ?></td>
<?php
    }
?>
                  </tr>
                </table>
          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' =>  TEXT_FAQ_HEADING_NEW_FAQ_CATEGORY);
      $contents[] = array('form' => tep_draw_form('categories_new', FILENAME_FAQ_CATEGORIES, 'action=insert', 'post', 'id="form_faq" enctype="multipart/form-data" onsubmit="javascript: return faq_checkform()"'));
      $contents[] = array('text' => '<div class="sidebar-title">'.TEXT_NEW_FAQ_CATEGORIES_INTRO.'</div>');
      $category_inputs_string = '';
      $languages = tep_get_languages();
      for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
		$category_inputs_string .= '<div class="input-group mb-2"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
        $category_inputs_string .= tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']');
		$category_inputs_string .= '</div>' ;
      }
      $category_description_inputs_string = '';
      for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
		$category_description_inputs_string .= '<div class="input-group mb-2"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
        $category_description_inputs_string .= tep_draw_textarea_field('categories_description[' . $languages[$i]['id'] . ']', 'soft', '40', '5', '', 'class="form-control"');
		$category_description_inputs_string .= '</div>' ;
      }
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_NAME . '</div><div>'. $category_inputs_string.'</div>');
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_DESCRIPTION . '</div><div>' . $category_description_inputs_string .'</div>');
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_IMAGE . '</div><div>' . tep_draw_file_field('categories_image', '', '', 'class="filestyle"') .'</div>');
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_SORT_ORDER . '</div><div>' . tep_draw_input_field('categories_sort_order', '', 'size="2"') .'</div>');
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_STATUS.'</div><div>'.tep_draw_radio_field('categories_status', 'on', true) .' '. TEXT_FAQ_CATEGORIES_STATUS_ENABLE.' '.tep_draw_radio_field('categories_status', 'off') .' '. TEXT_FAQ_CATEGORIES_STATUS_DISABLE.'</div>');

	  if ($cre_RCO->get('faq', 'cataccessgroup') !== true) {
      	$contents[] = array('text' => '
      		<div class="form-group row mt-3">
			  <div class="col-sm-12">
			   <div data-toggle="popover" data-placement="top" data-html="true" data-content=\'<div class="text-white">'. TEXT_B2B_UPSELL_POPOVER_BODY .'</div><div class="text-center w-100"><a href="'. TEXT_B2B_UPSELL_GET_B2B_URL .'" target="_blank" class="btn btn-warning btn-sm m-r-5 m-t-10">'. TEXT_B2B_UPSELL_GET_B2B .'</a></div>\'>
				<img src="images/category-access-settings.jpg" alt="Get B2B to unlock this feature.">
			   </div>
			  </div>
			</div>');
	  }

      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE . '</button><a class="btn btn-default btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_FAQ_CATEGORIES) . '">' . IMAGE_CANCEL . '</a></div>');
      break;
    case 'edit':
      $heading[] = array('text' => TEXT_FAQ_HEADING_EDIT_FAQ_CATEGORY );
      $contents[] = array('form' => tep_draw_form('categories_edit', FILENAME_FAQ_CATEGORIES, 'action=update', 'post', 'id="form_faq" enctype="multipart/form-data" onsubmit="javascript: return faq_checkform()"') . tep_draw_hidden_field('cID', $cInfo->categories_id));
      $contents[] = array('text' => '<div class="sidebar-title">'.TEXT_EDIT_FAQ_CATEGORIES_INTRO.'</div>');
      $category_inputs_string = '';
      $languages = tep_get_languages();
      for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
		$category_inputs_string .= '<div class="input-group mb-2"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
        $category_inputs_string .= tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']', tep_faq_get_category_name($cInfo->categories_id, $languages[$i]['id']));
		$category_inputs_string .= '</div>' ;
      }
      $category_description_inputs_string = '';
      for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
		$category_description_inputs_string .= '<div class="input-group mb-2"><span class="input-group-addon">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</span>' ;
        $category_description_inputs_string .= tep_draw_textarea_field('categories_description[' . $languages[$i]['id'] . ']', 'soft', '40', '5', tep_faq_get_category_description($cInfo->categories_id, $languages[$i]['id']), 'class="form-control"');
		$category_description_inputs_string .= '</div>' ;
      }
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_NAME .'</div><div>'. $category_inputs_string.'</div>');
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_DESCRIPTION .'</div><div>'.  $category_description_inputs_string .'</div>');
      $contents[] = array('text' => '<div class="form-label text-center mt-2"><a href="'. tep_catalog_path().DIR_WS_OTHERS.$cInfo->categories_image .'"class="fancybox">' . tep_info_image(DIR_WS_OTHERS.$cInfo->categories_image, $cInfo->categories_name, 100) . '<br/>' . $cInfo->categories_image .'</a></div>');
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_IMAGE . '</div><div>' . tep_draw_file_field('categories_image', '', '', 'class="filestyle"')  .'</div>');
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_SORT_ORDER . '</div><div>' . tep_draw_input_field('categories_sort_order', $cInfo->categories_sort_order, 'size="2"')  .'</div>');
      $contents[] = array('text' => '<div class="form-label">' . TEXT_FAQ_CATEGORIES_STATUS.'</div><div>'.tep_draw_radio_field('categories_status', 'on', ($cInfo->categories_status == '1') ? true : false).' &nbsp; '.TEXT_FAQ_CATEGORIES_STATUS_ENABLE.' &nbsp; '.tep_draw_radio_field('categories_status', 'off', ($cInfo->categories_status == '0') ? true : false).' &nbsp; '.TEXT_FAQ_CATEGORIES_STATUS_DISABLE.'</div>');
	  if ($cre_RCO->get('faq', 'cataccessgroup') !== true) {
      	$contents[] = array('text' => '
      		<div class="form-group row mt-3">
			  <div class="col-sm-12">
			   <div data-toggle="popover" data-placement="top" data-html="true" data-content=\'<div class="text-white">'. TEXT_B2B_UPSELL_POPOVER_BODY .'</div><div class="text-center w-100"><a href="'. TEXT_B2B_UPSELL_GET_B2B_URL .'" target="_blank" class="btn btn-warning btn-sm m-r-5 m-t-10">'. TEXT_B2B_UPSELL_GET_B2B .'</a></div>\'>
				<img src="images/category-access-settings.jpg" alt="Get B2B to unlock this feature.">
			   </div>
			  </div>
			</div>');
	  }
      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-success btn-sm mt-2 mb-2 btn-save" type="submit">' . IMAGE_SAVE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_FAQ_CATEGORIES, 'cID=' . $cInfo->categories_id) . '">' . IMAGE_CANCEL . '</a></div>');
      break;
    case 'delete':
      $heading[] = array('text' => TEXT_FAQ_HEADING_DELETE_FAQ_CATEGORY);
	  $count_text = '';
      $contents[] = array('form' => tep_draw_form('categories_delete', FILENAME_FAQ_CATEGORIES, 'action=delete_confirm') . tep_draw_hidden_field('cID', $cInfo->categories_id));
      if ($cInfo->categories_faq_count > 0) $count_text = sprintf(TEXT_DELETE_WARNING_PAGES, $cInfo->categories_faq_count);
      $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_DELETE_FAQ_CATEGORIES_INTRO .'<br><b>' . $cInfo->categories_name . '</b><br/><br/>'.$count_text.'</p></div></div></div>');
      $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_DELETE . '</button><a class="btn btn-grey btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_FAQ_CATEGORIES, 'cID=' . $cInfo->categories_id) . '">' . IMAGE_CANCEL . '</a></div>');
      break;
    default:
      if (isset($cInfo) && is_object($cInfo)) {
        $heading[] = array('text' => $cInfo->categories_name);
        $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a
        				class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_FAQ_CATEGORIES, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->categories_id . '&action=edit') . '">' . IMAGE_EDIT . '</a><a
        				class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_FAQ_CATEGORIES, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->categories_id . '&action=delete') . '">' . IMAGE_DELETE . '</a></div>');
        $contents[] = array('text' => '<div class="sidebar-text text-center"><a href="'. tep_catalog_path().DIR_WS_OTHERS.$cInfo->categories_image .'"class="fancybox">' . tep_info_image(DIR_WS_OTHERS.$cInfo->categories_image, $cInfo->categories_name, 150) . '<br/>' . $cInfo->categories_image .'</a></div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_FAQ_CATEGORY_DESCRIPTION . '</div><div class="sidebar-title">' . $cInfo->categories_description . '</div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_DATE_FAQ_CATEGORY_CREATED . '</div><div class="sidebar-title">' . tep_date_short($cInfo->categories_date_added) . '</div>');
        if (tep_not_null($cInfo->categories_last_modified)) {
          $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_DATE_FAQ_CATEGORY_LAST_MODIFIED . '</div><div class="sidebar-title">' . tep_date_short($cInfo->categories_last_modified) . '</div>');
        }
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_FAQ_CATEGORY_COUNT . '</div><div class="sidebar-title">'  . $cInfo->categories_faq_count . '</div>');
        $contents[] = array('text' => '<div class="sidebar-text">' . TEXT_FAQ_CATEGORY_SORT_ORDER . '</div><div class="sidebar-title">'  . $cInfo->categories_sort_order . '</div>');
      }
      break;
  }

	  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
		$box = new box;
		echo $box->showSidebar($heading, $contents);
	  }

// RCI code start
echo $cre_RCI->get('pagescategories', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script language="javascript">
function faq_checkform() {

<?php
echo $lcadmin->print_action_js('addupdatefaq');
?>

}
<?php
echo $lcadmin->print_js();
?>
</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
