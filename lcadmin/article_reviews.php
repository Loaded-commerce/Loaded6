<?php
/*
  $Id: article_reviews.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {
      case 'update':
        $reviews_id = tep_db_prepare_input($_GET['rID']);
        $reviews_rating = tep_db_prepare_input($_POST['reviews_rating']);
        $last_modified = tep_db_prepare_input($_POST['last_modified']);
        $reviews_text = tep_db_prepare_input($_POST['reviews_text']);

        tep_db_query("update " . TABLE_ARTICLE_REVIEWS . " set reviews_rating = '" . tep_db_input($reviews_rating) . "', last_modified = now() where reviews_id = '" . (int)$reviews_id . "'");
        tep_db_query("update " . TABLE_ARTICLE_REVIEWS_DESCRIPTION . " set reviews_text = '" . tep_db_input($reviews_text) . "' where reviews_id = '" . (int)$reviews_id . "'");

		  $mode = (isset($_POST['mode']) && $_POST['mode'] != '') ? $_POST['mode'] : 'save';
		  if ($mode == 'save') {
			tep_redirect(tep_href_link(FILENAME_ARTICLE_REVIEWS, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . '&rID=' . $reviews_id));
		  } else {  // save & stay
			tep_redirect(tep_href_link(FILENAME_ARTICLE_REVIEWS, (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . '&rID=' . $reviews_id . '&action=edit'));
		  }
        break;
      case 'deleteconfirm':
        $reviews_id = tep_db_prepare_input($_GET['rID']);

        tep_db_query("delete from " . TABLE_ARTICLE_REVIEWS . " where reviews_id = '" . (int)$reviews_id . "'");
        tep_db_query("delete from " . TABLE_ARTICLE_REVIEWS_DESCRIPTION . " where reviews_id = '" . (int)$reviews_id . "'");

        tep_redirect(tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page']));
        break;
      case 'approve_review':
        $reviews_id = tep_db_prepare_input($_GET['rID']);
        tep_db_query("update " . TABLE_ARTICLE_REVIEWS . " set approved=1 where reviews_id = " . $reviews_id);
        tep_redirect(tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $reviews_id));
        break;
      case 'disapprove_review':
        $reviews_id = tep_db_prepare_input($_GET['rID']);
        tep_db_query("update " . TABLE_ARTICLE_REVIEWS . " set approved=0 where reviews_id = " . $reviews_id);
        tep_redirect(tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $reviews_id));
        break;
    }
  }

include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

  <div class="col">
    <?php
    if ($messageStack->size('taxrates') > 0) {
      echo $messageStack->output('taxrates');
    }
    ?>
<?php
if ($action == 'edit') {
	echo '<form id="review" name="review" method="post" enctype="multipart/form-data" data-parsley-validate>';
?>
      <!-- begin button bar -->
      <div id="button-bar" class="row">
        <div class="col-9 m-b-10 w-100 pt-1 pl-0 pr-0">
            <a href="<?php echo tep_href_link(FILENAME_ARTICLE_REVIEWS, (isset($_GET['page'])?'page='.$_GET['page']:'')); ?>" class="btn btn-link m-r-3 f-w-200 text-primary hidden-xs hidden-sm"><i class="fa fa-chevron-left"></i> <?php echo ((isset($cID) && empty($cID) === false) ? BUTTON_RETURN_TO_LIST :  IMAGE_CANCEL); ?></a>
            <button type="submit" onclick="updateArticleReview('save');" class="btn btn-primary m-r-3"><i class="fa fa-plus"></i> <?php echo BUTTON_SAVE; ?></button>
            <button type="submit" onclick="updateArticleReview('stay');" class="btn btn-info m-r-3 btn-save-stay"><i class="fa fa-plus-square"></i> <?php echo BUTTON_SAVE_STAY; ?></button>
        </div>
        <div class="col-3 m-b-10 pt-1 pr-2">
        </div>
      </div>
      <!-- end button bar -->
<?php
}
?>
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-articlereviews" class="table-articlereviews">
        <div class="row">
<?php
  if ($action == 'edit') {
    $rID = tep_db_prepare_input($_GET['rID']);

    $reviews_query = tep_db_query("select r.reviews_id, r.articles_id, r.customers_name, r.date_added, r.last_modified, r.reviews_read, rd.reviews_text, r.reviews_rating from " . TABLE_ARTICLE_REVIEWS . " r, " . TABLE_ARTICLE_REVIEWS_DESCRIPTION . " rd where r.reviews_id = '" . (int)$rID . "' and r.reviews_id = rd.reviews_id");
    $reviews = tep_db_fetch_array($reviews_query);

    $articles_name_query = tep_db_query("select articles_name from " . TABLE_ARTICLES_DESCRIPTION . " where articles_id = '" . (int)$reviews['articles_id'] . "' and language_id = '" . (int)$languages_id . "'");
    $articles_name = tep_db_fetch_array($articles_name_query);
    if ($articles_name === false) $articles_name = array();

    $rInfo_array = array_merge($reviews, $articles_name);
    $rInfo = new objectInfo($rInfo_array);
?>
  <div class="col-md-8 col-xl-9 dark panel-left rounded-left">

              <div style="<?php echo $display; ?>" class="tab-page" id="<?php echo $languages[$i]['name'];?>">
			  <div class="ml-2 mr-2">
				<div class="main-heading"><span>Article Review Info</span>
				  <div class="main-heading-footer"></div>
				</div>

				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_ARTICLE; ?></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 mt-1 meta-input"><?php echo $rInfo->articles_name; ?></div>
				</div>
				<div class="form-group row mb-3 m-t-20 p-relative">
				  <label class="col-xs-4 col-md-3 col-lg-2 control-label main-text mt-1"><?php echo ENTRY_REVIEW; ?></label>
				  <div class="col-xs-7 col-md-8 col-lg-9 p-r-0 meta-input">
					<?php echo tep_draw_textarea_field('reviews_text', 'soft', '60', '15', $rInfo->reviews_text, 'class="ckeditor"'); ?>
				  </div>
				</div>

               </div>
              </div>


<?php echo tep_draw_hidden_field('reviews_id', $rInfo->reviews_id) .
			tep_draw_hidden_field('articles_id', $rInfo->articles_id) .
			tep_draw_hidden_field('customers_name', $rInfo->customers_name) .
			tep_draw_hidden_field('articles_name', $rInfo->articles_name) .
			tep_draw_hidden_field('date_added', $rInfo->date_added);
?>
	</div>
	<div class="col-md-4 col-xl-3 panel-right rounded-right light">
		<div class="sidebar-container p-4"><div class="sidebar-heading"><span><div class="text-truncate">Options</div></span></div>
		<div class="sidebar-heading-footer"></div>
			<div class="sidebar-content-container">
			<div class="form-group row mt-3">
			  <label class="col-sm-4 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo ENTRY_FROM; ?></label>
			  <div class="col-sm-8 mt-2 p-relative"><?php echo $rInfo->customers_name; ?></div>
			</div>
			<div class="form-group row mt-1">
			  <label class="col-sm-4 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo ENTRY_DATE; ?></label>
			  <div class="col-sm-8 mt-2 p-relative"><?php echo tep_date_short($rInfo->date_added); ?></div>
			</div>

			<div class="form-group row mt-2">
			  <label class="col-sm-4 control-label sidebar-edit pl-3 pr-0 c-pointer text-muted mt-2"><?php echo ENTRY_RATING; ?><span class="required"></span></label>
			  <div class="col-sm-8 p-relative"><?php echo TEXT_BAD; ?>&nbsp;<?php for ($i=1; $i<=5; $i++) echo tep_draw_radio_field('reviews_rating', $i, '', $rInfo->reviews_rating) . '&nbsp;'; echo TEXT_GOOD; ?></div>
			</div>
		  </div>
    </div>
  </form>
<?php
  } elseif ($action == 'preview') {
    if (tep_not_null($_POST)) {
      $rInfo = new objectInfo($_POST);
    } else {
      $rID = tep_db_prepare_input($_GET['rID']);

      $reviews_query = tep_db_query("select r.reviews_id, r.articles_id, r.customers_name, r.date_added, r.last_modified, r.reviews_read, rd.reviews_text, r.reviews_rating from " . TABLE_ARTICLE_REVIEWS . " r, " . TABLE_ARTICLE_REVIEWS_DESCRIPTION . " rd where r.reviews_id = '" . (int)$rID . "' and r.reviews_id = rd.reviews_id");
      $reviews = tep_db_fetch_array($reviews_query);

      $articles_name_query = tep_db_query("select articles_name from " . TABLE_ARTICLES_DESCRIPTION . " where articles_id = '" . (int)$reviews['articles_id'] . "' and language_id = '" . (int)$languages_id . "'");
      $articles_name = tep_db_fetch_array($articles_name_query);
      if ($articles_name === false) $articles_name = array();

      $rInfo_array = array_merge($reviews, $articles_name);
      $rInfo = new objectInfo($rInfo_array);
    }
?>
  <div class="col-md-12 col-xl-12 dark panel-left rounded-left">
  <table witdh="100%" border="0" cellspacing="0" cellpadding="0">
      <tr><?php echo tep_draw_form('update', FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $_GET['rID'] . '&action=update', 'post', 'enctype="multipart/form-data"'); ?>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="main" valign="top" colspan="2"><b><?php echo ENTRY_ARTICLE; ?></b> <?php echo $rInfo->articles_name; ?><br><b><?php echo ENTRY_FROM; ?></b> <?php echo $rInfo->customers_name; ?><br><br><b><?php echo ENTRY_DATE; ?></b> <?php echo tep_date_short($rInfo->date_added); ?></td>
          </tr>
        </table>
      </tr>
      <tr>
        <td><table witdh="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" class="main"><b><?php echo ENTRY_REVIEW; ?></b><br><br><?php echo nl2br(tep_db_output(tep_break_string($rInfo->reviews_text, 15))); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td class="main"><b><?php echo ENTRY_RATING; ?></b>&nbsp;<?php echo tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . 'stars_' . $rInfo->reviews_rating . '.gif', sprintf(TEXT_OF_5_STARS, $rInfo->reviews_rating)); ?>&nbsp;<small>[<?php echo sprintf(TEXT_OF_5_STARS, $rInfo->reviews_rating); ?>]</small></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
<?php
    if (tep_not_null($_POST)) {
/* Re-Post all POST'ed variables */
      reset($_POST);
	  foreach($_POST as $key=>$value) { echo tep_draw_hidden_field($key, $value); }
?>
      <tr>
        <td align="right" class="smallText"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id . '&action=edit') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
      </form></tr>
<?php
    } else {
      if (isset($_GET['origin'])) {
        $back_url = $_GET['origin'];
        $back_url_params = '';
      } else {
        $back_url = FILENAME_ARTICLE_REVIEWS;
        $back_url_params = 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id;
      }
?>
      <tr>
        <td align="right"><?php echo '<a href="' . tep_href_link($back_url, $back_url_params, 'NONSSL') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
      </tr>
<?php
    }
  } else {
?>
  <div class="col-md-9 col-xl-10 dark panel-left rounded-left">

            <table class="table table-hover w-100 mt-2">
              <thead>
               <tr class="th-row">
                <th scope="col" class="th-col dark text-left"><?php echo TABLE_HEADING_ARTICLES; ?></th>
                 <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_RATING; ?></th>
                 <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_DATE_ADDED; ?></th>
                 <th scope="col" class="th-col dark text-center"><?php echo TEXT_APPROVED; ?></th>
                 <th scope="col" class="th-col dark text-right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</th>
              </tr>
<?php
    $reviews_query_raw = "select reviews_id, articles_id, date_added, last_modified, reviews_rating, approved from " . TABLE_ARTICLE_REVIEWS . " order by date_added DESC";
    $reviews_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $reviews_query_raw, $reviews_query_numrows);
    $reviews_query = tep_db_query($reviews_query_raw);
    while ($reviews = tep_db_fetch_array($reviews_query)) {
      if ((!isset($_GET['rID']) || (isset($_GET['rID']) && ($_GET['rID'] == $reviews['reviews_id']))) && !isset($rInfo)) {
        $reviews_text_query = tep_db_query("select r.reviews_read, r.customers_name, length(rd.reviews_text) as reviews_text_size from " . TABLE_ARTICLE_REVIEWS . " r, " . TABLE_ARTICLE_REVIEWS_DESCRIPTION . " rd where r.reviews_id = '" . (int)$reviews['reviews_id'] . "' and r.reviews_id = rd.reviews_id");
        $reviews_text = tep_db_fetch_array($reviews_text_query);

        $articles_name_query = tep_db_query("select articles_name from " . TABLE_ARTICLES_DESCRIPTION . " where articles_id = '" . (int)$reviews['articles_id'] . "' and language_id = '" . (int)$languages_id . "'");
        $articles_name = tep_db_fetch_array($articles_name_query);
        if ($articles_name === false) $articles_name = array();

        $reviews_average_query = tep_db_query("select (avg(reviews_rating) / 5 * 100) as average_rating from " . TABLE_ARTICLE_REVIEWS . " where articles_id = '" . (int)$reviews['articles_id'] . "'");
        $reviews_average = tep_db_fetch_array($reviews_average_query);

        $review_info = array_merge($reviews_text, $reviews_average, $articles_name);
        $rInfo_array = array_merge($reviews, $review_info);
        $rInfo = new objectInfo($rInfo_array);
      }

		$selected = (isset($rInfo) && is_object($rInfo) && ($reviews['reviews_id'] == $rInfo->reviews_id) )? true : false;
		$col_selected = ($selected) ? ' selected' : '';

      if ($selected) {
        echo '              <tr id="defaultSelected" class="table-row dark selected" onclick="document.location.href=\'' . tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id . '&action=preview') . '\'">' . "\n";
      } else {
        echo '              <tr class="table-row dark" onclick="document.location.href=\'' . tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $reviews['reviews_id']) . '\'">' . "\n";
      }
?>
                <td class="table-col dark text-left<?php echo $col_selected; ?>"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $reviews['reviews_id'] . '&action=preview') . '"><i class="fa fa-circle-o fa-lg text-info mr-2 mr-2"></i></a>&nbsp;' . tep_get_articles_name($reviews['articles_id']); ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . 'stars_' . $reviews['reviews_rating'] . '.gif'); ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php echo tep_date_short($reviews['date_added']); ?></td>
                <td class="table-col dark text-center<?php echo $col_selected; ?>">
                <?php
                echo $reviews['approved']==1?'<i class="fa fa-lg fa-check-circle text-success mr-2"></i>':'<i class="fa fa-lg fa-times-circle text-danger mr-2"></i>'; ?></td>
                <td class="table-col dark text-right<?php echo $col_selected; ?>"><?php if ( (is_object($rInfo)) && ($reviews['reviews_id'] == $rInfo->reviews_id) ) { echo '<i class="fa fa-long-arrow-right fa-lg text-success mr-2"></i>' ; } else { echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $reviews['reviews_id']) . '"><i class="fa fa-info-circle fa-lg text-muted ml-1 mr-2"></i></a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
			</table>
			<table border="0" width="100%" cellspacing="0" cellpadding="0" class="data-table-foot">
			  <tr>
				<td class="smallText" valign="top"><?php echo $reviews_split->display_count($reviews_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></td>
				<td class="smallText" align="right"><?php echo $reviews_split->display_links($reviews_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
			  </tr>
			</table>

          </div>
          <div class="col-md-3 col-xl-2 dark panel-right rounded-right">

<?php
    $heading = array();
    $contents = array();

    switch ($action) {
      case 'delete':
        $heading[] = array('text' => TEXT_INFO_HEADING_DELETE_REVIEW);

        $contents[] = array('form' => tep_draw_form('reviews', FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id . '&action=deleteconfirm'));
        $contents[] = array('text' => '<div class="row"><div class="col p-0 mt-3 ml-2 mr-2"><div class="note note-danger m-0"><p class="mb-0 mt-0">'.TEXT_INFO_DELETE_REVIEW_INTRO .'<br><b>' . $rInfo->articles_name . '</b></p></div></div></div>');
        $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><button class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" type="submit">' . IMAGE_DELETE . '</button> <a
        					class="btn btn-default btn-sm mt-2 mb-2 btn-cancel" href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id) . '">' . IMAGE_CANCEL . '</a></div>');
        break;
      default:
      if (isset($rInfo) && is_object($rInfo)) {
        $heading[] = array('text' => $rInfo->articles_name);

        $contents[] = array('align' => 'center', 'text' => '<div class="mt-2 mb-2"><a
        					class="btn btn-success btn-sm mt-2 mb-2 btn-edit" href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id . '&action=edit') . '">' . IMAGE_EDIT . '</a><a
         					class="btn btn-danger btn-sm mt-2 mb-2 btn-delete" href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id . '&action=delete') . '">' . IMAGE_DELETE . '</a></div>');
        $contents[] = array('text' => '<div class="form-label mt-3">' . TEXT_INFO_DATE_ADDED . '<span class="sidebar-title ml-2">' . tep_date_short($rInfo->date_added) .'</span></div>');
        if (tep_not_null($rInfo->last_modified)) $contents[] = array('text' => '<div class="form-label mt-1">'. TEXT_INFO_LAST_MODIFIED . '<span class="sidebar-title ml-2">' . tep_date_short($rInfo->last_modified) .'</span></div>');
        $contents[] = array('text' => '<div class="form-label mt-1">' . TEXT_INFO_REVIEW_AUTHOR . '<span class="sidebar-title ml-2">' . $rInfo->customers_name .'</span></div>');
        $contents[] = array('text' => '<div class="form-label mt-1">'. TEXT_INFO_REVIEW_RATING . '<span class="sidebar-title ml-2">' . tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . 'stars_' . $rInfo->reviews_rating . '.gif').'</span></div>');
        $contents[] = array('text' => '<div class="form-label mt-1">'. TEXT_INFO_REVIEW_READ . '<span class="sidebar-title ml-2">' . $rInfo->reviews_read .'</span></div>');
        $contents[] = array('text' => '<div class="form-label mt-1">' . TEXT_INFO_REVIEW_SIZE . '<span class="sidebar-title ml-2">' . $rInfo->reviews_text_size . ' bytes</span></div>');
        $contents[] = array('text' => '<div class="form-label mt-1">' . TEXT_INFO_ARTICLES_AVERAGE_RATING . '<span class="sidebar-title ml-2">' . number_format($rInfo->average_rating, 2) . '%</span></div>');
        if($rInfo->approved==0){
          $contents[] = array('align' => 'left', 'text' => '<div class="form-label mt-1 mb-3">' . TEXT_APPROVED . ':<span class="sidebar-title ml-2">' . TEXT_NO .'</span></div>');
          $contents[] = array('align' => 'center', 'text' => '<a class="btn btn-success btn-sm mt-2 mb-2 btn-approve" href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, tep_get_all_get_params(array('action', 'info')) . 'action=approve_review&rID=' . $rInfo->reviews_id, 'NONSSL') . '">' . TEXT_APPROVE . '</a>');
          }
        elseif($rInfo->approved==1) {
          $contents[] = array('align' => 'left', 'text' => '<div class="form-label mt-1 mb-3">' . TEXT_APPROVED . ':<span class="sidebar-title ml-2">' . TEXT_YES .'</span></div>');
          $contents[] = array('align' => 'center', 'text' => '<a class="btn btn-success btn-sm mt-2 mb-2 btn-disapprove" href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, tep_get_all_get_params(array('action', 'info')) . 'action=disapprove_review&rID=' . $rInfo->reviews_id, 'NONSSL') . '">' . TEXT_DISAPPROVE . '</a>');
          }
        else{
          $contents[] = array('align' => 'left', 'text' => '<div class="form-label mt-3 mb-3">' . TEXT_APPROVED . ':<span class="sidebar-title ml-2">' . "Unknown</span></div>" );
          }
      }
        break;
    }

	  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
		$box = new box;
		echo $box->showSidebar($heading, $contents);
	  }

  }
?>
          </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script language="javascript">
function updateArticleReview(mode) {
  var action = '<?php echo str_replace('&amp;', '&', tep_href_link(FILENAME_ARTICLE_REVIEWS, (isset($_GET['page']) ? '&page=' . $_GET['page'] : '') . (isset($_GET['rID']) ? '&rID=' . $_GET['rID'] : '') . '&action=update')); ?>';
  // set the save mode in hidden form input
  $('<input />').attr('type', 'hidden')
      .attr('name', "mode")
      .attr('value', mode)
      .appendTo('#review');

  $('#review').attr('action', action).submit();
}
</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
