<?php
  require('includes/application_top.php');
  require(DIR_WS_FUNCTIONS . 'c_orders.php');
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  require_once(DIR_WS_CLASSES . 'PriceFormatter.php');
  $pf = new PriceFormatter;

  include(DIR_WS_CLASSES . 'order.php');

if (isset($_GET['oID'])){
  $oID = tep_db_prepare_input($_GET['oID']);
}else if (isset($_POST['oID'])){
  $oID = $_POST['oID'] ;
} else {
  $oID = '' ;
}
if (isset($_GET['step'])) {
  $step = $_GET['step'] ;
}else if (isset($_POST['step'])){
  $step = $_POST['step'] ;
} else {
  $step = 1 ;
}
if (isset($_GET['action'])) {
   $action = $_GET['action'] ;
 }else if (isset($_POST['action'])){
  $action = $_POST['action'] ;
  } else {
 $action = 'edit' ;
}
include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');

?>

<div id="content" class="content p-relative">
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php  echo ADDING_TITLE; ?> #<?php  echo $oID; ?></h1>
  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  <div class="col main-col">
    <div class="row">
  	<div class="col-9"></div>
  	<div class="col-3 pr-0 text-right">
		<?php  echo '<a href="' . tep_href_link(FILENAME_EDIT_ORDERS, tep_get_all_get_params(array('action', 'add_product')). 'action=edit', 'SSL') . '" class="btn btn-success btn-sm ml-1 btnback">Back</a>'; ?>
  	</div>
    </div>
  </div>

  <div class="col">
    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->
      <div id="table-addorderproduct" class="table-addorderproduct">
        <div class="row">
          <div class="col-md-12 col-xl-12 dark panel-left rounded-left rounded-right">
<?php

//************************************************** add product ****************************
if($action == "add_product")
{

    if (isset($_GET['add_product_categories_id'])) {
      $add_product_categories_id = $_GET['add_product_categories_id'] ;
    }else if (isset($_POST['add_product_categories_id'])){
      $add_product_categories_id = $_POST['add_product_categories_id'] ;
    } else {
      $add_product_categories_id  = '' ;
    }

    if (isset($_GET['add_product_products_id'])) {
      $add_product_products_id = $_GET['add_product_products_id'] ;
    }else if (isset($_POST['add_product_products_id'])){
      $add_product_products_id = $_POST['add_product_products_id'] ;
    } else {
      $add_product_products_id  = '' ;
    }
  $customer_id = $order->customer['id'];
   //gets customer group ID
  if($customer_id=="")
  {
    $customer_group_id="G";
  }
?>
	<table border="0" width="100%" cellspacing="0" cellpadding="0" class="addproducttbl" >

<?php
  // ############################################################################
  //   Get List of All Products
  // ############################################################################
  // get customer group ID
  $customer_group_id = 'G';
  $parent_ids = array();
  $parent_query = tep_db_query("SELECT DISTINCT products_parent_id
                                FROM " . TABLE_PRODUCTS . "
                                WHERE products_parent_id <> 0");
  while($parent = tep_db_fetch_array($parent_query)) {
    $parent_ids[$parent['products_parent_id']] = 1;
  }
  ksort($parent_ids);
  $ProductList = array();
  $CategoryList = array();
  $result = tep_db_query("SELECT pd.products_name, p.products_id, cd.categories_name, ptc.categories_id
                            from " . TABLE_PRODUCTS . " p,
                                 " . TABLE_PRODUCTS_DESCRIPTION . " pd,
                                 " . TABLE_PRODUCTS_TO_CATEGORIES . " ptc,
                                 " . TABLE_CATEGORIES_DESCRIPTION . " cd
                          WHERE cd.categories_id=ptc.categories_id
                            and ptc.products_id=p.products_id
                            and (p.products_status = '1'
                            or (p.products_status <> '1' and p.products_parent_id <> 0))
                            and p.products_id=pd.products_id
                            and pd.language_id = '" . (int)$languages_id . "'
                            and cd.language_id = '" . (int)$languages_id . "'
                          ORDER BY cd.categories_name");
    while($row = tep_db_fetch_array($result)) {
      if (isset($parent_ids[$row['products_id']])) {
        continue;
      }
      extract($row,EXTR_PREFIX_ALL,"db");
      $ProductList[$db_categories_id][$db_products_id] = $db_products_name;
      $CategoryList[$db_categories_id] = $db_categories_name;
      $LastCategory = $db_categories_name;
    }
    $LastOptionTag = "";
    $ProductSelectOptions = "<option value='0'>".DONT_ADD_NEW_PRODUCT . $LastOptionTag . "\n";
    $ProductSelectOptions .= "<option value='0'>&nbsp;" . $LastOptionTag . "\n";
    foreach($ProductList as $Category => $Products) {
      $ProductSelectOptions .= "<option value='0'>$Category" . $LastOptionTag . "\n";
      $ProductSelectOptions .= "<option value='0'>---------------------------" . $LastOptionTag . "\n";
      asort($Products);
      foreach($Products as $Product_ID => $Product_Name)
      {
        $ProductSelectOptions .= "<option value='$Product_ID'> &nbsp; $Product_Name" . $LastOptionTag . "\n";
      }

      if($Category != $LastCategory)
      {
        $ProductSelectOptions .= "<option value='0'>&nbsp;" . $LastOptionTag . "\n";
        $ProductSelectOptions .= "<option value='0'>&nbsp;" . $LastOptionTag . "\n";
      }
    }
  // ############################################################################
  //   Add Products Steps
  // ############################################################################

    echo '<tr><td><table border=\'0\' style=\'width:100%\'>' . "\n";
    ?>
      <tr>
        <td width="100%"><table class="main" border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="main" align="right"><b><?php  echo ADDPRODUCT_TEXT_PROGRESS ;?> </B></td>
            <td class="main" align="left">
            <?php
            if ($step == 1){
            echo '&nbsp;<b>' . ADDPRODUCT_TEXT_STEP_1 . '</b>' .  '&nbsp;' . ADDPRODUCT_TEXT_STEP_2  . '&nbsp;' . ADDPRODUCT_TEXT_STEP_3 . '&nbsp;' . ADDPRODUCT_TEXT_STEP_4  ;
            }
            if ($step == 2){
             echo '<a href="' . tep_href_link(FILENAME_EDIT_ORDERS, tep_get_all_get_params() . 'step=1&add_product_categories_id='. $add_product_categories_id ) . '">' . '&nbsp;' . ADDPRODUCT_TEXT_STEP_1 . ' </a>' .  '&nbsp;<b>' . ADDPRODUCT_TEXT_STEP_2  . '</b>'. '&nbsp; ' . ADDPRODUCT_TEXT_STEP_3 . '&nbsp;' . ADDPRODUCT_TEXT_STEP_4 ;
            }
            if ($step == 3){
             echo '<a href="' . tep_href_link(FILENAME_EDIT_ORDERS, tep_get_all_get_params() . 'step=1&add_product_categories_id='. $add_product_categories_id ) . '">' . '&nbsp;' . ADDPRODUCT_TEXT_STEP_1 . '</a>' .  '<a href="' . tep_href_link(FILENAME_EDIT_ORDERS, tep_get_all_get_params() . 'step=2&add_product_categories_id='. $add_product_categories_id . '&add_product_products_id='. $add_product_products_id) . '">' . '&nbsp;' . ADDPRODUCT_TEXT_STEP_2  . '</a>' . '&nbsp;<b>' . ADDPRODUCT_TEXT_STEP_3 . '</b>'  . '&nbsp;' . ADDPRODUCT_TEXT_STEP_4;
            }
            if ($step == 4){
             echo '<a href="' . tep_href_link(FILENAME_EDIT_ORDERS, tep_get_all_get_params() . 'step=1&add_product_categories_id='. $add_product_categories_id ) . '">' . '&nbsp;' . ADDPRODUCT_TEXT_STEP_1 . '</a>' .   '<a href="' . tep_href_link(FILENAME_EDIT_ORDERS, tep_get_all_get_params() . 'step=2&add_product_categories_id='. $add_product_categories_id . '&add_product_products_id='. $add_product_products_id ) . '">' . '&nbsp;' . ADDPRODUCT_TEXT_STEP_2  . '</a>' .  '<a href="' . tep_href_link(FILENAME_EDIT_ORDERS, tep_get_all_get_params() . 'step=3&add_product_categories_id='. $add_product_categories_id . '&add_product_products_id='. $add_product_products_id ) . '">' . '&nbsp;' . ADDPRODUCT_TEXT_STEP_3 . '</a>' . '&nbsp;<b> ' . ADDPRODUCT_TEXT_STEP_4. '</b>';
            }


            ; ?></td>
           <td class="pageHeading" align="right"><?php  echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>

          </tr>
        </table></td>
      </tr>
<?php
    // Set Defaults
      if(!isset($add_product_categories_id)){
      $add_product_categories_id = 0;
     }
      if(!isset($add_product_products_id)){
      $add_product_products_id = 0;
      }
    // Step 1: Choose Category
    if(($step >= 1) && ($step < 3)){

      //GET VALUES
	  $category=$_POST['add_product_categories_id'];
	  $manufacturers=$_POST['$manufacturers'];
	  $price=$_POST['price'];
	  $product_code=$_POST['product_code'];
	  $keyword=$_POST['keyword'];

	  $category = isset($category)?$category:0;


      ?>
       <tr>
          <td>

				<form method="POST" action="<?php  echo tep_href_link('add_order_product.php', 'oID='.$oID.'&action=add_product', 'SSL')?>">
				<?php
				if(isset($_POST["NCAdmin"]))
				{
				?>
				<input type="hidden" name="NCAdmin" value="<?php  echo $_POST["NCAdmin"]?>">
				<?php
				}
				?>
				<input type="hidden" name="action" value="add_product">
				<input type="hidden" name="subaction" value="search">
				<input type='hidden' name='step' value='1'>
				  <table border="0" cellspacing="1" cellpadding="2">
					<tr>
					  <td><b>Product Category</b><br>
					<?php
					      $tree = tep_get_category_tree('0', '', '', '','', $customer_group_id);
					      $dropdown= tep_draw_pull_down_menu('add_product_categories_id', $tree, $category, 'class="form-control"'); //single
					      echo $dropdown;
					?>
					</td>
					  <td><b>Product Price</b> <br>
					  <input type="text" name="price" size="20" value="<?php  echo $price?>" class="form-control"></td>
					  <td><b>Product Code</b><br>
					  <input type="text" name="product_code" size="20" value="<?php  echo $product_code?>" class="form-control"></td>
					  <td><b>Keyword</b><br>
					  <input type="text" name="keyword" size="20" value="<?php  echo $keyword?>" class="form-control"></td>
					  <td align="center"><br/><input type="submit" value="Search"  class="btn btn-success btn-sm ml-1" ></td>
					</tr>
				  </table>
				</form>

          </td>
       </tr>

       <tr>
          <td><table border="0" width="100%" cellspacing="1" cellpadding="2">
					<?php
						if(isset($_POST['subaction']) && $_POST['subaction'] == 'search')
						{

							$query = "SELECT * FROM products P INNER JOIN products_description PD ON P.products_id=PD.products_id";
							$query.= " INNER JOIN products_to_categories PC ON P.products_id=PC.products_id";
							$query.= " WHERE language_id=1 ";
							$query.= ($sel_prod != "")?" AND P.products_id NOT IN ($sel_prod)":"";
							$query.= ($manufacturers != "")?" AND manufacturers_id='$manufacturers'":"";
							$query.= ($category > 0)?" AND categories_id=$category":"";
							$query.= ($price != "")?" AND P.products_price >=$price":"";
							$query.= ($product_code != "")?" AND P.products_model LIKE '$product_code%'":"";
							$query.= ($keyword != "")?" AND (products_name LIKE '%$keyword%' OR products_description LIKE '%$keyword%' OR products_model LIKE '%$keyword%' OR P.products_id LIKE '%$keyword%')":'';
							$query.= " GROUP BY P.products_id ORDER BY PD.products_name";

							$res = tep_db_query($query);

						$customers_query = "SELECT customers_id FROM orders WHERE orders_id=$oID";
						$rw_customers = tep_db_fetch_array(tep_db_query($customers_query));
						$customer_id = $rw_customers['customers_id'];

					?>
						  <tr>
							<td width="100%"></td>
						  </tr>

						  <tr>
							<td width="100%">
								<div class="table-responsive">
									<table class="table table-hover w-100 mt-2">
									 <thead>
									  <tr class="th-row">
									    <td scope="col" class="th-col dark text-left">Code</td>
										<td scope="col" class="th-col dark text-left">ID</td>
										<td scope="col" class="th-col dark text-left">Product Name</td>
										<td scope="col" class="th-col dark text-left">Price</td>
										<td scope="col" class="th-col dark text-center"><?php  echo TABLE_HEADING_QUANTITY; ?></td>
										<td scope="col" class="th-col dark text-center d-none d-lg-table-cell col-blank">Action</td>
								  </tr>
								 </thead>
								 <tbody>
					<?php
					$num_srch_record = tep_db_num_rows($res);
					if($num_srch_record != 0)
					{

						while($row = tep_db_fetch_array($res))
						{
						   $products = $pf->loadProduct($row["products_id"], 1, $customer_id);

							 $p_products_price = $pf->computePrice('1');


							$products_id_tmp = $row["products_id"];

						   if(tep_subproducts_parent($products_id_tmp)){
								$products_id_query = tep_subproducts_parent($products_id_tmp);
							}else{
								$products_id_query = $products_id_tmp;
							}

							$result_attributes = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS_ATTRIBUTES . " pa	WHERE pa.products_id = '" . $products_id_query . "'");

							$product_has_attributes = 0;

							if(tep_db_num_rows($result_attributes) > 0) {
							  $curr_step = 3;
							}else{

								$curr_step = 5;
							}

					?>
					<form name="cart_quantity" action="<?php  echo tep_href_link('add_order_product.php', 'oID='.$oID.'&action=add_product', 'SSL')?>" id="cart_quantity_<?php  echo $row['products_id']?>" method="post" onSubmit="javascript:return check_val()">

							  <input type='hidden' name='step' value='<?php  echo $curr_step;?>'>
							  <input type='hidden' name='oID' value='<?php  echo $oID;?>'>
							  <input type="hidden" name="add_product_categories_id" value="<?php  echo $categories_id;?>"/>
							  <input type="hidden" value="<?php  echo $row["products_id"]?>" id="add_product_products_id" name="add_product_products_id">
							  <input type="hidden" value="<?php  echo $p_products_price?>" id="add_products_price" name="add_products_price">

							  <tr class="table-row dark">
							    <td class="table-col dark text-left"><?php  echo $row["products_model"]?></td>
								<td class="table-col dark text-left"><?php  echo $row["products_id"]?></td>
								<td class="table-col dark text-left"><?php  echo $row["products_name"]?><!-- (<?php  echo ($row["products_status"] == 1)?"Acive":"Inactive"?>)--></td>
								<td class="table-col dark text-right"><?php  echo number_format($p_products_price, 2)?></td>

								<td class="table-col dark text-right"><input type="text" name="add_product_quantity" value="1" maxlength="5" size="5" class="form-control"></td>
								<td class="table-col dark text-right">
								<?php  if($curr_step ==5){?>
								   <input type="button" value=" Add " onclick="javascript:addProdctToOrder(<?php  echo $row['products_id']?>);" class="btn btn-success btn-sm mt-2" style="margin-top: 0px !important;">
								<?php  }else{?>
								 <input type="submit" value=" Add " class="btn btn-success btn-sm mt-2" style="margin-top: 0px !important;">
								 <?php  }?>
								</td>
							  </tr>
						</form>
					<?php
						}
					?>
					<?php
					}
					?>

							</tbody>
						  </table>
						 </div>


							</td>
						  </tr>

					<?php
					}//end of main if
					?>
				</table>
          </td>
       </tr>

      <?php
    }
    // Step 2: Choose Product
    if(($step > 1) && ($step < 3)) {

		//print_r($_POST);
      print "<tr class=\"dataTableRow\" style='background:none'><form action='" . tep_href_link('add_order_product.php', 'oID='.$oID.'&action='.$action) . "' method='POST'>\n";
      print "<td class='dataTableContent' valign='top'><select name='add_product_products_id' class='form-control'>";
      $ProductOptions = "<option value='0'> " . TEXT_ADD_PROD_CHOOSE;
      asort($ProductList[$add_product_categories_id]);
      foreach($ProductList[$add_product_categories_id] as $ProductID => $ProductName)
      {
      $ProductOptions .= "<option value='$ProductID'> $ProductName\n";
      }
      $ProductOptions = str_replace("value='$add_product_products_id'","value='$add_product_products_id' selected", $ProductOptions);
      print $ProductOptions;
      print "</select></td>\n";
      print "<td class='dataTableContent' align='center'><input type='submit' value='" . TEXT_SELECT_PROD . "' class='cssButton' style='margin-top:0px'>";
      print "<input type='hidden' name='add_product_categories_id' value='$add_product_categories_id'>";
      print "<input type='hidden' name='step' value='3'>";
      print "</td>\n";
      print "</form></tr>\n";

      print "<tr><td colspan='3'>&nbsp;</td></tr>\n";
    }
// Step 3: Show product selected
if ($step == 3){
include(DIR_WS_CLASSES . 'creAttributes.php');
  if (isset($_GET['qty'])) {
    $qty = $_GET['qty'] ;
    }else if (isset($_POST['qty'])){
    $qty = $_POST['qty'] ;
    } else {
    $qty = '1' ;
   }
   if (isset($_POST['add_product_products_price'])){
   $p_products_price = $_POST['add_product_products_price'];
   }
$add_product_quantity = $_POST['add_product_quantity'];
// Eversun mod for show product price
    if ($add_product_products_id > 0) {

       $products = $pf->loadProduct($add_product_products_id, $languages_id, $customer_id);
         $p_products_price = $pf->computePrice('1');

   }
//if price is 0, get the regular price
if($p_products_price == 0){
    $product_price_query = tep_db_query("select products_price from " . TABLE_PRODUCTS . " where products_id = '" . $add_product_products_id . "'");
    if (tep_db_num_rows($product_price_query)) {
      $product_price = tep_db_fetch_array($product_price_query);
    $p_products_price = $product_price['products_price'];
    }
}

    $p_products_price = $currencies->format($p_products_price);
    $products_name_step = tep_get_products_name($add_product_products_id, (int)$languages_id);

// check to see if product has attibutes
    $products_id_tmp = $add_product_products_id;

   if(tep_subproducts_parent($products_id_tmp)){
		$products_id_query = tep_subproducts_parent($products_id_tmp);
	}else{
		$products_id_query = $products_id_tmp;
	}

	$result_attributes = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS_ATTRIBUTES . " pa	WHERE pa.products_id = '" . $products_id_query . "'");

	$product_has_attributes = 0;

	if(tep_db_num_rows($result_attributes) > 0) {
	  $product_has_attributes = 1;
	}

  echo tep_draw_form('add_product', 'add_order_product.php', 'oID='.$oID . '&action=add_product', 'post', '', 'SSL');

;?>

<td width="100%" align="left"><table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
         <td class="main" align="left">
         <?php     echo '<b>' .TEXT_ADD_PROD. 'ddd</b>' .$add_product_products_id . '&nbsp;&nbsp;<b>' . TEXT_ADD_PROD_NAME .'</b>&nbsp;' . $products_name_step . '&nbsp;&nbsp;<b>' . TEXT_ADD_PROD_PRICE .'</b>&nbsp;' . $p_products_price. '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>' . TABLE_HEADING_CUSTOMER_GROUP . '&nbsp;</b>' .$customers_groups['customers_group_name'] ; ?>
         </td>
        </tr>
   </table></td>
    </tr>
    <tr>
     <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
           <tr class="dataTableRow">
<?php
if ($product_has_attributes == 1) {

    $products_name_step = tep_get_products_name($add_product_products_id, (int)$languages_id);
    $products_id_tmp = $add_product_products_id;

      if(tep_subproducts_parent($products_id_tmp)){
         $products_id_query = tep_subproducts_parent($products_id_tmp);
       }else{
         $products_id_query = $products_id_tmp;
        }
$result = tep_db_query("SELECT * FROM
" . TABLE_PRODUCTS_ATTRIBUTES . " pa,
" . TABLE_PRODUCTS_OPTIONS . " po,
" . TABLE_PRODUCTS_OPTIONS_TEXT . " pot,
" . TABLE_PRODUCTS_OPTIONS_VALUES . " pov
WHERE
pa.products_id = '" . $products_id_query . "' and
pa.options_id = po.products_options_id and
pa.options_id = pot.products_options_text_id and
pa.options_values_id =pov.products_options_values_id
order by pa.products_options_sort_order, po.products_options_sort_order
");

    while($row = tep_db_fetch_array($result))
      {
       extract($row,EXTR_PREFIX_ALL,"db");
       $Options[$db_products_options_id] = $db_products_options_name;
       $ProductOptionValues[$db_products_options_id][$db_products_options_values_id] = $db_products_options_values_name;
     }

   if (isset($_POST['p_products_price'])){
   $p_products_price = $_POST['p_products_price'];
   }

;?>
     <td class="main"><strong><?php  echo TEXT_PRODUCT_OPTIONS; ?></strong></td>
<?php
//Display attibutes in there native format
//intilize variables
$tax_rate = '';
$rows = '';
$cols = '';
          $products_id_tmp = $add_product_products_id;

                   if(tep_subproducts_parent($products_id_tmp)){
                    $products_id_query = tep_subproducts_parent($products_id_tmp);
                    }else{
                    $products_id_query = $products_id_tmp;
                    }

    $products_attributes_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id='" . (int)$products_id_query . "' ");
    $products_attributes = tep_db_fetch_array($products_attributes_query);

     $products_options_query = tep_db_query("select pa.products_attributes_id, pa.options_id, pa.options_values_id, pa.options_values_price, pa.price_prefix, po.options_type, po.options_length, pot.products_options_name, pot.products_options_instruct from
           " . TABLE_PRODUCTS_ATTRIBUTES  . " AS pa,
           " . TABLE_PRODUCTS_OPTIONS  . " AS po,
           " . TABLE_PRODUCTS_OPTIONS_TEXT  . " AS pot
           where pa.products_id = '" . (int)$products_id_query . "'
             and pa.options_id = po.products_options_id
             and po.products_options_id = pot.products_options_text_id
             and pot.language_id = '" . (int)$languages_id . "'
           order by pa.products_options_sort_order, po.products_options_sort_order
           ");

      // Store the information from the tables in arrays for easy of processing
      $options = array();
      $options_values = array();
      while ($po = tep_db_fetch_array($products_options_query)) {
        //  we need to find the values name

        $product_attribute_id = $po['products_attributes_id'];



        if ( $po['options_type'] != 1  && $po['options_type'] != 4 ) {
          $options_values_query = tep_db_query("select products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " where products_options_values_id ='". $po['options_values_id'] . "' and language_id = '" . (int)$languages_id . "'");
          $ov = tep_db_fetch_array($options_values_query);
        } else {
          $ov['products_options_values_name'] = '';
        }
        $options[$po['options_id']] = array('name' => $po['products_options_name'],
                                            'type' => $po['options_type'],
                                            'length' => $po['options_length'],
                                            'instructions' => $po['products_options_instruct'],
                                            'price' => $po['options_values_price'],
                                            'prefix' => $po['price_prefix'],
                                            'attrib_id' => $po['products_attributes_id'],
                                            'attributes_image' => $po['attributes_image'],
                                            );

        $options_values[$po['options_id']][$po['options_values_id']] =  array('name' => stripslashes($ov['products_options_values_name']),
                                                                              'price' => $po['options_values_price'],
                                                                              'prefix' => $po['price_prefix'],
                                                                              'attrib_id' => $po['products_attributes_id'],
                                                                              'attributes_image' => $po['attributes_image']);
      }
      if(!isset($tax_rate)) {
        $tax_rate = tep_get_tax_rate($product_info['products_tax_class_id']);
      }

      foreach ($options as $oID => $op_data) {
        switch ($op_data['type']) {

          case 1:
            $maxlength = ( $op_data['length'] > 0 ? ' maxlength="' . $op_data['length'] . '"' : '' );
            $attribute_price = $currencies->display_price($op_data['price'], $tax_rate);
            $tmp_html = '<input type="text" name="add_product_options[' . $op_data['attrib_id'] . ']"' . $maxlength . ' />';
?>
              <tr>
                <td class="main"><?php
                echo $op_data['name'] . ':' . ($op_data['instructions'] != '' ? '<br><span class="smallText">' . $op_data['instructions'] . '</span>' : '' );
                echo ($attribute_price >= 0 ? '<br><span class="smallText">' . $op_data['prefix'] . ' ' . $attribute_price . '</span>' : '' );
                 ;?>
                </td>
                <td class="main"><?php  echo $tmp_html;  ?></td>
              </tr>
              <?php
            break;

          case 4:
            if ($op_data['length'] == '') {
             $tmp_html = '<textarea name="add_product_options[' . $op_data['attrib_id'] . ']" wrap="virtual"></textarea>';
            } else {
             $text_area_array = explode(';',$op_data['length']);
             $cols = '';
             $rows = '';
             if ($text_area_array[0] != '') $cols = ' cols="' . $text_area_array[0] . '" ';
             if (isset($text_area_array[1]) && $text_area_array[1] != '') $rows = ' rows="' .
            $text_area_array[1] . '" ';
            $attribute_price = $currencies->display_price($op_data['price'], $tax_rate);

             $tmp_html = '<textarea name="add_product_options[' . $op_data['attrib_id'] . ']" '.$rows.' '.$cols.'"
            wrap="virtual"></textarea>';
            }
?>
              <tr>
                <td class="main">
<?php
                echo $op_data['name'] . ':' . ($op_data['instructions'] != '' ? '<br><span class="smallText">' . $op_data['instructions'] . '</span>' : '' );
                echo ($attribute_price >= 0 ? '<br><span class="smallText">' . $op_data['prefix'] . ' ' . $attribute_price . '</span>' : '' );
;?>
                </td>
                <td class="main" align="center"><?php  echo $tmp_html;  ?></td>
              </tr>
<?php
            break;

          case 2:
          //radio buttons
            $tmp_html = '';
            foreach ( $options_values[$oID] as $vID => $ov_data ) {
              if ( (float)$ov_data['price'] == 0 ) {
                  $price = '&nbsp;';
              } else {
                  $price = '(&nbsp;' . $ov_data['prefix'] . '&nbsp;' . $currencies->display_price($ov_data['price'], $tax_rate) . '&nbsp;)';
              }
              $tmp_html .= '<input type="radio" name="add_product_options[' . $ov_data['attrib_id'] . ']" value="' . $vID . '">' .(($ov_data['attributes_image'] != '')? tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $ov_data['attributes_image'], $ov_data['name'], '20', '20', 'style="margin:0 3px"'): ''). $ov_data['name'] . '&nbsp;' . $price . '<br>';
            } // End of the for loop on the option value
?>
              <tr>
                <td class="main"><?php  echo $op_data['name'] . ':' . ($op_data['instructions'] != '' ? '<br><span class="smallText">' . $op_data['instructions'] . '</span>' : '' ); ?></td>
                <td class="main"><?php  echo $tmp_html;  ?></td>
              </tr>
<?php
            break;

          case 3:
          //check box
            $tmp_html = '';
            $i = 0;
            foreach ( $options_values[$oID] as $vID => $ov_data ) {
              if ( (float)$ov_data['price'] == 0 ) {
                $price = '&nbsp;';
              } else {
                $price = '(&nbsp;'.$ov_data['prefix'] . '&nbsp;' . $currencies->display_price($ov_data['price'], $tax_rate).'&nbsp;)';
              }
              $tmp_html .= '<input type="checkbox" name="add_product_options[' . $ov_data['attrib_id'] . ']" value="' . $vID . '">' .(($ov_data['attributes_image'] != '')? tep_image(HTTP_SERVER . DIR_WS_CATALOG_IMAGES . $ov_data['attributes_image'], $ov_data['name'], '20', '20', 'style="margin:0 3px"'): ''). $ov_data['name'] . '&nbsp;' . $price . '<br>';
              $i++;
            }
?>
              <tr>
                <td class="main"><?php  echo $op_data['name'] . ':' . ($op_data['instructions'] != '' ? '<br><span class="smallText">' . $op_data['instructions'] . '</span>' : '' ); ?></td>
                <td class="main"><?php  echo $tmp_html;  ?></td>
              </tr>
<?php
            break;

          case 0:
          //drop down select
            $tmp_html = '<select name="add_product_options[' . $op_data['attrib_id'] . ']">';
            foreach ( $options_values[$oID] as $vID => $ov_data ) {
              if ( (float)$ov_data['price'] == 0 ) {
                $price = '&nbsp;';
              } else {
                $price = '(&nbsp; '.$ov_data['prefix'] . '&nbsp;' . $currencies->display_price($ov_data['price'], $tax_rate).'&nbsp;)';
              }
              $tmp_html .= '<option value="' . $ov_data['attrib_id'] . '">' . $ov_data['name'] . '&nbsp;' . $price .'</option>';
            } // End of the for loop on the option values
            $tmp_html .= '</select>';
?>
              <tr>
                <td class="main"><?php  echo $op_data['name'] . ':' . ($op_data['instructions'] != '' ? '<br><span class="smallText">' . $op_data['instructions'] . '</span>' : '' ); ?></td>
                <td class="main"><?php  echo $tmp_html;  ?></td>
              </tr>
<?php
            break;
        }  //end of switch
      } //end of while
?>
            </table>

        </tr>
        <tr>
           <td class="dataTableContent" align='center'><input type="submit" value="<?php  echo SELECT_THESE_OPTIONS ;?>" class='cssButton' style='margin-top:0px'>
             <input type="hidden" name="oID" value="<?php  echo $oID ;?>">
              <input type="hidden" name="step" value="4">
              <input type="hidden" name="add_product_products_id" value="<?php  echo $add_product_products_id ;?>">
              <input type="hidden" name="product_options" value="1">
              <input type="hidden" name="$products_name_step" value="<?php  echo $products_name_step;?>">
              <input type="hidden" name="add_products_price" value="<?php  echo $p_products_price ;?>">
              <input type="hidden" name="add_product_quantity" value="<?php  echo $add_product_quantity ;?>">
             </td>
         </tr>
      </table></td>
    </tr></form>
<?php
  } else { // has attibutes
;?>
        <tr>
            <td class="dataTableContent" align='center' colspan = '3'>
              <input type="submit" value="<?php  echo ADDPRODUCT_TEXT_OPTIONS_NOTEXIST ;?>" class='cssButton' style='margin-top:0px'>
              <input type="hidden" name="oID" value="<?php  echo $oID ;?>">
              <input type="hidden" name="step" value="4">
              <input type="hidden" name="add_product_products_id" value="<?php  echo $add_product_products_id ;?>">
              <input type="hidden" name="product_options" value="0">
              <input type="hidden" name="products_name_step" value="<?php  echo $products_name_step;?>">
              <input type="hidden" name="add_products_price" value="<?php  echo $p_products_price ;?>">
              <input type="hidden" name="add_product_quantity" value="<?php  echo $add_product_quantity ;?>">
             </td>
           </tr>
      </table></td>
    </tr></form>
<?php
  } //end  has attibutes
} //end step
//end step 3
//**************************************************step 4 ****************************
// Step 4: Confirm
   if($step == 4){
    $products_name_step = tep_get_products_name($add_product_products_id, (int)$languages_id);
   if (isset($_POST['add_products_price'])){
   $p_products_price = $_POST['add_products_price'];
   }
   $add_product_quantity = $_POST['add_product_quantity'];
     ;?>    <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr class="dataTableRow" style="background:none">
            <?php
            echo  tep_draw_form('select_product', FILENAME_EDIT_ORDERS, 'oID='.$oID . '&action=add_product&ref_type=fancy', 'post', '', 'SSL');
            ;?>
    <td width="100%" colspan= "3"><table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
        <td class="main" align="left">
        <?php     echo '<b>' .TEXT_ADD_PROD. ':</b>&nbsp;' .$add_product_products_id . '&nbsp;&nbsp;<b>&nbsp;' . TEXT_ADD_PROD_NAME .':</b>&nbsp;' . $products_name_step . '&nbsp;&nbsp;<b>' . TEXT_ADD_PROD_PRICE .':</b>&nbsp;' . $p_products_price;?>
         </td></tr>
      </table></td>
     </tr>
     <tr><td colspan="3" height="20"></td></tr>
     <tr>
            <td class="dataTableContent" align="left"></td>
            <td class="dataTableContent" valign="top"><b><?php  echo TEXT_ADD_QUANTITY ;?> : </b> <input class="form-control" name="add_product_quantity" size="2" value="<?php  echo $add_product_quantity;?>" style="width:100px;display:inline-block;"></td>
            <td class="dataTableContent" align="center"><input type="submit" value="<?php  echo TEXT_ADD_NOW ;?>" class='cssButton' style='margin-top:0px'>
      <?php
  $product_options = $_POST['product_options'];
  $add_product_options = (isset($_POST['add_product_options']) ? $_POST['add_product_options'] :'') ;

$option_id = '';
$value = '';
$option_value_id = '';


if ( (isset($add_product_options)) && is_array(($add_product_options)) )   {
	$key = '';
	$value = '';
	foreach($_POST['add_product_options'] as $key=>$value) {
		if (!is_array($key)) {
		  echo tep_draw_hidden_field('add_product_options[' .$key. ']', htmlentities(stripslashes($value), ENT_QUOTES)) . "\n";
		} else {
		  foreach($value as $k=>$v) {
			echo tep_draw_hidden_field('add_product_options[' .$key. ']' . '[' . $k . ']', htmlentities(stripslashes($v), ENT_QUOTES)) . "\n";
		  }
		}
	}
 }
        ;?>
    <input type="hidden" name="oID" value="<?php  echo $oID ;?>">
    <input type="hidden" name="add_product_products_id" value="<?php  echo $add_product_products_id ;?>">
    <input type="hidden" name="products_name_step" value="<?php  echo $products_name_step;?>">
    <input type="hidden" name="add_products_price" value="<?php  echo $p_products_price ;?>">
    <input type="hidden" name="step" value="5">
             </td>
             </tr>
         </table></td>
    </tr></form>
        <?php
    }// end step

        print "</table>\n";
}// end add product
?>
    </table></td>
    </tr>
</table>
            </div>
        </div>
      </div>
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->
<script src="includes/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
function addProdctToOrder(pID){
	params = $('#cart_quantity_'+pID).serialize();
	$.ajax({
		type:'post',
		data: params,
		url:"<?php echo tep_href_link(FILENAME_EDIT_ORDERS);?>&action=add_ajax_product",
		success:function(retval)
		{
		  alert("Product added successfully!");
		}
	});
}
</script>
<?php
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>

