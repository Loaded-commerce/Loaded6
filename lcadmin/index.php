<?php
/*
  $Id: index.php,v 6.5.4 2017/12/17 01:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2017 Loaded Commerce
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
  require_once('includes/application_top.php');
  include(DIR_WS_INCLUDES . 'html_top.php');
  include(DIR_WS_INCLUDES . 'header.php');
  include(DIR_WS_INCLUDES . 'column_left.php');
  ?>
  <div id="content" class="content p-relative">
    <h1 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h1>

    <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>

    <div class="row">
      <!-- begin col-3 -->
      <div class="col-md-4 col-sm-6">
        <div class="widget widget-stats bg-green">
          <div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
        <!--<div class="stats-title">TODAY'S VISITS</div>-->
          <div class="stats-title">NEWSLETTER SUBSCRIBER</div>
          <div class="stats-number"><?php echo get_newsletter_subscriber();?></div>
           <?php $news_letter = get_total_newsletter();?>
		  <div class="progress" style="height:15px;margin-bottom:15px;">
			  <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo get_total_newsletter();?>%;background-color:#FF5B57;color:<?php echo ((($news_letter) < 0)?'#FF0000':'');?>"><?php echo round(get_total_newsletter());?>%</div>
		  </div>
		  <div class="stats-lmonth">Last Month : <?php echo date("M", strtotime("-1 month", strtotime(date("F") . "1")) ) . "\n";?>&nbsp;(<?php echo get_last_month_newsletter();?>)</div>
		  <div class="stats-cmonth">Current Month : <?php echo date('M');?>&nbsp;(<?php echo round(get_total_newsletter());?>%)</div>
          <div class="stats-desc">Wost than last month</div>
          <div class="stats-link"><a href="newsletters.php">View Newsletters</a></div>
        </div>
      </div>
      <!-- end col-3 -->
      <!-- begin col-3 -->
     <?php /* <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-blue">
          <div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
         <!-- <div class="stats-title">TODAY'S PROFITS</div>-->
          <div class="stats-title">CART'S - This Month</div>
          <div class="stats-number">180,200</div>
		  <div class="progress" style="height:15px;margin-bottom:15px;">
			  <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:40.5%;background-color:#FF5B57;">40.5%</div>
		  </div>
		  <div class="stats-lmonth">Last Month : <?php echo date("M", strtotime("-1 month", strtotime(date("F") . "1")) ) . "\n";?>&nbsp;(<?php echo get_last_month_cart();?>)</div>
		  <div class="stats-cmonth">Current Month : <?php echo date('M');?>&nbsp;</div>
          <div class="stats-desc">Better than last week (40.5%)</div>
          <div class="stats-link"><a href="#">View Carts</a></div>
        </div>
      </div> */ ?>
      <!-- end col-3 -->
      <!-- begin col-3 -->
      <div class="col-md-4 col-sm-6">
        <div class="widget widget-stats bg-purple">
          <div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
          <div class="stats-title">NEW ORDERS - This Month</div>
          <div class="stats-number"><?php echo get_order_count();?></div>
          <?php $percent_value = get_order_percent();?>
          <div class="progress" style="height:15px;margin-bottom:15px;">
		      <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo get_order_percent();?>%;background-color:#FF5B57;color:<?php  echo ((($percent_value) < 0)?'#ff0000':''); ?>"><?php echo round(get_order_percent());?>%</div>
		  </div>
		  <div class="stats-lmonth">Last Month : <?php echo date("M", strtotime("-1 month", strtotime(date("F") . "1")) ) . "\n";?>&nbsp;(<?php echo get_last_month_order();?>)</div>
		  <div class="stats-cmonth">Current Month : <?php echo date('M');?>&nbsp;(<?php echo round(get_order_percent());?>%)</div>
          <div class="stats-desc">Wost than last month</div>
          <div class="stats-link"><a href="orders.php">View Orders</a></div>
        </div>
      </div>

      <!-- end col-3 -->
      <!-- begin col-3 -->
      <div class="col-md-4 col-sm-6">
        <div class="widget widget-stats bg-black">
          <div class="stats-icon stats-icon-lg"><i class="fa fa-comments fa-fw"></i></div>
          <!--<div class="stats-title">NEW COMMENTS</div>-->
          <div class="stats-title">CUSTOMERS</div>
          <div class="stats-number"><?php echo get_order_register();?></div>
          <?php $cust_total = get_totalpercent_customer();?>
          <div class="progress" style="height:15px;margin-bottom:15px;">
		      <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo get_totalpercent_customer();?>%;background-color:#FF5B57;color:<?php echo ((($cust_total) < 0)?'#ff0000':'');?>"><?php echo round(get_totalpercent_customer());?>%</div>
		  </div>
		  <div class="stats-lmonth">Last Month : <?php echo date("M", strtotime("-1 month", strtotime(date("F") . "1")) ) . "\n";?>&nbsp;(<?php echo getlast_month_customer();?>)</div>
		  <div class="stats-cmonth">Current Month : <?php echo date('M');?>&nbsp;(<?php echo round(get_totalpercent_customer());?>%)</div>
          <div class="stats-desc">Wost than last month</div>
          <div class="stats-link"><a href="customers.php">View Customers</a></div>
        </div>
      </div>
      <!-- end col-3 -->
    </div>
    <!-- end row -->
 <!-- begin row -->
    <div class="row">
      <div class="col-md-8">
        <!--Graphs start-->
		<div class="panel panel-inverse">
			<div class="panel-heading">
				<h4 class="panel-title"> Sales Report </h4>
			</div>
			<div class="panel-body">
				<?php
					if(isset($_POST['begin_date']) && $_POST['begin_date']!=''){
						$date = $_POST['begin_date'];
					}else{
						$date = date("Y-m-d");
					}
					$ts = strtotime($date);
					$year = date('o', $ts);
					$week = date('W', $ts);
				?>
				<form method="POST" name="filter_graph" action="">
					<p style="text-align:center"><i class="fa fa-calendar" aria-hidden="true"></i> Select Date: <input class="class-date form-control" type="text" id="begin_date" name="begin_date" autocomplete="off" value="<?php  echo $date; ?>" style="display:inline;width:20%;height:22px;"/><button type="submit" class="btn btn-primary btn-xs" style="margin-top: -4px; border-bottom-left-radius: 0; border-top-left-radius: 0; margin-left: -5px;">Go</button> <a href="<?php  echo tep_href_link(FILENAME_DEFAULT)?>" data-toggle="tooltip" title="Reset"><i class="fa fa-refresh" aria-hidden="true"></i></a></p>
				</form>
				<div id="nv-bar-chart-weekly" class="height-sm"></div>
			</div>
		</div>
        <!--Graphs start-->

      </div>
      <div class="col-md-4">
        <!--Graphs start-->
		<div class="panel panel-inverse">
			<div class="panel-heading">
				<h4 class="panel-title"> Newsletter Subscription Report </h4>
			</div>
			<div class="panel-body">
				<?php
					$totalcustomers = newslettersubscribed('', 'total');
					$totalsubscribed = newslettersubscribed('1', '');
					$totalunsubscribed = newslettersubscribed('0', '');
				?>
				<p style="text-align:center"><b>Total Customers:</b> <?php echo $totalcustomers; ?></p>
				<div id="nv-pie-chart" class="height-sm"></div>
			</div>
		</div>

        <!--Graphs start-->

      </div>
    </div>
    <!-- end row -->

    <div class="clearfix"></div>

    <div class="row">
		 <div class="col-md-6 dashboard_box">
			<?php echo $cre_RCI->get('index', 'blockleft'); ?>
		 </div>
		 <div class="col-md-6 dashboard_box">
			<?php echo $cre_RCI->get('index', 'blockright'); ?>
		 </div>
    </div>
  </div>
  <?php
  include(DIR_WS_INCLUDES . 'html_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
  ?>