<?php
/*
  $Id: create_account.php,v 2.0.0.0 2008/06/16 13:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// needs to be included earlier to set the success message in the messageStack
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CREATE_ACCOUNT);
$process = false;  // used by the state routine

if (isset($_POST['action']) && ($_POST['action'] == 'process')) {
  $process = true;
  if (ACCOUNT_GENDER == 'true') {
    if (isset($_POST['gender'])) {
      $gender = tep_db_prepare_input($_POST['gender']);
    } else {
      $gender = false;
    }
  }
  $firstname = tep_db_prepare_input($_POST['firstname']);
  $lastname = tep_db_prepare_input($_POST['lastname']);
  if (ACCOUNT_DOB == 'true') $dob = tep_db_prepare_input($_POST['dob']);
  if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
    $email_address = strtolower(tep_db_prepare_input($_POST['email_address_creat']));
  }else{
    $email_address = strtolower(tep_db_prepare_input($_POST['email_address']));
  }
  if (ACCOUNT_COMPANY == 'true') $company = tep_db_prepare_input($_POST['company']);

  if (isset($_POST['newsletter'])) {
    $newsletter = tep_db_prepare_input($_POST['newsletter']);
  } else {
    $newsletter = false;
  }
  $password = tep_db_prepare_input($_POST['password']);
  $shortcheckout_passchecked = tep_db_prepare_input($_POST['show_passwords']);
  if($shortcheckout_passchecked){
  	$createaccountuser = 1;
  }else{
  	$createaccountuser = 0;
  }
  $confirmation = tep_db_prepare_input($_POST['confirmation']);
  $error = false;
  if (ACCOUNT_GENDER == 'true') {
    if ( ($gender != 'm') && ($gender != 'f') ) {
      $error = true;
      $messageStack->add('frontend_message', ENTRY_GENDER_ERROR);
    }
  }
  if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
    $error = true;

	if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
	  $messageStack->add_session('frontend_message', ENTRY_FIRST_NAME_ERROR);
	  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
	  exit();
	}else{
	  $messageStack->add('frontend_message', ENTRY_FIRST_NAME_ERROR);
	}
  }
  if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
    $error = true;
	if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
	  $messageStack->add_session('frontend_message', ENTRY_LAST_NAME_ERROR);
	  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
	  exit();
	}else{
	  $messageStack->add('frontend_message', ENTRY_LAST_NAME_ERROR);
	}
  }
  if (ACCOUNT_DOB == 'true') {
    if (checkdate(substr(tep_date_raw($dob), 4, 2), substr(tep_date_raw($dob), 6, 2), substr(tep_date_raw($dob), 0, 4)) == false) {
      $error = true;

	if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
	  $messageStack->add_session('frontend_message', ENTRY_DATE_OF_BIRTH_ERROR);
	  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
	  exit();
	}else{
	  $messageStack->add('frontend_message', ENTRY_DATE_OF_BIRTH_ERROR);
	}
    }
  }
  if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
    $error = true;
	if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
	  $messageStack->add_session('frontend_message', ENTRY_EMAIL_ADDRESS_ERROR);
	  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
	  exit();
	}else{
	  $messageStack->add('frontend_message', ENTRY_EMAIL_ADDRESS_ERROR);
	}
  } elseif (tep_validate_email($email_address) == false) {
    $error = true;
	if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
	  $messageStack->add_session('frontend_message', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
	  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
	  exit();
	}else{
	  $messageStack->add('frontend_message', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
	}

  } else {
		// Delete the Guest account of that customer & check for any account exist or not. If account exists show error
		$lc_customers->clean_guest_account($email_address); // Delete all the guest account in this email address
      	if(isset($_POST['show_passwords'])) { // Its create account case

            if ($lc_customers->checkCustomerExist($email_address)) { //echo 'hereeeeeee';exit;
              $error = true;
              if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
                $messageStack->add_session('frontend_message', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
                tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
                exit();
              }else{
                $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
              }
            }
      	}
  }

  if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
    if(isset($_POST['show_passwords']) || PWA_ON == 'false'){
		  if (strlen($password) < ENTRY_PASSWORD_MIN_LENGTH) {
			$error = true;
			if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
			 $messageStack->add_session('frontend_message', ENTRY_PASSWORD_ERROR);
			  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
			  exit();
			}else{
			  $messageStack->add('frontend_message', ENTRY_PASSWORD_ERROR);
			}
		  } elseif ($password != $confirmation) {
			$error = true;
			if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
			  $messageStack->add_session('frontend_message', ENTRY_PASSWORD_ERROR_NOT_MATCHING);
			  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
			  exit();
			}else{
			  $messageStack->add('frontend_message', ENTRY_PASSWORD_ERROR_NOT_MATCHING);
			}
		  }
    }else{
      //do not check password just proceed as a guest
    }
  }else{
  if (strlen($password) < ENTRY_PASSWORD_MIN_LENGTH) {
    $error = true;
	if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
	 $messageStack->add_session('frontend_message', ENTRY_PASSWORD_ERROR);
	  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
	  exit();
	}else{
	  $messageStack->add('frontend_message', ENTRY_PASSWORD_ERROR);
	}
  } elseif ($password != $confirmation) {
    $error = true;
	if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
	  $messageStack->add_session('frontend_message', ENTRY_PASSWORD_ERROR_NOT_MATCHING);
	  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
	  exit();
	}else{
	  $messageStack->add('frontend_message', ENTRY_PASSWORD_ERROR_NOT_MATCHING);
	}
  }
  }
    if ($captcha->captcha_status('VVC_CREATE_ACCOUNT_ON_OFF')){
		if(!$captcha->validate_captcha()) {
			$error = true;
			if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
			  $messageStack->add_session('frontend_message', VISUAL_VERIFY_CODE_ENTRY_ERROR);
			  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '' ));
			  exit();
			}else{
			  $messageStack->add('frontend_message', VISUAL_VERIFY_CODE_ENTRY_ERROR);
			}
		}
    }

  // RCI to include error checks
  echo $cre_RCI->get('createaccount', 'check', false);
  if ($error == false) {
    // RCI to include error data

    $sql_data_array = array('customers_firstname' => $firstname,
                            'customers_lastname' => $lastname,
                            'customers_email_address' => $email_address,
                            'customers_newsletter' => $newsletter,
                            'is_complete' => 0,
                            'customers_password' => $password);
    if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
    if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($dob);
    if (ACCOUNT_EMAIL_CONFIRMATION == 'false' ) $sql_data_array['customers_validation'] = '1';
    if(!isset($_POST['show_passwords']) && PWA_ON == 'true') $sql_data_array['purchased_without_account'] = '1';
    $cInfo = $lc_customers->addCustomer($sql_data_array);
    $customer_id = $cInfo['data']['customers_id'];
    $_SESSION['customer_id'] = $customer_id;

    echo $cre_RCI->get('createaccount', 'submit', false);

    $sql_data_array = array('customers_id' => $customer_id,
                            'entry_firstname' => $firstname,
                            'entry_lastname' => $lastname,
                            'entry_email_address' => $email_address);
    if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
    if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;

    $aInfo = $lc_customers->addAddressBook($customer_id, $sql_data_array);
    $address_id = $aInfo['data']['address_id'];
    $lc_customers->setDefaultAddress($customer_id, $address_id);

    if (SESSION_RECREATE == 'True') {
      tep_session_recreate();
    }
    // If we are not doing the email confirmation, then log the customer in
    if ( ACCOUNT_EMAIL_CONFIRMATION == 'false' ) {
      $_SESSION['customer_first_name'] = $firstname;
      $_SESSION['customer_default_address_id'] = $address_id;
      $_SESSION['customer_country_id'] = $country;
      $_SESSION['customer_zone_id'] = $zone_id;
    } else {  // we need to build the data to do the verification
      $Pass = '';
      $Pass_neu = '';
      $pw="ABCDEFGHJKMNOPQRSTUVWXYZabcdefghjkmnopqrstuvwxyz0123456789";
      srand((double)microtime()*1000000);
      for ($i=1;$i<=5;$i++){
        $Pass .= $pw{rand(0,strlen($pw)-1)};
       }
      $pw1="ABCDEFGHJKMNOPQRSTUVWXYZabcdefghjkmnopqrstuvwxyz0123456789";
      srand((double)microtime()*1000000);
      for ($i=1;$i<=5;$i++){
        $Pass_neu .= $pw1{rand(0,strlen($pw1)-1)};
       }
      $lc_customers->updateValidationCode($customer_id, $Pass . $Pass_neu);
    }
    // restore cart contents
    $cart->restore_contents();

	//Send the email if user creating an account not for the guest customer.
    if($createaccountuser) {
		$name = $firstname . ' ' . $lastname;
		if (ACCOUNT_GENDER == 'true') {
		  if ($gender == 'm') {
			$email_text = sprintf(EMAIL_GREET_MR, $lastname);
		  } else {
			$email_text = sprintf(EMAIL_GREET_MS, $lastname);
		  }
		} else {
		  $email_text = sprintf(EMAIL_GREET_NONE, $firstname);
		}
		if (EMAIL_USE_HTML == 'true') {
		  $formated_store_owner_email = '<a href="mailto:' . STORE_OWNER_EMAIL_ADDRESS . '">' . STORE_OWNER . ': ' . STORE_OWNER_EMAIL_ADDRESS . '</a>';
		} else {
		  $formated_store_owner_email = STORE_OWNER . ': ' . STORE_OWNER_EMAIL_ADDRESS;
		}
		//$email_text .= EMAIL_WELCOME . EMAIL_TEXT . EMAIL_CONTACT . $formated_store_owner_email . "\n\n" . EMAIL_WARNING . $formated_store_owner_email . "\n\n";
		// Points/Rewards system V2.00 BOF
		if (lc_check_constant_val('MODULE_ADDONS_POINTS_STATUS','True')) {
	      $lc_customers->updatePointsIP($_SESSION['customer_id'], $ip);
		  if (NEW_SIGNUP_POINT_AMOUNT > 0) {
			tep_add_welcome_points($_SESSION['customer_id']);
			$points_account .= '<a href="' . tep_href_link(FILENAME_MY_POINTS, '', 'SSL') . '"><b><u>' . EMAIL_POINTS_ACCOUNT . '</u></b></a> . ';
			$points_faq .= '<a href="' . tep_href_link(FILENAME_MY_POINTS_HELP, '', 'NONSSL') . '"><b><u>' . EMAIL_POINTS_FAQ . '</u></b></a> . ';
			$text_points = sprintf(EMAIL_WELCOME_POINTS , $points_account, number_format(NEW_SIGNUP_POINT_AMOUNT, POINTS_DECIMAL_PLACES), $currencies->format(tep_calc_shopping_pvalue(NEW_SIGNUP_POINT_AMOUNT)), $points_faq) ."\n\n";
		  }
		  $email_text .= EMAIL_WELCOME . EMAIL_TEXT . $text_points . EMAIL_CONTACT . $formated_store_owner_email . "\n\n" . EMAIL_WARNING . $formated_store_owner_email . "\n\n";
		} else {
		  $email_text .= EMAIL_WELCOME . EMAIL_TEXT . EMAIL_CONTACT . $formated_store_owner_email . "\n\n" . EMAIL_WARNING . $formated_store_owner_email . "\n\n";
		}
		// Points/Rewards system V2.00 EOF
		if ( ACCOUNT_EMAIL_CONFIRMATION == 'true' ) {
		  $email_text .=  "\n" . MAIL_VALIDATION . "\n" . '<a href="' . str_replace('&amp;', '&', tep_href_link('pw.php', 'action=reg&pass=' . $Pass . $Pass_neu . '&verifyid=' . $_SESSION['customer_id'], 'SSL', false)) . '">' . VALIDATE_YOUR_MAILADRESS . '</a>' . "\n" . "\n" . '(' . SECOND_LINK . ' ' . str_replace('&amp;', '&', tep_href_link('pw.php', 'action=reg&pass=' . $Pass . $Pass_neu . '&verifyid=' . $_SESSION['customer_id'], 'SSL', false)) . ' )' . "\n" . "\n". OR_VALIDATION_CODE . $Pass . $Pass_neu . "\n" . "\n";
		}
		if (NEW_SIGNUP_GIFT_VOUCHER_AMOUNT > 0) {
		  $coupon_code = create_coupon_code();

		  $arr_sql = array('coupon_code'=>$coupon_code, 'coupon_type'=>'G', 'coupon_amount'=>NEW_SIGNUP_GIFT_VOUCHER_AMOUNT, 'date_created'=>date('Y-m-d H:i:s'));
		  $insert_id = lc_coupon::insertCoupon($arr_sql);

		  $arr_sql = array('coupon_id'=>$insert_id, 'customer_id_sent'=>0, 'sent_firstname'=>'Admin', 'emailed_to' => tep_db_input($email_address), 'date_sent'=>date('Y-m-d H:i:s'));
		  lc_coupon::insertCouponTrack($coupon_id, $arr_sql);
		  $email_text .= lc_coupon::getNewSignupGFEmailText($coupon_code);

		}
		if (NEW_SIGNUP_DISCOUNT_COUPON != '') {
		  $coupon_code = NEW_SIGNUP_DISCOUNT_COUPON;
		  $coupon = lc_coupon::getCouponByCode($coupon_code);
		  $coupon_id = $coupon['coupon_id'];
		  $arr_sql = array('coupon_id'=>$coupon_id, 'customer_id_sent'=>0, 'sent_firstname'=>'Admin', 'emailed_to' => tep_db_input($email_address), 'date_sent'=>date('Y-m-d H:i:s'));
		  lc_coupon::insertCouponTrack($coupon_id, $arr_sql);
		  $email_text .= lc_coupon::getNewSignupEmailText($coupon);
		}
		if (isset($_SESSION['is_std'])) {
		  if (defined('EMAIL_USE_HTML') && EMAIL_USE_HTML == 'true') {
			$email_text .= '<a href="http://www.loadedcommerce.com" target="_blank">' . TEXT_POWERED_BY_CRE . '</a>' . "\n\n";
		  } else {
			$email_text .= TEXT_POWERED_BY_CRE . "\n\n";
		  }
		}

		//RCI Before Email
		echo $cre_RCI->get('createaccount', 'beforeemail', false);

    	tep_mail($name, $email_address, EMAIL_SUBJECT, $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
    }

    //RCI Before After successful account creation
    echo $cre_RCI->get('createaccount', 'successful', false);

  if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
    if ( ACCOUNT_EMAIL_CONFIRMATION == 'true' && isset($_POST['show_passwords'])) {
    // force a log off
    unset($_SESSION['customer_id']);
    $customer_id = 0;
    unset($_SESSION['customer_default_address_id']);
    unset($_SESSION['customer_first_name']);
    unset($_SESSION['customer_country_id']);
    unset($_SESSION['customer_zone_id']);
    unset($_SESSION['comments']);
    unset($_SESSION['gv_id']);
    unset($_SESSION['cc_id']);
    $cart->reset();
	  $messageStack->add_session('frontend_message', 'Require E-mail confirmation on account creation. We will get back to you after confirmation');
	  tep_redirect(tep_href_link(FILENAME_LOGIN, '' ));
  }
 }else{
    if ( ACCOUNT_EMAIL_CONFIRMATION == 'true') {
		// force a log off
		unset($_SESSION['customer_id']);
		$customer_id = 0;
		unset($_SESSION['customer_default_address_id']);
		unset($_SESSION['customer_first_name']);
		unset($_SESSION['customer_country_id']);
		unset($_SESSION['customer_zone_id']);
		unset($_SESSION['comments']);
		unset($_SESSION['gv_id']);
		unset($_SESSION['cc_id']);
		$cart->reset();
		  $messageStack->add_session('frontend_message', 'Require E-mail confirmation on account creation. We will get back to you after confirmation');
		  tep_redirect(tep_href_link(FILENAME_LOGIN, '' ));
  }
 }
	if(isset($_POST['return_to']) && $_POST['return_to'] == 'cart'){
	  if(!isset($_POST['show_passwords']) && PWA_ON == 'true'){
	  	$_SESSION['noaccount'] = true;
		$_SESSION['customer_first_name'] = $firstname;
		$_SESSION['customer_last_name'] = $lastname;
		$_SESSION['customer_email_address'] = $email_address;
		$_SESSION['customer_default_address_id'] = $address_id;
		$_SESSION['customer_country_id'] = $country;
		$_SESSION['customer_zone_id'] = $zone_id;
	  }
	  tep_redirect(tep_href_link(FILENAME_CHECKOUT, '' ));
	}else{
	  tep_redirect(tep_href_link(FILENAME_CREATE_ACCOUNT_SUCCESS, '', 'SSL'));
	}


  }
} else {
  // check to see if someone is already logged in
  if ( isset($_SESSION['customer_id']) ) {
    // force a log off
    unset($_SESSION['customer_id']);
    $customer_id = 0;
    unset($_SESSION['customer_default_address_id']);
    unset($_SESSION['customer_first_name']);
    unset($_SESSION['customer_country_id']);
    unset($_SESSION['customer_zone_id']);
    unset($_SESSION['comments']);
    unset($_SESSION['gv_id']);
    unset($_SESSION['cc_id']);
    $cart->reset();
  }
}
$breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'));
$content = CONTENT_CREATE_ACCOUNT;
$javascript = 'form_check.js.php';
require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);

?>