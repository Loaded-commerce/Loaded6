<?php
/*
  $Id: help.php,v 1.1.1.1 2019/03/04 23:38:02 devidash Exp $

  LoadedCommerce, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2019 loadedcommerce

  Released under the GNU General Public License
*/

$navigation->remove_current_page();

$helppage = isset($_GET['hpage'])?$_GET['hpage']:'';
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>">
<title><?php echo STORE_NAME; ?></title>
<link rel="stylesheet" href="templates/sevenofsix/css/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="templates/sevenofsix/css/css/template.css?v=295607052">
<link href="templates/sevenofsix/css/font-awesome.css" rel="stylesheet">
</head>
<body class="popupBody">
<?php
switch($helppage) {

	case'search_help':
		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADVANCED_SEARCH);
		echo '<h1>'. HEADING_SEARCH_HELP .'</h1>';
		if(defined('TEXT_SEARCH_HELP') && trim(TEXT_SEARCH_HELP) != '')
			echo '<div><p>'. TEXT_SEARCH_HELP .'</p></div>';
		break;
	
	case'cart_help':
		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_INFO_SHOPPING_CART);
		echo '<h1>'. HEADING_TITLE .'</h1>';
		if(defined('SUB_HEADING_TEXT_1') && trim(SUB_HEADING_TEXT_1) != '')
			echo '<div><p>'. SUB_HEADING_TEXT_1 .'</p></div>';

		if(defined('SUB_HEADING_TEXT_2') && trim(SUB_HEADING_TEXT_2) != '')
			echo '<div><p>'. SUB_HEADING_TEXT_2 .'</p></div>';

		if(defined('SUB_HEADING_TEXT_3') && trim(SUB_HEADING_TEXT_3) != '')
			echo '<div><p>'. SUB_HEADING_TEXT_3 .'</p></div>';
		break;
	case'coupon_help':
		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_POPUP_COUPON_HELP);
		// v5.13: security flaw fixed in query
		  $coupon_query = tep_db_query("select * from " . TABLE_COUPONS . " where coupon_id = '" . intval($_GET['cID']) . "'");
		  $coupon = tep_db_fetch_array($coupon_query);
		  $coupon_desc_query = tep_db_query("select * from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" . (int)$_GET['cID'] . "' and language_id = '" . (int)$languages_id . "'");
		  $coupon_desc = tep_db_fetch_array($coupon_desc_query);
		  $text_coupon_help = TEXT_COUPON_HELP_HEADER;
		  $text_coupon_help .= sprintf(TEXT_COUPON_HELP_NAME, $coupon_desc['coupon_name']);
		  if (tep_not_null($coupon_desc['coupon_description'])) $text_coupon_help .= sprintf(TEXT_COUPON_HELP_DESC, $coupon_desc['coupon_description']);
		  $coupon_amount = $coupon['coupon_amount'];
		  switch ($coupon['coupon_type']) {
			case 'F':
			$text_coupon_help .= sprintf(TEXT_COUPON_HELP_FIXED, $currencies->format($coupon['coupon_amount']));
			break;
			case 'P':
			$text_coupon_help .= sprintf(TEXT_COUPON_HELP_FIXED, number_format($coupon['coupon_amount'],2). '%');
			break;
			case 'S':
			$text_coupon_help .= TEXT_COUPON_HELP_FREESHIP;
			break;
			default:
		  }
			/************gsr******/
		   if($coupon['coupon_sale_exclude'] == 1) {
		  $text_coupon_help .=TEXT_COUPON_SALE_EXCLUDE;
			}
			/*********************/
		  if ($coupon['coupon_minimum_order'] > 0 ) $text_coupon_help .= sprintf(TEXT_COUPON_HELP_MINORDER, $currencies->format($coupon['coupon_minimum_order']));
		  $text_coupon_help .= sprintf(TEXT_COUPON_HELP_DATE, tep_date_short($coupon['coupon_start_date']),tep_date_short($coupon['coupon_expire_date']));
		  $text_coupon_help .= '<b>' . TEXT_COUPON_HELP_RESTRICT . '</b>';
		  $text_coupon_help .= '<br><br>' .  TEXT_COUPON_HELP_CATEGORIES;
		  $coupon_get=tep_db_query("select restrict_to_categories from " . TABLE_COUPONS . " where coupon_id='".(int)$_GET['cID']."'");
		  $get_result=tep_db_fetch_array($coupon_get);

		  $cat_ids = preg_split("/[,]/", $get_result['restrict_to_categories']);
		  for ($i = 0; $i < count($cat_ids); $i++) {
			$result = tep_db_query("SELECT * FROM categories, categories_description WHERE categories.categories_id = categories_description.categories_id and categories_description.language_id = '" . (int)$languages_id . "' and categories.categories_id='" . $cat_ids[$i] . "'");
			if ($row = tep_db_fetch_array($result)) {
			$cats .= '<br>' . $row["categories_name"];
			}
		  }
		  if ($cats=='') $cats = '<br>'.TEXT_NONE;
		  $text_coupon_help .= $cats;
		  $text_coupon_help .= '<br><br>' .  TEXT_COUPON_HELP_PRODUCTS;
		  $coupon_get=tep_db_query("select restrict_to_products from " . TABLE_COUPONS . "  where coupon_id='".(int)$_GET['cID']."'");
		  $get_result=tep_db_fetch_array($coupon_get);

		  $pr_ids = preg_split("/[,]/", $get_result['restrict_to_products']);
		  for ($i = 0; $i < count($pr_ids); $i++) {
			$result = tep_db_query("SELECT * FROM products, products_description WHERE products.products_id = products_description.products_id and products_description.language_id = '" . (int)$languages_id . "'and products.products_id = '" . $pr_ids[$i] . "'");
			if ($row = tep_db_fetch_array($result)) {
			  $prods .= '<br>' . $row["products_name"];
			}
		  }
		  if ($prods=='') $prods = '<br>'.TEXT_NONE;
		  $text_coupon_help .= $prods;

		echo '<h1>'. HEADING_COUPON_HELP .'</h1>';
		echo '<div><p>'. $text_coupon_help .'</p></div>';

		break;
	case'affiliate_help':
		  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_POPUP_AFFILIATE_HELP);
			  if (isset($_GET['help_text'])) {
					  $help_text = $_GET['help_text'] ;
					}else if (isset($_POST['help_text'])){
					  $help_text = $_POST['help_text'] ;
					} else {
					 $help_text = '' ;
				}

		 if (tep_not_null($help_text)) {
			switch ($help_text) {

		  case '1':
			$text_out_help = TEXT_IMPRESSIONS_HELP;
		  break;
		   case '2':
			$text_out_help = TEXT_VISITS_HELP;
		  break;
			case '3':
			$text_out_help = TEXT_TRANSACTIONS_HELP;
		  break;
			case '4':
			$text_out_help = TEXT_CONVERSION_HELP;
		  break;
			case '5':
			$text_out_help = TEXT_AMOUNT_HELP;
		  break;
			case '6':
			$text_out_help = TEXT_AVERAGE_HELP;
		  break;
			case '7':
			$text_out_help = TEXT_COMMISSION_RATE_HELP;
		  break;
		   case '8':
			$text_out_help = TEXT_CLICKTHROUGH_RATE_HELP;
			break;
		  case '9':
			$text_out_help = TEXT_PAY_PER_SALE_RATE_HELP;
		  break;
		  case '10':
			$text_out_help = TEXT_COMMISSION_HELP;
		  break;
		  case '12':
			$text_out_help = TEXT_DATE_HELP;
		  break;
		  case '13':
			$text_out_help = TEXT_CLICKED_PRODUCT_HELP;
		  break;
			case '14':
			  $text_out_help = TEXT_REFFERED_HELP;
		  break;
			case '15':
			  $text_out_help = TEXT_DATE_HELP_SALE;
		  break;
			  case '16':
				$text_out_help = TEXT_TIME_HELP_SALE;
			break;
			case '17':
			  $text_out_help = TEXT_SALE_VALUE_HELP_SALE;
		  break;
			case '18':
			  $text_out_help = TEXT_COMMISSION_RATE_HELP_SALE;
		  break;
			case '19':
			  $text_out_help = TEXT_COMMISSION_VALUE_HELP;
		  break;
			case '20':
			  $text_out_help = TEXT_STATUS_HELP;
		  break;
			case '21':
			  $text_out_help = TEXT_PAYMENT_ID_HELP;
		  break;
			case '22':
			  $text_out_help = TEXT_PAYMENT_HELP_1;
		  break;
			case '23':
			  $text_out_help = TEXT_PAYMENT_STATUS_HELP;
		  break;
			case '24':
			  $text_out_help = TEXT_PAYMENT_DATE_HELP;
		  break;

		   }
		 }
		echo '<h1>'. HEADING_SUMMARY_HELP .'</h1>';
		echo '<div><p>'. $text_out_help .'</p></div>';

		break;
	case'wishlist_help':
		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_WISHLIST_HELP);
		echo '<h1>'. HEADING_TITLE .'</h1>';
		echo '<div><p>'. TEXT_INFORMATION .'</p></div>';
		break;
	case'links_help':
		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LINKS_SUBMIT);
		echo '<h1>'. HEADING_LINKS_HELP .'</h1>';
		echo '<div><p>'. TEXT_LINKS_HELP .'</p></div>';
		break;
}

?>
</body>
</html>