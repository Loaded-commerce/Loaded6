<?php
/*
  $Id: pw.php,v 1.2 2020/03/09 19:56:29 devidash Exp $

  Loaded Commerce, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2003 LoadedCommerce

  Released under the GNU General Public License
*/

// the following cPath references come from application_top.php

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PW);
$content = CONTENT_PW;
require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);
?>