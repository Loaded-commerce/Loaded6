<?php
class Session {
    public static function unserialize($session_data) {
        $method = ini_get("session.serialize_handler");
        switch ($method) {
            case "php":
                return self::unserialize_php($session_data);
                break;
            case "php_binary":
                return self::unserialize_phpbinary($session_data);
                break;
            default:
                throw new Exception("Unsupported session.serialize_handler: " . $method . ". Supported: php, php_binary");
        }
    }

    private static function unserialize_php($session_data) {
        $return_data = array();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            if (!strstr(substr($session_data, $offset), "|")) {
                throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
            }
            $pos = strpos($session_data, "|", $offset);
            $num = $pos - $offset;
            $varname = substr($session_data, $offset, $num);
            $offset += $num + 1;
            $data = unserialize(substr($session_data, $offset));
            $return_data[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $return_data;
    }

    private static function unserialize_phpbinary($session_data) {
        $return_data = array();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            $num = ord($session_data[$offset]);
            $offset += 1;
            $varname = substr($session_data, $offset, $num);
            $offset += $num;
            $data = unserialize(substr($session_data, $offset));
            $return_data[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $return_data;
    }
}

$token = strip_tags($_GET['loginkey']);
$value_query = tep_db_query("select value from " . TABLE_SESSIONS . " where sesskey = '" . tep_db_input($token) . "' and expiry > '" . time() . "'");
if(tep_db_num_rows($value_query) > 0)
{
	$value = tep_db_fetch_array($value_query);
	if (isset($value['value'])) 
	{
		$sess_data = Session::unserialize($value['value']);
		if((isset($sess_data['login_id']) && $sess_data['login_id'] > 0) && (isset($sess_data['login_groups_id']) && $sess_data['login_groups_id'] > 0) && (isset($sess_data['adminlogin_customer']) && $sess_data['adminlogin_customer'] > 0)) {
			setcookie('adminstrator_login', 'true');
			
			$customers_id = $sess_data['adminlogin_customer'];
			$check_customer_query = tep_db_query("select customers_id, customers_firstname, customers_lastname, customers_password, customers_email_address, customers_validation, customers_default_address_id from " . TABLE_CUSTOMERS . " where customers_id = '" . $customers_id . "'");
			if (!tep_db_num_rows($check_customer_query)) {
				echo "Invalid Try, your IP address will be blacklisted in 3 invalid attempts.";
				exit();
			} else {
				$check_customer = tep_db_fetch_array($check_customer_query);

				//=================== Logout the existing session first.Code start here ===================
				unset($_SESSION['customer_id']);
				unset($_SESSION['customer_default_address_id']);
				unset($_SESSION['customer_first_name']);
				unset($_SESSION['customer_country_id']);
				unset($_SESSION['customer_zone_id']);
				unset($_SESSION['comments']);
				// Points/Rewards system V2.00 BOF
				unset($_SESSION['customer_shopping_points']);
				unset($_SESSION['customer_shopping_points_spending']);
				unset($_SESSION['customer_referral']);
				if (isset($_SESSION['point_covers'])) unset($_SESSION['point_covers']);
				// Points/Rewards system V2.00 EOF
				// unset the special session variable used by the shipping estimator
				if (isset($_SESSION['cart_address_id'])) unset($_SESSION['cart_address_id']);
				if (isset($_SESSION['cart_country_id'])) unset($_SESSION['cart_country_id']);
				if (isset($_SESSION['cart_zip_code'])) unset($_SESSION['cart_zip_code']);
				// unset the special variables used by the shipping and payment routines
				if (isset($_SESSION['sendto'])) unset($_SESSION['sendto']);
				if (isset($_SESSION['billto'])) unset($_SESSION['billto']);
				if (isset($_SESSION['cartID'])) unset($_SESSION['cartID']);
				if (isset($_SESSION['shipping'])) unset($_SESSION['shipping']);
				if (isset($_SESSION['payment'])) unset($_SESSION['payment']);
				if (isset($_SESSION['admin_login'])) unset($_SESSION['admin_login']);
				// ICW - logout -> unregister GIFT VOUCHER sessions - Thanks Fredrik
				unset($_SESSION['gv_id']);
				unset($_SESSION['cc_id']);
				// ICW - logout -> unregister GIFT VOUCHER sessions  - Thanks Fredrik
				$cart->reset();
				//=================== Logout code end here ===================

				//=================== Login Code start Here ===================
				if (SESSION_RECREATE == 'True') {
				tep_session_recreate();
				}
				// RCI login valid
				echo $cre_RCI->get('login', 'valid');
				$check_country_query = tep_db_query("select entry_country_id, entry_zone_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$check_customer['customers_id'] . "' and address_book_id = '" . (int)$check_customer['customers_default_address_id'] . "'");
				$check_country = tep_db_fetch_array($check_country_query);
				$_SESSION['customer_id'] = $check_customer['customers_id'];
				$_SESSION['customer_default_address_id'] = $check_customer['customers_default_address_id'];
				$_SESSION['customer_first_name'] = $check_customer['customers_firstname'];
				$_SESSION['customer_last_name'] = $check_customer['customers_lastname'];
				$_SESSION['customer_country_id'] = $check_country['entry_country_id'];
				$_SESSION['customer_zone_id'] = $check_country['entry_zone_id'];
				unset($_SESSION['noaccount']);
				tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_of_last_logon = now(), customers_info_number_of_logons = customers_info_number_of_logons+1 where customers_info_id = '" . (int)$_SESSION['customer_id'] . "'");
				// restore cart contents
				$cart->restore_contents();
				//=================== Login COde end here ===================
				
				tep_redirect(tep_href_link(FILENAME_ACCOUNT, '', 'NONSSL'));
				exit();
			}
		} else {
			echo "Invalid Try, your IP address will be blacklisted in 3 invalid attempts.";
			exit();
		}
	} else {
		echo "Invalid Try, your IP address will be blacklisted in 3 invalid attempts.";
		exit();
	}
}
?>
