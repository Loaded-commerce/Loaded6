<?php
/*
  $Id: contact_us.php,v 1.1.1.1 2004/03/04 23:37:58 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CONTACT_US);

  $error = false;
  if (isset($_GET['action']) && ($_GET['action'] == 'send')) {
    //print_r($_POST);exit;
    $name = tep_db_prepare_input($_POST['name']);
    $email_address = tep_db_prepare_input($_POST['email']);
    $enquiry = tep_db_prepare_input($_POST['enquiry']);

    $telephone = tep_db_prepare_input($_POST['telephone']);
    $company = tep_db_prepare_input($_POST['company']);
    $street = tep_db_prepare_input($_POST['street']);
    $city = tep_db_prepare_input($_POST['city']);
    $state = tep_db_prepare_input($_POST['state']);
    $postcode = tep_db_prepare_input($_POST['postcode']);
    $country = tep_db_prepare_input($_POST['country']);

    $topic = tep_db_prepare_input($_POST['topic']);
    $subject = tep_db_prepare_input($_POST['subject']);

    $urgent = (tep_db_prepare_input($_POST['urgent']) == 'on') ? '1' : '0';
    $self = (tep_db_prepare_input($_POST['self']) == 'on') ? '1' : '0';

    $urgent_string = '';
    if ($urgent == '1') {
      $urgent_string = '(' . TEXT_SUBJECT_URGENT . ')';
    }

    $sql_data_array = array('customer_name' => $name,
                            'customer_email_address' => $email_address,
                            'customer_topic' => $topic,
                            'customer_subject' => $subject,
                            'customer_enquiry' => $enquiry,
                            );
    tep_db_perform(TABLE_CONTACT_US, $sql_data_array);

   $subject_string = TEXT_SUBJECT_PREFIX . ' ' . $topic . ": " . $subject . $urgent_string;
    $message_string = $topic . ": " . $subject . "\n\n" .
      $enquiry . "\n\n" .
      ENTRY_NAME . ' ' . $name . "\n" .
      ENTRY_EMAIL . ' ' . $email_address . "\n";

    $ipaddress = $_SERVER['REMOTE_ADDR'];
    $message_string .= "\n\n" . 'IP: ' . $ipaddress . "\n";
    if ($email_address == '') {
      $error = true;
      $messageStack->add('frontend_message', ENTRY_EMAIL_ADDRESS_BLANK_ERROR);
      $subject = (isset($subject) && $subject != '' ? $subject : '');
      $enquiry = (isset($enquiry) && $enquiry != '' ? $enquiry : '');
      $name = (isset($name) && $name != '' ? $name : '');
    }

    if (!tep_validate_email($email_address) && $email_address != '') {
      $error = true;
      $messageStack->add('frontend_message', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
      $subject = (isset($subject) && $subject != '' ? $subject : '');
      $enquiry = (isset($enquiry) && $enquiry != '' ? $enquiry : '');
      $name = (isset($name) && $name != '' ? $name : '');
      $email = "";
    }

	if ($captcha->captcha_status('VVC_CONTACT_US_ON_OFF')){
		if(!$captcha->validate_captcha()) {
			$error = true;
			$messageStack->add('frontend_message', VISUAL_VERIFY_CODE_ENTRY_ERROR);
		}
    }

	$email_order['html'] = nl2br($message_string);
	$email_order['text'] = $message_string;
    if(!$error){
		tep_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $subject_string, $email_order, $name, $email_address);
      // email a copy to sender, if opted
      if ($self == '1') {
        tep_mail($name, $email_address, $subject_string, $email_order, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
      }
      tep_redirect(tep_href_link(FILENAME_CONTACT_US, 'action=success', 'SSL'));
    }
 }

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CONTACT_US, '', 'SSL'));
  $content = CONTENT_CONTACT_US;
  require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);
?>
