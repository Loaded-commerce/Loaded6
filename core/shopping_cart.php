<?php
/*
  $Id: shopping_cart.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_SHOPPING_CART);
$breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_SHOPPING_CART));
// validate cart for checkout
$valid_to_checkout = true;
$cart->get_products(true);
if ($cart->count_contents() > 100000){ $cart->reset(); }

require(DIR_WS_CLASSES . 'order.php');
$order = new order;

require(DIR_WS_CLASSES . 'order_total.php');//ICW ADDED FOR CREDIT CLASS SYSTEM
$order_total_modules = new order_total;//ICW ADDED FOR CREDIT CLASS SYSTEM
$order_total_modules->collect_posts();

$content = CONTENT_SHOPPING_CART;
$javascript = 'shopping_cart.js.php';
require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);
?>