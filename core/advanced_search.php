<?php
/*
  $Id: advanced_search.php,v 1.1.1.1 2004/03/04 23:37:53 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADVANCED_SEARCH);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ADVANCED_SEARCH));

  $content = CONTENT_ADVANCED_SEARCH;
  $javascript = $content . '.js';
  require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);

?>
<script type="text/javascript">
 	$('#from-date').datepicker({
	    todayHighlight: true
    });

 	$('#date-to').datepicker({
	    todayHighlight: true
    });

</script>