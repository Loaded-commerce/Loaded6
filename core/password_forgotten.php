<?php
/*
  $Id: password_forgotten.php,v 1.1.1.1 2020/03/04 23:38:01 devidash Exp $

  LoadedCommerce, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2020 osCommerce

  Released under the GNU General Public License
*/
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PASSWORD_FORGOTTEN);
  $obj_catalog->sanitize_get_var();
  $sID = isset($_GET['sID'])?trim(strip_tags($_GET['sID'])):'';
   if(!empty($sID)) {
	$check_query = tep_db_query("select customers_firstname, customers_lastname, customers_password, customers_id from " . TABLE_CUSTOMERS . " where secret_id = '" . $_GET['sID'] . "'");
	if (tep_db_num_rows($check_query) < 1 ) {
	  $messageStack->add_session('frontend_message', 'Your reset link is invalid !','error');
	  tep_redirect(tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL'));
	}
  }

  $error = false;

  if (isset($_GET['action']) && ($_GET['action'] == 'process')) {
    $email_address = strtolower(tep_db_prepare_input($_POST['email_address']));

    if ($email_address == '') {
      $error = true;
      $messageStack->add('frontend_message', ENTRY_EMAIL_ADDRESS_BLANK_ERROR);
    }

    if (!tep_validate_email($email_address) && $email_address != '') {
      $error = true;
      $messageStack->add('frontend_message', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
    }

  if (defined('VVC_SITE_ON_OFF') && VVC_SITE_ON_OFF == 'On' && defined('MODULE_ADDONS_LC_CAPTCHA_STATUS') && MODULE_ADDONS_LC_CAPTCHA_STATUS == 'True'){
    if (defined('VVC_PASSWORD_FORGOT_ON_OFF') && VVC_PASSWORD_FORGOT_ON_OFF == 'On') {
		if(!$captcha->validate_captcha()) {
			$error = true;
			$messageStack->add('frontend_message', VISUAL_VERIFY_CODE_ENTRY_ERROR);
		}
    }
  }

if(!$error) {
    $check_customer_query = tep_db_query("select customers_firstname, customers_lastname, customers_password, customers_id from " . TABLE_CUSTOMERS . " where lower(customers_email_address) = '" . tep_db_input($email_address) . "'");
	  if (tep_db_num_rows($check_customer_query)) {
	  $check_customer = tep_db_fetch_array($check_customer_query);

	  $secret_id = tep_create_random_value(8);
	 //echo $secret_id;exit;
	  $query = array('secret_id' => $secret_id);
	  tep_db_perform('customers',$query,'update', "customers_id = '".(int)$check_customer['customers_id']."'");
	  $resetlink = tep_href_link('password_forgotten.php','sID='.$secret_id);
	  $text_body = '<p>'.str_replace('#LINK_URL#',$resetlink,NEW_URL).'</p>';
	  tep_mail($check_customer['customers_firstname'] . ' ' . $check_customer['customers_lastname'], $email_address, EMAIL_PASSWORD_REMINDER_SUBJECT,$text_body, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
	  $messageStack->add_session('frontend_message', 'Please check your registered email to reset the password.', 'success');
	  tep_redirect(tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL'));
	} else {
	  $messageStack->add('frontend_message', TEXT_NO_EMAIL_ADDRESS_FOUND);
	}
  }
} elseif(isset($_GET['action']) && ($_GET['action'] == 'update')) {
	$check_customer_query = tep_db_query("select customers_firstname, customers_lastname, customers_password, customers_id from " . TABLE_CUSTOMERS . " where secret_id = '" . $_GET['sID'] . "'");
		if (tep_db_num_rows($check_customer_query)) {
		$check_customer = tep_db_fetch_array($check_customer_query);

		$customers_password = $_POST['new_password'];
		$confirm_password = $_POST['confirm_password'];
		if($_POST['confirm_password'] == $_POST['new_password'] && $customers_password != ''){
			$new_password = $_POST['new_password'];
			$crypted_password = tep_encrypt_password($customers_password);
			tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_db_input($crypted_password) . "', secret_id = '' where customers_id = '" . (int)$check_customer['customers_id'] . "'");
			$messageStack->add_session('frontend_message', 'Success: your password has been changed', 'success');
			tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
		} else {
			$messageStack->add_session('frontend_message', 'Password did not match.','error');
			tep_redirect(tep_href_link(FILENAME_PASSWORD_FORGOTTEN, 'sID='.$_GET['sID'], 'SSL'));
		}
	}
}
$breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_LOGIN, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL'));

$content = CONTENT_PASSWORD_FORGOTTEN;

require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);

?>