<?php
/*
  $Id: xc_process.php,v 1.0.0.0 2007/11/13 13:41:11 datazen Exp $

  Copyright (c) 2013 Loaded Commerce
  Copyright (c) 2007 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License

*/
if(isset($_GET['error']) && trim($_GET['error']) == 'cancelled') {
?>
<script type="text/javascript">
window,close();
</script>
<?php
exit();
}
if (isset($_GET['token']) && substr($_GET['token'], 0, 2) == 'EC') {
  $_SESSION['xcToken'] = $_GET['token'];
  $_SESSION['xcPayerID'] = $_GET['PayerID'];
  $_SESSION['xcSet'] = TRUE;
} else {
    if(isset($_SESSION['xcToken'])) unset($_SESSION['xcToken']);
    if(isset($_SESSION['xcPayerID'])) unset($_SESSION['xcPayerID']);
    if(isset($_SESSION['xcSet'])) unset($_SESSION['xcSet']);
}
if ($_SESSION['xcSet']) {
?>
<script type="text/javascript">
window.opener.ppreceive('<?php echo $_SESSION['xcToken']; ?>', '<?php echo $_SESSION['xcPayerID']; ?>');
window,close();
</script>
<?php
} elseif(isset($_GET['error']) && trim($_GET['error']) != '') {
    echo strip_tags($_GET['error']);
	exit();
} else {
	echo 'Invalid Action';
	exit();
}
?>