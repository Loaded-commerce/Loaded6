<?php
/*
  $Id: checkout_process.php,v 1.1.1.2 2020/03/04 23:37:57 devidash Exp $

  Loaded Commerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce

  Released under the GNU General Public License
*/

  if (isset($_GET['token']) && substr($_GET['token'], 0, 2) == 'EC') {
    $_SESSION['xcToken'] = $_GET['token'];
    $_SESSION['xcPayerID'] = $_GET['PayerID'];
    $_SESSION['xcSet'] = TRUE;
    $_SESSION['payment'] = 'paypal_xc';
  }
  if (isset($_POST['payment'])) {
    $payment = $_POST['payment'];
    $_SESSION['payment'] = $payment;
  }
  if (tep_not_null($_POST['comments'])) {
    $comments = tep_db_prepare_input($_POST['comments']);
    $_SESSION['comments'] = $comments;
    $GLOBALS['comments'] = $comments;
  }

// if the customer is not logged on, redirect them to the login page
  if ( ! isset($_SESSION['customer_id']) ) {
    $navigation->set_snapshot(array('mode' => 'SSL', 'page' => FILENAME_CHECKOUT_PAYMENT));
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

  if (!isset($_SESSION['sendto'])) {
    tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
  }

  if ( (tep_not_null(MODULE_PAYMENT_INSTALLED)) && (!isset($_SESSION['payment'])) ) {
    tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
 }


// avoid hack attempts during the checkout procedure by checking the internal cartID
  if (!isset($_GET['order_id']) && !$_SESSION['xcSet']) {
    if (isset($cart->cartID) && isset($_SESSION['cartID']) ) {
      if ($cart->cartID != $_SESSION['cartID']) {
        tep_redirect(tep_href_link(FILENAME_CHECKOUT, '', 'SSL'));
      }
    }
  }

  include(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_PROCESS);

  // RCI code start
  echo $cre_RCI->get('checkoutprocess', 'check', false);
  // RCI code eof

  // added for PPSM
  if (isset($_SESSION['sub_payment']) && $_SESSION['sub_payment'] == 'paypal_wpp_dp') $_SESSION['payment'] = 'paypal';

  // load selected payment module
  require(DIR_WS_CLASSES . 'payment.php');
  //if (isset($_SESSION['credit_covers'])) $_SESSION['payment'] = ''; //ICW added for CREDIT CLASS
  $payment_modules = new payment($_SESSION['payment']);

  if ((isset($_SESSION['token']) && substr($_SESSION['token'], 0, 2) == 'EC') || (isset($_SESSION['xcSet']) && $_SESSION['xcSet'] == TRUE)) $_SESSION['payment'] = 'paypal_xc';

// load the selected shipping module
  require(DIR_WS_CLASSES . 'shipping.php');
  $shipping_modules = new shipping($_SESSION['shipping']);
  lc_check_addon_class('order.php');
  $order = new order;

  if(!class_exists('order_total', false)) {
   include(DIR_WS_CLASSES . 'order_total.php');
   $order_total_modules = new order_total;
  }

  $order_totals = $order_total_modules->process();

  //Check the stock
	$obj_catalog->stockCheck();

//Add the data validations for blank data check for any reason blank data insert to the cart object
  if(trim($order->delivery['firstname'] . ' ' . $order->delivery['lastname']) == '' || trim($order->delivery['street_address']) == '' || trim($order->delivery['city']) == '' || trim($order->delivery['postcode']) == '' || 
	trim($order->delivery['state']) == '' || trim($order->delivery['country']['title']) == '' || trim($order->billing['firstname'] . ' ' . $order->billing['lastname']) == '' || trim($order->billing['street_address']) == '' || 
	trim($order->billing['city']) == '' || trim($order->billing['postcode']) == '' || trim($order->billing['state']) == '' || trim($order->billing['country']['title']) == '') {

	tep_redirect(tep_href_link(FILENAME_CHECKOUT, 'error=Data Validaion Error', 'SSL'));
	exit();
  }

  if($order->info['total'] > 0 && (trim($order->info['payment_method']) == '' || trim($_SESSION['payment']) == '')) {
	tep_redirect(tep_href_link(FILENAME_CHECKOUT, 'error=No Payment Method Selected', 'SSL'));
	exit();  
  }
  
  //Validate the payment methods with installed methods
  $arr_installed_payment_methods = explode(';', MODULE_PAYMENT_INSTALLED);
  if(in_array('paypal.php', $arr_installed_payment_methods)) {
	  array_push($arr_installed_payment_methods, "paypal_xc.php");
  }

  if($order->info['total'] > 0 && !in_array($_SESSION['payment'].'.php', $arr_installed_payment_methods)) {
	tep_redirect(tep_href_link(FILENAME_CHECKOUT, 'error=No Payment Method Selected', 'SSL'));
	exit();  
  }


// load the before_process function from the payment modules.
// Authorize.net/QuickCommerce/PlugnPlay processing - this called moved to a later point
// This is maintained for compatiblity with all other modules
 if(((defined('MODULE_PAYMENT_AUTHORIZENET_STATUS') && MODULE_PAYMENT_AUTHORIZENET_STATUS == 'True') && ($_SESSION['payment'] == 'authorizenet')) ||
    ((defined('MODULE_PAYMENT_PAYPAL_SMC_STATUS') && MODULE_PAYMENT_PAYPAL_SMC_STATUS =='True') && ($_SESSION['payment'] == 'paypal_smc')) ||
    ((defined('MODULE_PAYMENT_LOADEDPAYMENTS_STATUS') && MODULE_PAYMENT_LOADEDPAYMENTS_STATUS =='True') && ($_SESSION['payment'] == 'loadedpayments')) ||
    ((defined('MODULE_PAYMENT_PAYFLOWPRO_STATUS') && MODULE_PAYMENT_PAYFLOWPRO_STATUS =='True') && ($_SESSION['payment'] == 'payflowpro')) ||
    ((defined('MODULE_PAYMENT_CREMERCHANT_AUTHORIZENET_STATUS') && MODULE_PAYMENT_CREMERCHANT_AUTHORIZENET_STATUS == 'True') && ($_SESSION['payment'] == 'CREMerchant_authorizenet')) ||
    ((defined('MODULE_PAYMENT_CRESECURE_STATUS') && MODULE_PAYMENT_CRESECURE_STATUS == 'True') && (MODULE_PAYMENT_CRESECURE_BRANDED == 'False') && ($_SESSION['payment'] == 'cresecure')) ||
    ((defined('MODULE_PAYMENT_QUICKCOMMERCE_STATUS') && MODULE_PAYMENT_QUICKCOMMERCE_STATUS =='True') && ($_SESSION['payment'] == 'quickcommerce')) ||
    ((defined('MODULE_PAYMENT_PLUGNPAY_STATUS') && MODULE_PAYMENT_PLUGNPAY_STATUS =='True')  && ($_SESSION['payment'] == 'plugnpay'))){
   //don't load before process
  } elseif((defined('MODULE_PAYMENT_PAYPAL_STATUS') && MODULE_PAYMENT_PAYPAL_STATUS == 'True') && ($_SESSION['payment'] == 'paypal')) {
    if (isset($_SESSION['sub_payment']) && $_SESSION['sub_payment'] == 'paypal_wpp_dp') {
       if (isset($_GET['action']) && $_GET['action'] == 'cancel') tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
       $order->info['order_status'] = MODULE_PAYMENT_CRESECURE_ORDER_STATUS_COMPLETE_ID;
    } else {
      if( isset($order->info['total']) && $order->info['total'] > 0 ) {
        $payment_modules->before_process();
        include(DIR_WS_MODULES . 'payment/paypal/catalog/checkout_process.inc.php');
      }
    }
  } else {
    if ($_SESSION['payment'] == 'paypal_xc') {
      include_once('includes/modules/payment/paypal_xc.php');
      $ppxc = new paypal_xc();
      $ppxc->before_process();
    } else {
      $payment_modules->before_process();
    }
  }

  if ( lc_check_constant_val('PAYMENT_CC_CRYPT','True') && !empty($order->info['cc_number']) ){
   $cc_number1 = cc_encrypt($order->info['cc_number']);
   $cc_expires1 = cc_encrypt($order->info['cc_expires']);
  }else{
   $cc_number1 =$order->info['cc_number'];
   $cc_expires1 =$order->info['cc_expires'];
  }

  if ($order->info['payment_method'] == 'paypal_xc') $order->info['payment_method'] = 'PayPal Express Checkout';

  //Create the Order
  $insert_id = $lc_createOrder->insertOrder();

  // RCI code start
  echo $cre_RCI->get('checkoutprocess', 'logic', false);
  // RCI code eof

// Make sure the /catalog/includes/class/order.php is included
// and $order object is created before this!!!
// load the before_process function from the payment modules

//************
  if(defined('MODULE_PAYMENT_AUTHORIZENET_STATUS') && (MODULE_PAYMENT_AUTHORIZENET_STATUS == 'True') && ($_SESSION['payment'] == 'authorizenet')){
    //include(DIR_WS_MODULES . 'authorizenet_direct.php');
    $payment_modules->before_process();
  }
  if(defined('MODULE_PAYMENT_LOADEDPAYMENTS_STATUS') && (MODULE_PAYMENT_LOADEDPAYMENTS_STATUS == 'True') && ($_SESSION['payment'] == 'loadedpayments')){
    $payment_modules->before_process();
  }  
  // Payflow Pro
  if(defined('MODULE_PAYMENT_PAYFLOWPRO_STATUS') && (MODULE_PAYMENT_PAYFLOWPRO_STATUS == 'True') && ($_SESSION['payment'] == 'payflowpro')){
    include(DIR_WS_MODULES . 'payflowpro_direct.php');
    $payment_modules->before_process();
  }
  // CREMerchant_authorizenet
  if( defined('MODULE_PAYMENT_CREMERCHANT_AUTHORIZENET_STATUS') && (MODULE_PAYMENT_CREMERCHANT_AUTHORIZENET_STATUS == 'True') && ($_SESSION['payment'] == 'CREMerchant_authorizenet') ) {
    include(DIR_WS_MODULES . 'CREMerchant_authorizenet_direct.php');
    $payment_modules->before_process();
  }
 //quickcommerce
  if(defined('MODULE_PAYMENT_QUICKCOMMERCE_STATUS') && (MODULE_PAYMENT_QUICKCOMMERCE_STATUS =='True') && ($_SESSION['payment'] == 'quickcommerce')) {
    include(DIR_WS_MODULES . 'quickcommerce_direct.php');
    $payment_modules->before_process();
  }
  if(defined('MODULE_PAYMENT_PLUGNPAY_STATUS') && (MODULE_PAYMENT_PLUGNPAY_STATUS =='True')  && ($_SESSION['payment'] == 'plugnpay')) {
    include(DIR_WS_MODULES . 'plugnpay_api.php');
    $payment_modules->before_process();
  }
  // CRE Gateway
  if(defined('MODULE_PAYMENT_CRESECURE_STATUS') && (MODULE_PAYMENT_CRESECURE_STATUS == 'True') && (MODULE_PAYMENT_CRESECURE_BRANDED == 'False') && ($_SESSION['payment'] == 'cresecure')){
    include(DIR_WS_MODULES . 'cresecure_direct.php');
    $payment_modules->before_process();
  }
 //Paypal Smart Checkout 
  if(defined('MODULE_PAYMENT_PAYPAL_SMC_STATUS') && (MODULE_PAYMENT_PAYPAL_SMC_STATUS =='True')  && ($_SESSION['payment'] == 'paypal_smc')) {
    $payment_modules->before_process();
  } 
 
 
  //insert order total
  $lc_createOrder->insertOrderTotals($insert_id);

  //insert order History
  $customer_notification = (SEND_EMAILS == 'true') ? '1' : '0';
  $params['notification'] = $customer_notification;
  $params['comments'] = $order->info['comments'];
  $params['order_status_id'] = $order->info['order_status'];
  $lc_createOrder->insertOrderHistory($insert_id, $params);

  //insert order items
  $products_ordered = $lc_createOrder->insertOrderItem($insert_id);

  $order_total_modules->apply_credit();//ICW ADDED FOR CREDIT CLASS SYSTEM
  // lets start with the email confirmation

  // Include OSC-AFFILIATE - only if there is a affiliate_ref id available
  if (isset($_SESSION['affiliate_ref'])) {
    require(DIR_WS_INCLUDES . 'affiliate_checkout_process.php');
  }

  //send email to customers
  $payment = $_SESSION['payment'];
  if (is_object($$payment)) {
    $payment_class = $$payment;
    $payment_class_title = $payment_class->title;
    $payment_class_email_footer = ($payment_class->email_footer)?$payment_class->email_footer:'';
  } 
  $params['comments'] = $order->info['comments'];
  $params['products_ordered'] = $products_ordered;
  $params['noaccount'] = (isset($_SESSION['noaccount']))? 1 : 0;
  $params['payment_class_title'] = $payment_class_title;
  $params['payment_class_email_footer'] = $payment_class_email_footer;
  $products_ordered = $lc_createOrder->sendOrderEmail($insert_id, $params);
  
  // load the after_process function from the payment modules
  if ($_SESSION['payment'] == 'paypal_xc') {
    //include_once('includes/modules/payment/paypal_xc.php');
    //$ppxc = new paypal_xc();
    $ppxc->after_process();
  } else {
    $payment_modules->after_process();
  }

// AFSv1.0 - record the customers order and ip address info for fraud screening process

  $ip = $REMOTE_ADDR;
  $proxy = $HTTP_X_FORWARDED_FOR;
  if($proxy != ''){ $ip = $proxy; }
  $sql_data_array = array( 'order_id' => $insert_id,
                           'ip_address' => $ip);

  tep_db_perform('algozone_fraud_queries', $sql_data_array);

  //RCI for the after order processing	
  $cre_RCI->get('checkoutprocess', 'capture', false);

// End AFSv1.0
  $cart->reset(true);

// unregister session variables used during checkout
  unset($_SESSION['sendto']);
  unset($_SESSION['billto']);
  unset($_SESSION['shipping']);
  unset($_SESSION['payment']);
  unset($_SESSION['comments']);
  if (isset($_SESSION['cot_gv'])) {
    unset($_SESSION['cot_gv']);
  }
  if(isset($_SESSION['credit_covers']))   unset($_SESSION['credit_covers']);
  if(isset($_SESSION['sub_payment'])) unset($_SESSION['sub_payment']);
  // RCI code start
  echo $cre_RCI->get('checkoutprocess', 'unregister', false);

  $order_total_modules->clear_posts();//ICW ADDED FOR CREDIT CLASS SYSTEM
  tep_redirect(tep_href_link(FILENAME_CHECKOUT_SUCCESS, 'order_id='. $insert_id, 'SSL'));

?>