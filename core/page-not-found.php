<?php
/*
  Id: page-not-found.php,v 1.1.1.1 2004/03/04 23:37:52 ccwjr Exp 

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
  
  Copyright &copy; 2003-2005 Chain Reaction Works, Inc.
  
  Last Modified by : $AUTHOR$
  Latest Revision  : $REVISION$
  Last Revision Date : $DATE$
  License :  GNU General Public License 2.0
  
  http://creloaded.com
  http://creforge.com
  
*/

  header('HTTP/1.0 404 Not Found');
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PAGE_NOT_FOUND);
  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_PAGE_NOT_FOUND, '', 'SSL'));
  $content = CONTENT_PAGE_NOT_FOUND;
  require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);

?>
