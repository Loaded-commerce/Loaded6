<?php
/*
  Id: account.php,v 1.1.1.1 2004/03/04 23:37:52 ccwjr Exp

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License

  Copyright &copy; 2003-2005 Chain Reaction Works, Inc.

  Last Modified by : $AUTHOR$
  Latest Revision  : $REVISION$
  Last Revision Date : $DATE$
  License :  GNU General Public License 2.0

  http://creloaded.com
  http://creforge.com

*/
if (isset($_GET['token']) && substr($_GET['token'], 0, 2) == 'EC') {
  $_SESSION['xcToken'] = $_GET['token'];
  $_SESSION['xcPayerID'] = $_GET['PayerID'];
  $_SESSION['xcSet'] = TRUE;
}

//Check Registration status
//check_registrations();

// Reset $shipping if free shipping is on and weight is not 0
if (tep_get_configuration_key_value('MODULE_SHIPPING_FREESHIPPER_STATUS') == 'True' && $cart->show_weight()!=0) {
	if (isset($_SESSION['shipping'])) unset($_SESSION['shipping']);
}
if ( ! isset($_SESSION['customer_id']) ) {
	$navigation->set_snapshot();
	tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
}
// if there is nothing in the customers cart, redirect them to the shopping cart page
if ($cart->count_contents() < 1) {
	tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
}
// Validate Cart for checkout
$valid_to_checkout= true;
$cart->get_products(true);
if (!$valid_to_checkout) {
	$messageStack->add_session('header', ERROR_VALID_TO_CHECKOUT, 'error');
	tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
}

// if no shipping destination address was selected, use the customers own address as default
if (isset($_SESSION['shipping'])) {
	$shipping = $_SESSION['shipping'];
}
  // if no shipping destination address was selected, use the customers own address as default
  if ( ! isset($_SESSION['sendto']) ) {
    $_SESSION['sendto'] = $_SESSION['customer_default_address_id'];
  } else {
    //verify the selected shipping address
    $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$_SESSION['customer_id'] . "' and address_book_id = '" . (int)$_SESSION['sendto'] . "'");
    $check_address = tep_db_fetch_array($check_address_query);
    if ($check_address['total'] != '1') {
      $_SESSION['sendto'] = $_SESSION['customer_default_address_id'];
      if(isset($_SESSION['shipping']))   unset($_SESSION['shipping']);
    }
  }

  //Initialize the payment class	
  require(DIR_WS_CLASSES . 'payment.php');
  $payment_modules = new payment;
  $payment_modules->header_js();

  lc_check_addon_class('order.php');
  $order = new order;
  lc_check_addon_class('order_total.php');
  $order_total_modules = new order_total;
  $order_total_modules->collect_posts();
  $order_total_modules->pre_confirmation_check();
  $order_total_modules->process();

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CHECKOUT, '', 'SSL'));

  $content = CONTENT_CHECKOUT;
  $javascript = $content . '.js.php';

  require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);

?>