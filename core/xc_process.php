<?php
/*
  $Id: xc_process.php,v 1.0.0.0 2019/11/13 13:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2019 Loaded Commerce

  Released under the GNU General Public License

*/
require_once(DIR_WS_MODULES . 'payment/paypal_xc.php');

if(tep_paypal_xc_enabled()){
  $payment_modules = new paypal_xc();
  $payment_modules->ec_step1();
}
?>