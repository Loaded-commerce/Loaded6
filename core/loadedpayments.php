<?php
 /**
  @name       loadedpayments.php   
  @version    1.0.0 | 05-21-2012 | datazen
  @author     Loaded Commerce Core Team
  @copyright  (c) 2012 loadedcommerce.com
  @license    GPL2
*/
//require('includes/application_top.php');
  lc_check_addon_class('order.php');
  $order = new order;
require_once(DIR_WS_MODULES . 'payment/loadedpayments.php');
$loadedpayments_modules = new loadedpayments();
include(DIR_WS_LANGUAGES . $language . '/loadedpayments.php');
    $loginid = (defined('MODULE_PAYMENT_LOADEDPAYMENTS_USERNAME')) ? MODULE_PAYMENT_LOADEDPAYMENTS_USERNAME : '';
    $transactionkey = (defined('MODULE_PAYMENT_LOADEDPAYMENTS_TRANSKEY')) ? MODULE_PAYMENT_LOADEDPAYMENTS_TRANSKEY : '';  
    $sequence = rand(1, 1000); // a sequence number is randomly generated 
    $timestamp = time(); // a timestamp is generated 
    $amount = (float)number_format($order->info['total'], 2, '.', '');
    if( phpversion() >= "5.1.2" ) { 
      $fingerprint = hash_hmac("md5", $loginid . "" . $amount . "" . $sequence . "" . $timestamp . "", $transactionkey); 
    } else { 
      $fingerprint = bin2hex(mhash(MHASH_MD5, $loginid . "" . $amount . "" . $sequence . "" . $timestamp . "", $transactionkey)); 
    }      
    
   			     $process_button_array['loginid'] =$loginid;
                             $process_button_array['transactionkey'] =$transactionkey;
                             $process_button_array['firstname'] =(isset($order->billing['firstname'])) ? $order->billing['firstname'] : $order->customer['firstname'];
                             $process_button_array['lastname'] =(isset($order->billing['lastname'])) ? $order->billing['lastname'] : $order->customer['lastname'];
                             $process_button_array['address1'] =(isset($order->billing['street_address'])) ? $order->billing['street_address'] : $order->customer['street_address'];
                             $process_button_array['email'] =(isset($order->billing['email_address'])) ? $order->billing['email_address'] : $order->customer['email_address'];
                             $process_button_array['phone'] =(isset($order->billing['telephone'])) ? $order->billing['telephone'] : $order->customer['telephone'];
                             $process_button_array['city'] =(isset($order->billing['city'])) ? $order->billing['city'] : $order->customer['city']; 
                             $process_button_array['state'] =(isset($order->billing['state'])) ?  tep_get_zone_code($order->billing['country']['id'], $order->billing['zone_id'], '') :  tep_get_zone_code($order->customer['country']['id'], $order->customer['zone_id'], ''); 
                             $process_button_array['zip'] =(isset($order->billing['postcode'])) ? $order->billing['postcode'] : $order->customer['postcode'];
                             $process_button_array['country'] =(isset($order->billing['country']['iso_code_3'])) ? $order->billing['country']['iso_code_3'] : $order->customer['country']['iso_code_3'];
                             $process_button_array['amount'] =$amount;
                             $process_button_array['sequence'] =$sequence;
                             $process_button_array['timestamp'] =$timestamp;
                             $process_button_array['fingerprint'] =$fingerprint;
                             $process_button_array['invoicenumber'] =$order_id;
                             $process_button_array['ponumber'] =$order_id; 
                             $process_button_array['isRelayResponse'] ='T';
                             $process_button_array['customField1'] =tep_session_name();
                             $process_button_array['customField2'] =tep_session_id();
                             $process_button_array['customField3'] =$order_id;
                             $process_button_array['relayResponseURL'] =tep_href_link('loadedpayments-relay.php', '', 'SSL', false, false);
                             
    //if (defined('MODULE_PAYMENT_LOADEDPAYMENTS_FORM_URL') && MODULE_PAYMENT_LOADEDPAYMENTS_FORM_URL != NULL) {   
    //} else {                     
      $process_button_array['includeMerchantName'] ='F';
      $process_button_array['readonlyorderdetail'] ='F';
      $process_button_array['emailReceipt'] ='T';
      $process_button_array['includePO'] ='F';
      $process_button_array['includeInvoice'] ='F';
      $process_button_array['hideAddress'] ='T';
      $process_button_array['styleSheetURL'] ='https://qa.loadedcommerce.com/cedev/loadedpayments.css'; 
    //}
    //print_r($process_button_array);exit;
function rePost() {
global $process_button_array;
  foreach ($process_button_array as $key => $value) {
    echo '<input type="hidden" name="' . $key . '" value="' . $value . '">' . "\n";
  }
}


/*if (file_exists(DIR_WS_CLASSES . 'rci.php')) { // is CRE                                                   
  $breadcrumb->add(HEADING_TITLE, tep_href_link('loadedpayments.php', '', 'NONSSL'));
  $content = 'loadedpayments';
  require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);
} else if (file_exists(DIR_WS_INCLUDES . 'column_left.php')) { // is osc 2.2
  ?>
  <!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
  <html <?php echo HTML_PARAMS; ?>>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
    <title><?php echo TITLE; ?></title>
    <base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
  </head>
  <body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
    <!-- header //-->
    <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
    <!-- header_eof //-->
    <!-- body //-->
    <table border="0" width="100%" cellspacing="3" cellpadding="3">
      <tr>
        <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="0" cellpadding="2">
          <!-- left_navigation //-->
          <?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
          <!-- left_navigation_eof //-->
        </table></td>
        <!-- body_text //-->
        <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                <td class="pageHeading" align="right"><?php echo tep_image(DIR_WS_IMAGES . 'table_background_specials.gif', HEADING_TITLE, HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
          <tr>
            <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
              <tr class="infoBoxContents">
                <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="main">
                      <div id="container" style="position:relative;">
                        <form name="pmtForm" id="pmtForm" action="<?php echo $_SESSION['payform_url']; ?>" target="pmtFrame" method="post"><?php rePost(); ?></form>        
                        <div id="loadingContainer"  style="position: absolute; left:220px; top:100px;"><p><img border="0" src="images/lp-loading.png"></p></div>
                        <iframe frameborder="0" onload="setTimeout(function() {hideLoader();},1250);" src="" id="pmtFrame" name="pmtFrame" height="300px" width="606px" scrolling="no" marginheight="0" marginwidth="0">Your browser does not support iframes.</iframe> 
                      </div>
                    </td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
          <!-- body_text_eof //-->
        </td></table>
        <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="0" cellpadding="2">
          <!-- right_navigation //-->
          <?php require(DIR_WS_INCLUDES . 'column_right.php'); ?>
          <!-- right_navigation_eof //-->
        </table></td>
      </tr>
    </table>
    <!-- body_eof //-->
    <!-- footer //-->
    <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
    <!-- footer_eof //-->
    <br>
    <script>
      function hideLoader() {
        var loadDiv = document.getElementById("loadingContainer"); 
        loadDiv.style.display = "none"; 
      }
      
      window.onload = function(){
        document.forms["pmtForm"].submit();
      };        
    </script>
  </body>
  </html>
  <?php
} else { // is osc 2.3  
*/
  $breadcrumb->add('Loaded Payments', tep_href_link('loadedpayments.php')); 
 // require(DIR_WS_INCLUDES . 'template_top.php');
  ?>
  <style type="text/css">
  .footer-adv{display:none !important}
  </style>
  <h1><?php echo HEADING_TITLE; ?></h1>
  <div class="contentContainer">
    <div class="contentText" style="position:relative;">
      <form name="pmtForm" id="pmtForm" action="<?php echo $_SESSION['payform_url']; ?>" target="pmtFrame" method="post"><?php rePost(); ?></form>        
      <div id="loadingContainer"  style="position: absolute; left:220px; top:100px;"><p><img border="0" src="images/lp-loading.png"></p></div>
      <iframe frameborder="0" onload="setTimeout(function() {hideLoader();},1250);" src="" id="pmtFrame" name="pmtFrame" height="300px" width="606px" scrolling="no" marginheight="0" marginwidth="0">Your browser does not support iframes.</iframe> 
    </div>
    <script>
      function hideLoader() {
        var loadDiv = document.getElementById("loadingContainer"); 
        loadDiv.style.display = "none"; 
      }
      
      window.onload = function(){
        document.forms["pmtForm"].submit();
      };        
    </script>    
  </div>
  <?php
  //require(DIR_WS_INCLUDES . 'template_bottom.php'); 
//}
/*require(DIR_WS_INCLUDES . 'application_bottom.php');
*/
?>