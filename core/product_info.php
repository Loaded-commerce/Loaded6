<?php
/*
  $Id: product_info.php,v 1.1.1.1 2004/03/04 23:38:02 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/


  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PRODUCT_INFO);

  if (isset($_GET['werror']) && $_GET['werror'] == 1) {
	  $error = true;
	  if (PRODUCT_INFO_SUB_PRODUCT_ADDCART_TYPE == 'Checkbox') {
		$messageStack->add('frontend_message', WISHLIST_SUB_PRODUCT_CHECKBOX_ERROR);
	  } else {
		$messageStack->add('frontend_message', WISHLIST_SUB_PRODUCT_INPUT_ERROR);
	  }
  }

  //Initialize the Catalog object
  $obj_product_info = new productInfo((int)$_GET['products_id']);

  $product_query = "SELECT count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int)$_GET['products_id'] . "' and pd.products_id = p.products_id and pd.language_id = '" . (int)$languages_id . "'";
  $product_query .= $obj_catalog->b2b_filter_sql('p.products_group_access');
  $product_check_query = tep_db_query($product_query);
  $product_check = tep_db_fetch_array($product_check_query);

  $content = CONTENT_PRODUCT_INFO;
  $javascript = 'popup_window.js';

  require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);

?>
