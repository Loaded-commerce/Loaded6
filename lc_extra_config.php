<?php
/*
  Loaded Commerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce

  Released under the GNU General Public License
*/

define('DIR_FS_ADDONS', DIR_FS_CATALOG . 'addons/');
$arr_direct_page = array('paypal_xc_ipn.php', 'ipn.php', 'idealm.php'); // Define the pages you want to access directly in the site.
