<?php
global $content, $obj_catalog, $js_attribute_validation_code, $cart, $currencies;
echo $obj_catalog->print_footer_block();
$arr_contents = array('index_products', 'featured_products', 'specials','products_new','advanced_search_result');
?>
<div id="AllcommonModalpopup" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      	<h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body AllcommonModalcontainer">

      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>
<?php
if(in_array($content, $arr_contents))
{
?>
<script>
equalheight = function(container) {
  var currentTallest = 0,
      currentRowStart = 0,
      rowDivs = new Array(),
      $el,
      topPosition = 0;
  $(container).each(function() {
    $el = $(this);
    //$($el).height('auto')
    topPostion = $el.position().top;
    if (currentRowStart != topPostion) {
      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
      rowDivs.length = 0; // empty the array
      currentRowStart = topPostion;
      currentTallest = $el.height();
      rowDivs.push($el);
    } else {
      rowDivs.push($el);
      currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
    }
    for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
      rowDivs[currentDiv].height(currentTallest);
    }
  });
}

$(window).on("load",function(){
  equalheight('.product-listing-module-container .itembox');
});

$(window).resize(function(){
  equalheight('.product-listing-module-container .itembox');
});
</script>
<?php
}
?>
<?php if($content == 'product_info' || $content == 'product_reviews') { ?>
<script>
function writereview(pID){
	var params = 'products_id=' + pID;
	$.ajax({
		type:'post',
		data: params,
		url:'ajax_handler.php?action=review_form',
		success:function(retval)
		{
			$("#AllcommonModalpopup .modal-body ").html(retval);
			$("#AllcommonModalpopup .modal-title ").html('Add Products Review');
			$("#AllcommonModalpopup").modal("toggle");
	    }
	});
}

function ajaxAddReview(){
	params = $('#review_form').serialize();
	$.ajax({
		type:'post',
		data: params,
		url:'ajax_handler.php?action=ajax_add_review',
		success:function(retval)
		{
			if(retval != ''){
				$('#reviewerror').html('<font style="color:#ff0000">'+retval+'</font>')
			}else{
				location.reload();
			}
	    }
	});

	return false;
}

$(document).ready(function() {
 $("input[type='radio']").click(function() {
  var sim = $("input[type='radio']:checked").val();
  if (sim<3) { $('.myratings').css('color','red'); $(".myratings").text(sim); } else { $('.myratings').css('color','green'); $(".myratings").text(sim); } 
 });
});
</script>
<?php } ?>
<script>
<?php
echo $obj_catalog->print_footer_js();
?>
var app = angular.module('lccart', []);

app.controller('catalog', function($scope) {
  $scope.itemCount = "<?php echo $cart->count_contents(); ?>";
  $scope.cartContentsShort = "<?php echo 'total:'. $currencies->format($cart->show_total()) ;?> (<?php echo $cart->count_contents(); ?>)";

    $scope.loadCart = function (retval) {
	  $scope.itemCount = retval.itemCount;
	  $scope.cartContentsShort = retval.cartContentsShort;
    };
});
</script>
