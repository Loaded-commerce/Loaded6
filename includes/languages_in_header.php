<?php
/*
  language_header.php

  Shoppe Enhancement Controller - Copyright (c) 2003 WebMakers.com
  Linda McGrath - osCommerce@WebMakers.com

*/
?>
<!-- languages_header //-->
        <table>
          <tr>
            <td>
<?php
  if (!is_object($lng)) {
    include(DIR_WS_CLASSES . 'language.php');
    $lng = new language;
  }

  if (getenv('HTTPS') == 'on') $connection = 'SSL';
  else $connection = 'NONSSL';

  $languages_string = '';
  reset($lng->catalog_languages);
  foreach($lng->catalog_languages as $key => $value) {
    $languages_string .= ' <a href="' . tep_href_link(basename($_SERVER['PHP_SELF']), tep_get_all_get_params(array('language', 'currency')) . 'language=' . $key, $connection) . '">' . tep_image(DIR_WS_LANGUAGES .  $value['directory'] . '/images/' . $value['image'], $value['name']) . '</a> ';
  }

echo $languages_string;
?>
            </td>
          </tr>
        </table>
<!-- languages_header_eof //-->
