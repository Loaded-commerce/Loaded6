<?php
/*
  $Id: form_check.js.php,v 1.1.1.1 2004/03/04 23:40:52 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
?>
<script type="text/javascript"><!--
var form = "";
var submitted = false;
var error = false;
var error_message = "";

function check_input(field_name, field_size, message) {
  if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
    var field_value = form.elements[field_name].value;

    if (field_value == '' || field_value.length < field_size) {
      error_message = error_message + "* " + message + "\n";
      $('input[name='+field_name+']').css({border:'1px solid #ff0000', background:'#E5C9C9'});
      error = true;
    }
  }
}

function check_radio(field_name, message) {
  var isChecked = false;

  if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
    var radio = form.elements[field_name];

    for (var i=0; i<radio.length; i++) {
      if (radio[i].checked == true) {
        isChecked = true;
        break;
      }
    }

    if (isChecked == false) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}

function check_select(field_name, field_default, message) {
  if (form.elements[field_name] && (form.elements[field_name].type != "hidden")) {
    var field_value = form.elements[field_name].value;

    if (field_value == field_default) {
      error_message = error_message + "* " + message + "\n";
      error = true;
    }
  }
}

function check_password(field_name_1, field_name_2, field_size, message_1, message_2) {
  if (form.elements[field_name_1] && (form.elements[field_name_1].type != "hidden")) {
    var password = form.elements[field_name_1].value;
    var confirmation = form.elements[field_name_2].value;

    if (password == '' || password.length < field_size) {
      error_message = error_message + "* " + message_1 + "\n";
      $('input[name='+field_name_1+']').css({border:'1px solid #ff0000', background:'#E5C9C9'});
      error = true;
    } else if (password != confirmation) {
      error_message = error_message + "* " + message_2 + "\n";
      $('input[name='+field_name_1+']').css({border:'1px solid #ff0000', background:'#E5C9C9'});
      error = true;
    }
  }
}

function check_password_new(field_name_1, field_name_2, field_name_3, field_size, message_1, message_2, message_3) {
  if (form.elements[field_name_1] && (form.elements[field_name_1].type != "hidden")) {
    var password_current = form.elements[field_name_1].value;
    var password_new = form.elements[field_name_2].value;
    var password_confirmation = form.elements[field_name_3].value;

    if (password_current == '' || password_current.length < field_size) {
      error_message = error_message + "* " + message_1 + "\n";
      $('input[name='+field_name_1+']').css({border:'1px solid #ff0000', background:'#E5C9C9'});
      error = true;
    } else if (password_new == '' || password_new.length < field_size) {
      error_message = error_message + "* " + message_2 + "\n";
      $('input[name='+field_name_1+']').css({border:'1px solid #ff0000', background:'#E5C9C9'});
      error = true;
    } else if (password_new != password_confirmation) {
      error_message = error_message + "* " + message_3 + "\n";
      $('input[name='+field_name_1+']').css({border:'1px solid #ff0000', background:'#E5C9C9'});
      error = true;
    }
  }
}

function check_form(form_name) { //alert('herere');
  if (submitted == true) {
    alert("<?php echo JS_ERROR_SUBMITTED; ?>");
    return false;
  }

  error = false;
  form = form_name;
  error_message = "<?php echo JS_ERROR; ?>";

  var is_guest_allow = "<?php echo PWA_ON; ?>";
  check_input("firstname", <?php echo lc_defined_constant('ENTRY_FIRST_NAME_MIN_LENGTH'); ?>, "<?php echo lc_defined_constant('ENTRY_FIRST_NAME_ERROR'); ?>");
  check_input("lastname", <?php echo lc_defined_constant('ENTRY_LAST_NAME_MIN_LENGTH'); ?>, "<?php echo lc_defined_constant('ENTRY_LAST_NAME_ERROR'); ?>");
  check_input("email_address_creat", <?php echo lc_defined_constant('ENTRY_EMAIL_ADDRESS_MIN_LENGTH'); ?>, "<?php echo lc_defined_constant('ENTRY_EMAIL_ADDRESS_ERROR'); ?>");

if(($("#show_passwords").is(":checked")) || is_guest_allow == 'false'){
  check_password("password", "confirmation", <?php echo lc_defined_constant('ENTRY_PASSWORD_MIN_LENGTH'); ?>, "<?php echo lc_defined_constant('ENTRY_PASSWORD_ERROR'); ?>", "<?php echo lc_defined_constant('ENTRY_PASSWORD_ERROR_NOT_MATCHING'); ?>");
  check_password_new("password_current", "password_new", "password_confirmation", <?php echo lc_defined_constant('ENTRY_PASSWORD_MIN_LENGTH'); ?>, "<?php echo lc_defined_constant('ENTRY_PASSWORD_ERROR'); ?>", "<?php echo lc_defined_constant('ENTRY_PASSWORD_NEW_ERROR'); ?>", "<?php echo lc_defined_constant('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING'); ?>");
}

if($("#show_company").is(":checked")){
  check_input("company", <?php echo lc_defined_constant('ENTRY_COMPANY_MIN_LENGTH'); ?>, "<?php echo lc_defined_constant('ENTRY_COMPANY_NAME_ERROR'); ?>");
  check_input("company_tax_id", "", "<?php echo lc_defined_constant('ENTRY_COMPANY_TAXID_ERROR'); ?>");
}
  if (error == true) {
    alert(error_message);
    return false;
  } else {
    submitted = true;
    return true;
  }
}
//-->

function show_companyadd(){
  var companyval = $("input[name='show_company']").val();
  if($("#show_company").is(":checked")){
	$("#show_company_field").slideDown(800);
  }else{
	$("#show_company_field").slideUp(800);
  }
}

function show_custpasswords(){
  var showpassval = $("input[name='show_passwords']").val();
  if($("#show_passwords").is(":checked")){
	$("#show_password_fields").slideDown(800);
  }else{
	$("#show_password_fields").slideUp(800);
  }
}

function calculate_shipping_estimate()
{
  var customer_id = "<?php echo $_SESSION['customer_id']; ?>";
  var country_id = $("select[name='country_id'] option:selected").val();
  var zip_code = $("input[name='zip_code']").val(); //alert(country_id);
  params = 'country_id='+country_id+'&zip_code='+zip_code;
  if(customer_id == ''){ $('#loader-cart').css('display', 'block'); }
	$.ajax({
		type:'post',
		data: params,
		url: 'ajax_handler.php?action=getshippingestimate',
		success:function(retval){
			//alert(retval);
			$('#calculate-shipping').html(retval);
			$(".search-js-select").select2({ width: '25%' });
			$('#loader-cart').css('display', 'none');
       }
	});
}

function redeem()
{
var redeemval = $("input[name='gv_redeem_code']").val();
	if(redeemval == ''){
	alert("Please enter Coupon code!!");
	return false;
	} else{
    $( "#cart_quantity" ).submit();
	}

}

function checkemail(form_name){
  check_form(form_name);
  var email_address_value = $("input[name='email_address_creat']").val();
  var firstname_value = $("input[name='firstname']").val();
  var lastname_value = $("input[name='lastname']").val();
  var company_value = $("input[name='company']").val();
  var company_tax_id_value = $("input[name='company_tax_id']").val();
  var guest_edit = $("input[name='guest_edit']").val();
  var action = $("input[name='action']").val();
  var ccode = $("input[name='ccode']").val();
  if(email_address_value != ''){
    params = 'emailadd='+email_address_value;
	$.ajax({
		type:'post',
		data: params,
		url: 'ajax_handler.php?action=checkemailexists',
		success:function(retval){
         if(retval == 'exist'){
          if((guest_edit != 1 && $("#show_passwords").is(":checked")) || action == 'process'){
           $('#emailavailable').modal('show');
           $('input[name="email_address_creat"]').val(email_address_value);
           $('input[name="firstname"]').val(firstname_value);
           $('input[name="lastname"]').val(lastname_value);
           $('input[name="company"]').val(company_value);
           $('input[name="company_tax_id"]').val(company_tax_id_value);
           $('input[name="ccode"]').val(ccode);
          }else{
		   form_name.submit();
		   return true;
          }

		 }else{
		   form_name.submit();
		   return true;
		 }

       }
	});
   return false;
  }
}
function checkout_asguest(formname)
{
   var email_address = $("input[name='email_address']").val();
   formname.submit();
}
function login_withexists()
{
   var email_address = $("input[name='email_address']").val();
   //active login tab when we click on the login button from modal
   $(".login_acc_tab").removeClass("fade");
   $(".login_acc_tab").addClass("active");
    $(".login_acc").addClass("active");
   //deactive create account tab when we click on the login button from modal
   $(".create_acc_tab").removeClass("active");
   $(".create_acc_tab").addClass("fade");
   $(".creat_acc").removeClass("active");

}
</script>
