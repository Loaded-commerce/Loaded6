<script language="javascript">
function showCCSection(pmethod, selectedval){
    if(selectedval == ''){
       $( '#'+pmethod+'_cc_section' ).removeClass( 'd-none' );
       $( '#'+pmethod+'_cc_section' ).addClass( 'd-inline' );
    }else{
       $( '#'+pmethod+'_cc_section' ).removeClass( 'd-inline' );
       $( '#'+pmethod+'_cc_section' ).addClass( 'd-none' );
  }

}
function openeditbillingaddress(billaddrid){
		$.ajax({
		type: 'post',
		url: 'ajax_handler.php?action=editbillingaddresshtml',
		data: {'billaddrid':billaddrid},
		success: function (retval) {
			$('#modaleditbilladdressmodal').html(retval);
			$('#editbillingaddress').modal('show');
			$(".search-js-select").select2({ width: '95%' });
			$("#state_id select").select2({ width: '95%' });
			$("#billing_state_id select").select2({ width: '95%' });
		}
	});
}
function openeditshippingaddress(shipaddrid){
		$.ajax({
		type: 'post',
		url: 'ajax_handler.php?action=editshippingaddresshtml',
		data: {'shipaddrid':shipaddrid},
		success: function (retval) {
			$('#modaleditaddressmodal').html(retval);
			$('#editshippingaddress').modal('show');
			$(".search-js-select").select2({ width: '95%' });
			$("#state_id select").select2({ width: '95%' });
			$("#billing_state_id select").select2({ width: '95%' });
		}
	});
}
function updatebillinginfo(){
	error = false;
	var params;
	error_message = "Errors have occurred during the process of your form.\nPlease make the following corrections:\n";
	var bill_firstname = $("#modaleditbilladdressmodal input[name=bill_firstname]").val();
	var bill_lastname = $("#modaleditbilladdressmodal input[name=bill_lastname]").val();
	var bill_stree_address = $("#modaleditbilladdressmodal input[name=bill_stree_address]").val();
	var bill_city = $("#modaleditbilladdressmodal input[name=bill_city]").val();
	var bill_state = $("#modaleditbilladdressmodal select[name=bill_state]").val();
	var bill_stateinput = $("#modaleditbilladdressmodal input[name=bill_state]").val();
	var bill_postalcode = $("#modaleditbilladdressmodal input[name=bill_postalcode]").val();
	var bill_country = $("#modaleditbilladdressmodal select[name=bill_country]").val();
	var bill_telephone = $("#modaleditbilladdressmodal input[name=bill_telephone]").val();
	if(bill_firstname == ''){
		error = true;
		error_message += 'Please Enter the Shipping Address. \n';
		$('input[name=bill_firstname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=bill_firstname]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(bill_lastname == ''){
		error = true;
		error_message += 'Please Enter the Shipping Address. \n';
		$('input[name=bill_lastname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=bill_lastname]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(bill_stree_address == ''){
		error = true;
		error_message += 'Please Enter the Shipping Address. \n';
		$('input[name=bill_stree_address]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=bill_stree_address]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(bill_stree_address == ''){
		error = true;
		error_message += 'Please Enter the Shipping Address. \n';
		$('input[name=bill_stree_address]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=bill_stree_address]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(bill_city == ''){
		error = true;
		error_message += 'Please Enter the Shipping City. \n';
		$('input[name=bill_city]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=bill_city]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(bill_state == '' || bill_stateinput == ''){
		error = true;
		error_message += 'Please Select the Shipping State. \n';
		$('select[name=bill_state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		$('input[name=bill_state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('select[name=bill_state]').css({border:'1px solid #ced4da', background:'#fff'});
		$('input[name=bill_state]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(bill_postalcode == ''){
		error = true;
		error_message += 'Please Enter the Shipping Postal Code. \n';
		$('input[name=bill_postalcode]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=bill_postalcode]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(bill_country == ''){
		error = true;
		error_message += 'Please Select the Shipping Country. \n';
		$('select[name=bill_country]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('select[name=bill_country]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(bill_telephone == ''){
		error = true;
		error_message += 'Please Enter the Shipping Phone Number. \n';
		$('input[name=bill_telephone]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=bill_telephone]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	params = $("#editmybillinginfo").serialize();
	if(error == true){
		alert(error_message);
		return false;
	} else {
		$('#loader-cart').css('display', 'block');
		$.ajax({
			type: 'post',
			url: 'ajax_handler.php?action=addshippingaddress',
			 data: {'data':params},
			success: function (retval) {
					$('#ajaxreloadDiv').html(retval);
					$('#loader-cart').css('display', 'none');
					$('#editbillingaddress').modal('hide');
					setshippingAddress();
					setbillingAddress();
					var checkmethod = $('input[name=shipping]:checked').val();
					if(checkmethod != 'undefined'){
						shipping = 'shipping='+checkmethod;
						updateOrderTotal(shipping);
					}
			}
		});
	}
}
function updateshippinginfo(){
	error = false;
	var params;
	error_message = "Errors have occurred during the process of your form.\nPlease make the following corrections:\n";
	var firstname = $("#shippingformdetails input[name=firstname]").val();
	var lastname = $("#shippingformdetails input[name=lastname]").val();
	var stree_address = $("#shippingformdetails input[name=stree_address]").val();
	var city = $("#shippingformdetails input[name=city]").val();
	var state = $("#shippingformdetails select[name=state]").val();
	var stateinput = $("#shippingformdetails input[name=state]").val();
	var postalcode = $("#shippingformdetails input[name=postalcode]").val();
	var country = $("#shippingformdetails select[name=country]").val();
	var telephone = $("#shippingformdetails input[name=telephone]").val();
	params = $("#editmyshippinginfo").serialize();
	if(firstname == ''){
		error = true;
		error_message += 'Please Enter the Shipping Firstname. \n';
		$('input[name=firstname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=firstname]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(lastname == ''){
		error = true;
		error_message += 'Please Enter the Shipping Lastname. \n';
		$('input[name=lastname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=lastname]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(stree_address == ''){
		error = true;
		error_message += 'Please Enter the Shipping Address. \n';
		$('input[name=stree_address]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=stree_address]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(city == ''){
		error = true;
		error_message += 'Please Enter the Shipping City. \n';
		$('input[name=city]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=city]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(state == '' || stateinput == ''){
		error = true;
		error_message += 'Please Select the Shipping State. \n';
		$('select[name=state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		$('input[name=state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('select[name=state]').css({border:'1px solid #ced4da', background:'#fff'});
		$('input[name=state]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(postalcode == ''){
		error = true;
		error_message += 'Please Enter the Shipping Postal Code. \n';
		$('input[name=postalcode]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=postalcode]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(country == ''){
		error = true;
		error_message += 'Please Select the Shipping Country. \n';
		$('select[name=country]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('select[name=country]').css({border:'1px solid #ced4da', background:'#fff'});
	}
	if(telephone == ''){
		error = true;
		error_message += 'Please Enter the Shipping Phone Number. \n';
		$('input[name=telephone]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
	}else{
		$('input[name=telephone]').css({border:'1px solid #ced4da', background:'#fff'});
	}

	if(error == true){
		alert(error_message);
		return false;
	} else {
		$('#loader-cart').css('display', 'block');
		$.ajax({
			type: 'post',
			url: 'ajax_handler.php?action=addshippingaddress',
			 data: {'data':params},
			success: function (retval) {
					$('#ajaxreloadDiv').html(retval);
					$('#loader-cart').css('display', 'none');
					$('#editshippingaddress').modal('hide');
					setshippingAddress();
					setbillingAddress();
					var checkmethod = $('input[name=shipping]:checked').val();
					if(checkmethod != 'undefined'){
						shipping = 'shipping='+checkmethod;
						updateOrderTotal(shipping);
					}

			}
		});
	}
}
function updateaccountinfo(){
	$('#loader-cart').css('display', 'block');
	params = $("#editaccountinfo").serialize();
	$.ajax({
		type: 'post',
		url: 'ajax_handler.php?action=updateaccounfinormation',
		 data: {'data':params},
		success: function (retval) {
			$('#ajaxinforeloadDiv').html(retval);
			$('#loader-cart').css('display', 'none');
			$('#editaccount').modal('hide');
		}
	});
}
function check_form(shippingaddress){
	error = false;
	var params;
	error_message = "Errors have occurred during the process of your form.\nPlease make the following corrections:\n";
	var firstname = $("input[name=firstname]").val();
	var lastname = $("input[name=lastname]").val();
	var stree_address = $("input[name=stree_address]").val();
	var city = $("input[name=city]").val();
	var state = $("select[name=state]").val();
	var stateinput = $("input[name=state]").val();
	var postalcode = $("input[name=postalcode]").val();
	var country = $("select[name=country]").val();
	var telephone = $("input[name=telephone]").val();
	var selectedbilladdress = $("select[name=choosebillingaddr]").val();

	<?php if(isset($_SESSION['noaccount'])){ ?>
		var addressselect = 'new';
	<?php } else { ?>
		var addressselect = $('input[name=addressselect]:checked').val();
	<?php } ?>

	if(addressselect == 'new'){
		if($('input[name=shipbilladdr]').is(":checked")) {

		} else {
			if(selectedbilladdress == '' ){
				error = true;
				error_message += 'Please Select Or Enter a New Billing Address. \n';
			}
		}
	} else {
		if(!$('input[name=shipbilladdr]').is(":checked")) {
			if(selectedbilladdress == '' ){
				error = true;
				error_message += 'Please Select Or Enter a New Billing Address. \n';
			}
		}
	}

<?php if(!isset($_SESSION['noaccount'])){ ?>
	if(selectedbilladdress == 'addnew' && !$('input[name=shipbilladdr]').is(":checked")){
		var bill_firstname = $("input[name=bill_firstname]").val();
		var bill_lastname = $("input[name=bill_lastname]").val();
		var bill_stree_address = $("input[name=bill_stree_address]").val();
		var bill_city = $("input[name=bill_city]").val();
		var bill_state = $("select[name=bill_state]").val();
		var bill_stateinput = $("input[name=bill_state]").val();
		var bill_postalcode = $("input[name=bill_postalcode]").val();
		var bill_country = $("select[name=bill_country]").val();
		var bill_telephone = $("input[name=bill_telephone]").val();
		if(bill_firstname == ''){
			error = true;
			error_message += 'Please Enter the Shipping Address. \n';
			$('input[name=bill_firstname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_firstname]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_lastname == ''){
			error = true;
			error_message += 'Please Enter the Shipping Address. \n';
			$('input[name=bill_lastname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_lastname]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_stree_address == ''){
			error = true;
			error_message += 'Please Enter the Shipping Address. \n';
			$('input[name=bill_stree_address]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_stree_address]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_stree_address == ''){
			error = true;
			error_message += 'Please Enter the Shipping Address. \n';
			$('input[name=bill_stree_address]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_stree_address]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_city == ''){
			error = true;
			error_message += 'Please Enter the Shipping City. \n';
			$('input[name=bill_city]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_city]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_state == '' || bill_stateinput == ''){
			error = true;
			error_message += 'Please Select the Shipping State. \n';
			$('select[name=bill_state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
			$('input[name=bill_state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('select[name=bill_state]').css({border:'1px solid #ced4da', background:'#fff'});
			$('input[name=bill_state]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_postalcode == ''){
			error = true;
			error_message += 'Please Enter the Shipping Postal Code. \n';
			$('input[name=bill_postalcode]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_postalcode]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_country == ''){
			error = true;
			error_message += 'Please Select the Shipping Country. \n';
			$('select[name=bill_country]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('select[name=bill_country]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_telephone == ''){
			error = true;
			error_message += 'Please Enter the Shipping Phone Number. \n';
			$('input[name=bill_telephone]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_telephone]').css({border:'1px solid #ced4da', background:'#fff'});
		}
	}
<?php } else { ?>
	if(!$('input[name=shipbilladdr]').is(":checked")){
		var bill_firstname = $("input[name=bill_firstname]").val();
		var bill_lastname = $("input[name=bill_lastname]").val();
		var bill_stree_address = $("input[name=bill_stree_address]").val();
		var bill_city = $("input[name=bill_city]").val();
		var bill_state = $("select[name=bill_state]").val();
		var bill_stateinput = $("input[name=bill_state]").val();
		var bill_postalcode = $("input[name=bill_postalcode]").val();
		var bill_country = $("select[name=bill_country]").val();
		var bill_telephone = $("input[name=bill_telephone]").val();
		if(bill_firstname == ''){
			error = true;
			error_message += 'Please Enter the Billing Firstname. \n';
			$('input[name=bill_firstname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_firstname]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_lastname == ''){
			error = true;
			error_message += 'Please Enter the Billing Lastname. \n';
			$('input[name=bill_lastname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_lastname]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_stree_address == ''){
			error = true;
			error_message += 'Please Enter the Billing Address. \n';
			$('input[name=bill_stree_address]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_stree_address]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_city == ''){
			error = true;
			error_message += 'Please Enter the Billing City. \n';
			$('input[name=bill_city]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_city]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_state == '' || bill_stateinput == ''){
			error = true;
			error_message += 'Please Select the Billing State. \n';
			$('select[name=bill_state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
			$('input[name=bill_state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('select[name=bill_state]').css({border:'1px solid #ced4da', background:'#fff'});
			$('input[name=bill_state]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_postalcode == ''){
			error = true;
			error_message += 'Please Enter the Billing Postal Code. \n';
			$('input[name=bill_postalcode]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_postalcode]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_country == ''){
			error = true;
			error_message += 'Please Select the Billing Country. \n';
			$('select[name=bill_country]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('select[name=bill_country]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(bill_telephone == ''){
			error = true;
			error_message += 'Please Enter the Billing Phone Number. \n';
			$('input[name=bill_telephone]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=bill_telephone]').css({border:'1px solid #ced4da', background:'#fff'});
		}
	}
<?php } ?>

    if(addressselect == 'new'){
		if(firstname == ''){
			error = true;
			error_message += 'Please Enter the Shipping Firstname. \n';
			$('input[name=firstname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=firstname]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(lastname == ''){
			error = true;
			error_message += 'Please Enter the Shipping Lastname. \n';
			$('input[name=lastname]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=lastname]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(stree_address == ''){
			error = true;
			error_message += 'Please Enter the Shipping Address. \n';
			$('input[name=stree_address]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=stree_address]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(city == ''){
			error = true;
			error_message += 'Please Enter the Shipping City. \n';
			$('input[name=city]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=city]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(state == '' || stateinput == ''){
			error = true;
			error_message += 'Please Select the Shipping State. \n';
			$('select[name=state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
			$('input[name=state]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('select[name=state]').css({border:'1px solid #ced4da', background:'#fff'});
			$('input[name=state]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(postalcode == ''){
			error = true;
			error_message += 'Please Enter the Shipping Postal Code. \n';
			$('input[name=postalcode]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=postalcode]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(country == ''){
			error = true;
			error_message += 'Please Select the Shipping Country. \n';
			$('select[name=country]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('select[name=country]').css({border:'1px solid #ced4da', background:'#fff'});
		}
		if(telephone == ''){
			error = true;
			error_message += 'Please Enter the Shipping Phone Number. \n';
			$('input[name=telephone]').css({border:'1px solid #ff0000', background:'#E5C9C9'});
		}else{
			$('input[name=telephone]').css({border:'1px solid #ced4da', background:'#fff'});
		}
	}
	if(error == true){
		alert(error_message);
		return false;
	} else {
		$('#loader-cart').css('display', 'block');
		params = $("#shippingaddress").serialize();
		$.ajax({
			type: 'post',
			url: 'ajax_handler.php?action=addshippingaddress',
			 data: {'data':params},
			success: function (retval) {
				//alert(retval);
				$('#ajaxreloadDiv').html(retval);
				$('#loader-cart').css('display', 'none');
				setshippingAddress();
				setbillingAddress();
				var checkmethod = $('input[name=shipping]:checked').val();
				if(checkmethod != 'undefined'){
					shipping = 'shipping='+checkmethod;
					updateOrderTotal(shipping);
				}
			}
		});

	}
}

<?php
  $account_querys = tep_db_query("select is_complete from  " . TABLE_CUSTOMERS . " c, " . TABLE_ADDRESS_BOOK . " ca where c.customers_id = '" . (int)$_SESSION['customer_id'] . "' && ca.customers_id = '" . (int)$_SESSION['customer_id'] . "'");
  $accounts = tep_db_fetch_array($account_querys);
  $is_my_complete = $accounts['is_complete'];
?>


<?php if(!isset($_SESSION['noaccount']) && $is_my_complete){ ?>
	function shipbilladdrChanged(){
		if($('#shipbilladdr').is(":checked")){
			$(".bill-address-select").hide(500);
			$(".bill-address").hide(500);
		}else{
			$(".bill-address-select").show(500);
		}
	}
<?php }else{ ?>
	function shipbilladdrChanged(){
		if($('#shipbilladdr').is(":checked")){
			$(".bill-address").hide(500);
		}else{
			$(".bill-address").show(500);
		}
	}
<?php } ?>
/*
function enternewbilladdress(){
    if($('#enternew').is(":checked")){
	    $('.js-select').val(''); // Select the option with a value of '1'
	    //$('.js-select').trigger('change'); // Notify any JS components that the value changed
        $(".bill-address").show(500);
    }else{
        $(".bill-address").hide(500);
    }
}
*/
function Fchoosebillingaddr(val){
	if(val == 'addnew'){
		$(".bill-address").show(500);
	}else{
		$(".bill-address").hide(500);
	}
}
/*
$(".choosebillingaddr").change(function() {
	var selectedbilladdress = $("select[name=choosebillingaddr]").val();
	alert(selectedbilladdress);
	if(selectedbilladdress == 'addnew'){
		$(".bill-address").show(500);
	}else{
		$(".bill-address").hide(500);
	}
	//$("input[name=enternewbilladdr]").prop('checked', false);
	//$(".bill-address").hide(500);
});
*/
$(document).ready(function() {
	$('.js-select').select2({
		minimumResultsForSearch: -1
	});
});
function setshippingAddress(){
		$('#loader-cart').css('display', 'block');
		$.ajax({
			type: 'post',
			url: 'ajax_handler.php?action=setshippingAddresshtml',
			success: function (retval) {
				$('#cart-shipping-section').html(retval);
				$('#loader-cart').css('display', 'none');
			}
		});
}
function setbillingAddress(){
		$('#loader-cart').css('display', 'block');
		$.ajax({
			type: 'post',
			url: 'ajax_handler.php?action=setbillingAddresshtml',
			success: function (retval) {
				$('#cart-billing-section').html(retval);
				$('#loader-cart').css('display', 'none');
			}
		});
}
function setshippingmethod(shippingmethod){
	error = false;
	var params;
	error_message = "Errors have occurred during the process of your form.\nPlease make the following corrections:\n";
	var shippingmethod = $("input[name='shipping']:checked").length;
	var choosenmethod = $("input[name=shipping]").val();
	if(shippingmethod == 0){
		error_message += 'Please Select a Shipping Method to proceed. \n';
		alert(error_message);
		return false;
	} else {
		params = choosenmethod;
		$('#loader-cart').css('display', 'block');
		$.ajax({
			type: 'post',
			dataType: "json",
			url: 'ajax_handler.php?action=setshippingmethod',
			data: {'data':params},
			success: function (retval) {
				$('#ajaxreloadDiv').html(retval.data);
				$(".cc-number").inputmask();
				$(".cc-exp").inputmask();
				$(".cc-cvc").inputmask();
				carcfun();
				$('#loader-cart').css('display', 'none');
				if(parseInt(retval.number_of_pm) == 1){
				thispaymentmethod(retval.single_payment_method);
				}

			}
		});
         }
}

function confirmpayment(f){
	error = false;
	var params;
	error_message = "Errors have occurred during the process of your form.\nPlease make the following corrections:\n";
	var paymentmethod = $("input[name='payment']:checked").length;
	var number_of_pmethod = $("#number_of_pmethod").val();
	if(paymentmethod == 0 && parseInt(number_of_pmethod) > 1){
		error_message += 'Please Select a Payment Method to proceed. \n';
		alert(error_message);
		return false;
	}
	<?php if (ACCOUNT_CONDITIONS_REQUIRED == 'true') { ?>
		if($('#agree').prop("checked") == false){
				alert('Please CHECK MARK agreement to our conditions of use. If you do not do so, we can not deliver to you.');
				return false;
		}
	<?php } ?>
}
function backtoshipping(){
		$('#loader-cart').css('display', 'block');
		$.ajax({
			type: 'post',
			url: 'ajax_handler.php?action=addshippingaddress',
			success: function (retval) {
				$('#ajaxreloadDiv').html(retval);
				$('#loader-cart').css('display', 'none');
			}
		});
}
function backtoaddressform(){
		$('#loader-cart').css('display', 'block');
		$.ajax({
			type: 'post',
			url: 'ajax_handler.php?action=shippingaddressform',
			success: function (retval) {
				$('#ajaxreloadDiv').html(retval);
				$('#loader-cart').css('display', 'none');
				$(".search-js-select").select2({ width: '95%' });
				$("#state_id select").select2({ width: '95%' });
				$("#billing_state_id select").select2({ width: '95%' });
				$('.js-select').select2({
					minimumResultsForSearch: -1
				});
			}
		});
}

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  $("#cc-number").inputmask();
  $("#cc-exp").inputmask();
});

function change_state(country_id){
	$.get('ajax_handler.php?action=getstate&lcsid=<?=$_COOKIE['lcsid']?>&cpage=checkout&country_id='+country_id, function(data){
		document.getElementById('state_id').innerHTML = data;
		$("#state_id select").select2({ width: '95%' });
	});
}
function billing_change_state(country_id){
	$.get('ajax_handler.php?action=getstate&lcsid=<?=$_COOKIE['lcsid']?>&cpage=checkout&isbilladdress=1&country_id='+country_id, function(data){
		document.getElementById('billing_state_id').innerHTML = data;
		$("#billing_state_id select").select2({ width: '95%' });
	});
}



</script>

<script>

function paymentselection(payment){
	$('input[type=radio][value='+payment+']').prop('checked', true);
}

function carcfun(){
        // If JavaScript is enabled, hide fallback select field
        $('.no-js').removeClass('no-js').addClass('js');

        // When the user focuses on the credit card input field, hide the status
        $('.card input').bind('focus', function() {
            $('.card .status').hide();
        });

        // When the user tabs or clicks away from the credit card input field, show the status
        $('.card input').bind('blur', function() {
            $('.card .status').show();
        });

        // Run jQuery.cardcheck on the input
        $('.card input').cardcheck({
            callback: function(result) {

                var status = (result.validLen && result.validLuhn) ? 'valid' : 'invalid',
                    message = '',
                    types = '';

                // Get the names of all accepted card types to use in the status message.
                for (i in result.opts.types) {
                    types += result.opts.types[i].name + ", ";
                }
                types = types.substring(0, types.length-2);

                // Set status message
                if (result.len < 1) {
                    message = 'Please provide a credit card number.';
                } else if (!result.cardClass) {
                    message = 'We accept the following types of cards: ' + types + '.';
                } else if (!result.validLen) {
                    message = 'Please check that this number matches your ' + result.cardName + ' (it appears to be the wrong number of digits.)';
                } else if (!result.validLuhn) {
                    message = 'Please check that this number matches your ' + result.cardName + ' (did you mistype a digit?)';
                } else {
                    message = 'Great, looks like a valid ' + result.cardName + '.';
                }

				var selectedpayment = $("input[type='radio']:checked").val();;
//alert(selectedpayment);
                // Show credit card icon
               $('#ccbox_'+selectedpayment+' .card .card_icon').removeClass().addClass(' card_icon ' + result.cardClass);

                // Show status message
               $('#ccbox_'+selectedpayment+' .card .status').removeClass('invalid valid').addClass(status).children('.status_message').text(message);
            }
        });
}

    jQuery(function($) {

        // If JavaScript is enabled, hide fallback select field
        $('.no-js').removeClass('no-js').addClass('js');

        // When the user focuses on the credit card input field, hide the status
        $('.card input').bind('focus', function() {
            $('.card .status').hide();
        });

        // When the user tabs or clicks away from the credit card input field, show the status
        $('.card input').bind('blur', function() {
            $('.card .status').show();
        });

    });

    //new for shipping calculation

    function selectRowEffect(object, buttonSelect) {
    	  $('#loader-cart').css('display', 'block');
		  var selected;
		  if (!selected) {
			if (document.getElementById) {
			  selected = document.getElementById('defaultSelected');
			} else {
			  selected = document.all['defaultSelected'];
			}
		  }

		  if (selected) selected.className = 'moduleRow';
		  object.className = 'moduleRowSelected';
		  selected = object;

		// one button is not an array
		  if (document.shippingmethod.shipping[0]) {

			document.shippingmethod.shipping[buttonSelect].checked=true;
			//Uncheck redeme checkbox
			//document.getElementById("id_customer_shopping_points_spending").checked = false;
			shipping = document.shippingmethod.shipping[buttonSelect].value;

		  } else {
			document.shippingmethod.shipping.checked=true;
				shipping = document.shippingmethod.shipping.value;
		  }

		shipping = 'shipping='+shipping;
	    updateOrderTotal(shipping);
    }

	<?php
		if (isset($_GET[tep_session_name()]) && tep_not_null($_GET[tep_session_name()])) {
		  if (defined('MVS_STATUS') && MVS_STATUS == 'true') {
			  echo 'var url_order = "get_order_total.php?' . tep_session_name() . '=' . $_GET[tep_session_name()] . '";' . "\n";
		  } else {
			  echo 'var url_order = "get_order_total.php?' . tep_session_name() . '=' . $_GET[tep_session_name()] . '";' . "\n";
		  }
		} else {
		  echo 'var url_order = "get_order_total.php";' . "\n";
		}
	?>
	function handleHttpResponse_order() {
	  if (http_order.readyState == 4) {
		results = http_order.responseText;
		document.getElementById('cart-calculation-section').innerHTML = results;
		$('#loader-cart').css('display', 'none');
	  }
	}
	function getHTTPObject_order() {
	  var xmlhttp;
	  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
		try {
		  xmlhttp = new XMLHttpRequest();
		} catch (e) {
		  xmlhttp = false;
		}
	  }
	  return xmlhttp;
	}
	var http_order = getHTTPObject_order(); // We create the HTTP Object
	function updateOrderTotal(shipping) {
		<?php
		if (isset($_GET[tep_session_name()]) && tep_not_null($_GET[tep_session_name()])) {
		?>
		  http_order.open("GET", url_order + '&' + shipping, true);
		<?php
		} else {
		?>
		  http_order.open("GET", url_order + '?' + shipping, true);
		<?php
			}
		?>
		  http_order.onreadystatechange = handleHttpResponse_order;
		  http_order.send(null);
	}
	function thispaymentmethod(value){
		$('#ccbox_authorizenet').hide(500);
		$('#ccbox_loadedpayments').hide(500);
		$('#dynamic_process_btn').css('display','none');
		$('#process_payment').css('display','inline-block');

		<?php
		echo $obj_catalog->print_hooks_js('onselectAction');
		?>
	}

	/**********************new script added based on new pdf******************************/
	function fetchaddressdetails(value){
		if(value == 'new'){
			$('.parentofselect').hide(500);
			$('#shippingformdetails').show(500);
			$('input[name=stree_address]').val('');
			$('input[name=stree_address2]').val('');
			$('input[name=city]').val('');
			$('input[select=state]').val('');
			$('input[name=postalcode]').val('');
			$('input[name=telephone]').val('');
			$("#existingaddress").removeClass("bactive");
			$("#newaddress").addClass("bactive");
		} else {
			$('#shippingformdetails').hide(500);
			$('.parentofselect').show(500);
			$("#existingaddress").addClass("bactive");
			$("#newaddress").removeClass("bactive");
		}
	}
	<?php
	echo $obj_catalog->print_hooks_js('checkoutFunctions');
	?>
  </script>  