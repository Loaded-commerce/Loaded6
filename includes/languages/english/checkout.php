<?php
/*
  $Id: account.php,v 1.2 2004/03/05 00:36:42 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Checkout');
define('HEADING_TITLE', 'Checkout');

define('ADDRESS', 'Address');
define('SHIPPING_METHODS', 'Shipping Method');
define('PAYMENT_METHODS', 'Payment Method');
define('SHIPPING_ADDRESS', 'Shipping Address');
define('USE_EXISTING_ADDRESS', 'Use Existing Address');
define('USE_NEW_ADDRESS', 'Use New Address');
define('EXISTING_SHIPPING_ADDRESS', 'Existing Shipping Address');
define('FIRST_NAME', 'First Name:');
define('LAST_NAME', 'Last Name:');
define('COMPANY', 'Company:');
define('STREET_ADDRESS', 'Street Address:');
define('STREET_ADDRESS2', 'Street Address2:');
define('CITY', 'City:');
define('STATE', 'State:');
define('POSTAL_CODE', 'Postal Code:');
define('COUNTRY', 'Country:');
define('SHIPPING_ADDRESS_IS_BILLING_ADDRESS', 'Shipping Address is Billing Address');
define('HEADING_TITLE', 'BILLING_ADDRESS');
define('SET_ADDRESS_AND_CONTINUE', 'Set Address And Continue');
define('ALL_FIELDS_ARE_REQUIRED', '<span style="color:red;font-weight:bold">*</span> fields are Required');
define('BILLING_ADDRESS', 'Billing Address');
define('PHONE_NUMBER', 'Phone Number:');
define('EMAIL_ADDRESS', 'Email Address:');
define('TAXID', 'Tax ID:');
define('CHECKOUT_SUMMARY', 'Checkout Summary');
define('EDIT_INFO', 'Edit Info');
define('SAME_AS_SHIPPING', 'Same as Shipping');
define('TAXID', 'Tax ID:');
define('FAX', 'Fax Number:');
define('CHECKOUT_TERM_CONDITION','have read the <a href="'. tep_href_link('pages&pID=7&CDpath=5').'" target="_blank">conditions of use</a> and I agree to them.');
define('NO_SHPPING_METHOD', 'No Shipping');
define('NO_SHIPPING_METHOD_REQUIRED', 'No Shipping Method Required, Please Proceed to Payment Method');
?>