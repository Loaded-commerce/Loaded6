<?php
/*
  $Id: product_reviews.php,v 1.2 2004/03/05 00:36:42 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('NAVBAR_TITLE', 'Reviews');
define('TEXT_CLICK_TO_ENLARGE', 'Click to enlarge');
define('TEXT_OF_5_STARS', '%s of 5 Stars!');
define('ADD_TO_CART', '<i class="fa fa-shopping-cart"></i> Add To Cart');
define('QUICK_VIEW', '<i class="fa fa-search"></i> Quick View');
define('VIEW_MORE', '<i class="fa fa-eye"></i> View More');
define('TEXT_MODEL','Model');
define('TEXT_MANUFACTURER','Manufacturer');
?>