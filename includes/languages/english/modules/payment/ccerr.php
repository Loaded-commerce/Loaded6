<?php
/*
  $Id: ccerr.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_CCERR_TEXT_TITLE', 'Credit Class Error');
define('MODULE_PAYMENT_CCERR_TEXT_DESCRIPTION', 'Credit Class Error, needed with Gift Voucher');  
define('MODULE_PAYMENT_CCERR_TEXT_ERROR', 'Payment selection error');
?>