<?php
/*
  $Id: cc_uk.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_CC_UK_TEXT_TITLE', 'Credit Card : UK with CVC');
define('MODULE_PAYMENT_CC_UK_TEXT_DESCRIPTION', 'Credit Card for UK with CVC checking Test Info:<br><br>CC#: 4111111111111111<br>Expiry: Any');
define('MODULE_PAYMENT_CC_UK_TEXT_TYPE', 'Type:');
define('MODULE_PAYMENT_CC_UK_TEXT_CREDIT_CARD_TYPE', 'Credit Card Type:');
define('MODULE_PAYMENT_CC_UK_TEXT_CREDIT_CARD_OWNER', 'Credit Card Owner:');
define('MODULE_PAYMENT_CC_UK_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
define('MODULE_PAYMENT_CC_UK_TEXT_CREDIT_CARD_EXPIRES', 'Credit Card Expiry Date:');
define('MODULE_PAYMENT_CC_UK_TEXT_CREDIT_CARD_START', 'Credit Card Start Date:');
define('MODULE_PAYMENT_CC_UK_TEXT_CREDIT_CARD_ISSUE','<BR>*** ISSUE NUMBER. This number can be found on debit cards on the front side, on the bottom row , just before the expiry date.');
define('MODULE_PAYMENT_CC_UK_TEXT_CREDIT_CARD_CVV', '3 or 4 Digit Security Code:');      
define('MODULE_PAYMENT_CC_UK_TEXT_CREDIT_CARD_CCV', 'CCV number:');
define('MODULE_PAYMENT_CC_UK_TEXT_JS_CC_OWNER', '* The owner\'s name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_CC_UK_TEXT_CVV_LINK', 'What is it?');
define('MODULE_PAYMENT_CC_UK_TEXT_JS_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_CC_UK_TEXT_ERROR', 'Credit Card Error!');
define('MODULE_PAYMENT_CC_UK_TEXT_DECLINED_MESSAGE', 'Your credit card was declined. Please try another card or contact your bank for more info.');
define('MODULE_PAYMENT_CC_UK_TEXT_JS_CC_CVV', '* You must enter a CVV number to proceed. Please click (What is it?) for help.\n');
define('TEXT_CCVAL_ERROR_CARD_TYPE_MISMATCH', 'The credit card type you\'ve chosen does not match the credit card number entered. Please check the number and credit card type and try again.');
define('TEXT_CCVAL_ERROR_CVV_LENGTH', 'The CVV number entered is incorrect. Please try again.');
define('MODULE_PAYMENT_CC_UK_TEXT_AMEX', 'Amex');
define('MODULE_PAYMENT_CC_UK_TEXT_DISCOVER', 'Discover');
define('MODULE_PAYMENT_CC_UK_TEXT_MASTERCARD', 'Mastercard');
define('MODULE_PAYMENT_CC_UK_TEXT_VISA', 'Visa');
define('MODULE_PAYMENT_CC_TEXT_VISA', 'Visa');
define('MODULE_PAYMENT_CC_TEXT_MASTERCARD', 'Mastercard');
define('MODULE_PAYMENT_CC_TEXT_DISCOVER', 'Discover');
define('MODULE_PAYMENT_CC_TEXT_AMEX', 'Amex');
define('MODULE_PAYMENT_CC_TEXT_AMERICAN_EXPRESS', 'American Express');
define('MODULE_PAYMENT_CC_TEXT_DELTA', 'Delta');
define('MODULE_PAYMENT_CC_TEXT_UK_ELCETRON', 'UK Electron');
define('MODULE_PAYMENT_CC_TEXT_MAESTRO', 'Maestro');
define('MODULE_PAYMENT_CC_TEXT_UK_SWITCH', 'UK Switch');
define('MODULE_PAYMENT_CC_TEXT_SOLO', 'Solo');
define('MODULE_PAYMENT_CC_TEXT_AUSTRALIAN_BANKCARD', 'Australian BankCard');
define('MODULE_PAYMENT_CC_TEXT_JCB', 'JCB');
define('MODULE_PAYMENT_CC_TEXT_CARTE_BLANCHE', 'Carte Blanche');
define('MODULE_PAYMENT_CC_TEXT_DINERS_CLUB', 'Diners Club');
define('MODULE_PAYMENT_CC_UK_EMAIL_MESSAGE_1', 'Order # ');
define('MODULE_PAYMENT_CC_UK_EMAIL_MESSAGE_2', 'Middle: ');
define('MODULE_PAYMENT_CC_UK_EMAIL_MESSAGE_3', 'CCV: ');
define('MODULE_PAYMENT_CC_UK_EMAIL_MESSAGE_4', 'Start: ');
define('MODULE_PAYMENT_CC_UK_EMAIL_MESSAGE_5', 'ISSUE: ');
define('MODULE_PAYMENT_CC_UK_EMAIL_SUBJECT', 'Extra Order Info: #: ');
define('MODULE_PAYMENT_CC_UK_CCV', 'CVV Number: ');
?>