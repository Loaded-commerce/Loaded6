<?php
/*
  $Id: eustandardtransfer.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_EUTRANSFER_TEXT_TITLE', 'EU-Standard Bank Transfer');
define('MODULE_PAYMENT_EUTRANSFER_TEXT_DESCRIPTION', '<BR>Please use the following details to transfer your total order value:<BR>' .
                                                     '<BR>Bank Name: ' . MODULE_PAYMENT_EUTRANSFER_BANKNAM .
                                                     '<BR>Branch: ' . MODULE_PAYMENT_EUTRANSFER_BRANCH .
                                                     '<BR>Account Name: ' . MODULE_PAYMENT_EUTRANSFER_ACCNAM .
                                                     '<br>Account No.: ' . MODULE_PAYMENT_EUTRANSFER_ACCNUM .
                                                     '<BR>IBAN:: ' . MODULE_PAYMENT_EUTRANSFER_ACCIBAN .
                                                     '<BR>BIC/SWIFT: ' . MODULE_PAYMENT_EUTRANSFER_BANKBIC .
                                                     '<BR><BR>Your order will not ship until we receive payment in the above account.<BR>');
define('MODULE_PAYMENT_EUTRANSFER_TEXT_EMAIL_FOOTER', str_replace('<BR>','\n',MODULE_PAYMENT_EUTRANSFER_TEXT_DESCRIPTION));
?>