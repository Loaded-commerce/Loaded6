<?php
/*
  $Id: nochex.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_NOCHEX_TEXT_TITLE', 'Nochex');
define('MODULE_PAYMENT_NOCHEX_TEXT_DESCRIPTION', 'Nochex Requires the GBP currency.<br><br>Nochex limits transactions to 100 GBP.  Anything over requires either a Merchant Account or a Nochex account.<br>');
?>