<?php
/*
  $Id: quickcommerce.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_TITLE', 'Credit Card : QuickCommerce Pro');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_DESCRIPTION', 'Credit Card Test Info:<br><br>CC#: 4111111111111111<br>Expiry: Any');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_TYPE', 'Type:');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_CREDIT_CARD_OWNER', 'Credit Card Owner:');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_CREDIT_CARD_EXPIRES', 'Credit Card Expiry Date:');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_CREDIT_CARD_TYPE', 'Credit Card Type:');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_JS_CC_OWNER', '* The owner\'s name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_CVV_LINK', 'What is it?');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_JS_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_ERROR_MESSAGE', 'There has been an error processing your credit card. Please try again.');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_DECLINED_MESSAGE', 'Your credit card was declined. Please try another card or contact your bank for more info.');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_ERROR', 'Credit Card Error!');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_JS_CC_CVV', '* You must enter a CVV number to proceed.\n');
define('TEXT_CCVAL_ERROR_CARD_TYPE_MISMATCH', 'The credit card type you\'ve chosen does not match the credit card number entered. Please check the number and credit card type and try again.');
define('TEXT_CCVAL_ERROR_CVV_LENGTH', 'The CVV number entered is incorrect. Please try again.');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_AMEX', 'Amex');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_DISCOVER', 'Discover');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_MASTERCARD', 'Mastercard');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_VISA', 'Visa');
define('MODULE_PAYMENT_QUICKCOMMERCE_STATUS', 'Status');
define('MODULE_PAYMENT_QUICKCOMMERCE_ORDER_STATUS_ID', 'Order Status ID');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_CVV_NUMBER', 'CVV Number');
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_SERVER_ERROR','The server cannot connect to Quickcommerce for processing, please contact site admin' . STORE_OWNER_EMAIL_ADDRESS);
define('MODULE_PAYMENT_QUICKCOMMERCE_TEXT_ERROR_UNKNOWN','There was an unspecified error processing your credit card.')
?>