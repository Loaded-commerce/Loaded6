<?php
/*
  $Id: ckmerchantpartners.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
$ck_number_min_length = defined('CK_NUMBER_MIN_LENGTH')?CK_NUMBER_MIN_LENGTH:0;
define('MODULE_PAYMENT_CKMERCHANTPARTNERS_TEXT_TITLE', 'Electronic Check: Pay-Me-Now');
define('MODULE_PAYMENT_CKMERCHANTPARTNERS_TEXT_DESCRIPTION', 'Merchant Partners Pay-Me-Now Electronic Check Number:<br><br>ACCT#: 999999999<br>ABA: 999999999');
define('CKMERCHANTPARTNERS_ERROR_HEADING', 'There has been an error processing your Electronic Check');
define('CKMERCHANTPARTNERS_ERROR_MESSAGE', 'Please check your Check details!');
define('MODULE_PAYMENT_CKMERCHANTPARTNERS_TEXT_CHECK_OWNER', 'Name on Check:');
define('MODULE_PAYMENT_CKMERCHANTPARTNERS_TEXT_CHECK_NUMBER', 'Check Account Number:');
define('MODULE_PAYMENT_CKMERCHANTPARTNERS_TEXT_JS_CK_OWNER', 'Check Account Owner is Blank!');
define('MODULE_PAYMENT_CKMERCHANTPARTNERS_TEXT_CHECK_ABA', 'Check Routing Number (ABA):');
define('MODULE_PAYMENT_CKMERCHANTPARTNERS_TEXT_JS_CK_NUMBER', '* The check number must be at least ' . $ck_number_min_length . ' characters.' . "\n");
define('MODULE_PAYMENT_CKMERCHANTPARTNERS_TEXT_JS_CK_NUMBER', 'Check Number is Blank!');
?>