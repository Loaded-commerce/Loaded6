<?php
/*
  $Id: plugnpay.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_PLUGNPAY_TEXT_TITLE', 'Credit Card : Plug\'n Pay');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_DESCRIPTION', 'Plug\'n Pay API');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_TYPE', 'Type:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_CREDIT_CARD_OWNER', 'Credit Card Owner:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_CREDIT_CARD_CVV', 'CVV/CVV2:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_CREDIT_CARD_EXPIRES', 'Credit Card Expiry Date:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_CREDIT_CARD_TYPE', 'Credit Card Type:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_JS_CC_OWNER', '* The owner\'s name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_CVV_LINK', 'What is it?');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_JS_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ERROR_MESSAGE', 'There has been an error processing your credit card. Please try again.');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_DECLINED_MESSAGE', 'Your credit card was declined. Please try another card or contact your bank for more info.');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ERROR', 'Credit Card Error!');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_JS_CC_CVV', '* You must enter a CVV number to proceed.\n');
define('TEXT_CCVAL_ERROR_CARD_TYPE_MISMATCH', 'The credit card type you\'ve chosen does not match the credit card number entered. Please check the number and credit card type and try again.');
define('TEXT_CCVAL_ERROR_CVV_LENGTH', 'The CVV number entered is incorrect. Please try again.');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_AMEX', 'Amex');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_DISCOVER', 'Discover');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_MASTERCARD', 'Mastercard');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_VISA', 'Visa');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ECHECK_ACCTTYPE', 'Account Type:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ECHECK_ROUTINGNUM', 'Routing Number:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ECHECK_ACCOUNTNUM', 'Account Number:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ECHECK_CHECKNUM', 'Check Number:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_PAYTYPE', 'Payment Method:');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ERROR_1', 'Your authorization was declined.  Please try another card.');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ERROR_2', 'Your transaction was rejected.  Please contact the merchant for ordering assistance.');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ERROR_3', 'There was an error processing your transaction.  Please contact the merchant for ordering assistance.');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_ERROR_4', 'There was an unspecified error processing your transaction.<br>Received empty cURL response - check cURL connectivity to PnP server.');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_1', '&nbsp; or Electronic Check');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_2', '<b>Select Your Method Of Payment:</b>');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_3', '&nbsp;<p><b>Credit Card Info:</b>');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_5', 'CVV number ');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_4', '&nbsp;<p><b>Electronic Checking Info:</b>');
define('MODULE_PAYMENT_PLUGNPAY_TEXT_6', ': Electronic Check Payments');
?>