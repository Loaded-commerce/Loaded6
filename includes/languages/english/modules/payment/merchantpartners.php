<?php
/*
  $Id: merchantpartners.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_MERCHANTPARTNERS_TEXT_TITLE', 'Credit Card : Pay Me Now');
define('MODULE_PAYMENT_MERCHANTPARTNERS_TEXT_DESCRIPTION', 'Merchant Partners, Pay me now(Credit Card Test Info:<br><br>CC#: 4111111111111111<br>Expiry: Any');
define('MERCHANTPARTNERS_ERROR_HEADING', 'There has been an error processing your credit card');
define('MERCHANTPARTNERS_ERROR_MESSAGE', 'Please check your credit card details!');
define('MODULE_PAYMENT_MERCHANTPARTNERS_TEXT_CREDIT_CARD_OWNER', 'Credit Card Owner:');
define('MODULE_PAYMENT_MERCHANTPARTNERS_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
define('MODULE_PAYMENT_MERCHANTPARTNERS_TEXT_CREDIT_CARD_EXPIRES', 'Credit Card Expiry Date:');
define('MODULE_PAYMENT_MERCHANTPARTNERS_TEXT_CREDIT_CARD_CVV2', 'CVV2/CCV2/CID number:');
define('MODULE_PAYMENT_MERCHANTPARTNERS_TEXT_CREDIT_CARD_CVV2_LOCATION', '(3 or 4 digit # on back of card):');
define('MODULE_PAYMENT_MERCHANTPARTNERS_TEXT_JS_CC_OWNER', '* The owner\'s name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_MERCHANTPARTNERS_TEXT_JS_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
?>