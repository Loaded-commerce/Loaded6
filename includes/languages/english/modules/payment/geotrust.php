<?php
/*
  $Id: geotrust.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_GEOTRUST_TEXT_TITLE', 'Credit Card : GeoTrust QuickPayments');
define('MODULE_PAYMENT_GEOTRUST_TEXT_DESCRIPTION', 'Credit Card Test Info:<br><br>CC#: 4445999922225<br>Expiry: Any');
define('MODULE_PAYMENT_GEOTRUST_TEXT_ERROR', 'Credit Card Error!');
define('MODULE_PAYMENT_GEOTRUST_TEXT_ERROR_MESSAGE', 'There has been an error processing your credit card. Please try again.');
define('MODULE_PAYMENT_GEOTRUST_TEXT_CREDIT_CARD_OWNER_FIRST_NAME', 'First Name:');
define('MODULE_PAYMENT_GEOTRUST_TEXT_CREDIT_CARD_OWNER_LAST_NAME', 'Last Name:');
define('MODULE_PAYMENT_GEOTRUST_TEXT_CREDIT_CARD_OWNER', 'Name:');
define('MODULE_PAYMENT_GEOTRUST_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
define('MODULE_PAYMENT_GEOTRUST_TEXT_CREDIT_CARD_EXPIRES', 'Credit Card Expiration Date:');
define('MODULE_PAYMENT_GEOTRUST_TEXT_CREDIT_CARD_CVV2', 'Card Verification Value:');
define('MODULE_PAYMENT_GEOTRUST_TEXT_CREDIT_CARD_CVV2_LOCATION', '(last 3 digits, located at the back of the credit card)');
define('MODULE_PAYMENT_GEOTRUST_TEXT_JS_CC_OWNER', '* The owner\'s name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_GEOTRUST_TEXT_JS_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
?>