<?php
/*
  $Id: paybox.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_PAYBOX_TEXT_TITLE', 'Credit Card : Paybox');
define('MODULE_PAYMENT_PAYBOX_TEXT_DESCRIPTION', 'Paybox Credit Card Payment Module');
define('MODULE_PAYMENT_PAYBOX_TEXT_ERROR', 'Credit Card Error!');
define('MODULE_PAYMENT_PAYBOX_TEXT_ERROR_MESSAGE', 'There has been an error processing your credit card. Please try again.');
?>