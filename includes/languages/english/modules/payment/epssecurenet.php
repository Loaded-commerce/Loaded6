<?php
/*
  $Id: epssecurenet.php,v 2.1 2008/06/12 00:36:41 datazen Exp $

  CRE Loaded, Commerical Open Source eCommerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_PAYMENT_EFSNET_TEXT_TITLE', 'Credit Card : EPSsecurenet');
define('MODULE_PAYMENT_EPSSECURENET_CART_SYSTEM_NAME', 'CRE Loaded');
define('MODULE_PAYMENT_EPSSECURENET_CONNECTION_TYPE', 'sd');
define('MODULE_PAYMENT_EPSSECURENET_ORDER_DESCRIP', 'Refer to CRE Loaded Customer Order Screen');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_TITLE', 'EPSsecurenet');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_DESCRIPTION', 'EPSsecurenet Payment Module Credit Card Test Info:<br><br>CC#: 5123456789012346<br>Expiry: Any'); 
define('MODULE_PAYMENT_EPSSECURENET_TEXT_CREDIT_CARD_OWNER', 'Credit Card Owner:');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_CREDIT_CARD_EXPIRES', 'Credit Card Expiry Date:');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_CREDIT_CARD_CVV2', 'CVV2 Security Number:');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_JS_CC_OWNER', '* The owner\'s name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_JS_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_JS_CC_CVV2', '* The CVV2 Number must at least be three or four characters.\n    -For VISA/MasterCard this number is on the back of the Card.\n    -For American Express it is 4 digits and located on the front of the Card.\n');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_ERROR_MESSAGE', 'There has been an error processing your credit card. Please try again.');
define('MODULE_PAYMENT_EPSSECURENET_TEXT_ERROR', 'Credit Card Error!');
?>