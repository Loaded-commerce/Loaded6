<?php
/*
  $Id: articles_upcoming.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

?>
<?php
  $expected_query = tep_db_query("select a.articles_id, a.articles_date_added, a.articles_date_available as date_expected, ad.articles_name, ad.articles_head_desc_tag, au.authors_id, au.authors_name, td.topics_id, td.topics_name from " . TABLE_ARTICLES . " a, " . TABLE_ARTICLES_TO_TOPICS . " a2t, " . TABLE_TOPICS_DESCRIPTION . " td, " . TABLE_AUTHORS . " au, " . TABLE_ARTICLES_DESCRIPTION . " ad where to_days(a.articles_date_available) > to_days(now()) and a.articles_id = a2t.articles_id and a2t.topics_id = td.topics_id and a.authors_id = au.authors_id and a.articles_status = '1' and a.articles_id = ad.articles_id and ad.language_id = '" . (int)$languages_id . "' and td.language_id = '" . (int)$languages_id . "' order by date_expected limit " . MAX_DISPLAY_UPCOMING_ARTICLES);
  if (tep_db_num_rows($expected_query) > 0) {
?>
<!-- upcoming_articles //-->

<div class="row">
 <div class="col-lg-12 col-xs-12 main topic_details"><?php echo '<h1 class="no-margin-top box-head">' . TEXT_UPCOMING_ARTICLES . '</h1>'; ?></div>
  <div cass="col-lg-12 col-xs-12  no-padding" style="">
  <?php
    while ($articles_expected = tep_db_fetch_array($expected_query)) {
   ?>
     <div class="col-lg-12 col-xs-12 main-outer" style="">
       <div class="col-lg-9 col-xs-12 box-header" style="padding:0px;margin-left:0px;">
		<?php
		  echo '<a href="' . tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $articles_expected['articles_id'] . $CDpath) . '" style="padding:0px;font-weight:400;color:#428bca;">' . $articles_expected['articles_name'] . '</a>'; ?>

		</div>
		 <?php if (DISPLAY_AUTHOR_ARTICLE_LISTING == 'true') {
		 ?>

		    <div class="col-lg-12 col-xs-12" style="padding-top:5px;margin-left:0px;padding-left:0px">

		    <?php echo '<b style="font-weight:500;">'. TEXT_DATE_EXPECTED . '</b> ' . tep_date_long($articles_expected['date_expected']).','; ?>
		    <?php echo '&nbsp'.By .'&nbsp;'. '<a  href="' . tep_href_link(FILENAME_ARTICLES, 'authors_id=' . $articles_expected['authors_id'] . $CDpath) . '" class="text" style="color:#428bca">' . $articles_expected['authors_name'].'</a>'; ?>
 <?php } ?>

		<?php
		      if (DISPLAY_TOPIC_ARTICLE_LISTING == 'true') {
         ?>
		<div class="col-lg-3 col-xs-12 topic_text_up" style=""> <?php echo '<b style="color:#ff0000">'. TEXT_TOPIC . '</b>&nbsp;<a class="text" href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . $articles_expected['topics_id'] . $CDpath) . '">' . $articles_expected['topics_name'].'</a>'; ?> </div></div>
		<?php
		  }
        ?>
        <?php
		      if (DISPLAY_ABSTRACT_ARTICLE_LISTING == 'true') {
         ?>
        <div class="col-lg-12 col-xs-12 article_topic" style=""><?php echo clean_html_comments(substr($articles_expected['articles_head_desc_tag'],0, MAX_ARTICLE_ABSTRACT_LENGTH)) . ((strlen($articles_expected['articles_head_desc_tag']) >= MAX_ARTICLE_ABSTRACT_LENGTH) ? '...' : ''); ?></div>
      <?php } ?>

     </div>
    <?php
	      if (DISPLAY_ABSTRACT_ARTICLE_LISTING == 'true' || DISPLAY_DATE_ADDED_ARTICLE_LISTING) {
    ?>


<?php } } ?>
</div>
</div>

<?php /*
    while ($articles_expected = tep_db_fetch_array($expected_query)) {
?>
          <div class="col-lg-12 col-xs-12">
            <div class="col-lg-8 col-xs-12 main"  valign="top"  width="75%">
<?php
  echo '<font color="#999999"><b>' . $articles_expected['articles_name'] . '</b></font> ';
  if (DISPLAY_AUTHOR_ARTICLE_LISTING == 'true') {
   echo TEXT_BY . ' ' . $articles_expected['authors_name'];
  }
?>
            </div>
<?php
      if (DISPLAY_TOPIC_ARTICLE_LISTING == 'true') {
?>
            <div valign="top" class="col-lg-4 col-xs-12 main" width="25%" nowrap="nowrap"><?php echo TEXT_TOPIC . '&nbsp;' . $articles_expected['topics_name']; ?></td>
<?php
      }
?>
          </div>
<?php
      if (DISPLAY_ABSTRACT_ARTICLE_LISTING == 'true') {
?>

            <div class="col-lg-12 col-xs-12 main" style="padding-left:15px"><?php echo clean_html_comments(substr($articles_expected['articles_head_desc_tag'],0, MAX_ARTICLE_ABSTRACT_LENGTH)) . ((strlen($articles_expected['articles_head_desc_tag']) >= MAX_ARTICLE_ABSTRACT_LENGTH) ? '...' : ''); ?></div>

<?php
      }
?>

            <div class="col-lg-12 col-xs-12 smalltext" style="padding-left:15px"><?php echo TEXT_DATE_EXPECTED . ' ' . tep_date_long($articles_expected['date_expected']); ?></div>

<?php
      if (DISPLAY_ABSTRACT_ARTICLE_LISTING == 'true' || DISPLAY_DATE_ADDED_ARTICLE_LISTING) {
?>

<?php
     }
  } */ // End of listing loop
?>

<!-- eof upcoming_articles //-->
<?php
  }
?>

