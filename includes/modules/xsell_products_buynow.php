<?php
/*
$Id: xsell_products.php, v1  2002/09/11

  LoadedCommerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2018 LoadedCommerce

Released under the GNU General Public License
*/

if ($_GET['products_id'])
{
	$xsell_query = tep_db_query("select distinct p.products_id, p.products_image, p.products_price, p.manufacturers_id, pd.products_name,
	p.products_tax_class_id, p.products_price, p.products_date_added, p.products_image , xp.xsell_id products_price, p.products_model
	from (" . TABLE_PRODUCTS . " p left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id), " . TABLE_PRODUCTS_XSELL . " xp, " . TABLE_PRODUCTS_DESCRIPTION . " pd
	where xp.products_id = '" . (int)$_GET['products_id'] . "' and p.products_id = xp.xsell_id and pd.products_id = xp.xsell_id
	and pd.language_id = '" . $languages_id . "' and p.products_status = '1' order by rand(), xp.products_id asc limit " . MAX_DISPLAY_XSELL);

	$num_products_xsell = tep_db_num_rows($xsell_query);

	if ($num_products_xsell >= MIN_DISPLAY_XSELL)
	{
		require(DIR_WS_LANGUAGES . $language . '/modules/xsell_products.php');
		echo '<div class="col-xl-12 col-lg-12 col-12 col-sm-12 module-heading"><h3 class="mod_xsell">' . TEXT_XSELL_PRODUCTS . '</h3></div>';
		echo '<div class="col-xl-12 col-lg-12 col-12 col-sm-12 products-xsell-container product-listing-module-container"><div class="row" style="margin:0px;">';
		while ($xsell = tep_db_fetch_array($xsell_query))
		{
			$product_contents = $obj_catalog->prod_grid_listing($xsell, $column_list);
			$lc_text = implode( $product_contents);
			echo '<div class="'.$divcolumn.' product-listing-module-items item-outer with-small-padding">
			        <div class=" itembox">
							<div class="thumbnail align-center row" style="margin-left:0px;margin-right:0px;">'. $lc_text ;
					echo (tep_check_specials_products($xsell['products_id'])?'<div class="stickers"> <div class="sale">Sale</div></div>':'');
					echo '</div>
					</div>
				  </div>';
		}
		echo '</div></div><div class="clearfix"></div>'."\n";
	}
}
?>
