<?php
/*
  $Id: article_listing.php, v1.0 2003/12/04 12:00:00 datazen Exp $

  CRE Loaded, Commercial Open Source E-Commerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
// added for CDS CDpath support

?>
	<div class="col-lg-12 col-xs-12 col-12 col-sm-12 col-xl-12" style="padding:5px;margin-left;0px !important;">
	  <div class="row">
		  <div class="col-lg-4 col-xs-12 col-12 col-sm-12 text-center" style="padding: 10px;">
		  <a href="<?php echo tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $articles_all['articles_id'] . $CDpath); ?>"><?php echo tep_image(DIR_WS_ARTICLES . $articles_all['articles_image'], $articles_all['articles_name'], '', SMALL_IMAGE_HEIGHT, 'style="height:'.SMALL_IMAGE_HEIGHT.'px;object-fit:contain"');?></a>
		  </div>
		  <div class="col-lg-8 col-xs-12 col-12 col-sm-12" style="padding: 10px;">
		   <?php
		   echo  '<div class="col-lg-12 col-xs-12 col-12 col-sm-12 box-header small-margin-bottom article-box-head " style=""><b><a style="font-weight:600;" href="' . tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $articles_all['articles_id'] . $CDpath) . '">' . $articles_all['articles_name'] . '</a></b></div>'; ?>
			    <div class="col-lg-12 col-xs-12 col-12 col-sm-12 heading1" style="padding-left:0px;padding-bottom:10px;">
					   <?php
						  if($articles_all['articles_date_added'] !='0000-00-00 00:00:00' && $articles_all['articles_date_added'] != ''){
							echo  '<div class="col-lg-6" style="float:left;padding-left:0px;">'.tep_date_long($articles_all['articles_date_added']).'&nbsp;/&nbsp;'.'<a href="' . tep_href_link(FILENAME_ARTICLES, 'authors_id=' . $articles_all['authors_id'] . $CDpath) . '"> ' . tep_get_author_name($articles_all['authors_id']) . '</a></div>';
						  }else{
							echo '<div class="col-lg-6" style="float:left;padding-left:0px;"><a href="' . tep_href_link(FILENAME_ARTICLES, 'authors_id=' . $articles_all['authors_id'] . $CDpath) . '"> ' . tep_get_author_name($articles_all['authors_id']) . '</a></div>';

						  }
					   ?>
						<?php  if (DISPLAY_TOPIC_ARTICLE_LISTING == 'true' && tep_get_topic_name($articles_all['articles_id']) != '') {
							 echo '<div class="col-lg-6 col-md-3 col-12 col-sm-12  topic_text" style="color:#ff0000;float:right;"><b>'. TEXT_TOPIC . '</b>&nbsp;<a href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=' . tep_get_topic_id($articles_all['articles_id']) . $CDpath) . '">' . tep_get_topic_name($articles_all['articles_id']) . '</a></div>';
						  } ?>
		       </div>
			<div class="col-lg-12 desc_tag" style="clear:both;padding-left:0px;margin-bottom:20px;">
				  <?php echo clean_html_comments(substr($articles_all['articles_head_desc_tag'],0, MAX_ARTICLE_ABSTRACT_LENGTH)) . ((strlen($articles_all['articles_head_desc_tag']) >= MAX_ARTICLE_ABSTRACT_LENGTH) ? '...' : ''); ?>
			</div>
			<?php echo '<div class=""><a href="' . tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $articles_all['articles_id'] . $CDpath) . '" class="more-link btn btn-sm cursor-pointer small-margin-right btn-success">Continue reading</a></div>';?>

		  </div>
		 <div class="clear-both"></div>
	   </div>
	 </div>