<?php
/*
  osCommerce 2.2 (Snapshot on November 10, 2002) Open Source E-Commerce Solutions
  Authorizenet ADC Direct Connection
  Last Update: November 10, 2002
  Author: Bao Nguyen
  Email: baonguyenx@yahoo.com

  Update: August 13, 2003
  Added: Transaction Key, Sort Order
  Author: Austin Renfroe (Austin519)
  Email: Austin519@aol.com

  Updated for: Authorize.net Consolidated v1.4
  Date: February 28, 2004
  Added: Credit Card Processing Mode, Currency Code, Company Information, Updated Billing Address, Invoice Number, Echeck Foundation, Debug Code
  Author: Austin Renfroe (Austin519)
  Email: Austin519@aol.com

  Updated for: Authorize.net Consolidated v1.4
  Date: March 1, 2004
  Added: Linux Compatibility
  Author: Austin Renfroe (Austin519)
  Email: Austin519@aol.com

  Updated for: Authorize.net Consolidated v1.5
  Date: March 12, 2004
  Added: CVV Card Specific Checking, CC Images
  Author: Austin Renfroe (Austin519), some code thanks to Dansken
  Email: Austin519@aol.com

  Updated for: Authorize.net Consolidated v1.5
  Date: March 24, 2004
  Added: Relay Response var, Test and Debug Mode, cURL var for admin module, cURL autofind, Windows 2003 Compatibility
  Author: Austin Renfroe (Austin519), some code thanks to dreamscape
  Email: Austin519@aol.com
*/

  unset($form_data);
  $xx = '';
  for ($i=0; $i<sizeof($order->products); $i++) {
    $xx .= $order->products[$i]['qty'] . '-' . ($order->products[$i]['name']) . '**';
  }
  //Austin519 - added transaction key, ccmode
  $form_data = array(
  x_Login => MODULE_PAYMENT_AUTHORIZENET_LOGIN,
  x_Tran_Key => MODULE_PAYMENT_AUTHORIZENET_TRANSKEY,
  x_Relay_Response => 'FALSE',
  x_Delim_Data => 'TRUE',
  x_Delim_char => '|',
  x_Version => '3.1',
  x_Type => MODULE_PAYMENT_AUTHORIZENET_CCMODE == 'Authorize Only' ? 'AUTH_ONLY' : 'AUTH_CAPTURE',
  x_Method => MODULE_PAYMENT_AUTHORIZENET_METHOD == 'Credit Card' ? 'CC' : 'ECHECK',
  x_Amount => number_format($order->info['total'], 2),
  x_Currency_Code => "{$order->info['currency']}",
  x_Email_Customer => MODULE_PAYMENT_AUTHORIZENET_EMAIL_CUSTOMER == 'True' ? 'TRUE': 'FALSE',
  x_Email_Merchant => MODULE_PAYMENT_AUTHORIZENET_EMAIL_MERCHANT == 'True' ? 'TRUE': 'FALSE',
  x_Cust_ID => "$customer_id",
  x_Invoice_Num => "$insert_id",
  x_First_Name => "{$order->billing['firstname']}",
  x_Last_Name => "{$order->billing['lastname']}",
  x_Company => "{$order->billing['company']}",
  x_Address => "{$order->billing['street_address']}",
  x_City => "{$order->billing['city']}",
  x_State => "{$order->billing['state']}",
  x_Zip => "{$order->billing['postcode']}",
  x_Country => "{$order->billing['country']['title']}",
  x_Phone => "{$order->customer['telephone']}",
  x_Email => "{$order->customer['email_address']}",
  x_Ship_To_First_Name => "{$order->delivery['firstname']}",
  x_Ship_To_Last_Name => "{$order->delivery['lastname']}",
  x_Ship_To_Company => "{$order->delivery['company']}",
  x_Ship_To_Address => "{$order->delivery['street_address']}",
  x_Ship_To_City => "{$order->delivery['city']}",
  x_Ship_To_State => "{$order->delivery['state']}",
  x_Ship_To_Zip => "{$order->delivery['postcode']}",
  x_Ship_To_Country => "{$order->delivery['country']['title']}",
  x_Customer_IP => "{$_SERVER['REMOTE_ADDR']}",
  x_Description => "$xx",
  tep_session_name() => tep_session_id());

  if(MODULE_PAYMENT_AUTHORIZENET_METHOD == 'Credit Card') {

// Eversun mod for authorize.net problem
//    $form_data['x_Card_Num'] = "$x_Card_Num";
//    $form_data['x_Exp_Date'] = "$x_Exp_Date";
    $form_data['x_Card_Num'] = $_SESSION['x_Card_Num'];
    $form_data['x_Exp_Date'] = $_SESSION['x_Exp_Date'];
// Eversun mod end for authorize.net problem
        
  if(MODULE_PAYMENT_AUTHORIZENET_CCV  == 'True' ) {
    
// Eversun mod for authorize.net problem
//    $form_data['x_Card_Code'] = "$x_Card_Code";
    $form_data['x_Card_Code'] = $_SESSION['x_Card_Code'];
// Eversun mod end for authorize.net problem
    
  }
  } else {
    //E-check Information (Currently NOT IMPLEMENTED!)
    $form_data['x_Recurring_Billing'] = '';
    $form_data['x_Bank_Aba_Code'] = '';
    $form_data['x_Bank_Acct_Num'] = '';
    $form_data['x_Bank_Acct_Type'] = '';
    $form_data['x_Bank_Name'] = '';
    $form_data['x_Bank_Acct_Name'] = '';
    $form_data['x_echeck_type'] = '';
    $form_data['x_drivers_license_num'] = '';
    $form_data['x_drivers_license_state'] = '';
    $form_data['x_drivers_license_DOB'] = '';
  }

     if (($x_Card_Num == '4111111111111111') || ($x_Card_Num == '370000000000002') || ($x_Card_Num == '6011000000000012') || ($x_Card_Num == '5424000000000015') || ($x_Card_Num == '4007000000027')) {
      $cardtestmode=0;
    } else {
      $cardtestmode=0;
    }

  if((MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Test') || (MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Test And Debug') ) {
    //$form_data['x_Test_Request'] = 'TRUE';
  }

  // concatenate order information variables to $data
  $log_data = $data;  // the credit card number must not be log in full
  foreach($form_data as $key=>$value) {
    if ($key != 'x_Card_Num') {
      $data .= $key . '=' . urlencode(str_replace(',', '', $value)) . '&';
      $log_data .= $key . '=' . urlencode(str_replace(',', '', $value)) . '&';
    } else {
      $data .= $key . '=' . urlencode(str_replace(',', '', $value)) . '&';
      $log_data .= $key . '=' . urlencode(substr($value, 0, 4) . '...' . substr($value, -4)) . '&';
    }
  }

  // take the last & out for the string
  $data = substr($data, 0, -1);
  $log_data = substr($log_data, 0, -1);

  /* Debug code - will dump cURL data to a file when
   * the admin module Transaction Mode is set to Test and Debug.
   * The file is written into the root directory of the catalog
   *
   * Monitor Mode added - 03/20/2008 DMG
   *
   * This Mode provides debug monitoring during Active Sales 
   * conditions. Note - no toggle of transaction URL or 
   * transmission of test mode toggle switch to Authnet should
   * occur as a result of this mod.
   */
//  if ((MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Test And Debug') || (MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Monitor' ) ) {
    $filename = './debug/authnet_debug.txt';
    $fp = fopen($filename, "a");
    $write = fputs($fp, $log_data . "\n");
    fclose($fp);
//  }
  unset($response);

/******************************************************************************************
 * Post order info data to Authorize.net, make sure you have curl installed
 *    Those with cURL not compiled into PHP (Windows users, some Linux users):
 *          Please type in your path in the admin module.  The code below will TRY to find
 *          your cURL path for you (may not work under Windows) and if it finds it, will
 *          default to that, but you should enter your cURL path in the admin module to be
 *          sure.  You no longer need to edit the code manually in this file (that code thanks
 *          to dreamscape).
 *    Those with cURL compiled into PHP (some Linux users):
 *          This should work without any editing if cURL is compiled into your PHP and you
 *          have PHP configured to realize it (per the PHP guides).
******************************************************************************************
 */
  if (MODULE_PAYMENT_AUTHORIZENET_CURL == 'Not Compiled') {
    if (function_exists('exec')) {
      exec('which curl', $curl_output);
      if ($curl_output) {
        $curl_path = $curl_output[0];
      }else {
        $curl_path = MODULE_PAYMENT_AUTHORIZENET_CURL_PATH;
      }
    }
    
    if (($x_Card_Num == '4111111111111111') || ($x_Card_Num == '370000000000002') || ($x_Card_Num == '6011000000000012') || ($x_Card_Num == '5424000000000015') || ($x_Card_Num == '4007000000027')) {
      $cardtestmode=0;
    } else {
      $cardtestmode=0;
    }
    if((MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Test') || (MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Test And Debug') ) {
      exec("$curl_path -d $data https://test.authorize.net/gateway/transact.dll", $response);
    } else {
      exec("$curl_path -d $data https://secure.authorize.net/gateway/transact.dll", $response);
    }

  } else {
    if((MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Test') || (MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Test And Debug') ) {
      $url = "https://test.authorize.net/gateway/transact.dll";
    } else {
      $url = "https://secure.authorize.net/gateway/transact.dll";
    }
    $agent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_TIMEOUT, 120);
                curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  //Windows 2003 Compatibility
    // added support for curl proxy
    if (defined('CURL_PROXY_HOST') && defined('CURL_PROXY_PORT') && CURL_PROXY_HOST != '' && CURL_PROXY_PORT != '') {
      curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
      curl_setopt($ch, CURLOPT_PROXY, CURL_PROXY_HOST . ":" . CURL_PROXY_PORT);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    }
    if (defined('CURL_PROXY_USER') && defined('CURL_PROXY_PASSWORD') && CURL_PROXY_USER != '' && CURL_PROXY_PASSWORD != '') {
      curl_setopt($ch, CURLOPT_PROXYUSERPWD, CURL_PROXY_USER . ':' . CURL_PROXY_PASSWORD);
    }
    $authorize = curl_exec($ch);
    curl_close($ch);
    $response = explode('|', $authorize);  // break the responce based on delim char sent
  }


//  if ((MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Test And Debug') || (MODULE_PAYMENT_AUTHORIZENET_TESTMODE == 'Monitor' )) {
    $str = ':Response String : ' . $authorize . "\n";
    $fp = fopen($filename,"ab");
    $write = fputs($fp, $str, strlen($str));
    $write = fputs($fp, "\n\n");
    fclose($fp);
//  }
?>