<?php
/*
  $Id: article_listing.php, v1.0 2003/12/04 12:00:00 datazen Exp $

  CRE Loaded, Commercial Open Source E-Commerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
// added for CDS CDpath support
$params = '';
$row = 0;
$column = 0;

$CDpath = (isset($_SESSION['CDpath'])) ? '&CDpath=' . $_SESSION['CDpath'] : '';
$listing_split = new splitPageResults_rspv($listing_sql, MAX_ARTICLES_PER_PAGE);
if (($listing_split->number_of_rows > 0) && ((ARTICLE_PREV_NEXT_BAR_LOCATION == 'top') || (ARTICLE_PREV_NEXT_BAR_LOCATION == 'both'))) {
  ?>
      <div class="col-lg-12 col-xs-12 product-listing-module-pagination large-margin-top">
        <div class="pull-left  page-results" style="padding-left:0px !important"><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_ARTICLES); ?></div>
        <div class="pull-right  no-margin-top">
          <ul class="pagination no-margin-top no-margin-bottom">
          <?php echo  $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?>

          </ul>
        </div>
      </div><div class="clear-both"></div>
  <?php
}
	if ($listing_split->number_of_rows > 0) {
		$articles_listing_query = tep_db_query($listing_split->sql_query);
		while ($articles_listing = tep_db_fetch_array($articles_listing_query)) {
			$articles_all = $articles_listing;
			include(DIR_WS_MODULES.'article_listing_view.php');
      } // End of listing loop
    } else {
      ?>
        <div class="main">
          <?php
          if ($topic_depth == 'articles') {
            echo TEXT_NO_ARTICLES;
          } else if (isset($_GET['authors_id'])) {
            echo  TEXT_NO_ARTICLES2;
          }
          ?>
        </div>

      <?php
    }
if (($listing_split->number_of_rows > 0) && ((ARTICLE_PREV_NEXT_BAR_LOCATION == 'bottom') || (ARTICLE_PREV_NEXT_BAR_LOCATION == 'both'))) {
  ?>

        <div class="col-lg-12 col-xs-12 product-listing-module-pagination margin-bottom large-padding-top" style="padding-left:0px;">
        <div class="pull-left no-padding large-margin-bottom page-results" style=""><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_ARTICLES); ?></div>
        <div class="pull-right large-margin-bottom no-margin-top">
          <ul class="pagination no-margin-top no-margin-bottom">
          <?php echo  $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?>

          </ul>
        </div>
      </div><div class="clear-both"></div>

  <?php
}
?>