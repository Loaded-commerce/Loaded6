<?php
include(DIR_WS_INCLUDES . 'html_top.php');
include(DIR_WS_INCLUDES . 'header.php');
include(DIR_WS_INCLUDES . 'column_left.php');
?>
<div id="content" class="content p-relative">         
  <h1 class="page-header"><i class="fa fa-laptop"></i> <?php echo HEADING_ADMIN_TITLE; ?></h1>

  <?php if (file_exists(DIR_WS_INCLUDES . 'toolbar.php')) include(DIR_WS_INCLUDES . 'toolbar.php'); ?>
  
  <div class="col">   
    <?php
    if ($messageStack->size('currencies') > 0) {
      echo $messageStack->output('currencies'); 
    }

  $payment_statuses = array(
                              array('id' =>'Completed',          'text' => 'Completed'),
                              array('id' =>'Pending',            'text' => 'Pending'),
                              array('id' =>'Failed',             'text' => 'Failed'),
                              array('id' =>'Denied',             'text' => 'Denied'),
                              array('id' =>'Refunded',           'text' => 'Refunded'),
                              array('id' =>'Reversed',           'text' => 'Reversed'),
                              array('id' =>'Canceled_Reversal',  'text' => 'Canceled_Reversal')
                            );

    ?>
      <div class="row">
        <div class="col-9"></div>
        <div class="col-3 pr-0">
           <div class="form-group row mb-2 pr-0">
            <label for="cPath" class="hidden-xs col-sm-3 col-form-label text-center m-t-10 pr-0"><?php echo HEADING_PAYMENT_STATUS; ?></label>
            <div class="col-sm-9 p-0 dark rounded">
				<?php echo tep_draw_form('payment_status', FILENAME_PAYPAL, '', 'get') .tep_draw_hidden_field(tep_session_name(), $_GET[tep_session_name()]) . tep_draw_pull_down_menu('payment_status', array_merge(array(array('id' => 'ALL', 'text' => TEXT_ALL_IPNS)), $payment_statuses), (isset($_GET['payment_status']) ? $_GET['payment_status'] : '' ), 'onChange="this.form.submit();"').'</form>'; ?>
            </div>
          </div>
          </div>
        </div>
      </div>

    <!-- begin panel -->
    <div class="dark">
      <!-- body_text //-->     
      <div id="table-currencies" class="table-currencies">
        <div class="row">
          <div class="col-md-8 col-xl-9 dark panel-left rounded-left">
    
			<?php require($page->contentFile); ?>    
    
          </div>
        </div>
      </div>   
      <!-- end body_text //-->
    </div>
    <!-- end panel -->
  </div>
</div>
<!-- body_eof //-->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Transaction Details </h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      
    </div>
</div>
<script>
function openpopup(par)
{
    $('.modal-body').load('<?php echo tep_href_link(FILENAME_PAYPAL, tep_get_all_get_params(array('ipnID', 'oID', 'action')) . 'action=details'); ?>&info='+par,function(){
        $('#myModal').modal({show:true});
    });
}
</script>
<?php 
include(DIR_WS_INCLUDES . 'html_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); 
?>
