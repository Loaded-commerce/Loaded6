<?php
$paymntmtd = "'".$selection[$i]['id']."'";
$list = array();
$list[]= array('id' => '', 'text' => 'Add New Card');
$existing_payment_query  = tep_db_query("SELECT * FROM customers_cc_info WHERE customers_id = '".$_SESSION['customer_id']."' AND cc_type = '".$selection[$i]['id']."' order by date_added");
if(tep_db_num_rows($existing_payment_query) > 0){
	  $loop =1;
	  while($rw_list = tep_db_fetch_array($existing_payment_query)){
	  	  $meta_data = json_decode($rw_list['meta_data'], true);
	  	  $customers_token_id = $meta_data['loaded_token_id'];
	  	  if($customers_token_id != ''){
			  if($loop == 1){
			    $sel_payment_profile_id = $customers_token_id;
			  }
			  $list[] = array('id' => $customers_token_id,'text' => $rw_list['card_number']);

			 $loop++;
	         }
	  }
}
$cc_section_sh = 'd-inline';

$paymentmrthod .='<div >
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 mx-auto no-padding" id="ccbox_'.$selection[$i]['id'].'">
								<div id="pay-invoice" class="">
									<div class="card-body">';
									 if(count($list) > 1){
									   $paymentmrthod .='<div class="form-group has-success nameoncard">
												<label for="Saved Cards" class="control-label mb-1">Saved Cards</label>
												
												 '.tep_draw_pull_down_menu($selection[$i]['id']."_existing_card", $list, $sel_payment_profile_id,' id="'.$selection[$i]['id'].'_existing_card"  class="form-control" onchange="javascript:showCCSection(\''.$selection[$i]['id'].'\',this.value)"').'
											</div>';
									   $cc_section_sh = 'd-none';
									 }else{
									  $paymentmrthod .='<input type="hidden" name="'.$selection[$i]['id'].'_existing_card" id="'.$selection[$i]['id'].'_existing_card" value=""/>';
									 }
									   
									$paymentmrthod .='<div id="'.$selection[$i]['id'].'_cc_section"  class="'.$cc_section_sh.'"> <div class="form-group has-success nameoncard">
												<label for="'.$selection[$i]['id'].'-cc-name" class="control-label mb-1">Name on card</label>
												<input id="'.$selection[$i]['id'].'-cc-name" name="'.$selection[$i]['id'].'_cc_firstname" value="'.$order->billing['firstname'] . ' ' . $order->billing['lastname'].'" type="text" class="form-control cc-name" required autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
											</div>
											<div class="row">
												<div class="col-12 col-lg-12 col-md-12 cardnumber">
													<div class="form-group">
														  <div class="card">
															<p>
																<label for="'.$selection[$i]['id'].'-cc-number" class="control-label mb-1">Card number</label>
																<input id="'.$selection[$i]['id'].'-cc-number" onClick="return paymentselection('.$paymntmtd.');" data-inputmask="\'mask\': \'9999999999999999\'" name="'.$selection[$i]['id'].'_cc_number" type="tel" class="form-control cc-number identified" required="" pattern="[0-9]{16}">
																<span class="card_icon"></span>
															</p>
														  </div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-6 col-lg-6 col-md-6 cardexpdate">
													<div class="form-group">
														<label for="'.$selection[$i]['id'].'-cc-exp" class="control-label mb-1">Expiration</label>
														<input id="'.$selection[$i]['id'].'-cc-exp" data-inputmask="\'mask\': \'99/99\'" name="'.$selection[$i]['id'].'_cc_expires" type="tel" class="form-control cc-exp" required placeholder="MM / YY" autocomplete="cc-exp">
													</div>
												</div>
												<div class="col-6 col-lg-6 col-md-6 cardcvvnum">
													<label for="'.$selection[$i]['id'].'-card-code" class="control-label mb-1">CVV</label>
													<div class="input-group">
														<input id="'.$selection[$i]['id'].'-card-code" id="cc-exp" data-inputmask="\'mask\': \'9999\'" name="'.$selection[$i]['id'].'_cc_checkcode" type="tel" class="form-control cc-cvc" required autocomplete="off">
														<div class="input-group-append">
															<div class="input-group-text">
																<span class="whatiscvv" data-toggle="modal" data-target="#whatisCVV"><i class="fa fa-question-circle fa-lg"></i></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										     </div>
									</div>
								</div>
							</div>
						</div>
					';
?>