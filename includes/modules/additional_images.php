<?php
//check to see if there is actually anything to be done here
if ( ($product_info['products_image_sm_1'] != '') || ($product_info['products_image_xl_1'] != '') ||
     ($product_info['products_image_sm_2'] != '') || ($product_info['products_image_xl_2'] != '') ||
     ($product_info['products_image_sm_3'] != '') || ($product_info['products_image_xl_3'] != '') ||
     ($product_info['products_image_sm_4'] != '') || ($product_info['products_image_xl_4'] != '') ||
     ($product_info['products_image_sm_5'] != '') || ($product_info['products_image_xl_5'] != '') ||
     ($product_info['products_image_sm_6'] != '') || ($product_info['products_image_xl_6'] != '') ) {

	for($adimg_cnt=1; $adimg_cnt <= 6; $adimg_cnt++) {
		$sm_img = '';
		$xl_img = '';
		if(trim($product_info['products_image_sm_'.$adimg_cnt]) != '') {
			$sm_img = trim($product_info['products_image_sm_'.$adimg_cnt]);
			$xl_img = trim($product_info['products_image_sm_'.$adimg_cnt]);
		}
		if(trim($product_info['products_image_xl_'.$adimg_cnt]) != '') {
			if($sm_img == '')
				$sm_img = trim($product_info['products_image_xl_'.$adimg_cnt]);
			$xl_img = trim($product_info['products_image_xl_'.$adimg_cnt]);
		}
		if($sm_img != '' && $xl_img != '') {
?>
	<div class="slider-item"><div class="product-block"><?php echo '<a rel="'.$product_info['products_name'] .'" class="thumbnail elevatezoom-gallery fancybox" title="'.$product_info['products_name'].'" href="'.DIR_WS_PRODUCTS . $xl_img.'">'.tep_image(DIR_WS_PRODUCTS . $sm_img, $product_info['products_name'], '85', '85','class="img-responsive img-thumbnail"').'</a>'; ?></div></div>
<?php
		}
	}
} // end of initial IF
?>