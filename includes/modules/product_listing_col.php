<?php
/*
  $Id: product_listing_col.php,v 1.1.1.1 2018/03/04 23:41:11 DeviDash Exp $
*/
//declare variables and initialize
// added for CDS CDpath support
$params = '';
$row = 0;
$column = 0;

$num_products_per_page = (isset($_GET['show']) && (int)$_GET['show'] > 0)?(int)$_GET['show']:MAX_DISPLAY_SEARCH_RESULTS;
$allowed_sort = array('1a', '1d', '2a', '2d','3a', '3d','4a', '4d','5a', '5d');
$sel_sort = (isset($_GET['sort']) && in_array($_GET['sort'], $allowed_sort))?trim($_GET['sort']):'';


$arr_rec_perpage[] = array('id'=>MAX_DISPLAY_SEARCH_RESULTS, 'text'=>MAX_DISPLAY_SEARCH_RESULTS);
for($i=1; $i<=5; $i++) {
	$rec_per_page = MAX_DISPLAY_SEARCH_RESULTS * (4 * $i);
	$arr_rec_perpage[] = array('id'=>$rec_per_page, 'text'=>$rec_per_page);
}

$listing_split = new splitPageResults_rspv($listing_sql, $num_products_per_page, 'p.products_id');
$list_box_contents = array();

  if ($listing_split->number_of_rows > 0) {
    $listing_query = tep_db_query($listing_split->sql_query);

    $row = 0;
    $column = 0;
    $no_of_listings = tep_db_num_rows($listing_query);


    echo tep_draw_form('search_filter', tep_href_link(basename($_SERVER['PHP_SELF']), tep_get_all_get_params(array('sort', 'show')), 'NONSSL', false), 'get').'<div class="row"><div class="category_filter">
						<div class="btn-group">
							<a href="javascript:;" id="grid" class="btn btn-default active" data-toggle="tooltip" data-plcaement="top" data-original-title="Grid" onclick="grid_view()"><i class="fa fa-th-large fa-1x" ></i></a>
							<a href="javascript:;" id="list" class="btn btn-default" data-toggle="tooltip" data-plcaement="top" data-original-title="List" onclick="list_view()"><i class="fa fa-th-list fa-1x" ></i></a>
						</div>
						<div class="pagination-right">
							 <div class="sort-by-wrapper">
								<div class="sortby"><label class="control-label sort-label">Sort By:</label>'. tep_draw_pull_down_menu('sort', get_sort_column_dropdown(), $sel_sort, 'class="form-control sort-label-class" name="sort" style="display:inline;max-width:63%;" onchange="document.search_filter.submit();"') .'</div>
							 </div>
							 <div class="show-wrapper">
								<div class="show"><label class="control-label">Show:</label>'. tep_draw_pull_down_menu('show', $arr_rec_perpage, $num_products_per_page, 'class="form-control selectshow" name="show" style="display:inline;max-width:60%;" onchange="document.search_filter.submit();"') .'</div>
							</div>
						</div>
			</div></div></form>';

	//echo '<div class="product-listing-module-container">';
	echo '<div id="products" class="col-12 col-xl-12 col-sm-12 col-lg-12 col-md-12"><div class="row">';
    while ($_listing = tep_db_fetch_array($listing_query)) {
      $product_contents = $obj_catalog->prod_grid_listing($_listing, $column_list);
      $lc_text = implode( $product_contents);
	  echo '<div class="'.$divcolumn.' product-listing-module-items item-outer">
	  			<div class="itembox">
	  				<div class="col-lg-12 col-xs-12 thumbnail align-center row itemboxthumb">' . $lc_text.'<div class="col-lg-8 col-xl-8 col-12 col-sm-12 showin-listview" style="display:none;"></div>' ;
	  	           if($pf->hasSpecialPrice == 1) echo '<div class="savepercent">'.floatval(get_special_percent($_listing['products_id'])).'%</div>';
	  echo '</div>
	  		</div></div>';

    }
    echo '</div></div>';
  }
  else
  {
	echo '<div class="product-listing-module-no-products"><div class="well"><p>'. TEXT_NO_PRODUCTS. '</p></div></div>';
  }
?>
  <div class="clearfix"></div>
  <div class="content-product-listing-div">
<?php
if ( ($listing_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3')) )
{
?>
      <div class="product-listing-module-pagination margin-bottom">
        <div class="pull-left large-margin-top page-results"><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></div>
        <div class="pull-right small-margin-top">
          <ul class="pagination no-margin-top no-margin-bottom">
           <?php echo  $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?>

          </ul>
        </div>
      </div>
      <div class="clear-both"></div>
<?php
}
?>
</div>
