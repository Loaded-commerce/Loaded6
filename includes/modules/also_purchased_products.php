<?php
/*
  $Id: also_purchased_products.php,v 1.2 2004/03/05 00:36:42 ccwjr Exp $

  LoadedCommerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2018 LoadedCommerce

  Released under the GNU General Public License
*/
?>
<div class="clearfix"></div>
<?php
if (isset($_GET['products_id']))
{
	$orders_query = tep_db_query("select p.products_id, p.products_image, p.products_model from " . TABLE_ORDERS_PRODUCTS . " opa, " . TABLE_ORDERS_PRODUCTS . " opb, " . TABLE_ORDERS . " o, " . TABLE_PRODUCTS . " p where opa.products_id = '" . (int)$_GET['products_id'] . "' and opa.orders_id = opb.orders_id and opb.products_id != '" . (int)$_GET['products_id'] . "' and opb.products_id = p.products_id and opb.orders_id = o.orders_id and p.products_status = '1' group by p.products_id order by o.date_purchased desc limit " . MAX_DISPLAY_ALSO_PURCHASED);
	$num_products_ordered = tep_db_num_rows($orders_query);
	if ($num_products_ordered >= MIN_DISPLAY_ALSO_PURCHASED)
	{
		echo '<div class="col-xl-12 col-lg-12 col-12 col-sm-12 fullpage-module-heading"><h3 class="mod_also_purchased">' . TEXT_ALSO_PURCHASED_PRODUCTS . '</h3></div>';
		echo '<div class="col-xl-12 col-lg-12 col-12 col-sm-12 also-purchase-container product-listing-module-container"><div class="row" style="margin:0px;">';
		while ($rw_also_purchased = tep_db_fetch_array($orders_query))
		{
			$rw_also_purchased['products_name'] = tep_get_products_name($rw_also_purchased['products_id']);
			$product_contents = $obj_catalog->prod_grid_listing($rw_also_purchased, $column_list);
			$lc_text = implode( $product_contents);
			echo '<div class="'.$divcolumn.' product-listing-module-items item-outer with-small-padding">
			        <div class=" itembox">
							<div class="thumbnail align-center row" style="margin-left:0px;margin-right:0px;height:350px;">'. $lc_text ;
				echo '</div>
					</div>
				  </div>';
		}
	echo '</div></div>
	<div class="clearfix"></div>'."\n";
	}
}
?>

