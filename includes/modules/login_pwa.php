<?php
//BOF: MaxiDVD Returning Customer Info SECTION
//===========================================================
$returning_customer_title = HEADING_RETURNING_CUSTOMER; // DDB - 040620 - PWA - change TEXT by HEADING

if ($setme != '')
{
$returning_customer_info = "
<!--Confirm Block-->

<td width=\"50%\" height=\"100%\" valign=\"top\"><table border=\"0\" width=\"100%\" height=\"100%\" cellspacing=\"1\" cellpadding=\"2\" class=\"infoBox\">
<tr class=\"infoBoxContents\">
<td>
<table border=\"0\" width=\"100%\" height=\"100%\" cellspacing=\"0\" cellpadding=\"2\">
 <tr>
   <td class=\"main\" colspan=\"2\">".TEXT_YOU_HAVE_TO_VALIDATE."</td>
 </tr>
 <tr>
   <td class=\"main\"><b>". ENTRY_EMAIL_ADDRESS."</b></td>
   <td class=\"main\">". tep_draw_input_field('email_address')."</td>
 </tr>
 <tr>
   <td class=\"main\"><b>". ENTRY_VALIDATION_CODE."</b></td>
   <td class=\"main\">".tep_draw_input_field('pass').tep_draw_input_field('password',$_POST['password'],'','hidden')."</td>
 </tr>
 <tr>
   <td class=\"smallText\" colspan=\"2\">". '<a href="' . tep_href_link('validate_new.php', '', 'SSL') . '">' . TEXT_NEW_VALIDATION_CODE . '</a>'."</td>
 </tr>
 <tr>
   <td colspan=\"2\">". tep_draw_separator('pixel_trans.gif', '100%', '10')."</td>
 </tr>
 <tr>
   <td colspan=\"2\"><table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"2\">
     <tr>
       <td width=\"10\">". tep_draw_separator('pixel_trans.gif', '10', '1')."</td>
       <td align=\"right\">".tep_template_image_submit('button_continue.gif', IMAGE_BUTTON_CONTINUE)."</td>
       <td width=\"10\">".tep_draw_separator('pixel_trans.gif', '10', '1')."</td>
     </tr>
</table>
        </table></td>
      </tr>
    </table></form></td>

<!--Confirm Block END-->
";

}else
{
$returning_customer_info = "


<div class=\"row\">
    <h1 class=\"no-margin-top\"></h1>
    <div class=\"row\">
      <div class=\"col-sm-6 col-lg-6 col-xs-12 login-top large-padding-left margin-top userloginwell\">
        <div class=\"well no-padding-top\">
            <h3 class=\"no-margin-top user-login\">" . MENU_TEXT_RETURNING_CUSTOMER . "</h3>
            <div class=\"form-group\">
			  <label class=\"control-label\">E-Mail Address</label>  " .tep_draw_input_field('email_address', '' , 'class="form-control login-email" placeholder="' . ENTRY_EMAIL_ADDRESS . '"') . "
            </div>
            <div class=\"form-group\">
		       <label class=\"control-label\">Password</label>  " . tep_draw_password_field('password', '' , 'class="form-control login-password" placeholder="' . MENU_TEXT_PASSWORD . '"') . "
            </div>
          <div class=\"button-set clearfix\">
          <p class=\"help-block small-margin-left\">" . '<a href="' . tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL') . '">' . TEXT_PASSWORD_FORGOTTEN . '</a>' . "</p>
		  <button class=\"pull-right btn btn-lg btn-primary\" type=\"submit\">" .MENU_TEXT_SIGN_IN. "</button>
          </div>

          </form>

        </div>
      </div>
      <div class=\"col-sm-6 col-lg-6 col-xs-12 login-top margin-top large-padding-left create-account-div\">
        <div class=\"well no-padding-top\">
          <h3 class=\"user-create\">" .TEXT_NEW_CUSTOMER."</h3>
          <p>" .TEXT_NEW_CUSTOMER_INTRODUCTION. "</p>
          <div class=\"buttons-set clearfix\">"
	       . '<a href="' . tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL') . '"><button type="button" class="pull-right btn btn-lg btn-primary">' . IMAGE_BUTTON_CREATE_ACCOUNT . '</button></a>' ."

          </div>
        </div>
      </div>
        </div>
      </div>





";
}
//===========================================================
// RCI code start
echo $cre_RCI->get('login', 'aboveloginbox');
// RCI code end
?>
<!-- login_pwa -->

    <tr>
     <td class="main" width=100% valign="top" align="center">
<?php
  $info_box_contents = array();
  $info_box_contents[] = array('align' => 'left',
                               'text'  => $returning_customer_title );
//  new contentBoxHeading($info_box_contents, false, false);

  $info_box_contents = array();
  $info_box_contents[] = array('align' => 'left',
                               'text'  => $returning_customer_info);
 new contentBox($info_box_contents, true, true);
  if (TEMPLATE_INCLUDE_CONTENT_FOOTER =='false'){
  //new contentboxFooter($info_box_contents);
  }
?>
  </td>
 </tr>

<?php
//EOF: MaxiDVD Returning Customer Info SECTION
//===========================================================

// RCI code start
echo $cre_RCI->get('login', 'belowloginbox');
// RCI code end

//MaxiDVD New Account Sign Up SECTION
//===========================================================
$create_account_title = HEADING_NEW_CUSTOMER;
$create_account_info = "";
//===========================================================
?>
    <tr>
     <td class="main" width=100% valign="top" align="center">
<?php
  $info_box_contents = array();
  $info_box_contents[] = array('align' => 'left',
                               'text'  => $create_account_title );
//  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();
  $info_box_contents[] = array('align' => 'left',
                               'text'  => $create_account_info);
  new contentBox($info_box_contents, true, true);
  if (TEMPLATE_INCLUDE_CONTENT_FOOTER =='true'){

//  new contentboxFooter($info_box_contents);
  }
?>
  </td>
  </tr>

<?php
//EOF: MaxiDVD New Account Sign Up SECTION
//===========================================================

?>
