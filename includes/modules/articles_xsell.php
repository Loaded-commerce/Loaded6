<?php
/*
  $Id: articles_xsell.php, v1.0 2003/12/04 12:00:00 ra Exp $

  LoadedCommerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2018 LoadedCommerce

Released under the GNU General Public License
*/
if ($_GET['articles_id'])
{
	$xsell_query = tep_db_query("select distinct a.products_id, a.products_image, ad.products_name from " . TABLE_ARTICLES_XSELL . " ax, " . TABLE_PRODUCTS . " a, " . TABLE_PRODUCTS_DESCRIPTION . " ad where ax.articles_id = '" . (int)$_GET['articles_id'] . "' and ax.xsell_id = a.products_id and a.products_id = ad.products_id and ad.language_id = '" . (int)$languages_id . "' and a.products_status = '1' order by ax.sort_order asc limit " . MAX_DISPLAY_ARTICLES_XSELL);
	$num_products_xsell = tep_db_num_rows($xsell_query);
	if ($num_products_xsell >= MIN_DISPLAY_ARTICLES_XSELL)
	{
		echo '<div class="col-xl-12 col-lg-12 col-12 col-sm-12 "><h3 class="mod_xsell">' . TEXT_XSELL_ARTICLES . '</h3></div>';
		echo '<div class="col-xl-12 col-lg-12 col-12 col-sm-12 article-xsell-container"><div class="row" style="margin:0px;">';
		while ($xsell = tep_db_fetch_array($xsell_query))
		{
			$product_contents = $obj_catalog->prod_grid_listing($xsell, $column_list);
			$lc_text = implode( $product_contents);
			echo '<div class="'.$divcolumn.' with-small-padding">
					<div class="col-lg-12 col-xs-12 thumbnail align-center">'. $lc_text .'</div>
				  </div>';
		}
		echo '</div></div><div class="clearfix"></div>'."\n";
	}
}
?>
