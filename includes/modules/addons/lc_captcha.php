<?php
class lc_captcha {

  public function __construct() {
	global $language;
	include(DIR_FS_CATALOG.DIR_WS_LANGUAGES . $language . '/modules/addons/lc_captcha.php');

      $this->code = 'lc_captcha';
      $this->title = (defined('MODULE_ADDONS_LC_CAPCTCHA_TITLE')) ? MODULE_ADDONS_LC_CAPCTCHA_TITLE : '';
      $this->description = (defined('MODULE_ADDONS_LC_CAPCTCHA_DESCRIPTION')) ? MODULE_ADDONS_LC_CAPCTCHA_DESCRIPTION : '';
      if (defined('MODULE_ADDONS_LC_CAPTCHA_STATUS')) {
        $this->enabled = ((MODULE_ADDONS_LC_CAPTCHA_STATUS == 'True') ? true : false);
      } else {
        $this->enabled = false;
      }
      $this->sort_order  = (defined('MODULE_ADDONS_LC_CAPTCHA_SORT_ORDER')) ? (int)MODULE_ADDONS_LC_CAPCTCHA_SORT_ORDER : 0;

  }  //end of __construct

  function display_captcha_label() {
	return MODULE_ADDONS_LC_CAPTCHA_LABEL;
  }  

  function display_captcha_instructions() {
	return MODULE_ADDONS_LC_CAPTCHA_INSTRUCTIONS;
  }  
 
  function check() {
      if (!isset($this->_check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_ADDONS_LC_CAPTCHA_STATUS'");
        $this->_check = tep_db_num_rows($check_query);
      }
      return $this->_check;
  }

  function install() {
      global $languages_id;
                  
      // insert module config values
      tep_db_query("INSERT IGNORE INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES ('', 'Enable Default Captcha', 'MODULE_ADDONS_LC_CAPTCHA_STATUS', 'True', 'Enable the captcha security code.', '811', '101', now(), now(), NULL, 'tep_cfg_select_option(array(''True'', ''False''),')");
      tep_db_query("INSERT IGNORE INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES ('', 'Captcha Strength', 'MODULE_ADDONS_LC_CAPTCHA_STRENGTH', '6', 'Number of Character Captcha you want to generate, Value 1 to 10.', '811', '101', now(), now(), NULL, '')");

	  $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_CAPTCHA_INSTALLED'");
	  if(tep_db_num_rows($check_query) > 0) {
	  	//Delete the Key and add in next step
		tep_db_query("DELETE FROM `configuration` WHERE configuration_key='MODULE_CAPTCHA_INSTALLED'");	  	
	  }
      tep_db_query("INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES ('', 'Installed Modules', 'MODULE_CAPTCHA_INSTALLED', 'lc_captcha.php', 'This is automatically updated. No need to edit.', '6', '', now(), now(), NULL, '')");
  }

  function remove() {
      tep_db_query("DELETE FROM `configuration` WHERE configuration_key in ('" . implode("', '", $this->keys()) . "')");
	  tep_db_query("DELETE FROM `configuration` WHERE configuration_key='MODULE_CAPTCHA_INSTALLED'");	  	
  }

  function keys() {
      return array('MODULE_ADDONS_LC_CAPTCHA_STATUS', 'MODULE_ADDONS_LC_CAPTCHA_STRENGTH');
  }

  function render_captcha_image_data() {

	$permitted_chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
	$input = $permitted_chars;
	$string_length = MODULE_ADDONS_LC_CAPTCHA_STRENGTH;
	$strength = MODULE_ADDONS_LC_CAPTCHA_STRENGTH;
	
	$input_length = strlen($input);
	$random_string = '';
	for($i = 0; $i < $strength; $i++) {
		$random_character = $input[mt_rand(0, $input_length - 1)];
		$random_string .= $random_character;
	}
	
	$captcha_string = $random_string;
	$captcha_code = $captcha_string;
	$_SESSION["captcha_code"] = $captcha_code;

	$image = imagecreatetruecolor(200, 40);

	imageantialias($image, true);

	$colors = [];

	$red = rand(125, 175);
	$green = rand(125, 175);
	$blue = rand(125, 175);

	for($i = 0; $i < 5; $i++) {
	  $colors[] = imagecolorallocate($image, $red - 20*$i, $green - 20*$i, $blue - 20*$i);
	}

	imagefill($image, 0, 0, $colors[0]);

	for($i = 0; $i < 10; $i++) {
	  imagesetthickness($image, rand(2, 10));
	  $line_color = $colors[rand(1, 4)];
	  imagerectangle($image, rand(-10, 190), rand(-10, 10), rand(-10, 190), rand(40, 60), $line_color);
	}

	$black = imagecolorallocate($image, 0, 0, 0);
	$white = imagecolorallocate($image, 255, 255, 255);
	$redt = imagecolorallocate($image, 255, 0, 0);
	$textcolors = [$black, $white, $redt];

	$fonts = [DIR_FS_CATALOG.'includes/fonts/acme.ttf', DIR_FS_CATALOG.'includes/fonts/ubuntu.ttf', DIR_FS_CATALOG.'includes/fonts/merriweather.ttf', DIR_FS_CATALOG.'includes/fonts/Ubuntu-RI.ttf', DIR_FS_CATALOG.'includes/fonts/Ubuntu-R.ttf', DIR_FS_CATALOG.'includes/fonts/playfairdisplay.ttf', DIR_FS_CATALOG.'includes/fonts/playfairdisplay-bold.ttf'];
	for($i = 0; $i < $string_length; $i++) {
	  $letter_space = 170/$string_length;
	  $initial = 15;

	  imagettftext($image, 24, rand(-15, 15), $initial + $i*$letter_space, rand(25, 35), $textcolors[rand(0, 3)], $fonts[array_rand($fonts)], $captcha_string[$i]);
	}

	ob_start();
	imagepng($image);
	$stringdata = ob_get_clean();
	imagedestroy($image);
	
	return $stringdata;

  }  

  function render_captcha_image($strength=6) {
	return '<img src="data:image/png;base64, '. base64_encode($this->render_captcha_image_data($strength)) .'" />';
  }  

  function render_captcha_control($params='') {
	return '<input type="text" name="ccode" style="display:inline;width:170px; height:40px;border-radius:0;" placeholder="Enter Captcha Code" '. $params .'>';
  }  

  function validate_captcha() {
	if(strtoupper($_SESSION['captcha_code']) == strtoupper($_POST['ccode'])) 
		return true;
	else
		return false;
		
	$_SESSION['captcha_code'] = md5(rand());
	unset($_SESSION['captcha_code']);
  }  
}
?>