<?php
$attributes = new creAttributes();
if ($attributes->load($load_attributes_for)) {
    $options_HTML = $attributes->get_HTML();
    if (count($options_HTML) > 0) {
        ?>
        <div class="col-sm-12 col-lg-12 clearfix padding-left pro-attribut">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th colspan="2"><strong><?php echo TEXT_PRODUCT_OPTIONS; ?></strong></th>
                    </tr>
                </thead>
                <?php
                foreach ($options_HTML as $op_data) {
                    ?>
                    <tr style="background:#f0f0f0;">
                        <td><?php echo $op_data['HTML'];  ?><?php echo $op_data['label']; ?></td>
                        <!--<td><?php echo $op_data['label']; ?></td>-->
                    </tr>
                    <?php
                } //end of foreach
                ?>
            </table>
        </div>    <div class="clearfix"></div>
        <?php
    }  // end of count
} // end of new attributes
?>