<?php
			$js_attribute_validation_code = '';
            $attributes = new creAttributes();
            if ($attributes->load($load_attributes_for)) {
              $js_attribute_validation_code = $attributes->get_js_validation();
              $options_HTML = $attributes->get_HTML();
              if (count($options_HTML) > 0) {
              	$products_has_attributes = true;
              ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="table table-borderless">
              <tr>
                <td class="main" colspan="2" align="center" style="background: #d3d3d3; opacity: 0.5;"><strong><?php echo TEXT_PRODUCT_OPTIONS; ?></strong></td>
              </tr>
<?php
                foreach ($options_HTML as $op_data) {
                if($op_data['HTML'] == ''){
                	$colspan = 'colspan="2"';
                }else{
                	$colspan = '';
                }
                ?>
              <tr>
                <td class="main attributeslabel" <?php echo $colspan; ?> style=" position: relative; vertical-align: middle; "><?php echo $op_data['label']; ?></td>
                <?php if($op_data['HTML'] != ''){ ?>
                	<td class="main attributeshtml"><?php echo $op_data['HTML'];  ?></td>
                <?php } ?>
              </tr>
<?php
                } //end of foreach
                ?>
          </table>
<?php
              }  // end of count
            } // end of new attributes
            ?>