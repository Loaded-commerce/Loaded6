<?php
/*
  $Id: html_output.php,v 1.2 2004/03/05 00:36:42 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// The HTML href link wrapper function
  function tep_href_link($page = '', $parameters = '', $connection = 'NONSSL', $add_session_id = true, $search_engine_safe = true) {
    global $request_type, $session_started, $SID, $spider_flag, $arr_addon_pages, $arr_direct_page;
    $req_page = $page;
    parse_str(str_ireplace('&amp;', '&', $parameters), $output_param);
    $output = $output_param;

  	if(in_array($req_page, $arr_direct_page))
		$page = $req_page;
  	else {

		if(isset($output_param['rt']))
		{
		  unset($output_param['rt']);
		  $parameters = http_build_query($output_param);
		}
		$arr_available_addons = json_decode(AVAILABLE_ADDONS_jSON);
		$addons_page = 0;
		foreach($arr_available_addons as $indv_addons) {
		  $addon_mod = substr($indv_addons, 3);
		  $arr_mod_files = is_array($arr_addon_pages[$addon_mod])?$arr_addon_pages[$addon_mod]:array();
		  if(in_array($page, $arr_mod_files)) {
			$parameters = (trim($parameters) == '')?'rt='. $addon_mod .'/'.substr($page, 0, -4):'rt='. $addon_mod .'/'.substr($page, 0, -4).'&'.$parameters;
			$page_mod = substr($page, 0, -4);
			$addons_page = 1;
		  }
		}

		if(!$addons_page) {
			$parameters = (trim($parameters) == '')?'rt=core/'.substr($page, 0, -4):'rt=core/'.substr($page, 0, -4).'&'.$parameters;
			$page_mod = substr($page, 0, -4);
		}

		$page = 'index.php';
	}

	if (defined('MODULE_ADDONS_ULTIMATESEO_STATUS') && MODULE_ADDONS_ULTIMATESEO_STATUS == 'True')
	{

		$friendly_name = '';
		if(isset($output_param['products_id']))
			$friendly_name = get_permalink_name($output['products_id'],'product');
		elseif(isset($output['cPath'])) {
			$arr_cat_path = explode('_', $output['cPath']);
			$friendly_name = '';
			if(count($arr_cat_path) > 0){
				$cat_id = end($arr_cat_path);
				$friendly_name = (($friendly_name == '')? get_permalink_name($cat_id, 'category'): $friendly_name.'/'.get_permalink_name($cat_id, 'category'));
			}
		}
		elseif(isset($output['manufacturers_id']) && $output['manufacturers_id'] > 0)
			$friendly_name =get_permalink_name($output['manufacturers_id'], 'manufacturer');
		elseif(isset($output['forms_id']) && $output['forms_id'] > 0)
			$friendly_name = 'forms/'.get_permalink_name($output['forms_id'], 'form_builder');
		elseif(isset($output['pID']))
			$friendly_name = get_permalink_name($output['pID'], 'page');
		elseif(isset($output['lPath']))
			$friendly_name = 'links/'.get_permalink_name($output['lPath'], 'link_category');
		elseif(isset($output['cID']))
			$friendly_name = 'faq/'.get_permalink_name($output['cID'], 'faq_category');
		elseif(isset($output['tPath']))
			$friendly_name = 'articles/'.get_permalink_name($output['tPath'], 'articles_category');
		elseif(isset($output['articles_id']))
			$friendly_name = 'articles/'.get_permalink_name($output['articles_id'], 'articles_info');
		elseif(isset($output['month']) && isset($output['year']))
			$friendly_name = 'articles/'.$output['year'].'-'.$output['month'];

		elseif(isset($output['testcategories_id']))
			$friendly_name = 'customer-testimonials/'.get_permalink_name($output['testcategories_id'], 'testimonialcat');
		elseif(isset($output['testimonial_id']))
			$friendly_name = 'customer-testimonials/tmid-'.$output['testimonial_id'];
		elseif(isset($output['lPath']))
			$friendly_name.= '/'.get_permalink_name($output['lPath'], 'link_category');
		elseif(isset($output['CDpath']))
			$friendly_name = get_permalink_name($output['CDpath'], 'page_category');
		else {
		    parse_str(str_ireplace('&amp;', '&', $parameters), $indv_param);
			$def_route = $indv_param['rt'];
			 $query = "SELECT * FROM default_permalink WHERE route='$def_route'";
			$rw_def = tep_db_fetch_array(tep_db_query($query));
			if(trim($rw_def['permalink_name']) != '')
				$friendly_name = trim($rw_def['permalink_name']);
		}

		if(isset($output['order_id']) && $output['order_id'] > 0)
			$friendly_name_param = '/oid-'. (int) $output['order_id'];
		elseif(isset($output['tlid']) && $output['tlid'] != '')
			$friendly_name_param = '/tlid-'.$output['tlid'];
		elseif(isset($output['fID']) && $output['fID'] != '')
			$friendly_name_param = '/fid-'.$output['fID'];
		elseif(isset($output['pid']) && $output['pid'] > 0)
			$friendly_name_param = '/pid-'.$output['pid'];
		elseif(isset($output['page']))
			$friendly_name_param = '/page-'.$output['page'];
		elseif(isset($output['sub']) && $output['sub'] != '')
			$friendly_name_param = '/sub-'.$output['sub'];
		elseif(isset($output['error_message']) && $output['error_message'] != '')
			$friendly_name_param = '/ERROR-'.$output['error_message'];
		elseif(isset($output['success_message']) && $output['success_message'] != '')
			$friendly_name_param = '/SUCCESS-'.$output['success_message'];
		elseif(isset($output['info_message']) && $output['info_message'] != '')
			$friendly_name_param = '/WARNING-'.$output['info_message'];
		elseif(isset($output['edit']))
			$friendly_name_param = '/edit-'.$output['edit'];
		elseif(isset($output['delete']))
			$friendly_name_param = '/delete-'.$output['delete'];
		elseif(isset($output['action']) && $output['action'] == 'deleteconfirm')
			$friendly_name_param = '/deleteconfirm';
		elseif($req_page == 'product_reviews_write.php')
			$friendly_name_param = '/write-your-review';
		elseif(isset($output['authors_id']) && (int) $output['authors_id'] > 0)
			$friendly_name_param = '/aid-'. (int) $output['authors_id'];
		elseif($req_page == 'article_reviews.php')
			$friendly_name_param = '/article-reviews';
		elseif($req_page == 'article_reviews_write.php')
			$friendly_name_param = '/write-your-article-review';
		elseif($req_page == 'article_reviews_info.php')
			$friendly_name_param = '/arid-'. (int) $output['reviews_id'];
		elseif($req_page == 'product_reviews.php')	
		$friendly_name = 'product-reviews/prid-'. (int) $output['products_id'];			

		$found_friendly_name = 0;
		if(trim($friendly_name) != '') {
			$parameters = '';
			$page = $friendly_name.$friendly_name_param;
			$found_friendly_name = 1;
		}
		$seo_params = '';
		$allowed_actions = array('buy_now', 'buy_quote', 'send', 'edit_quote', 'add_wishlist', 'add_product', 'edit_quote', 'emptycart', 'update_product', 'process');
		if(isset($output['action']) && in_array($output['action'], $allowed_actions))
			$seo_params = ($seo_params != '')?$seo_params."&action=".$output['action']:"action=".$output['action'];

		$allowed_append_vars = array('keywords', 'popup', 'subspopup', 'show_limit', 'show', 'sort', 'display', 'qID', 'quotes_id', 'action','sID','pass','verifyid');
		foreach($allowed_append_vars as $output_key) {
			if(isset($output[$output_key]))
				$seo_params = ($seo_params != '')?$seo_params."&". $output_key ."=".$output[$output_key]:$output_key."=".$output[$output_key];
		}

		if($found_friendly_name) {
			$parameters = $seo_params;
		}

	}
    parse_str(str_ireplace('&amp;', '&', $parameters), $final_param);
    if(count($final_param) == 1 && (isset($final_param['rt']) && $final_param['rt'] == 'core/index')) {
    	$parameters = '';
    	$page = '';
    }
    else {
		if (!tep_not_null($page)) {
		  die(TEP_HREF_LINK_ERROR1);
		}
    }

    if ($connection == 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_HTTP_CATALOG;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == 'true') {
        $link = HTTPS_SERVER . DIR_WS_HTTPS_CATALOG;
      } else {
        $link = HTTP_SERVER . DIR_WS_HTTP_CATALOG;
      }
    } else {
      die(TEP_HREF_LINK_ERROR2);
    }
    if (tep_not_null($parameters)) {
      while ( (substr($parameters, -5) == '&amp;') ) $parameters = substr($parameters, 0, strlen($parameters)-5);
      $link .= $page . '?' . tep_output_string($parameters, false, true);
      $separator = '&amp;';
    } else {
      $link .= $page;
      $separator = '?';
    }
    $link = str_replace('&amp;', '&', $link);
    return $link;
  }

////
// The HTML image wrapper function
  function tep_image($src, $alt = '', $width = '', $height = '', $parameters = '') {
    if ( (empty($src) || ($src == DIR_WS_IMAGES)) ) {
      $src = DIR_WS_DEFAULT . "no_image.png";
    }

    if (file_exists($src)) {  // is there actually a file to process
      if ( (CONFIG_CALCULATE_IMAGE_SIZE == 'true' && (empty($width) || empty($height))) ) {
        if ($image_size = @getimagesize($src)) {
          if (empty($width) && tep_not_null($height)) {
            $ratio = $height / $image_size[1];
            $width = $image_size[0] * $ratio;
          } elseif (tep_not_null($width) && empty($height)) {
            $ratio = $width / $image_size[0];
            $height = $image_size[1] * $ratio;
          } elseif (empty($width) && empty($height)) {
            $width = $image_size[0];
            $height = $image_size[1];
          }
        }else{
          $src = DIR_WS_DEFAULT . "no_image.png";
        }
      }
    } else {
      $src = DIR_WS_DEFAULT . "no_image.png";
    }

    // alt is added to the img tag even if it is null to prevent browsers from outputting
    // the image filename as default
    $image = '<img src="' . tep_output_string($src) . '" border="0" alt="' . tep_output_string($alt) . '"';

    if (tep_not_null($alt)) {
      $image .= ' title=" ' . tep_output_string($alt) . ' "';
    }

    if (tep_not_null($width) && tep_not_null($height)) {
      $image .= ' width="' . tep_output_string($width) . '" height="' . tep_output_string($height) . '"';
    }

    if (tep_not_null($parameters)) $image .= ' ' . $parameters;

    $image .= '>';

    return $image;
  }


// The Javascript Image wrapper build a image tag for a dummy picture,
// then uses javascript to load the actual picure.  This approach prevents spiders from stealing images.
  function tep_javascript_image($src, $name, $alt = '', $width = '', $height = '', $parameters = '', $popup = false) {
    global $product_info;
    $image = '';
    if ( empty($name) || ((empty($src) || ($src == DIR_WS_IMAGES)) && (IMAGE_REQUIRED == 'false')) ) {
      return false;
    }

    // alt is added to the img tag even if it is null to prevent browsers from outputting
    // the image filename as default
    $image .= '<img name="' . tep_output_string($name) . '" src="' . DIR_WS_IMAGES . 'pixel_trans.gif" border="0" alt="' . tep_output_string($alt) . '"';

    if (tep_not_null($alt)) {
      $image .= ' title=" ' . tep_output_string($alt) . ' "';
    }

    if ( (CONFIG_CALCULATE_IMAGE_SIZE == 'true' && (empty($width) || empty($height))) && file_exists($src) ) {
      if ($image_size = @getimagesize($src)) {
        if (empty($width) && tep_not_null($height)) {
          $ratio = $height / $image_size[1];
          $width = $image_size[0] * $ratio;
        } elseif (tep_not_null($width) && empty($height)) {
          $ratio = $width / $image_size[0];
          $height = $image_size[1] * $ratio;
        } elseif (empty($width) && empty($height)) {
          $width = $image_size[0];
          $height = $image_size[1];
        }
      } elseif (IMAGE_REQUIRED == 'false') {
        return false;
      }
    }

    if (tep_not_null($width) && tep_not_null($height)) {
      $image .= ' width="' . tep_output_string($width) . '" height="' . tep_output_string($height) . '"';
    }

    if (tep_not_null($parameters)) $image .= ' ' . $parameters;

    $image .= '>' . "\n";

    // Do we need to add the pop up link code?
    if ( $popup ) {
      $image .= '<div align="center"><a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_IMAGE, 'pID=' . $product_info['products_id'] . '&image=0') . '\')">' . "\n";
      $image .= '<br>' . tep_template_image_button('image_enlarge.gif', TEXT_CLICK_TO_ENLARGE) . '</a></div>' . "\n";
    }

    // Now for the Javascript loading code
    $image .= '<script type="text/javascript"><!-- ' . "\n";
    $image .= "document['" . tep_output_string($name) . "'].src = '" . tep_output_string($src) . "'" . "\n";
    $image .= ' //--></script>' ."\n";

    return $image;
  }


////
// Output a separator either through whitespace, or with an image
  function tep_draw_separator($image = 'pixel_black.gif', $width = '100%', $height = '1') {
    return tep_image(DIR_WS_IMAGES . $image, '', $width, $height);
  }

////
// Output a form
  function tep_draw_form($name, $action, $method = 'post', $parameters = '') {
    $action = str_replace('&amp;', '&', $action);
    $form = '<form name="' . tep_output_string($name) . '" action="' . tep_output_string($action) . '" method="' . tep_output_string($method) . '"';
    if (tep_not_null($parameters)) $form .= ' ' . $parameters;
    $form .= '>';

	$str_querystring = parse_url($action, PHP_URL_QUERY);
	parse_str($str_querystring, $arr_url);
	if(trim(strtolower($method)) == 'get') {
		foreach($arr_url as $qkey=>$qval){
			$form .= tep_draw_hidden_field($qkey, $qval);
		}
	}
    return $form;
  }

////
// Output a form input field
  function tep_draw_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
    $field = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '"';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    } elseif (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }

////
// Output a form password field
  function tep_draw_password_field($name, $value = '', $parameters = 'maxlength="40" autocomplete="off"') {
    return tep_draw_input_field($name, '', $parameters, 'password', false);
  }

////
// Output a selection field - alias function for tep_draw_checkbox_field() and tep_draw_radio_field()
  function tep_draw_selection_field($name, $type, $value = '', $checked = false, $parameters = '') {
    $selection = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '"';

    if (tep_not_null($value)) $selection .= ' value="' . tep_output_string($value) . '"';

    if ( ($checked == true) || ( isset($GLOBALS[$name]) && is_string($GLOBALS[$name]) && ( ($GLOBALS[$name] == 'on') || (isset($value) && (stripslashes($GLOBALS[$name]) == $value)) ) ) ) {
      $selection .= ' CHECKED';
    }

    if (tep_not_null($parameters)) $selection .= ' ' . $parameters;

    $selection .= '>';

    return $selection;
  }

////
// Output a form checkbox field
  function tep_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
    return tep_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }

////
// Output a form radio field
  function tep_draw_radio_field($name, $value = '', $checked = false, $parameters = '') {
    return tep_draw_selection_field($name, 'radio', $value, $checked, $parameters);
  }

////
// Output a form textarea field
  function tep_draw_textarea_field($name, $wrap, $width, $height, $text = '', $parameters = '', $reinsert_value = true) {
    $field = '<textarea name="' . tep_output_string($name) . '" wrap="' . tep_output_string($wrap) . '" cols="' . tep_output_string($width) . '" rows="' . tep_output_string($height) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= tep_output_string_protected(stripslashes($GLOBALS[$name]));
    } elseif (tep_not_null($text)) {
      $field .= tep_output_string_protected($text);
    }

    $field .= '</textarea>';

    return $field;
  }

////
// Output a form hidden field
  function tep_draw_hidden_field($name, $value = '', $parameters = '') {
    $field = '<input type="hidden" name="' . tep_output_string($name) . '"';

    if (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    } elseif (isset($GLOBALS[$name])) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }

////
// Hide form elements
  function tep_hide_session_id() {
    global $session_started, $SID;

    if (($session_started == true) && tep_not_null($SID)) {
      return tep_draw_hidden_field(tep_session_name(), tep_session_id());
    }
  }

////
// Output a form pull down menu
  function tep_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false) {
    $field = '<select name="' . tep_output_string($name) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if (empty($default) && isset($GLOBALS[$name])) $default = stripslashes($GLOBALS[$name]);

    for ($i=0, $n=sizeof($values); $i<$n; $i++) {
      $field .= '<option value="' . tep_output_string($values[$i]['id']) . '"';
      if ($default == $values[$i]['id']) {
        $field .= ' selected="selected"';
      }

      $field .= '>' . tep_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }

////
// Creates a pull-down list of countries
  function tep_get_country_list($name, $selected = '', $parameters = '') {
    $countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
    $countries = tep_get_countries();

    for ($i=0, $n=sizeof($countries); $i<$n; $i++) {
      $countries_array[] = array('id' => $countries[$i]['countries_id'], 'text' => $countries[$i]['countries_name']);
    }

    return tep_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
  }

////
// Generate reviews stars with css
  function reviews_rating_css($n, $size=''){
      if($n==5){
       $css_rating =  '<i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i>';
      } else if($n==4){
        $css_rating =  '<i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-unchecked '. $size . '"></i>';
      } else if($n=3){
        $css_rating =  '<i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-unchecked '. $size . '"></i><i class="fa fa-star star-unchecked '. $size . '"></i>';
      } else if($n=2){
        $css_rating =  '<i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star '. $size . '"></i><i class="fa fa-star '. $size . '"></i><i class="fa fa-star '. $size . '"></i>';
      } else if($n=1){
          $css_rating =  '<i class="fa fa-star star-checked '. $size . '"></i><i class="fa fa-star star-unchecked '. $size . '"></i><i class="fa fa-star star-unchecked '. $size . '"></i><i class="fa fa-star star-unchecked '. $size . '"></i><i class="fa fa-star star-unchecked '. $size . '"></i>';
      }
      return $css_rating;
  }
?>
