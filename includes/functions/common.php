<?php
/*
  $Id: common.php,v 1.1.1.1 2020/09/04 23:39:50 devidash Exp $

  LoadedCommerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce

  Released under the GNU General Public License
*/

function cleanString($string) {
	$string = strip_tags($string);
	$string = tep_db_prepare_input($string);
	return $string;
}

function cleanHTMLControls($htmldata) {

  $data = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $htmldata);
  $data = preg_replace('/<input\b[^>]*>/is', "", $data);
  $data = preg_replace('/<select\b[^>]*>(.*?)<\/select>/is', "", $data);
  $data = preg_replace('/<noscript\b[^>]*>(.*?)<\/noscript>/is', "", $data);
  $data = preg_replace('/<form\b[^>]*>(.*?)<\/form>/is', "", $data);

  return $data;
}

function check_bool($input) {

	if (is_bool($input) === true) {
		if($input)
			return 1;
		else
			return 0;
	} else {
		return 0;
	}
}

function check_integer($input) {
	$input = (int) $input;
	return $input;
}

function check_float($input) {
	$input = (float) $input;
	return $input;
}

function lc_check_constant_val($const_name, $const_val) {

	$retval = 0;
	if(defined($const_name)) {
		$retval = (constant($const_name) == $const_val)? 1 : 0 ;
	}
	return $retval;
}

function lc_defined_constant($const_name) {

	$retval = '';
	if(defined($const_name)) {
		$retval = constant($const_name);
	}
	return $retval;
}

function lc_defined_constant_int($const_name) {

	$retval = 0;
	if(defined($const_name)) {
		$retval = (int) constant($const_name);
	}
	return $retval;
}

function getCustomerIP()	{
	if (isset($_SERVER)) {
	  if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	  } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	  } else {
		$ip = $_SERVER['REMOTE_ADDR'];
	  }
	} else {
	  if (getenv('HTTP_X_FORWARDED_FOR')) {
		$ip = getenv('HTTP_X_FORWARDED_FOR');
	  } elseif (getenv('HTTP_CLIENT_IP')) {
		$ip = getenv('HTTP_CLIENT_IP');
	  } else {
		$ip = getenv('REMOTE_ADDR');
	  }
	}
	return $ip;
}

function getCustomerIPInfo()	{

	$ip = getCustomerIP();
	if(function_exists('getCustomerIPInfoPro')) {
		return getCustomerIPInfoPro();
	} else {
		$api = 'manual';
		$arrIpData = array();
		$client = gethostbyaddr($ip);
		$str = preg_split("/\./", $client);
		$i = count($str);
		$x = $i - 1;
		$n = $i - 2;
		$isp = (isset($str[$n]) ? $str[$n] : '') . "." . (isset($str[$x]) ? $str[$x] : '');
		$retData = array('ip' => $ip, 'isp' => $isp, 'api' => $api, 'data'=>$arrIpData);
		return $retData;
	}
}

?>