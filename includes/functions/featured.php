<?php
/*
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License

  Featured Products V1.1
  Expiry Functions
*/

////
// Sets the status of a featured product
  function tep_set_featured_status($featured_id, $status) {
    return tep_db_query("update " . TABLE_PRODUCTS . " set featured = '" . $status . "' where products_id = '" . $featured_id . "'");
  }

////
// Auto expire featured products
  function tep_expire_featured() {
    $featured_query = tep_db_query("select products_id from " . TABLE_PRODUCTS . " where products_status = 1 and now() >= featured_expires and featured_expires > 0");
    if (tep_db_num_rows($featured_query)) {
      while ($featured = tep_db_fetch_array($featured_query)) {
        tep_set_featured_status($featured['products_id'], '0');
      }
    }
  }
?>
