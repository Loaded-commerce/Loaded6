<?php
if(MODULE_ADDONS_ULTIMATESEO_STATUS == 'True')
{
	if(isset($_GET['srt']))
	{
		$page_url = $_GET['srt'];
		unset($_GET['srt']);

		$arr_url = explode('/',$page_url);
		$arr_url_count = count($arr_url);

		$buynow = 0;
		$page =0;
		$subpage ='';
		$deleteconfirm = 0;
		$edit = 0;
		$delete = 0;
		$insert = 0;
		$add_wishlist = 0;
		$add_del_products_wishlist = 0;

		$emptycart = 0;
		$update_product = 0;
		$reorder_product = 0;
		$update = 0;
		$save_cart = 0;
		$saved_add_to_cart = 0;
		$process = 0;
		$success = 0;
		$send = 0;
		$reason = '';
		$albums_id = 0;
		$form_id = 0;
		$last_element = $arr_url[(count($arr_url) - 1)];
		@$last_before_element = $arr_url[(count($arr_url) - 2)];
		$product_id = 0;
		$review_id = 0;
		$topic_id = 0;
		$articles_id = 0;
		$article_review = 0;
		$ticket_id = '';
		$review_write = 0;
		$products_review_write = 0;
		if(trim($last_element) == '')
		{
			$last_element = $arr_url[(count($arr_url) - 2)];
			$last_before_element = ((count($arr_url) - 3) >= 0)?$arr_url[(count($arr_url) - 3)]:'';
		}
		
		$arr_action_element = array('buynow'=>'buynow', 'products_review_write'=>'write-your-review', 'article_review'=>'article-reviews', 'review_write'=>'write-your-article-review'
						, 'deleteconfirm' => 'deleteconfirm', 'insert' => 'insert', 'update_product' => 'update_product', 'reorder_product' => 'reorder_product', 'update' => 'update'
						, 'save_cart' => 'save_cart', 'saved_add_to_cart' => 'saved_add_to_cart', 'add_wishlist' => 'add_wishlist', 'add_del_products_wishlist' => 'add_del_products_wishlist'
						, 'process' => 'process', 'success' => 'success', 'send' => 'send', 'emptycart' => 'emptycart');
		$arr_action_element_rev = array_flip($arr_action_element);

		if (array_key_exists($last_element, $arr_action_element_rev))
		{
			${$arr_action_element_rev[$last_element]} = 1;
			unset($arr_url[(count($arr_url) - 1)]);
			$last_element = $arr_url[(count($arr_url) - 1)];
		}

;
		$arr_friendly_var = array('tmid-'=>'testimonial_id', 'arid-'=>'reviews_id', 'aid-' => 'authors_id', 'stid-'=>'ticket_id', 'tlid-'=>'tlid', 'cid-'=>'cID', 'page-'=>'page',
		'edit-'=>'edit', 'delete-'=>'delete', 'ERROR-'=>'error_msg', 'spp'=>'spp', 'online'=>'online', 'SUCCESS-'=>'success_msg', 'WARNING-'=>'warning_msg', 'REASON-'=>'reason',
		'oid-'=>'order_id', 'wid-'=>'wishlist_token', 'rwid-'=>'review_id', 'pid-'=>'product_id', 'articles-'=>'articles_id','prid-'=>'products_id');

		foreach($arr_friendly_var as $fvar_key => $fvar_val)
		{
			$fvar_key_length = strlen($fvar_key);

			if(substr($last_element, 0, $fvar_key_length) == $fvar_key){
				${"tmp_".$fvar_val} = substr($arr_url[(count($arr_url) - 1)], $fvar_key_length);
				unset($arr_url[(count($arr_url) - 1)]);
				$last_element = $arr_url[(count($arr_url) - 1)];
				${$fvar_val} = ${"tmp_".$fvar_val};
				$_GET[$fvar_val] = ${$fvar_val};
			}

		}

		//removing bad non english/utf8 url
		$permalink_query = tep_db_query("SELECT * FROM permalinks where permalink_name = '".$last_element."' ");
		

		if(tep_db_num_rows($permalink_query) > 0){
		
			$row_perma = tep_db_fetch_array($permalink_query);
			$param_key = $row_perma['permalink_type'];
			
			//only single level in url instead of tree structure
			if($arr_url_count > 1 && ($param_key == 'category' || $param_key == 'product') && $page == 0 && $products_review_write == 0 && $subpage == ''){
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: ".$ltmp_ink.$last_element);
				exit();
			}

		}else{
			if($arr_url['0'] == 'articles' && strlen($last_element) == 7) {
			   $param_key = 'articles';
			   $yearmonth = $last_element;
			}else{
			 $page_referer_url = (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']!='') ? $_SERVER['HTTP_REFERER'] : $session_refrel_url;
			 $counter = 1;
			 $user_browser = $_SERVER['HTTP_USER_AGENT'];
			 $user_ip = $_SERVER['HTTP_X_REAL_IP'];
			 if($user_ip == ''){
				$user_ip = $_SERVER['REMOTE_ADDR'];
			 }
			 $date_time = date("Y-m-d H:i:s");
			header('HTTP/1.0 404 Not Found', true, 404);
			header("Location: ". tep_href_link('page-not-found.php'));
			exit();
		      }
		}
		switch($param_key)
		{
			case'manufacturer':
				$_GET['manufacturers_id'] = $row_perma['manufacturers_id'];
				$seo_page_name = 'index.php';
				$code_mod = 'core';
				$code_file = $seo_page_name;
				break;
			case'product':
				$temp_cpath = '';
				unset($arr_url[(count($arr_url) - 1)]);
				$cnt = 1;
				$num_param = count($arr_url);
				foreach($arr_url as $url_name){
					if($page_extension != '' && $num_param == $cnt) {
						$inner_query = tep_db_query("SELECT * FROM permalinks where permalink_name IN ('".$url_name."', '".$url_name.$page_extension."') ");
					} else  {
						$inner_query = tep_db_query("SELECT * FROM permalinks where permalink_name = '".$url_name."' ");
					}
					$row = tep_db_fetch_array($inner_query);
					$temp_cpath = (($temp_cpath == '')? $row['categories_id']: $temp_cpath.'_'.$row['categories_id']);
					$cnt++;
				}
				$_GET['cPath'] = $temp_cpath;
				$_GET['products_id'] = $row_perma['products_id'];
				if($buynow)
				  $_GET['action'] = 'buy_now';
				if($subpage != '')
				  $_GET['sub'] = $subpage;

				if(isset($products_review_write) && $products_review_write == 1)
					$seo_page_name = 'product_reviews_write.php';
				else
					$seo_page_name = 'product_info.php';

				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;
				break;
			case'category':
				$temp_cpath = '';
				$cnt = 1;
				$num_param = count($arr_url);
			   foreach($arr_url as $url_name){
					if($page_extension != '' && $num_param == $cnt) {
						$inner_query = tep_db_query("SELECT * FROM permalinks where permalink_name IN ('".$url_name."', '".$url_name.$page_extension."') ");
					} else  {
						$inner_query = tep_db_query("SELECT * FROM permalinks where permalink_name = '".$url_name."' ");
					}
					$row = tep_db_fetch_array($inner_query);
					$temp_cpath = (($temp_cpath == '')? $row['categories_id']: $temp_cpath.'_'.$row['categories_id']);
					$cnt++;
				}
				$_GET['cPath'] = $temp_cpath;
				if($buynow)
				  $_GET['action'] = 'buy_now';
				if($page > 0)
				  $_GET['page'] = $page;

				$seo_page_name = 'index.php';
				$_SERVER['PHP_SELF'] = $seo_page_name;
				$_SERVER['SCRIPT_NAME'] = $seo_page_name;
				$code_mod = 'core';
				$code_file = $seo_page_name;
				break;
			case'page':
				if(!isset($_GET['CDpath'])) {
					if(count($arr_url) >= 2){
						$inner_query = tep_db_query("SELECT * FROM permalinks where permalink_name = '".$arr_url[0]."' ");
						$row = tep_db_fetch_array($inner_query);
						$_GET['CDpath'] = $row['pages_categories_id'];
					} else {
						$query = "SELECT categories_id FROM pages_to_categories WHERE pages_id='". $row_perma['pages_id'] ."'";
						$row = tep_db_fetch_array(tep_db_query($query));
						$_GET['CDpath'] = $row['categories_id'];
					}
				}
				$_GET['pID'] = $row_perma['pages_id'];
				$seo_page_name = 'pages.php';
				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;
				break;
			case'link_category':
				$_GET['lPath'] = $row_perma['link_categories_id'];
				$seo_page_name = 'links.php';
				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;
				break;
			case'faq_category':
				$_GET['cID'] = $row_perma['faq_categories_id'];
				$seo_page_name = 'faq.php';
				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;
				break;

			case'page_category':
				$_GET['CDpath'] = $row_perma['pages_categories_id'];
				$seo_page_name = 'pages.php';
				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;
				break;
			case'specials':
				$seo_page_name = 'specials.php';
				$_GET['curr_page'] = 'specials';
				if($page > 0)
				  $_GET['page'] = $page;

				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;
				break;

			case'form_builder':
				$_GET['forms_id'] = $row_perma['form_id'];
				$seo_page_name = 'fss_forms_detail.php';
				if(count($_POST) > 1)
					$_GET['action'] = 'submit';
				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;
				break;
			case'articles':
				if($yearmonth != ''){
				  $seo_page_name = 'articles.php';
				  $yearmontharr = explode('-',$yearmonth);
				  $_GET['year'] = $yearmontharr['0'];
				  $_GET['month'] = $yearmontharr['1'];
				}else{			
					$_GET['articles_id'] = $row_perma['articles_id'];
					$seo_page_name = 'article_info.php';
					unset($_GET['pgID']);
					if($review_write > 0)
						$seo_page_name = 'article_reviews_write.php';
					elseif($article_review > 0)
					  $seo_page_name = 'article_reviews.php';
					elseif(isset($_GET['reviews_id']))
					  $seo_page_name = 'article_reviews_info.php';
					else
						$seo_page_name = 'article_info.php';

				}
				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;				
				break;

			case'topics':
				$_GET['tPath'] = $row_perma['topics_id'];
				$seo_page_name = 'articles.php';
				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;
				break;
			case'testimonialcat':
				$_GET['testcategories_id'] = $row_perma['testimonial_cat_id'];
				$seo_page_name = 'customer_testimonials.php';
				$_SERVER['PHP_SELF'] = $seo_page_name;
				$_SERVER['SCRIPT_NAME'] = $seo_page_name;
				$code_mod = 'core';
				$code_file = $seo_page_name;
				$_SERVER['PHP_SELF'] = $seo_page_name;
				break;

			case'static':
				$permalink_name = $row_perma['permalink_name'];
				$seo_page_name = $row_perma['filename'];
				$_SERVER['PHP_SELF'] = $seo_page_name;
				$_SERVER['SCRIPT_NAME'] = $seo_page_name;
				$arr_route = explode('/', $row_perma['route']);
				$code_mod = $arr_route['0'];
				$code_file = $seo_page_name;

				if($page > 0)
				  $_GET['page'] = $page;

				if($insert)
				  $_GET['action'] = 'insert';
				if($reorder_product)
				  $_GET['action'] = 'reorder_product';
				if($update_product)
				  $_GET['action'] = 'update_product';
				if($update)
				  $_GET['action'] = 'update';
				if($emptycart)
				  $_GET['action'] = 'emptycart';
				if($save_cart)
				  $_GET['action'] = 'save_cart';
				if($saved_add_to_cart)
				  $_GET['action'] = 'saved_add_to_cart';

				if($add_wishlist)
				  $_GET['action'] = 'add_wishlist';

				if($add_del_products_wishlist)
				  $_GET['action'] = 'add_del_products_wishlist';

				if($process)
				  $_GET['action'] = 'process';
				if($success)
				  $_GET['action'] = 'success';
				if($send)
				  $_GET['action'] = 'send';

				if(isset($edit) && $edit > 0)
				 $_GET['edit'] = $edit;

				if(isset($delete) && $delete > 0)
				  $_GET['delete'] = $delete;

				if(isset($error_msg) && $error_msg != '')
				  $_GET['error_message'] = $error_msg;
				if(isset($status) && $status != '')
				  $_GET['status'] = $status;

				if(isset($success_msg) && $success_msg != '')
				  $_GET['success_message'] = $success_msg;

				if(isset($warning_msg) && $warning_msg != '')
				  $_GET['warning_msg'] = $warning_msg;

				if(isset($order_id) && $order_id > 0)
				  $_GET['order_id'] = $order_id;
				if(isset($product_id) && $product_id > 0 && $permalink_name !='product-reviews-info')
				  $_GET['pid'] = $product_id;

				
				if(isset($topic_id) && $topic_id > 0)
				  $_GET['tPath'] = $topic_id;

				if(isset($articles_id) && $articles_id > 0)
				  $_GET['articles_id'] = $articles_id;

				if(isset($review_id) && $review_id > 0)
				  $_GET['reviews_id'] = $review_id;

				if(isset($testimonial_id) && $testimonial_id > 0)
				  $_GET['testimonial_id'] = $testimonial_id;

				if(isset($wishlist_token) && $wishlist_token > 0)
				  $_GET['wishlist_token'] = $wishlist_token;

				if($deleteconfirm)
				  $_GET['action'] = 'deleteconfirm';
				if($reason != '') {
					$_GET['st'] = 'fail';
					$_GET['reason'] = $reason;
				}


			$code_mod = ($code_mod != '')?$code_mod:'core';
			$code_file = $seo_page_name;
			break;
		}
		$obj_catalog->code_mod = $code_mod;
		$obj_catalog->code_file = $code_file;
		$obj_catalog->code_page = substr($code_file, 0, -4);
	}
	else
	{
		if(isset($_GET['rt']) && trim($_GET['rt']) != '')
		{
			$rt = isset($_GET['rt'])?$_GET['rt']:'';
			if($rt != '') {
				$arr_route = explode('/', $_GET['rt']);
				$code_mod = trim($arr_route[0]);
				$code_page = trim($arr_route[1]);
				$code_file = trim($arr_route[1]).'.php';
			}else {
				$code_mod = 'core';
				$code_page = 'index';
				$code_file = 'index.php';
			}
			$obj_catalog->code_mod = $code_mod;
			$obj_catalog->code_file = $code_file;
			$obj_catalog->code_page = $code_page;
			$_SERVER['PHP_SELF'] = $code_file;
			$_SERVER['SCRIPT_NAME'] = $code_file;
		}
	}
}
?>
