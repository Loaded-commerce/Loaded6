<?php
/*
  $Id: cre_RCI.php,v 1.0.0.0 2006/11/21 13:41:11 ccwjr Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2006 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  class cre_RCI {
    var $_folders = array();

    function __construct() {
      if ( ! isset($_SESSION['cre_RCI_data']) ) {
        $_SESSION['cre_RCI_data'] = array('folders' => array() );
      }
      $this->_folders =& $_SESSION['cre_RCI_data']['folders'];
    }

    function get($pageName, $function, $display = false) {
      $pageName = strtolower($pageName);
      $function = strtolower($function);
      $rci_holder = '';
      
      // if cache is allowed, see if the page name is known
      if (USE_CACHE == 'false' || USE_CACHE == 'False' || ! array_key_exists($pageName, $this->_folders) ) {
        $this->_build_folder($pageName);
      }

      if ( array_key_exists($function, $this->_folders[$pageName]) ) {
        foreach( $this->_folders[$pageName][$function] as $fileName ) {
          // safety check in case a fasle positive was received on the cache check
          if ( file_exists($fileName) ) {
			  $rci = '';
			  if ($pageName == 'stylesheet') {  // special case of a style sheet
				$rci = '<link rel="stylesheet" type="text/css" href="' . $fileName . '">';
			  } else {
				include($fileName);
			  }
			  if ((DISPLAY_PAGE_PARSE_TIME == 'true') && ($display == true)) {
				$rci_holder .= '<!-- RCI [BOM] -' . $fileName . ' -->' . "\n" . $rci . '<!-- RCI [EOM] -' . $fileName . ' -->' . "\n";
			  } else {
				$rci_holder .= $rci;
			  }
          }
          else {
            $this->_folders = array();  // invalid the cache since it is corrupt
            continue;
          }
        }
      }
      return $rci_holder;
    }

    function _build_folder($pageName) {
      $this->_folders[$pageName] = array();    

      $checkfolder[] = DIR_WS_INCLUDES . 'runtime/' . $pageName;
      $arr_available_addons = json_decode(AVAILABLE_ADDONS_jSON);
      foreach($arr_available_addons as $indv_addons) {
	      $checkfolder[] = DIR_FS_CATALOG.'addons/'. $indv_addons .'/catalog/includes/runtime/' . $pageName;
      }

	  $filesFound = array();
      foreach($checkfolder as $indv_folders) {
		  if ( is_dir($indv_folders) ) {
			if ($pageName == 'stylesheet') {
			  $pattern = '/(\w*)_*(\w+)_(\w+)_(\w+)\.css$/';
			}else{
			  $pattern = '/(\w*)_*(\w+)_(\w+)_(\w+)\.php$/';
			}
			$dir = opendir($indv_folders);
			while( $file = readdir( $dir ) ) {
			  if ($file == '.'  || $file == '..') continue;
			  $match = array();
			  if ( preg_match($pattern, $file, $match) > 0 ) {
				if ( $match[3] == $pageName ) {
				  $filesFound[$indv_folders.'/'.$match[0]] = $match[4];
				}
			  }
			}
		  }
		}

        if ( count($filesFound) > 0) {
          ksort($filesFound);
          foreach( $filesFound as $file => $function ) {
            $this->_folders[$pageName][$function][] = $file;
          }
        }
    }
  }
?>