<?php
class lc_customers {

  public function __construct() {
	global $language;

  }  //end of __construct

  public function checkCustomerExist($cEmail) {
	global $language;

	$check_email_query = tep_db_query("select count(customers_id) as total from " . TABLE_CUSTOMERS . " where lower(customers_email_address) = '" . tep_db_input($cEmail) . "'");
	$check_email = tep_db_fetch_array($check_email_query);
	if ($check_email['total'] > 0) 
		return 1;
	else	
		return 0;
  }  

  public function getCustomerById($cId) {
	global $language;

	$query = "SELECT * FROM ". TABLE_CUSTOMERS." A INNER JOIN ". TABLE_CUSTOMERS_INFO ." B ON A.customers_id=B.customers_info_id INNER JOIN ". TABLE_ADDRESS_BOOK ." C ON A.customers_default_address_id=C.address_book_id WHERE A.customers_id='". (int) $cId ."'";
	$rw = tep_db_fetch_array(tep_db_query($query));
	return $rw;
  }  

  public function getCustomerByEmail($cEmail) {
	global $language;
	$query = "SELECT * FROM ". TABLE_CUSTOMERS." A INNER JOIN ". TABLE_CUSTOMERS_INFO ." B ON A.customers_id=B.customers_info_id INNER JOIN ". TABLE_ADDRESS_BOOK ." C ON A.customers_default_address_id=C.address_book_id WHERE A.customers_email_address='". tep_db_input($cEmail) ."'";
	$rw = tep_db_fetch_array(tep_db_query($query));
	return $rw;

  }  
  public function modelCustomer() {

	$customer_model = array('customers_firstname'=>'cleanString', 'customers_lastname'=>'cleanString', 'customers_email_address'=>'cleanString', 'customers_password'=>'', 'customers_newsletter'=>'check_bool', 'purchased_without_account'=>'check_bool', 'is_complete'=>'check_bool', 'customers_gender'=>'cleanString', 'customers_dob'=>'cleanString');
	return $customer_model;
  }

  public function applyType($model, $data) {
  
  	$arr_sanitize_data = array();
  	foreach($data as $dkey=>$dval) {
  		if(array_key_exists($dkey, $model)) {
	  		if(trim($model[$dkey]) != '' && function_exists($model[$dkey])) {
				$func_name = $model[$dkey];
  				$arr_sanitize_data[$dkey] = $func_name($dval);
			} else {
				$arr_sanitize_data[$dkey] = $dval;
			}
		}
  	}
 	return $arr_sanitize_data; 	
  }  

  public function addCustomer($cData) {
	global $language;
	$cData = $this->applyType($this->modelCustomer(), $cData);
	if((isset($cData['customers_firstname']) && trim($cData['customers_firstname']) == '') || (isset($cData['customers_lastname']) && trim($cData['customers_lastname']) == '') || (isset($cData['customers_email_address']) && trim($cData['customers_email_address']) == '')) {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {
		$sql_data_array = array('customers_firstname' => $cData['customers_firstname'],
								'customers_lastname' => $cData['customers_lastname'],
								'customers_email_address' => $cData['customers_email_address']);
		if (isset($cData['customers_newsletter'])) $sql_data_array['customers_newsletter'] = $cData['customers_newsletter'];
		if (isset($cData['purchased_without_account'])) $sql_data_array['purchased_without_account'] = $cData['purchased_without_account'];
		if (isset($cData['is_complete'])) $sql_data_array['is_complete'] = $cData['is_complete'];
		if (isset($cData['customers_password']) && trim($cData['customers_password']) != '') $sql_data_array['customers_password'] = tep_encrypt_password($cData['customers_password']);
		if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $cData['gender'];
		if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($cData['dob']);
		if (ACCOUNT_EMAIL_CONFIRMATION == 'false' ) $sql_data_array['customers_validation'] = '1';
		tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);
		$customers_id = tep_db_insert_id();

	    tep_db_query("insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_number_of_logons, customers_info_date_account_created) values ('" . (int)$customers_id . "', '0', now())");

		$return_data = array('status'=>1, 'data'=>array('customers_id'=>$customers_id));
		return $return_data;
	}
  }  

  public function updateCustomer($cId, $cData) {
	global $language;
	$cData = $this->applyType($this->modelCustomer(), $cData);
	if($cId <= 0 || (isset($cData['customers_firstname']) && trim($cData['customers_firstname']) == '') || (isset($cData['customers_lastname']) && trim($cData['customers_lastname']) == '')) {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {
		$sql_data_array = array('customers_firstname' => $cData['customers_firstname'],
								'customers_lastname' => $cData['customers_lastname']);
		//if (isset($cData['customers_email_address'])) $sql_data_array['customers_email_address'] = $cData['customers_email_address'];
		if (isset($cData['customers_newsletter'])) $sql_data_array['customers_newsletter'] = $cData['customers_newsletter'];
		if (isset($cData['purchased_without_account'])) $sql_data_array['purchased_without_account'] = $cData['purchased_without_account'];
		if (isset($cData['is_complete'])) $sql_data_array['is_complete'] = $cData['is_complete'];
		if (isset($cData['customers_password'])) $sql_data_array['customers_password'] = tep_encrypt_password($cData['customers_password']);
		if (ACCOUNT_GENDER == 'true' && isset($cData['customers_gender'])) $sql_data_array['customers_gender'] = $cData['customers_gender'];
		if (ACCOUNT_DOB == 'true' && isset($cData['customers_dob'])) $sql_data_array['customers_dob'] = tep_date_raw($cData['customers_dob']);

		tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$cId . "'");

		tep_db_query("UPDATE " . TABLE_CUSTOMERS_INFO . " set customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$cId . "'");

		$return_data = array('status'=>1, 'data'=>array('customers_id'=>$cId));
		return $return_data;
	}
  }  

  public function setIsComplete($cId, $complete) {

		tep_db_query("UPDATE " . TABLE_CUSTOMERS . " set is_complete = ". (int)$complete ." where customers_id = '" . (int) $cId . "'");
  }

  public function setCustomerValidation($cId, $validation) {

		tep_db_query("UPDATE " . TABLE_CUSTOMERS . " set customers_validation = ". (int)$validation ." where customers_id = '" . (int) $cId . "'");
  }

  public function deleteCustomer($cId) {
	global $language;
	$cId = (int) $cId;
	
	$query = "SELECT address_id FROM ". TABLE_ADDRESS_BOOK ." WHERE customers_id='". $cId ."'";
	$rs = tep_db_query($query);
	while($rw = tep_db_fetch_array($rs)) {
	
		$this->deleteAddressBook($cId, $rw['address_id']);
	
	}
	
	$query = "DELETE FROM ". TABLE_CUSTOMERS ." WHERE customers_id='". $cId ."'";
	tep_db_query($query);

	$query = "DELETE FROM ". TABLE_CUSTOMERS_INFO ." WHERE customers_info_id='". $cId ."'";
	tep_db_query($query);
  }  

  public function modelAddressBook() {

	$addressbook_model = array('customers_id'=>'check_integer', 'entry_firstname'=>'cleanString', 'entry_lastname'=>'cleanString', 'entry_email_address'=>'cleanString', 'entry_telephone'=>'cleanString', 'entry_fax'=>'cleanString', 'entry_street_address'=>'cleanString', 'entry_postcode'=>'cleanString', 'entry_city'=>'cleanString', 'entry_country_id'=>'check_integer', 'entry_gender'=>'cleanString', 'entry_company'=>'cleanString', 'entry_company_tax_id'=>'cleanString', 'entry_suburb'=>'cleanString', 'entry_zone_id'=>'check_integer', 'entry_state'=>'cleanString');
	return $addressbook_model;
  }

  public function addAddressBook($cId, $abData) {
	global $language;
	$abData = $this->applyType($this->modelAddressBook(), $abData);
	$cId = (int) $cId;
	if($cId <= 0 || (isset($abData['entry_firstname']) && trim($abData['entry_firstname']) == '') || (isset($abData['entry_lastname']) && trim($abData['entry_lastname']) == '')) {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {

	  $sql_data_array = array('customers_id' => $abData['customers_id'],
	  						  'entry_firstname' => $abData['entry_firstname'],
							  'entry_lastname' => $abData['entry_lastname'],
							  'entry_email_address' => $abData['entry_email_address'],
							  'entry_telephone' => $abData['entry_telephone'],
							  'entry_fax' => $abData['entry_fax'],
							  'entry_street_address' => $abData['entry_street_address'],
							  'entry_postcode' => $abData['entry_postcode'],
							  'entry_city' => $abData['entry_city'],
							  'entry_country_id' => (int)$abData['entry_country_id']);

	  if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $abData['entry_gender'];
	  if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $abData['entry_company'];

	  if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $abData['entry_suburb'];
	  if (ACCOUNT_STATE == 'true') {
		if ($abData['entry_zone_id'] > 0) {
		  $sql_data_array['entry_zone_id'] = (int)$abData['entry_zone_id'];
		  $sql_data_array['entry_state'] = '';
		} else {
		  $sql_data_array['entry_zone_id'] = '0';
		  $sql_data_array['entry_state'] = $abData['entry_state'];
		}
	  }
		tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
		$address_id = tep_db_insert_id();
		$return_data = array('status'=>1, 'data'=>array('address_id'=>$address_id));
		return $return_data;
	}
  }  

  public function updateAddressBook($cId, $abId, $abData) {
	global $language;
	$abData = $this->applyType($this->modelAddressBook(), $abData);
	$cId = (int) $cId;
	$abId = (int) $abId;
	if($cId <= 0 ||  $abId <= 0 || (isset($abData['entry_firstname']) && trim($abData['entry_firstname']) == '') || (isset($abData['entry_lastname']) && trim($abData['entry_lastname']) == '')) {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {

      $sql_data_array = array('entry_firstname' => $abData['entry_firstname'],
                              'entry_lastname' => $abData['entry_lastname']);
	  if (isset($abData['entry_email_address'])) $sql_data_array['entry_email_address'] = $abData['entry_email_address'];
	  if (isset($abData['entry_street_address'])) $sql_data_array['entry_street_address'] = $abData['entry_street_address'];
	  if (isset($abData['entry_telephone'])) $sql_data_array['entry_telephone'] = $abData['entry_telephone'];
	  if (isset($abData['entry_fax'])) $sql_data_array['entry_fax'] = $abData['entry_fax'];
	  if (isset($abData['entry_postcode'])) $sql_data_array['entry_postcode'] = $abData['entry_postcode'];
	  if (isset($abData['entry_city'])) $sql_data_array['entry_city'] = $abData['entry_city'];
	  if (isset($abData['entry_country_id'])) $sql_data_array['entry_country_id'] = $abData['entry_country_id'];

      if (ACCOUNT_GENDER == 'true' && isset($abData['entry_gender'])) $sql_data_array['entry_gender'] = $abData['entry_gender'];
      if (ACCOUNT_COMPANY == 'true' && isset($abData['entry_company'])) $sql_data_array['entry_company'] = $abData['entry_company'];
      if (ACCOUNT_SUBURB == 'true' && isset($abData['entry_suburb'])) $sql_data_array['entry_suburb'] = $abData['entry_suburb'];
      if (ACCOUNT_STATE == 'true' && (isset($abData['entry_zone_id']) || $abData['entry_state'])) {
        if (isset($abData['entry_zone_id']) && $abData['entry_zone_id'] > 0) {
          $sql_data_array['entry_zone_id'] = (int)$abData['entry_zone_id'];
          $sql_data_array['entry_state'] = '';
        } else {
          $sql_data_array['entry_zone_id'] = '0';
          $sql_data_array['entry_state'] = $abData['entry_state'];
        }
      }
		tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "address_book_id = '" . (int)$abId . "' and customers_id ='" . (int)$cId . "'");
		$return_data = array('status'=>1, 'data'=>array('customers_id'=>$cId, 'address_id'=>$address_id));
		return $return_data;
	}
  }  

  public function deleteAddressBook($cId, $abId) {
	global $language;
	$cId = (int) $cId;
	$abId = (int) $abId;
	if($cId <= 0 ||  $abId <= 0) {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {

		$query = "DELETE FROM ". TABLE_ADDRESS_BOOK ." WHERE customers_id='". $cId ."' AND address_id='". $abId ."'";
		tep_db_query($query);
		$return_data = array('status'=>1, 'msg' => '');
		return $return_data;		
	}

  }  

  public function getAllAddressById($cId) {
	$addresses_query = "select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$cId . "' order by address_book_id ASC";
	$data = tep_db_get_result($addresses_query);
	return $data;
  }
  
  public function getAddressBookById($cId, $abId) {

	$query = "SELECT address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city
				, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id, entry_telephone as telephone, entry_fax as fax
				from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$cId . "' and address_book_id = '" . (int)$abId . "'";
	$address = tep_db_fetch_array(tep_db_query($query));
	return $address;
	
  }

  public function checkValidAddress($cId, $abId) {

	$address = $this->getAddressBookById($cId, $abId);
	if(isset($address['address_book_id']) && $address['address_book_id'] > 0)
		return 1;
	else	
		return 0;
  }

  public function setDefaultAddress($cId, $abId) {
	if($cId <= 0 ||  $abId <= 0) {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {
		tep_db_query("UPDATE " . TABLE_CUSTOMERS . " set customers_default_address_id = ". (int)$abId ." where customers_id = '" . (int) $cId . "'");
		$return_data = array('status'=>1, 'msg' => '');
		return $return_data;		
	}
  }
  
  public function showCustomerInfoBrief($account) {
	ob_start();
	include('templates/content/partial/brief_customer_info.tpl.php');	  
	$page_data = ob_get_clean();
	return $page_data;
  }

  public function updateValidationCode($cId, $code) {
      tep_db_query('UPDATE '. TABLE_CUSTOMERS .' SET customers_validation_code = "' . $code . '" where customers_id = "' . (int) $cId . '"');
  }

  public function updatePointsIP($cId, $ip) {
      tep_db_query("UPDATE " . TABLE_CUSTOMERS . " set customers_points_ip = '" . $ip . "' where customers_id = '" . (int)$cId . "'");
  }
  public function clean_guest_account($email_address, $restrict_id='') {
	if(trim($email_address) != '') {

		$query = "select customers_id, customers_email_address, purchased_without_account from " . TABLE_CUSTOMERS . " where lower(customers_email_address) = '" . tep_db_input($email_address) . "' and purchased_without_account = 1";
		if(trim($restrict_id) != '') {
			$query .= ' AND customers_id NOT IN ('. $restrict_id .')';
		}

        $get_customer_info = tep_db_query($query);
        while($customer_info = tep_db_fetch_array($get_customer_info)) {
			$customer_id = $customer_info['customers_id'];

			  tep_db_query("delete from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . $customer_id . "'");
			  tep_db_query("delete from " . TABLE_CUSTOMERS . " where customers_id = '" . $customer_id . "'");
			  tep_db_query("delete from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . $customer_id . "'");
			  tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . $customer_id . "'");
			  tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . $customer_id . "'");
			  tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where customer_id = '" . $customer_id . "'");
		}
	}
  }
}
?>
