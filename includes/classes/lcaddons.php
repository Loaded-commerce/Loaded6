<?php
class lcaddons {
  public $addon_payment = array();
  public $addon_shipping = array();
  public $addon_ordertotal = array();

  public function __construct() {
	global $language;

  }  //end of __construct

  public function set_payment($addonkey, $addons_data) {
  	$this->addon_payment[$addonkey] = $addons_data;
  }

  public function get_payment() {
  	return $this->addon_payment;
  }

  public function set_shipping($addonkey, $addons_data) {
  	$this->addon_shipping[$addonkey] = $addons_data;
  }

  public function get_shipping() {
  	return $this->addon_shipping;
  }

  public function set_ordertotal($addonkey, $addons_data) {
  	$this->addon_ordertotal[$addonkey] = $addons_data;
  }

  public function get_ordertotal() {
  	return $this->addon_ordertotal;
  }

}
?>
