<?php
class lc_checkout {
  public function __construct() {
	global $language;

  }  //end of __construct

  //
  public function addressBookForm($address, $bill_address=0) {
	global $language, $lc_customers, $obj_catalog;
	require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);
	$country = 223;
	$str_suburb_control = '';
	$str_company_control = '';
	$control_append = ($bill_address)?'bill_':'';
	$stateDivID = ($bill_address)?'billing_state_id':'state_id';
	$onchange = ($bill_address)?'billing_change_state':'change_state';
	if (defined('ACCOUNT_COMPANY') && ACCOUNT_COMPANY == 'true'){
		$str_company_control = '<label>'.COMPANY.'</label>' . tep_draw_input_field($control_append.'company', $address['company'],'class="form-control"' );
	}
	if (defined('ACCOUNT_SUBURB') && ACCOUNT_SUBURB == 'true'){
		$str_suburb_control = '<label>'.STREET_ADDRESS2.'</label>' . tep_draw_input_field($control_append.'stree_address2', $address['suburb'],'class="form-control"' );
	}

	$addressbook_form = '<div class="col-md-6 col-lg-6 col-sm-6 col-6 col-xl-6 no-padding-left">
							<div class="">
								<label>'.FIRST_NAME.'</label>
								' . tep_draw_input_field($control_append.'firstname', $address['firstname'],'class="form-control"' ) . '<span id="redq">*</span>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 col-6 col-xl-6 no-padding-left">
							<div class="">
								<label>'.LAST_NAME.'</label>
								' . tep_draw_input_field($control_append.'lastname', $address['lastname'],'class="form-control"' ) . '<span id="redq">*</span>
							</div>
						</div>
						'.$str_company_control.'
						<label>'.STREET_ADDRESS.'</label>' . tep_draw_input_field($control_append.'stree_address', $address['street_address'],'class="form-control"' ) . '<span id="redq">*</span>
						' . $str_suburb_control .'
						<div class="col-md-6 col-lg-6 col-sm-6 col-6 col-xl-6 no-padding-left">
							<div class="">
								<label>'.CITY.'</label>
								' . tep_draw_input_field($control_append.'city', $address['city'],'class="form-control"' ) . '<span id="redq">*</span>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 col-6 col-xl-6 no-padding-right"><!--state Div-->
							<div class=""><!--state row-->
								<div id="'.$stateDivID.'" style="width:100%">'. $obj_catalog->getZonePullDown($control_append.'state', ((isset($address['country_id']) && (int)$address['country_id'] > 0) ? (int)$address['country_id'] : $country), $address['zone_id'], $address['state']) .'<span id="redq">*</span></div>
							</div><!--state row-->
						</div><!--state Div-->
						<div class="col-md-6 col-lg-6 col-sm-6 col-6 col-xl-6 no-padding-left"><!--postal code div-->
							<div class="">
								<label>'.POSTAL_CODE.'</label>
								' . tep_draw_input_field($control_append.'postalcode', $address['postcode'],'class="form-control"' ) . '<span id="redq">*</span>
							</div>
						</div><!--postal code div-->
						<div class="col-md-6 col-lg-6 col-sm-6 col-6 col-xl-6 no-padding-right"><!--country div-->
							<div class="">
								<label>'.COUNTRY.'</label>
								' . tep_get_country_list($control_append.'country' , (isset($address['country_id']) ? $address['country_id'] : $country), 'class="form-control search-js-select"  onchange="javascript:'.$onchange.'(this.value)" ') . '<span id="redq">*</span>
							</div>
						</div><!--country div-->
						<div class="col-md-6 col-lg-6 col-sm-6 col-6 col-xl-6 no-padding-left"><!--telephone code div-->
							<div class="">
								<label>'.PHONE_NUMBER.'</label>
								' . tep_draw_input_field($control_append.'telephone', $address['telephone'],'class="form-control"' ) . '<span id="redq">*</span>
							</div>
						</div><!--telephone div-->
						<div class="col-md-6 col-lg-6 col-sm-6 col-6 col-xl-6 no-padding-right"><!--telephone code div-->
							<div class="">
								<label>'.FAX.'</label>
								' . tep_draw_input_field($control_append.'fax', $address['fax'],'class="form-control"' ) . '
							</div>
						</div>';
	return $addressbook_form;

  }
  public function shippingaddressform() {
	global $language, $lc_customers, $obj_catalog;
	require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);
	$address = $lc_customers->getAddressBookById((int)$_SESSION['customer_id'], (int)$_SESSION['sendto'] );
	$country = isset($_POST['country'])?$_POST['country']:((isset($address['country_id']) && $address['country_id'] > 0)?$address['country_id']:SHIPPING_ORIGIN_COUNTRY);
	$shippingaddress = '';
	$shippingaddress .= '
					<div class="checkout-address active col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right">'.ADDRESS.'<small class="highlight"></small></div>
					<div class="checkout-shipping col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right">'.SHIPPING_METHODS.'</div>
					<div class="checkout-payment col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right">'.PAYMENT_METHODS.'</div>
					<div class="shipping-address-form col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><!--shipping-address-form-->
						' .tep_draw_form('shippingaddress', '#', 'post', 'id="shippingaddress"');
						if(isset($_SESSION['noaccount'])){
							$shippingaddress .= '<div class="row large-margin-top"> <b>'.SHIPPING_ADDRESS.'</b> </div>';
						} else {
							$existingchecked = '';
							$newchecked = '';
							if($_SESSION['adddressselecttype'] == 'existing'){
								$existingchecked = 'true';
							}
							if($_SESSION['adddressselecttype'] == 'new'){
								$newchecked = 'true';
							}
							if(isset($_SESSION['adddressselecttype']) && $_SESSION['adddressselecttype'] == 'new'){
								$parentofselectstyle = 'display:none;';
							}else{
								$parentofselectstyle = '';
							}
							$shippingaddress .= '<div class="row selectaddress">';
								$shippingaddress .= '<div class="col-lg-10 col-md-10 col-xl-10 no-padding large-margin-top choaddr"></div>';
								$shippingaddress .= '<div class="col-lg-12 col-xl-12"></div>';
								$shippingaddress .= '<div class="col-lg-8 col-md-10 col-xl-8 no-padding choaddr">';
									$shippingaddress .= '<label class="custom-radio-container checkctnr"><span id="existingaddress" class="bactive">'.USE_EXISTING_ADDRESS.'</span>' . tep_draw_radio_field('addressselect', 'existing',$existingchecked,' onClick="fetchaddressdetails(this.value);" ').'<span class="checkmark"></span></label>';
									$shippingaddress .= '<label class="custom-radio-container mid-margin-bottom checkctnr"><span id="newaddress">'.USE_NEW_ADDRESS.'</span>' . tep_draw_radio_field('addressselect', 'new',$newchecked,' onClick="fetchaddressdetails(this.value);" ').'<span class="checkmark"></span></label>';
									  $shippingaddress .= '<div class="parentofselect" style="'.$parentofselectstyle.'">';
										  $shippingaddress .= '<label><b>'.EXISTING_SHIPPING_ADDRESS.'</b></label>';
										  $shippingaddress .= '<select class="js-select choosebillingaddr" name="selectshippngaddressdp" style="width:100%">';

										  $arr_addresses = $lc_customers->getAllAddressById((int)$_SESSION['customer_id']);
										  foreach($arr_addresses as $addresses) {
											$format_id = tep_get_address_format_id($addresses['country_id']);
											$cname = tep_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']);
											$caddr = tep_address_format($format_id, $addresses, true, ' ', ', ');
											$shippingaddress .= '<option value="'.$addresses['address_book_id'].'" '.(($_SESSION['sendto'] == $addresses['address_book_id'])?'selected':'').' >'.$caddr.'</option>';
										  }
										  $shippingaddress .= '</select>';
									  $shippingaddress .= '</div>';
								$shippingaddress .= '</div>';
							$shippingaddress .= '</div>';
						}
						/*============new layout goes here============*/
						if(isset($_SESSION['noaccount'])){
							$shippingformdetailsstyle = '';
						} else {
							$shippingformdetailsstyle = 'display:none;';
							if(isset($_SESSION['adddressselecttype']) && $_SESSION['adddressselecttype'] == 'new'){
								$shippingformdetailsstyle = '';
								$shippingaddress .= '<input type="hidden" name="submitednewaddress" value="'.$_SESSION['sendto'].'">';
							}
						}

	$shippingaddress .= 	'<div class="row" id="shippingformdetails" style="'.$shippingformdetailsstyle.'">'. $this->addressBookForm($address) .'</div>
							<div class="row" id="billingformdetails" style="">
								<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><!--sames hipping billing div-->
									<div class="row">';
										if(SHIPPING_SKIP != 'Always'){
											$shippingaddress .= '<label class="isbillshipsame">'.tep_draw_checkbox_field('shipbilladdr', $address['address_book_id'], ($address['address_book_id'] == $_SESSION['billto'])?true:false, 'id="shipbilladdr" onchange="shipbilladdrChanged()"').  SHIPPING_ADDRESS_IS_BILLING_ADDRESS.'</label>';
										}else{
											$shippingaddress .= '<label style="display:none;">'.tep_draw_checkbox_field('shipbilladdr', $address['address_book_id'], ($address['address_book_id'] == $_SESSION['billto'])?true:false, 'id="shipbilladdr" onchange="shipbilladdrChanged()"') . SHIPPING_ADDRESS_IS_BILLING_ADDRESS.'</label>';
										}
			$shippingaddress .=		'</div>
								</div><!--sames hipping billing div-->
								<!--billing address-->';
							  if($_SESSION['sendto'] == $_SESSION['billto']){
								$sameAddress = 'checked';
								$displayAddress = 'display:none;';
							  }else{
								$sameAddress = '';
								$displayAddress = '';
							  }
							  if(!isset($_SESSION['noaccount'])){
			$shippingaddress .=
								'<div class="col-md-12 col-lg-8 col-sm-12 col-12 col-xl-8 mid-margin-top bill-address-select" style="margin:auto;'.$displayAddress.'">
									<div class="row">
										<select class="js-select choosebillingaddr" onchange="javascript:Fchoosebillingaddr(this.value)" name="choosebillingaddr" style="width:100%">
											<option value="">Select Existing Billing Address</option>
											<option value="addnew">Add New Billing Address</option>';

												  $arr_addresses = $lc_customers->getAllAddressById((int)$_SESSION['customer_id']);
												  foreach($arr_addresses as $addresses)
												  {
													 $format_id = tep_get_address_format_id($addresses['country_id']);
													 $cname = tep_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']);
													 $caddr = tep_address_format($format_id, $addresses, true, ' ', ', ');
													 $checkaddress = (($addresses['address_book_id'] == $_SESSION['billto']) ? 'selected':'') ;
													 $shippingaddress .= '<option value="'.$addresses['address_book_id'].'" '.$checkaddress.'>'.$caddr.'</option>';
												  }

			$shippingaddress .=			'</select>
									</div>
								</div>';
								}

			$billaddress = $lc_customers->getAddressBookById((int)$_SESSION['customer_id'], (int)$_SESSION['billto']);
			$str_suburb_control = '';
			$str_company_control = '';
			if (defined('ACCOUNT_COMPANY') && ACCOUNT_COMPANY == 'true'){
				$str_company_control = '<label>'.COMPANY.'</label>' . tep_draw_input_field('bill_company', '','class="form-control"' );
			}
			if (defined('ACCOUNT_SUBURB') && ACCOUNT_SUBURB == 'true'){
				$str_suburb_control = '<label>'.STREET_ADDRESS2.'</label>' . tep_draw_input_field('bill_stree_address2', '','class="form-control"' );
			}

			$shippingaddress .= '<div class="col-lg-12 col-xl-12 col-12 no-padding-left large-margin-top bill-address" style="display:none;">
									<b>'.BILLING_ADDRESS.'</b>
								</div>
								<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12 bill-address" style="display:none;">
									<div class="row">'. tep_draw_hidden_field('billtoid', $_SESSION['billto']) . $this->addressBookForm($address, 1) .'</div>
								</div>
								<!--billing address-->
							</div>
							<div class="row large-margin-top">
								<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><!--save continue-->
									<div class="row">
										<button type="button" name="setaddress" value="setaddress" class="btn btn-primary btn-savecontinue" onClick="return check_form(shippingaddress);">'.SET_ADDRESS_AND_CONTINUE.'</button>
										<p style="text-align:center;margin:auto"><small class="reqdfields">'.ALL_FIELDS_ARE_REQUIRED.'</small></p>
									</div>
								</div><!--save continue-->
							</div>
						</form>
					</div><!--shipping-address-form-->
						';
		$arr_data = array('data' => $shippingaddress);
		return $arr_data;
  }
  public function setshippingAddresshtml() {
		global $cart, $language, $lc_customers;
		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);
		$html = '';
		if(isset($_SESSION['sendto'])){
			$_SESSION['customer_default_address_id'] = $_SESSION['sendto'];
		}
		$html .= '
				<div class="products-cart col-md-3 col-lg-2 col-sm-2 col-2 col-xl-2 hide-on-mobile">
					<a href="javascript:" data-toggle="modal" onclick="return openeditshippingaddress('.$_SESSION['sendto'].')" class="btn btn-primary btn-sm btn-checkout">'.EDIT_INFO.'</a>
				</div>
				<div class="products-cart no-padding col-md-9 col-lg-9 col-sm-12 col-12 col-xl-9">
					<a href="javascript:" data-toggle="modal" onclick="return openeditshippingaddress('.$_SESSION['sendto'].')" class="btn btn-primary show-on-mobile btn-sm btn-checkout edit-cart-btn-mobile">'.EDIT_INFO.'</a>
					<div class="cart-shipping-section">';

					$addresses = $lc_customers->getAddressBookById((int)$_SESSION['customer_id'], (int)$_SESSION['customer_default_address_id']);
					$format_id = tep_get_address_format_id($addresses['country_id']);
					$html .= '<div class="row hide-on-mobile"><div class="col-md-3 col-lg-3 col-sm-4 col-4 col-xl-3 no-padding-right"><label> <b>'.SHIPPING_ADDRESS.'</b> </label></div><div class="col-md-9 col-lg-9 col-sm-8 col-8 col-xl-9 no-padding-left">'.tep_address_format($format_id, $addresses, true, ' ', '<br>').'</div></div>';
					$html .= '<div class="row show-on-mobile"><div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><label> <b>'.SHIPPING_ADDRESS.'</b> </label></div><div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12">'.tep_address_format($format_id, $addresses, true, ' ', '<br>').'</div></div>';
					if($_SESSION['billto'] == $_SESSION['sendto']){
						$html .= '<div class="row small-margin-top hide-on-mobile"><div class="col-md-3 col-lg-3 col-sm-3 col-3 col-xl-3 no-padding-right"><label> <b>'.BILLING_ADDRESS.'</b> </label></div><div class="col-md-9 col-lg-9 col-sm-9 col-9 col-xl-9 no-padding-left"><i>'.SAME_AS_SHIPPING.'</i></div></div>';
						$html .= '<div class="row small-margin-top show-on-mobile"><div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><label> <b>'.BILLING_ADDRESS.'</b> </label></div><div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><i>'.SAME_AS_SHIPPING.'</i></div></div>';
					}
		$html .=   '</div>
				</div>';
		$arr_data = array('data' => $html);
		return $arr_data;
  }
  public function setbillingAddresshtml() {
		global $cart, $language, $lc_customers;
		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);
		$html = '';
		if($_SESSION['billto'] != $_SESSION['sendto'] && $_SESSION['billto'] != '0')
		{
			$html .= '
					<div class="products-cart no-padding col-md-3 col-lg-2 col-sm-2 col-2 col-xl-2 hide-on-mobile" style="text-align:center">
						<a href="javascript:" data-toggle="modal" onclick="return openeditbillingaddress('.$_SESSION['billto'].')" class="btn btn-primary btn-sm btn-checkout">'.EDIT_INFO.'</a>
					</div>
					<div class="products-cart no-padding col-md-9 col-lg-9 col-sm-12 col-12 col-xl-9">
						<a href="javascript:" data-toggle="modal" onclick="return openeditbillingaddress('.$_SESSION['billto'].')" class="btn btn-primary show-on-mobile btn-sm btn-checkout edit-cart-btn-mobile">'.EDIT_INFO.'</a>
						<div class="cart-shipping-section">';

							$addresses = $lc_customers->getAddressBookById((int)$_SESSION['customer_id'], (int)$_SESSION['billto']);
							$format_id = tep_get_address_format_id($addresses['country_id']);
							$html .= '<div class="row hide-on-mobile"><div class="col-md-3 col-lg-3 col-sm-4 col-4 col-xl-3 no-padding-right"><label> <b>Billing Address</b> </label></div><div class="col-md-9 col-lg-9 col-sm-8 col-8 col-xl-9 no-padding-left">'.tep_address_format($format_id, $addresses, true, ' ', '<br>').'</div></div>';
							$html .= '<div class="row show-on-mobile"><div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><label> <b>Billing Address</b> </label></div><div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12">'.tep_address_format($format_id, $addresses, true, ' ', '<br>').'</div></div>';
			$html .=   '</div>
				</div>';
	    }
		$arr_data = array('data' => $html);
		return $arr_data;
  }
  public function addshippingaddress() {
		global $cart, $order, $language, $obj_catalog, $lc_customers, $currencies;
		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);

		$quotes = array();
		unset($_SESSION['shipping']);
		parse_str($_POST['data'],$data_arr);
		$firstname = $data_arr['firstname'];
		$lastname = $data_arr['lastname'];
		$entry_email_address = $data_arr['customers_email_address'];
		$company = $data_arr['company'];
		$stree_address = $data_arr['stree_address'];
		$stree_address2 = $data_arr['stree_address2'];
		$city = $data_arr['city'];
		$state = $data_arr['state'];
		$postalcode = $data_arr['postalcode'];
		$country = $data_arr['country'];
		$telephone = $data_arr['telephone'];
		$fax = $data_arr['fax'];
		$adddressselecttype = $data_arr['addressselect'];
		$_SESSION['adddressselecttype'] = $adddressselecttype;
		$selectshippngaddressdp = $data_arr['selectshippngaddressdp'];
		$submitednewaddress = $data_arr['submitednewaddress'];
		$isshipbilladdrsame = $data_arr['shipbilladdr'];
		$choosebillingaddressdp = $data_arr['choosebillingaddr'];
		$isnewaccountcreate = (isset($data_arr['newaccountcreate']) && $data_arr['newaccountcreate'] == 1) ? 1 : 0;
		$isactioncase = (isset($data_arr['actioncase']) && $data_arr['actioncase'] == 'edit') ? 'edit' : '';
		$addressupdatetype = $data_arr['addressupdatetype'];
		if(!isset($_SESSION['noaccount'])){
			$lc_customers->setIsComplete((int)$_SESSION['customer_id'], 1); //Set the customer as complete
			//for logged in account code here
			if($adddressselecttype == 'existing' && $selectshippngaddressdp > 0){
				$_SESSION['sendto'] = $selectshippngaddressdp;
			}elseif($adddressselecttype == 'new'){
				$zone_id = $obj_catalog->getZoneIdByName((int)$country, $state);
				$sql_data_array = array('entry_firstname' => $firstname,
									  'entry_lastname' => $lastname,
									  'entry_email_address' => $entry_email_address,
									  'entry_company' => $company,
									  'customers_id' => $_SESSION['customer_id'],
									  'entry_street_address' => $stree_address,
									  'entry_suburb' => $stree_address2,
									  'entry_postcode' => $postalcode,
									  'entry_city' => $city,
									  'entry_country_id' => $country,
									  'entry_fax' => $fax,
									  'entry_telephone' => $telephone);
				if ($zone_id > 0) {
				  $sql_data_array['entry_zone_id'] = (int)$zone_id;
				  $sql_data_array['entry_state'] = '';
				} else {
				  $sql_data_array['entry_zone_id'] = '0';
				  $sql_data_array['entry_state'] = $state;
				}
				if($isnewaccountcreate){
					$lc_customers->updateAddressBook((int)$_SESSION['customer_id'], (int)$_SESSION['sendto'], $sql_data_array);
				}else{
					if(isset($submitednewaddress) && $submitednewaddress > 0){
						$lc_customers->updateAddressBook((int)$_SESSION['customer_id'], (int)$submitednewaddress, $sql_data_array);
						$_SESSION['sendto'] = $submitednewaddress;
					}else{
						$arr_adbook = $lc_customers->addAddressBook((int)$_SESSION['customer_id'], $sql_data_array);
						$new_address_book_id = $arr_adbook['data']['address_id'];
						$_SESSION['sendto'] = $new_address_book_id;
					}
				}
			}elseif($isactioncase == 'edit'){
					$zone_id = $obj_catalog->getZoneIdByName((int)$country, $state);
					$sql_data_array = array('entry_firstname' => $firstname,
										  'entry_lastname' => $lastname,
										  'entry_email_address' => $entry_email_address,
										  'entry_company' => $company,
										  'customers_id' => $_SESSION['customer_id'],
										  'entry_street_address' => $stree_address,
										  'entry_suburb' => $stree_address2,
										  'entry_postcode' => $postalcode,
										  'entry_city' => $city,
										  'entry_country_id' => $country,
										  'entry_fax' => $fax,
										  'entry_telephone' => $telephone);
					if ($zone_id > 0) {
					  $sql_data_array['entry_zone_id'] = (int)$zone_id;
					  $sql_data_array['entry_state'] = '';
					} else {
					  $sql_data_array['entry_zone_id'] = '0';
					  $sql_data_array['entry_state'] = $state;
					}
					if($addressupdatetype == 'ship'){
						if($stree_address != '' && $city != '' && $postalcode != '' && $country != ''){
							$lc_customers->updateAddressBook((int)$_SESSION['customer_id'], (int)$_SESSION['sendto'], $sql_data_array);
						}
					}

					$bill_zone_id = $obj_catalog->getZoneIdByName((int)$data_arr['bill_country'], $bill_state);
					$bill_sql_data_array = array('customers_id' => $_SESSION['customer_id'],
										  'entry_firstname' => $data_arr['bill_firstname'],
										  'entry_lastname' => $data_arr['bill_lastname'],
										  'entry_email_address' => $entry_email_address,
										  'entry_company' => $data_arr['bill_company'],
										  'entry_street_address' => $data_arr['bill_stree_address'],
										  'entry_suburb' => $data_arr['bill_stree_address2'],
										  'entry_postcode' => $data_arr['bill_postalcode'],
										  'entry_city' => $data_arr['bill_city'],
										  'entry_country_id' => $data_arr['bill_country'],
										  'entry_fax' => $data_arr['bill_fax'],
										  'entry_telephone' => $data_arr['bill_telephone']);
					if ($bill_zone_id > 0) {
					  $bill_sql_data_array['entry_zone_id'] = (int)$bill_zone_id;
					  $bill_sql_data_array['entry_state'] = '';
					} else {
					  $bill_sql_data_array['entry_zone_id'] = '0';
					  $bill_sql_data_array['entry_state'] = $bill_state;
					}
					if($addressupdatetype == 'bill'){
						if($bill_stree_address != '' && $bill_city != '' && $bill_postalcode != '' && $bill_country != ''){
							$lc_customers->updateAddressBook((int)$_SESSION['customer_id'], (int)$_SESSION['billto'], $bill_sql_data_array);
						}
					}
			}
			//now code for billing address
			if(isset($isshipbilladdrsame) && $isshipbilladdrsame > 0){
				$choosebillingaddressdp = '';
			}
			if(isset($isshipbilladdrsame) && $isshipbilladdrsame > 0 && $choosebillingaddressdp <= 0 && $choosebillingaddressdp != 'addnew'){
				$_SESSION['billto'] = $_SESSION['sendto'];
			}elseif($choosebillingaddressdp > 0 && $choosebillingaddressdp != 'addnew'){
				$_SESSION['billto'] = $choosebillingaddressdp;
			}elseif($choosebillingaddressdp == 'addnew'){
					//insert billing address
					$bill_zone_id = $obj_catalog->getZoneIdByName((int)$data_arr['bill_country'], $bill_state);
					$bill_sql_data_array = array('customers_id' => $_SESSION['customer_id'],
										  'entry_firstname' => $data_arr['bill_firstname'],
										  'entry_lastname' => $data_arr['bill_lastname'],
										  'entry_email_address' => $entry_email_address,
										  'entry_company' => $data_arr['bill_company'],
										  'entry_street_address' => $data_arr['bill_stree_address'],
										  'entry_suburb' => $data_arr['bill_stree_address2'],
										  'entry_postcode' => $data_arr['bill_postalcode'],
										  'entry_city' => $data_arr['bill_city'],
										  'entry_country_id' => $data_arr['bill_country'],
										  'entry_fax' => $data_arr['bill_fax'],
										  'entry_telephone' => $data_arr['bill_telephone']);
					if ($bill_zone_id > 0) {
					  $bill_sql_data_array['entry_zone_id'] = (int)$bill_zone_id;
					  $bill_sql_data_array['entry_state'] = '';
					} else {
					  $bill_sql_data_array['entry_zone_id'] = '0';
					  $bill_sql_data_array['entry_state'] = $data_arr['bill_state'];
					}
					if($data_arr['bill_stree_address'] != '' && $data_arr['bill_city'] != '' && $data_arr['bill_postalcode'] != '' && $data_arr['bill_country'] != ''){

						$arr_addressbook = $lc_customers->addAddressBook((int)$_SESSION['customer_id'], $bill_sql_data_array);
						$_SESSION['billto'] = $arr_addressbook['data']['address_id'];
					}
			}else{
					//insert billing address
					$bill_firstname = $data_arr['bill_firstname'];
					$bill_lastname = $data_arr['bill_lastname'];
					$bill_company = $data_arr['bill_company'];
					$bill_stree_address = $data_arr['bill_stree_address'];
					$bill_stree_address2 = $data_arr['bill_stree_address2'];
					$bill_city = $data_arr['bill_city'];
					$bill_state = $data_arr['bill_state'];
					$bill_postalcode = $data_arr['bill_postalcode'];
					$bill_country = $data_arr['bill_country'];
					$bill_fax = $data_arr['bill_fax'];
					$bill_telephone = $data_arr['bill_telephone'];
					$bill_zone_id = $obj_catalog->getZoneIdByName((int)$data_arr['bill_country'], $bill_state);
					$bill_sql_data_array = array('customers_id' => $_SESSION['customer_id'],
										  'entry_firstname' => $bill_firstname,
										  'entry_lastname' => $bill_lastname,
										  'entry_email_address' => $entry_email_address,
										  'entry_company' => $bill_company,
										  'entry_street_address' => $bill_stree_address,
										  'entry_suburb' => $bill_stree_address2,
										  'entry_postcode' => $bill_postalcode,
										  'entry_city' => $bill_city,
										  'entry_country_id' => $bill_country,
										  'entry_fax' => $bill_fax,
										  'entry_telephone' => $bill_telephone);
					if ($bill_zone_id > 0) {
					  $bill_sql_data_array['entry_zone_id'] = (int)$bill_zone_id;
					  $bill_sql_data_array['entry_state'] = '';
					} else {
					  $bill_sql_data_array['entry_zone_id'] = '0';
					  $bill_sql_data_array['entry_state'] = $bill_state;
					}
					if($bill_stree_address != '' && $bill_city != '' && $bill_postalcode != '' && $bill_country != ''){

						$arr_addressbook = $lc_customers->addAddressBook((int)$_SESSION['customer_id'], $bill_sql_data_array);
						$_SESSION['billto'] = $arr_addressbook['data']['address_id'];
					}
			}
		} else {
			//for guest account code here
				$lc_customers->setIsComplete((int)$_SESSION['customer_id'], 1); //Set the customer as complete
			    $_SESSION['sendto'] = $_SESSION['customer_default_address_id'];
				$zone_id = $obj_catalog->getZoneIdByName((int)$country, $state);
				$sql_data_array = array('entry_firstname' => $firstname,
									  'entry_lastname' => $lastname,
									  'entry_email_address' => $entry_email_address,
									  'entry_company' => $company,
									  'customers_id' => $_SESSION['customer_id'],
									  'entry_street_address' => $stree_address,
									  'entry_suburb' => $stree_address2,
									  'entry_postcode' => $postalcode,
									  'entry_city' => $city,
									  'entry_country_id' => $country,
									  'entry_telephone' => $telephone);
				if ($zone_id > 0) {
				  $sql_data_array['entry_zone_id'] = (int)$zone_id;
				  $sql_data_array['entry_state'] = '';
				} else {
				  $sql_data_array['entry_zone_id'] = '0';
				  $sql_data_array['entry_state'] = $state;
				}
				if($stree_address != '' && $city != '' && $postalcode != '' && $country != ''){
					$lc_customers->updateAddressBook((int)$_SESSION['customer_id'], (int)$_SESSION['sendto'], $sql_data_array);
				}
				if(isset($isshipbilladdrsame) && $isshipbilladdrsame > 0){
					$_SESSION['billto'] = $isshipbilladdrsame;
				}else{
					$bill_firstname = $data_arr['bill_firstname'];
					$bill_lastname = $data_arr['bill_lastname'];
					$bill_company = $data_arr['bill_company'];
					$bill_stree_address = $data_arr['bill_stree_address'];
					$bill_stree_address2 = $data_arr['bill_stree_address2'];
					$bill_city = $data_arr['bill_city'];
					$bill_state = $data_arr['bill_state'];
					$bill_postalcode = $data_arr['bill_postalcode'];
					$bill_country = $data_arr['bill_country'];
					$bill_telephone = $data_arr['bill_telephone'];
					$bill_zone_id = $obj_catalog->getZoneIdByName((int)$bill_country, $bill_state);
					$bill_sql_data_array = array('customers_id' => $_SESSION['customer_id'],
										  'entry_firstname' => $bill_firstname,
										  'entry_lastname' => $bill_lastname,
										  'entry_email_address' => $entry_email_address,
										  'entry_company' => $bill_company,
										  'entry_street_address' => $bill_stree_address,
										  'entry_suburb' => $bill_stree_address2,
										  'entry_postcode' => $bill_postalcode,
										  'entry_city' => $bill_city,
										  'entry_country_id' => $bill_country,
										  'entry_telephone' => $bill_telephone);
					if ($bill_zone_id > 0) {
					  $bill_sql_data_array['entry_zone_id'] = (int)$bill_zone_id;
					  $bill_sql_data_array['entry_state'] = '';
					} else {
					  $bill_sql_data_array['entry_zone_id'] = '0';
					  $bill_sql_data_array['entry_state'] = $bill_state;
					}
					if($bill_stree_address != '' && $bill_city != '' && $bill_postalcode != '' && $bill_country != ''){

						$arr_addressbook = $lc_customers->addAddressBook((int)$_SESSION['customer_id'], $bill_sql_data_array);
						$_SESSION['billto'] = $arr_addressbook['data']['address_id'];

					}
				}
		}

		//shipping methods form
	    lc_check_addon_class('order.php');
	    $order = new order;

		//----------------- load all enabled shipping modules
		// MVS Start
		if (defined('MVS_STATUS') && MVS_STATUS == 'true') {
		  include(DIR_WS_CLASSES . 'vendor_shipping.php');
		} else {
		  include(DIR_WS_CLASSES . 'shipping.php');
		}
		// MVS end
		require_once(DIR_WS_CLASSES . 'http_client.php');
		$shipping_modules = new shipping;

		// MVS Start
		if (defined('MVS_STATUS') && MVS_STATUS == 'true') {
		  $quotes = $shipping_modules->quote();
		} else {
		  $free_shipping = false;
		  $free_shipping_over = '';
		  $data_array = explode(',', lc_defined_constant('MODULE_SHIPPING_FREESHIPPER_OVER'));
		  foreach ($data_array as $value) {
		    $tmp = explode('-', $value);
		    if(!isset($_SESSION['sppc_customer_group_id'])) {
		      if (is_numeric($tmp[1]) && $cart->show_total() >= $tmp[1]) {
		          $free_shipping_over = $tmp[1];
		      } else {
		        if (defined('MODULE_SHIPPING_FREESHIPPER_OVER') && is_numeric(MODULE_SHIPPING_FREESHIPPER_OVER)) {
		          $free_shipping_over = MODULE_SHIPPING_FREESHIPPER_OVER;
		        }
		      }
		      break;
		    } else {
		       if ($tmp[0] == $_SESSION['sppc_customer_group_id']) {
		        if (is_numeric($tmp[1]) && $cart->show_total() >= $tmp[1]) {
		          $free_shipping_over = $tmp[1];
		        }
		        break;
		      }
		    }
		  }

		  if (is_numeric($free_shipping_over) && $order->info['total'] >= $free_shipping_over && defined('MODULE_SHIPPING_FREESHIPPER_STATUS') && MODULE_SHIPPING_FREESHIPPER_STATUS == 'True' ) {
		    $free_shipping = true;
		    include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
		    $freeshipping_over_amount = $free_shipping_over;
		    // Check for free shipping zone
		    $chk_val = chk_free_shipping_zone(MODULE_SHIPPING_FREESHIPPER_ZONE);
		    if ($chk_val == 0) {
		      $free_shipping = false;
		    }
		  }

		  // if the order contains only virtual products, forward the customer to the billing page as
		  // a shipping address is not needed
		  if (($order->content_type == 'virtual') || ($order->content_type == 'virtual_weight') ) {
		    $shipping = false;
		    //$_SESSION['sendto'] = false;
		  } else {
		    if ( (tep_count_shipping_modules() > 0) || ($free_shipping == true) ) {
		      $shipping = true;
		    } else {
		      $shipping = false;
		      //$_SESSION['sendto'] = false;
		    }
		  }

		  //$_SESSION['shipping'] = $shipping;
		  $total_weight = $cart->show_weight();
		  // get all available shipping quotes
		  if ($shipping !== false) {
		      $quotes = $shipping_modules->quote();
		  }
			   if(!isset($_SESSION['shipping'])){
				 if (!isset($_SESSION['shipping']) || (isset($_SESSION['shipping']) && ($shipping == true) && (tep_count_shipping_modules() > 0))) {
					$cheapest = $shipping_modules->cheapest();

					if(is_array($cheapest))
						$_SESSION['shipping'] = $cheapest;
				  }
			   }


		  if($free_shipping == true) {
		    $_SESSION['shipping'] = array('id' => 'free_free',
		                                  'title' => FREE_SHIPPING_TITLE,
		                                  'cost' => (INSTALLED_VERSION_TYPE =='B2B') ? get_free_shipping_handling_cost(MODULE_SHIPPING_FREESHIPPER_COST) : MODULE_SHIPPING_FREESHIPPER_COST);
		  }
		}// MVS end
		//----------------- load all enabled shipping modules

		$shippingmrthod = '';
		$shippingmrthod .=
				'<div class="checkout-address col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right">Address</div>
				<div class="checkout-shipping active col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right">Shipping Methods <small class="highlight"></small></div>
				<div class="checkout-payment col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right">Payment Methods</div>
				<div class="shipping-method-form col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><!--shipping-address-form-->
					' .tep_draw_form('shippingmethod', '#', 'post', 'id="shippingmethod"').'
						<div class="shipping-method-list large-margin-top">';
							//----------------- load shippinh module html
								if (tep_count_shipping_modules() > 0 || lc_check_constant_val('MVS_STATUS','true')) {
								  if (lc_check_constant_val('MVS_STATUS', 'true')) {
									require(DIR_WS_MODULES . 'vendor_shipping.php');
								  } else {
									 if (($order->content_type == 'virtual') || ($order->content_type == 'virtual_weight') || SHIPPING_SKIP == 'Always' || (SHIPPING_SKIP == 'If Weight = 0' && $cart->weight == 0)) {
									  $_SESSION['shipping'] = false;
									  //$_SESSION['sendto'] = false;
									  $free_shipping = true;
									 }
									 if ($free_shipping == true) {
											$shippingmrthod .= '
													<div class="large-margin-bottom"><b>'.FREE_SHIPPING_TITLE.'</b>&nbsp;'.$quotes[$i]['icon'].'</div>
													<div class="shipping-list"><label class="custom-radio-container">'.sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format($freeshipping_over_amount)) . tep_draw_radio_field('shipping', 'free_free','true').'</label></div>
													<hr>';
									 } else {
									  $radio_buttons = 0;
									  for ($i=0, $n=sizeof($quotes); $i<$n; $i++) {
											$shippingmrthod .= '
													<div class="large-margin-bottom"><b>'.$quotes[$i]['module'].'</b>&nbsp;'.((isset($quotes[$i]['icon']) && tep_not_null($quotes[$i]['icon'])) ?  $quotes[$i]['icon']: '').'</div>';
													if (isset($quotes[$i]['error'])) {
														$shippingmrthod .= '<div class="shipping-list"><label class="custom-radio-container">'.$quotes[$i]['error'].'</label></div>';
													}else{
														for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
															// set the radio button to be checked if it is the method chosen
															$checked = (($quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'] == $_SESSION['shipping']['id']) ? true : false);
															if ( ($n > 1) || ($n2 > 1) ) {
																$shippingmrthod .= '<div class="shipping-list" id="defaultSelected" onclick="selectRowEffect(this, ' . $radio_buttons . ')"><label class="custom-radio-container"><span class="shipping_name_label"> '.$quotes[$i]['methods'][$j]['title'].'</span> <span class="shipping-price">'.$currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))).'</span> '.tep_draw_radio_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'], $checked, ' class="chooseshipping"').'<span class="checkmark"></span> </label></div>';
															} else {
																$shippingmrthod .= '<div class="shipping-list" id="defaultSelected" onclick="selectRowEffect(this, ' . $radio_buttons . ')"><label class="custom-radio-container"><span class="shipping_name_label"> '.$quotes[$i]['methods'][$j]['title'].'</span> <span class="shipping-price">'.$currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], $quotes[$i]['tax'])) . tep_draw_hidden_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id']).'</span> '.tep_draw_radio_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'], true, 'class="chooseshipping"').' <span class="checkmark"></span> </label></div>';
															}
															$radio_buttons++;
														}
													}
											$shippingmrthod .=	'<hr>';
									  }
									}
								  }
								}
								if(sizeof($quotes) <= 0 && $free_shipping != true){
										$shippingmrthod .= '
												<div class="large-margin-bottom"><b>'.NO_SHPPING_METHOD.'</b>&nbsp;'.$quotes[$i]['icon'].'</div>
												<div class="shipping-list"><label class="custom-radio-container">'. NO_SHIPPING_METHOD_REQUIRED . tep_draw_radio_field('shipping', 'free_free','true').'</label></div>
												<hr>';
								}
							//----------------- load shippinh module html
	$shippingmrthod .=	'</div>
						<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12 large-margin-top"><!--save continue-->
							<div class="row">
								<button type="button" name="setshipping" value="setshipping" class="btn btn-primary btn-savecontinue" onClick="return setshippingmethod(shippingmethod);">Set Shipping Method And Continue</button>
								<button type="button" name="backtoaddress" value="backtoaddress" class="btn btn-default btn-grey" onClick="return backtoaddressform();">Back to Address</button>
							</div>
						</div><!--save continue-->
					</form>
				</div>';
		$arr_data = array('data' => $shippingmrthod);
		return $arr_data;
  }
  public function setshippingmethod() {
		global $cart, $order, $order, $language, $lc_customers, $cre_RCO, $cre_RCI;
		//set the shipping method here and proceed to payment form
		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);
		$shippingmethod = $_POST['data'];
	        lc_check_addon_class('order.php');
	        $order = new order;
		lc_check_addon_class('order_total.php');
		$order_total_modules = new order_total;
		$order_total_modules->clear_posts();
		$order_total_modules->collect_posts();
		$order_total_modules->pre_confirmation_check();
		$order_total_modules->process();

		require_once(DIR_WS_CLASSES . 'payment.php');
		$payment_modules = new payment;
		$paymentmrthod = '';
		$paymentmrthod .=
			   '<div class="checkout-address col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right">Address</div>
				<div class="checkout-shipping col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right">Shipping Methods</div>
				<div class="checkout-payment active col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right">Payment Methods <small class="highlight"></small></div>
				<div class="payment-method-form col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><!--payment-method-form-->
					' . tep_draw_form('checkout_payment', tep_href_link(FILENAME_CHECKOUT_PROCESS, '', 'SSL'), 'post', 'novalidate="novalidate" class="needs-validation" id="checkout_payment" onsubmit="return pfvalidate();"') . tep_draw_hidden_field('cot_gv', '0') .'
						<div class="payment-method-list large-margin-top">';

						if( $order->info['total'] > 0 ){
						  if (isset($_SESSION['token']) && substr($_SESSION['token'], 0, 2) == 'EC') $_SESSION['skip_payment'] == '1';
						  if (!defined('MODULE_PAYMENT_PAYPAL_WPP_STATUS') || MODULE_PAYMENT_PAYPAL_WPP_STATUS != 'True' || $_SESSION['skip_payment'] != '1') {
							// RCO start
							if ($cre_RCO->get('ordercheckout', 'paymentmodule') !== true) {
								$selection = $payment_modules->selection();
								$radio_buttons = 0;
								$selecte_pm = '';
								if(sizeof($selection) == 1){
								$selecte_pm = $selection[0]['id'];
								}
								$paymentmrthod .= '<input type="hidden" name="number_of_pmethod"  id="number_of_pmethod" value="'.sizeof($selection).'"/>';
								for ($i=0, $n=sizeof($selection); $i<$n; $i++) {
									$need_checkbox = 0;
									if (sizeof($selection) > 1) {
									    $need_checkbox = 1;
										$paymentradio = tep_draw_radio_field('payment', $selection[$i]['id'],'','onclick="javascript:thispaymentmethod(this.value);"');
									} else {
										$paymentradio = tep_draw_radio_field('payment', $selection[$i]['id'],'true');
									}

									$paymentmrthod .= '<label class="custom-radio-container"> <div style="padding-right:20px;display:inline;"><b>'.$selection[$i]['module'].'</div></b> '.$paymentradio.(($need_checkbox > 0)?' <span class="checkmark"></span>':'').' </label>';
											if (isset($selection[$i]['error'])) {
												$paymentmrthod .= '<div class="small-margin-top">'.$selection[$i]['error'].'</div>';
											} else{
												if(isset($selection[0]['fields']) && is_array($selection[0]['fields'])) {
													for ($j=0, $n2=sizeof($selection[$i]['fields']); $j<$n2; $j++) {
														 if($selection[$i]['fields'][$j]['title'] == 'CUSTOMCC'){
															include(DIR_FS_CATALOG . DIR_WS_MODULES . 'payment/additional_cc/additional_cc.php');
														 }elseif($selection[$i]['fields'][$j]['title'] == 'PSMC'){
															 $paymentmrthod .= '<div id="paypal-buttons"></div>';
														 }elseif($selection[$i]['fields'][$j]['title'] == 'HIDDEN'){
															 $paymentmrthod .= $selection[$i]['fields'][$j]['field'];
														 }else{
														 	if($selection[$i]['fields'][$j]['title'] != ''){
																$paymentmrthod .= '<label>'.$selection[$i]['fields'][$j]['title'].'</label>';
															}
															$paymentmrthod .= $selection[$i]['fields'][$j]['field'];
														  }
													}
												}
											}

									$paymentmrthod .= '<hr>';
									$radio_buttons++;
								}
							  }
							  // RCO end
							  } else {
								$paymentmrthod .= tep_draw_hidden_field('payment', 'paypal_wpp');
							  }

						  } else {
							$paymentmrthod .= '<div class="payment-method-list large-margin-top"><div class="small-margin-top"><label>'.TEXT_ORDER_TOTAL_ZERO.'</label></div>';
							$paymentmrthod .= tep_draw_hidden_field('payment', 'freecharger');
						}
							$paymentmrthod .= '<div id="div_order_total_payment_informayion"></div>';
							$paymentmrthod .= tep_draw_hidden_field('shipping', $shippingmethod);

						  // Points/Rewards Module V2.00 Redeemption box bof
						  if (lc_check_constant_val('MODULE_ADDONS_POINTS_STATUS', 'True')) {
							$orders_total = tep_count_customer_orders();

							$paymentmrthod .= points_selection();

							if (tep_not_null(USE_REFERRAL_SYSTEM)) {
							  if ($orders_total < 1) {
								$paymentmrthod .= referral_input();
							  }
							}
						  }
						  // Points/Rewards Module V2.00 Redeemption box eof
		$paymentmrthod .='</div>
						<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12 no-padding large-margin-top large-margin-bottom"><!--save continue-->
							<textarea class="form-control" name="comments" placeholder="Add your Order Notes"></textarea>
						</div>
						<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12 large-margin-top">';
						if (ACCOUNT_CONDITIONS_REQUIRED == 'true') {
							$paymentmrthod .='<p style="font-size:13px"><input type="checkbox" value="0" name="agree" id="agree"> &nbsp;' . CHECKOUT_TERM_CONDITION . '</p>';
						}
		  			 $paymentmrthod .='<div class="row">
		  			 		      <div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12">
		  			 		        <div id="dynamic_process_btn" style="width:100%"></div>
								<button type="submit" id="process_payment" name="setshipping" value="setshipping" class="btn btn-primary btn-savecontinue" onClick="return confirmpayment();">Confirm order and process payment</button>
								<button type="button" name="backtoaddress" value="backtoaddress" class="btn btn-default btn-grey" onClick="return backtoshipping();">Back to shipping method</button>
							      </div>
							</div>
						</div><!--save continue-->
					</form>
				</div>';
		$arr_data = array('data' => $paymentmrthod, 'number_of_pm' => sizeof($selection), 'single_payment_method' => $selecte_pm);
		return $arr_data;
  }
  public function getshippingestimate() {
		global $cart, $order, $language, $lc_customers, $currencies, $obj_catalog, $shipping_weight;

		$extra = '';
		$ShipTxt = '';
		$modules_icon = '';
		$modules_error = '';
		$modules_tax = 0;

		if (!isset($_SESSION['customer_default_address_id'])) {
		  $_SESSION['customer_default_address_id'] = 0;
		}
		// Only do when something is in the cart
		if ($cart->count_contents() > 0) { //print_r($_POST);exit;
		// Could be placed in english.php
		// shopping cart quotes

		  // shipping cost
		 require('includes/classes/http_client.php'); // shipping in basket
		 require(DIR_WS_LANGUAGES . $language . '/modules/shipping_estimator.php');

		  $valid_address_id = 0;
		  $valid_country_id = 0;
		  if (isset($_POST['zip_code'])) {
			 $zip_code = validate_zip_code($_POST['zip_code']) ? $_POST['zip_code'] : SHIPPING_ORIGIN_ZIP ;
		  }

		  if($cart->get_content_type() !== 'virtual') {
			if ( isset($_SESSION['customer_id']) ) {
			  // user is logged in
			  if (isset($_POST['address_id']) && (int)$_POST['address_id'] > 0) {
				$valid_address_id = $lc_customers->checkValidAddress((int)$_SESSION['customer_id'], (int)$_POST['address_id']);
			  }

			  if ( $valid_address_id == 1){
				// user changed address
				$_SESSION['sendto'] = $_POST['address_id'];
			  }elseif ( isset($_SESSION['cart_address_id']) ){
				// user once changed address
				$_SESSION['sendto'] = $_SESSION['cart_address_id'];
			  }else{
				// first timer
				$_SESSION['sendto'] = $_SESSION['customer_default_address_id'];
			  }
			  // set session now
			  $_SESSION['cart_address_id'] = $_SESSION['sendto'];
			  // include the order class (uses the sendto !)
			  lc_check_addon_class('order.php');
			  $order = new order;
			}else{
			  // user not logged in !
			  if (isset($_POST['country_id']) && (int)$_POST['country_id'] > 0) {
				$valid_country_id = $obj_catalog->checkValidCountry((int)$_POST['country_id']);
			  }
			  if ($valid_country_id == 1) {
				// country is selected
				$country_info = tep_get_countries($_POST['country_id'],true);
				$order->delivery = array('postcode' => $zip_code,
										 'country' => array('id' => $_POST['country_id'], 'title' => $country_info['countries_name'], 'iso_code_2' => $country_info['countries_iso_code_2'], 'iso_code_3' =>  $country_info['countries_iso_code_3']),
										 'country_id' => $_POST['country_id'],
										 'format_id' => tep_get_address_format_id($_POST['country_id']));
				$_SESSION['cart_country_id'] = $_POST['country_id'];
				$_SESSION['cart_zip_code'] = $zip_code;
			  }elseif ( isset($_SESSION['cart_country_id']) ){
				// session is available
				$country_info = tep_get_countries($_SESSION['cart_country_id'], true);
				$order->delivery = array('postcode' => $_SESSION['cart_zip_code'],
										 'country' => array('id' => $_SESSION['cart_country_id'], 'title' => $country_info['countries_name'], 'iso_code_2' => $country_info['countries_iso_code_2'], 'iso_code_3' =>  $country_info['countries_iso_code_3']),
										 'country_id' => $_SESSION['cart_country_id'],
										 'format_id' => tep_get_address_format_id($_SESSION['cart_country_id']));
			  } else {
				// first timer
				$_SESSION['cart_country_id'] = STORE_COUNTRY;
				$_SESSION['cart_zip_code'] = SHIPPING_ORIGIN_ZIP;

				$country_info = tep_get_countries(STORE_COUNTRY,true);
				$order->delivery = array('postcode' => SHIPPING_ORIGIN_ZIP,
										 'country' => array('id' => STORE_COUNTRY, 'title' => $country_info['countries_name'], 'iso_code_2' => $country_info['countries_iso_code_2'], 'iso_code_3' =>  $country_info['countries_iso_code_3']),
										 'country_id' => STORE_COUNTRY,
										 'format_id' => ((isset($order)) && is_object($order) && isset($order->delivery['country_id']) ?  tep_get_address_format_id($order->delivery['country_id']) : 0));
			  }

			   $zipInfo = json_decode(get_zip_infos($zip_code));
				//echo $zipInfo->state;exit;
				 $temp_city	=	$zipInfo->city;
				 $temp_state	= $zipInfo->state;

				$new_array = array('city' => $temp_city,'state' => $temp_state);
				$order->delivery = array_merge($new_array,$order->delivery);

			  // set the cost to be able to calvculate free shipping
			  $order->info = array('total' => $cart->show_total()); // TAX ????
			}
			// weight and count needed for shipping !
			$total_weight = $cart->show_weight();
			$total_count = $cart->count_contents();
			//require(DIR_WS_CLASSES . 'shipping.php');
			if (defined('MVS_STATUS') && MVS_STATUS == 'true') {
			  include(DIR_WS_CLASSES . 'vendor_shipping.php');
			} else {
			  include(DIR_WS_CLASSES . 'shipping.php');
			}
			$shipping_modules = new shipping;
			$quotes = $shipping_modules->quote();
			$cheapest = $shipping_modules->cheapest();
			$selected_country = $order->delivery['country']['id'];
			$selected_zip = $order->delivery['postcode'];
			if (isset($_SESSION['sendto'])){
			$selected_address = $_SESSION['sendto'];
			}
		  }
			// eo shipping cost

		if (!isset($pass)) {
		  $pass = false;
		}

		$free_shipping = false;
		$free_shipping_over = '';
		$data_array = explode(',', MODULE_SHIPPING_FREESHIPPER_OVER);
		foreach ($data_array as $value) {
		  $tmp = explode('-', $value);
		  if(!isset($_SESSION['sppc_customer_group_id'])) {
			if (is_numeric($tmp[1]) && $cart->show_total() >= $tmp[1]) {
				$free_shipping_over = $tmp[1];
			}
			break;
		  } else {
			 if ($tmp[0] == $_SESSION['sppc_customer_group_id']) {
			  if (is_numeric($tmp[1]) && $cart->show_total() >= $tmp[1]) {
				$free_shipping_over = $tmp[1];
			  }
			  break;
			}
		  }
		}
		if ( defined('MODULE_SHIPPING_FREESHIPPER_STATUS') && (MODULE_SHIPPING_FREESHIPPER_STATUS == 'True') ) {
		  if (is_numeric($free_shipping_over) && $cart->show_total() >= $free_shipping_over) {
			$free_shipping = true;
		  }
		}
		// check free shipping based on order $total

		  if ( $free_shipping == true) {
			if ($order->info['total'] >= $free_shipping_over ) {
			  $free_shipping = true;
			  include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
			  $freeshipping_over_amount = $free_shipping_over;
			  // Check for free shipping zone
			  $chk_val = chk_free_shipping_zone(MODULE_SHIPPING_FREESHIPPER_ZONE);
			  if ($chk_val == 0) {
				$free_shipping = false;
			  }
			}
		  }
		// end free shipping based on order total
		  if(sizeof($quotes)) {
			if ( isset($_SESSION['customer_id']) ) {
			  // logged in
			  $ShipTxt.= '<div class="calculate-shipping-box">';

			  $arr_addresses = $lc_customers->getAllAddressById((int)$_SESSION['customer_id']);
			  foreach($arr_addresses as $addresses) {
				$addresses_array[] = array('id' => $addresses['address_book_id'], 'text' => tep_address_format(tep_get_address_format_id($addresses['country_id']), $addresses, 0, ' ', ' '));
			  }
			  $ShipTxt.= tep_draw_hidden_field('address_id', $selected_address, 'size="10" class="form-control"');
			  $ShipTxt.='<span><b>Estimated Shipping Cost:</b></span>'.ENTRY_POST_CODE .'&nbsp;&nbsp;'. tep_draw_input_field('zip_code', $addresses['postcode'], 'size="10" class="form-control zip_code_cart"');
			  $ShipTxt.= tep_get_country_list('country_id', $selected_country,'class="form-control country_id_cart search-js-select"');
			  $ShipTxt.='&nbsp;<i class="fa fa-repeat" aria-hidden="true" onclick="javascript:calculate_shipping_estimate()" style="font-size:22px;font-weight:600;color:#666666;text-align: right;float: right;margin-right: 15px;margin-top: 3px;cursor:pointer;"></i><div style="clear:both;"></div>';
			  $ShipTxt.= '</div>';
			} else {
			  if (isset($quotes[$i]['icon'])){
				$modules_icon = $quotes[$i]['icon'];
			  }
			  if (isset($quotes[$i]['error'])){
				$modules_tax = $quotes[$i]['error'];
			  }
			  if (isset($quotes[$i]['tax'])){
				$modules_tax = $quotes[$i]['tax'];
			  }

			  // not logged in
			  $ShipTxt.= '<div class="calculate-shipping-box">';
			  $ShipTxt.='<span><b>Estimated Shipping Cost:</b></span>'.ENTRY_POST_CODE .'&nbsp;&nbsp;'. tep_draw_input_field('zip_code', $zip_code, 'size="10" class="form-control zip_code_cart"');
			  $ShipTxt.= tep_get_country_list('country_id', $selected_country,' class="form-control country_id_cart search-js-select"');
			  $ShipTxt.='&nbsp;<i class="fa fa-repeat" aria-hidden="true" onclick="javascript:calculate_shipping_estimate()" style="font-size:22px;font-weight:600;color:#666666;text-align: right;float: right;margin-right: 15px;margin-top: 3px;cursor:pointer;"></i><div style="clear:both;"></div>';
			  $ShipTxt.= '</div>';
			}
			if (defined('MVS_STATUS') && MVS_STATUS == 'true') {
			  $vendor_shipping = $cart->vendor_shipping();
			  //Display a notice if we are shipping by multiple methods
			  if (count ($vendor_shipping) > 1) {
				$s_shipping = '';
				//Draw a selection box for each shipping_method
				foreach ($vendor_shipping as $vendor_id => $vendor_data) {
				  $total_weight = $vendor_data['weight'];
				  $shipping_weight = $total_weight;
				  $cost = $vendor_data['cost'];
				  $ship_tax = $shipping_tax;   //for taxes
				  $total_count = $vendor_data['qty'];
				  //  Much of the code from the top of the main page has been moved here, since
				  //    it has to be executed for each vendor
				  if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
					$pass = false;
					switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
					  case 'national':
						if ($order->delivery['country_id'] == STORE_COUNTRY) {
						  $pass = true;
						}
					  break;
					  case 'international':
						if ($order->delivery['country_id'] != STORE_COUNTRY) {
						  $pass = true;
						}
					  break;
					  case 'both':
						$pass = true;
					  break;
					}
					$free_shipping = false;
					if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
					  $free_shipping = true;

					  include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
					}
				  } else {
					$free_shipping = false;
				  }

				  //Get the quotes array
				  $quotes = $shipping_modules->quote('', '', $vendor_id);
				  $_SESSION['shipping'] = $shipping_modules->cheapest($vendor_id);
				  $products_ids = $vendor_data['products_id'];

				  if ($s_shipping != '') {
					$s_shipping .= '&';
				  }



				  if ($free_shipping == true) {
					// order $total is free
					$ShipTxt.= sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) ;
				  } else {
				   $ShipTxt.= '<div class="show-calculated-shipping" id="show-calculated-shipping"><div class="row">
							  <div class="col-lg-3 col-xl-3 col-md-3 col-12 col-sm-12"></div>
							 <div class="col-lg-9 col-xl-9 col-md-9 col-12 col-sm-12 text-left shipping-estimateresult"><span>As low as: </span> &nbsp;';
								for ($i=0, $n=count($quotes); $i<$n; $i++) {
								  for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
									// set the radio button to be checked if it is the method chosen
									$checked = (($quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'] == $_SESSION['shipping']['id']) ? true : false);
									$shipping_actual_tax = $quotes[$i]['tax'] / 100;
									$shipping_tax = $shipping_actual_tax * $quotes[$i]['methods'][$j]['cost'];

									$ShipTxt.='<p style="margin: 0px;"><b>' . $quotes[$i]['module']."</b><b>(".$quotes[$i]['methods'][$j]['title'] . ')</b><b style="margin-left:5px;">' . $currencies->format($quotes[$i]['methods'][$j]['cost']) . '</b></p>';
								  }
								}
					$ShipTxt.= '</div></div>';
				  }

				}
			  } else {
				$s_shipping = '';
				//Draw a selection box for each shipping_method
				foreach ($vendor_shipping as $vendor_id => $vendor_data) {
				  $total_weight = $vendor_data['weight'];
				  $shipping_weight = $total_weight;
				  $cost = $vendor_data['cost'];
				  $ship_tax = $shipping_tax;   //for taxes
				  $total_count = $vendor_data['qty'];
				  //  Much of the code from the top of the main page has been moved here, since
				  //    it has to be executed for each vendor
				  if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
					$pass = false;

					switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
					  case 'national':
						if ($order->delivery['country_id'] == STORE_COUNTRY) {
						  $pass = true;
						}
					  break;
					  case 'international':
						if ($order->delivery['country_id'] != STORE_COUNTRY) {
						  $pass = true;
						}
					  break;
					  case 'both':
						$pass = true;
						break;
					}

					$free_shipping = false;
					if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
					  $free_shipping = true;
					  include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
					}
				  } else {
					$free_shipping = false;
				  }
				  $quotes = $shipping_modules->quote('', '', $vendor_id);
				  $_SESSION['shipping'] = $shipping_modules->cheapest($vendor_id);
				  $products_ids = $vendor_data['products_id'];

				  if ($s_shipping != '') {
					$s_shipping .= '&';
				  }


				  if ($free_shipping == true) {
					$ShipTxt.= sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) ;
				  } else {
				   $ShipTxt.= '<div class="show-calculated-shipping" id="show-calculated-shipping"><div class="row">
							  <div class="col-lg-3 col-xl-3 col-md-3 col-12 col-sm-12"></div>
							 <div class="col-lg-9 col-xl-9 col-md-9 col-12 col-sm-12 text-left shipping-estimateresult"><span>As low as: </span> &nbsp;';
					for ($i=0, $n=count($quotes); $i<$n; $i++) {
					  for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
						$shipping_actual_tax = $quotes[$i]['tax'] / 100;
						$shipping_tax = $shipping_actual_tax * $quotes[$i]['methods'][$j]['cost'];
						$ShipTxt.='<p style="margin: 0px;"><b>' . $quotes[$i]['module']."</b>(".$quotes[$i]['methods'][$j]['title'] . ')<b>' . $currencies->format($quotes[$i]['methods'][$j]['cost']) . '</b></p>';
					  }
					}
					$ShipTxt.= '</div></div>';
				  }
				}
			  }
		  } else {
			if ($free_shipping==1) {
			  // order $total is free
			  $ShipTxt.= sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format($freeshipping_over_amount)) ;

			}else{
			  // shipping display
			  for ($i=0, $n=sizeof($quotes); $i<$n; $i++) {
				if ( ! isset($quotes[$i]['methods'])) continue;
				if (sizeof($quotes[$i]['methods'])==1) {
				  // simple shipping method
				  $thisquoteid = $quotes[$i]['id'].'_'.$quotes[$i]['methods'][0]['id'];
				  $ShipTxt.=$modules_icon.'&nbsp;';
				  if(isset($quotes[$i]['error'])){
					$ShipTxt.= $quotes[$i]['module'].'&nbsp;';
					$ShipTxt.= '('.$quotes[$i]['error'].')';
				  }else{
				   $ShipTxt.= '<div class="show-calculated-shipping" id="show-calculated-shipping"><div class="row">
							  <div class="col-lg-3 col-xl-3 col-md-3 col-12 col-sm-12"></div>
							 <div class="col-lg-9 col-xl-9 col-md-9 col-12 col-sm-12 text-left shipping-estimateresult"><span>As low as: </span> &nbsp;';
					if($cheapest['id'] == $thisquoteid){
					  $ShipTxt.='<p style="margin: 0px;"><b>'.$quotes[$i]['module'].'&nbsp;';
					  $ShipTxt.= '('.$quotes[$i]['methods'][0]['title'].')</b><b style="margin-left:5px;">'.$currencies->format(tep_add_tax($quotes[$i]['methods'][0]['cost'], $modules_tax)).'<b></p>';
					}else{
					  $ShipTxt.='<p style="margin: 0px;">'.$quotes[$i]['module'].'&nbsp;';
					  $ShipTxt.= '('.$quotes[$i]['methods'][0]['title'].')'.$currencies->format(tep_add_tax($quotes[$i]['methods'][0]['cost'], $modules_tax)).'</p>';
					}
				   $ShipTxt.= '</div></div>';
				  }
				} else {
				  // shipping method with sub methods (multipickup)
				   $ShipTxt.= '<div class="show-calculated-shipping" id="show-calculated-shipping"><div class="row">
							  <div class="col-lg-3 col-xl-3 col-md-3 col-12 col-sm-12"></div>
							 <div class="col-lg-9 col-xl-9 col-md-9 col-12 col-sm-12 text-left shipping-estimateresult"> <span>As low as: </span> &nbsp;';
				  for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
					$thisquoteid = $quotes[$i]['id'].'_'.$quotes[$i]['methods'][$j]['id'];
					$ShipTxt.='<p style="margin: 0px;">'.(tep_not_null($quotes[$i]['icon']) ? $quotes[$i]['icon'] : '').'&nbsp;';
					if($quotes[$i]['error']){
					  $ShipTxt.=$quotes[$i]['module'].'&nbsp;';
					  $ShipTxt.= '('.$modules_error.')';
					}else{
					  if($cheapest['id'] == $thisquoteid){
						$ShipTxt.='<b>'.$quotes[$i]['module'].'&nbsp;';
						$ShipTxt.= '('.$quotes[$i]['methods'][$j]['title'].')</b><b style="margin-left:5px;">'.$currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], $modules_tax)).'</b></p>';
					  }else{
						$ShipTxt.=$quotes[$i]['module'].'&nbsp;';
						$ShipTxt.= '('.$quotes[$i]['methods'][$j]['title'].')'.$currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], $modules_tax)).'</p>';
					  }
					}
				  }
				  $ShipTxt.= '</div></div>';
				}
			  }
			}
		  }

		  } else {
			// virtual product/download
			$ShipTxt.= SHIPPING_METHOD_FREE_TEXT . ' ' . SHIPPING_METHOD_ALL_DOWNLOADS ;
		  }

		}
		$arr_data = array('data' => $ShipTxt);
		return $arr_data;
  }
  public function updateaccounfinormation() {
		global $cart, $language, $lc_customers;

  		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);
		parse_str($_POST['data'],$data_arr);
		extract($data_arr);

		$sql_data_array = array('customers_firstname' => $firstname, 'customers_lastname' => $lastname, 'customers_email_address' => $emailaddress );
		if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
		if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($dob);
		$lc_customers->updateCustomer((int)$_SESSION['customer_id'], $sql_data_array);

		$sql_data_array = array('entry_firstname' => $firstname, 'entry_lastname' => $lastname, 'entry_telephone' => $phonenumber, 'entry_fax' => $fax, 'entry_company' => $company, 'entry_company_tax_id' => $taxid );
		$lc_customers->updateAddressBook((int)$_SESSION['customer_id'], (int)$_SESSION['customer_default_address_id'], $sql_data_array);

		$_SESSION['customer_first_name'] = $firstname;

		$infohtml = '';
		$account = $lc_customers->getCustomerById((int)$_SESSION['customer_id']);
		$infohtml = $lc_customers->showCustomerInfoBrief($account);
		$is_complete = $account['is_complete'];
		$arr_data = array('data' => $infohtml);
		return $arr_data;
  }
  public function editshippingaddresshtml() {
		global $cart, $language, $lc_customers, $obj_catalog;
  		require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);
  		if(!isset($_SESSION['sendto'])){
			$_SESSION['sendto'] = $_SESSION['customer_default_address_id'];
		}

		$address = $lc_customers->getAddressBookById((int)$_SESSION['customer_id'], (int)$_SESSION['sendto']);
		$country = isset($_POST['country'])?cleanString($_POST['country']):((isset($address['country_id']) && $address['country_id'] > 0)?$address['country_id']:SHIPPING_ORIGIN_COUNTRY);
  		$shippingaddress = '';
  		$shippingaddress .= '<div class="modaleditaddress">';
		$shippingaddress .= 	'<div class="row" id="shippingformdetails">'. tep_draw_hidden_field('actioncase', 'edit','class="form-control"' ) . tep_draw_hidden_field('addressupdatetype', 'ship','class="form-control"' ). $this->addressBookForm($address) .'</div> ';
		$shippingaddress .= '</div>';

		$arr_data = array('data' => $shippingaddress);
		return $arr_data;
  }
  public function editbillingaddresshtml() {
	global $language, $lc_customers, $obj_catalog;
  	require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT);
	$address = $lc_customers->getAddressBookById((int)$_SESSION['customer_id'], (int)$_SESSION['billto']);
	$country = isset($_POST['country'])?cleanString($_POST['country']):((isset($address['country_id']) && (int)$address['country_id'] > 0)? (int) $address['country_id']:SHIPPING_ORIGIN_COUNTRY);
   	$shippingaddress = '';
  	$shippingaddress .= '<div class="modaleditaddress">
							<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12 bill-address">
								<div class="row">'. tep_draw_hidden_field('actioncase', 'edit','class="form-control"' ) .  tep_draw_hidden_field('addressupdatetype', 'bill','class="form-control"' ) . $this->addressBookForm($address, 1) .'</div>
							</div>
							<!--billing address-->
						</div></div>';

		$arr_data = array('data' => $shippingaddress);
		return $arr_data;
  }
  public function checkemailexists() {
		global $obj_catalog;
		$customerExist = $obj_catalog->getCustomerExistByEmail(tep_db_input($_POST['emailadd']));
		if($customerExist)
			$arr_data = array('data' => 'exist');
		else
			$arr_data = array('data' => 'not exist');

		$arr_data = array('data' => $shippingaddress);
		return $arr_data;
  }

}
?>