<?php
class productInfo {
  public $products_data = array();

  public function __construct($products_id=0) {
    global $languages_id, $obj_catalog;

	$append_query = $obj_catalog->get_append_query();
	if(defined('DISPLAY_OUT_OF_STOCK_PRODUCT') && DISPLAY_OUT_OF_STOCK_PRODUCT == 'true') {
	  $append_query = str_replace("and pd.products_quantity > 0", "", $append_query);
	}
	if(!isset($_GET['u']) && !isset($_GET['amp;u'])){
	  $query = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd WHERE p.products_id = pd.products_id and pd.language_id = " . (int)$languages_id . " AND p.products_id=$products_id AND p.products_status = '1' ". $append_query ." ORDER BY pd.products_name");
	}else{
	  $query = tep_db_query("SELECT * FROM " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd WHERE p.products_id = pd.products_id and pd.language_id = " . (int)$languages_id . " AND p.products_id=$products_id ");
	}

    $data = tep_db_fetch_array($query);
    $this->product_info = $data;
	$updateattrib_price_js= 'function updateConfigPrice()
	{
		params = $(\'#cart_quantity\').serializeArray();
		$.ajax({

			type: \'post\',
			url: \'ajax_handler.php?action=config-price&rand=\'+Math.random(),
			data: params,
			success: function(retVal){
				//alert(retVal);
				$(\'#config-price\').html(retVal);
			}

		})
	}
	$(document).ready(function() {
		updateConfigPrice();
	});';

	$obj_catalog->set_js($updateattrib_price_js);
	$this->product_info['page_js']['updateattrib_price_js'] = $updateattrib_price_js;
  }  //end of __construct

  public function product_get_raw_data() {
		return $this->product_info;
  }
  public function set_product_tabs($tabdata) {
  	$this->product_tab[] = $tabdata;
  }
  public function product_tabs() {
		global $obj_catalog, $arr_addons_pagedata, $languages_id, $divcolumn, $column_list;
		$str_description = '';
		if($this->product_info['products_blurb'] != "")
			if(DISPLAY_SHORT_DESCRIPTION == 'true'){
				$str_description .= '<p>' .  cre_clean_product_description($this->product_info['products_blurb']) . '</p>';
			}

		if($this->product_info['products_description'] != "")
			$str_description .= '<p>' .  cre_clean_product_description($this->product_info['products_description']) . '</p>';
		if (tep_not_null($this->product_info['products_url'])) {
			$str_description .= '<br>' . sprintf(TEXT_MORE_INFORMATION, tep_href_link(FILENAME_REDIRECT, 'action=url&amp;goto=' . urlencode($this->product_info['products_url']), 'NONSSL', true, false)) . '</br>';
		}
		if(defined('DISPLAY_SPEC_FIELDS_IN_DESCRIPTION_TAB') && DISPLAY_SPEC_FIELDS_IN_DESCRIPTION_TAB == 'true') {
				$str_description .= '<table cellpadding="5" cellspacing="5" border="0" width="100%" class="table table-striped extra-fields-tab">';
				//Extra field Information will Display here
				$extra_fields = tep_db_query("SELECT pef.* , p2pef.* FROM `products_extra_fields` pef INNER JOIN `products_to_products_extra_fields` p2pef ON pef.`products_extra_fields_id`
											= p2pef.`products_extra_fields_id` WHERE products_id = '".$this->product_info['products_id']."' AND products_extra_fields_search_status = 1 ");
				if(tep_db_num_rows($extra_fields) > 0){
					while($row = tep_db_fetch_array($extra_fields)){
						$str_description .= '<tr><td class="extra-fields-tab-td"><b>'.$row['products_extra_fields_label'].' </b></td><td>' . $row['products_extra_fields_value'] . '</td></tr>';
					}
				}
				$str_description .= '</table>';
				//Extra field Information will Display here
	    }
			$arr_tabs[] = array('heading'=>TEXT_TAB_PRODUCT_INFO, 'tag'=>'tab-description', 'active'=>1, 'content'=>$str_description);

		//Display specs fileds specs
		if(defined('DISPLAY_SPECS_FILEDS_AS_TAB') && DISPLAY_SPECS_FILEDS_AS_TAB == '1')
		{
				$specs_description = '';
				$specs_description .= '<table cellpadding="5" cellspacing="5" border="0" width="100%" class="table table-striped extra-fields-tab">';
				//Extra field Information will Display here
				$extra_fields = tep_db_query("SELECT pef.* , p2pef.* FROM `products_extra_fields` pef INNER JOIN `products_to_products_extra_fields` p2pef ON pef.`products_extra_fields_id`
											= p2pef.`products_extra_fields_id` WHERE products_id = '".$this->product_info['products_id']."' AND products_extra_fields_search_status = 1 ");
				if(tep_db_num_rows($extra_fields) > 0){
					while($row = tep_db_fetch_array($extra_fields)){
						$specs_description .= '<tr><td class="extra-fields-tab-td"><b>'.$row['products_extra_fields_label'].' </b></td><td>' . $row['products_extra_fields_value'] . '</td></tr>';
					}
				}
				$specs_description .= '</table>';
				//Extra field Information will Display here
			$arr_tabs[] = array('heading'=>'Specs', 'tag'=>'specs-tab', 'active'=>0, 'content'=>$specs_description);
		}
		//Display specs fileds specs
		if(defined('PRODUCT_INFO_TAB_MANUFACTURER') && PRODUCT_INFO_TAB_MANUFACTURER == 'True')
		{
			$str_extra_info = '';
			$sql = "select m.manufacturers_id, m.manufacturers_name, m.manufacturers_image, mi.manufacturers_url from " . TABLE_MANUFACTURERS . " m
					INNER JOIN " . TABLE_MANUFACTURERS_INFO . " mi on (m.manufacturers_id = mi.manufacturers_id and mi.languages_id = '" . (int)$languages_id . "')
					where m.manufacturers_id = '" . (int)$this->product_info['manufacturers_id'] . "'";

			$rs_manufacturer_info = tep_db_query($sql);
			if(tep_db_num_rows($rs_manufacturer_info) > 0)
			{
				$manufacturer_info = tep_db_fetch_array($rs_manufacturer_info);
				if (tep_not_null($manufacturer_info['manufacturers_image']))
					$str_extra_info .= '<a href="' . tep_href_link(FILENAME_DEFAULT, 'manufacturers_id=' . $manufacturer_info['manufacturers_id']) . '">'.tep_image(DIR_WS_MANUFACTURERS . $manufacturer_info['manufacturers_image'], $manufacturer_info['manufacturers_name']) . '</a><br> <br>';
				$str_extra_info .= '<strong>' . BOX_MANUFACTURER_BRAND . '</strong>';
				if (tep_not_null($manufacturer_info['manufacturers_url'])) {
					$str_extra_info .= '<span class="main">&bull; <a href="' . tep_href_link(FILENAME_REDIRECT, 'action=manufacturer&manufacturers_id=' . $manufacturer_info['manufacturers_id']) . '" target="_blank">' . sprintf(BOX_MANUFACTURER_INFO_HOMEPAGE, $manufacturer_info['manufacturers_name']) . '</a></span><br>';
				}
				//$str_extra_info .='<span class="main"><a href="' . tep_href_link(FILENAME_DEFAULT, 'manufacturers_id=' . $manufacturer_info['manufacturers_id']) . '"> ' . $manufacturer_info['manufacturers_name'] . '</a></span><br>';

				$sql = "select p.products_id, p.products_image, p.products_model, pd.products_name, m.manufacturers_id, m.manufacturers_name, m.manufacturers_image, mi.manufacturers_url from " . TABLE_MANUFACTURERS . " m
						INNER JOIN " . TABLE_MANUFACTURERS_INFO . " mi on (m.manufacturers_id = mi.manufacturers_id and mi.languages_id = '" . (int)$languages_id . "')
						INNER JOIN " . TABLE_PRODUCTS . " p ON p.manufacturers_id = m.manufacturers_id
						INNER JOIN ". TABLE_PRODUCTS_DESCRIPTION ." pd on p.products_id=pd.products_id AND pd.language_id=  ". (int) $languages_id ."
						where p.manufacturers_id = '" . (int)$this->product_info['manufacturers_id'] . "' AND p.products_id <> " . (int)$this->product_info['products_id'] . " LIMIT 8";
				$product_manufacturer_query = tep_db_query($sql);
				if (tep_db_num_rows($product_manufacturer_query))
				{
					$str_extra_info .= '<div class="col-xl-12 col-lg-12 col-12 col-sm-12 fullpage-module-heading"><h3 class="mod_also_purchased">' . TEXT_MORE_PRODUCTS_FROM_THIS_MANUFACTURER . '</h3></div><div class="col-xl-12 col-lg-12 col-12 col-sm-12 also-purchase-container product-listing-module-container"><div class="row" style="margin:0px;">';
					while ($manufacturer = tep_db_fetch_array($product_manufacturer_query))
					{
						$product_contents = $obj_catalog->prod_grid_listing($manufacturer, $column_list);
						$lc_text = implode( $product_contents);

						$str_extra_info .='<div class="'.$divcolumn.' product-listing-module-items item-outer with-small-padding"><div class=" itembox"><div class="thumbnail align-center row" style="margin-left:0px;margin-right:0px;height:350px;">'. $lc_text .'</div></div></div>';
					}
					$str_extra_info .= '</div></div><div class="clearfix"></div>'."\n";
				}

				$arr_tabs[] = array('heading'=>TEXT_TAB_PRODUCT_MANUFACTURER, 'tag'=>'tab-mfg_info', 'active'=>0, 'content'=>$str_extra_info);
			}
		}

		$other_tab = 1;
		$extra_desc_tab_content = '';
		if (defined('MODULE_ADDONS_TABS_STATUS') && MODULE_ADDONS_TABS_STATUS == 'True') {
			if(tep_not_null($this->product_info['products_tab_2'])){
				$extra_desc_tab_content .= '<div id="tab-description2" class="tab-pane">'. cre_clean_product_description($this->product_info['products_tab_2']) .'</div>';
				$arr_tabs[] = array('heading'=>(tep_not_null(TEXT_PRODUCTS_TAB_2_TITLE ) ? TEXT_PRODUCTS_TAB_2_TITLE : ' &nbsp; '), 'tag'=>'tab-products_tab_2', 'active'=>0, 'content'=>$extra_desc_tab_content);
			}
			if(tep_not_null($this->product_info['products_tab_3'])){
				$extra_desc_tab_content .= '<div id="tab-description3" class="tab-pane">'. cre_clean_product_description($this->product_info['products_tab_3']) .'</div>';
				$arr_tabs[] = array('heading'=>(tep_not_null(TEXT_PRODUCTS_TAB_3_TITLE ) ? TEXT_PRODUCTS_TAB_3_TITLE : ' &nbsp; '), 'tag'=>'tab-products_tab_3', 'active'=>0, 'content'=>$extra_desc_tab_content);
			}
			if(tep_not_null($this->product_info['products_tab_4'])){
				$extra_desc_tab_content .= '<div id="tab-description4" class="tab-pane">'. cre_clean_product_description($this->product_info['products_tab_4']) .'</div>';
				$arr_tabs[] = array('heading'=>(tep_not_null(TEXT_PRODUCTS_TAB_4_TITLE ) ? TEXT_PRODUCTS_TAB_4_TITLE : ' &nbsp; '), 'tag'=>'tab-products_tab_4', 'active'=>0, 'content'=>$extra_desc_tab_content);
			}
		}
			$reviews_content = '';
			$reviews_query = tep_db_query("select count(*) as count from " . TABLE_REVIEWS . " where products_id = '" . $this->product_info['products_id'] . "' and isapproved = 1");
			$reviews = tep_db_fetch_array($reviews_query);
			if ($reviews['count'] > 0) {
				//$reviews_content .= '<span class="main" style="font-weight:bold">' . TEXT_CURRENT_REVIEWS . ' </span> ' . $reviews['count'];
				$reviews_content .= '<div class="row">';
					if(B2B_ALLOW_LOGIN){
					  $reviews_content .= '<div class="col-lg-4 col-12 col-md-4 col-sm-12 show-on-mobile small-padding-left small-padding-right"><div class="rating-block clearfix"><h4>Add user rating</h4><p><a rel="nofollow" href="javascript:" onclick="writereview('.$this->product_info['products_id'].');" style="font-weight:bold"><i class="fa fa-comments" aria-hidden="true"></i> ' . BOX_REVIEWS_WRITE_REVIEW .'</a></p></div></div>';
					}
					$reviews_content .= '<div class="col-lg-4 col-12 col-md-4 col-sm-6 small-padding-left small-padding-right">
											<div class="rating-block clearfix">
												<h4>Average user rating</h4>
												<h2 class="bold padding-bottom-7">'.avgreviewRatingStars($this->product_info['products_id']).' <small>/ 5</small></h2>
												'.totalavgUserRating(avgreviewRatingStars($this->product_info['products_id'])).'
											</div>
										</div>';
					$reviews_content .= '
										<div class="col-lg-4 col-12 col-md-4 col-sm-6 small-padding-left small-padding-right">
											<div class="rating-block clearfix">
												<h4>Rating breakdown</h4>
												'.reviewRatingBar($this->product_info['products_id']).'
											</div>
										</div>';
					if(B2B_ALLOW_LOGIN){
					  $reviews_content .= '<div class="col-lg-4 col-12 col-md-4 col-sm-12 hide-on-mobile small-padding-left small-padding-right"><div class="rating-block clearfix"><h4>Add user rating</h4><p><a rel="nofollow" href="javascript:" onclick="writereview('.$this->product_info['products_id'].');" style="font-weight:bold"><i class="fa fa-comments" aria-hidden="true"></i> ' . BOX_REVIEWS_WRITE_REVIEW .'</a></p></div></div>';
					}
					$reviews_content .= '</div>';

             $reviews_read = tep_db_query("select *  from " . TABLE_REVIEWS . " r , " . TABLE_REVIEWS_DESCRIPTION . "  rd where products_id = '" . $this->product_info['products_id'] . "' AND r.reviews_id = rd.reviews_id and isapproved = 1 order by date_added DESC limit 5 ");
             $count=0;
             while($current_reviews = tep_db_fetch_array($reviews_read)){
				$reviews_content .='

									<div class="row">
										<div class="col-sm-12">
											<div class="review-block">
												<div class="row">
													<div class="col-sm-3">
														<i class="fa fa-3x fa-user" aria-hidden="true" style="border: 1px solid antiquewhite; padding: 10px; color: antiquewhite;"></i>
														<div class="review-block-name">'.$current_reviews['customers_name'].'</div>
														<div class="review-block-date">'.tep_date_long($current_reviews['date_added']).'<br/></div>
													</div>
													<div class="col-sm-9">
														<div class="review-block-rate">'.reviewRatingStars($current_reviews['reviews_rating']).'</div>
														<div class="review-block-title">'.tep_output_string_protected(substr($current_reviews['reviews_text'],0,50)).'...</div>
														<div class="review-block-description">'.tep_break_string(nl2br(tep_output_string_protected($current_reviews['reviews_text'])), 60, '-<br>').'</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								   ';
             }
             $reviews_content .= '<span class="main pull-right"><a href="' . tep_href_link(FILENAME_PRODUCT_REVIEWS, 'products_id=' . $this->product_info['products_id']) . '" style="font-weight:bold">' . BOX_REVIEWS_READ_REVIEW . '</a></span>';
			} else {
				$reviews_content = '<a href="javascript:" onclick="writereview('.$this->product_info['products_id'].');"><h4> <i class="fa fa-comments" aria-hidden="true"></i> Write a review</h4></a><hr class="small-margin-top small-margin-bottom">';
				$reviews_content .= '<span class="main">' . BOX_REVIEWS_NO_REVIEWS . '</span>';
				if(defined('B2B_ALLOW_LOGIN') && B2B_ALLOW_LOGIN == '1'){
				  $reviews_content .= '<span class="main pull-right"><a rel="nofollow" href="' . tep_href_link(FILENAME_PRODUCT_REVIEWS_WRITE, 'products_id=' . $this->product_info['products_id']) . '" style="font-weight:bold">' . BOX_REVIEWS_WRITE_REVIEW .'</a></span><br>';
			    }
			}
			$reviews_content .= '<br><br>';

			if(isset($arr_addons_pagedata['product_info']['tabs'])) {
			  foreach($arr_addons_pagedata['product_info']['tabs'] as $tabdata) {
				$arr_tabs[] = $tabdata;
			  }
			}

			if(isset($this->product_tab) && is_array($this->product_tab)) {
			  foreach($this->product_tab as $tabdata) {
				$arr_tabs[] = $tabdata;
			  }
			}

			$reviews_heading = 'Review';
			if ($reviews['count'] > 0) {
				$reviews_heading .= '<span class="main">(' . $reviews['count'] .')</span>';
			} else {
				$reviews_heading .= '<span class="main">(0)</span><br>';
			}
			$arr_tabs[] = array('heading'=>$reviews_heading, 'tag'=>'tab-reviews', 'active'=>0, 'content'=>$reviews_content);

		// Code for Extra Info
		if(defined('PRODUCT_INFO_TAB_EXTRAINFO') && PRODUCT_INFO_TAB_EXTRAINFO == 'True')
		{
			$str_extra_info = '';
			$str_extra_info.= '<table cellpadding="0" cellspacing="0" border="0" style="width:100%;font-size:16px;"> <tr> <td style="padding-left:8px;">';
			if (tep_not_null($this->product_info['products_url'])) {
			 $str_extra_info .= '<span class="main">' . sprintf(TEXT_MORE_INFORMATION, tep_href_link(FILENAME_REDIRECT,  'action=url&amp;goto=' . urlencode($this->product_info['products_url']), 'NONSSL', true, false)) . '</span><br>';
			}

			$str_extra_info .= '<span class="main"><strong>' . TAB_EXTRA_INFORMATIONS . '</strong></span><br>';
			if ($this->product_info['products_date_available'] > date('Y-m-d H:i:s')) {
				$str_extra_info .= '<span class="main">' . sprintf(TEXT_DATE_AVAILABLE, tep_date_long($this->product_info['products_date_available'])) . '</span>';
			} else {
				$str_extra_info .= '<span class="main">' . sprintf(TEXT_DATE_ADDED, tep_date_long($this->product_info['products_date_added'])) . '</span>';
			}
			$str_extra_info .= '<br><span class="main"><a href="' . tep_href_link(FILENAME_TELL_A_FRIEND, 'pid=' . (int)$_GET['products_id']) . '">' . BOX_TELL_A_FRIEND_TEXT . '</a></span><br>';
			$str_extra_info .= '</td> </tr> </table>';

			$arr_tabs[] = array('heading'=>TEXT_TAB_PRODUCT_EXTRA_INFO, 'tag'=>'tab-extra_info', 'active'=>0, 'content'=>$str_extra_info);
		}

		return $arr_tabs;
	}
	function has_subproducts()
	{
		$query = "SELECT count(products_id) as numsub FROM ".TABLE_PRODUCTS." WHERE products_parent_id='". $this->product_info['products_id'] ."'";
		$rw_sub = tep_db_fetch_array(tep_db_query($query));
		return $rw_sub['numsub'];
	}

	function show_inventory_text()
	{
		if(DISPLAY_INVENTORY_OPTION != 'DO_NOT_DISPLAY')
		{
			$minimum_stock_for_display_and_buy = (int) MINIMUM_STOCK_FOR_DISPLAY_AND_BUY;
			$minimum_stock_for_display_and_buy = ($minimum_stock_for_display_and_buy == '')?1:$minimum_stock_for_display_and_buy;
			$str_inventory_text = '';
			if(DISPLAY_INVENTORY_OPTION == 'AS_NUMBER' && ($this->product_info['products_quantity']) > $minimum_stock_for_display_and_buy)
				$str_inventory_text = '<b>Stock:</b> '.$this->product_info['products_quantity'];
			elseif(DISPLAY_INVENTORY_OPTION == 'AS_TEXT_ONLY')
				$str_inventory_text = ($this->product_info['products_quantity'] >= $minimum_stock_for_display_and_buy)?'<span class="fa fa-check-circle" style="margin-bottom: 5px;margin-top: 5px;">&nbsp;'  . STOCK_MESSAGE_TEXT . '</span>':'<span class="fa fa-circle" style="margin-left:0px">&nbsp;' . OUT_OF_STOCK_MESSAGE_TEXT . '</span>';
			if($str_inventory_text != "")
				$ret_text = '<br/><span class="content-products-info-reviews-rating margin-right">'.$str_inventory_text.'</span>';
			else
				$ret_text = '';
		}
		else
			$ret_text = '';
		return $ret_text;
	}

	function show_manufacturer_text()
	{
		if($this->product_info['manufacturers_id'] > 0)
		{
			return '<span class="content-products-info-reviews-rating margin-right">'. tep_get_manufacturers_name($this->product_info['manufacturers_id']) .'</span>';
		}
	}


	function product_info_page_js()
	{
		$page_js = "";
		if(isset($this->product_info['page_js']))
		{
			foreach($this->product_info['page_js'] as $indv_js)
			{
				$page_js .= trim($indv_js);
			}
		}

		if(trim($page_js) != "")
		{
			$page_js = "\n".trim($page_js)."\n";
		}
		return $page_js;
	}
} //end of class
?>