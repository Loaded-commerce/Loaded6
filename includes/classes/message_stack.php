<?php
/*
  $Id: message_stack.php,v 1.1.1.1 2004/03/04 23:40:44 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License

  Example usage:

  $messageStack = new messageStack();
  $messageStack->add('general', 'Error: Error 1', 'error');
  $messageStack->add('general', 'Error: Error 2', 'warning');
  if ($messageStack->size('general') > 0) echo $messageStack->output('general');
*/
  class messageStack extends tableBoxMessagestack {
    var $messageToStack, $messages;

// class constructor
    function __construct() {
      $this->messages = array();

      if ( ! isset($_SESSION['messageStack_data']) ) {
        $_SESSION['messageStack_data'] = array('messageToStack' => array());
      }
      $this->messageToStack =& $_SESSION['messageStack_data']['messageToStack'];

      for ($i=0, $n=sizeof($this->messageToStack); $i<$n; $i++) {
        $this->add($this->messageToStack[$i]['class'], $this->messageToStack[$i]['text'], $this->messageToStack[$i]['type']);
      }
      $this->messageToStack = array();
    }

// class methods
    function add($class, $message, $type = 'error') {
      if ($type == 'error') {
        $this->messages[] = array('params' => 'class="messageStackError"', 'class' => $class, 'text' => $message,'type' => $type);
      } elseif ($type == 'warning') {
        $this->messages[] = array('params' => 'class="messageStackWarning"', 'class' => $class, 'text' =>$message,'type' => $type);
      } elseif ($type == 'success') {
        $this->messages[] = array('params' => 'class="messageStackSuccess "', 'class' => $class, 'text' =>$message,'type' => $type);
      }
      elseif($type == 'error_noimg') {
        $this->messages[] = array('params' => 'class="messageStackError"', 'class' => $class, 'text' => $message,'type' => $type);
      } else {
        $this->messages[] = array('params' => 'class="messageStackError"', 'class' => $class, 'text' => $message,'type' => $type);
      }
    }


    function add_session($class, $message, $type = 'error') {
    // echo $message; exit;
     $this->messageToStack[] = array('class' => $class, 'text' => $message, 'type' => $type);
    }

    function reset() {
      $this->messages = array();
    }

    function output($class) {
      $this->table_data_parameters = 'class="messageBox"';

      $output = array();

      $message_type = '';
      $message_class = '';
      $list_error = '';
      $list_warning = '';
      $list_success = '';

      $output_all_message = '';
      for ($i=0, $n=sizeof($this->messages); $i<$n; $i++) {
        if ($this->messages[$i]['class'] == $class) {
          //$output[] = $this->messages[$i];
          $output_all_message .= '<li>'.$this->messages[$i]['text'].'</li>';

          if($message_type == ''){
           $message_type = $this->messages[$i]['type'];
           $message_class = $this->messages[$i]['params'];
          }
        }
      }
      // $output_all_message.= '</ul>';


      if ($message_type == 'error') {
        $list_error .= '<div id="alert-error" class="row fade-error mb-2"><div class="col p-0 mt-0 mb-2"><div class="note note-danger m-0"><h4 class="m-0">' . TEXT_ERROR . '</h4><ul class="list-unstyled mt-2 mb-0">';
        $list_error .= $output_all_message;
        $list_error .= '</ul></div></div></div>';
      }

      if ($message_type == 'warning') {
        $list_warning .= '<div id="alert-warning" class="row fade-error mb-2"><div class="col p-0 mt-0 mb-2"><div class="note note-warning m-0"><h4 class="m-0">' . TEXT_WARNING . '</h4><ul class="list-unstyled mt-2 mb-0">';
		$list_warning .= $output_all_message;
		$list_warning .= '</ul></div></div></div>';
      }

      if ($message_type == 'success') {
        $list_success .= '<div id="alert-success" class="row fade-error mb-2"><div class="col p-0 mt-0 mb-2"><div class="note note-success m-0"><h4 class="m-0">' . TEXT_SUCCESS . '</h4><ul class="list-unstyled mt-2 mb-0">';
        $list_success .= $output_all_message;
        $list_success .= '</ul></div></div></div>';
      }


      return ($list_error . $list_warning . $list_success);
     // return $this->tableBoxMessagestack($output);
    }

    function size($class) {
      $count = 0;

      for ($i=0, $n=sizeof($this->messages); $i<$n; $i++) {
        if ($this->messages[$i]['class'] == $class) {
          $count++;
        }
      }

      return $count;
    }
  }
?>
