<?php
  class captcha{

	public function __construct() {
		global $language;

		$this->active_captcha = (defined('MODULE_CAPTCHA_INSTALLED') && trim(MODULE_CAPTCHA_INSTALLED) != '')?substr(MODULE_CAPTCHA_INSTALLED, 0, -4):'no_captcha';
		if($this->active_captcha != 'no_captcha') {
			include(DIR_WS_MODULES . 'addons/' . $this->active_captcha .'.php');
	          $this->active_captcha_class = new $this->active_captcha;
		}


	}  //end of __construct
	public function captcha_status($page_status) {
	    return (($this->active_captcha != 'no_captcha' && ((defined($page_status) && constant($page_status) == 'On')))?1:0);
	}

	public function display_captcha_label() {
		global $language;
		if($this->active_captcha != 'no_captcha') {
			return $this->active_captcha_class->display_captcha_label();
		}
	}

	public function display_captcha_instructions() {
		global $language;
		if($this->active_captcha != 'no_captcha') {
			return $this->active_captcha_class->display_captcha_instructions();
		}
	}

	public function render_captcha_image_data() {
		if($this->active_captcha != 'no_captcha') {
			return $this->active_captcha_class->render_captcha_image_data();
		}
	}

	public function render_captcha_image() {
		if($this->active_captcha != 'no_captcha') {
			return $this->active_captcha_class->render_captcha_image();
		}
	}

	public function render_captcha_control($params) {
		if($this->active_captcha != 'no_captcha') {
			return $this->active_captcha_class->render_captcha_control($params);
		}
	}

	public function validate_captcha() {
		if($this->active_captcha != 'no_captcha') {
			return $this->active_captcha_class->validate_captcha();
		}else
		  return false;
	}

  }

?>