<?php
class lc_coupon {

  public function __construct() {
	global $language;

  }  //end of __construct
	
  public function getCouponByCode($code) {
	global $language;

	$coupon_query = "SELECT * FROM " . TABLE_COUPONS . " A INNER JOIN ". TABLE_COUPONS_DESCRIPTION ." B ON A.coupon_id=B.coupon_id WHERE coupon_code = '" . $code . "'";
	$coupon = tep_db_get_result($coupon_query);
	return $coupon[0];
  } 
  public function checkCouponByCode($code) {
	global $language;

	$coupon_query = "SELECT coupon_code FROM " . TABLE_COUPONS . " WHERE coupon_code = '" . $code . "'";
	$num = tep_db_num_rows(tep_db_query($coupon_query));
	if($num > 0)
		return 1;
	else	
		return 0;
  } 

  public function insertCoupon($cData) {
	$sql_data_array = array('coupon_code' => $cData['coupon_code'],
							'coupon_type' => $cData['coupon_type'],
							'coupon_amount' => $cData['coupon_amount'],
							'date_created' => $cData['date_created']);
	tep_db_perform(TABLE_COUPONS, $sql_data_array);
	$coupon_id = tep_db_insert_id();
	return $coupon_id;
  }
  
  public function insertCouponTrack($coupon_id, $cData) {

	$sql_data_array = array('coupon_id' => $cData['coupon_id'],
							'customer_id_sent' => $cData['customer_id_sent'],
							'sent_firstname' => $cData['sent_firstname'],
							'date_sent' => $cData['date_sent'],
							'emailed_to' => $cData['emailed_to']);
	tep_db_perform(TABLE_COUPON_EMAIL_TRACK, $sql_data_array);
  }
  
  public function getNewSignupEmailText($coupon) {
	  $email_text = EMAIL_COUPON_INCENTIVE_HEADER .  "\n" .
					 sprintf("%s", $coupon['coupon_description']) ."\n\n" .
					 sprintf(EMAIL_COUPON_REDEEM, $coupon['coupon_code']) . "\n\n" .
					 "\n\n";
  	  return $email_text;	
  }

  public function getNewSignupGFEmailText($coupon_code) {
	global $currencies;
	$email_text = sprintf(EMAIL_GV_INCENTIVE_HEADER, $currencies->format(NEW_SIGNUP_GIFT_VOUCHER_AMOUNT)) . "\n\n" .
				 sprintf(EMAIL_GV_REDEEM, $coupon_code) . "\n\n" .
				 EMAIL_GV_LINK . tep_href_link(FILENAME_GV_REDEEM, 'gv_no=' . $coupon_code,'NONSSL', false) .
				 "\n\n";
	return $email_text;	
  }
  
}
?>