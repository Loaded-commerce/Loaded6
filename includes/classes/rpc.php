<?php
class rpc extends catalog {

  function __construct()
  {
    // some code here
  }
  public function rpc_shippingaddressform() {

 	global $lc_checkout;
 	$rdata = $lc_checkout->shippingaddressform();
 	echo $rdata['data'];

  }

  public function rpc_setshippingAddresshtml() {

 	global $lc_checkout;
 	$rdata = $lc_checkout->setshippingAddresshtml();
 	echo $rdata['data'];

  }

  public function rpc_setbillingAddresshtml() {

 	global $lc_checkout;
 	$rdata = $lc_checkout->setbillingAddresshtml();
 	echo $rdata['data'];

  }

  public function rpc_addshippingaddress() {

 	global $lc_checkout;
 	$rdata = $lc_checkout->addshippingaddress();
 	echo $rdata['data'];

  }

  public function rpc_setshippingmethod() {

 	global $lc_checkout;
 	$rdata = $lc_checkout->setshippingmethod();
 	echo json_encode($rdata);
 	//echo $rdata['data'];

  }

  public function rpc_getshippingestimate() {

 	global $lc_checkout;
 	$rdata = $lc_checkout->getshippingestimate();
 	echo $rdata['data'];

  }

  public function rpc_updateaccounfinormation() {

 	global $lc_checkout;
 	$rdata = $lc_checkout->updateaccounfinormation();
 	echo $rdata['data'];

  }

  public function rpc_editshippingaddresshtml() {

 	global $lc_checkout;
 	$rdata = $lc_checkout->editshippingaddresshtml();
 	echo $rdata['data'];

  }

  public function rpc_editbillingaddresshtml() {

 	global $lc_checkout;
 	$rdata = $lc_checkout->editbillingaddresshtml();
 	echo $rdata['data'];

  }

  public function rpc_getstate() {

 	global $obj_catalog;
 	$rdata = $obj_catalog->getstate();
 	echo $rdata['data'];

  }
  public function rpc_getprice() {

 	global $obj_catalog;
 	$rdata = $obj_catalog->getAttributePrice();
 	echo $rdata['data'];

  }

  public function rpc_review_form() {
 	global $obj_catalog;
 	$rdata = $obj_catalog->review_form();
 	echo $rdata['data'];
  }
  public function rpc_ajax_add_review() {
 	global $obj_catalog;
 	$rdata = $obj_catalog->ajax_add_review();
 	echo $rdata['data'];
  }

}

?>