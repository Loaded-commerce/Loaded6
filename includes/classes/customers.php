<?php
class customers {

  public function __construct() {
	global $language;

  }  //end of __construct

  public function checkCustomerExist($cEmail) {
	global $language;

	$check_email_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where lower(customers_email_address) = '" . tep_db_input($cEmail) . "'");
	$check_email = tep_db_fetch_array($check_email_query);
	if ($check_email['total'] > 0) 
		return true;
	else	
		return false;
  }  

  public function getCustomerById($cId) {
	global $language;

	$query = "SELECT * FROM ". TABLE_CUSTOMERS." A INNER JOIN ". TABLE_CUSTOMERS_INFO ." B ON A.customers_id=B.customers_info_id WHERE A.customers_id='". (int) $cId ."'";
	$rw = tep_db_fetch_array(tep_db_query($query));
	return $rw;
  }  

  public function getCustomerByEmail($cEmail) {
	global $language;
	$query = "SELECT * FROM ". TABLE_CUSTOMERS." A INNER JOIN ". TABLE_CUSTOMERS_INFO ." B ON A.customers_id=B.customers_info_id WHERE A.customers_email_address='". tep_db_input($cEmail) ."'";
	$rw = tep_db_fetch_array(tep_db_query($query));
	return $rw;

  }  

  public function addCustomer($cData) {
	global $language;
	if(trim($cData['firstname']) == '' || trim($cData['lastname']) == '' || trim($cData['email_address']) == '') {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {
		$sql_data_array = array('customers_firstname' => $cData['firstname'],
								'customers_lastname' => $cData['lastname'],
								'customers_email_address' => $cData['email_address'],
								'customers_newsletter' => $cData['newsletter'],
								'purchased_without_account' => $cData['purchased_without_account'],
								'is_complete' => 0,
								'customers_password' => tep_encrypt_password($cData['password']));
		if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $cData['gender'];
		if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($cData['dob']);
		if (ACCOUNT_EMAIL_CONFIRMATION == 'false' ) $sql_data_array['customers_validation'] = '1';
		tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);
		$customers_id = tep_db_insert_id();

	    tep_db_query("insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_number_of_logons, customers_info_date_account_created) values ('" . (int)$customers_id . "', '0', now())");

		$return_data = array('status'=>1, 'data'=>array('customers_id'=>$customers_id));
		return $return_data;
	}
  }  

  public function updateCustomer($cId, $cData) {
	global $language;
	if($cId <= 0 || trim($cData['customers_firstname']) == '' || trim($cData['customers_lastname']) == '') {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {
		$sql_data_array = array('customers_firstname' => $cData['customers_firstname'],
								'customers_lastname' => $cData['customers_lastname']);
		//if (isset($cData['customers_email_address'])) $sql_data_array['customers_email_address'] = $cData['customers_email_address'];
		if (isset($cData['customers_newsletter'])) $sql_data_array['customers_newsletter'] = $cData['customers_newsletter'];
		if (isset($cData['purchased_without_account'])) $sql_data_array['purchased_without_account'] = $cData['purchased_without_account'];
		if (isset($cData['is_complete'])) $sql_data_array['is_complete'] = $cData['is_complete'];
		if (isset($cData['customers_password'])) $sql_data_array['customers_password'] = tep_encrypt_password($cData['customers_password']));
		if (ACCOUNT_GENDER == 'true' && isset($cData['customers_gender'])) $sql_data_array['customers_gender'] = $cData['customers_gender'];
		if (ACCOUNT_DOB == 'true' && isset($cData['customers_dob'])) $sql_data_array['customers_dob'] = tep_date_raw($cData['customers_dob']);

		tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$cId . "'");

		tep_db_query("UPDATE " . TABLE_CUSTOMERS_INFO . " set customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$cId . "'");

		$return_data = array('status'=>1, 'data'=>array('customers_id'=>$cId));
		return $return_data;
	}
  }  

  public function setIsComplete($cId, $complete) {

		tep_db_query("UPDATE " . TABLE_CUSTOMERS . " set is_complete = ". (int)$complete ." where customers_id = '" . (int) $cId . "'");
  }

  public function setCustomerValidation($cId, $validation) {

		tep_db_query("UPDATE " . TABLE_CUSTOMERS . " set customers_validation = ". (int)$validation ." where customers_id = '" . (int) $cId . "'");
  }

  public function deleteCustomer($cId) {
	global $language;
	$cId = (int) $cId;
	
	$query = "SELECT address_id FROM ". TABLE_ADDRESS_BOOK ." WHERE customers_id='". $cId ."'";
	$rs = tep_db_query($query);
	while($rw = tep_db_fetch_array($rs)) {
	
		$this->deleteAddressBook($cId, $rw['address_id']);
	
	}
	
	$query = "DELETE FROM ". TABLE_CUSTOMERS ." WHERE customers_id='". $cId ."'";
	tep_db_query($query);

	$query = "DELETE FROM ". TABLE_CUSTOMERS_INFO ." WHERE customers_info_id='". $cId ."'";
	tep_db_query($query);
  }  

  public function addAddressBook($cId, $abData) {
	global $language;
	$cId = (int) $cId;
	if($cId <= 0 || trim($cData['firstname']) == '' || trim($cData['lastname']) == '' || trim($cData['email_address']) == '') {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {

	  $sql_data_array = array('entry_firstname' => $abData['firstname'],
							  'entry_lastname' => $abData['lastname'],
							  'entry_email_address' => $abData['email_address'],
							  'entry_telephone' => $abData['telephone'],
							  'entry_fax' => $abData['fax'],
							  'entry_street_address' => $abData['street_address'],
							  'entry_postcode' => $abData['postcode'],
							  'entry_city' => $abData['city'],
							  'entry_country_id' => (int)$abData['country']);

	  if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $abData['gender'];
	  if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $abData['company'];

	  if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $abData['suburb'];
	  if (ACCOUNT_STATE == 'true') {
		if ($abData['zone_id'] > 0) {
		  $sql_data_array['entry_zone_id'] = (int)$abData['zone_id'];
		  $sql_data_array['entry_state'] = '';
		} else {
		  $sql_data_array['entry_zone_id'] = '0';
		  $sql_data_array['entry_state'] = $abData['state'];
		}
	  }
		tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
		$address_id = tep_db_insert_id();
		$return_data = array('status'=>1, 'data'=>array('address_id'=>$address_id));
		return $return_data;
	}
  }  

  public function updateAddressBook($cId, $abId, $abData) {
	global $language;
	$cId = (int) $cId;

	if($cId <= 0 ||  $abId <= 0 || trim($cData['firstname']) == '' || trim($cData['lastname']) == '' || trim($cData['email_address']) == '') {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {

      $sql_data_array = array('entry_firstname' => $abData['firstname'],
                              'entry_lastname' => $abData['lastname'],
                              'entry_email_address' => $abData['email_address'],
                              'entry_telephone' => $abData['telephone'],
                              'entry_fax' => $abData['fax'],
                              'entry_street_address' => $abData['street_address'],
                              'entry_postcode' => $abData['postcode'],
                              'entry_city' => $abData['city'],
                              'entry_country_id' => (int)$abData['country']);

      if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $abData['gender'];
      if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $abData['company'];

      if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $abData['suburb'];
      if (ACCOUNT_STATE == 'true') {
        if ($abData['zone_id'] > 0) {
          $sql_data_array['entry_zone_id'] = (int)$abData['zone_id'];
          $sql_data_array['entry_state'] = '';
        } else {
          $sql_data_array['entry_zone_id'] = '0';
          $sql_data_array['entry_state'] = $abData['state'];
        }
      }
		tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "address_book_id = '" . (int)$abId . "' and customers_id ='" . (int)$cId . "'");
		$return_data = array('status'=>1, 'data'=>array('customers_id'=>$cId, 'address_id'=>$address_id));
		return $return_data;
	}
  }  

  public function deleteAddressBook($cId, $abId) {
	global $language;
	$cId = (int) $cId;
	$abId = (int) $abId;
	if($cId <= 0 ||  $abId <= 0) {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {

		$query = "DELETE FROM ". TABLE_ADDRESS_BOOK ." WHERE customers_id='". $cId ."' AND address_id='". $abId ."'";
		tep_db_query($query);
		$return_data = array('status'=>1, 'msg' => '');
		return $return_data;		
	}

  }  

  public function setDefaultAddress($cId, $abId) {
	if($cId <= 0 ||  $abId <= 0) {
		$return_data = array('status'=>0, 'msg' => MISSING_DATA);
		return $return_data;		
	}
	else {
		tep_db_query("UPDATE " . TABLE_CUSTOMERS . " set customers_default_address_id = ". (int)$abId ." where customers_id = '" . (int) $cId . "'");
		$return_data = array('status'=>1, 'msg' => '');
		return $return_data;		
	}
  }

}
?>