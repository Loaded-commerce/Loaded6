<?php
  if(isset($obj_catalog->code_mod) && $obj_catalog->code_mod != '') {
	//Call addon functions to show the page data
	lc_addon_pagedata();
	//----------
	if($obj_catalog->code_mod != 'core') {
		define('MODULE_ADDONS_CATALOG_PATH', 'addons/lc_'.$obj_catalog->code_mod.'/catalog/');
		$addon_file_path = MODULE_ADDONS_CATALOG_PATH . $obj_catalog->code_file;
		define('MODULE_ADDONS', $obj_catalog->code_mod);
		if(file_exists($addon_file_path)) {
			include($addon_file_path);
		}
	}
	else {
		if(file_exists('addons/lc_custom/catalog/'. $obj_catalog->code_file)) {
			$obj_catalog->code_mod = 'custom';
			define('MODULE_ADDONS_CATALOG_PATH', 'addons/lc_custom/catalog/');
			$addon_file_path = MODULE_ADDONS_CATALOG_PATH . $obj_catalog->code_file;
			define('MODULE_ADDONS', $obj_catalog->code_mod);
			include($addon_file_path);
		} else if(file_exists($obj_catalog->code_mod.'/'. $obj_catalog->code_file))
			require($obj_catalog->code_mod.'/'. $obj_catalog->code_file);
		else {
			header("Location: ". tep_href_link('page-not-found.php'));
		}
	}
  }
  else {
	require('core/index.php');
  }
