<?php
/*
  $Id: index.php,v 1.2 2019/03/09 19:56:29 devidash Exp $

  Loaded Commerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com/

  Copyright (c) 2019 loadedcommerce

  Released under the GNU General Public License
*/
  require('includes/application_top.php');
  require('includes/load_page.php');

  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>