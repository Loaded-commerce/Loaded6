<?php
$right_col_div = (BOX_WIDTH_RIGHT > 0)?BOX_WIDTH_RIGHT:3;
$content_col_div = 12 - $right_col_div;
$withsidebar = 'with-sidebar-page';
?>
 <!-- header //-->
 <?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . FILENAME_HEADER); ?>
 <!-- header_eof //-->
 <!-- body //-->
   <div class="container <?php echo $withsidebar; ?>" id="content-container">
    <div class="row mob-row">
    <?php if($content != 'index_default') {  ?>
 	<div class="small-margin-top hide-on-mobile col-xl-12 col-lg-12 col-md-12 no-padding main-breadcrumb breadcrumb-<?php echo $content; ?>" style="">
 		<?php echo '<ol class="breadcrumb"><li class="active">' . $breadcrumb->trail('&nbsp;&nbsp;/&nbsp;&nbsp;') . '</li></ol>' . "\n"; ?>
 	</div>
 	 <?php } ?>
 
    <div id="content-center-container" class="col-md-12 col-lg-<?php echo $content_col_div; ?> col-sm-12 col-12"> <?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/body.php'); ?> </div>
  
     <!-- content_eof //-->
     <?php $column_location = 'Right_'; ?>
        <div class="col-md-<?php echo $right_col_div; ?> col-lg-<?php echo $right_col_div; ?> col-sm-12 col-12 no-padding-right" id="content-right-container">
         <!-- right_navigation //-->
         <?php require(DIR_WS_INCLUDES . FILENAME_COLUMN_RIGHT); ?>
         <!-- right_navigation_eof //-->
       </div>
 
       <?php $column_location = ''; ?>
    </div>
   </div>
     <!-- content_eof //-->
 </div>
 <!-- body_eof //-->
 <!-- footer //-->
 <?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME .'/'.FILENAME_FOOTER); ?>
<!-- footer_eof //-->