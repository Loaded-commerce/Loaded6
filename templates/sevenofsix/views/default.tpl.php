<?php
 if(DISPLAY_COLUMN_LEFT == 'yes' && DISPLAY_COLUMN_RIGHT == 'yes' && (DOWN_FOR_MAINTENANCE =='false' || DOWN_FOR_MAINTENANCE_COLUMN_LEFT_OFF =='false')) {
 	$left_col_div = (BOX_WIDTH_LEFT > 0)?BOX_WIDTH_LEFT:2;
 	$right_col_div = (BOX_WIDTH_RIGHT > 0)?BOX_WIDTH_RIGHT:2;
 	$content_col_div = (12 - ($left_col_div + $right_col_div));
 	$withsidebar = 'with-sidebar-page';
 } elseif((DISPLAY_COLUMN_LEFT == 'yes' || DISPLAY_COLUMN_RIGHT == 'yes') && (DOWN_FOR_MAINTENANCE =='false' || DOWN_FOR_MAINTENANCE_COLUMN_LEFT_OFF =='false')) {
     $withsidebar = 'with-sidebar-page';
 	if(DISPLAY_COLUMN_LEFT == 'yes')
 		$left_col_div = (BOX_WIDTH_LEFT > 0)?BOX_WIDTH_LEFT:3;
 	else
 		$left_col_div = 0;
 	if(DISPLAY_COLUMN_RIGHT == 'yes')
 		$right_col_div = (BOX_WIDTH_RIGHT > 0)?BOX_WIDTH_RIGHT:3;
 	else
 		$right_col_div = 0;
 	$content_col_div = (12 - ($left_col_div + $right_col_div));
 } else {
     $withsidebar = 'no-sidebar-page';
 	$content_col_div = 12;
 }
 ?>
 <!-- header //-->
 <?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . FILENAME_HEADER); ?>
 <!-- header_eof //-->
 <!-- body //-->
   <div class="container <?php echo $withsidebar; ?>" id="content-container">
    <div class="row mob-row">
    <?php if($content != 'index_default'){  ?>
 	<div class="small-margin-top hide-on-mobile col-xl-12 col-lg-12 col-md-12 no-padding main-breadcrumb breadcrumb-<?php echo $content; ?>" style="">
 		<?php echo '<ol class="breadcrumb"><li class="active">' . $breadcrumb->trail('&nbsp;&nbsp;/&nbsp;&nbsp;') . '</li></ol>' . "\n"; ?>
 	</div>
 	 <?php }
 	 if (DISPLAY_COLUMN_LEFT == 'yes' && (DOWN_FOR_MAINTENANCE =='false' || DOWN_FOR_MAINTENANCE_COLUMN_LEFT_OFF =='false'))  {
 		 $column_location = 'Left_';
 	 ?>
    <div class="col-md-<?php echo $left_col_div; ?> col-lg-<?php echo $left_col_div; ?> col-sm-12 col-12 hide-on-mobile no-padding-left" id="content-left-container">
		<?php 
		ob_start();
		require(DIR_WS_INCLUDES . FILENAME_COLUMN_LEFT);
		$var = ob_get_clean();
		echo $var;
		?>
 	</div>
	<?php
		$column_location = '';
	}
	?>
    <div id="content-center-container" class="col-md-12 col-lg-<?php echo $content_col_div; ?> col-sm-12 col-12"><?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/body.php'); ?></div>
	<?php
	if (DISPLAY_COLUMN_LEFT == 'yes' && (DOWN_FOR_MAINTENANCE =='false' || DOWN_FOR_MAINTENANCE_COLUMN_LEFT_OFF =='false'))  {
	   $column_location = 'Left_';
	?>
	  <div class="col-md-<?php echo $left_col_div; ?> col-lg-<?php echo $left_col_div; ?> col-sm-12 col-12 large-margin-top show-on-mobile" id="content-left-container"> <?php echo $var;   ?> </div>
	<?php
	  $column_location = '';
	}
	?>
 
     <!-- content_eof //-->
	<?php
	if (DISPLAY_COLUMN_RIGHT == 'yes' && (DOWN_FOR_MAINTENANCE =='false' || DOWN_FOR_MAINTENANCE_COLUMN_LEFT_OFF =='false'))  {
		$column_location = 'Right_';
	?>
        <div class="col-md-<?php echo $right_col_div; ?> col-lg-<?php echo $right_col_div; ?> col-sm-12 col-12 no-padding-right" id="content-right-container"><?php require(DIR_WS_INCLUDES . FILENAME_COLUMN_RIGHT); ?></div>
	<?php
		$column_location = '';
	}
	?>
    </div>
   </div>
     <!-- content_eof //-->
 </div>
 <!-- body_eof //-->
 <!-- footer //-->
 <?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME .'/'.FILENAME_FOOTER); ?>
<!-- footer_eof //-->