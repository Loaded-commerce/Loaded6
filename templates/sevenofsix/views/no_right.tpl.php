<?php
$left_col_div = (BOX_WIDTH_LEFT > 0)?BOX_WIDTH_LEFT:3;
$content_col_div = 12 - $left_col_div;
$withsidebar = 'with-sidebar-page';
?>
 <!-- header //-->
 <?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . FILENAME_HEADER); ?>
 <!-- header_eof //-->
 <!-- body //-->
   <div class="container <?php echo $withsidebar; ?>" id="content-container">
    <div class="row mob-row">
    <?php if($content != 'index_default'){  ?>
 	<div class="small-margin-top hide-on-mobile col-xl-12 col-lg-12 col-md-12 no-padding main-breadcrumb breadcrumb-<?php echo $content; ?>" style="">
 		<?php echo '<ol class="breadcrumb"><li class="active">' . $breadcrumb->trail('&nbsp;&nbsp;/&nbsp;&nbsp;') . '</li></ol>' . "\n"; ?>
 	</div>
 	 <?php }
 		 $column_location = 'Left_';
 	 ?>
    <div class="col-md-<?php echo $left_col_div; ?> col-lg-<?php echo $left_col_div; ?> col-sm-12 col-12 hide-on-mobile no-padding-left" id="content-left-container">
		<?php 
		ob_start();
		require(DIR_WS_INCLUDES . FILENAME_COLUMN_LEFT);
		$var = ob_get_clean();
		echo $var;
		?>
 	</div>
    <div id="content-center-container" class="col-md-12 col-lg-<?php echo $content_col_div; ?> col-sm-12 col-12">
		<?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/body.php'); ?>
    </div>

	<div class="col-md-<?php echo $left_col_div; ?> col-lg-<?php echo $left_col_div; ?> col-sm-12 col-12 large-margin-top show-on-mobile" id="content-left-container">
	   <?php echo $var;   ?>
	</div>
	<?php
	$column_location = '';
	?>
 
  </div>
 </div>
 <!-- content_eof //-->
</div>
 <!-- body_eof //-->
 <!-- footer //-->
 <?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME .'/'.FILENAME_FOOTER); ?>
<!-- footer_eof //-->