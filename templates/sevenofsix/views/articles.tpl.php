 <!-- header //-->
 <?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . FILENAME_HEADER); ?>
 <!-- header_eof //-->
 <!-- body //-->
   <div class="container no-sidebar-page" id="content-container">
    <div class="row mob-row">
    <?php if($content != 'index_default') {  ?>
 	  <div class="small-margin-top hide-on-mobile col-xl-12 col-lg-12 col-md-12 no-padding main-breadcrumb breadcrumb-<?php echo $content; ?>" style="">
 		<?php echo '<ol class="breadcrumb"><li class="active">' . $breadcrumb->trail('&nbsp;&nbsp;/&nbsp;&nbsp;') . '</li></ol>' . "\n"; ?>
 	  </div>
 	 <?php } ?>
      <div id="content-center-container" class="col-md-12 col-lg-12 col-sm-12 col-12">
		<?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/body.php'); ?>
      </div>
    </div>
   </div>
 <!-- content_eof //-->
 <!-- body_eof //-->
 <!-- footer //-->
 <?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME .'/'.FILENAME_FOOTER); ?>
<!-- footer_eof //-->