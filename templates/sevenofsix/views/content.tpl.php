<?php
/* No Header, Footer, Left and Right nav theme*/
?>
<!-- body //-->
<div class="container no-sidebar-page" id="content-container">
 <div class="row mob-row">
   <div id="content-center-container" class="col-md-12 col-lg-12 col-sm-12 col-12">
	<?php require(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/body.php'); ?>
   </div>
 </div>
</div>
<!-- body_eof //-->
