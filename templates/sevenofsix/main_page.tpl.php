<?php
/*
  $Id: main_page.tpl.php,v 1.0 2020/06/23 00:18:17 devidash Exp $

  Loaded Commerce, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce
  Copyright (c) 2016 CRE Loaded

  Released under the GNU General Public License
*/
$site_branding_query = tep_db_query("SELECT * FROM " . TABLE_BRANDING_DESCRIPTION . " WHERE language_id = " . $languages_id);
$site_brand_info = tep_db_fetch_array($site_branding_query);

//Load the template JS Files
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/angular/angular.min.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/jquery-3.4.1.min.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/jquery.inputmask.bundle.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/popper.min.js');

$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/bootstrap/js/bootstrap.min.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/bootstrap/bootstrap-datepicker.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/bootstrap/js/bootstrap-fileinput.js');

$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/respond.min.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/jquery.loadmask.js');

$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/custom.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/jstree.min.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/carousel.min.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/megnor.min.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/jquery.custom.min.js');

$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/jquery.cardcheck.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/scrollBar.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/jquery/jquery.menu-aim.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/fancyBox/jquery.fancybox.js');
$obj_catalog->set_header_js_file(DIR_WS_TEMPLATES . 'default/library/general/lc-general.js');

$obj_catalog->set_footer_js_file(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/js/footer.js');

//Load the template CSS Files
$obj_catalog->set_header_css_file(TEMPLATE_STYLE);
$obj_catalog->set_header_css_file(DIR_WS_TEMPLATES . 'default/library/bootstrap/css/bootstrap.min.css');
$obj_catalog->set_header_css_file(DIR_WS_TEMPLATES . 'default/library/bootstrap/css/bootstrap-datepicker.css');
$obj_catalog->set_header_css_file(DIR_WS_TEMPLATES . 'default/css/font-awesome.css');
$obj_catalog->set_header_css_file(DIR_WS_TEMPLATES . 'default/library/fancyBox/jquery.fancybox.css?v=2.1.5', 'media="screen"');
$obj_catalog->set_header_css_file(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/css/stylesheet.css');
$obj_catalog->set_header_css_file(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/css/lightbox.css');
$obj_catalog->set_header_css_file(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/css/carousel.css');
$obj_catalog->set_header_css_file(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/css/template.css');

if($content == 'checkout' || $content == 'address_book_process' || $content == 'shopping_cart') {
  $obj_catalog->set_header_css_file(DIR_WS_TEMPLATES .'default/library/select2/select2.min.css');
  $obj_catalog->set_header_js_file(DIR_WS_TEMPLATES .'default/library/select2/select2.min.js');
}
?>
<!DOCTYPE html>
<html <?php echo HTML_PARAMS; ?>>
<head>
<base href="<?php echo (($request_type == 'SSL' || (isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && $_SERVER["HTTP_X_FORWARDED_PROTO"] == 'https')) ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>">
<?php
if ( file_exists(DIR_WS_INCLUDES . FILENAME_HEADER_TAGS) ) {
  require(DIR_WS_INCLUDES . FILENAME_HEADER_TAGS);
} else {
?>
<title><?php echo TITLE ?></title>
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="<?php echo DIR_WS_TEMPLATES . TEMPLATE_NAME . '/'?>favicon.ico">
<?php
// RCI code start
echo $cre_RCI->get('stylesheet', 'cre65ats');
echo $cre_RCI->get('global', 'head');
// RCI code eof

//Catalog Hooks
echo $obj_catalog->printHeaderJSFile();
echo $obj_catalog->printHeaderCSSFile();
echo $obj_catalog->print_header_code();
// RCI code start
echo $cre_RCI->get('global', 'headbottom');
// RCI code eof
if (isset($javascript) && file_exists(DIR_WS_JAVASCRIPT . basename($javascript))) { require(DIR_WS_JAVASCRIPT . basename($javascript)); }
if(trim($site_brand_info['custom_css']) != ''){
	echo "<style>\n". trim($site_brand_info['custom_css']) ."\n</style>\n";
}
?>
</head>
<?php
// RCO start
if ($cre_RCO->get('mainpage', 'body') !== true) {
  echo '<body>' . "\n";
}
// RCO end
//include languages if avaible for template
echo $cre_RCI->get('global', 'bodytop');
?>
<div id="loaded-wrapper" class="loadedcommerce-main-wrapper" ng-app="lccart" ng-controller="catalog">
<?php
// RCI top
echo $cre_RCI->get('mainpage', 'top');

//Set the page Views
$nosidebar_pages = array('shopping_cart', 'checkout', 'checkout_processing');
$article_pages = array('articles', 'article_search', 'article_info', 'articles_new');
$noright_pages = array('index_nested', 'index_products', 'product_info', 'specials', 'advanced_search_result');
$noleft_pages = array();
$content_pages = array(); //No header, footer, left and right
if($content == 'index_default') {
	$view_file = 'views/no_sidebar.tpl.php';
} else if(in_array($content, $nosidebar_pages)) {
	$view_file = 'views/no_sidebar.tpl.php';
} else if(in_array($content, $article_pages)) {
	$view_file = 'views/articles.tpl.php';
} else if(in_array($content, $noleft_pages)) {
	$view_file = 'views/no_left.tpl.php';
} else if(in_array($content, $noright_pages)) {
	$view_file = 'views/no_right.tpl.php';
} else if(in_array($content, $content_pages)) {
	$view_file = 'views/content.tpl.php';
} else {
	$view_file = 'views/'. $content .'.tpl.php';
}

if(!file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/'.$view_file))
	$view_file = 'views/default.tpl.php';
require_once(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/'.$view_file);
?>
</div>
<?php
$jsVars = array('page_content'=>$content, 'listing_type'=>PRODUCT_LIST_CONTENT_LISTING, 'list_image_height'=>PRODUCTS_LISTING_IMAGE_HIEGHT, 'list_image_wifth'=>PRODUCTS_LISTING_IMAGE_WIDTH, 'small_image_height'=>SMALL_IMAGE_HEIGHT, 'small_image_width'=>SMALL_IMAGE_WIDTH, 'get_column_css'=>get_column_css());
echo "<script language=\"javascript\">lcPAGEVARS = ". json_encode($jsVars) ."</script>";
//Catalog Hooks
echo $obj_catalog->printFooterJSFile();
echo $obj_catalog->printFooterCSSFile();
// RCI global footer
echo $cre_RCI->get('global', 'footer');
?>
</body>
</html>