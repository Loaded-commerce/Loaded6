<?php
/*
  $Id: footer.php,v 1.0 2008/06/23 00:18:17 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2008 AlogoZone, Inc.
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
if (DOWN_FOR_MAINTENANCE =='false' || DOWN_FOR_MAINTENANCE_FOOTER_OFF =='false') {
// RCI top
echo $cre_RCI->get('footer', 'top');
?>
<div id="footer">
 <hr style="border-top: 3px solid #428BCF !important">
 <div class="container">
	<div class="row footer-desktop">
		<div class="col-sm-3 col-lg-3 col-md-2 col-xl-3 footer-column" style="padding-right: 0px;">
			<?php
			cre_cds_footer(footer_id_five);
			echo '<div class="footer-logobox" >'.cre_site_branding_rspv('logo').'</div>';
			?>
		</div>
		<div class="col-sm-3 col-lg-3 col-md-3 footer-column"> <?php cre_cds_footer(footer_id_two); ?> </div>
		<div class="col-sm-3 col-lg-3 col-md-3 footer-column"> <?php cre_cds_footer(footer_id_three); ?> </div>
		<div class="col-sm-3 col-lg-3 col-md-4 large-margin-bottom footer-column" style="color:#343a40 !important;"> <?php cre_cds_footer(footer_id_four); ?> </div>
		<div class="col-medmargin-left small-padding-left margin-right small-padding-right"></div>
	</div>
	<!-- footer acccordian start //-->
	<div class="footer_accordian_mobile_show heading_hide_in_desktop">
		<div class="accordion show-on-mobile">
			  <div class="col-sm-3 col-lg-3 text-center footer-block" style="">
				<?php
				if(defined('footer_id_one'))
				{
					$box_string .= '<h4 class="line3 center standard-h4title"><span> &nbsp; </span></h4>';
					$box_array = cre_get_box_array(footer_id_one);
					reset($box_array);
				    foreach($box_array as $subkey => $subvalue) {
						$title = cre_build_listing_link($subvalue);
						if ($subvalue['type'] == 'c') {
							$blurb = cre_get_category_blurb($subvalue['ID']);
							echo cre_build_listing_link($subvalue, true) . '<br>';
						}
					}
				}
				?>
			  </div>
			  <div class="col-sm-12 col-lg-3 col-12 item footer-mobile-block">
				<div class="heading show-on-mobile">OFFER <i class="fa fa-chevron-circle-up" style="float:right;margin-top:12px;font-size:25px;"></i><i class="fa fa-chevron-circle-down" style="float:right;margin-top:12px;"></i></div>
				<div class="content"> <?php cre_cds_footer(footer_id_two); ?> </div>
			  </div>
			  <div class="col-sm-12 col-lg-3 col-12 item footer-mobile-block">
				<div class="heading show-on-mobile">TERMS AND CONDITIONS <i class="fa fa-chevron-circle-up" style="float:right;margin-top:12px;font-size:25px;"></i><i class="fa fa-chevron-circle-down" style="float:right;margin-top:12px;"></i></div>
				<div class="content"> <?php cre_cds_footer(footer_id_three); ?> </div>
			  </div>
			  <div class="col-sm-12 col-lg-3 col-12 item footer-mobile-block">
				<div class="heading show-on-mobile">LOADED COMMERCE STORE <i class="fa fa-chevron-circle-up" style="float:right;margin-top:12px;font-size:25px;"></i><i class="fa fa-chevron-circle-down" style="float:right;margin-top:12px;"></i></div>
				<div class="content"> <?php cre_cds_footer(footer_id_four); ?> </div>
			  </div>
		</div>
	</div>
	<!-- footer acccordian end //-->
	<p class="margin-top text-center"  style="color:#343a40 !important;"> Copyright <?php echo date("Y") . " &copy; " . STORE_NAME;?><br> </p>
</div>
<?php
	//Show Footer Advertisements
	if ($banner = tep_banner_exists('dynamic', 'googlefoot')) {
	  echo '<div class="row">'. tep_display_banner('static', $banner) .'</div>';
	}
	// RCI bottom
	echo $cre_RCI->get('footer', 'bottom');
}
?>
