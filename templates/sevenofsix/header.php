<?php
/*
$Id: header.php,v 1.0 2019/06/23 00:18:17 datazen Exp $

Loaded Commerce, Open Source E-Commerce Solutions
http://www.loadedcommerce.com

Copyright (c) 2019 Loaded Commerce

Released under the GNU General Public License
*/

if (DOWN_FOR_MAINTENANCE_HEADER_OFF == 'false') {
?>
    <!--header.php start-->
    <div class="topnav">
        <div class="container topnav-container">
        	<div class="pull-left topnav-left">
        	    <ul class="phone-menu nav-item pull-left no-margin-bottom">
        	    	<li>
        	    	<?php
						if(trim($site_brand_info['store_brand_support_phone']) != "") {
							echo '<span class="show-on-mobile header-fa-icons"><i data-content="'. $site_brand_info['store_brand_support_phone'].'" data-toggle="popover" data-placement="bottom" title="" style="padding:5px;margin-right:2px" class="fa fa-phone fa-lg fa-sales-phone small-margin-bottom cursor-pointer" data-original-title="Sales Phone" id="example2"></i></span>';
						} ?>
					</li>
        	    	<li>
        	    	<?php
						if(trim($site_brand_info['store_brand_support_email']) != "") {
						   echo '<span class="show-on-mobile header-fa-icons"><i data-content="'. $site_brand_info['store_brand_support_email'].'" data-toggle="popover" data-placement="bottom" title="" style="padding:5px;margin-right:2px" class="fa fa-envelope fa-lg fa-sales-email small-margin-bottom cursor-pointer" data-original-title="Sales Email" id="example1"></i></span>';
						} ?>
					</li>
        	    </ul>
        	</div>
            <div class="pull-right margin-right">
                <ul class="cart-menu nav-item pull-right no-margin-bottom">
                    <li class="dropdown">
                        <a href="<?php echo tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'); ?>">
                            <span class="hide-on-mobile-portrait"><?php echo MENU_TEXT_CHECKOUT ?></span>
                        </a>
                    </li>
                </ul>

                <ul class="cart-menu nav-item pull-right no-margin-bottom">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-shopping-cart white small-margin-right"></i> ({{itemCount}}) <span class="hide-on-mobile-portrait"><?php echo MENU_TEXT_ITEMS ?></span> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu cart-dropdown" style="margin-left:-31px">
                            <!-- going to change -->
                            <li style="white-space: nowrap;">
                                <a href="<?php echo tep_href_link(FILENAME_SHOPPING_CART, '', 'SSL'); ?>"><?php  echo MENU_TEXT_ITEMS; ?> | {{cartContentsShort}}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
			<?php if(defined('FILENAME_COMPARE')) { ?>
			    <ul class="cart-menu nav-item pull-right no-margin-bottom header-compare-menu" style="<?php echo (($count_compare > 0)?'display:unset':'display:none'); ?>">
                    <li class="dropdown">
                        <a class=""  href="<?php echo tep_href_link(FILENAME_COMPARE); ?>">
                            <i class="fa fa-exchange white small-margin-right"></i><span class="hide-on-mobile-portrait">Compare </span><span id="comp_click_header"> (<?php echo $count_compare; ?>)</span>
                        </a>
                    </li>
				</ul>
			<?php } ?>

                <ul class="account-menu nav-item pull-right no-margin-bottom">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user white small-margin-right"></i> <i class="fa fa-bars fa-bars-mobile"></i><span class="hide-on-mobile-portrait"><?php echo HEADER_TITLE_MY_ACCOUNT; ?></span> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu account-dropdown">
                            <?php
                            if ($obj_catalog->lc_checklogin()) {
                                echo '<li><a href="' . tep_href_link(FILENAME_LOGOFF, '', 'SSL') , '">' . MENU_TEXT_LOGOUT . '</a></li>
									<li><a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') , '">' . HEADER_TITLE_MY_ACCOUNT .'</a></li>
									<li><a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') , '">My Order</a></li>
									<li><a href="' . tep_href_link(FILENAME_WISHLIST, '', 'SSL') , '">My Wishlist</a></li>
									<li><a href="' . tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL') , '">' . IMAGE_BUTTON_ADDRESS_BOOK .'</a></li>
									<li><a href="' . tep_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL') , '">Change Password</a></li>';
                        } else {
                                echo '<li><a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '">' . MENU_TEXT_LOGIN . '</a></li>
									<li><a href="' . tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL') . '">'. IMAGE_BUTTON_CREATE_ACCOUNT .'</a></li>
									<li><a href="' . tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL') . '">'. TEXT_PASSWORD_FORGOTTEN .'</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container container-pageheader" style="">
            <div class="row mid-margin-bottom">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                </div>
                <div class="col-4 col-sm-4 col-md-3 col-lg-3 col-xl-3 logo-div">
                    <div class="logo">
                        <?php echo cre_site_branding_rspv('logo');?>
                    </div>
					<?php
						if(trim($site_brand_info['store_brand_slogan']) != "") {
						 echo '<span class="slogan" style="font-size:13px;">'.$site_brand_info['store_brand_slogan'] .'</span>';
						}
					?>
                </div>
                <div class="col-8 col-sm-8 col-md-4 col-lg-5 fix_gap">
                    <?php
                       echo tep_draw_form('search', tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT, '', 'SSL'), 'get', 'role="form" class="form-inline" id="search"');
                    ?>
                        <div class="col-md-12 input-group large-margin-left searchbox">
                            <input type="text" class="form-control input-lg" style="width: 100% !important; z-index: 0 !important; border-radius:0px"  placeholder="Search Our Catalog" name="keywords" value="<?php echo htmlspecialchars(StripSlashes(@$_GET["keywords"]));?>">
                            <span class="input-group-addon large-padding-left" style="border-radius:0px; background-color: #24529E !important; cursor:pointer;position:absolute;right:0px;padding:6px;padding-right:6px !important;" onclick="document.getElementById('search').submit();">
                                <i class="fa fa-search serchicon" style="font-size: 22px; color:#fff;"></i>
                            </span>
                        </div>
                    </form>
                </div>

                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 hide-on-mobile pl-0 mob-phone-head">
                    <div class="text-right">
                        <ul class="list-inline rgt-list support">
                            <?php
                            if(trim($site_brand_info['store_brand_support_phone']) != "") {
                                echo '<li><i class="fa fa-phone supporticon"></i> <a href="tel:'. $site_brand_info['store_brand_support_phone'] . '">' . $site_brand_info['store_brand_support_phone'] . '</a></li>';
                            }
                            if(trim($site_brand_info['store_brand_support_email']) != "") {
                                echo '<li><i class="fa fa-envelope supporticon"></i> <a href="mailto:'. $site_brand_info['store_brand_support_email'] . '">' . $site_brand_info['store_brand_support_email'] . '</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>


		<nav class="navbar navbar-expand-md navbar-dark row nav-header-menu ">
		  <a class="navbar-brand navbar-toggler" href="#" data-toggle="collapse" data-target="#collapsibleNavbar">Menu</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="nav  navbar-nav">
				<li><a href="<?php echo tep_href_link(FILENAME_DEFAULT); ?>"><?php echo MENU_TEXT_HOME;?></a></li>
				   <li class="mega-popup nav dropdown"><!--start of megamenu li-->
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">	<span >Products<b class="caret"></b></span></a>
					<?php
						if(count($category_tree) > 0) {
					?>
					<ul class="dropdown-menu new-mega-menu" role="menu">
						<?php
							foreach($category_tree as $mega_menu) {
							 $cPath_new = $mega_menu['cPath'];
							 $num_sub = isset($mega_menu['sub'])?count($mega_menu['sub']):0;
						?>
							<li data-submenu-id="submenu-<?php echo $mega_menu['categories_id']; ?>">
								<a href="<?php echo tep_href_link(FILENAME_DEFAULT, $cPath_new);?>"><?php echo $mega_menu['categories_name']; ?><?php echo (($num_sub > 0)?'<i class="fa fa-angle-right float-right" style="line-height:2;"></i>':''); ?></a>
								<?php
								if($num_sub > 0){
								?>
									<div id="submenu-<?php echo $mega_menu['categories_id']; ?>" class="popover">
								    <h2 class="popover-title hell <?php echo $mega_menu['categories_image']; ?>"><?php echo $mega_menu['categories_name']; ?><?php if($mega_menu['categories_banner_image'] != ''){ ?><span class="pull-right"><?php echo tep_image(DIR_WS_CATEGORIES . $mega_menu['categories_banner_image'], $mega_menu['categories_name'], '', '70', 'class="img-menu-banner"');?><span><?php } ?></h2>
									<div style="clear:both;"></div>
									<div class="popover-content row">
									<?php
										foreach($mega_menu['sub'] as $inner_mega_menu) {
										$cPath_new = $inner_mega_menu['cPath'];
									?>
										 <div class="col-lg-4 col-sm-4 col-xl-4 col-md-4 megasub-container">
											<p class="megasub-heading"><a href="<?php echo tep_href_link(FILENAME_DEFAULT, $cPath_new);?>"><?php echo $inner_mega_menu['categories_name']; ?></a></p>
											<?php
												if(isset($inner_mega_menu['sub'])) {
												  echo '<ul class="style-type yesgivdot">';
												  foreach($inner_mega_menu['sub'] as $inner_submega_menu) {
													$cPath_new = $inner_submega_menu['cPath'];
													echo '<li><a href="'.tep_href_link(FILENAME_DEFAULT, $cPath_new).'">'.$inner_submega_menu['categories_name'].'</a></li>';
												  }
												  echo '</ul>';
												}
										   ?>
										 </div>
									<?php } ?>
									</div>
								</div>
							   <?php } ?>
							</li>
						<?php
						}
						?>
						</ul>
					<?php
					}
					?>
				  </li><!--end of megamenu li-->
					  <li class="nav-inner show-on-mobile show-on-tab" style="width:100%">
						<div class="">
							<div id="menu" class="main-menu">
							 <div class="nav-responsive"><span>Products</span><div class="expandable"></div></div>
							  <ul class="main-navigation product-div" style="display:none;">
									  <?php
									foreach($category_tree as $categories) {
										$cPath_new = $categories['cPath'];
										$num_second_level = isset($categories['sub'])?count($categories['sub']):0;
									?>
									<li><a href="<?php echo tep_href_link(FILENAME_DEFAULT, $cPath_new);?>" class="activSub"><?php echo $categories['categories_name'];?> </a>
										<?php
											//second level
											if($num_second_level > 0){
												echo '<ul>';
												foreach($categories['sub'] as $sub_categories) {
													$cPath_new_sub = $sub_categories['cPath'];
													$num_third_level = isset($sub_categories['sub'])?count($sub_categories['sub']):0;
												   echo '<li><a  href="'.tep_href_link(FILENAME_DEFAULT, $cPath_new_sub).'">'.$sub_categories['categories_name'].'</a>';
														//third level
														if($num_third_level > 0){
															 echo '<ul class="cat-submenu">';
															foreach($sub_categories['sub'] as $sub_trd_categories) {
		  													   $cPath_new_sub = $sub_trd_categories['cPath'];
															   echo '<li><a href="'.tep_href_link(FILENAME_DEFAULT, $cPath_new_sub).'">'.$sub_trd_categories['categories_name'].'</a></li>';
															}
														  echo '</ul>';
														}
												  echo '</li>';
												}
												echo '</ul>';
											}
										?>
									</li>
									<?php
								 }
								 ?>
								</ul>
							 </div>
							</div>

				 </li>
				<?php
				//CDS NAV
				$cds_nav_qry = tep_db_query("select categories_id from " . TABLE_PAGES_CATEGORIES . " where categories_parent_id = 0 and categories_in_menu = 1 and categories_status = '1'");
				while($cds_nav = tep_db_fetch_array($cds_nav_qry)){
					echo cre_cds_nav($cds_nav['categories_id'] );
				}
				?>

			</ul>
		  </div>
		</nav>
      </div>
    </div>
<?php
}
?>
<!-- header_eof //-->
