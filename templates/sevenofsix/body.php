<?php
if(file_exists(TEMPLATE_FS_CUSTOM_INCLUDES . FILENAME_WARNINGS)){
	require(TEMPLATE_FS_CUSTOM_INCLUDES . FILENAME_WARNINGS);
} else {
	require(DIR_WS_INCLUDES . FILENAME_WARNINGS);
}

if (isset($content_template) && file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/content/'.  basename($content_template))) {
require(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/content/' . $content . '.tpl.php');
} else if (file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/content/' . $content . '.tpl.php')) {
require(DIR_WS_TEMPLATES . TEMPLATE_NAME.'/content/'. $content . '.tpl.php');
} else if (isset($content_template) && file_exists(DIR_WS_CONTENT . basename($content_template)) ){
require(DIR_WS_CONTENT . basename($content_template));
} else {
if(defined('MODULE_ADDONS'))
	require('addons/lc_' . MODULE_ADDONS.'/catalog/templates/content/'.$content . '.tpl.php');
else
	require(DIR_WS_CONTENT . $content . '.tpl.php');
}
?>