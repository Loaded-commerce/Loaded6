var $menu = $(".dropdown-menu");
$menu.menuAim({
	activate: activateSubmenu,
	deactivate: deactivateSubmenu
});
function activateSubmenu(row) {
	var $row = $(row),
		submenuId = $row.data("submenuId"),
		$submenu = $("#" + submenuId),
		height = $menu.outerHeight(),
		width = $menu.outerWidth();

	$submenu.css({
		display: "block",
		top: -1,
		left: width + 20,  // main should overlay submenu
		height: height + 'auto'  // padding for main dropdown's arrow
	});
	$row.find("a").addClass("maintainHover");
}
function deactivateSubmenu(row) {
	var $row = $(row),
		submenuId = $row.data("submenuId"),
		$submenu = $("#" + submenuId);

	// Hide the submenu and remove the row's highlighted look
	$submenu.css("display", "none");
	$row.find("a").removeClass("maintainHover");
}
$(".dropdown-menu li").click(function(e) {
	e.stopPropagation();
});
$(".popover").css("display", "none");
$(document).click(function() {
	$("a.maintainHover").removeClass("maintainHover");
});
/*Footer mobile menu Accordion Start*/
$('.accordion .item .heading').click(function() {
	var a = $(this).closest('.item');
	var b = $(a).hasClass('open');
	var c = $(a).closest('.accordion').find('.open');
	if(b != true) {
		$(c).find('.content').slideUp(200);
		$(c).removeClass('open');
	}
	$(a).toggleClass('open');
	$(a).find('.content').slideToggle(200);
});
/*Footer mobile menu Accordion End*/
function list_view()
{
   var psize  = lcPAGEVARS.list_image_height;
   if(psize == ''){
      var psize  = lcPAGEVARS.list_image_wifth;
   }
	 //event.preventDefault();
	 $('#products .product-listing-module-name').removeClass('col-sm-12 col-lg-12 col-12');
	 $('#products .product-listing-module-name').addClass('col-sm-6 col-lg-6 col-12');
	 $('#products .bottom-cover-div').removeClass('col-lg-12 col-xl-12 col-sm-12 col-12');
	 $('#products .bottom-cover-div').addClass('col-lg-3 col-xl-3 col-sm-12 col-12');
	 $('#products .cart-button').removeClass('col-lg-12 col-xl-12 col-sm-12 col-12');
	 $('#products .itembox').addClass('col-12 col-md-12 col-lg-12 listitems');
	 $('#products .itembox').removeClass(lcPAGEVARS.get_column_css);
	 $('#products .item-outer').addClass('col-12 col-md-12 col-lg-12 listitemsouter');
	 $('#products .item-outer').removeClass(lcPAGEVARS.get_column_css);
	 $('#products .thumb-img').addClass('col-lg-3 col-md-3 col-12');
	 $('#products .thumb-img').removeClass('col-sm-12 col-xl-12 col-lg-12 col-12');
	 $(".product-listing-module-manufacturer").css({"text-align":"left", "padding":"0px" });
	 $(".listitems").css("height","auto");
	 $(".thumb-img img").css({"height":psize, "padding":"0px" });
}

function grid_view()
{
   var psize  = lcPAGEVARS.small_image_height;
   if(psize == ''){
      var psize  = lcPAGEVARS.SMALL_IMAGE_WIDTH;
   }
  //event.preventDefault();
  $('#products .price_mainpage').removeClass('col-lg-4 col-md-4 col-12');
  $('#products .bottom-cover-div').removeClass('col-lg-3 col-xl-3 col-sm-12 col-12');
  $('#products .bottom-cover-div').addClass('col-lg-12 col-xl-12 col-sm-12 col-12');
  $('#products .cart-button').addClass('col-lg-12 col-xl-12 col-sm-12 col-12');
  $(".itembox").removeAttr("style");
  $(".product-listing-module-manufacturer").removeAttr("style");
  $(".product-listing-module-manufacturer").css({"text-align":"center", "height":"25px" });
  $('#products .itembox').removeClass('col-12 col-md-12 col-lg-12 listitems');
  $('#products .itembox').addClass('');
  $('#products .item-outer').removeClass('col-12 col-md-12 col-lg-12 listitemsouter');
  $('#products .item-outer').addClass(lcPAGEVARS.get_column_css);
  $('#products .thumb-img').removeClass('col-lg-3 col-md-3');
  $('#products .thumb-img').addClass('col-sm-12 col-xl-12 col-lg-12 col-12');
  $('#products .product-listing-module-name').removeClass('col-sm-8 col-lg-8');
  $('#products .product-listing-module-name').addClass('col-sm-12 col-lg-12 col-12');
  $('#products .price_mainpage').addClass('col-lg-12 col-md-12 col-12');
  $(".thumb-img img").css({"height":psize, "padding":"0px" });
}
$(document).ready(function() {
  var defView = lcPAGEVARS.listing_type;
  if (defView == 'column')
    grid_view();
  if (defView == 'row')
    list_view();
});
