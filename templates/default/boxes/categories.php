<?php
/*
  $Id: categories.php,v 1.2 2008/06/23 00:18:17 datazen Exp $

  Loaded Commerce, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2020 LoadedCommerce

  Released under the GNU General Public License
*/
?>
  <!-- categories_eof //-->
  <div class="box-heading">Categories</div>
  <div class="well box-conten">
	<?php
		foreach($category_tree as $leftCat) {
		 $cPath_new = $leftCat['cPath'];
		 $num_sub = isset($mega_menu['sub'])?count($mega_menu['sub']):0;
		 $activeCSS = (isset($cPath_array) && in_array($leftCat['categories_id'], $cPath_array))?' class="font-weight-bold"':'';
	?>
      <ul class="categorieslink box-information_pages-ul list-unstyled list-indent-large" ><li<?php echo $activeCSS; ?>><a style="color:#5E5E5E;" href="<?php echo tep_href_link(FILENAME_DEFAULT, $cPath_new);?>"><?php echo $leftCat['categories_name']; ?></a></li></ul>
	<?php
		if(isset($leftCat['sub']) && (isset($cPath_array) && in_array($leftCat['categories_id'], $cPath_array))) {
		   foreach($leftCat['sub'] as $leftCat_sub) {
			  $cPath_new = $leftCat_sub['cPath'];
			  $activeCSS = (isset($cPath_array) && in_array($leftCat_sub['categories_id'], $cPath_array))?' font-weight-bold':'';
		      echo '<ul class="categorieslink box-information_pages-ul list-unstyled list-indent-large" ><li class="pl-2'. $activeCSS .'"><a style="color:#5E5E5E;" href="'. tep_href_link(FILENAME_DEFAULT, $cPath_new) .'">'. $leftCat_sub['categories_name'].'</a></li></ul>';
				if(isset($leftCat_sub['sub'])) {
				   foreach($leftCat_sub['sub'] as $leftCat_sub2) {
					  $cPath_new = $leftCat_sub2['cPath'];
					  $activeCSS = (isset($cPath_array) && in_array($leftCat_sub2['categories_id'], $cPath_array))?' font-weight-bold':'';
					  echo '<ul class="categorieslink box-information_pages-ul list-unstyled list-indent-large" ><li class="pl-4'. $activeCSS .'"><a style="color:#5E5E5E;" href="'. tep_href_link(FILENAME_DEFAULT, $cPath_new) .'">'. $leftCat_sub2['categories_name'].'</a></li></ul>';
				   }
				}
		   }
		}
	}
	?>
  </div>
