<?php
/*
  $Id: articles.php,v 1.2 2008/06/23 00:18:17 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2008 AlogoZone, Inc.
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
$articles = new box_articles();

?>
<!-- articles //-->

<div class="box-heading"><?php echo BOX_HEADING_ARTICLES_ARCHIVE; ?></div>
<div class="well box-conten">
<?php $article_query = tep_db_query("SELECT LPAD(MONTH(articles_date_added), 2, '0') AS MONTH,YEAR(articles_date_added) AS YEAR, COUNT(*) as total FROM articles GROUP BY MONTH ORDER BY YEAR DESC, MONTH DESC");
while($article_data=tep_db_fetch_array($article_query)){

$monthNum = sprintf("%02s", $article_data['MONTH']);

$monthName = date("F", mktime(null, null, null, $monthNum));
if($article_data['YEAR'] == '0')
{
$year = '';
}else {
$year = $article_data['YEAR'];
}
?>
<p style="color:#ff0000;margin-bottom:5px;"><i class="fa fa-circle-o" style="font-size: 9px;vertical-align: middle;color:#000000;"></i>&nbsp;

<?php echo '<a href="' . tep_href_link(FILENAME_ARTICLES,'month='.$article_data['MONTH'].'&year='.$article_data['YEAR'],'NONSSL') . '" style="color:#ff0000;margin-bottom:5px;font-weight:200;">'; ?><?php echo($monthName.'&nbsp;&nbsp;'.$year.'&nbsp;&nbsp;<span style="color:#000000;">('.$article_data['total']).')</span><br/>'; ?><?php echo '</a>';?>


</p>
<?php } ?>

</div>
<!-- articles_eof //-->

