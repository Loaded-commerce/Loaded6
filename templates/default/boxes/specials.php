<?php
/*
  $Id: specials.php,v 1.2 2008/06/23 00:18:17 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2008 AlogoZone, Inc.
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
$specials = new box_specials();

if (count($specials->rows) > 0) {
?>
  <!-- specials //-->
  <!-- shop_by_price eof//-->
  <div class="box-heading"><?php echo  BOX_HEADING_SPECIALS ; ?></div>
    <div class="well box-conten" style="border:3px solid #E3E3E3;text-align:center;">
      <?php
      foreach ($specials->rows as $product_specials22) {
        $product_specials22_id = $product_specials22['products_id'];
        $product_specials22_image = tep_get_products_image($product_specials22['products_id']);
        $product_specials22_name = tep_get_products_name($product_specials22['products_id']);
        $pf->loadProduct($product_specials22['products_id'],$languages_id);
        $special_random_price = $pf->getPriceStringShort('r');
		 echo '<div class="special-box small-margin-bottom">
		 		 <div class="boxsale">Sale</div><div class="col-lg-12 col-12 col-sm-12 col-xl-12 no-padding">
		 		    <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $product_specials22_id) . '">' .tep_image(DIR_WS_PRODUCTS . $product_specials22_image, $product_specials22_name, INFOBOX_IMAGE_WIDTH, INFOBOX_IMAGE_HEIGHT, 'class="img-responsive img-thumbnail"') .'</a>
		          </div>

				  <div class="col-lg-12 col-12 col-sm-12 col-xl-12 no-padding special-name-price">
				    <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $product_specials22_id) . '">' .$product_specials22_name . '</a>
					<p class="special-box-price">' . $special_random_price.'</p>
				  </div>
		      </div>';
        }
      ?>

    </div>
<!-- specials eof//-->
  <?php
}
?>
