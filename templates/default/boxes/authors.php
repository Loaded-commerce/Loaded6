 <?php
/*
  $Id: authors.php,v 1.2 2008/06/23 00:18:17 datazen Exp $
  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com
  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2008 AlogoZone, Inc.
  Copyright (c) 2003 osCommerce
  Released under the GNU General Public License
*/
$authors = new box_authors();
if (count($authors->rows) > 0) {
?>
       <div class="box-heading"><?php echo  BOX_HEADING_AUTHORS ; ?></div>
       <div class="well box-conten">
        <form role="form" class="form-inline no-margin-bottom" name="authors" action="<?php echo tep_href_link(FILENAME_ARTICLES, '', 'NONSSL', false)?>" method="get">
            <?php
        $authors_array = array();
        if (MAX_AUTHORS_LIST < 2) {
          $authors_array[] = array('id' => '', 'text' => PULL_DOWN_DEFAULT);
          }
        foreach ($authors->rows as $authors) {
          $authors_name = ((strlen($authors['authors_name']) > MAX_DISPLAY_AUTHOR_NAME_LEN) ? substr($authors['authors_name'], 0, MAX_DISPLAY_AUTHOR_NAME_LEN) . '..' : $authors['authors_name']);
          $authors_array[] = array('id' => $authors['authors_id'],
                                   'text' => $authors_name);
          }

            echo '<ul class="box-information_pages-ul list-unstyled list-indent-large"><li>' . tep_draw_pull_down_menu('authors_id', $authors_array, (isset($_GET['authors_id']) ? $_GET['authors_id'] : ''), 'onChange="this.form.submit();" size="' . MAX_MANUFACTURERS_LIST . '"class="box-manufacturers-select form-control form-input-width" style="width: 100%"') . tep_hide_session_id() . '</select><li></ul>';
           ?>
       </form>
      </div>

<script>
$(document).ready(function() {
  $('.box-manufacturers-select').addClass('form-input-width');
});
$('.box-manufacturers-selection').addClass('form-group full-width');
$('.box-manufacturers-select').addClass('form-control');
</script>
  <!-- authors_eof //-->
<?php
}
?>