<?php
/*
  $Id: featured.php,v 1.2 2008/06/23 00:18:17 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2008 AlogoZone, Inc.
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
$featured = new box_featured();

if ($featured->row_count > 0) {
?>
  <!-- featured -->
<div class="box-heading"><?php echo  BOX_HEADING_FEATURED ; ?></div>
<div class="well box-conten" >
<div style="text-align:center" style="border:3px solid #E3E3E3;text-align:center;">
<?php
      $featured_product21_id = $featured->rows[0]['products_id'];
      $featured_product21_image = $featured->rows[0]['products_image'];
      $featured_product21_name = tep_get_products_name($featured->rows[0]['products_id']);
      //$featuredsale_box = tep_check_specials_products($featured->rows[0]['products_id']);
      $pf->loadProduct($featured->rows[0]['products_id'],$languages_id);
      $featured_price1 = $pf->getPriceStringShort('r');
      echo '<div class="special-box small-margin-bottom">';
	  if($pf->hasSpecialPrice == 1){
	    echo '<div class="boxsale">Sale</div>';
	  }
      echo '<div class="col-lg-12 col-12 col-sm-12 col-xl-12 no-padding">
      <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $featured_product21_id) . '">' . tep_image(DIR_WS_PRODUCTS . $featured_product21_image, $featured_product21_name, INFOBOX_IMAGE_WIDTH, INFOBOX_IMAGE_HEIGHT, 'class="img-responsive img-thumbnail"') . '</a></div>

      <div class="col-lg-12 col-12 col-sm-12 col-xl-12 no-padding special-name-price"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $featured_product21_id, 'NONSSL') . '">' . $featured_product21_name . '</a>
      <p class="special-box-price">' . $featured_price1.'</p>
      </div></div>';
      ?>
 </div>
</div>

<?php
}
?>
<!-- featured eof//-->
