  <div class="box-heading">My Account Info</div>
  <div class="well box-conten">
  <?php
/*
  $Id: loginbox.php,v 1.2 2008/06/23 00:18:17 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2008 AlogoZone, Inc.
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
require(DIR_WS_LANGUAGES . $language . '/'.FILENAME_LOGINBOX);
if ( ! isset($_SESSION['customer_id']) )  {
  if ( !isset($_SESSION['customer_id']) ) {
    ?>
    <!-- loginbox //-->
        <?php
         if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') || $_SERVER['SERVER_PORT'] == 443) {
        $loginboxcontent = "
        <form name=\"login\" method=\"post\" action=\"" . tep_href_link(FILENAME_LOGIN, 'action=process', 'SSL') . "\">
             <div class=\"form-group\">
                   <label class=\"sr-only\"></label>
                   <input type=\"text\" name=\"email_address\" maxlength=\"96\" size=\"20\" value=\"\" placeholder=\"Email-Address\"  class=\"form-control\">
             </div>
             <div class=\"form-group\">
                   <label class=\"sr-only\"></label>
                      " . tep_draw_password_field('password','' , 'class="form-control" maxlength="40" size="20" autocomplete="off" placeholder="' . MENU_TEXT_PASSWORD . '"') . "
             </div>
			 <div class=\"buttons-set clearfix small-margin-top\">
			   <input type=\"submit\" value=\"Sign In\" class=\"pull-right btn btn-sm btn-primary\">
			 </div>
        </form>";
        }else{
			 echo "<div class=\"clearfix\" style='float:left;'>
             <a  class=\"pull-right btn btn-sm btn-primary sign-in\" href=\"" . tep_href_link(FILENAME_LOGIN, '', 'SSL') . "\" style=''>Sign In</a></div>";

			 echo "<div class=\"clearfix\" >
             <a  class=\"pull-right btn btn-sm btn-primary\" href=\"" . tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL') . "\" style='font-size:15px;' >Register</a>
			 </div>";

        }
      echo '<div style="margin-left:5px">' . $loginboxcontent . '</div>';
        ?>
    <!-- loginbox eof//-->

    <?php
  } else {
    // If you want to display anything when the user IS logged in, put it
    // in here...  Possibly a "You are logged in as :" box or something.
  }
} else {
  if ( isset($_SESSION['customer_id']) ) {
    $pwa_query = tep_db_query("select purchased_without_account from " . TABLE_CUSTOMERS . " where customers_id = '" . $_SESSION['customer_id'] . "'");
    $pwa = tep_db_fetch_array($pwa_query);
    if ($pwa['purchased_without_account'] == '0') {
      ?>
      <!-- loginbox //-->

    <?php /*  <div class="box-header small-margin-bottom small-margin-left">My Account Info</div> */ ?>
				   <?php
						echo '<ul class="box-information_pages-ul listclass" style="list-style:none;margin-left:-25px;margin-top:7px;"><li><a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '">' . LOGIN_BOX_MY_ACCOUNT . '</a></li>';
						echo '<li><a href="' . tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL') . '">' . LOGIN_BOX_ACCOUNT_EDIT . '</a></li>';
						echo '<li><a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') . '">' . LOGIN_BOX_ACCOUNT_HISTORY . '</a></li>';
						echo '<li><a href="' . tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL') . '">' . LOGIN_BOX_ADDRESS_BOOK . '</a></li>';
						echo '<li><a href="' . tep_href_link(FILENAME_ACCOUNT_NOTIFICATIONS, '', 'NONSSL') . '">' . LOGIN_BOX_PRODUCT_NOTIFICATIONS . '</a></li>';
						echo '<li><a href="' . tep_href_link(FILENAME_LOGOFF, '', 'NONSSL') . '">' . LOGIN_BOX_LOGOFF . '</a></li></ul>';
					?>
<!-- loginbox eof//-->
      <?php
    }
  }
}
?>
</div>
