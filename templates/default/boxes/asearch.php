<?php
/*
  $Id: asearch.php,v 1.2 2008/06/23 00:18:17 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2008 AlogoZone, Inc.
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
?>
<!-- asearch //-->
  <script>
  $(document).ready(function() {
    $('.box-manufacturers-select').addClass('form-input-width');
  });
  $('.box-manufacturers-selection').addClass('form-group full-width');
  $('.box-manufacturers-select').addClass('form-control');
</script>
<div class="box-heading">Article Search</div>
     <div class="well box-conten">
		<?php
		$hide = tep_hide_session_id();
		?>
		 <form role="form" class="form-inline no-margin-bottom" name="quick_find_article" action="<?php echo tep_href_link(FILENAME_ARTICLE_SEARCH, '', 'NONSSL', false) ?>" method="get">
		 <?php
			  echo $hide . '<input type="hidden" name="rt" value="core/article_search">
			  <input class="form-control" type="text" required name="keywords" size="10" maxlength="30" value="' . htmlspecialchars(StripSlashes(@$_GET["akeywords"])) . '" placeholder="Article Keyword" style="width:78%;border-bottom-right-radius: 0;border-top-right-radius: 0;">
			  <div class="buttons-set text-center" style="padding:0px;">
				<button type="submit" class="btn cursor-pointer small-margin-right btn-success" style="padding: 5px;border-bottom-left-radius: 0;border-top-left-radius: 0;"> <i class="fa fa-search serchicon" style="font-size: 22px; color:#fff;"></i> </button>
			 </div> <br><div style="text-align:center"><input type="checkbox" name="description">&nbsp;&nbsp;' . BOX_ASEARCH_TEXT . '<br></a></div>';
		 ?>

     </form>
    </div>
<!-- asearch eof//-->