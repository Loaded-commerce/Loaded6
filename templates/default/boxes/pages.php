<?php
/*
  $Id: pages.php,v 2.0 2008/07/08 13:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License

*/
?>
<!-- pages_eof //-->
<div class="box-heading"><?php echo BOX_HEADING_PAGES; ?></div>
<div class="well box-conten">
  <ul class="box-information_pages-ul list-unstyled list-indent-large"><?php echo cre_get_box_string() ;?></ul>
</div>
