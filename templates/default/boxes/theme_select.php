<?php
/*
  $Id: pages.php,v 2.0 2008/07/08 13:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License

*/
?>
<!-- pages_eof //-->
<div class="box-heading"><?php echo BOX_HEADING_TEMPLATE_SELECT; ?></div>
<div class="well box-conten">
    <ul class="box-information_pages-ul list-unstyled list-indent-large">
			<?php
				echo '<form name="template" method="post" action="' . tep_href_link(FILENAME_DEFAULT, '&action=update_template', 'NONSSL') . '">';
				$template_query = tep_db_query("select template_id, template_name from " . TABLE_TEMPLATE . " where active = '1' order by template_name");
			// Display a drop-down
				$select_box = '<select class="form-control" name="template" onChange="this.form.submit();" size="' . MAX_MANUFACTURERS_LIST . '" style="width: 100%">';
				if (MAX_THEME_LIST < 2) {
				  $select_box .= '<option value="">' . PULL_DOWN_DEFAULT . '</option>';
				}
				while ($template_values = tep_db_fetch_array($template_query)) {
				  $select_box .= '<option value="' . $template_values['template_name'] . '"';
				  if ($_GET['template_id'] == $template_values['template_id']) $select_box .= ' SELECTED';
				  $select_box .= '>' . substr($template_values['template_name'], 0, MAX_DISPLAY_MANUFACTURER_NAME_LEN) . '</option>';
				}
				$select_box .= "</select>";
				$select_box .= tep_hide_session_id();
				echo $select_box;
				echo '</form>';
			?>
    </ul>
</div>






