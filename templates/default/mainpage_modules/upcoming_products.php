<?php
/*
  $Id: upcoming_products.php,v 1.1.1.3 2004/04/07 23:42:23 ccwjr Exp $

  LoadedCommerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2018 LoadedCommerce

  Released under the GNU General Public License
*/
?>
<!-- upcoming_products mainpage_modules //-->
<?php
$expected_query_raw= tep_db_query("select p.products_id, pd.products_name, p.products_image, products_date_available, p.products_price as products_price
						 from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd
						 where to_days(products_date_available) >= to_days(now())
						 and p.products_id = pd.products_id
						 and pd.language_id = '" . (int)$languages_id . "' ". $obj_catalog->b2b_filter_sql('p.products_group_access') ."
						 order by rand() limit " . MAX_DISPLAY_UPCOMING_PRODUCTS);
if(tep_db_num_rows($expected_query_raw) > 0)
{
	echo '<div class="col-xl-12 col-lg-12 col-12 col-sm-12 no-padding-left home-module-heading"><h3 class="hm_upcoming_products">'. sprintf(TABLE_HEADING_UPCOMING_PRODUCTS, strftime('%B')) .'</h3></div>';
	while ($rwm_upcoming = tep_db_fetch_array($expected_query_raw))
	{
		$upcoming_column_list = $column_list;
		foreach($upcoming_column_list as $key=>$val){
			if($val == 'PRODUCT_LIST_BUY_NOW')
				$upcoming_column_list[$key] = 'PRODUCT_LIST_DATE_EXPECTED';
		}
	    $duedate= date('m/d/Y', strtotime($rwm_upcoming['date_expected']));
		$product_contents = $obj_catalog->prod_grid_listing($rwm_upcoming, $upcoming_column_list);
		$lc_text = implode( $product_contents);
		echo '<div class="'.$divcolumn.' with-small-padding">
					<div class="col-lg-12 col-xs-12 thumbnail align-center row m-0">'. $lc_text.'</div></div>' ;
					//echo (tep_check_specials_products($rwm_upcoming['products_id'])?'<div class="stickers"> <div class="sale">Sale</div></div>':'');
					//echo (tep_check_specials_products($rwm_upcoming['products_id'])?'<div class="savepercent">'.floatval(get_special_percent($rwm_upcoming['products_id'])).'%</div>':'');


	}
	echo '<div class="clearfix"></div>'."\n";
}
?>
<!--D upcoming_products_eof //-->
