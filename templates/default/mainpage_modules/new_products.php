<?php
/*

  LoadedCommerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2018 LoadedCommerce

  Released under the GNU General Public License
*/
?>
<!-- D mainpages_modules.new_products.php//-->
<?php
$new_products_query = tep_db_query("select distinct
				  p.products_id,
				  p.products_price,
				  p.manufacturers_id,
				  pd.products_name,
				  p.products_tax_class_id,
				  p.products_price,
				  p.products_date_added,
				   p.products_image
				  from (" . TABLE_PRODUCTS . " p
left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id),
" . TABLE_PRODUCTS_DESCRIPTION . " pd
where
p.products_status = '1'
and pd.products_id = p.products_id
and pd.language_id = '" . $languages_id . "'
and DATE_SUB(CURDATE(),INTERVAL " .NEW_PRODUCT_INTERVAL ." DAY) <= p.products_date_added
order by rand(), p.products_date_added desc limit " . MAX_DISPLAY_NEW_PRODUCTS);


if(tep_db_num_rows($new_products_query) > 0)
{
	echo '<h3 class="hm_new_products col-lg-12 col-xl-12 col-md-12 col-12 col-sm-12 no-padding">'. sprintf(TABLE_HEADING_NEW_PRODUCTS, strftime('%B')) .'</h3>';
	while ($new_products = tep_db_fetch_array($new_products_query))
	{
		$product_contents = $obj_catalog->prod_grid_listing($new_products, $column_list);
		$lc_text = implode( $product_contents);
		echo '<div class="'.$divcolumn.' with-small-padding">
				<div class="thumbnail align-center with-small-padding row m-0">'. $lc_text ;
			echo (tep_check_new_products($new_products['products_id'])?'<div class="newstickers">New</div>':'');
			//echo (tep_check_specials_products($new_products['products_id'])?'<div class="stickers"> <div class="sale">Sale</div></div>':'');
			//echo (tep_check_specials_products($new_products['products_id'])?'<div class="savepercent">'.floatval(get_special_percent($new_products['products_id'])).'%</div>':'');
		echo '</div>
			  </div>';
	}
	echo '<div class="clearfix"></div>'."\n";
}
?>
<!-- D new_products_eof //-->
