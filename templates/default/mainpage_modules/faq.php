<?php
/*
  $Id: faq.php,v 1.1.1.1 2008/07/30 23:41:14 wa4u Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

echo '<!--faq mainpage //-->' . "\n";
if(!defined('MAX_DISPLAY_MODULES_FAQ_CATEGORY')) { define('MAX_DISPLAY_MODULES_FAQ_CATEGORY','3');}
if(!defined('MAINPAGE_FAQ_SHOW_CATEGORY_FAQ')) { define('MAINPAGE_FAQ_SHOW_CATEGORY_FAQ','true');}
if(!defined('MAINPAGE_FAQ_NUM_FAQ')) { define('MAINPAGE_FAQ_NUM_FAQ','3');}

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_FAQ);

  if(!isset($_SESSION['sppc_customer_group_id'])) {
  $customer_group_id = 'G';
  } else {
   $customer_group_id = $_SESSION['sppc_customer_group_id'];
  }

   $faq_categories_query = tep_db_query("select ic.categories_id, icd.categories_name,ic.categories_image, icd.categories_description from " . TABLE_FAQ_CATEGORIES . " ic, " . TABLE_FAQ_CATEGORIES_DESCRIPTION . " icd  where icd.categories_id = ic.categories_id and icd.language_id = '" . (int)$languages_id . "' and ic.categories_status = '1'");

    $faq_categories_check = tep_db_num_rows($faq_categories_query);
    if ($faq_categories_query > 0){

		echo '<div class="col-lg-12" style="padding:20px;"><h3 class="no-margin-top">' .TABLE_HEADING_NEW_FAQ .'</h3></div>';
		  $row = 0;
		  $count = 0;
		  while ($faq_categories = tep_db_fetch_array($faq_categories_query)) {

			   if($faq_categories['categories_image'] == '')
			   {
			   $image='<i class="fa fa-folder-open-o" style="color:#ffffff;font-size:30px;"></i>';
			   }else{
			   $image = '<img src="'.DIR_WS_OTHERS.$faq_categories['categories_image'].'" class="img-responsive" height="40">';
			   }
		     ?>
				  <div class="col-lg-3 col-12" style="margin-bottom:5px;">
					 <div class="col-lg-12" style="border:1px solid #DDDDDD;height:250px;padding: 0px;">
					  <div class="col-lg-12" style="height:80px;background-color:#00AAFF;text-align:center;padding: 30px;"><?php echo '<a href="' . tep_href_link(FILENAME_FAQ, 'cID=' . (int)$faq_categories['categories_id']) . '#' . $faq['faq_id'] . '">'.$image .'</a>';?>
					  </div>
					  <p style="padding:10px;text-align: center;"><b><?php echo '<a href="' . tep_href_link(FILENAME_FAQ, 'cID=' . (int)$faq_categories['categories_id']) . '#' . $faq['faq_id'] . '">'. $faq_categories['categories_name'].'</a></b>';?></p>
					  <p style="padding:10px;text-align: center;font-size:13px;overflow: hidden;"><?php echo $faq_categories['categories_description']; ?></p>
					 </div>
				  </div>
			  <?php
				$count++;
				$row ++;
 		   }
       } //check eof
?>

<!--faq mainpage //-->