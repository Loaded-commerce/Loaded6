<?php
/*
  LoadedCommerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2018 LoadedCommerce

  Released under the GNU General Public License

  Featured Products Listing Module
*/
?>
<!--D featured_products-->
<?php
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_FEATURED_PRODUCTS);
 $mainpage_featured2_query = tep_db_query("select distinct p.products_image, p.products_id, pd.products_name
                          	from " . TABLE_PRODUCTS . " p INNER JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON (p.products_id = pd.products_id and pd.language_id = '" . $languages_id . "')
							 where p.products_status = 1 and p.featured = 1". $obj_catalog->b2b_filter_sql('p.products_group_access') ."
							 order by rand(), p.featured_sort ASC, p.products_date_added DESC, pd.products_name limit " . MAX_DISPLAY_FEATURED_PRODUCTS);

if (tep_db_num_rows($mainpage_featured2_query) > 0)
{
	echo '<div class="col-lg-12 col-12 col-sm-12 col-xl-12 no-padding home-module-heading"><h3 class="hm_featured">'. TABLE_HEADING_FEATURED_PRODUCTS .'</h3></div>';
	while ($rwm_featured = tep_db_fetch_array($mainpage_featured2_query))
	{
		$product_contents = $obj_catalog->prod_grid_listing($rwm_featured, $column_list);
		$lc_text = implode( $product_contents);
		echo '<div class="'.$divcolumn.' with-small-padding"><div class="col-lg-12 col-xs-12 thumbnail align-center row m-0">'. $lc_text .'</div></div>';
	}
	echo '<div class="clearfix"></div>'."\n";
}
?>
<!-- featured_products eof -->
