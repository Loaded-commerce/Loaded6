<?php
/*

  Loaded Commerce, Open Source E-Commerce Solutions
  http://www.loadedcommerce.com

  Copyright (c) 2018 osCommerce

  Released under the GNU General Public License
*/
?>

<div id="CarouselModule" class="carousel slide" data-ride="carousel" style="overflow:hidden">
   <div class="carousel-inner" role="listbox" >
		<?php
		$query = tep_db_query("SELECT * FROM " . TABLE_BANNERS . " WHERE banners_group='HOMESLIDER' and status = '1' ORDER BY date_added asc");
		 $ct = 0;
		 while($rec = tep_db_fetch_array($query)) {
		 	$target = (trim($rec['banners_url']) == '')?'':' target="_blank"';
		 ?>
		 <div class="carousel-item <?php echo (($ct == 0)? 'active':'');?>">
		 	 <a href="<?php echo ((trim($rec['banners_url']) == '')?'javascript:;':tep_href_link(FILENAME_REDIRECT, 'action=banner&goto='.$rec['banners_id']));?>"<?php echo $target; ?>><img src="<?php echo DIR_WS_BANNERS.$rec['banners_image']; ?> " alt="<?php echo $rec['banners_title']; ?>" title="<?php echo $rec['banners_title']; ?>" /></a>
		 </div>
		<?php
		$ct++;
		}
		?>
   </div>
       <ol class="carousel-indicators text-right">
       <?php
       for($i=0;$i<$ct;$i++){
       ?>
          <li data-target="#CarouselModule" data-slide-to="<?php echo $i; ?>"></li>

       <?php } ?>
        </ol>
    <!-- Left and right controls -->

  <a class="carousel-control-prev" href="#CarouselModule" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#CarouselModule" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>


</div>
