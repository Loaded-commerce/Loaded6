<?php
/*
  $Id: main_categories.php,v 1.0a 2002/08/01 10:37:00 Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com/

  Copyright (c) 2002 Barreto
  Gustavo Barreto <gustavo@barreto.net>
  http://www.barreto.net/

  Based on: all_categories.php Ver. 1.6 by Christian Lescuyer

  History: 1.0 Creation
     1.0a Correction: Extra Carriage Returns
     1.1  added parameters to change display options -- mdt

  Released under the GNU General Public License

*/
if(COLUMN_COUNT == 4)
	$divcolumn="col-lg-3 col-md-4 col-sm-6 col-6";
elseif(COLUMN_COUNT == 2)
	$divcolumn="col-lg-6 col-md-6 col-sm-6 col-6";
else
	$divcolumn="col-lg-4 col-md-6 col-sm-6 col-6";

$row = 0;
$col = 0;
if(isset($category_tree) && count($category_tree) > 0)
{
	echo '<div class="col-lg-12 col-12 col-sm-12 col-xl-12 no-padding home-module-heading"><h3 class="no-margin-top">'. BOX_HEADING_CATEGORIES_MAIN_PAGE .'</h3></div>';
	foreach ($category_tree as $categories_cat)
	{
		$cpath_cat_new = $categories_cat['cPath'];
		if(defined('HOMEPAGE_MODULES_IMAGE_HEIGHT') && HOMEPAGE_MODULES_IMAGE_HEIGHT != ''){
          $imgdiv_height = HOMEPAGE_MODULES_IMAGE_HEIGHT + '20';
          $imgheight = HOMEPAGE_MODULES_IMAGE_HEIGHT;
        }else{
          $imgdiv_height = SMALL_IMAGE_HEIGHT + '20';
          $imgheight = SMALL_IMAGE_HEIGHT;
        }
		if(defined('HOMEPAGE_MODULES_IMAGE_WIDTH') && HOMEPAGE_MODULES_IMAGE_WIDTH != ''){
          $imgwidth = HOMEPAGE_MODULES_IMAGE_WIDTH;
        }else{
          $imgwidth = SMALL_IMAGE_WIDTH;
        }
        if(trim($categories_cat['categories_image']) == '')
        	$categories_cat['categories_image'] = 'no_image.png';
        
		echo '<div class="'.$divcolumn.' with-small-padding product-listing-module-items">
					<div class="thumbnail align-center large-padding-top">
					 <div class="col-sm-12 col-lg-12 no-padding-left no-padding-right specials-cat " style="height:'.$imgdiv_height.'px">
						<a href="' . tep_href_link(FILENAME_DEFAULT, $cpath_cat_new) . '">
						  ' . tep_image(DIR_WS_CATEGORIES . $categories_cat['categories_image'], $categories_cat['categories_name'], $imgwidth, $imgheight, 'style="object-fit: contain;" class="img-responsive img-thumbnail"') . '
						</a>
					  </div>
					  <div class="col-sm-12 col-lg-12 no-padding-left no-padding-right name-box pt-3" style="">
						<div  class="pageHeading prodname">
						 <a href="' . tep_href_link(FILENAME_DEFAULT, $cpath_cat_new) . '">' . $categories_cat['categories_name'] . '</a>
						</div>
					  </div>
					  <div class="row pricing-row"></div>
					</div>
			   </div>';
	}
	echo '<div class="clearfix"></div>'."\n";
}
?>
<!-- main_categories_eof //-->
