<?php
/*
  $Id: default_specials.php,v 2.0 2003/06/13

  LoadedCommerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2018 LoadedCommerce

  Released under the GNU General Public License
*/
?>
<!-- D default_specials //-->
<?php
$query = "select distinct p.products_id, pd.products_name, p.products_price, p.products_image,s.special_percentage
		  from " . TABLE_PRODUCTS . " p, " . TABLE_SPECIALS . " s, " . TABLE_PRODUCTS_DESCRIPTION . " pd
		  where p.products_status = '1' and p.products_id = s.products_id and
		  pd.products_id = p.products_id and pd.language_id = '" . $languages_id . "' and s.status = '1'
		  ". $obj_catalog->b2b_filter_sql('p.products_group_access') ."
		  order by rand(),  s.specials_date_added DESC limit " . MAX_DISPLAY_SPECIAL_PRODUCTS;
$rs_special = tep_db_query($query);
if(tep_db_num_rows($rs_special) > 0)
{
  echo '<div class="col-xl-12 col-lg-12 col-xs-12 col-sm-12 no-padding-left home-module-heading"><h3 class="hm_special">'. sprintf(TABLE_HEADING_DEFAULT_SPECIALS, strftime('%B')) .'</h3></div>';
  while ($rw_special = tep_db_fetch_array($rs_special)) {

		$product_contents = $obj_catalog->prod_grid_listing($rw_special, $column_list);
		$lc_text = implode( $product_contents);
		echo '<div class="'.$divcolumn.' with-small-padding">
				<div class="col-lg-12 col-xs-12 thumbnail align-center">'. $lc_text .'
					<div class="stickers">
						<div class="sale">Sale</div>
					</div>
		        </div>
			  </div>';

  }
  echo '<div class="clearfix"></div>'."\n";
}
?>
<!-- D default_specials_eof //-->
