<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('passwordforgotten', 'top');
// RCI code eof
?>
<div class="row">
  <div class="col-sm-12 col-lg-12 col-xs-12 no-padding">
	<h1 class="no-margin-top" id="head_title"> <?php echo (empty($sID)?HEADING_TITLE:HEADING_TITLE2);?> </h1>
    <div class="well">
<?php 
if(empty($sID)) {
	echo tep_draw_form('password_forgotten', tep_href_link(FILENAME_PASSWORD_FORGOTTEN, 'action=process', 'SSL'));
?>
  <p class="no-margin-bottom"><?php echo TEXT_MAIN; ?></p>
	<div class="row mt-4">
	  <div class="col-lg-2" style="padding-right:6px;"><?php echo ENTRY_EMAIL_ADDRESS; ?></div>
	  <div class="col-lg-10 form-group  margin-bottom" style="padding-left:4px;"><?php echo tep_draw_input_field('email_address', null, 'placeholder="' . ENTRY_EMAIL_ADDRESS . '" class="form-control"'); ?></div>
	</div>
	<?php
	if (defined('VVC_SITE_ON_OFF') && VVC_SITE_ON_OFF == 'On' && defined('MODULE_ADDONS_LC_CAPTCHA_STATUS') && MODULE_ADDONS_LC_CAPTCHA_STATUS == 'True') {
		if (defined('VVC_PASSWORD_FORGOT_ON_OFF') && VVC_PASSWORD_FORGOT_ON_OFF == 'On') {
			$captcha_instructions = trim($captcha->display_captcha_instructions());
	?>
	<!-- Captcha CODE start -->
	<h3><?php echo $captcha->display_captcha_label(); ?></h3>
	<div class="form-group full-width margin-bottom"><?php echo $captcha->render_captcha_image();?><?php echo $captcha->render_captcha_control('class="form-control"');?></div>
	<?php if($captcha_instructions != '') {?>
	<div class="form-group full-width margin-bottom"><?php echo $captcha_instructions; ?></div>
	<?php } ?>
	<!-- Captcha VERIFY CODE end -->
	<?php }
	}?>

	<div class="btn-set small-margin-top clearfix">
		<?php echo '<button class="pull-right btn btn-lg btn-primary" type="submit">' . IMAGE_BUTTON_CONTINUE . '</button>'; ?>
		<?php echo '<a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '"><button class="pull-left btn btn-lg btn-default" type="button">' .  IMAGE_BUTTON_BACK  . '</button></a>'; ?>

	</div>
</form>
<?php  
} else {
	$secret_id = $sID;
	$js_validation_code = 'function validateForm(val) {
						 var new_password=document.forms["reset_password"]["new_password"].value;
						 var confirm_password=document.forms["reset_password"]["confirm_password"].value;
							 if(new_password == "") {
							 	alert("Please enter your password");
							 	return false;
							 }
							 if(confirm_password == "") {
							 	alert("Please enter your confirmation password");
							 	return false;
							 }
							 if(new_password != confirm_password) {
							 	alert("New password and Confirm password must be same.");
							 	return false;
							 }
						}';
	$obj_catalog->set_footer_js($js_validation_code);				
	unset($js_validation_code);
	echo tep_draw_form('reset_password', tep_href_link(FILENAME_PASSWORD_FORGOTTEN, 'action=update&sID='.$sID, 'SSL'), 'post', 'onsubmit="return validateForm()"');
?>
			<div class="col-lg-6 col-xs-12" style="text-align:left"><b>New Password :</b></div>
			<div class="col-lg-6 col-xs-12" style="padding-bottom:10px;"><input type="password" name="new_password" class="form-control" placeholder="" value=""></div>
			<div class="col-lg-6 col-xs-12" style="text-align:left"><b>Confirm Password : </b></div>
			<div class="col-lg-6 col-xs-12"><input type="password" name="confirm_password" class="form-control" value=""/></div>
			<div class="btn-set small-margin-top clearfix">
				<?php echo '<button class="pull-right btn btn-lg btn-primary" type="submit" style="margin-top:5px;">' . IMAGE_BUTTON_CONTINUE . '</button>'; ?>
				<?php echo '<a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '"><button class="pull-left btn btn-lg btn-default" type="button">' .  IMAGE_BUTTON_BACK  . '</button></a>'; ?>
			</div>
		</form>
<?php  }  ?>
	</div>
  <?php
// RCI code start
echo $cre_RCI->get('passwordforgotten', 'menu');
// RCI code eof
?>
 </div>
</div>
<?php
// RCI code start
echo $cre_RCI->get('passwordforgotten', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>