<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
//echo $cre_RCI->get('productreviews', 'top');
// RCI code eof
?>
<div class="row">
   <h3 class="no-margin-top list-heading">Product Reviews</h3>
   <div class="col-sm-12 col-lg-12">
   		<?php if(isset($_GET['products_id']) && $_GET['products_id'] > 0){ ?>
   			<div class="row">
                <div class="col-sm-6">
                    <div id="mainImage">
						<?php
						  if (tep_not_null($product_info['products_image'])) {
								echo '<a data-toggle="modal" href="#popup-image-modal" class="">' . tep_image(DIR_WS_PRODUCTS .  $product_info['products_image'], $product_info['products_name'], MEDIUM_IMAGE_WIDTH, MEDIUM_IMAGE_HEIGHT, 'class="img-responsive img-thumbnail"') . '</a><br><p class="text-center no-margin-top no-margin-bottom"><a data-toggle="modal" href="#popup-image-modal" class="btn btn-sm normal">Click To Enlarge</a></p>    <div class="modal fade" id="popup-image-modal">
										  <div class="modal-dialog">
											<div class="modal-content">
											  <div class="modal-header">
												<h4 class="modal-title">'.$product_info['products_name'] .'</h4>
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											  </div>
											  <div class="modal-body pop_im">'.tep_image(DIR_WS_PRODUCTS .  $product_info['products_image'], $product_info['products_name'], LARGE_IMAGE_WIDTH, LARGE_IMAGE_HEIGHT, 'class="img-responsive img-thumbnail" ').'
											  </div>
											  <div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">close</button>
											  </div>
											</div>
										  </div>
										</div>
									';
							  }
						?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box" id="productMain">
                        <h1 class="text-left no-margin-top"><?php echo $product_info['products_name'];?></h1>
                        <div class="row">
							<div class="col-lg-6 col-sm-6 col-12">
								<h3 class="text-left priceclass "><strong id="config-price" class="lcpricedisplay"><?php echo $products_price; ?></strong></h3>
							</div>
							<div class="col-lg-6 col-sm-6 col-12">
								<?php
									echo $stock_status;
									if($product_info['products_model'] != '') {
										echo '<p class="text-right" style="margin:0">'. TEXT_MODEL . ':&nbsp; '. $product_info['products_model'] .'</p>';
									}
									$product_manufacturer_query = tep_db_query("select m.manufacturers_id, m.manufacturers_name, m.manufacturers_image, mi.manufacturers_url from " . TABLE_MANUFACTURERS . " m left join " . TABLE_MANUFACTURERS_INFO . " mi on (m.manufacturers_id = mi.manufacturers_id and mi.languages_id = '" . (int)$languages_id . "'), " . TABLE_PRODUCTS . " p  where p.products_id = '" . (int)$product_info['products_id'] . "' and p.manufacturers_id = m.manufacturers_id");
									if (tep_db_num_rows($product_manufacturer_query)) {
									?>
										<p class="text-right hide-on-mobile" style="margin:0">
											<?php
											while ($manufacturer = tep_db_fetch_array($product_manufacturer_query)) {
												echo '<b>'. TEXT_MANUFACTURER . ': </b>&nbsp;<a href="'.tep_href_link(FILENAME_DEFAULT, 'manufacturers_id='.$manufacturer['manufacturers_id']).'">'. $manufacturer['manufacturers_name'].'</a>';
											}
											?>
										</p>
										<?php
									}
								?>
							</div>
                        </div>
						</div>
					</div>
				</div>
			 <?php } ?>
			<div class="clearfix"></div>
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-lg-6 small-margin-top small-margin-bottom">
					<?php if(isset($_GET['products_id']) && $_GET['products_id'] > 0){ ?>
						<a onclick="javascript:quick_view(<?php echo $product_info['products_id']; ?>)" href="javascript:"><?php echo tep_template_image_button('button_in_cart.gif', QUICK_VIEW)?></a> &nbsp;
						<?php if(check_products_have_options($product_info['products_id'])){ ?>
							<a class="btn btn-sm cursor-pointer small-margin-right btn-success" href="<?php echo tep_href_link(FILENAME_PRODUCT_INFO, 'products_id='.$product_info['products_id']) ;?>"><?php echo VIEW_MORE; ?></a>
						<?php } else { ?>
							<a href="<?php echo tep_href_link(basename($_SERVER['PHP_SELF']), tep_get_all_get_params(array('action','cPath','products_id')) . 'action=buy_now&products_id=' . $product_info['products_id'] . '&cPath=' . tep_get_product_path($product_info['products_id']));?>"><?php echo tep_template_image_button('button_in_cart.gif', ADD_TO_CART)?></a>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-xs-6 col-sm-6 col-lg-6 small-margin-top small-margin-bottom">
					<?php
						if(isset($_GET['products_id']) && $_GET['products_id'] > 0){
							echo '<a href="javascript:" onclick="writereview('.$_GET['products_id'].');" class="btn cursor-pointer small-margin-right btn-success pull-right"><i class="fa fa-comments" aria-hidden="true"></i> ' . IMAGE_BUTTON_WRITE_REVIEW . '</a>';
						}
					?>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="clearfix"></div>
			<?php
			  $reviewAppend = ' ';
			  if(isset($_GET['products_id']) && $_GET['products_id'] > 0){
			  		$reviewAppend = " r.products_id = '" . (int)$product_info['products_id'] . "' and ";
			  }
			  $reviews_query_raw = "select r.reviews_id, rd.reviews_text, r.reviews_rating, r.date_added, r.customers_name, r.products_id from " . TABLE_REVIEWS . " r, " . TABLE_REVIEWS_DESCRIPTION . " rd where $reviewAppend r.reviews_id = rd.reviews_id and rd.languages_id = '" . (int)$languages_id . "' and isapproved = 1 order by date_added desc , r.products_id";
			  $reviews_split = new splitPageResults_rspv($reviews_query_raw, MAX_DISPLAY_NEW_REVIEWS);

			  if ($reviews_split->number_of_rows > 0) {
				if ((PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3')) {
			?>
				  <div class="product-listing-module-pagination margin-bottom">
					<div class="pull-left large-margin-bottom page-results"><?php echo $reviews_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></div>
					<div class="pull-right large-margin-bottom no-margin-top">
					  <ul class="pagination no-margin-top no-margin-bottom">
					   <?php echo  $reviews_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info'))); ?>
					  </ul>

					</div>
				  </div>
			<div class="clearfix"></div>
			<?php
				}
				if(isset($_GET['products_id']) && $_GET['products_id'] > 0){
					$reviews_content .= '<div class="row">
											<div class="col-sm-6">
												<div class="rating-block clearfix">
													<h4>Average user rating</h4>
													<h2 class="bold padding-bottom-7">'.avgreviewRatingStars($product_info['products_id']).' <small>/ 5</small></h2>
													'.totalavgUserRating(avgreviewRatingStars($product_info['products_id'])).'
												</div>
											</div>';
					$reviews_content .= '<div class="col-sm-4">
												<div class="rating-block clearfix">
													<h4>Rating breakdown</h4>
													'.reviewRatingBar($product_info['products_id']).'
												</div>
											</div>
										</div>';
				}
				$reviews_query = tep_db_query($reviews_split->sql_query);
				while ($reviews = tep_db_fetch_array($reviews_query)) {
				if(!isset($_GET['products_id']) && $_GET['products_id'] <= 0){
					$product_info_query = tep_db_query("select p.products_id, p.products_model, p.products_image, p.products_price, pd.products_name from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$reviews['products_id'] . "' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "'");
					$product_info = tep_db_fetch_array($product_info_query);
					$pf->loadProduct($reviews['products_id'],$languages_id);
	  				$products_price = $pf->getPriceStringShort();
	  					 /*
						 if (tep_not_null($product_info['products_image'])) {
						 $reviews_content .= '<div class="col-sm-3 align-center"><a data-toggle="modal" href="#popup-image-modal'.$reviews['products_id'].'" class="">' . tep_image(DIR_WS_PRODUCTS .  $product_info['products_image'], $product_info['products_name'], 80, 80, 'class="img-responsive img-thumbnail"') . '</a><br><p class="text-center no-margin-top no-margin-bottom"><a data-toggle="modal" href="#popup-image-modal'.$reviews['products_id'].'" class="btn btn-sm normal">Click To Enlarge</a></p>    <div class="modal fade" id="popup-image-modal'.$reviews['products_id'].'">
											  <div class="modal-dialog">
												<div class="modal-content">
												  <div class="modal-header">
													<h4 class="modal-title">'.$product_info['products_name'] .'</h4>
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												  </div>
												  <div class="modal-body pop_im" style="margin:auto">'.tep_image(DIR_WS_PRODUCTS .  $product_info['products_image'], $product_info['products_name'], LARGE_IMAGE_WIDTH, LARGE_IMAGE_HEIGHT, 'class="img-responsive img-thumbnail" ').'
												  </div>
												  <div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">close</button>
												  </div>
												</div>
											  </div>
											</div></div>
										';
						}
						*/
				}
				$reviews_content .='
									<div class="row">
										<div class="col-sm-12">
											<div class="review-block">
												<div class="row">
													<div class="col-sm-3">
														<i class="fa fa-3x fa-user" aria-hidden="true" style="border: 1px solid antiquewhite; padding: 10px; color: antiquewhite;"></i>
														<div class="review-block-name">'.$reviews['customers_name'].'</div>
														<div class="review-block-date">'.tep_date_long($reviews['date_added']).'<br/></div>
													</div>
													<div class="col-sm-9">';
														if(!isset($_GET['products_id']) && $_GET['products_id'] <= 0){
															$reviews_content .=' <div class="review-block-description" style="font-size:16px;margin-bottom:10px"><b>Review of: </b><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id='.$product_info['products_id']) . '" >'.$product_info['products_name'].'</a>&nbsp; <a onclick="javascript:quick_view('.$product_info['products_id'].')"  class="custom-quick-view" href="javascript:" title="Quick View" data-toggle="tooptip"><i class="fa fa-search"></i> </a></div>';
														}
								$reviews_content .='<div class="review-block-rate">'.reviewRatingStars($reviews['reviews_rating']).'</div>
														<div class="review-block-title">'.tep_output_string_protected(substr($reviews['reviews_text'],0,50)).'...</div>
														<div class="review-block-description">'.tep_break_string(nl2br(tep_output_string_protected($reviews['reviews_text'])), 60, '-<br>').'</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								   ';
			 	}
			 	echo $reviews_content;
			?>
			<?php
			  if (($reviews_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
			?>
			  <div class="content-product-listing-div">
				  <div class="product-listing-module-pagination margin-bottom">
					<div class="pull-left large-margin-bottom page-results"><?php echo $reviews_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></div>
					<div class="pull-right large-margin-bottom no-margin-top">
					  <ul class="pagination no-margin-top no-margin-bottom">
					   <?php echo  $reviews_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info'))); ?>
					  </ul>

					</div>
				  </div>
				 </div>

			<?php
			  } //end bottom pagination
			  } else {
			echo '<div class="col-sm-12 col-lg-12"><div class="well"><h4>' . TEXT_NO_REVIEWS .'</h4></div></div>';

			// RCI code start
			echo $cre_RCI->get('productreviews', 'menu');
			  }
			?>

			<div class="clearfix"></div>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-lg-6 small-margin-top small-margin-bottom">
					<?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, tep_get_all_get_params()) . '" class="btn btn-sm cursor-pointer small-margin-right btn-success" >' . IMAGE_BUTTON_BACK . '</a>'; ?> &nbsp;
					<?php if(isset($_GET['products_id']) && $_GET['products_id'] > 0){ ?>
						<a onclick="javascript:quick_view(<?php echo $product_info['products_id']; ?>)" href="javascript:"><?php echo tep_template_image_button('button_in_cart.gif', QUICK_VIEW)?></a> &nbsp;
						<?php if(check_products_have_options($product_info['products_id'])){ ?>
							<a class="btn btn-sm cursor-pointer small-margin-right btn-success" href="<?php echo tep_href_link(FILENAME_PRODUCT_INFO, 'products_id='.$product_info['products_id']) ;?>"><?php echo VIEW_MORE; ?></a>
						<?php } else { ?>
							<a href="<?php echo tep_href_link(basename($_SERVER['PHP_SELF']), tep_get_all_get_params(array('action','cPath','products_id')) . 'action=buy_now&products_id=' . $product_info['products_id'] . '&cPath=' . tep_get_product_path($product_info['products_id']));?>"><?php echo tep_template_image_button('button_in_cart.gif', ADD_TO_CART)?></a>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-xs-6 col-sm-6 col-lg-6 small-margin-top small-margin-bottom">
					<?php
						if(isset($_GET['products_id']) && $_GET['products_id'] > 0){
							echo '<a href="javascript:" onclick="writereview('.$_GET['products_id'].');" class="btn cursor-pointer small-margin-right btn-success pull-right"><i class="fa fa-comments" aria-hidden="true"></i> ' . IMAGE_BUTTON_WRITE_REVIEW . '</a>';
						}
					?>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="clearfix"></div>
 </div>
</div>
<?php
// RCI code start
echo $cre_RCI->get('productreviews', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
