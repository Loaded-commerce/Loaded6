<?php
/*
  $Id: article_info.tpl.php,v 2.1.0.0 2008/01/21 11:21:11 datazen Exp $

  CRE Loaded, Commercial Open Source E-Commerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('articleinfo', 'top');
// RCI code eof
// added for CDS CDpath support
$CDpath = (isset($_SESSION['CDpath'])) ? '&CDpath=' . $_SESSION['CDpath'] : '';
?>
<div class="row">
 <div class="col-lg-9 col-xs-12 col-sm-12" style="float:left;">
  <?php
  if ($article_check['total'] < 1) {
   ?>
      <div><?php new infoBox(array(array('text' => HEADING_ARTICLE_NOT_FOUND))); ?></div>
        <div class="infoBoxContents">
              <div class="cil-lg-12" align="right"><?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . tep_template_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE) . '</a>'; ?></div>
        </div>
    <?php
  } else {
    $article_info_query = tep_db_query("SELECT a.articles_id, a.articles_date_added,a.articles_image, a.articles_date_available, a.authors_id, ad.articles_name, ad.articles_description, ad.articles_url, au.authors_name
                                        from " . TABLE_ARTICLES . " a,
                                             " . TABLE_ARTICLES_DESCRIPTION . " ad,
                                             " . TABLE_AUTHORS . " au
                                        WHERE a.articles_status = '1'
                                          and a.articles_id = '" . (int)$_GET['articles_id'] . "'
                                          and a.authors_id = au.authors_id
                                          and ad.articles_id = a.articles_id
                                          and ad.language_id = '" . (int)$languages_id . "'");

    $article_info = tep_db_fetch_array($article_info_query);
    //print_r($article_info);

    tep_db_query("update " . TABLE_ARTICLES_DESCRIPTION . " set articles_viewed = articles_viewed+1 where articles_id = '" . (int)$_GET['articles_id'] . "' and language_id = '" . (int)$languages_id . "'");

    $articles_name = $article_info['articles_name'];

    $articles_author_id = $article_info['authors_id'];
    $articles_author = $article_info['authors_name'];
    if (tep_not_null($articles_author)) $title_author = '<span class="smallText" style="margin-top:0px !important;"> By <a href="' . tep_href_link(FILENAME_ARTICLES,'authors_id=' . $articles_author_id . $CDpath) . '">' . $articles_author . '</a></span>';

      ?>
            <div class="col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12 pageHeading" align="left"  style="padding:0px;"><h1 class="no-margin-top mob-heading" style=""><?php echo $articles_name; ?></h1></div>
            <div class="col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12 pageHeading" align="left" style="padding:0px;margin-top:0px;"><?php echo $title_author; ?></div>
            <div class="col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12 mt-3 mb-3" style="text-align: center;"><?php echo tep_image(DIR_WS_ARTICLES . $article_info['articles_image'], $article_info['articles_name'], '0', '0', 'style="width:100%;object-fit: contain;"');?></div>


      <div class="col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12 main" valign="top" style="margin-left:0px;padding-left:0px;">
        <p><?php echo stripslashes($article_info['articles_description']); ?></p>
      </div>

    <?php
    if (tep_not_null($article_info['articles_url'])) {
    ?>
        <div class="main"><?php echo sprintf(TEXT_MORE_INFORMATION, tep_href_link(FILENAME_REDIRECT, 'action=article&amp;goto=' . urlencode($article_info['articles_url'] . $CDpath), 'NONSSL', true, false)); ?></div>
    <?php
    }
    if ($article_info['articles_date_available'] > date('Y-m-d H:i:s')) {
      ?>
        <div class="col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12 smallText" style="padding-left:0px;"><?php echo sprintf(TEXT_DATE_AVAILABLE, tep_date_long($article_info['articles_date_available'])); ?></div>
      <?php
    } elseif ($article_info['articles_date_added'] !='0000-00-00 00:00:00' && $article_info['articles_date_added'] != ''){
      ?>
        <div align="left" class="col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12 smallText" style="padding-left:0px;"><?php echo sprintf(TEXT_DATE_ADDED, tep_date_long($article_info['articles_date_added'])); ?></div>
      <?php
    }
    // RCI code start
    echo $cre_RCI->get('articleinfo', 'menu');
    // RCI code eof

    if (ENABLE_ARTICLE_REVIEWS == 'true') {
      $reviews_query = tep_db_query("select count(*) as count from " . TABLE_ARTICLE_REVIEWS . " where articles_id = '" . (int)$_GET['articles_id'] . "' and approved = '1'");
      $reviews = tep_db_fetch_array($reviews_query);
      ?>
            <div class="main"><?php echo TEXT_CURRENT_REVIEWS . ' ' . $reviews['count']; ?></div>
            <?php
            if ($reviews['count'] <= 0) {
              ?>
              <div class="col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12 main clearfix" align="right"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS_WRITE, tep_get_all_get_params()) . '">
              <button class="pull-right btn btn-lg btn-primary" type="button">'. IMAGE_BUTTON_WRITE_REVIEW .'</button></a>'; ?></div>
              <?php
            } else {
              ?>
              <div align="center" class="col-lg-12 col-xl-12 col-sm-12 col-md-12 col-12" style="text-align:right;">
                  <?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, tep_get_all_get_params()) . '"><button class="btn btn-lg btn-primary" type="button" style="margin-right:10px;">'. IMAGE_BUTTON_REVIEWS .'</button></a>'; ?>
                  <?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS_WRITE, tep_get_all_get_params()) . '"><button class="btn btn-lg btn-primary" type="button" style="">'. IMAGE_BUTTON_WRITE_REVIEW .'</button></a> '; ?>
              </div>

              <?php
            }
           ?>
 <?php // </form> ?>
      <?php
    }
    ?>
        <?php
        if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ARTICLES_XSELL)) {
          require(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ARTICLES_XSELL);
        } else {
          require(DIR_WS_MODULES . FILENAME_ARTICLES_XSELL);
        }
       ?>

    <!-- body_text_eof //-->
    <?php
  }
  ?>
 </div>
 <div class="col-lg-3" style="float:left;">
      <?php
      include('templates/default/boxes/asearch.php');
      include('templates/default/boxes/articles.php');
      include('templates/default/boxes/article_archive.php');
      ?>

 </div>
</div>
<?php
// RCI code start
echo $cre_RCI->get('articleinfo', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
