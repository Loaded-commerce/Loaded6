<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('faq', 'top');
// RCI code eof

?>
<?php
if ($display_mode == 'faq') {
  $faq_category_query = tep_db_query("select icd.categories_name,icd.categories_description, ic.categories_image from " . TABLE_FAQ_CATEGORIES_DESCRIPTION . " icd, " . TABLE_FAQ_CATEGORIES . " ic where ic.categories_id = icd.categories_id and icd.categories_id = '" . (int)$cID . "' and icd.language_id = '" . (int)$languages_id . "' and ic.categories_status = '1'");

  $faq_category = tep_db_fetch_array($faq_category_query);
?>
 <h1 class="no-margin-top"><?php echo $faq_category['categories_name']; ?></h1>
  <?php //   GSR Start ?>
      <div class="col-lg-12" style="text-align:left;padding:10px;padding-left:0px;"><?php echo stripslashes($faq_category['categories_description']); ?></div>
<?php //   GSR End ?>

				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default-faq">
						<?php
						  $toc_query = tep_db_query("select ip.faq_id, ip.question, ip.answer from " . TABLE_FAQ . " f INNER JOIN ". TABLE_FAQ_DESCRIPTION ." ip on f.faq_id=ip.faq_id left join " . TABLE_FAQ_TO_CATEGORIES . " ip2c on ip2c.faq_id = ip.faq_id where ip2c.categories_id = '" . (int)$cID . "' and ip.language_id = '" . (int)$languages_id . "' and f.visible = '1' order by f.v_order, ip.question");
						  while ($toc = tep_db_fetch_array($toc_query)) {?>
								<div class="col-lg-12 well-box well" style="padding-left:0px;">
										<div class="panel-headings" role="tab" id="headingOne-<?php echo $toc['faq_id']; ?>">
										<a role="button" class="show-class" id="collapse-<?php echo $toc['faq_id']; ?>" data-toggle="collapse" data-parent="#accordion" onclick="add_class(<?php echo $toc['faq_id']; ?>)" href="#collapseOne-<?php echo $toc['faq_id']; ?>" aria-expanded="true" aria-controls="collapseOne">
											<h4 class="panel-title title-well-box" style="color:inherit;padding-left:8px;">
												   <?php echo $toc['question']; ?>
											</h4>
										</a>
										</div>

										<div id="collapseOne-<?php echo $toc['faq_id']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne-<?php echo $toc['faq_id']; ?>">
											<div class="panel-body" style="padding:10px;">
												  <?php echo $toc['answer'] ; ?>
											</div>
										</div>
								  </div>


						<?php }?>

   </div>
 </div>



<?php
} else {
?>
<h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1></td>

     		<div class="col-lg-12 panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="padding-left:0px;">
			<div class="panel panel-default-faq">
<?php
  $faq_categories_query = tep_db_query("select ic.categories_id, icd.categories_name,ic.categories_image, icd.categories_description from " . TABLE_FAQ_CATEGORIES . " ic, " . TABLE_FAQ_CATEGORIES_DESCRIPTION . " icd  where icd.categories_id = ic.categories_id and icd.language_id = '" . (int)$languages_id . "' and ic.categories_status = '1'");
  // faq outside categories
  $faq_query = tep_db_query("select ip.faq_id, ip.question, ip.answer from " . TABLE_FAQ . " f INNER JOIN ". TABLE_FAQ_DESCRIPTION ." ip left join " . TABLE_FAQ_TO_CATEGORIES . " ip2c on ip2c.faq_id = ip.faq_id where ip2c.categories_id = '0' and ip.language_id = '" . (int)$languages_id . "' and f.visible = '1' order by f.v_order, ip.question");
	  if ((tep_db_num_rows($faq_categories_query) > 0) || (tep_db_num_rows($faq_query) > 0)) {
		while ($faq_categories = tep_db_fetch_array($faq_categories_query)) {

       if($faq_categories['categories_image'] == '')
       {
       $image='<i class="fa fa-folder-open-o" style="color:#ffffff;font-size:30px;"></i>';
       }else{
       $image = '<img src="'.DIR_WS_OTHERS.$faq_categories['categories_image'].'" class="img-responsive" height="40">';
       }
		  //echo '<h4 class="faqTitle">' . '<a href="' . tep_href_link(FILENAME_FAQ, 'cID=' . (int)$faq_categories['categories_id']) . '">' . $faq_categories['categories_name'] . '</a>' . '</h4>' . "\n";
		  //echo '<p class="faqBlurb">' . $faq_categories['categories_description'] . '</p>' . "\n\n";
?>
 <div class="col-lg-3 col-12" style="margin-bottom:5px;float:left;padding-left:0px;">
     <div class="col-lg-12" style="border:1px solid #DDDDDD;height:250px;padding: 0px;">
      <div class="col-lg-12" style="height:80px;background-color:#00AAFF;text-align:center;padding: 30px;"><?php echo $image ;?></div>
      <p style="padding:10px;text-align: center;"><b><?php echo '<a href="' . tep_href_link(FILENAME_FAQ, 'cID=' . (int)$faq_categories['categories_id']) . '#' . $faq['faq_id'] . '">'. $faq_categories['categories_name'].'</b></a>';?></p>
      <p style="padding:10px;text-align: center;font-size:13px;overflow: hidden;"><?php echo $faq_categories['categories_description']; ?></p>
     </div>
  </div>

<?php		}

		/*while ($faq = tep_db_fetch_array($faq_query)) { ?>

				   <div class="col-lg-12" style="border-bottom:1px dashed #dadada;clear:both;">
					<div class="col-lg-12 panel-headings" role="tab" id="headingOne-<?php echo $faq['faq_id']; ?>">
						<h4 class="panel-title" style="padding:20px;color:inherit;">
							<a role="button" class="show-class" id="collapse-<?php echo $faq['faq_id']; ?>" data-toggle="collapse" data-parent="#accordion" onclick="add_class(<?php echo $faq['faq_id']; ?>)" href="#collapseOne-<?php echo $faq['faq_id']; ?>" aria-expanded="true" aria-controls="collapseOne">
							   <b><?php echo $faq['question']; ?></b>
							</a>
						</h4>
					</div>

					<div id="collapseOne-<?php echo $faq['faq_id']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne-<?php echo $faq['faq_id']; ?>">
						<div class="panel-body" style="padding:10px;">
							  <?php echo $faq['answer'] ; ?>
						</div>
					</div>
				  </div>
	<?php
	} */
	  } else {
		echo '<p>' . TEXT_NO_CATEGORIES . '</p>' . "\n\n";
	  } ?>

     </div>
    </div>

<?php
}
?>
<?php
// RCI code start
echo $cre_RCI->get('faq', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
<script type="text/javascript">
function add_class(Id){
 $('#collapse-'+Id).addClass("intro");
}
</script>
