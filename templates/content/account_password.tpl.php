<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('accountpassword', 'top');
// RCI code eof
echo tep_draw_form('account_password', tep_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL'), 'post', 'onSubmit="return check_form(account_password);"') . tep_draw_hidden_field('action', 'process');
?>
  <div class="row">
  <div class="col-sm-12 col-lg-12 col-xs-12 fix_heading">
    <h3 class="no-margin-top"><?php echo HEADING_TITLE; ?></h3>
	<div class="col-12 col-sm-12 col-xl-12 col-lg-12">
      <div class="well password-account">
        <div class="col-lg-4 col-sm-4">Current Password</div>
        <div class="col-lg-8 col-sm-8"><label class="sr-only"></label><?php echo tep_draw_password_field('password_current', '', 'placeholder="' . ENTRY_PASSWORD_CURRENT . '" class="form-control"'); ?></div>
        <div class="col-lg-4 col-sm-4 mt-2">New Password</div>
        <div class="col-lg-8 col-sm-8"><label class="sr-only"></label><?php echo tep_draw_password_field('password_new','', 'placeholder="' . ENTRY_PASSWORD_NEW . '" class="form-control"'); ?></div>
        <div class="col-lg-4 col-sm-4 mt-2">Password Confirmation</div>
        <div class="col-lg-8 col-sm-8"><label class="sr-only"></label><?php echo tep_draw_password_field('password_confirmation','', 'placeholder="' . ENTRY_PASSWORD_CONFIRMATION . '" class="form-control"'); ?></div>
      </div>

	  <div class="btn-set small-margin-top clearfix col-lg-12">
	    <button class="pull-right btn btn-lg btn-primary" type="submit"><?php echo IMAGE_BUTTON_CONTINUE; ?></button>
	    <a href="<?php echo tep_href_link(FILENAME_ACCOUNT, '', 'SSL') ?>"><button class="pull-left btn btn-lg btn-default" type="button"><?php echo IMAGE_BUTTON_BACK; ?></button></a>
	  </div>
   </div>
	<?php
	// RCI code start
	echo $cre_RCI->get('accountpassword', 'menu');
	// RCI code eof
	?>
   </div>
  </div></form>
<?php
// RCI code start
echo $cre_RCI->get('accountpassword', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>