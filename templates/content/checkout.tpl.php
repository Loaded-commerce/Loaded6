<?php
/*
  $Id: sss_accounthistory_.php,v 1.0.0.0 2008/05/13 13:41:11 datazen Exp $

  CRE Loaded, Open Source E-Commerce Solutions
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('checkout', 'top');


 if ($messageStack->size('checkout') > 0) {
?>
    <div class="message-stack-container alert">
       <?php echo $messageStack->output('checkout'); ?>
   </div>
  <?php
  }
?>
<style>
/***dynamic credit card css***/
.form-control{width:96%;display:inline}

.card .card_icon,  .card .status_icon {
  display: inline-block;
  vertical-align: bottom;
  height: 23px;
  width: 27px;
}
/* --- Card Icon --- */
.card .card_icon { background: transparent url('images/credit_card_sprites.png') no-repeat 30px 0; position:absolute; }
.card .card_icon.visa { background-position: 0 0 !important; position:absolute; right: 12px; top: 45px;}
.card .card_icon.mastercard { background-position: -30px 0 !important; position:absolute; right: 12px; top: 45px;}
.card .card_icon.amex { background-position: -60px 0 !important; position:absolute; right: 12px; top: 45px;}
.card .card_icon.discover { background-position: -90px 0 !important; position:absolute; right: 12px; top: 45px;}
/* --- Card Status --- */
.card .status_icon { background: transparent url('images/status_sprites.png') no-repeat 33px 0; }
.card{border:none;}
.card .invalid .status_icon { background-position: 3px 0 !important; }
.card .valid .status_icon { background-position: -27px 0 !important; }
/***dynamic credit card css***/
</style>
<div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 pl-sm-2 pl-2 pl-xl-0 pl-lg-0 pl-md-2 module-heading"><h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1></div>
<div class="loader-cart" id="loader-cart">
    <img src="<?php echo DIR_WS_DEFAULT;?>ajax-loader.gif" id="loaderimg"/>
</div>
<div class="row <?php echo $content;?>pagecss"><!--row-->
		<div class="checkout-left col-md-7 col-lg-7 col-sm-7 col-12 col-xl-7"><!--checkout-left-->
			<div class="row"><!--inner row-->
				<div class="products-cart col-md-3 col-lg-2 col-sm-2 col-2 col-xl-2 hide-on-mobile">
					<a href="<?php echo tep_href_link(FILENAME_SHOPPING_CART); ?>" class="btn btn-primary btn-sm btn-checkout edit-cart-btn">Edit Cart</a>
				</div>
				<div class="products-cart no-padding col-md-9 col-lg-9 col-sm-12 col-12 col-xl-9">
					<b>Order Summary</b> <a href="<?php echo tep_href_link(FILENAME_SHOPPING_CART); ?>" class="btn btn-primary show-on-mobile btn-sm btn-checkout edit-cart-btn-mobile">Edit Cart</a>
					<hr id="order-hr">
					<?php //echo '<pre>'; print_r($order);
						for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
							echo '<div class="products-info">
									<span class="products-title">'.$order->products[$i]['name'].'</span>
									<span class="products-price">'.$currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']).'</span>';
									if ( (isset($order->products[$i]['attributes'])) && (sizeof($order->products[$i]['attributes']) > 0) ) {
									  for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
										echo '<br><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'] . ' ' . $order->products[$i]['attributes'][$j]['prefix'] . ' ' . $currencies->display_price($order->products[$i]['attributes'][$j]['price'], tep_get_tax_rate($products[$i]['tax_class_id']), 1)  . '</i></small></nobr>';
									  }
									}
							echo '</div>';
						}
					?>
					<hr>
					<div class="cart-calculation-section" id="cart-calculation-section">
						<?php echo '<table class="appendedtabe">' . $order_total_modules->output() . '</table>';?>
					</div>
				</div>
			</div><!--inner row-->
			<div class="row mid-margin-top"><!--inner row 2-->
				<div class="products-cart col-md-3 col-lg-2 col-sm-2 col-2 col-xl-2 hide-on-mobile">
					<a href="javascript:" data-toggle="modal" data-target="#editaccount" class="btn btn-primary btn-sm btn-checkout edit-cart-btn">Edit Info</a>
				</div>
				<div class="products-cart no-padding col-md-9 col-lg-9 col-sm-12 col-12 col-xl-9">
					<b><?php echo CHECKOUT_SUMMARY; ?></b> <a href="javascript:" data-toggle="modal" data-target="#editaccount" class="btn btn-primary show-on-mobile btn-sm btn-checkout edit-cart-btn-mobile"><?php echo EDIT_INFO; ?></a>
					<hr id="order-hr">
					<div class="cart-info-section" id="ajaxinforeloadDiv">
						<?php
						  $account = $lc_customers->getCustomerById((int)$_SESSION['customer_id']);
						  $is_complete = $account['is_complete'];
						  echo $lc_customers->showCustomerInfoBrief($account);
					    ?>
					</div>
					<hr>
				</div>
			</div><!--inner row 2-->
			<div class="row" id="cart-shipping-section"><!--inner row 3-->

			</div><!--inner row 3-->
			<div class="row" id="cart-billing-section"><!--inner row 3-->

			</div><!--inner row 3-->
		</div><!--checkout-left-->

		<div class="checkout-right col-md-5 col-lg-5 col-sm-5 col-12 col-xl-5"><!--checkout-right-->
			<div class="row" id="ajaxreloadDiv">
				<div class="checkout-address active col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right"><?php echo ADDRESS; ?><small class="highlight"></small></div>
				<div class="checkout-shipping col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right"><?php echo SHIPPING_METHODS; ?></div>
				<div class="checkout-payment col-md-4 col-lg-4 col-sm-4 col-4 col-xl-4 no-padding-left no-padding-right"><?php echo PAYMENT_METHODS; ?></div>
				<div class="shipping-address-form col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><!--shipping-address-form-->
					<?php echo tep_draw_form('shippingaddress', '#', 'post', 'id="shippingaddress"'); ?>
							<?php
							/*============new layout goes here============*/
							if(isset($_SESSION['noaccount'])){
								echo '<div class="row large-margin-top"> <b>'.SHIPPING_ADDRESS.'</b> </div>';
							} else {
								if($is_complete){
									$selectexistingselectdropdown = '';
									$thisischecked = 'true';
									$thisischecked2 = '';
								}else{
									$thisischecked = '';
									$thisischecked2 = 'true';
									$selectexistingselectdropdown = 'display:none;';
								}
								echo '<div class="row selectaddress" style="'.$selectexistingselectdropdown.'">';
									echo '<div class="col-lg-10 col-md-10 col-xl-10 no-padding large-margin-top choaddr"></div>';
									echo '<div class="col-lg-12 col-xl-12"></div>';
									echo '<div class="col-lg-8 col-md-10 col-xl-8 no-padding choaddr">';
										echo '<label class="custom-radio-container checkctnr"><span id="existingaddress" class="bactive">'.USE_EXISTING_ADDRESS.'</span>' . tep_draw_radio_field('addressselect', 'existing',$thisischecked,' onClick="fetchaddressdetails(this.value);" ').'<span class="checkmark"></span></label>';
										echo '<label class="custom-radio-container mid-margin-bottom checkctnr"><span id="newaddress">'.USE_NEW_ADDRESS.'</span>' . tep_draw_radio_field('addressselect', 'new',$thisischecked2,' onClick="fetchaddressdetails(this.value);" ').'<span class="checkmark"></span></label>';
										  echo '<div class="parentofselect" style="'.$selectexistingselectdropdown.'">';
											  echo '<label><b>'.EXISTING_SHIPPING_ADDRESS.'</b></label>';
											  echo '<select class="js-select choosebillingaddr" name="selectshippngaddressdp" style="width:100%">';

											  $arr_addresses = $lc_customers->getAllAddressById((int)$_SESSION['customer_id']);
											  foreach($arr_addresses as $addresses)
											  {
												$format_id = tep_get_address_format_id($addresses['country_id']);
												$cname = tep_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']);
												$caddr = tep_address_format($format_id, $addresses, true, ' ', ', ');
												echo '<option value="'.$addresses['address_book_id'].'">'.$caddr.'</option>';
											  }
											  echo '</select>';
										  echo '</div>';
									echo '</div>';
								echo '</div>';
							}
							/*============new layout goes here============*/
							if(isset($_SESSION['noaccount'])){
								$shippingformdetailsstyle = '';
							} else {
								if($is_complete){
									$shippingformdetailsstyle = 'display:none;';
								}else{
									$shippingformdetailsstyle = '';
								}
							}
							?>
							<div id="forshippingparentdiv">
								<?php
								if(!isset($_SESSION['sendto'])){
									$_SESSION['sendto'] = $_SESSION['customer_default_address_id'];
								}

								$address = $lc_customers->getAddressBookById((int)$_SESSION['customer_id'], (int)$_SESSION['sendto']);
								$address_query = tep_db_query("select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id, entry_telephone as telephone, entry_fax as fax from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$_SESSION['customer_id'] . "' and address_book_id = '" . (int)$_SESSION['sendto'] . "'");
								$country = isset($_POST['country'])?$_POST['country']:((isset($address['country_id']) && $address['country_id'] > 0)?$address['country_id']:SHIPPING_ORIGIN_COUNTRY);
								?>
									<div class="row" id="shippingformdetails" style="<?php echo $shippingformdetailsstyle; ?>">
										<?php
											if(!$is_complete){
												echo tep_draw_hidden_field('newaccountcreate', '1','class="form-control"' );
											}
											echo tep_draw_hidden_field('customers_email_address', $account['customers_email_address'],'class="form-control"' );

											echo $lc_checkout->addressBookForm($address);
										?>
								    </div>
								    <div class="row" id="billingformdetails" style="">
										<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><!--sames hipping billing div-->
											<div class="row">
												<?php if(SHIPPING_SKIP != 'Always'){ ?>
													<label class="isbillshipsame"><?php echo tep_draw_checkbox_field('shipbilladdr', $address['address_book_id'], true, 'id="shipbilladdr" onchange="shipbilladdrChanged()"'); ?> <?php echo SHIPPING_ADDRESS_IS_BILLING_ADDRESS; ?></label>
												<?php } else { ?>
													<label style="display:none;"><?php echo tep_draw_checkbox_field('shipbilladdr', $address['address_book_id'], true, 'id="shipbilladdr" onchange="shipbilladdrChanged()"'); ?> <?php echo SHIPPING_ADDRESS_IS_BILLING_ADDRESS; ?></label>
												<?php } ?>
											</div>
										</div><!--sames hipping billing div-->
										<!--billing address-->

										<?php
										  if(!isset($_SESSION['noaccount']) && $is_complete) {
										?>
										<div class="col-md-12 col-lg-8 col-sm-12 col-12 col-xl-8 mid-margin-top bill-address-select" style="margin:auto;display:none">
											<div class="row">
												<select class="js-select choosebillingaddr" onchange="javascript:Fchoosebillingaddr(this.value)" name="choosebillingaddr" style="width:100%">
													<option value="">Select Existing Billing Address</option>
													<option value="addnew">Add New Billing Address</option>
													<?php
														  $arr_addresses = $lc_customers->getAllAddressById((int)$_SESSION['customer_id']);
														  foreach($arr_addresses as $addresses)
														  {
															 $format_id = tep_get_address_format_id($addresses['country_id']);
															 $cname = tep_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']);
															 $caddr = tep_address_format($format_id, $addresses, true, ' ', ', ');
															 echo '<option value="'.$addresses['address_book_id'].'">'.$caddr.'</option>';
														  }
													?>
												</select>
											</div>
										</div>
										<?php } ?>
										<div class="col-lg-12 col-xl-12 col-12 no-padding-left large-margin-top bill-address" style="display:none">
											<b><?php echo BILLING_ADDRESS; ?></b>
										</div>

										<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12 bill-address" style="display:none">
											<div class="row">
												<?php echo $lc_checkout->addressBookForm($address, 1); ?>
											</div>
										</div>
										<!--billing address-->
									</div>
									<div class="row large-margin-top">
										<div class="col-md-12 col-lg-12 col-sm-12 col-12 col-xl-12"><!--save continue-->
											<div class="row">
												<button type="button" name="setaddress" value="setaddress" class="btn btn-primary btn-savecontinue" onClick="return check_form(shippingaddress);"><?php echo SET_ADDRESS_AND_CONTINUE; ?></button>
												<div class="col-lg-12"><div class="row"><p style="text-align:center;margin:auto"><small class="reqdfields"><?php echo ALL_FIELDS_ARE_REQUIRED; ?></small></p></div></div>
											</div>
										</div><!--save continue-->
									</div>
							</div>
					</form>
				</div><!--shipping-address-form-->
			</div>
		</div><!--checkout-right-->
</div><!--row-->

<div id="editaccount" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form name="editaccountinfo" action="#" id="editaccountinfo" method="post">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title">Edit Info</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		  </div>
		  <div class="modal-body">
			<?php
			  $account = $lc_customers->getCustomerById((int)$_SESSION['customer_id']);
			  extract($account);
			?>
				<div class="row">
					<div class="col-lg-6 col-sm-6 col-6">
						<p><?php echo FIRST_NAME; ?><input type="input" name="firstname" value="<?php echo $customers_firstname; ?>" class="form-control"></p>
						<p><?php echo EMAIL_ADDRESS; ?><input type="input" name="emailaddress" value="<?php echo $customers_email_address; ?>" class="form-control"></p>
					</div>
					<div class="col-lg-6 col-sm-6 col-6">
						<p><?php echo LAST_NAME; ?><input type="input" name="lastname" value="<?php echo $customers_lastname; ?>" class="form-control"></p>
						<p><?php echo PHONE_NUMBER; ?><input type="input" name="phonenumber" value="<?php echo $entry_telephone; ?>" class="form-control"></p>
					</div>
					<?php if (ACCOUNT_COMPANY == 'true') { ?>
						<div class="col-lg-6 col-sm-6 col-6">
							<p><?php echo COMPANY; ?><input type="input" name="company" value="<?php echo $entry_company; ?>" class="form-control"></p>
						</div>
						<div class="col-lg-6 col-sm-6 col-6">
							<p><?php echo TAXID; ?><input type="input" name="taxid" value="<?php echo $entry_company_tax_id; ?>" class="form-control"></p>
						</div>
					<?php } ?>
					<div class="col-lg-6 col-sm-6 col-6">
						<p><?php echo FAX; ?><input type="input" name="fax" value="<?php echo $entry_fax; ?>" class="form-control"></p>
					</div>
				</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-primary" value="update" onclick="javascript:updateaccountinfo()">Update</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	</form>
  </div>
</div>

<div id="editshippingaddress" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form name="editmyshippinginfo" action="#" id="editmyshippinginfo" method="post">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title">Edit Shipping Address</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		  </div>
		  <div class="modal-body modaleditaddressmodal" id="modaleditaddressmodal">

		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-primary" value="update" onclick="javascript:updateshippinginfo()">Update</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	</form>
  </div>
</div>

<div id="editbillingaddress" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form name="editmybillinginfo" action="#" id="editmybillinginfo" method="post">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title">Edit Billing Address</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		  </div>
		  <div class="modal-body modaleditbilladdressmodal" id="modaleditbilladdressmodal">

		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-primary" value="update" onclick="javascript:updatebillinginfo()">Update</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	</form>
  </div>
</div>

<?php
// RCI bottom
echo $cre_RCI->get('checkout', 'bottom');
echo $cre_RCI->get('global', 'bottom');

$checkoutPageJS = '$(document).ready(function() {
    $(".search-js-select").select2({ width: \'95%\' });
    $("#state_id select").select2({ width: \'95%\' });
    $("#billing_state_id select").select2({ width: \'95%\' });
});

function pfvalidate() {
	sel_method = $("input[name=\'payment\']:checked").val();
	if(sel_method == \'paypal_xc\') {
		 PopupCenter("'. tep_href_link('xc_process.php') .'", "ppwindow", 600, 800);
		return false;
	}
	'. $payment_modules->footer_js() .'
}
function ppreceive(token, payerid)
{
	$(\'#loader-cart\').css(\'display\', \'block\');
    var form = document.createElement("form");
    var element1 = document.createElement("input");
    var element2 = document.createElement("input");
    element1.type = "hidden";
    element2.type = "hidden";

    form.method = "POST";
    form.action = \''. tep_href_link('checkout_process.php', 'p=paypal') .'&token=\'+ token +\'&PayerID=\'+payerid;

    element1.value = 0;
    element1.name = "cotgv";
    form.appendChild(element1);

    element2.value = document.checkout_payment.comments.value;
    element2.name = "comments";
    form.appendChild(element2);

    document.body.appendChild(form);

    form.submit();

}

$(document).ready(function(){    // get current url hash values.
    var current = window.location.hash;
});';
$obj_catalog->set_footer_js($checkoutPageJS);
unset($checkoutPageJS);
$cardModal = '
  <div class="modal fade" id="whatisCVV" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Enter the 3-digit code on back</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <img src="'. DIR_WS_DEFAULT .'cvv.jpg" style="width:100%">
          <p class="cvvinfo">Enter the 3 Digit Code, can be found backside of your Card</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>';
$obj_catalog->add_footer_block($cardModal);
unset($cardModal);
?>
