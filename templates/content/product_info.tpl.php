<?php
/*
$Id: product_info.tpl.php,v 1.2.0.0 2008/01/22 13:41:11 datazen Exp $

CRE Loaded, Open Source E-Commerce Solutions
http://www.creloaded.com

Copyright (c) 2018 Loaded Commerce (Devi Dash)
Copyright (c) 2008 CRE Loaded
Copyright (c) 2003 osCommerce

Released under the GNU General Public License
*/
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('productinfo', 'top');
// RCI code eof
$product_subproducts_check = tep_has_product_subproducts((int)$_GET['products_id']);
echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_PRODUCT_INFO, tep_get_all_get_params(array('action', 'products_id', 'id')) . 'action=add_product' . '&' . $params), 'post', 'enctype="multipart/form-data" id="cart_quantity" onsubmit="javascript:return ajaxAddProduct()"');
echo tep_draw_hidden_field('products_id', $product_info['products_id']);
?>

<div class="row" id="content">
    <?php
    // added for CDS CDpath support
    $params = (isset($_SESSION['CDpath'])) ? 'CDpath=' . $_SESSION['CDpath'] : '';
    if ($product_check['total'] < 1) {
		echo '<h1 class="no-margin-top">'. TEXT_PRODUCT_NOT_FOUND_HEADING .'</h1>';
		echo '<div class="category_desc" style="min-height:60px; padding-top:20px;">'. TEXT_PRODUCT_NOT_FOUND .'</div>';
		echo '<div style="float:right;"><a href="' . tep_href_link(FILENAME_DEFAULT) . '" class="btn btn-sm btn-primary">' . IMAGE_BUTTON_CONTINUE . '</a></div>';
    } else {
        $product_info_query = tep_db_query("select p.products_id, pd.products_name, pd.products_description, p.products_model, p.products_quantity, p.products_image, p.products_image_med, p.products_image_lrg, p.products_image_sm_1, p.products_image_xl_1, p.products_image_sm_2, p.products_image_xl_2, p.products_image_sm_3, p.products_image_xl_3, p.products_image_sm_4, p.products_image_xl_4, p.products_image_sm_5, p.products_image_xl_5, p.products_image_sm_6, p.products_image_xl_6, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.manufacturers_id from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int)$_GET['products_id'] . "' and pd.products_id = p.products_id and pd.language_id = '" . (int)$languages_id . "'");
        $product_info = tep_db_fetch_array($product_info_query);
        tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1 where products_id = '" . (int)$product_info['products_id'] . "' and language_id = '" . (int)$languages_id . "'");
        $products_name = '<h1 class="pageHeading">' . $product_info['products_name'] . '</h1>';
        if ($product_has_sub > '0'){ // if product has sub products
            $products_price ='';// if you like to show some thing in place of price add here
        } else {
            $pf->loadProduct($product_info['products_id'],$languages_id);
            $products_price = $pf->getPriceStringShort('f');
        } // end sub product check

        if ($product_info['products_image_med']!='') {
            $new_image = $product_info['products_image_med'];
            $image_height = SMALL_IMAGE_HEIGHT;
        } else {
            $new_image = $product_info['products_image'];
            $image_height = SMALL_IMAGE_HEIGHT;
        }

        $valid_to_checkout= true;
        $disable_add_cart = '';
        $stock_status = '<div class="text-right text-success ProductInStock"><b><i class="fa fa-check-circle"></i> Stock : In-Stock</b></div>';
        if (STOCK_CHECK == 'true') {
            $stock_check = tep_get_products_stock($product_info['products_id']);
            if ($stock_check == 0 ){
                $stock_status = '<div class="text-right text-danger ProductOutStock"><b>Stock : Out of Stock</b></div>';
            }
            if (($stock_check == 0 ) && (STOCK_ALLOW_CHECKOUT == 'false')) {
                $valid_to_checkout = false;
                $disable_add_cart = ' disabled="disabled"';
            }
        }

        if(!$obj_catalog->lc_isMobile()) {
            $show_modal = ' data-toggle="modal" data-target="#LargeImage"';
        }
        ?>
        <div class="col-md-12 large-margin-top">

            <div class="row" id="productMain">
                <div class="col-sm-6">
                    <div id="mainImage">
                        <?php echo tep_image(DIR_WS_PRODUCTS . $new_image, $product_info['products_name'],  MEDIUM_IMAGE_WIDTH, MEDIUM_IMAGE_HEIGHT, 'itemprop="image" class="img-responsive img-thumbnail" '. $show_modal);?>
                    </div>
                    <?php
                    // RCI code start
                    echo $cre_RCI->get('productinfo', 'additionalimages');
                    // RCI code eof
					?>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <h1 class="text-left no-margin-top"><?php echo  $product_info['products_name'];?></h1>
                        <?php
                        if(!$product_subproducts_check){ ?>
                        <div class="row">
							<div class="col-lg-6 col-sm-6 col-12">
								<h3 class="text-left priceclass" style="padding: 0px 0px;margin: 0;"><span id="config-price" style="font-weight: 200; font-size: 20px;"><?php echo $products_price; ?></span></h3>
							</div>
							<div class="col-lg-6 col-sm-6 col-12">
								<?php
									echo $stock_status;
									if($product_info['products_model'] != '') {
										echo '<p class="text-right" style="margin:0">'. TEXT_MODEL . ':&nbsp; '. $product_info['products_model'] .'</p>';
									}
									$product_manufacturer_query = tep_db_query("select m.manufacturers_id, m.manufacturers_name, m.manufacturers_image, mi.manufacturers_url from " . TABLE_MANUFACTURERS . " m left join " . TABLE_MANUFACTURERS_INFO . " mi on (m.manufacturers_id = mi.manufacturers_id and mi.languages_id = '" . (int)$languages_id . "'), " . TABLE_PRODUCTS . " p  where p.products_id = '" . (int)$product_info['products_id'] . "' and p.manufacturers_id = m.manufacturers_id");
									if (tep_db_num_rows($product_manufacturer_query)) {
									?>
										<p class="text-right hide-on-mobile" style="margin:0">
											<?php
											while ($manufacturer = tep_db_fetch_array($product_manufacturer_query)) {
												echo '<b>'. TEXT_MANUFACTURER . ': </b>&nbsp;<a href="'.tep_href_link(FILENAME_DEFAULT, 'manufacturers_id='.$manufacturer['manufacturers_id']).'">'. $manufacturer['manufacturers_name'].'</a>';
											}
											?>
										</p>
										<?php
									}
								?>
							</div>
                        </div>
                        <hr style="margin:0">
                        <?php } ?>
                        <p>
            <!-- attributes -->
 	            <?php
 	              $products_has_attributes = false;
 	              $products_id_tmp = $product_info['products_id'];
 	            if(tep_subproducts_parent($products_id_tmp)){
 	              $products_id_query = tep_subproducts_parent($products_id_tmp);
 	            } else {
 	              $products_id_query = $products_id_tmp;
 	            }
 	            if($product_has_sub > '0') {
 	              if (lc_check_constant_val('PRODUCT_INFO_SUB_PRODUCT_ATTRIBUTES', 'False')) {
 	                $load_attributes_for = $products_id_query;
 	                include(DIR_WS_MODULES . 'product_info/product_attributes.php');
 	              }
 	            } else {
 	              // show attributes for parent only
 	              $load_attributes_for = $products_id_query;
 	              include(DIR_WS_MODULES . 'product_info/product_attributes.php');
 	            }
 	            ?>
 	            <!-- attributes eof -->
	            <!-- sub products -->
                        </p>
                        <?php
                        if(!$product_subproducts_check){?>
                        <div style="padding-top:10px;">
							<?php
							if (lc_check_constant_val('SHOW_PRICE_BREAK_TABLE', 'true')) {
								 if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_PRODUCT_QUANTITY_TABLE)) {
									require(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_PRODUCT_QUANTITY_TABLE);
								} else {
									require(DIR_WS_MODULES . FILENAME_PRODUCT_QUANTITY_TABLE);
								}
							}
							?>
						</div>
                        <?php if ($obj_catalog->show_addtocart_button()) { ?><div class="row"><div class="col-lg-12 col-sm-12 col-12" style="list-style:none;position:relative;"><strong>Qty:</strong>&nbsp;&nbsp;<?php echo tep_draw_input_field('cart_quantity', '1','max-length=7 class="form-control" id="cart_quantitys" style="width:20%;display:inline;border-right: none;border-top-right-radius: 0;border-bottom-right-radius: 0;"');?><button type="button" value="" id="adds" class="btn btn-default quantity_box_up" /><i class="fa fa-angle-up"></i></button><button type="button" value="" id="subs" class="btn btn-default quantity_box_down" style="" /><i class="fa fa-angle-down"></i></button></div></div><?php } ?>
                        <p class="text-left buttons large-margin-top small-padding-right">
                            <?php
							if ($obj_catalog->show_addtocart_button()) {
								echo '<button type="submit" class="btn btn-primary btn-lg large-margin-bottom"'. $disable_add_cart .'><i class="fa fa-shopping-cart"></i> Add to cart</button>';
							}

                            if (DESIGN_BUTTON_WISHLIST == 'true') {
                                echo '<a href="javascript:addwishlist()" class="btn btn-default btn-lg large-margin-bottom ml-2">Add to Wishlist</a>';
	                        }
							// RCI code start
							echo $cre_RCI->get('productinfo', 'underpriceheading');
							// RCI code eof
                           ?>
						</p>
                        <?php } ?>


                    </div>
                </div>
            </div>
   			 <div class="col-sm-12 col-lg-12 no-padding-left no-padding-right large-margin-bottom"><!--newtabstart12div-->

		            <!-- sub products -->
		            <?php
		            $select_order_by .= 'sp.products_model';
		            $sub_products_query = tep_db_query("select sp.products_id, sp.products_quantity, sp.products_price, sp.products_tax_class_id, sp.products_image, spd.products_name, spd.products_blurb, sp.products_model from " . TABLE_PRODUCTS . " sp, " . TABLE_PRODUCTS_DESCRIPTION . " spd where sp.products_quantity > 0 and sp.products_parent_id = " . (int)$product_info['products_id'] . " and spd.products_id = sp.products_id and spd.language_id = " . (int)$languages_id . " order by " . $select_order_by);

		    if ( tep_db_num_rows($sub_products_query) > 0 ) {
		        if(defined('PRODUCT_INFO_SUB_PRODUCT_DISPLAY') && PRODUCT_INFO_SUB_PRODUCT_DISPLAY == 'In Listing'){
		            include(DIR_WS_MODULES . 'product_info/sub_products_listing.php');
		       }
		    }
		    // sub product_eof
		    ?>
			</div>

     <ul class="nav nav-tabs">
		<?php
		$arr_product_tabs = $obj_product_info->product_tabs();
		foreach($arr_product_tabs as $tab_heading) {  //print_r($tab_heading);
		?>
				<li class="nav-item"><a class="nav-link<?php echo ($tab_heading['active'])?' active':'';?>" data-toggle="tab" href="#<?php echo $tab_heading['tag'];?>"><?php echo $tab_heading['heading'];?></a></li>
		<?php } ?>
	 </ul>

	  <div class="tab-content">
		<?php
			foreach($arr_product_tabs as $tab_content) {
		?>
			<div id="<?php echo $tab_content['tag'];?>" class="tab-pane<?php echo ($tab_content['active'])?' active':'';?>"<?php if($tab_content['tag'] == 'tab-description'){ echo ' itemprop="description"'; }?>><?php echo $tab_content['content']?></div>
		<?php } ?>
      </div>



    </div>
    <!-- /.col-md-12 -->
    <div class="col-sm-12 col-lg-12" style="margin-top:10px;padding:0px;">
 <?php
     if ( (USE_CACHE == 'true') && !SID) {
          echo tep_cache_also_purchased(3600);
         if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_XSELL_PRODUCTS)) {
            require(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_XSELL_PRODUCTS);
        } else {
            require(DIR_WS_MODULES . FILENAME_XSELL_PRODUCTS);
        }
    } else {
         if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_XSELL_PRODUCTS_BUYNOW)) {
            require(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_XSELL_PRODUCTS_BUYNOW);
        } else {
            require(DIR_WS_MODULES . FILENAME_XSELL_PRODUCTS_BUYNOW);
        }

         if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ALSO_PURCHASED_PRODUCTS)) {
            require(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ALSO_PURCHASED_PRODUCTS);
        } else {
            require(DIR_WS_MODULES . FILENAME_ALSO_PURCHASED_PRODUCTS);
        }
    }
 ?>
 </div>

 <?php
 } //$product_check
?>
</div>   <!-- class="row" id="content -->
</form>
<?php
// product info page bottom
echo $cre_RCI->get('productinfo', 'bottom');
// RCI code start
echo $cre_RCI->get('global', 'bottom');
?>
<div id="LargeImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="LargeImageLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title"> <?php echo $product_info['products_name']; ?></h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php echo tep_image(DIR_WS_PRODUCTS . $new_image, $product_info['products_name'],  '','', 'class="img-responsive img-thumbnail"');?><br>
            </div>
        </div>
    </div>
</div>
<?php 
$productPagejs = '';
if (DESIGN_BUTTON_WISHLIST == 'true') {
  $productPagejs = 'function addwishlist() {
    document.cart_quantity.action=\''. str_replace('&amp;', '&', tep_href_link(FILENAME_PRODUCT_INFO, 'action=add_wishlist' . '&' . $params)) .'\';
    document.cart_quantity.submit();
  }';
}
if($products_has_attributes) { 
$productPagejs = 'function updateConfigPrice(){
		params = $(\'#cart_quantity\').serializeArray();
		$.ajax({
			type: \'post\',
			url: \'ajax_handler.php?action=getprice&rand=\'+Math.random(),
			data: params,
			success: function(retVal){
				$(\'#config-price\').html(retVal);
			}

		})
	}
	$(document).ready(function() {
		updateConfigPrice();
	});';
}
$productPagejs = '
$(\'#adds\').click(function add() {
    var $rooms = $("#cart_quantitys");
    var a = $rooms.val();
    a++;
    $("#subs").prop("disabled", !a);
    $rooms.val(a);
});
$("#subs").prop("disabled", !$("#cart_quantitys").val());

$(\'#subs\').click(function subst() {
    var $rooms = $("#cart_quantitys");
    var b = $rooms.val();
    if (b >= 1) {
        b--;
        $rooms.val(b);
    }
    else {
        $("#subs").prop("disabled", true);
    }
});
function ajaxAddProduct(){
	pID = '. (int) $_GET['products_id'].';
	'. $js_attribute_validation_code .'
	params = $(\'#cart_quantity\').serialize();
	'. $obj_catalog->print_hooks_js('ajax_add_to_cart') .'
}

$(document).ready(function() {
	$(\'.pickoptiondate\').datepicker({
	todayHighlight: true,
	autoclose: true,
	format: \'yyyy-mm-dd\'
	});
 });';
$obj_catalog->set_footer_js($productPagejs);
unset($productPagejs);
?>
