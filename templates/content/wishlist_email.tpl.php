<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('wishlistemail', 'top');
// RCI code eof
echo '<div class="row">';
         echo tep_draw_form('email_wish', tep_href_link(FILENAME_WISHLIST_SEND, 'action=process&products_id=' . (isset($_GET['products_id']) ? (int)$_GET['products_id'] : 0))) . tep_draw_hidden_field('products_name', (isset($product_info['products_name']) ? $product_info['products_name'] : ''));
?>
<div class="pageHeading"><h1><?php echo HEADING_TITLE; ?></h1></div>
<?php
 /* if ($messageStack->size('friend') > 0) {
?>

        <div><?php echo $messageStack->output('friend'); ?></div>


<?php
  } */
?>
	<div class="col-lg-12 col-sm-12 col-xs-12 main-div" style="padding:0px;margin-left:-10px;">
		<div class="col-lg-12 col-sm-12 col-xs-12"  style="padding:0px;">
          <div  class="col-lg-4 main"><b><?php echo FORM_TITLE_CUSTOMER_DETAILS; ?></b></div>
          <div class="col-lg-8 inputRequirement req-infm" align="right"><?php echo FORM_REQUIRED_INFORMATION; ?></div>
		  <div class="col-lg-4 main"><?php echo FORM_FIELD_CUSTOMER_NAME; ?></div>
		  <div class="col-lg-8 main" style="margin-bottom:10px;"><?php echo tep_draw_input_field('from_name', '', 'class="form-control" style="width:96%"'); ?></div>
		  <div class="col-lg-4 main"><?php echo FORM_FIELD_CUSTOMER_EMAIL; ?></div>
		  <div class="col-lg-8 main"><?php echo tep_draw_input_field('from_email_address', '', 'class="form-control" style="width:96%"'); ?></div>
		</div>
		<div class="col-lg-12 col-sm-12 col-xs-12" style="padding:0px;">
		  <div class="col-lg-12 frnd-detail" colspan="2"><b><?php echo FORM_TITLE_FRIEND_DETAILS; ?></b></div>
		  <div class="col-lg-4 main"><?php echo FORM_FIELD_FRIEND_NAME; ?></div>
		  <div class="col-lg-8 main" style="margin-bottom:10px;"><?php echo tep_draw_input_field('to_name', '', 'class="form-control" style="display:inline;width:96%"') . '&nbsp;<span class="inputRequirement">' . ENTRY_FIRST_NAME_TEXT . '</span>'; ?></div>
		  <div class="col-lg-4 main"><?php echo FORM_FIELD_FRIEND_EMAIL; ?></div>
		  <div class="col-lg-8 main"><?php echo tep_draw_input_field('to_email_address', '', 'class="form-control" style="display:inline;width:96%"') . '&nbsp;<span class="inputRequirement">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>'; ?></div>
		</div>

	    <div class="col-lg-12 col-sm-12 col-xs-12 main prod-detail" style="margin-bottom:10px;"><b><?php echo FORM_FIELD_PRODUCTS; ?></b></div>
		<div class="col-lg-12 col-sm-12 col-xs-12"><?php echo nl2br($wishliststring); ?></div>
      <div class="col-lg-12 col-sm-12 col-xs-12 main"  style="margin-top:10px;"><b><?php echo FORM_TITLE_FRIEND_MESSAGE; ?></b></div>
	<div class="col-lg-12 col-sm-12 col-xs-12"><?php echo tep_draw_textarea_field('message', 'soft', 40, 8, '', 'class="form-control" style="margin-bottom:15px;"'); ?></div>
</div>
<?php
// RCI code start
echo $cre_RCI->get('wishlistemail', 'menu');
// RCI code eof
?>

<?php if ($valid_product == "true") { ?>
   <div><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . (int)$_GET['products_id']) . '">' . tep_template_image_button('button_back.gif', IMAGE_BUTTON_BACK) . '</a>'; ?></div>

<?php } ?>
       <div style="margin-right:20px;text-align:right;"><?php echo tep_template_image_submit('button_continue.gif', IMAGE_BUTTON_CONTINUE); ?></div>
 </form>
</div><!--row end-->
<?php
// RCI code start
echo $cre_RCI->get('wishlistemail', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>