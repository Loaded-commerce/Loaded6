<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('indexproducts', 'top');
// RCI code eof
// Get the category information
$category_query = tep_db_query("select cd.categories_name, cd.categories_heading_title, cd.categories_description, c.categories_image from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$current_category_id . "' and cd.categories_id = '" . (int)$current_category_id . "' and cd.language_id = '" . $languages_id . "'");
$category = tep_db_fetch_array($category_query);

$manufacturer_data =tep_db_fetch_array(tep_db_query("select manufacturers_name from manufacturers where manufacturers_id ='".$_GET['manufacturers_id']."'"));
if(tep_not_null($category['categories_heading_title'])){
	$heading_text = $category['categories_heading_title'];
} else if(tep_not_null($category['categories_name'])){
	$heading_text = $category['categories_name'];
} else if(tep_not_null($manufacturer_data['manufacturers_name'])){
   $heading_text = $manufacturer_data['manufacturers_name'];
}else {
	$heading_text = HEADING_TITLE;
}
?>
<!-- bof content.index_products.tpl.php-->
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 pl-sm-2 pl-2 pl-xl-0 pl-lg-0 pl-md-0 module-heading"><h3 class="no-margin-top list-heading"><?php echo $heading_text; ?></h3></div>
  <div id="content-product-listing-category-description-container" class="">
	<?php if ( (ALLOW_CATEGORY_DESCRIPTIONS == 'true') && (isset($category) && tep_not_null($category['categories_description'])) ) {  echo '<span class="category_desc">' . $category['categories_description'] . '</span>'; } ?>
   </div>
  <div class="clearfix"></div>
<?php
// optional Product List Filter
    if (PRODUCT_LIST_FILTER > 0) {
      if (isset($_GET['manufacturers_id'])) {
        $filterlist_sql = "select distinct c.categories_id as id, cd.categories_name as name
                           from " . TABLE_PRODUCTS . " p,
                                " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c,
                                " . TABLE_CATEGORIES . " c,
                                " . TABLE_CATEGORIES_DESCRIPTION . " cd
                           where p2c.categories_id = c.categories_id
                             and p.products_id = p2c.products_id
                             and cd.categories_id = c.categories_id
                             and cd.language_id = '" . (int)$languages_id . "'
                             and p.manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "'
                             and p.products_status = '1'
                           order by cd.categories_name";
      } else {
        $filterlist_sql= "select distinct m.manufacturers_id as id, m.manufacturers_name as name from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, " . TABLE_MANUFACTURERS . " m where p.products_status = '1' and p.manufacturers_id = m.manufacturers_id and p.products_id = p2c.products_id and p2c.categories_id = '" . (int)$current_category_id . "' order by m.manufacturers_name";
      }
      $filterlist_query = tep_db_query($filterlist_sql);
      if (tep_db_num_rows($filterlist_query) > 1) {
      	if(isset($_GET['cPath']) && $_GET['cPath'] != '')
      		$filter_param = 'cPath='.$_GET['cPath'];
      	if(isset($_GET['manufacturers_id']) && (int) $_GET['manufacturers_id'] > 0)
      		$filter_param = 'manufacturers_id='. (int) $_GET['manufacturers_id'];

        echo '<div class="col-sm-12 col-lg-12 col-xl-12 col-xs-12 pull-right no-padding"><div class="col-sm-7 col-lg-7 col-xs-12 pull-right">' . tep_draw_form('filter', tep_href_link(FILENAME_DEFAULT, $filter_param), 'get') .' <div class="col-sm-2 col-lg-2 col-xs-3 manu-label pull-left">'. TEXT_SHOW . '</div>';
        if (isset($_GET['manufacturers_id'])) {
         // echo tep_draw_hidden_field('manufacturers_id', (int)$_GET['manufacturers_id']);
          $options = array(array('id' => '', 'text' => TEXT_ALL_CATEGORIES));
        } else {
         // echo tep_draw_hidden_field('cPath', $cPath);
          $options = array(array('id' => '', 'text' => TEXT_ALL_MANUFACTURERS));
        }
        //echo tep_draw_hidden_field('sort', (isset($_GET['sort']) ? $_GET['sort'] : ''));
        while ($filterlist = tep_db_fetch_array($filterlist_query)) {
          $options[] = array('id' => $filterlist['id'], 'text' => $filterlist['name']);
        }
        echo '<div class="col-sm-10 col-lg-10 col-xs-9 pull-left" style="padding-bottom:10px;">'.tep_draw_pull_down_menu('filter_id', $options, (isset($_GET['filter_id']) ? $_GET['filter_id'] : ''), 'onchange="this.form.submit()" class="form-control"');
        echo '</form></div></div></div><div class="clearfix"></div>' . "\n";
      }
    }

// Get the right image for the top-right
    $image = DIR_WS_IMAGES . 'table_background_list.gif';
    if (isset($_GET['manufacturers_id'])) {
       $manufactures_query = tep_db_query("select manufacturers_name from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "'");
       $manufactures = tep_db_fetch_array($manufactures_query);
       $heading_text_box =  $manufactures['manufacturers_name'];
      $image = tep_db_query("select manufacturers_image from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "'");
      $image = tep_db_fetch_array($image);
      $image = $image['manufacturers_image'];
    } elseif ($current_category_id) {
      $image = tep_db_query("select categories_image from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$current_category_id . "'");
      $image = tep_db_fetch_array($image);
      $image = $image['categories_image'];
    }
?>
<div class="clear-both"></div>
<div class="product-listing-module-container col-lg-12 col-xl-12 col-md-12 col-sm-12 col-12">
<?php
$obj_catalog->load_listing_module();
?>
	</div>
</div>

<?php
// RCI code start
echo $cre_RCI->get('indexproducts', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
