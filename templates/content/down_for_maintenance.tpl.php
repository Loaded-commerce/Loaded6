<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('downformaintenance', 'top');
// RCI code eof
?>
<div class="row">
<?php
// BOF: Lango Added for template MOD
if (SHOW_HEADING_TITLE_ORIGINAL == 'yes') {
$header_text = '&nbsp;'
//EOF: Lango Added for template MOD
?>
      <div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12"><h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1></div>
      <div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 centerdiv"><?php echo tep_image(DIR_WS_DEFAULT . 'lc_maintenance.jpg', HEADING_TITLE, 400, 200); ?></div>

<?php
// BOF: Lango Added for template MOD
}else{
$header_text = HEADING_TITLE;
}
// EOF: Lango Added for template MOD
?>


<?php
// BOF: Lango Added for template MOD
/* if (MAIN_TABLE_BORDER == 'yes'){
table_image_border_top(false, false, $header_text);
} */
// EOF: Lango Added for template MOD
?>

            <div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 centerdiv"><?php echo DOWN_FOR_MAINTENANCE_TEXT_INFORMATION; ?></div>

    <?php if (DISPLAY_MAINTENANCE_TIME == 'true') { ?>

            <div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 centerdiv"><?php echo TEXT_MAINTENANCE_ON_AT_TIME . TEXT_DATE_TIME; ?></div>

     <?php
      }
      if (DISPLAY_MAINTENANCE_PERIOD == 'true') { ?>
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 centerdiv"><?php echo TEXT_MAINTENANCE_PERIOD . TEXT_MAINTENANCE_PERIOD_TIME; ?></div>
      <?php }
// RCI code start
echo $cre_RCI->get('downformaintenance', 'menu');
// RCI code eof
// BOF: Lango Added for template MOD
if (MAIN_TABLE_BORDER == 'yes'){
table_image_border_bottom();
}
// EOF: Lango Added for template MOD
?>

        <div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 centerdiv"><br><?php echo DOWN_FOR_MAINTENANCE_STATUS_TEXT ;?><br>
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12" style="text-align:right;"><?php echo '<a  href="' . tep_href_link(FILENAME_DEFAULT) . '">' . tep_template_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE) . '</a>'; ?></div>

    </div>
<?php
// RCI code start
echo $cre_RCI->get('downformaintenance', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>