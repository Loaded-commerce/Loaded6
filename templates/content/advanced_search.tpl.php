<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('advancedsearch', 'top');
// RCI code eof
echo tep_draw_form('advanced_search', tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT, '', 'NONSSL', false), 'get', 'onSubmit="return check_form(this);"') . tep_hide_session_id(); ?>
<div class="col-sm-12 col-lg-12" style="padding:0px;">
<?php
// BOF: Lango Added for template MOD
if (SHOW_HEADING_TITLE_ORIGINAL == 'yes') {
$header_text = '&nbsp;'
//EOF: Lango Added for template MOD
?>
    <div class="row">
      <div class="col-sm-12 col-lg-12">
        <div class="row main-class">
         <div class="col-lg-12" style="padding-left:0px">
           <div class="col-lg-6 col-12" style="float:left;padding-left:0px"><h1> <?php echo HEADING_TITLE_1; ?></h1></div>
           <div class="col-lg-6 col-12 align-right" style="float:left;padding-left:0px"><?php echo '<a onclick="javascript:popupfancy(\''. tep_href_link(FILENAME_POPUP_HELP, 'hpage=search_help') .'\')" href="javascript:void(0);">' . TEXT_SEARCH_HELP_LINK . ' <i class="fa fa-question searchicon" aria-hidden="true" data-toggle="tooltip" title="Search Help"></i> </a>'; ?></div>
        </div>
       </div>
<?php
}else{
$header_text = '<h1>' . HEADING_TITLE_1 . '</h1>';
}
?>

<?php
// BOF: Lango Added for template MOD
// EOF: Lango Added for template MOD
?>
<?php
  if ($messageStack->size('search') > 0) {
?>
       <div class="message-stack-container alert alert-danger">
             <?php echo $messageStack->output('search'); ?>
       </div>



<?php
  }
  $date = date('01-m-Y');
  $date1 =date("m-d-Y");
?>
        <div class="content-search-container main-cont">
          <div class="form-group  large-padding-right">
            <label class="sr-only"></label>
            <input type="text" id="search-div" name="keywords" value="" class="form-control" placeholder="<?php echo HEADING_SEARCH_CRITERIA; ?>" style="float:left;">
			<p class="pull-right"><input type="submit" class="btn btn-sm cursor-pointer small-margin-right btn-success btn-primary" value="Search">
			<?php //echo tep_template_image_submit('button_search.gif', IMAGE_BUTTON_SEARCH,'','','class="btn-primary"'); ?>
			</p>
          </div>
        </div>
        <div class="clearfix"></div>&nbsp;

        <div class="col-lg-12 col-12 col-md-12 serch-checkbox">
           <div class="col-lg-6 serch-content help-block" style="float:left;"><label class="custom-radio-container"><?php echo tep_draw_checkbox_field('search_in_description', '1') . '<span class="checkmark cartremove search-check" style=""></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.TEXT_SEARCH_IN_DESCRIPTION.'</label>'; ?></div>
           <div class="col-lg-6 col-12 help-block " style="float:left;"><label class="custom-radio-container"><?php echo tep_draw_checkbox_field('inc_subcat', null, null, 'class=""', null).'<span class="checkmark cartremove search-check"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.ENTRY_INCLUDE_SUBCATEGORIES.'</label> '; ?></div>
        </div>
      </div>
    </div>&nbsp;

    <div class="row">
      <div class="col-sm-12 col-lg-12 form-content" style="">
        <div class="col-lg-12 col-xs-12 no-padding form-group has-success category-main-div">
          <div class="col-lg-2 col-xs-12 main-div" style="float:left"><label class="control-label  text-right category-div" style="padding-left:0px;"><?php echo ENTRY_CATEGORIES; ?></label></div>
          <div class="col-sm-10 col-xs-12 col-lg-10 control-input">
          <?php echo tep_draw_pull_down_menu('categories_id', tep_get_categories(array(array('id' => '', 'text' => TEXT_ALL_CATEGORIES))), null, 'class="form-control categories_list"'); ?></div>
        </div>&nbsp;




       <?php /* <div class="col-lg-12 no-padding form-group has-success">
          <div class="col-lg-2 col-xs-12 main-div" style="float:left"><label class="control-label text-right margin-top" style="padding-left:0px;"><?php echo ENTRY_INCLUDE_SUBCATEGORIES; ?></label></div>
          <div class="col-sm-10 col-xs-12 col-lg-2 control-input"><?php echo tep_draw_checkbox_field('inc_subcat', null, null, 'class="form-control"', null); ?></div>
        </div> */?>
        <div class="col-lg-12 no-padding form-group has-success form-content">
          <div class="col-lg-2 col-xs-12 main-div" style="float:left"><label class="control-label text-right small-margin-top" style="padding-left:0px;"><?php echo ENTRY_MANUFACTURERS; ?></label></div>
          <div class="col-sm-10 col-xs-12 col-lg-10 control-input"><?php echo tep_draw_pull_down_menu('manufacturers_id', tep_get_manufacturers(array(array('id' => '', 'text' => TEXT_ALL_MANUFACTURERS))), null, 'class="form-control manufacture-list"'); ?></div>
        </div>
       <?php /* <div class="col-lg-12 no-padding form-group has-success">
          <div class="col-lg-2 col-xs-12 main-div" style="float:left"><label class="control-label text-right small-margin-top" style="padding-left:0px;"><?php echo ENTRY_PRICE_FROM; ?></label></div>
          <div class="col-sm-10 col-xs-12 col-lg-10 control-input"><?php echo tep_draw_input_field('pfrom', null, 'class="form-control"'); ?></div>
        </div>
        <div class="col-lg-12 no-padding form-group has-success">
          <div class="col-lg-2 col-xs-12 main-div" style="float:left"><label class="control-label text-right small-margin-top" style="padding-left:0px;"><?php echo ENTRY_PRICE_TO; ?></label></div>
          <div class="col-sm-10 col-xs-12 col-lg-10 control-input"><?php echo tep_draw_input_field('pto', null, 'class="form-control"'); ?></div>
        </div>
        <div class="col-lg-12 no-padding form-group has-success">
          <div class="col-lg-2 col-xs-12 main-div" style="float:left"><label class="control-label text-right small-margin-top" style="padding-left:0px;"><?php echo ENTRY_DATE_FROM; ?></label></div>
          <div class="col-sm-10 col-xs-12 col-lg-10 control-input"><?php echo tep_draw_input_field('dfrom', DOB_FORMAT_STRING, 'class="form-control" onFocus="RemoveFormatString(this, \'' . DOB_FORMAT_STRING . 'class="form-control"'); ?></div>
        </div>
        <div class="col-lg-12 no-padding form-group has-success">
         <div class="col-lg-2 col-xs-12 main-div" style="float:left"><label class="control-label text-right small-margin-top" style="padding-left:0px;"><?php echo ENTRY_DATE_TO; ?></label></div>
          <div class="col-sm-10 col-xs-12 col-lg-10 control-input"><?php echo tep_draw_input_field('dto', DOB_FORMAT_STRING, 'class="form-control" onFocus="RemoveFormatString(this, \'' . DOB_FORMAT_STRING . 'class="form-control"'); ?></div>
        </div> */ ?>
        <div class="clearfix"></div>
        <div class="col-lg-12 no-padding form-group has-success">
              <div class="col-lg-2 control-input price-from" style="float:left"><label><?php echo ENTRY_PRICE_FROM; ?></label></div>
				<div class="col-lg-10" style="float:left">
				 <div class="row">
				   <div class="col-lg-5 col-xl-5 col-md-5 search-price-box" style=""><div class="input-group"><span class="addon" style="float:left;">$</span><?php echo tep_draw_input_field('pfrom', null, 'class="form-control price-input"'); ?></div></div>
				   <div class="col-lg-7 col-xl-7 col-md-7 search-price-box" style="">

						<div class="input-group mb-3">
							<div class="col-lg-3 no-padding" style=""><label><?php echo ENTRY_PRICE_TO; ?></label></div>
							<div class="col-lg-9 col-xl-9 col-md-9 price-input-box" style="">
							  <div class="input-group-prepend pull-left">
								<span class="addon">$</span>
							  </div>
								<?php echo tep_draw_input_field('pto', null, 'class="form-control price-to" style=""'); ?>
							</div>
						</div>
                   </div>
                   </div>
				  </div>
        </div>&nbsp;
        <div class="clearfix"></div>
        <div class="col-lg-12 no-padding form-group has-success">
              <div class="col-lg-2 control-input price-from" style="float:left"><label><?php echo ENTRY_DATE_FROM; ?></label></div>
				  <div class="col-lg-10" style="float:left">
				     <div class="row">
					   <div class="col-lg-5 col-xl-5 col-md-5 search-price-box" style=""><div class="input-group"><span class="addon fa fa-calendar" style="float:left;"></span><?php echo tep_draw_input_field('dfrom', '', 'class="form-control from_date" id="from-date" class="form-control" placeholder="dd-mm-yyyy" data-date-format="dd-mm-yyyy"'); ?></div></div>
					   <div class="col-lg-7 col-xl-7 col-md-7 search-price-box date-box">
					     <div class="input-group mb-3">

						   <div class="col-lg-3 no-padding date-label-input" style=""><label class="date-label" style=""><?php echo ENTRY_DATE_TO; ?></label></div>
						   <div class="col-lg-9 col-xl-9 col-md-9 date-box" style="padding-right:0px;"><div class="input-group-prepend pull-left"><span class="addon fa fa-calendar"></span></div><?php echo tep_draw_input_field('dto', '', 'class="form-control to-date" id="date-to" placeholder="dd-mm-yyyy"'); ?></div>

					    </div>
					   </div>
				     </div>
				  </div>
        </div>&nbsp;
      </div>
     </div>



<?php
// BOF: Lango Added for template MOD
// RCI code start
echo $cre_RCI->get('advancedsearch', 'menu');
// RCI code eof
// EOF: Lango Added for template MOD
?>
    </div>
   </form>
<?php
// RCI code start
echo $cre_RCI->get('advancedsearch', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>