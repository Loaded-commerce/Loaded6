<!-- body_text //-->
<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('indexrestricted', 'top');
// RCI code eof
?>
<div class="row">
	<div class="col-lg-12 col-sm-12 col-12 col-xl-12"><h1 class="no-margin-top"><?php echo TEXT_INDEX_RESTRICTED_HEADING; ?></h1></div>
	<div class="col-lg-12 col-sm-12 col-12 col-xl-12 category_desc" style="min-height:60px; padding-top:20px;"><?php echo TEXT_INDEX_RESTRICTED_TEXT. '<br>'; ?></div>
	<div class="col-lg-12 col-sm-12 col-12 col-xl-12" style="text-align: center;"><?php echo '<img src="'.DIR_WS_DEFAULT.'lock1.png" height="220px">';?></div>
	<div class="col-lg-12 col-sm-12 col-12 col-xl-12" style="float:right;text-align: right;"><?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT) . '" class="btn btn-sm btn-primary">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></div>
</div>
<?php
// RCI code start
echo $cre_RCI->get('global', 'bottom');
echo $cre_RCI->get('indexrestricted', 'bottom');
// RCI code eof
?><!-- body_text_eof //-->
