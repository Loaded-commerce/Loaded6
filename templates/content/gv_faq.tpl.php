<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('gvfaq', 'top');
// RCI code eof
?>    
<h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1>
<table border="0" width="100%" cellspacing="0" cellpadding="<?php echo CELLPADDING_SUB; ?>">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><?php echo TEXT_INFORMATION; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td class="main"><b><?php echo SUB_HEADING_TITLE; ?></b></td>
      </tr>
      <tr>
       <td class="main"><?php echo SUB_HEADING_TEXT; ?></td>
      </tr>
<?php
// RCI code start
echo $cre_RCI->get('gvfaq', 'menu');
// RCI code eof
?>
      <tr>
        <td style="padding:10px;">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" class="main"><?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT, '', 'NONSSL') . '"><button class="pull-right btn btn-lg btn-primary" type="button">'. IMAGE_BUTTON_CONTINUE .'</button></a>'; ?></td>
      </tr>
    </table>
<?php
// RCI code start
echo $cre_RCI->get('gvfaq', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
