<div class="row">
  <div class="col-sm-12 col-lg-12 col-xs-12 no-padding">
	<h1 class="no-margin-top list-heading"><?php echo HEADING_TITLE; ?></h1>
	<div>The page you have requested not avilable now. Please verify the URL and try again.</div>
	<div style="text-align:center;"><img alt="404 - Page Not Found" class="img-responsive" src="<?php echo DIR_WS_DEFAULT; ?>pagenotfound.png"></div>
 </div>
</div>
