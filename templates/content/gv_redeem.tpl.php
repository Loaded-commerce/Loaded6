<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('gvredeem', 'top');
// RCI code eof
?>      
<h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1>
<table border="0" width="100%" cellspacing="0" cellpadding="<?php echo CELLPADDING_SUB; ?>">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><?php echo TEXT_INFORMATION; ?></td>
          </tr>
<?php
// if we get here then either the url gv_no was not set or it was invalid
// so output a message.
  $message = sprintf(TEXT_VALID_GV, $currencies->format($coupon['coupon_amount']));
  if ($error) {
    $message = TEXT_INVALID_GV;
  }
?>
          <tr>
            <td class="main"><?php echo $message; ?></td>
          </tr>
          <?php
          // RCI code start
          echo $cre_RCI->get('gvredeem', 'menu');
          // RCI code eof
          ?>  
          <tr>
            <td align="right"><br><?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT) . '"><button class="pull-right btn btn-lg btn-primary" type="button">'. IMAGE_BUTTON_CONTINUE .'</button></a>'; ?></td>
          </tr>
        </table></td>
      </tr>
    </table>
<?php
// RCI code start
echo $cre_RCI->get('gvredeem', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
