<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('productsnew', 'top');
// RCI code eof
?>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 pl-sm-2 pl-2 pl-xl-0 pl-lg-0 pl-md-2 module-heading"><h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1></div>
  <div id="content-product-listing-category-description-container" class="col-sm-12 col-lg-12 col-xs-12 margin-top">
<?php
// BOF: Lango Added for template MOD
if (SHOW_HEADING_TITLE_ORIGINAL == 'yes') {
$header_text = '&nbsp;'
//EOF: Lango Added for template MOD
?>
<?php
}else{
$header_text =  HEADING_TITLE;
}


$obj_catalog->load_listing_module();

// RCI code start
echo $cre_RCI->get('productsnew', 'menu');
// RCI code eof
// BOF: Lango Added for template MOD
// EOF: Lango Added for template MOD
?>
    </div>
</div>

<?php
// RCI code start
echo $cre_RCI->get('productsnew', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>