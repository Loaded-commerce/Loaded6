<?php
echo tep_draw_form('order', tep_href_link(FILENAME_CHECKOUT_SUCCESS, 'action=update', 'SSL'), 'post', 'enctype="multipart/form-data"'); ?>
<div class="row">
  <div class="col-sm-8 col-lg-8 large-margin-bottom checkout-success-div">
  <?php
  // BOF: Lango Added for template MOD
  if (SHOW_HEADING_TITLE_ORIGINAL == 'yes') {
    $header_text = '&nbsp;'
    //EOF: Lango Added for template MOD
    ?>
<div class="large-margin-top align-center">
 <h4 class="no-margin-top"><strong>Order : #<?php echo $_GET['order_id'] ;?></strong></h4>
  <h3 class="no-margin-top"><?php echo HEADING_TITLE; ?></h3>
  <p><?php echo HEADING_SUB_TITLE; ?></p>
</div>
    <?php
    // BOF: Lango Added for template MOD
  }else{
    $header_text = HEADING_TITLE;
  }
  // EOF: Lango Added for template MOD
  $order = new order((int)$_GET['order_id']);
  if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ADD_CHECKOUT_SUCCESS)) {
    require(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ADD_CHECKOUT_SUCCESS);
  } else {
    require(DIR_WS_MODULES . FILENAME_ADD_CHECKOUT_SUCCESS);
  }
  ?>
<b><?php echo ORDER_SUMMARY; ?></b>
<hr>
<div class="cart-info-section">
	<?php //echo $_SESSION['shipto'].'----------'.$_SESSION['billto'];
	  if ($_GET['order_id'] > 0 ) {
	  	  $order_query = tep_db_query("select `customers_name`,`customers_company`,`customers_email_address` from orders where orders_id = '".(int)$_GET['order_id']."' ");
	  	  $account = tep_db_fetch_array($order_query);
	  	  $customersname = explode(" ",$account['customers_name']);
	  	  $customers_firstname = $customersname[0];
	  	  $customers_lastname = $customersname[1];
	  	  if (ACCOUNT_COMPANY == 'true'){
			  if ($account['customers_company'] != '') {
				echo '<div><label>Company Name: </label>'.$account['customers_company'].'</div>';
			  }
		  }
		  echo '<div><label>First Name: </label>'.$customers_firstname.'</div>';
		  echo '<div><label>Last Name: </label>'.$customers_lastname.'</div>';
		  echo '<div><label>Email Address: </label>'.$account['customers_email_address'].'</div>';
	  }
	?>
	<hr>
	<div class="row">
		<div class="col-md-3 col-lg-3 col-sm-4 col-4 col-xl-3 no-padding-right success-order"><label>Shipping Address: </label></div>
		<div class="col-md-9 col-lg-9 col-sm-8 col-8 col-xl-9" style="padding-left:10px;">
		<?php echo tep_address_format($order->delivery['format_id'], $order->delivery, true, ' ', '<br>'); ?>
		</div>

		<div class="col-md-3 col-lg-3 col-sm-4 col-4 col-xl-3 no-padding-right success-order"><label>Billing Address: </label></div>
		<div class="col-md-9 col-lg-9 col-sm-8 col-8 col-xl-9" style="padding-left:10px;">
		<?php echo tep_address_format($order->billing['format_id'], $order->billing, true, ' ', '<br>'); ?>
		</div>
	</div>
	<hr>
	<?php //echo '<pre>'; print_r($order);
		for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
			echo '<div class="products-info">
					<span class="products-title-success">'.$order->products[$i]['name'].'</span>
					<span class="products-price-success">'.$currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']).'</span>';
					if ( (isset($order->products[$i]['attributes'])) && (sizeof($order->products[$i]['attributes']) > 0) ) {
					  for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
						echo '<br><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'] . ' ' . $order->products[$i]['attributes'][$j]['prefix'] . ' ' . $currencies->display_price($order->products[$i]['attributes'][$j]['price'], tep_get_tax_rate($products[$i]['tax_class_id']), 1)  . '</i></small></nobr>';
					  }
					}
			echo '</div>';
		}
	?>
	<hr>
	<div class="total-success">
		<?php
		  for ($i = 0, $n = sizeof($order->totals); $i < $n; $i++) {
			echo '<label>'.$order->totals[$i]['title'].'<span class="totprice">'.$order->totals[$i]['text'].'</span></label>';
		  }
		?>
	</div>
	<hr>
	<div class="large-margin-bottom">Paid: <?php echo $order->info['payment_method']; ?></div>
	<?php if(trim($order->info['comment']) != ''){ ?>
		<div class="large-margin-bottom">Order notes: <?php echo $order->info['comment']; ?></div>
	<?php } ?>
</div>


  <!-- checkout_success_modules - start -->


      <?php /*
      if (MODULE_CHECKOUT_SUCCESS_INSTALLED) {
        $checkout_success_modules->process();
        echo $checkout_success_modules->output();
      }
      */ ?>


  <!-- checkout_success_modules - end -->



  <?php
  //RCI start
  echo $cre_RCI->get('checkoutsuccess', 'insideformabovebuttons');
  //RCI end
  ?>

<div class="small-margin-top clearfix">
	  <?php echo '<a href="javascript:popupWindow(\'' .  tep_href_link(FILENAME_ORDERS_PRINTABLE, tep_get_all_get_params(array('order_id')) . 'order_id=' . (int)$_GET['order_id'].'&onetimevalidInvoice='.(int)$onetimevalidInvoice.'&customer_id='.(int)$customer_id, 'NONSSL') . '\')"><button type="button" class="btn btn-primary btn-savecontinue">' . IMAGE_BUTTON_PRINT_ORDER_INVOICE . '</button></a>'; ?>
      <button type="submit" class="btn btn-default btn-grey">Continue</button>
</div>
  <?php
  //RCI start
  echo $cre_RCI->get('checkoutsuccess', 'insideformbelowbuttons');
   //RCI end
  ?>
  <?php if (DOWNLOAD_ENABLED == 'true') include(DIR_WS_MODULES . 'downloads.php'); ?>
</div>
</div>
</form>
<?php
// RCI code start
echo $cre_RCI->get('checkoutsuccess', 'bottom', false);
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>