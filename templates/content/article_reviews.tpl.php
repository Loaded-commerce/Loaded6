<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('articlereviews', 'top');
// RCI code eof
?>
<h1><?php echo HEADING_TITLE . '\'' . $articles_name . '\''; ?></h1>
<table border="0" width="100%" cellspacing="0" cellpadding="<?php echo CELLPADDING_SUB;?>" class="table-condensed">
<?php
  $reviews_query_raw = "select r.reviews_id, left(rd.reviews_text, 100) as reviews_text, r.reviews_rating, r.reviews_read, r.date_added, r.customers_name from " . TABLE_ARTICLE_REVIEWS . " r, " . TABLE_ARTICLE_REVIEWS_DESCRIPTION . " rd where r.articles_id = '" . (int)$article_info['articles_id'] . "' and r.reviews_id = rd.reviews_id and rd.languages_id = '" . (int)$languages_id . "' and r.approved = '1' order by r.reviews_id desc";
  $reviews_split = new splitPageResults($reviews_query_raw, MAX_DISPLAY_NEW_REVIEWS);

  if ($reviews_split->number_of_rows > 0) {
    if ((PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3')) {
?>
	  <tr>
		<td>
		  <table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
			  <td class="smallText"><?php echo $reviews_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></td>
			  <td align="right" class="smallText pagination-td">
			  <?php echo $reviews_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info'))); ?>
			  </td>
			</tr>
		  </table>
		</td>
	  </tr>
<?php
    }//pagination eof

    $reviews_query = tep_db_query($reviews_split->sql_query);
    while ($reviews = tep_db_fetch_array($reviews_query)) {
?>
	  <tr>
		<td>
		  <table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
			  <td class="main"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS_INFO, 'articles_id=' . $article_info['articles_id'] . '&amp;reviews_id=' . $reviews['reviews_id']) . '"><u><b><font color="Black">' . sprintf(TEXT_REVIEW_BY, tep_output_string_protected($reviews['customers_name'])) . '</font></b></u></a> (' . TEXT_REVIEW_VIEWS . $reviews['reviews_read'] . ')'; ?></td>
			  <td class="smallText" align="right"><?php echo sprintf(TEXT_REVIEW_DATE_ADDED, tep_date_long($reviews['date_added'])); ?></td>
			</tr>
		  </table>
		</td>
	  </tr>
	  <tr>
		<td>
		  <table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
			<tr class="infoBoxContents">
			  <td>
				<table border="0" width="100%" cellspacing="0" cellpadding="2">
				  <tr>
					<td valign="top" class="main"><?php echo tep_break_string(tep_output_string_protected($reviews['reviews_text']), 60, '-<br>') . '<br>' . '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS_INFO, 'articles_id=' . $article_info['articles_id'] . '&amp;reviews_id=' . $reviews['reviews_id']) . '">' . TEXT_READ_REVIEW . '</a><br><br><i>' . sprintf(TEXT_REVIEW_RATING, tep_image(DIR_WS_DEFAULT . 'stars_' . $reviews['reviews_rating'] . '.gif', sprintf(TEXT_OF_5_STARS, $reviews['reviews_rating'])), sprintf(TEXT_OF_5_STARS, $reviews['reviews_rating'])) . '</i>'; ?></td>
				  </tr>
				</table>
			  </td>
			</tr>
		  </table>
		</td>
	  </tr>
<?php
}
  if (($reviews_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
?>
	  <tr>
		<td>
		  <table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
			  <td class="smallText"><?php echo $reviews_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></td>
			  <td align="right" class="smallText pagination-td"><?php echo $reviews_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info'))); ?></td>
			</tr>
		  </table>
		</td>
	  </tr>

<?php
  } //pagination eof

    } else {
?>
	  <tr>
		<td class="main"><?php echo TEXT_NO_ARTICLE_REVIEWS; ?></td>
	  </tr>
<?php
}
// RCI code start
echo $cre_RCI->get('articlereviews', 'menu');
// RCI code eof

?>

	  <tr>
		<td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
		  <tr class="infoBoxContents">
			<td><table border="0" width="100%" cellspacing="0" cellpadding="2">
			  <tr>
				<td class="main"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_INFO, tep_get_all_get_params()) . '"><button class="pull-left btn btn-lg btn-default" type="button">'. IMAGE_BUTTON_BACK .'</button></a>'; ?></td>
				<td class="main" align="right"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS_WRITE, tep_get_all_get_params()) . '"><button class="pull-right btn btn-lg btn-primary" type="button">'. IMAGE_BUTTON_WRITE_REVIEW .'</button></a>'; ?></td>
			  </tr>
			 </table></td>
		  </tr>
		 </table></td>
	  </tr>
   </table>
<?php
// RCI code start
echo $cre_RCI->get('articlereviews', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
