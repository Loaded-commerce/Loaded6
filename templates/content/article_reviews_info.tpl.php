<?php
/*
  $Id: article_reviews_info.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce
  Chain Reaction Works, Inc.
  
  Copyright &copy; 2005-2006
  
  Last Modified By : $Author$
  Last Modified On : $Date$
  Latest Revision : $Revision:$

  Released under the GNU General Public License
*/
?>
<!-- articles_reviews.tpl.php //-->
<?php 
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('articlereviewsinfo', 'top');
// RCI code eof
?>
<h1><?php echo HEADING_TITLE . '\'' . $articles_name . '\''; ?></h1>
<table border="0" width="100%" cellspacing="0" cellpadding="<?php echo CELLPADDING_SUB;?>" class="table-condensed">
  <tr>
	<td><table border="0" width="100%" cellspacing="0" cellpadding="2">
	  <tr>
		<td class="main"><?php echo '<b>' . sprintf(TEXT_REVIEW_BY, tep_output_string_protected($review['customers_name'])) . '</b>'; ?></td>
		<td align="right"><small><i><?php echo sprintf(TEXT_REVIEW_DATE_ADDED, tep_date_long($review['date_added'])); ?></i></small></td>
	  </tr>
	</table></td>
  </tr>
  <tr>
	<td><table border="0" width="100%" cellspacing="0" cellpadding="2">
		  <tr>
			<td valign="top" class="main"><?php echo tep_break_string(nl2br(tep_output_string_protected($review['reviews_text'])), 60, '-<br>') . '<br><br><i>' . sprintf(TEXT_REVIEW_RATING, tep_image(DIR_WS_IMAGES . 'stars_' . $review['reviews_rating'] . '.gif', sprintf(TEXT_OF_5_STARS, $review['reviews_rating'])), sprintf(TEXT_OF_5_STARS, $review['reviews_rating'])) . '</i>'; ?></td>
		  </tr>
		</table></td>
  </tr>
  <tr>
	<td><table border="0" width="100%" cellspacing="0" cellpadding="2">
		  <tr>
			<td class="main"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, tep_get_all_get_params(array('reviews_id'))) . '"><button class="pull-left btn btn-lg btn-default" type="button">'. IMAGE_BUTTON_BACK .'</button></a>'; ?></td>
			<td class="main" align="right"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS_WRITE, tep_get_all_get_params(array('reviews_id'))) . '"><button class="pull-right btn btn-lg btn-primary" type="button">'. IMAGE_BUTTON_WRITE_REVIEW .'</button></a>'; ?></td>
		  </tr>
		</table></td>
  </tr>
<?php
// RCI code start
echo $cre_RCI->get('articlereviews', 'menu');
// RCI code eof
?>
    </table>
<?php 
// RCI code start
echo $cre_RCI->get('articlereviewsinfo', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
