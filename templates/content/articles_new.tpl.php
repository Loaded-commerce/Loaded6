<?php
/*
  $Id: articles_new.tpl.php,v 2.1.0.0 2008/01/21 11:21:11 datazen Exp $

  CRE Loaded, Commercial Open Source E-Commerce
  http://www.creloaded.com

  Copyright (c) 2008 CRE Loaded
  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('articlesnew', 'top');
// RCI code eof
// added for CDS CDpath support
$CDpath = (isset($_SESSION['CDpath'])) ? '&CDpath=' . $_SESSION['CDpath'] : '';
?>
<div class="row">
	<div class="col-lg-9" style="float:left">
	  <div class="col-lg-12 col-xs-12" style="padding:0px;"><h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1></div>
	  <div border="0" width="100%" cellspacing="0" cellpadding="<?php echo CELLPADDING_SUB;?>" class="table-condensed">
		<hr style="border-top: 3px solid #428BCF !important;"></hr>
<?php
$group_access_new_articles = $obj_catalog->b2b_filter_sql('a.products_group_access');
$articles_new_array = array();
$articles_new_query_raw = "select a.articles_id, a.articles_date_added,a.articles_image, ad.articles_name, ad.articles_head_desc_tag, au.authors_id, au.authors_name, td.topics_id, td.topics_name
						 from " . TABLE_ARTICLES . " a,
							  " . TABLE_AUTHORS . " au,
							  " . TABLE_ARTICLES_DESCRIPTION . " ad,
							  " . TABLE_ARTICLES_TO_TOPICS . " a2t,
							  " . TABLE_TOPICS_DESCRIPTION . " td
						 where a.articles_status = '1'
						   and (a.articles_date_available IS NULL or to_days(a.articles_date_available) <= to_days(now()))
						   and a.articles_date_added > SUBDATE(now( ), INTERVAL '" . NEW_ARTICLES_DAYS_DISPLAY . "' DAY)
						   and a.authors_id = au.authors_id
						   and a.articles_id = ad.articles_id
						   and a.articles_id = a2t.articles_id
						   and a2t.topics_id = td.topics_id
						   and ad.language_id = '" . (int)$languages_id . "'
						   and td.language_id = '" . (int)$languages_id . "' " . $group_access_new_articles . "
						 order by a.articles_date_added desc, ad.articles_name";

      $articles_new_split = new splitPageResults($articles_new_query_raw, MAX_NEW_ARTICLES_PER_PAGE);
      if (($articles_new_split->number_of_rows > 0) && ((ARTICLE_PREV_NEXT_BAR_LOCATION == 'top') || (ARTICLE_PREV_NEXT_BAR_LOCATION == 'both'))) {
        ?>
          <div class="col-lg-12" style="padding:0px;">
              <div class="col-lg-12 col-xs-12 smallText" style="padding-left:0px;"><b><?php echo $articles_new_split->display_count(TEXT_DISPLAY_NUMBER_OF_ARTICLES_NEW); ?></b></div>
              <div align="right" class="smallText col-lg-12 col-xs-12"><?php echo TEXT_RESULT_PAGE . ' ' . $articles_new_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></div>
         </div>
        <?php
      }
      ?>


          <?php
          if ($articles_new_split->number_of_rows > 0) {
            $articles_new_query = tep_db_query($articles_new_split->sql_query);
            ?>

             <div class="col-lg-12 col-xs-12 main" style="padding:0px;margin-bottom:10px;"><?php echo sprintf(TEXT_NEW_ARTICLES, NEW_ARTICLES_DAYS_DISPLAY); ?></div>

            <?php
            while ($articles_all = tep_db_fetch_array($articles_new_query)) {

              include(DIR_WS_MODULES.'article_listing_view.php');
            } // End of listing loop
          } else {
            ?>

              <div class="col-lg-12 main" style="padding:0px;margin-bottom:20px;"><?php echo sprintf(TEXT_NO_NEW_ARTICLES, NEW_ARTICLES_DAYS_DISPLAY); ?></div>

		<?php
          }
          ?>

      <?php
      if (($articles_new_split->number_of_rows > 0) && ((ARTICLE_PREV_NEXT_BAR_LOCATION == 'bottom') || (ARTICLE_PREV_NEXT_BAR_LOCATION == 'both'))) {
        ?>
          <div class="col-lg-12" style="padding:0px;">
              <div class="col-lg-12 smallText" style="padding-left:0px;"><?php echo $articles_new_split->display_count(TEXT_DISPLAY_NUMBER_OF_ARTICLES_NEW); ?></td>
              <div align="right" class="smallText col-lg-12" style=""><?php echo TEXT_RESULT_PAGE . ' ' . $articles_new_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></div>
         </div>
        <?php
      }
      // RCI code start
      echo $cre_RCI->get('articlesnew', 'menu');
      // RCI code eof
      ?>
	  </div>
</div>
<div class="col-lg-3 col-12" style="float:left;">
      <?php include('templates/default/boxes/asearch.php');
      include('templates/default/boxes/articles.php');
      include('templates/default/boxes/article_archive.php');

      ?>
</div>
	</div>
<?php
// RCI code start
echo $cre_RCI->get('articlesnew', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
