<?php
/*
  $Id: articles.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('articles', 'top');
// RCI code eof
// added for CDS CDpath support
//$CDpath = (isset($_SESSION['CDpath'])) ? '&CDpath=' . $_SESSION['CDpath'] : '';
$CDpath = '';
?>

	<div class="row">
	<div class="col-lg-9 col-xs-12  col-sm-12" style="">
	  <?php
	  if ($topic_depth == 'nested') {
		$group_access_nested_articles = $obj_catalog->b2b_filter_sql('t.products_group_access');
		$topic_query = tep_db_query("select td.topics_name, td.topics_heading_title, td.topics_description from " . TABLE_TOPICS . " t, " . TABLE_TOPICS_DESCRIPTION . " td where t.topics_id = '" . (int)$current_topic_id . "' and td.topics_id = '" . (int)$current_topic_id . "' and td.language_id = '" . (int)$languages_id . "' ".$group_access_nested_articles);
		$topic = tep_db_fetch_array($topic_query);
		?>
			  <div valign="top" class="pageHeading"><h1>
				<?php
				if ( tep_not_null($topic['topics_heading_title']) ) {
				  echo $topic['topics_heading_title'];
				} else {
				  echo HEADING_TITLE;
				}
				?></h1>
			  </div>
			  <div valign="top" class="pageHeading" align="right"></div>

			<?php
			if ( tep_not_null($topic['topics_description']) ) { ?>

				<div><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></div>
				<div align="left" colspan="2" class="main"><?php echo $topic['topics_description']; ?></div>
				<div><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></div>

			  <?php
			}

			  if (isset($tPath) && strpos('_', $tPath)) {
				// check to see if there are deeper topics within the current topic
				$topic_links = array_reverse($tPath_array);
				for($i=0, $n=sizeof($topic_links); $i<$n; $i++) {
				  $topics_query = tep_db_query("select count(*) as total from " . TABLE_TOPICS . " t, " . TABLE_TOPICS_DESCRIPTION . " td where t.parent_id = '" . (int)$topic_links[$i] . "' and t.topics_id = td.topics_id and td.language_id = '" . (int)$languages_id . "'");
				  $topics = tep_db_fetch_array($topics_query);
				  if ($topics['total'] < 1) {
					// do nothing, go through the loop
				  } else {
					$topics_query = tep_db_query("select t.topics_id, td.topics_name, t.parent_id from " . TABLE_TOPICS . " t, " . TABLE_TOPICS_DESCRIPTION . " td where t.parent_id = '" . (int)$topic_links[$i] . "' and t.topics_id = td.topics_id and td.language_id = '" . (int)$languages_id . "' order by sort_order, td.topics_name");
					break; // we've found the deepest topic the customer is in
				  }
				}
			  } else {
				$topics_query = tep_db_query("select t.topics_id, td.topics_name, t.parent_id from " . TABLE_TOPICS . " t, " . TABLE_TOPICS_DESCRIPTION . " td where t.parent_id = '" . (int)$current_topic_id . "' and t.topics_id = td.topics_id and td.language_id = '" . (int)$languages_id . "' order by sort_order, td.topics_name");
			  }
			  // needed for the new articles module shown below
			  $new_articles_topic_id = $current_topic_id;

			include(DIR_WS_MODULES . FILENAME_NEW_ARTICLES);

	  } elseif ($topic_depth == 'articles' || isset($_GET['authors_id'])) {
		// Get the topic name and description from the database
		$group_access_topic_articles = $obj_catalog->b2b_filter_sql('t.products_group_access');
		$group_access_author_articles = $obj_catalog->b2b_filter_sql('a.products_group_access');
		$topic_query = tep_db_query("select td.topics_name, td.topics_heading_title, td.topics_description from " . TABLE_TOPICS . " t, " . TABLE_TOPICS_DESCRIPTION . " td where t.topics_id = '" . (int)$current_topic_id . "' and td.topics_id = '" . (int)$current_topic_id . "' and td.language_id = '" . (int)$languages_id . "' ".$group_access_topic_articles);
		$topic = tep_db_fetch_array($topic_query);
		// show the articles of a specified author
		if (isset($_GET['authors_id'])) {
		  if (isset($_GET['filter_id']) && tep_not_null($_GET['filter_id'])) {
			// We are asked to show only a specific topic
			$listing_sql = "SELECT a.articles_id, a.authors_id, a.articles_date_added,a.articles_image, ad.articles_name, ad.articles_head_desc_tag, au.authors_name, td.topics_name, a2t.topics_id from
							 " . TABLE_ARTICLES . " a,
								 " . TABLE_ARTICLES_DESCRIPTION . " ad,
								 " . TABLE_AUTHORS . " au,
								 " . TABLE_ARTICLES_TO_TOPICS . " a2t,
								 " . TABLE_TOPICS_DESCRIPTION . " td
							WHERE a.articles_status = '1'
							  and (a.articles_date_available IS NULL or to_days(a.articles_date_available) <= to_days(now()))
							  and au.authors_id = '" . (int)$_GET['authors_id'] . "'
							  and a.authors_id = '" . (int)$_GET['authors_id'] . "'
							  and a.articles_id = a2t.articles_id
							  and ad.articles_id = a2t.articles_id
							  and a2t.topics_id = '" . (int)$_GET['filter_id'] . "'
							  and td.topics_id = '" . (int)$_GET['filter_id'] . "'
							  and ad.language_id = '" . (int)$languages_id . "'
							  and td.language_id = '" . (int)$languages_id . "'" . $group_access_author_articles . "
							ORDER BY a.articles_date_added desc, ad.articles_name";
		  } else {
			// We show them all
			$listing_sql = "SELECT a.articles_id, a.authors_id, a.articles_date_added,a.articles_image, ad.articles_name, ad.articles_head_desc_tag, au.authors_name, td.topics_name, a2t.topics_id from
							" . TABLE_ARTICLES . " a,
								 " . TABLE_ARTICLES_DESCRIPTION . " ad,
								 " . TABLE_AUTHORS . " au,
								 " . TABLE_ARTICLES_TO_TOPICS . " a2t,
								 " . TABLE_TOPICS_DESCRIPTION . " td
							WHERE a.articles_status = '1'
							  and (a.articles_date_available IS NULL or to_days(a.articles_date_available) <= to_days(now()))
							  and au.authors_id = '" . (int)$_GET['authors_id'] . "'
							  and a.authors_id = '" . (int)$_GET['authors_id'] . "'
							  and a.articles_id = a2t.articles_id
							  and ad.articles_id = a2t.articles_id
							  and a2t.topics_id = td.topics_id
							  and ad.language_id = '" . (int)$languages_id . "'
							  and td.language_id = '" . (int)$languages_id . "'" . $group_access_author_articles . "
							ORDER BY a.articles_date_added desc, ad.articles_name";
		  }
		} else {
		  // show the articles in a given category
		  if (isset($_GET['filter_id']) && tep_not_null($_GET['filter_id'])) {
			// We are asked to show only specific catgeory
			$listing_sql = "SELECT a.articles_id, a.authors_id, a.articles_date_added,a.articles_image, ad.articles_name, ad.articles_head_desc_tag, au.authors_name, td.topics_name, a2t.topics_id from
							" . TABLE_ARTICLES . " a,
								 " . TABLE_ARTICLES_DESCRIPTION . " ad,
								 " . TABLE_AUTHORS . " au,
								 " . TABLE_ARTICLES_TO_TOPICS . " a2t,
								 " . TABLE_TOPICS_DESCRIPTION . " td
							WHERE a.articles_status = '1'
							  and (a.articles_date_available IS NULL or to_days(a.articles_date_available) <= to_days(now()))
							  and au.authors_id = '" . (int)$_GET['filter_id'] . "'
							  and a.authors_id = '" . (int)$_GET['filter_id'] . "'
							  and a.articles_id = a2t.articles_id
							  and ad.articles_id = a2t.articles_id
							  and a2t.topics_id = '" . (int)$current_topic_id . "'
							  and td.topics_id = '" . (int)$current_topic_id . "'
							  and ad.language_id = '" . (int)$languages_id . "'
							  and td.language_id = '" . (int)$languages_id . "' " . $group_access_author_articles . "
							ORDER BY a.articles_date_added desc, ad.articles_name";
		  } else {
			// We show them all
			$listing_sql = "SELECT a.articles_id, a.authors_id, a.articles_date_added,a.articles_image, ad.articles_name, ad.articles_head_desc_tag, au.authors_name, td.topics_name, a2t.topics_id from
							" . TABLE_ARTICLES . " a,
								 " . TABLE_ARTICLES_DESCRIPTION . " ad,
								 " . TABLE_AUTHORS . " au,
								 " . TABLE_ARTICLES_TO_TOPICS . " a2t,
								 " . TABLE_TOPICS_DESCRIPTION . " td
							WHERE a.articles_status = '1'
							  and (a.articles_date_available IS NULL or to_days(a.articles_date_available) <= to_days(now()))
							  and a.authors_id = au.authors_id
							  and a.articles_id = a2t.articles_id
							  and ad.articles_id = a2t.articles_id
							  and a2t.topics_id = '" . (int)$current_topic_id . "'
							  and td.topics_id = '" . (int)$current_topic_id . "'
							  and ad.language_id = '" . (int)$languages_id . "'
							  and td.language_id = '" . (int)$languages_id . "' " . $group_access_author_articles . "
							ORDER BY a.articles_date_added desc, ad.articles_name";
		  }
		}
		?>
		 <div class="col-lg-12 col-xs-12 col-sm-12" style="padding-left:0px;">
		  <?php
			if ( tep_not_null($topic['topics_heading_title']) ) {
			  $_heading_title = $topic['topics_heading_title'];
			} else {
			  $_heading_title =  HEADING_TITLE;
			}
			if (isset($_GET['authors_id'])) {
			  $author_query = tep_db_query("select au.authors_name, aui.authors_description, aui.authors_url from " . TABLE_AUTHORS . " au, " . TABLE_AUTHORS_INFO . " aui where au.authors_id = '" . (int)$_GET['authors_id'] . "' and au.authors_id = aui.authors_id and aui.languages_id = '" . (int)$languages_id . "'");
			  $authors = tep_db_fetch_array($author_query);
			  $author_name = $authors['authors_name'];
			  $authors_description = $authors['authors_description'];
			  $authors_url = $authors['authors_url'];
			  $_author_name =  TEXT_ARTICLES_BY . $author_name;

			}
			?>
			   <div class="col-lg-12 col-xs-12 mob-heading" style="padding-left:0px;"><h1 class="mob-heading  no-margin-top"><?php echo $_heading_title.' '.$_author_name; ?></h1></div>
			<?php
			if ( tep_not_null($topic['topics_description']) ) {
			  ?>
					<div class="main col-lg-12 col-xs-12 col-sm-12 " style="padding-left:0px;padding-right:0px;"><?php echo $topic['topics_description']; ?></div>
				  <?php

				  if (tep_not_null($authors_description)) {
					?>
					  <div class="main col-lg-12 col-xs-12 col-sm-12" valign="top"><?php echo $authors_description; ?></div>
					<?php

				  }
				  if (tep_not_null($authors_url)) {
					?>
					  <div class="main col-lg-12 col-xs-12 col-sm-12" valign="top"><?php echo sprintf(TEXT_MORE_INFORMATION, $authors_url); ?></div>
					<?php
				  }
			}
			?>

		<hr style="border-top: 3px solid #428BCF !important;"></hr>
		</div>
		   <div class="row">
		   	<div class="col-lg-6 col-xs-6 col-sm-6"><?php echo TEXT_ARTICLES; ?>sss</div>
		   	<div class="col-lg-6 col-xs-6 col-sm-6 manu-label"><div style="width:20%;float:left"><?php echo TEXT_SHOW; ?></div><div style="width:80%;float:right">
			 <?php
			  // optional Article List Filter
			  if (ARTICLE_LIST_FILTER) {
				if (isset($_GET['authors_id'])) {
				  $filterlist_sql = "select distinct t.topics_id as id, td.topics_name as name from " . TABLE_ARTICLES . " a, " . TABLE_ARTICLES_TO_TOPICS . " a2t, " . TABLE_TOPICS . " t, " . TABLE_TOPICS_DESCRIPTION . " td where a.articles_status = '1' and a.articles_id = a2t.articles_id and a2t.topics_id = t.topics_id and a2t.topics_id = td.topics_id and td.language_id = '" . (int)$languages_id . "' and a.authors_id = '" . (int)$_GET['authors_id'] . "' order by td.topics_name";
				} else {
				   $filterlist_sql= "select distinct au.authors_id as id, au.authors_name as name from " . TABLE_ARTICLES . " a, " . TABLE_ARTICLES_TO_TOPICS . " a2t, " . TABLE_AUTHORS . " au where a.articles_status = '1' and a.authors_id = au.authors_id and a.articles_id = a2t.articles_id and a2t.topics_id = '" . (int)$current_topic_id . "' order by au.authors_name";
				}
				$filterlist_query = tep_db_query($filterlist_sql);
				if (tep_db_num_rows($filterlist_query) > 1) {
				  echo tep_draw_form('filter', tep_href_link(FILENAME_ARTICLES), 'get');
				  if (isset($_GET['authors_id'])) {
					echo tep_draw_hidden_field('authors_id', $_GET['authors_id']);
					$options = array(array('id' => '', 'text' => TEXT_ALL_TOPICS));
				  } else {
					echo tep_draw_hidden_field('tPath', $tPath);
					$options = array(array('id' => '', 'text' => TEXT_ALL_AUTHORS));
				  }
				  echo tep_draw_hidden_field('sort', $_GET['sort']);
				  while ($filterlist = tep_db_fetch_array($filterlist_query)) {
					$options[] = array('id' => $filterlist['id'], 'text' => $filterlist['name']);
				  }
				  echo tep_draw_pull_down_menu('filter_id', $options, (isset($_GET['filter_id']) ? $_GET['filter_id'] : ''), 'class="form-control" onchange="this.form.submit()"');
				  echo '</form><div class="clearfix"></div>' . "\n";
				}
			  }
		  ?>
		   	 </div>
		   	</div>
		   </div>

		   <div class="col-lg-12 col-xs-12 col-sm-12">
		  <?php
			 if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ARTICLE_LISTING)) {
				require(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ARTICLE_LISTING);
			} else {
				require(DIR_WS_MODULES . FILENAME_ARTICLE_LISTING);
			}
		  ?>
		  </div>
	<?php
	  } else { // default page
		$expected_query = tep_db_query("select a.articles_id, a.articles_date_added, a.articles_date_available as date_expected, ad.articles_name, ad.articles_head_desc_tag, au.authors_id, au.authors_name, td.topics_id, td.topics_name from " . TABLE_ARTICLES . " a, " . TABLE_ARTICLES_TO_TOPICS . " a2t, " . TABLE_TOPICS_DESCRIPTION . " td, " . TABLE_AUTHORS . " au, " . TABLE_ARTICLES_DESCRIPTION . " ad where to_days(a.articles_date_available) > to_days(now()) and a.articles_id = a2t.articles_id and a2t.topics_id = td.topics_id and a.authors_id = au.authors_id and a.articles_status = '1' and a.articles_id = ad.articles_id and ad.language_id = '" . (int)$languages_id . "' and td.language_id = '" . (int)$languages_id . "' order by date_expected limit " . MAX_DISPLAY_UPCOMING_ARTICLES);
		if (tep_db_num_rows($expected_query) > 0) {
		  ?>
		<div style="clear:both;"></div>
		<div class="col-lg-12 col-sm-12 col-xs-12 upcoming-article ">
			<?php
				if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ARTICLES_UPCOMING)) {
				  require(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_ARTICLES_UPCOMING);
				} else {
				  require(DIR_WS_MODULES . FILENAME_ARTICLES_UPCOMING);
				}
			?>
		</div>
		<div style="clear:both;"></div>
		  <?php
		}
		?>
			<div class="col-lg-12 col-xs-12 col-md-12 col-xl-12 col-12 col-sm-12 module-heading" style="padding:0px !important;margin-left:0px !important"><h1 class="no-margin-top box-head" style=""><?php echo HEADING_TITLE; ?></h1></div>
		<?php
		$group_access_current_articles = '';
		$articles_all_array = array();
		if($_GET['month'] !='' && $_GET['year'] !='' )
		{
			$articles_all_query_raw = "SELECT a.articles_id, a.articles_date_added,a.articles_image, ad.articles_name, ad.articles_head_desc_tag, au.authors_id, au.authors_name, td.topics_id, td.topics_name from
											" . TABLE_ARTICLES . " a,
											" . TABLE_AUTHORS . " au,
											" . TABLE_ARTICLES_DESCRIPTION . " ad,
											" . TABLE_ARTICLES_TO_TOPICS . " a2t,
											" . TABLE_TOPICS_DESCRIPTION . " td
									   WHERE a.articles_status = '1'
										 and (a.articles_date_available IS NULL or to_days(a.articles_date_available) <= to_days(now()))
										 and a.authors_id = au.authors_id
										 and a.articles_id = a2t.articles_id
										 and a.articles_id = ad.articles_id
										 and a2t.topics_id = td.topics_id
										 and ad.language_id = '" . (int)$languages_id . "'
										 and td.language_id = '" . (int)$languages_id . "' " . $group_access_current_articles . "
										 and YEAR(a.articles_date_added) ='".$_GET['year']."'
										 and MONTH(a.articles_date_added) = '".$_GET['month']."'
									  ORDER BY a.articles_date_added desc, ad.articles_name";

		} else {
			$articles_all_query_raw = "SELECT a.articles_id, a.articles_date_added,a.articles_image, ad.articles_name, ad.articles_head_desc_tag, au.authors_id, au.authors_name, td.topics_id, td.topics_name from
										" . TABLE_ARTICLES . " a,
											" . TABLE_AUTHORS . " au,
											" . TABLE_ARTICLES_DESCRIPTION . " ad,
											" . TABLE_ARTICLES_TO_TOPICS . " a2t,
											" . TABLE_TOPICS_DESCRIPTION . " td
									   WHERE a.articles_status = '1'
										 and (a.articles_date_available IS NULL or to_days(a.articles_date_available) <= to_days(now()))
										 and a.authors_id = au.authors_id
										 and a.articles_id = a2t.articles_id
										 and a.articles_id = ad.articles_id
										 and a2t.topics_id = td.topics_id
										 and ad.language_id = '" . (int)$languages_id . "'
										 and td.language_id = '" . (int)$languages_id . "' " . $group_access_current_articles . "
									  ORDER BY a.articles_date_added desc, ad.articles_name";
		}
$articles_all_split = new splitPageResults_rspv($articles_all_query_raw, MAX_ARTICLES_PER_PAGE);
if (($articles_all_split->number_of_rows > 0) && ((ARTICLE_PREV_NEXT_BAR_LOCATION == 'top') || (ARTICLE_PREV_NEXT_BAR_LOCATION == 'both'))) {
 ?>
		<hr style="border-top: 3px solid #428BCF !important;"></hr>
		 <div class="product-listing-module-pagination margin-bottom" style="padding:0px;">
			<div class="pull-left large-margin-bottom page-results" style="padding-left:0px !important"><?php echo $articles_all_split->display_count(TEXT_DISPLAY_NUMBER_OF_ARTICLES); ?></div>
			<div class="pull-right large-margin-bottom no-margin-top">
			  <ul class="pagination no-margin-top no-margin-bottom box-pagination pagination-box">
			   <?php echo  $articles_all_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?>
			  </ul>
			</div>
		 </div><div class="clear-both"></div>
		  <?php
		   }
		  ?>
		  <div class="col-lg-12 col-xs-12 col-md-12 col-xl-12 col-12 col-sm-12 table-content">
			<?php
			if ($articles_all_split->number_of_rows > 0) {
			  $articles_all_query = tep_db_query($articles_all_split->sql_query);
			  while ($articles_all = tep_db_fetch_array($articles_all_query)) {
			  	include(DIR_WS_MODULES.'article_listing_view.php');
			  } // End of listing loop
			} else {
			  ?>
			<div  class="main"><?php echo TEXT_NO_ARTICLES; ?></div>
		  <?php
			}
			?>
		  </div>

	  <div class="clearfix"></div>
	  <div class="content-product-listing-div">

	<?php if (($articles_all_split->number_of_rows > 0) && ((ARTICLE_PREV_NEXT_BAR_LOCATION == 'bottom') || (ARTICLE_PREV_NEXT_BAR_LOCATION == 'both'))) {?>
		  <div class="col-lg-12 col-xs-12 product-listing-module-pagination margin-bottom" style="">
			<div class="pull-left large-margin-bottom page-results" style="padding-left:0px !important"><?php echo $articles_all_split->display_count(TEXT_DISPLAY_NUMBER_OF_ARTICLES); ?></div>
			<div class="pull-right large-margin-bottom no-margin-top">
			  <ul class="pagination no-margin-top no-margin-bottom box-pagination pagination-box">
			   <?php echo  $articles_all_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?>

			  </ul>
			</div>
		  </div><div class="clear-both"></div>
	<?php
	  }
	?>
	</div>
	<?php
		// RCI code start
		echo $cre_RCI->get('articles', 'menu');
		// RCI code eof
	}
	?>
	 </div>
	<div class="col-lg-3 col-12" style="float:left;">
      <?php include('templates/default/boxes/asearch.php');
      include('templates/default/boxes/articles.php');
      include('templates/default/boxes/article_archive.php');
      ?>
	</div>
</div>

<?php
// RCI code start
echo $cre_RCI->get('articles', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>

