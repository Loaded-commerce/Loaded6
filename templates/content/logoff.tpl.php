<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('logoff', 'top');
// RCI code eof
?>
<div class="row">
  <div class="col-sm-12 col-lg-12 col-xs-12 no-padding">
    <h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1>
    <div class="well">
      <p><?php echo TEXT_MAIN; ?></p>
    </div>
      <div class="button-set clearfix">
      <form action="<?php echo tep_href_link(FILENAME_DEFAULT); ?>" method="post"><button class="pull-right btn btn-lg btn-primary">Continue</button></form>

    </div>
  </div>
</div>
<?php
// RCI code start
echo $cre_RCI->get('logoff', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>