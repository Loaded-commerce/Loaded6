<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('shoppingcart', 'top');
// RCI code eof

?>
<div class="row">
  <div class="col-sm-12 col-lg-12 col-xs-12 no-padding">
<?php
// BOF: Lango Added for template MOD
if (SHOW_HEADING_TITLE_ORIGINAL == 'yes') {
$header_text = '&nbsp;'
//EOF: Lango Added for template MOD
?>
<div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 pl-sm-2 pl-2 pl-xl-0 pl-lg-0 pl-md-2 module-heading"><h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1></div>
<?php
}else{
$header_text = '<div class="col-xl-12 col-lg-12 col-md-12 col-12 col-sm-12 pl-sm-2 pl-2 pl-xl-0 pl-lg-0 pl-md-2 module-heading"><h1 class="no-margin-top">'. HEADING_TITLE .'</h1></div>';
}
?>
<div class="row">
<div class="loader-cart" id="loader-cart">
    <img src="<?php echo DIR_WS_DEFAULT ?>ajax-loader.gif" id="loaderimg"/>
 </div>
<?php
  if ($cart->count_contents() > 0) { ?>
  <div class="col-lg-8 col-xl-8 col-md-10 col-sm-12 col-12 left-shop-cart m-auto">
    <div class="left-shop-cart-inner">
  <?php
     echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_SHOPPING_CART, 'action=update_product'),'post','id="cart_quantity"');
  ?>
        <table class="table tabled-striped table-responsive no-margin-bottom" id="shopping-cart-table">
          <thead>
            <tr>
              <th class="hide-on-mobile-portrait"></th>
              <th class="text-left hide-on-mobile-portrait"></th>
              <th class="text-center large-padding-left"></th>
              <th class="content-price-th"></th>
              <th class="text-center hide-on-mobile-portrait"></th>
            </tr>
          </thead>
		  <tbody>

				<?php

					$any_out_of_stock = 0;
					$products = $cart->get_products();

					for ($i=0, $n=sizeof($products); $i<$n; $i++) {
					  $db_sql = "select products_parent_id from " . TABLE_PRODUCTS . " where products_id = " . (int)$products[$i]['id'];
					  $products_parent_id = tep_db_fetch_array(tep_db_query($db_sql));
				// Push all attributes information in an array
					  if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
						$attribute_product_id = (int)$products[$i]['id'];
						if ((int)$products_parent_id['products_parent_id'] != 0) {
						  $attribute_product_id = (int)$products_parent_id['products_parent_id'];
						}

						reset($products[$i]['attributes']);
					    foreach($products[$i]['attributes'] as $option => $value) {
						  if ( ! is_array($value) ) {
							$attributes = tep_db_query("select op.options_id, ot.products_options_name, o.options_type, ov.products_options_values_name, op.options_values_price, op.price_prefix
														from " . TABLE_PRODUCTS_ATTRIBUTES . " op,
															 " . TABLE_PRODUCTS_OPTIONS_VALUES . " ov,
															 " . TABLE_PRODUCTS_OPTIONS . " o,
															 " . TABLE_PRODUCTS_OPTIONS_TEXT . " ot
														where op.products_id = " . $attribute_product_id . "
														  and op.options_values_id = " . $value . "
														  and op.options_id = " . $option . "
														  and ov.products_options_values_id = op.options_values_id
														  and ov.language_id = " . (int)$languages_id . "
														  and o.products_options_id = op.options_id
														  and ot.products_options_text_id = o.products_options_id
														  and ot.language_id = " . (int)$languages_id . "
													   ");
							$attributes_values = tep_db_fetch_array($attributes);

							$products[$i][$option][$value]['products_options_name'] = $attributes_values['products_options_name'];
							$products[$i][$option][$value]['options_values_id'] = $value;
							$products[$i][$option][$value]['products_options_values_name'] = $attributes_values['products_options_values_name'];
							$products[$i][$option][$value]['options_values_price'] = $attributes_values['options_values_price'];
							$products[$i][$option][$value]['price_prefix'] = $attributes_values['price_prefix'];

						  } elseif ( isset($value['c'] ) ) {
							foreach ($value['c'] as $v) {
							  $attributes = tep_db_query("select op.options_id, ot.products_options_name, o.options_type, ov.products_options_values_name, op.options_values_price, op.price_prefix
														  from " . TABLE_PRODUCTS_ATTRIBUTES . " op,
															   " . TABLE_PRODUCTS_OPTIONS_VALUES . " ov,
															   " . TABLE_PRODUCTS_OPTIONS . " o,
															   " . TABLE_PRODUCTS_OPTIONS_TEXT . " ot
														  where op.products_id = " . $attribute_product_id . "
															and op.options_values_id = " . $v . "
															and op.options_id = " . $option . "
															and ov.products_options_values_id = op.options_values_id
															and ov.language_id = " . (int)$languages_id . "
															and o.products_options_id = op.options_id
															and ot.products_options_text_id = o.products_options_id
															and ot.language_id = " . (int)$languages_id . "
														 ");
							  $attributes_values = tep_db_fetch_array($attributes);

							  $products[$i][$option][$v]['products_options_name'] = $attributes_values['products_options_name'];
							  $products[$i][$option][$v]['options_values_id'] = $v;
							  $products[$i][$option][$v]['products_options_values_name'] = $attributes_values['products_options_values_name'];
							  $products[$i][$option][$v]['options_values_price'] = $attributes_values['options_values_price'];
							  $products[$i][$option][$v]['price_prefix'] = $attributes_values['price_prefix'];
							}

						  } elseif ( isset($value['t'] ) ) {
							$attributes = tep_db_query("select op.options_id, ot.products_options_name, o.options_type, op.options_values_price, op.price_prefix
														from " . TABLE_PRODUCTS_ATTRIBUTES . " op,
															 " . TABLE_PRODUCTS_OPTIONS . " o,
															 " . TABLE_PRODUCTS_OPTIONS_TEXT . " ot
														where op.products_id = " . $attribute_product_id . "
														  and op.options_id = " . $option . "
														  and o.products_options_id = op.options_id
														  and ot.products_options_text_id = o.products_options_id
														  and ot.language_id = " . (int)$languages_id . "
													   ");
							$attributes_values = tep_db_fetch_array($attributes);

							$products[$i][$option]['t']['products_options_name'] = $attributes_values['products_options_name'];
							$products[$i][$option]['t']['options_values_id'] = '0';
							$products[$i][$option]['t']['products_options_values_name'] = $value['t'];
							if(get_Options_type($option) == 5 && $value['t'] == 'Yes'){
								$products[$i][$option]['t']['options_values_price'] = $attributes_values['options_values_price'];
							}
							$products[$i][$option]['t']['price_prefix'] = $attributes_values['price_prefix'];
						  }
						}
					  }
					}
					for ($i=0, $n=sizeof($products); $i<$n; $i++) {
					  if (($i/2) == floor($i/2)) {
						$info_box_contents[] = array('params' => 'class="productListing-even"');
					  } else {
						$info_box_contents[] = array('params' => 'class="productListing-odd"');
					  }

					  $cur_row = sizeof($info_box_contents) - 1;

					   /*attribute data starts*/
					 $attribute_data = '<div id="cart-attributes'.$products[$i]['id'].'">';
					 if (isset($products[$i]['model']) && $products[$i]['model'] != '') {
					  $attribute_data .= 'Code: '. $products[$i]['model'];
					 }
					 $attribute_data .= '<br/>Unit Price: '. $currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id']), 1);
					  if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
						reset($products[$i]['attributes']);
					    foreach($products[$i]['attributes'] as $option => $value) {
						  if ( !is_array($value) ) {
							if ($products[$i][$option][$value]['options_values_price'] > 0 ){
							  $attribute_price = $products[$i][$option][$value]['price_prefix']  . $currencies->display_price($products[$i][$option][$value]['options_values_price'], tep_get_tax_rate($products[$i]['tax_class_id']));
							} else {
							  $attribute_price ='';
							}
							$attribute_data .= '<br>' . ' - ' . '<small><i>' . $products[$i][$option][$value]['products_options_name'] . ' : ' . $products[$i][$option][$value]['products_options_values_name'] . '&nbsp;&nbsp;&nbsp;' .$attribute_price . '</i></small>';
						  } else {
							if ( isset($value['c']) ) {
							  foreach ( $value['c'] as $v ) {
								if ($products[$i][$option][$v]['options_values_price'] > 0 ){
								  $attribute_price = $products[$i][$option][$v]['price_prefix']  . $currencies->display_price($products[$i][$option][$v]['options_values_price'], tep_get_tax_rate($products[$i]['tax_class_id']));
								} else {
								  $attribute_price ='';
								}
								$attribute_data .= '<br>' . ' - ' . '<small><i>' . $products[$i][$option][$v]['products_options_name'] . ' : ' . $products[$i][$option][$v]['products_options_values_name'] . '&nbsp;&nbsp;&nbsp;' .$attribute_price . '</i></small>';
							  }
							} elseif ( isset($value['t']) ) {
							  if ($products[$i][$option]['t']['options_values_price'] > 0 ){
								$attribute_price = $products[$i][$option]['t']['price_prefix']  . $currencies->display_price($products[$i][$option]['t']['options_values_price'], tep_get_tax_rate($products[$i]['tax_class_id']));
							  } else {
								$attribute_price ='';
							  }
							  $attribute_data .= '<br>' . ' - ' . '<small><i>' . $products[$i][$option]['t']['products_options_name'] . ' : ' . $value['t'] . '&nbsp;&nbsp;&nbsp;' . $attribute_price . '</i></small>';
							}
						  }
						}
					  }
					 $attribute_data .= '</div>';

				  /*attribute data eof*/

				echo	'<tr class="prodname-show-on-mobile-tr">';
					if ((int)$products_parent_id['products_parent_id'] != 0) {
							   echo '<td colspan="4" class="text-left prod-cart-name prodname-show-on-mobile"><p style="margin-bottom:0px;">'. tep_draw_checkbox_field('cart_delete[]', $products[$i]['id_string']).'
									 <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products_parent_id['products_parent_id']) . '">' . tep_image(DIR_WS_PRODUCTS . $products[$i]['image'], $products[$i]['name'], 70, 70) . '</a>
									 <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products_parent_id['products_parent_id']) . '?popup=yes" id="iframe"><b>' . $products[$i]['name'] . '</b></a></p><p class="view-all" onclick="cartattributeshowhide('.$products[$i]['id'].','.$i.')">View More</p>
									 <div class="shopping-cart-attribute-div" id ="attributediv'.$products_parent_id['products_parent_id'].'-'.$i.'" style="display:none;">'.$attribute_data.' </div>
									 </td>';

				   } else {
							   echo '<td colspan="4" class="text-left prod-cart-name prodname-show-on-mobile"><p style="margin-bottom:0px;display:inline-flex;">'. tep_draw_checkbox_field('cart_delete[]', $products[$i]['id_string'], '' ,'class="mob-check"').'
									 <a class="mob-crtimg" href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[$i]['id']) . '">' . tep_image(DIR_WS_PRODUCTS . $products[$i]['image'], $products[$i]['name'], 70, 70) . '</a>
									 <a class="mob-crtname" href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[$i]['id']) . '?popup=yes" id="iframe">' . $products[$i]['name'] . '</br>Model: ' . $products[$i]['model'] . '</br>Unit Price: $' . number_format($products[$i]['price'],2) . '</a></p>
									 <div style="clear:both"></div>';
									 if(isset($products[$i]['attributes']) && is_array($products[$i]['attributes']) || (isset($products[$i]['attributes']) && $products[$i]['attributes'] != '')){
									   echo '<p class="view-all" onclick="cartattributeshowhide('.$products[$i]['id'].','.$i.')">View More</p>
									   <div class="shopping-cart-attribute-div" id ="attributediv'.$products[$i]['id'].'-'.$i.'" style="display:none;">'.$attribute_data.' </div>';
									 }
									 echo '</td>';


					}
				echo	'</tr>';

				echo	'<tr class="buttom-td">';
				///////////////////////////////////////////////////////////////////////////////////////////////////////
				// MOD begin of sub product
					if ((int)$products_parent_id['products_parent_id'] != 0) {
					$products_name =
									   '  ' .
									   '<td class="text-center content-shopping-cart-image-td hide-on-mobile-portrait">    <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products_parent_id['products_parent_id']) . '">' . tep_image(DIR_WS_PRODUCTS . $products[$i]['image'], $products[$i]['name'], '', 80) . '</a></td>' .
									   '    <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products_parent_id['products_parent_id']) . '" class="prod-name-cart"><b>' . $products[$i]['name'] . '</b></a>';
					} else {
					$products_name =
									   '  ' .
									   '   <td class="text-center content-shopping-cart-image-td hide-on-mobile-portrait"> <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[$i]['id']) . '">' . tep_image(DIR_WS_PRODUCTS . $products[$i]['image'], $products[$i]['name'], '', 80) . '</a></td>' .
									   '  <td class="text-left hide-on-mobile-portrait content-shopping-cart-prodname-td">  <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[$i]['id']) . '" class="prod-name-cart">' . $products[$i]['name'] . '</a>';
					}

					  if (STOCK_CHECK == 'true') {
						$stock_check = tep_check_stock((int)$products[$i]['id'], $products[$i]['quantity']);
						if (tep_not_null($stock_check)) {
						  $any_out_of_stock = 1;
						  $products_name .= $stock_check;
						}
					  }


					  $products_name .= $attribute_data;
					  echo  $products_name;


					// echo '<td class="text-right hide-on-mobile-portrait content-shopping-cart-price-td">' . $currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id'])) . '</td>';
					 echo '<td class="text-right content-shopping-cart-qty-input-td">' .tep_draw_input_field('cart_quantity[]', $products[$i]['quantity'],'class="form-control"' ) . tep_draw_hidden_field('products_id[]', $products[$i]['id_string']).'</td>';
					  echo  ' <td class="content-price-td hide-on-mobile-portrait"><span class="price">' . $currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id']), $products[$i]['quantity']) . '</span></td>';
					  echo  ' <td class="text-right content-shopping-cart-total-price-td " colspan="2"><span class="total-label">Item Total: </span><span class="price">' . $currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id']), $products[$i]['quantity']) . '</span></td>';
					  echo '<td class="text-center content-shopping-cart-remove-td hide-on-mobile-portrait"><label class="custom-radio-container">'. tep_draw_checkbox_field('cart_delete[]', $products[$i]['id_string'],'','id="cart_delete"').'<span class="checkmark cartremove" style="left:unset;margin-left: -12px;"></span></label></td>';
				echo '</tr>';

					}

				?>
        </tbody>
          <tfoot>
            <tr>
              <td></td>
              <td class="hide-on-mobile-portrait"></td>
              <td class="hide-on-mobile-portrait"></td>
              <td colspan="3"></td>

            </tr>
          </tfoot>

   	   </table>

		<?php    if ($any_out_of_stock == 1) {
			  if (STOCK_ALLOW_CHECKOUT == 'true') {
				$valid_to_checkout = true;
		?>

				<p class="alert alert-danger text-center"><?php echo OUT_OF_STOCK_CAN_CHECKOUT; ?></p>

		<?php
			  } else {
				$valid_to_checkout= false;
		?>

			   <p class="alert alert-danger text-center"><?php echo OUT_OF_STOCK_CANT_CHECKOUT; ?><p>

		<?php
			  }
			}
			if (RETURN_CART == 'L'){
			   $back = sizeof($navigation->path)-2;
				if (isset($navigation->path[$back])) {
					/***** Fix ********/
					$link_vars_post = tep_array_to_string($navigation->path[$back]['post'], array('cart_quantity','id'));
					$link_vars_get = tep_array_to_string($navigation->path[$back]['get'], array('action'));

					$return_link_vars = '';
					if($link_vars_get != '' && $link_vars_post !=''){
						$return_link_vars = $link_vars_get . '&' . $link_vars_post;
					} else if($link_vars_get != '' && $link_vars_post == ''){
						$return_link_vars = $link_vars_get;
					} else if($link_vars_get == '' && $link_vars_post != ''){
						$return_link_vars = $link_vars_post;
					}

				   $nav_link = '<div class="large-margin-bottom pull-right"><button onclick="javascript:self.location=\'' . tep_href_link($navigation->path[$back]['page'], $return_link_vars, $navigation->path[$back]['mode']) . '\';" class="btn btn-primary btn-special-continue disabled btn-lg"> ' .  IMAGE_BUTTON_CONTINUE_SHOPPING . '</button></div>';
				   //$nav_link = '<a href="' . tep_href_link($navigation->path[$back]['page'], tep_array_to_string($navigation->path[$back]['get'], array('action')), $navigation->path[$back]['mode']) . '">' . tep_template_image_button('button_continue_shopping.gif', IMAGE_BUTTON_CONTINUE_SHOPPING) . '</a>';
				   /***** fix end ****/
				}
			 } else if ((RETURN_CART == 'C') || (isset($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'], 'wishlist'))){
			  if (!stristr($_SERVER['HTTP_REFERER'], 'wishlist')) {
				$products = $cart->get_products();
				$products = array_reverse($products);
				$cat = tep_get_product_path($products[0]['id']) ;
				$cat1= 'cPath=' . $cat;
				if ($products == '') {
				  $back = sizeof($navigation->path)-2;
				  if (isset($navigation->path[$back])) {
					$nav_link = '<button onclick="javascript:self.location=\'' . tep_href_link($navigation->path[$back]['page'], tep_array_to_string($navigation->path[$back]['get'], array('action')), $navigation->path[$back]['mode']) . '\';" class="btn btn-primary btn-special-continue btn-lg">' .  IMAGE_BUTTON_CONTINUE_SHOPPING . '</button>';
				  }
				}else{
				  $nav_link = '<button onclick="javascript:self.location=\'' . tep_href_link(FILENAME_DEFAULT, $cat1) . '\';" class="btn btn-primary btn-special-continue btn-lg" type="button">' . IMAGE_BUTTON_CONTINUE_SHOPPING . '</button>'  ;
				}
			  }else{
				$nav_link = '<button href="' . tep_href_link(FILENAME_DEFAULT) . '" class="btn btn-primary btn-special-continue btn-lg" type="button">' . IMAGE_BUTTON_CONTINUE_SHOPPING . '</button>'  ;
			  }
			} else if (RETURN_CART == 'P'){
			  $products = $cart->get_products();
			  $products = array_reverse($products);
			  if ($products == '') {
				$back = sizeof($navigation->path)-2;
				  if (isset($navigation->path[$back])) {
					$nav_link = '<button onclick="javascript:self.location=\'' . tep_href_link($navigation->path[$back]['page'], tep_array_to_string($navigation->path[$back]['get'], array('action')), $navigation->path[$back]['mode']) . '\';" class="btn btn-primary btn-special-continue btn-lg" type="button">' . IMAGE_BUTTON_CONTINUE_SHOPPING . '</button>';
				  }
			  }else{
				$nav_link = '<div class="large-margin-bottom pull-left" ><button onclick="javascript:self.location=\'' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products[0]['id']) . '\';" class="btn btn-primary btn-special-continue btn-lg" type="button">' .  IMAGE_BUTTON_CONTINUE_SHOPPING . '</button></div>';
			  }
			}
	?>

		<div class="row" id="content-shopping-cart-updatecart" style="margin-left:0px;">
		  <div class="col-lg-6 col-xl-6 col-md-6 col-sm-7 col-7 btn-continue-shopping-div">
			 <?php echo $nav_link; ?>
		  </div>
		  <div class="col-lg-6 col-xl-6 col-md-6 col-sm-56 col-5 text-right btn-updatecart-div">
			<?php echo tep_template_image_submit('index.png', IMAGE_BUTTON_UPDATE_CART)  ;?>
		  </div>
		</div>



<?php  if ((SHIPPING_SKIP == 'No' || SHIPPING_SKIP == 'If Weight = 0') && $cart->weight > 0) {
         if (SHOW_SHIPPING_ESTIMATOR == 'true') { ?>
           <div class="col-lg-12 col-xl-12 col-md-12 col-12 col-sm-12 text-right shipping-estimate-cartbox">
              <div class="shipping-estimate-cartbox-inner" id="calculate-shipping">
               <div class="calculate-shipping">
                <span><b>Estimated Shipping Cost:</b></span>
                <span>Add Zip Code</span>
                <?php //echo '</pre>'; print_r($_SESSION);
                      $selected_country = ((isset($_SESSION['customer_country_id']) && $_SESSION['customer_country_id'] > 0 )?$_SESSION['customer_country_id']:STORE_COUNTRY);
                      //echo $selected_country;
                     $selected_zip = tep_get_customer_zipcode($_SESSION['customer_id']);
                      echo tep_draw_input_field('zip_code', $selected_zip, 'size="10" class="form-control zip_code_cart" placeholder="Zipcode"');
                      echo tep_get_country_list('country_id', $selected_country,'style="" class="form-control country_id_cart"') ?>
                      <i class="fa fa-repeat" aria-hidden="true" onclick="javascript:calculate_shipping_estimate()" style="font-size:22px;font-weight:600;color:#666666;text-align: right;float: right;margin-left: 13px;margin-top: 8px;cursor:pointer;"></i>

               </div>
               <div id="show-calculated-shipping">
                <div class="row">
                  <div class="col-lg-6 col-xl-6 col-md-6 col-12 col-sm-12"></div>
                  <div class="col-lg-6 col-xl-6 col-md-6 col-12 col-sm-12 text-left">
                    <span>Estimate your shipping cost </span>
                  </div>
                </div>
               </div>
              </div>
           </div>
     <?php }
         } ?>




      <div class="row" id="content-shopping-cart-order-totals">
        <div id="content-shopping-cart-order-totals-left-blank" class="col-sm-6 col-lg-7 col-xl-7 col-12 col-sm-12 no-padding-left">
        </div>
        <div id="content-shopping-cart-order-totals-right" class="col-sm-6 col-lg-5 col-xl-5 col-12 col-sm-12 no-padding">
          <div class="clearfix col-lg-12 col-xs-12 order-totalouter">
               <?php ob_start(); ?>
					  <table class="table table-cart-total">
					   <?php
						  // RCI code start
						  $offset_amount = 0;
						  $returned_rci = $cre_RCI->get('shoppingcart', 'offsettotal');
						  // RCI code eof
						  if (trim(strip_tags($returned_rci)) != NULL) {
							echo '<tr>' . "\n";
							echo '  <td align="right"><table cellspacing="2" cellpadding="2" border="0">' . "\n";
							echo '    <tr>' . "\n";
							echo '      <td class="text-right"><b>' . SUB_TITLE_SUB_TOTAL . '</b></td>' . "\n";
							echo '      <td class="text-right" style="width: 15%;"><b>' . $currencies->format($cart->show_total()) . '</b></td>' . "\n";
							echo '    </tr>' . "\n";
							echo $returned_rci;
							echo '    <tr>' . "\n";
							echo '      <td class="text-right"><b>' . SUB_TITLE_TOTAL . '</b></td>' . "\n";
							echo '      <td class="text-right" style="width: 15%;"><b>' . $currencies->format($cart->show_total() + $offset_amount) . '</b></td>' . "\n";
							echo '    </tr>' . "\n";
							echo '  </table></td>' . "\n";
							echo '</tr>' . "\n";
						  } else {
							$order_total_modules->process();
							echo $order_total_modules->output();

						  }//echo tep_template_image_submit('index.png', IMAGE_BUTTON_UPDATE_CART)  ;
					?>
					   </table>
				<?php $ordertodat_data = ob_get_clean();
				echo $ordertodat_data; ?>
           </div>
          </div>
       </div>

		<?php
		// RCI code start
		echo $cre_RCI->get('shoppingcart', 'insideformabovebuttons');
		// RCI code eof
		if(defined('MODULE_ORDER_TOTAL_COUPON_STATUS') && MODULE_ORDER_TOTAL_COUPON_STATUS == 'true'){
		?>

           <div class="col-lg-12 col-xl-12 col-md-12 col-12 col-sm-12 text-right cart-coupon-box">
			<div id="content-shopping-cart-order-totals-left" class="col-sm-12 col-lg-12 col-xl-12 col-12 col-sm-12 no-padding-left text-right">
				  <div class="cart-coupon-box-inner">
					<span class="coupon-text-cart"><b><?php echo TEXT_COUPON_HEADIBNG_CART; ?></b></span>
					<?php echo tep_draw_input_field('gv_redeem_code', '','class="form-control gv_redeem_code" id="gv_redeem_code" style=""' ); ?>
					<button class="btn btn-lg btn-success btn-cart-coupon" type="button" onclick="javascript:redeem();">Redeem</button>
				  </div>
			</div>
           </div>

          <?php
         }
          //RCI start
          echo $cre_RCI->get('shoppingcart', 'insideformbelowbuttons');
          //RCI end
          ?>
        </form>
 </div><!--left-section-inner-ends-->
</div><!--left-section-ends-->



  <div class="col-lg-4 colxl-4 col-md-10 col-sm-12 col-12 right-shop-cart m-auto">
    <div class="right-shop-cart-inner">
       <p class="to-checkout">To Checkout</p>
      <?php  if(!isset($_SESSION['customer_id'])){ ?>
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item">
			  <a class="nav-link creat_acc active" data-toggle="tab" href="#new-customer">New Customer</a>
			</li>
			<li class="nav-item">
			  <a class="nav-link login_acc" data-toggle="tab" href="#cart-login">Login</a>
			</li>
		  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
			<div id="new-customer" class="create_acc_tab tab-pane active">
			  <?php $get_customer_data = tep_db_fetch_array(tep_db_query("select c.`customers_id`,c.`purchased_without_account`, c.`customers_firstname`,c.`customers_lastname`,c.`customers_email_address`,c.`customers_default_address_id` , ab.`address_book_id`,ab.`entry_company_tax_id`,ab.`entry_company`from customers c , address_book ab where c.`customers_id` = '".$_SESSION['customer_id']."' and c.customers_default_address_id = ab.address_book_id"));
			        echo tep_draw_form('create_account', tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'), 'post', 'onSubmit="return check_form(create_account);" enctype="multipart/form-data"') . tep_draw_hidden_field('action', 'process').tep_draw_hidden_field('return_to', 'cart').tep_draw_hidden_field('address_book_id', $get_customer_data['address_book_id']);
			        echo (isset($_SESSION['customer_id'])?tep_draw_hidden_field('guest_edit', $get_customer_data['purchased_without_account']):'');
			  ?>
                  <label class="custom-radio-container show-comp-box" style="padding-left:25px;"><input type="checkbox" name="show_company" id="show_company" value="1" onchange="javascript:show_companyadd(this.value);"><span class="checkmark comp-checkmark"></span>&nbsp;&nbsp;I am a company</label>
                  <div class="" id="show_company_field" style="display:none">
					  <div class="form-group full-width margin-bottom">
						<label class="" style=""><?php echo ENTRY_COMPANY.':'; ?></label>
						<?php echo tep_draw_input_field('company', '' , 'class="form-control" placeholder="' . ENTRY_COMPANY . '"'); ?>
					  </div>
					  <div class="form-group full-width margin-bottom">
						<label class="" style="">Tax ID:</label>
						<?php echo tep_draw_input_field('company_tax_id', '' , 'class="form-control" placeholder="' . ENTRY_COMPANY_TAX_ID . '"'); ?>
					  </div>
                  </div>
                  <div class="form-group full-width margin-bottom">
                    <label class="" style=""><?php echo ENTRY_FIRST_NAME.':'; ?></label>
                    <?php echo tep_draw_input_field('firstname', $_POST['firstname'] , 'class="form-control" placeholder="' . ENTRY_FIRST_NAME . '"'); ?>
                  </div>
                  <div class="form-group full-width margin-bottom">
                    <label class="" style=""><?php echo ENTRY_LAST_NAME.':'; ?></label>
                    <?php echo tep_draw_input_field('lastname', '' , 'class="form-control" placeholder="' . ENTRY_LAST_NAME . '"'); ?>
                  </div>
                  <div class="form-group full-width margin-bottom">
                    <label class="" style=""><?php echo ENTRY_EMAIL_ADDRESS.':'; ?></label>
                    <?php echo tep_draw_input_field('email_address_creat', '' , 'class="form-control" placeholder="' . ENTRY_EMAIL_ADDRESS . '"'); ?>
                  </div>
                <?php if (PWA_ON == 'true') { ?>
                  <label class="custom-radio-container showpass-box" style="padding-left:27px !important;"><input type="checkbox" name="show_passwords" id="show_passwords" value="1" onchange="javascript:show_custpasswords(this.value);"><span class="checkmark" style="left: -4px;!important;"></span> I want to create a account</label>
                <?php } ?>
                  <div class="" id="show_password_fields" style="<?php echo ((PWA_ON == 'true')?'display:none':''); ?>">
					  <div class="form-group full-width margin-bottom">
						<label class="" style=""><?php echo ENTRY_PASSWORD.':'; ?></label>
						<?php echo tep_draw_password_field('password', '' , 'class="form-control" placeholder="' . ENTRY_PASSWORD . '" autocomplete="off"'); ?>
					  </div>
					  <div class="form-group full-width margin-bottom">
						<label class="" style="">Repeat Password:</label>
						<?php echo tep_draw_password_field('confirmation', '' , 'class="form-control" placeholder="' . ENTRY_PASSWORD_CONFIRMATION . '" autocomplete="off"'); ?>
					  </div>
                  </div>
						<?php

						if (defined('VVC_CREATE_ACCOUNT_ON_OFF') && VVC_CREATE_ACCOUNT_ON_OFF == 'On'){
							$captcha_instructions = trim($captcha->display_captcha_instructions());
						?>
						<!-- Captcha CODE start -->
						<h5><?php echo $captcha->display_captcha_label(); ?></h5>
		    			<div class="form-group full-width margin-bottom captcha-img"><?php echo $captcha->render_captcha_image();?><?php echo $captcha->render_captcha_control('class="form-control"');?></div>
						<?php if($captcha_instructions != '') {?>
		    			<div class="form-group full-width margin-bottom"><?php echo $captcha_instructions; ?></div>
						<?php } ?>
						<!-- Captcha VERIFY CODE end -->
						<?php }  ?>
                <?php
                if($valid_to_checkout == true){
                echo '<div class="margin-top mid-margin-bottom pull-right col-lg-12 col-xs-12 btn-checkout-cart" style=""><button class="btn btn-lg btn-success btn-cart-checkout" type="button" onclick="javascript:checkemail(create_account)">' .  IMAGE_BUTTON_CHECKOUT . '</button></div>';
                  /*<a href="' . tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL') . '" class="btn btn-lg btn-success btn-cart-checkout" type="button">' .  IMAGE_BUTTON_CHECKOUT . '</a></div>'; */
                }
                ?>
                <p class="text-center mandetory-fields clearfix">All fields are required</p>
			  </form>
			</div>
			<div id="cart-login" class="container login_acc_tab tab-pane fade">
			  <?php echo tep_draw_form('login', tep_href_link(FILENAME_LOGIN, '', 'SSL'),'post').tep_draw_hidden_field('action','process').tep_draw_hidden_field('login_return_to', 'login_cart'); ?>
                  <div class="form-group full-width margin-bottom">
                    <?php echo "<label class=\"\">Email Address:</label>   " .tep_draw_input_field('email_address', '' , 'class="form-control" placeholder="' . ENTRY_EMAIL_ADDRESS . '"') ?>
                  </div>
                  <div class="form-group full-width margin-bottom">
                    <?php echo "<label class=\"\">Password:</label>      " . tep_draw_password_field('password', '' , 'class="form-control" placeholder="' . MENU_TEXT_PASSWORD . '"') ?>
                  </div>
                  <?php echo '<p class="text-center"><a href="' . tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL') . '" style="text-decoration: underline;"> Forgot Password? </a></p>'; ?>
                <?php
                if($valid_to_checkout == true){
                echo '<div class="margin-top small-margin-bottom pull-right col-lg-12 col-xs-12 btn-checkout-cart" style=""><button class="btn btn-lg btn-success btn-cart-checkout">' .  IMAGE_BUTTON_CHECKOUT . '</button></div>';
                }
                ?>
			  </form>
			</div>
		  </div>
	<?php }else{
                echo $ordertodat_data;
                if($valid_to_checkout == true){
                echo '<div class="margin-top large-margin-bottom col-lg-12 col-xs-12 btn-checkout-cart" style=""><a href="' . tep_href_link(FILENAME_CHECKOUT, '', 'SSL') . '" class="btn btn-lg btn-success btn-cart-checkout" type="button">' .  IMAGE_BUTTON_CHECKOUT . '</a></div>';
                }

	} ?>
		</div>
    </div><!--right-section-ends-->
  </div><!--right-section-inner-ends-->

</div><!--2nd row ends-->

  <?php
  //RCI start
  echo $cre_RCI->get('shoppingcart', 'logic');
  //RCI end

  // WebMakers.com Added: Shipping Estimator
/*  if ((SHIPPING_SKIP == 'No' || SHIPPING_SKIP == 'If Weight = 0') && $cart->weight > 0) {
    if (SHOW_SHIPPING_ESTIMATOR == 'true') {
      // always show shipping table
      ?>

        <?php
         if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_SHIPPING_ESTIMATOR)) {
            require(TEMPLATE_FS_CUSTOM_MODULES . FILENAME_SHIPPING_ESTIMATOR);
        } else {
            require(DIR_WS_MODULES . FILENAME_SHIPPING_ESTIMATOR);
        }
         ?>

      <?php
    }
  } */
} else {
  ?>

    <?php

    if (isset($_GET['hide_add_to_cart_error']) &&     (int)$_GET['hide_add_to_cart_error'] == 1) {
      echo TEXT_HIDE_ADD_TO_CART_ERROR;
    } else { ?>
       <div class="col-lg-8 col-xl-8 col-md-8  col-12 col-sm-12 empty-my-cart">
         <div class="empty-my-cart-inner">
            <img class="img-humbnail empty-cart-img" src="<?php echo DIR_WS_DEFAULT ?>loaded-shopping-cart-empty.jpg" style="height:200px;	">
				<div class="large-margin-top ">
				  <p class="empty-cart-headingtext"><?php echo TEXT_CART_EMPTY ; ?></p>
				  <p class="empty-cart-subheadingtext"><?php echo TEXT_CART_EMPTY_SUB ; ?></p>
				</div>
		 </div>
       </div>
  <?php  }
  ?>


      <div class="col-lg-12 col-xl-12 col-md-12 col-12 col-sm-12 btn-set clearfix">
        <?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT) . '"><button class="pull-right btn btn-lg btn-primary" type="submit">' .IMAGE_BUTTON_CONTINUE  . '</button></a>'; ?>
      </div>


<?php
  }
?>
    </div>
   </div>
	<div id="emailavailable" class="modal fade" role="dialog">
	  <div class="modal-dialog" style="max-width:600px;">
	    <div class="modal-content">
			  <div class="modal-header">
				<h4 style="font-size:18px;font-weight:550;color:#444444;">An account with this email address already exists. Would you like to login?</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  </div>
			  <div class="modal-body text-center">
			     <?php echo tep_draw_form('create_account_pop', tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'), 'post', 'enctype="multipart/form-data"') . tep_draw_hidden_field('action', 'process').tep_draw_hidden_field('return_to', 'cart'); ?>
			        <input type="hidden" name="email_address_creat" id="email_address">
					<?php echo tep_draw_hidden_field('company', '' , 'id="company"'); ?>
					<?php echo tep_draw_hidden_field('company_tax_id', '' , 'id="company_tax_id"'); ?>
                    <?php echo tep_draw_hidden_field('firstname', $_POST['firstname'] , 'id="firstname"'); ?>
                    <?php echo tep_draw_hidden_field('lastname', '' , 'id="lastname"'); ?>
                    <?php echo tep_draw_hidden_field('ccode', '' , 'id="ccode"'); ?>

				    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="javascript:login_withexists();">Login</button>
				    <?php if(PWA_ON == 'true'){ ?>
				    <button type="button" class="btn btn-primary" name="action" onclick="javascript:checkout_asguest(create_account_pop);">Checkout as Guest</button><?php } ?>
					<div class="clearfix"></div>
				</form>
			  </div>
			  <div class="modal-footer text-center">
			  </div>
			</div>
	  </div>
	</div>
<?php 
$cartjs = '';
if(isset($_SESSION['customer_id']) && $_SESSION['customer_id'] > 0) {  
	if ((SHIPPING_SKIP == 'No' || SHIPPING_SKIP == 'If Weight = 0') && $cart->weight > 0) {
         if (SHOW_SHIPPING_ESTIMATOR == 'true') { 
		  $cartjs = 'calculate_shipping_estimate();';
	    }
	}
}
$cartjs .= 'function cartattributeshowhide(id,cnt){
			$(\'#attributediv\'+id+\'-\'+cnt).toggle(500);
		}
		$(document).ready(function() {
			$(".search-js-select").select2();
		});
		';
$obj_catalog->set_footer_js($cartjs);
unset($cartjs);

// RCI code start
echo $cre_RCI->get('shoppingcart', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>

