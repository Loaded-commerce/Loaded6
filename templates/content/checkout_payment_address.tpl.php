<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('checkoutpaymentaddress', 'top');
// RCI code eof
echo tep_draw_form('checkout_address', tep_href_link(FILENAME_CHECKOUT_PAYMENT_ADDRESS, '', 'SSL'), 'post', 'onSubmit="return check_form_optional(checkout_address);"'); ?>
<div class="row">
  <div class="col-sm-12 col-lg-12 large-margin-bottom">
    <h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1>
<?php
/*  if ($messageStack->size('checkout_address') > 0) {
?>
    <div class="alert alert-danger"><?php echo $messageStack->output('checkout_address'); ?></div>
<?php
  } */
if ($process == false) {
?>
	<div class="clearfix panel panel-default no-margin-bottom">
	  <div class="panel-heading"><h3 class="no-margin-top no-margin-bottom"><?php echo HEADING_TITLE; ?></h3></div>
	  <div class="panel-body no-padding-bottom">
	  	<div class="col-sm-6 col-lg-6">
		  <div class="well clearfix"><b><?php echo TABLE_HEADING_PAYMENT_ADDRESS; ?></b><br><?php echo TEXT_SELECTED_PAYMENT_DESTINATION; ?></div>
	    </div>
		<div class="col-sm-6 col-lg-6">
		  <div class="well clearfix"><?php echo '<b>' . TITLE_PAYMENT_ADDRESS . '</b>'; ?><br><?php echo tep_address_label($_SESSION['customer_id'], $_SESSION['billto'], true, ' ', '<br>'); ?></div>
		</div>
<?php
    if ($addresses_count > 1) {
?>
	   <div class="col-sm-12 col-lg-12">
         <div class="well clearfix">
            <b><?php echo TABLE_HEADING_ADDRESS_BOOK_ENTRIES; ?></b></br>
                <?php echo TEXT_SELECT_OTHER_PAYMENT_DESTINATION; ?>
                <?php echo '<b><br>' . TITLE_PLEASE_SELECT . '</b><br>' ; ?>
<table>
<?php
      $radio_buttons = 0;

      $addresses_query = tep_db_query("select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$_SESSION['customer_id'] . "'");
      while ($addresses = tep_db_fetch_array($addresses_query)) {
        $format_id = tep_get_address_format_id($addresses['country_id']);
?>
              <tr>
                <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
       if ($addresses['address_book_id'] == $_SESSION['billto']) {
          echo '                  <tr id="defaultSelected" class="moduleRowSelected" onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
        } else {
          echo '                  <tr class="moduleRow" onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
        }
?>
                    <td class="main" colspan="2"><b><?php echo tep_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']); ?></b></td>
                    <td class="main" align="right"><?php echo tep_draw_radio_field('address', $addresses['address_book_id'], ($addresses['address_book_id'] == $_SESSION['billto'])); ?></td>
                  </tr>
                  <tr>
                    <td colspan="3"><table border="0" cellspacing="0" cellpadding="2">
                      <tr>
                        <td class="main"><?php echo tep_address_format($format_id, $addresses, true, ' ', ', '); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
<?php
        $radio_buttons++;
      }
?>
  </table>
 </div>
</div>
<?php
    }
  }
  if ($addresses_count < MAX_ADDRESS_BOOK_ENTRIES) {
?>
<div class="row">
  <div class="col-sm-12 col-lg-12 large-margin-bottom">
    <h3 class="no-margin-top"><?php echo TABLE_HEADING_NEW_PAYMENT_ADDRESS; ?></h3><br>
	<h7> <?php echo TEXT_CREATE_NEW_PAYMENT_ADDRESS; ?></h7>
	<?php
        if ( file_exists(TEMPLATE_FS_CUSTOM_MODULES . 'checkout_new_address.php')) {
          require(TEMPLATE_FS_CUSTOM_MODULES . 'checkout_new_address.php');
        } else {
          require(DIR_WS_MODULES . 'checkout_new_address.php');
        }
	?>
	</div>
</div>
<?php
  }
// RCI code start
echo $cre_RCI->get('checkoutpaymentaddress', 'menu');
// RCI code eof
?>
     <div class="btn-set small-margin-top clearfix">
      <button type="submit" class="pull-right btn btn-lg btn-primary"><?php echo IMAGE_BUTTON_CONTINUE ; ?></button>
      <div class="well clearfix" style="max-width:47%">
     <?php 
     echo tep_draw_hidden_field('action', 'submit');
     echo '<b>' . TITLE_CONTINUE_CHECKOUT_PROCEDURE . '</b><br>' . TEXT_CONTINUE_CHECKOUT_PROCEDURE;
     ?>
     </div>
   </div>
<?php
if ($process == true) {
	echo '<a href="' . tep_href_link(FILENAME_CHECKOUT_PAYMENT_ADDRESS, '', 'SSL') . '"><button type="button" class="btn btn-md btn-primary">' . IMAGE_BUTTON_BACK . '</button></a>';
}
?>
     </div>
    </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="no-margin-top no-margin-bottom">Order Conformation</h3>
          </div>
        </div>

   </div>
  </div>
</form>
<?php
// RCI code start
echo $cre_RCI->get('checkoutpaymentaddress', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
