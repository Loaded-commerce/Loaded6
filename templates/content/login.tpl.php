<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('login', 'top');
// RCI code eof
?>
<div class="row">
<?php
if ($cart->count_contents() > 0) {
	echo TEXT_VISITORS_CART;
}
if(lc_check_constant_val('B2B_REQUIRE_LOGIN','true')) {
	echo  TEXT_REQUIRE_LOGIN_MESSAGE_HEADING;
	echo TEXT_REQUIRE_LOGIN_MESSAGE;
}
$returning_customer_title = HEADING_RETURNING_CUSTOMER; // DDB - 040620 - PWA - change TEXT by HEADING
$newcustomer_code = '';
$loginbox_code = '';
if(lc_check_constant_val('B2B_ALLOW_CREATE_ACCOUNT','true') ) {
	$newcustomer_file = get_content_file_path('partial/new_customer.tpl.php');
	$newcustomer_code = get_include_contents($newcustomer_file) ;	
}
$loginbox_file = get_content_file_path('partial/loginbox.tpl.php');
$loginbox_code = get_include_contents($loginbox_file) ;	
//===========================================================
if ($setme != '')
{
	$loginbox_code = '';
	$validation_file = get_content_file_path('partial/account_validation.tpl.php');
	$loginbox_code = get_include_contents($validation_file) ;	
}
// RCI code start
echo $cre_RCI->get('login', 'aboveloginbox');
// RCI code end
?>
<div class="col-sm-12 col-lg-12 no-padding-top margin-top">
    <div class="row">
      <div class="col-sm-6 col-lg-6 col-xs-6 login-top no-padding margin-top userloginwell"><?php echo $loginbox_code; ?></div>
      <div class="col-sm-6 col-lg-6 col-xs-6 login-top large-padding-left margin-top create-account-div"><?php echo $newcustomer_code; ?></div>
	</div>
</div>
<?php
// RCI code start
echo $cre_RCI->get('login', 'insideformbelowbuttons');
// RCI code eof
?>
</div>
<?php
// RCI code start
echo $cre_RCI->get('login', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>