<?php 
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('pw', 'top');
// RCI code eof
?>    
<div class="row">
  <div class="col-sm-12 col-lg-12 col-xs-12 no-padding">
    <h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1>
    <div class="well">
		<table border="0" width="100%" cellspacing="0" cellpadding="2">
		  <tr>
<?php
if (($_GET['pass'] != '') && ($_GET['verifyid'] != '')) {
	$select = tep_db_query("SELECT * from " . TABLE_CUSTOMERS . " where customers_id =  " . (int)$_GET['verifyid'] . "");
	$start = tep_db_fetch_array($select);
	if ($start['customers_validation_code'] == $_GET['pass'])
	{
		if ($start['customers_validation'] == '1')
		{
			echo '<td class="main">'. TEXT_YOUR_ACCOUNT_ALREADY_EXIST . '<br></td>';
		} else {
			tep_db_query("update " . TABLE_CUSTOMERS . " set customers_validation = '1', customers_email_registered = '" . $start['customers_email_address'] . "' where customers_id = '" . (int)$_GET['verifyid'] . "'");

			if (defined('LOGIN_AFTER_VALIDATE') && LOGIN_AFTER_VALIDATE == 'true')
			{
				$_SESSION['customer_id'] = $start['customers_id'];
				$_SESSION['customer_default_address_id'] = $start['customers_default_address_id'];
				$_SESSION['customer_first_name'] = $start['customers_firstname'];
				$_SESSION['customer_country_id'] = $start['entry_country_id'];
				$_SESSION['customer_zone_id'] = $start['entry_zone_id'];

				tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_of_last_logon = now(), customers_info_number_of_logons = customers_info_number_of_logons+1 where customers_info_id = '" . (int)$_SESSION['customer_id'] . "'");
				$cart->restore_contents();
			}
			echo '<td class="main">'. TEXT_ACCOUNT_CREATED . '<br></td>';
	    }
	} else {
		echo '<td class="main">'. TEXT_ACCOUNT_CREATED_FAIL . '<br></td>';
	}
} else {
	echo '<td class="main">'. TEXT_ACCOUNT_CREATED_FAIL2 . '<br></td>';
}
?>
      </tr>
      <?php 
      // RCI code start
      echo $cre_RCI->get('pw', 'menu');
      // RCI code eof
      ?>    
		</table>

    </div>
    <div class="button-set clearfix"> <a href="<?php echo tep_href_link(FILENAME_DEFAULT); ?>"><button class="pull-right btn btn-lg btn-primary">Continue</button></a> </div>
  </div>
</div>


<?php 
// RCI code start
echo $cre_RCI->get('global', 'bottom');
echo $cre_RCI->get('pw', 'bottom');
// RCI code eof
?>