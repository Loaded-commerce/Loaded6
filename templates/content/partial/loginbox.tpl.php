<?php
echo tep_draw_form('login', tep_href_link(FILENAME_LOGIN, '', 'SSL'),'post').tep_draw_hidden_field('action','process');
?>
<div class="well no-padding-top box-login">
  <h3 class="no-margin-top user-login"><?php echo MENU_TEXT_RETURNING_CUSTOMER; ?></h3>
  <div class="form-group"><label class="control-label label1"><?php echo ENTRY_EMAIL_ADDRESS; ?></label><?php echo tep_draw_input_field('email_address', '' , 'class="form-control login-email" placeholder="' . ENTRY_EMAIL_ADDRESS . '"') ;?></div>
  <div class="form-group"><label class="control-label label1"><?php echo MENU_TEXT_PASSWORD; ?></label><?php echo tep_draw_password_field('password', '' , 'class="form-control login-password" placeholder="' . MENU_TEXT_PASSWORD . '"') ; ?></div>
  <div class="button-set clearfix">
   <p class="help-block small-margin-left"><a href="<?php echo tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL');?>"><?php echo TEXT_PASSWORD_FORGOTTEN ;?></a></p>
   <button class="pull-right btn btn-lg btn-primary" type="submit"><?php echo MENU_TEXT_SIGN_IN; ?></button>
  </div>
</div>
</form>

