<?php
echo tep_draw_form('login', tep_href_link(FILENAME_LOGIN, '', 'SSL'),'post').tep_draw_hidden_field('action','process');
?>
<div class="box-accountValidation well no-padding-top">
  <h3 class="no-margin-top user-login"><?php echo TEXT_ACCOUNT_VALIDATION; ?></h3>
  <div class="pb-3"><?php echo TEXT_YOU_HAVE_TO_VALIDATE; ?></div>
  <div class="form-group"><label class="control-label label1"><?php echo ENTRY_EMAIL_ADDRESS; ?></label><?php echo tep_draw_input_field('email_address', '' , 'class="form-control" placeholder="' . ENTRY_EMAIL_ADDRESS . '"'); ?></div>
  <div class="form-group"><label class="control-label label1"><?php echo ENTRY_VALIDATION_CODE; ?></label><?php echo tep_draw_input_field('pass','','class="form-control" placeholder="' . ENTRY_VALIDATION_CODE . '"').tep_draw_input_field('password',$_POST['password'],'','hidden'); ?>
  </div>
  <div class="button-set clearfix">
   <p class="help-block small-margin-left"><a href="<?php echo tep_href_link('validate_new.php', '', 'SSL');?>"><?php echo TEXT_NEW_VALIDATION_CODE; ?></a></p>
   <button class="pull-right btn btn-lg btn-primary" type="submit"><?php echo BTN_TEXT_VALIDATE; ?></button>
  </div>
</div>
</form>
