<div class="box-new-customer well no-padding-top">
  <h3 class="user-create"><?php echo TEXT_NEW_CUSTOMER; ?></h3>
  <div style="min-height:171px;"><?php echo TEXT_NEW_CUSTOMER_INTRODUCTION; ?></div>
  <div class="buttons-set clearfix"><a href="<?php echo tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'); ?>"><button type="button" class="pull-right btn btn-lg btn-primary"><?php echo IMAGE_BUTTON_CREATE_ACCOUNT; ?></button></a></div>
</div>
