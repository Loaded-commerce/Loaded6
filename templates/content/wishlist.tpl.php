<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('wishlist', 'top');
// RCI code eof
?>
<!-- wishlist.tpl.php //start -->
<h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1>
<table border="0" width="100%" cellspacing="0" cellpadding="<?php echo CELLPADDING_SUB; ?>">
  <tr>
    <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <td width="100%" valign="top" align="center"><table border="0" width="100%" cellspacing="0" cellpadding="0">
        <!-- wishlist content //start -->
        <?php
        $wishlist_sql = "select * from " . TABLE_WISHLIST . " where customers_id = '" . $_SESSION['customer_id'] . "' and products_id > 0 order by products_name";
        $wishlist_split = new splitPageResults_rspv($wishlist_sql, MAX_DISPLAY_WISHLIST_PRODUCTS);
        $wishlist_query = tep_db_query($wishlist_split->sql_query);

        $info_box_contents = array();
        if (tep_db_num_rows($wishlist_query)) {
          $product_ids = '';
          while ($wishlist = tep_db_fetch_array($wishlist_query)) {
            $product_ids .= $wishlist['products_id'] . ',';
          }
          $product_ids = substr($product_ids, 0, -1);

          $products_query = tep_db_query("select pd.products_id, pd.products_name, pd.products_description, p.products_image, p.products_price, p.products_tax_class_id from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where pd.products_id in (" . $product_ids . ") and p.products_id = pd.products_id and pd.language_id = '" . $languages_id . "' order by products_name");

          if ( ($wishlist_split->number_of_rows > 0) && ( (PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3') ) ) {
          ?>
		  <div class="product-listing-module-pagination margin-bottom">
			<div class="pull-left large-margin-bottom page-results"><?php /* echo $wishlist_split->display_count(TEXT_DISPLAY_NUMBER_OF_WISHLIST); */ ?></div>
			<div class="pull-right large-margin-bottom no-margin-top">
			  <ul class="pagination no-margin-top no-margin-bottom">
			   <?php /* echo  $wishlist_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); */ ?>

			  </ul>
			</div>
		  </div><div class="clear-both"></div>

          <?php
          }
          ?>
          <tr>
            <td colspan="4">
              <table border="0" width="100%" cellspacing="0" cellpadding="2">
                <tr>
                  <td colspan="4"width="100%" valign="top">
                    <?php // echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_WISHLIST)) . tep_draw_hidden_field('wishlist_action', 'add_delete_products_wishlist');
                    echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_WISHLIST, tep_get_all_get_params(array('action')) . 'action=add_del_products_wishlist')); ?>
                  </td>
                </tr>
              <tr>
                  <?php
                  $col = 0;
                  while ($products = tep_db_fetch_array($products_query)) {
                    $col++;
                    ?>
                    <td><table class="linkListing" width="100%" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td width="20%" valign="top" align="center" class="smallText">
                          <a href="<?php echo tep_href_link(FILENAME_PRODUCT_INFO, 'cPath=' . tep_get_product_path($products['products_id']) . '&amp;products_id=' . $products['products_id'], 'NONSSL'); ?>"><?php echo tep_image(DIR_WS_PRODUCTS . $products['products_image'], $products['products_name'],'','','class="img-thumbnail"'); ?></a>
                        </td>
                        <td valign="top" align="left" class="smallText wishprodname">
                          <b><a href="<?php echo tep_href_link(FILENAME_PRODUCT_INFO, 'cPath=' . tep_get_product_path($products['products_id']) . '&amp;products_id=' . $products['products_id'], 'NONSSL'); ?>"><?php echo $products['products_name']; ?></a></b>
                          <?php
                          // Begin Wish List Code w/Attributes
                          $attributes_addon_price = 0;

                          // Now get and populate product attributes
                          if ($_SESSION['customer_id'] > 0) {
                            $wishlist_products_attributes_query = tep_db_query("select products_options_id as po, products_options_value_id as pov from " . TABLE_WISHLIST_ATTRIBUTES . " where customers_id='" . $_SESSION['customer_id'] . "' and products_id = '" . $products['products_id'] . "'");
                            while ($wishlist_products_attributes = tep_db_fetch_array($wishlist_products_attributes_query)) {
                              $data1 = $wishlist_products_attributes['pov'];
                              $data = unserialize(str_replace("\\",'',$data1));

                                if(array_key_exists('c',$data)) {
                                 foreach($data['c'] as $ak => $av) {
                                  $data = $av;
                                  // We now populate $id[] hidden form field with product attributes
                                  echo tep_draw_hidden_field('id['.$products['products_id'].']['.$wishlist_products_attributes['po'].']', $wishlist_products_attributes['pov']);
                                  // And Output the appropriate attribute name
                                  $attributes = tep_db_query("select poptt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_TEXT . " poptt,  " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa where pa.products_id = '" . $products['products_id'] . "' and pa.options_id = '" . $wishlist_products_attributes['po'] . "' and pa.options_id = popt.products_options_id and pa.options_values_id = '" . $data . "' and pa.options_values_id = poval.products_options_values_id  and poptt.products_options_text_id = popt.products_options_id                                 and poptt.language_id = '" . $languages_id . "' and poval.language_id = '" . $languages_id . "'");

                                  $attributes_values = tep_db_fetch_array($attributes);
                                  if ($attributes_values['price_prefix'] == '+') {
                                    $attributes_addon_price += $attributes_values['options_values_price'];
                                  } else if ($attributes_values['price_prefix'] == '-') {
                                    $attributes_addon_price -= $attributes_values['options_values_price'];
                                  }
                                  echo '<br><small><i>' . $attributes_values['products_options_name'] . '&nbsp;:&nbsp; ' . $attributes_values['products_options_values_name'] .'&nbsp; :'.$attributes_values['price_prefix'].$attributes_values['options_values_price']. '</i></small>';
                                  // end while attributes for product
                                }
                              } else {
                                $data = implode(",", $data);
                                // We now populate $id[] hidden form field with product attributes
                                echo tep_draw_hidden_field('id['.$products['products_id'].']['.$wishlist_products_attributes['po'].']', $wishlist_products_attributes['pov']);
                                // And Output the appropriate attribute name
                                $attributes = tep_db_query("select poptt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_TEXT . " poptt,  " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa where pa.products_id = '" . $products['products_id'] . "' and pa.options_id = '" . $wishlist_products_attributes['po'] . "' and pa.options_id = popt.products_options_id and pa.options_values_id = '" . $data . "' and pa.options_values_id = poval.products_options_values_id  and poptt.products_options_text_id = popt.products_options_id                                 and poptt.language_id = '" . $languages_id . "' and poval.language_id = '" . $languages_id . "'");
                                $attributes_values = tep_db_fetch_array($attributes);
                                if ($attributes_values['price_prefix'] == '+') {
                                  $attributes_addon_price += $attributes_values['options_values_price'];
                                } else if ($attributes_values['price_prefix'] == '-') {
                                $attributes_addon_price -= $attributes_values['options_values_price'];
                                }
                                echo '<br><small><i>' . $attributes_values['products_options_name'] . '&nbsp;:&nbsp; ' . $attributes_values['products_options_values_name'] .'&nbsp; :'.$attributes_values['price_prefix'].$attributes_values['options_values_price']. '</i></small>';
                                }
                            } // end while attributes for product

                            $pf->loadProduct($products['products_id'],$languages_id);
                            $products_price = $pf->getPriceStringShort();
                          }
                          echo BOX_TEXT_PRICE . '&nbsp;' . $products_price. '<br>';
                          // End Wish List Code w/Attributes
                          ?>
                          <br>
                          <!-- move/delete checkboxes_start -->
                                <table border="0" cellspacing="0" cellpadding="0" align="left">
                                  <tr>
                                    <td class="main" align="right" width="130"><?php echo  tep_draw_checkbox_field('add_wishprod[]',$products['products_id']); ?> &nbsp;<font color="green"><b><?php echo BOX_TEXT_MOVE_TO_CART ?></b></font>&nbsp;&nbsp;</td>
                                    <td class="main" align="right"><?php echo tep_draw_checkbox_field('del_wishprod[]',$products['products_id']); ?> &nbsp;<font color="BD1415"><b><?php echo BOX_TEXT_DELETE; ?></b></font>&nbsp;&nbsp;</td>
                                  </tr>
                                </table>
                        </td>
                      </tr>
                    </table></td>
                    <!-- move/delete checkboxes_end -->
                    <?php
                      if ( ($col / 1) == floor($col / 1) ) {
                      ?>
                        </tr>
                        <tr><td><hr size="1"></hr></td></tr>
                        <tr>
                      <?php
                      }
                  } //end while
                  ?>
                </tr>
                <tr>
                  <td colspan="4"></td>
                </tr>
              </table>
             </td>
          </tr>
           <tr>
            <td colspan="4">
              <?php
              if ( ($wishlist_split->number_of_rows > 0) && ( (PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3') ) ) {
              ?>
      <div class="product-listing-module-pagination margin-bottom">
        <div class="pull-left large-margin-bottom page-results"><?php echo $wishlist_split->display_count(TEXT_DISPLAY_NUMBER_OF_WISHLIST); ?></div>
        <div class="pull-right large-margin-bottom no-margin-top">
          <ul class="pagination no-margin-top no-margin-bottom">
           <?php echo  $wishlist_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?>

          </ul>
        </div>
      </div><div class="clear-both"></div>
		</td>
	  </tr>
	</table>
          <tr>
            <td class="main" colspan="4">
              <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="main" width="33%" align="left"><a href="<?php echo tep_href_link(FILENAME_DEFAULT); ?>"><button class="pull-left btn btn-lg btn-default" type="button"><?php echo IMAGE_BUTTON_CONTINUE_SHOPPING;?></button></a></td>
                  <td class="main" width="33%" align="center"><a href="<?php echo tep_href_link(FILENAME_SHOPPING_CART); ?>"><button class="pull-middle btn btn-lg btn-primary" type="button"><?php echo IMAGE_BUTTON_VIEW_CART;?></button></a></td>
                  <td class="main" width="33%" align="right"><button class="pull-right btn btn-lg btn-primary" type="submit"><?php echo IMAGE_BUTTON_UPDATE;?></button></td>
                </tr>
              </table>
            </form></td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">
              <!-- tell_a_friend //-->
              <?php
			   echo '<div class="well"><div class="col-lg-12 col-sm-12 col-xs-12" style="padding:0px;"><h3 class="no-margin-top heading">'.BOX_HEADING_SEND_WISHLIST .'</h3></div>';
              $info_box_contents = array();
              $info_box_contents[] = array('form' => tep_draw_form('tell_a_friend', tep_href_link(FILENAME_WISHLIST_SEND, '', 'NONSSL', false), 'get'),
                                                        'align' => 'left',
                                                        'text' => tep_draw_input_field('send_to', '', 'size="20" class="form-control email-input" style="max-width:69%;display:inline;" placeholder="Your friend email address"') . '&nbsp;' . tep_template_image_submit('button_tell_a_friend.gif', BOX_TEXT_SEND) . tep_draw_hidden_field('products_ids', isset($_GET['products_ids'])) . tep_hide_session_id() . '<br><br>');
              new contentBox($info_box_contents, true, true);
              ?>
				</div>
              <!-- tell_a_friend_eof //-->
            </td>
            </tr>
              <?php
              }

            } else { // Nothing in the customers wishlist
            ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
              <tr>
                <td class="main"><?php echo BOX_TEXT_NO_ITEMS;?></td>
              </tr>
            <tr>
            <td>&nbsp;</td>
          </tr>
      </table></td>
      <?php
      // RCI code start
      echo $cre_RCI->get('wishlist', 'menu');
      // RCI code eof
	 ?>
		<tr>
            <td>&nbsp;</td>
          </tr>
       <div class="textright">No wishlist in our list. Start shopping</div>
       <div class="col-lg-4 main" align="right" style="float:right;"><a href="<?php echo tep_href_link(FILENAME_DEFAULT); ?>"><?php echo tep_template_image_button('button_continue_shopping.gif', IMAGE_BUTTON_CONTINUE_SHOPPING); ?></a></div>


<?php
}
      ?>
    </table></td>
  </tr>
</table>
<?php
// RCI code start
echo $cre_RCI->get('wishlist', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
