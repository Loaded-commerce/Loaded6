<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('articlereviewswrite', 'top');
// RCI code eof
echo tep_draw_form('article_reviews_write', tep_href_link(FILENAME_ARTICLE_REVIEWS_WRITE, 'action=process&amp;articles_id=' . $_GET['articles_id']), 'post', 'onSubmit="return checkForm();"'); ?>
<h1><?php echo HEADING_TITLE . $articles_name . '\''; ?></h1>
<table border="0" width="100%" cellspacing="0" cellpadding="<?php echo CELLPADDING_SUB; ?>" class="table-condensed">
<?php
  if ($messageStack->size('review') > 0) {
?>
      <tr>
        <td><div class="alert alert-danger"><?php echo $messageStack->output('review'); ?></div></td>
      </tr>
<?php
  }
?>
              <tr>
                <td class="main"><font color="red"><i><?php echo TEXT_APPROVAL_WARNING; ?></i></font></td>
              </tr>
              <tr>
                <td class="main" colspan="2"><?php echo '<b>' . SUB_TITLE_FROM . '</b> ' . tep_output_string_protected($customer['customers_firstname'] . ' ' . $customer['customers_lastname']); ?></td>
              </tr>
              <tr>
                <td class="main"><b><?php echo SUB_TITLE_REVIEW; ?></b></td>
              </tr>
			  <tr>
				<td class="main"><?php echo tep_draw_textarea_field('article_review', 'soft', 60, 10,(isset($_POST['article_review']) ? $_POST['article_review'] : ''), 'class="form-control"'); ?></td>
			  </tr>
			  <tr>
				<td class="main"><?php echo '<b>' . SUB_TITLE_RATING . '</b> ' . TEXT_BAD . ' ' . tep_draw_radio_field('article_rating', '1',((isset($_POST['article_rating']) && (int)$_POST['article_rating'] == 1 ) ? ' checked' : '')) . ' ' . tep_draw_radio_field('article_rating', '2',((isset($_POST['article_rating']) && (int)$_POST['article_rating'] == 2 ) ? ' checked' : '')) . ' ' . tep_draw_radio_field('article_rating', '3',((isset($_POST['article_rating']) && (int)$_POST['article_rating'] == 3 ) ? ' checked' : '')) . ' ' . tep_draw_radio_field('article_rating', '4',((isset($_POST['article_rating']) && (int)$_POST['article_rating'] == 4 ) ? ' checked' : '')) . ' ' . tep_draw_radio_field('article_rating', '5',((isset($_POST['article_rating']) && (int)$_POST['article_rating'] == 5 ) ? ' checked' : '')) . ' ' . TEXT_GOOD; ?></td>
			  </tr>

<!-- VISUAL VERIFY CODE start -->
  <?php /* if (VVC_SITE_ON_OFF == 'On'){
          if (VVC_ARTICLE_REVIEWS_ON_OFF == 'On'){
   ?>
      <tr>
        <td class="main"><b><?php echo VISUAL_VERIFY_CODE_CATEGORY; ?></b></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          <tr class="infoBoxContents">
            <td><table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td class="main"><?php echo VISUAL_VERIFY_CODE_TEXT_INSTRUCTIONS; ?></td>
                <td class="main"><?php echo tep_draw_input_field('visual_verify_code') . '&nbsp;' . '<span class="inputRequirement">' . VISUAL_VERIFY_CODE_ENTRY_TEXT . '</span>'; ?></td>
                <td class="main">
                  <?php
                      //can replace the following loop with $visual_verify_code = substr(str_shuffle (VISUAL_VERIFY_CODE_CHARACTER_POOL), 0, rand(3,6)); if you have PHP 4.3
                    $visual_verify_code = "";
                    for ($i = 1; $i <= rand(3,6); $i++){
                          $visual_verify_code = $visual_verify_code . substr(VISUAL_VERIFY_CODE_CHARACTER_POOL, rand(0, strlen(VISUAL_VERIFY_CODE_CHARACTER_POOL)-1), 1);
                     }
                     $vvcode_oscsid = tep_session_id();
                     tep_db_query("DELETE FROM " . TABLE_VISUAL_VERIFY_CODE . " WHERE oscsid='" . $vvcode_oscsid . "'");
                     $sql_data_array = array('oscsid' => $vvcode_oscsid, 'code' => $visual_verify_code);
                     tep_db_perform(TABLE_VISUAL_VERIFY_CODE, $sql_data_array);
                     $visual_verify_code = "";
                     echo('<img src="' . FILENAME_VISUAL_VERIFY_CODE_DISPLAY . '?vvc=' . $vvcode_oscsid . '" alt="' . VISUAL_VERIFY_CODE_CATEGORY . '">');
                  ;?>
                </td>
                <td class="main"><?php echo VISUAL_VERIFY_CODE_BOX_IDENTIFIER; ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
<!--  VISUAL VERIFY CODE stop   -->
<?php
 }
} */
?>
<tr>
  <td>
		<?php
		if (defined('VVC_ARTICLE_REVIEWS_ON_OFF') && VVC_ARTICLE_REVIEWS_ON_OFF == 'On'){
			$captcha_instructions = trim($captcha->display_captcha_instructions());
		?>
		<!-- Captcha CODE start -->
		<h3><?php echo $captcha->display_captcha_label(); ?></h3>
		<div class="form-group full-width margin-bottom"><?php echo $captcha->render_captcha_image();?><?php echo $captcha->render_captcha_control('class="form-control"');?></div>
		<?php if($captcha_instructions != '') {?>
		<div class="form-group full-width margin-bottom"><?php echo $captcha_instructions; ?></div>
		<?php } ?>
		<!-- Captcha VERIFY CODE end -->
		<?php } ?>
  </td>
</tr>
<?php
// RCI code start
echo $cre_RCI->get('articlereviewswrite', 'menu');
// RCI code eof
?>
	  <tr>
		<td><table border="0" width="100%" cellspacing="0" cellpadding="2">
			  <tr>
				<td class="main"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_REVIEWS, tep_get_all_get_params(array('reviews_id', 'action'))) . '"><button class="pull-left btn btn-lg btn-default" type="button">'. IMAGE_BUTTON_BACK .'</button></a>'; ?></td>
				<td class="main" align="right"><button class="pull-right btn btn-lg btn-primary"><?php echo IMAGE_BUTTON_CONTINUE; ?></button></td>
			  </tr>
			</table>
		</td>
	  </tr>

    </table></form>
<?php
// RCI code start
echo $cre_RCI->get('articlereviewswrite', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
