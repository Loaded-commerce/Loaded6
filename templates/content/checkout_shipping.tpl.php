<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('checkoutshipping', 'top');
// RCI code eof
echo tep_draw_form('checkout_address', tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL')) . tep_draw_hidden_field('action', 'process'); ?>
<div class="row">
<div class="col-sm-12 col-lg-12 large-margin-bottom">
    <h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1>

<?php
if (isset($_GET['shipping_error'])) {
	$error['error'] = TEXT_CHOOSE_SHIPPING_METHOD ;
	echo '<div class="alert alert-danger">'. tep_output_string_protected($error['error']) .'</div>'; 
}
?>
<div class="panel panel-default no-margin-bottom">
  <div class="panel-heading">
	<h3 class="no-margin-top no-margin-bottom"><?php echo HEADING_TITLE; ?></h3>
  </div>
  <div class="panel-body no-padding-bottom">
	<div class="row">
	 <div class="col-sm-6 col-lg-6">
	  <div class="well clearfix">
			<?php 
			echo '<h4 class="no-margin-top"><b>' . TITLE_SHIPPING_ADDRESS . '</b></h4>' ; 
			// RCO start
			if ($cre_RCO->get('checkoutshipping', 'changeaddressbutton') !== true) {
			 }
			// RCO eof
			?>
	  </div>
	 </div>
	<div class="col-sm-6 col-lg-6">
	 <div class="well clearfix">
	 <?php echo '<h4 class="no-margin-top"><b>' . TITLE_SHIPPING_ADDRESS . '</b></h4>' ; ?>
		  <?php echo tep_address_label($_SESSION['customer_id'], $_SESSION['sendto'], true, ' ', '<br>'); ?>
	 </div>
	</div>

  <?php
  //MVS start
  if (tep_count_shipping_modules() > 0 || MVS_STATUS == 'true') {?>
  	<div class="col-sm-12 col-lg-12">
  	   <div class="well clearfix">
  <?php  
  if (MVS_STATUS == 'true') {
      require(DIR_WS_MODULES . 'vendor_shipping.php');
    } else {
  ?>
	  <table border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
		  <td class="main"><b><?php echo TABLE_HEADING_SHIPPING_METHOD; ?></b></td>
		</tr>
	  </table>
      <tr>
        <td align="left"><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          <tr class="infoBoxContents">
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <?php
              if (sizeof($quotes) > 1 && sizeof($quotes[0]) > 1) {
                ?>
                <tr>
                  <td class="main" width="50%" valign="top"><?php echo TEXT_CHOOSE_SHIPPING_METHOD; ?></td>
                </tr>
                <?php
              } elseif ($free_shipping == false) {
                ?>
                <tr>
                  <td class="main" width="100%" colspan="2"><?php echo TEXT_ENTER_SHIPPING_INFORMATION; ?></td>
                </tr>
                <?php
              }
              if ($free_shipping == true) {
                ?>
                <tr>
                  <td colspan="2" width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                    <tr>
                      <td class="main" colspan="3"><b><?php echo FREE_SHIPPING_TITLE; ?></b>&nbsp;<?php echo (isset($quotes[$i]['icon']) ? $quotes[$i]['icon'] : ''); ?></td>
                    </tr>
                    <tr id="defaultSelected" class="moduleRowSelected" onclick="selectRowEffect(this, 0)">
                      <td class="main" width="100%"><?php echo sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format($freeshipping_over_amount)) . tep_draw_hidden_field('shipping', 'free_free'); ?></td>
                    </tr>
                  </table></td>
                </tr>
                <?php
              } else {
                $radio_buttons = 0;
                for ($i=0, $n=sizeof($quotes); $i<$n; $i++) {
                  ?>
                  <tr>
                    <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2" class="table table-hover">
                      <tr>
                        <td class="main" colspan="3"><b><?php echo $quotes[$i]['module']; ?></b>&nbsp;<?php if (isset($quotes[$i]['icon']) && tep_not_null($quotes[$i]['icon'])) { echo $quotes[$i]['icon']; } ?></td>
                      </tr>
                      <?php
                      if (isset($quotes[$i]['error'])) {
                        ?>
                        <tr>
                          <td class="main" colspan="3"><?php echo $quotes[$i]['error']; ?></td>
                        </tr>
                        <?php
                      } else {
                        for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
                          // set the radio button to be checked if it is the method chosen
                          $checked = (($quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'] == $_SESSION['shipping']['id']) ? true : false);
                          if ( ($checked == true) || ($n == 1 && $n2 == 1) ) {
                            echo '<tr id="defaultSelected" onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
                          } else {
                            echo '<tr onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
                          }
                          ?>
                          <td class="main" align="center"><?php echo tep_draw_radio_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'], $checked); ?></td>
                          <td class="main" width="75%"><?php echo $quotes[$i]['methods'][$j]['title']; ?></td>
                          <?php
                          if ( ($n > 1) || ($n2 > 1) ) {
                            ?>
                            <td class="main"><?php echo $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))); ?></td>
                            <?php
                          } else {
                            ?>
                            <td class="main" align="right" colspan="2"><?php echo $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))) . tep_draw_hidden_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id']); ?></td>
                            <?php
                          }
                          ?>
                          </tr>
                          <?php
                          $radio_buttons++;
                        }
                      }
                      ?>
                    </table></td>
                  </tr>
                  <?php
                }
              }
              ?>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <?php
    }
?>
 </div>
</div>
<?php  
	} //MVS end
?>

	<div class="col-sm-12 col-lg-12">
	  <div class="well ">
		<div class="form-group"><label class="sr-only"></label>
		  <h4 style="color:#000000"><b><?php echo TABLE_HEADING_COMMENTS; ?></b></h4>
		  <?php echo tep_draw_textarea_field('comments','soft', '60', '5',$_SESSION['comments'], 'class="form-control"'); ?>
	   </div>
	  </div>
	 </div>

	  <?php
	  //RCI above buttons
	  echo $cre_RCI->get('checkoutshipping', 'insideformabovebuttons');
	  ?>
	  <div class="col-sm-12 col-lg-12"><div class="well clearfix">
	  <?php echo '<h4 style="color:#000000"><b>' . TITLE_CONTINUE_CHECKOUT_PROCEDURE . '</b></h4>' . TEXT_CONTINUE_CHECKOUT_PROCEDURE; ?></div></div>
			<div class="btn-set small-margin-top clearfix">
		  <button type="submit" class="pull-right btn btn-lg btn-primary">Continue</button>
	  </div>

  <?php
  //RCI below buttonsbullet
  echo $cre_RCI->get('checkoutshipping', 'insideformbelowbuttons');
  ?>
      </div>
     </div>
    </div>
        <div class="clearfix panel panel-default no-margin-bottom">
          <div class="panel-heading">
            <h3 class="no-margin-top no-margin-bottom">Payment Information</h3>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="no-margin-top no-margin-bottom">Order Conformation</h3>
          </div>
        </div>
   </div>
  </div>

</form>
<?php
// RCI code start
echo $cre_RCI->get('checkoutshipping', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
