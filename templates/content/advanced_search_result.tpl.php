<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('advancedsearchresult', 'top');
// RCI code eof
?>
<div class="row">
  <div class="product-listing-module-container col-lg-12 col-xl-12 col-md-12 col-sm-12 col-12">
	 <h1 class="no-margin-top"><?php echo HEADING_TITLE_2 .'"'. trim(strip_tags($_GET['keywords'])) .'"' ; ?></h1>
		<?php
			$obj_catalog->load_listing_module();
		?>
  </div>
</div>

<?php
// RCI code start
echo $cre_RCI->get('advancedsearchresult', 'menu');
// RCI code eof
?>

<div class="button-set clearfix large-margin-bottom">
<?php echo '<a href="' . tep_href_link(FILENAME_ADVANCED_SEARCH, tep_get_all_get_params(array('sort', 'page')), 'NONSSL', true, false) . '"><button class="pull-left btn btn-lg btn-default" type="button">'.IMAGE_BUTTON_BACK.'</button></a>'; ?>
</div>
<?php
// RCI code start
echo $cre_RCI->get('advancedsearchresult', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
