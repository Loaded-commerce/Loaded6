<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('tellafriendarticle', 'top');
// RCI code eof
echo tep_draw_form('email_friend_article', tep_href_link(FILENAME_TELL_A_FRIEND_ARTICLE, 'action=process&amp;articles_id=' . $_GET['articles_id']));
?>
<h1><?php echo sprintf(HEADING_TITLE, $article_info['articles_name']); ?></h1>
  <table border="0" width="100%" cellspacing="0" cellpadding="<?php echo CELLPADDING_SUB; ?>" class="table-condensed">
<?php
/*  if ($messageStack->size('friend') > 0) {
?>
      <tr>
        <td><div class="alert alert-danger"><?php echo $messageStack->output('friend'); ?></div></td>
      </tr>
<?php
  } */
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="main"><b><?php echo FORM_TITLE_CUSTOMER_DETAILS; ?></b></td>
                <td class="inputRequirement" align="right"><?php echo FORM_REQUIRED_INFORMATION; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
              <tr class="infoBoxContents">
                <td><table border="0" cellspacing="0" cellpadding="2" width="100%" class="table-condensed">
                  <tr>
                    <td class="main"><?php echo FORM_FIELD_CUSTOMER_NAME; ?></td>
                    <td class="main" width="70%"><?php echo tep_draw_input_field('from_name', '', 'class="form-control"'); ?></td>
                  </tr>
                  <tr>
                    <td class="main"><?php echo FORM_FIELD_CUSTOMER_EMAIL; ?></td>
                    <td class="main"><?php echo tep_draw_input_field('from_email_address', '', 'class="form-control"'); ?></td>
                  </tr>
				  <tr>
					<td class="main" colspan="2"><b><?php echo FORM_TITLE_FRIEND_DETAILS; ?></b></td>
				  </tr>
                  <tr>
                    <td class="main"><?php echo FORM_FIELD_FRIEND_NAME; ?></td>
                    <td class="main"><?php echo tep_draw_input_field('to_name', '', 'class="form-control" style="display:inline;width:96%"') . '&nbsp;<span class="inputRequirement">' . ENTRY_FIRST_NAME_TEXT . '</span>'; ?></td>
                  </tr>
                  <tr>
                    <td class="main"><?php echo FORM_FIELD_FRIEND_EMAIL; ?></td>
                    <td class="main"><?php echo tep_draw_input_field('to_email_address', '', 'class="form-control" style="display:inline;width:96%"') . '&nbsp;<span class="inputRequirement">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>'; ?></td>
                  </tr>
				  <tr>
					<td class="main" colspan="2"><b><?php echo FORM_TITLE_FRIEND_MESSAGE; ?></b></td>
				  </tr>
				  <tr>
					<td colspan="2"><?php echo tep_draw_textarea_field('message', 'soft', 40, 8, '', 'class="form-control"'); ?></td>
				  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>

      <!--  VISUAL VERIFY CODE start -->
  <?php
  /*if (defined('VVC_SITE_ON_OFF') && VVC_SITE_ON_OFF == 'On'){
  if(defined('VVC_TELL_FRIEND_ARTICLE_ON_OFF') && VVC_TELL_FRIEND_ARTICLE_ON_OFF == 'On'){
   ?>
      <tr>
        <td class="main"><b><?php echo VISUAL_VERIFY_CODE_CATEGORY; ?></b></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          <tr class="infoBoxContents">
            <td><table border="0" cellspacing="2" cellpadding="2" class="table-condensed">
              <tr>
                <td class="main"><?php echo VISUAL_VERIFY_CODE_TEXT_INSTRUCTIONS; ?></td>
                <td class="main"><?php echo tep_draw_input_field('visual_verify_code', '', 'class="form-control" style="display:inline;width:90%"') . '&nbsp;' . '<span class="inputRequirement">' . VISUAL_VERIFY_CODE_ENTRY_TEXT . '</span>'; ?></td>
               </tr>
               <tr>
                <td class="main">
                  <?php
                      //can replace the following loop with $visual_verify_code = substr(str_shuffle (VISUAL_VERIFY_CODE_CHARACTER_POOL), 0, rand(3,6)); if you have PHP 4.3
                    $visual_verify_code = "";
                    for ($i = 1; $i <= rand(3,6); $i++){
                          $visual_verify_code = $visual_verify_code . substr(VISUAL_VERIFY_CODE_CHARACTER_POOL, rand(0, strlen(VISUAL_VERIFY_CODE_CHARACTER_POOL)-1), 1);
                     }
                     $vvcode_oscsid = tep_session_id();
                     tep_db_query("DELETE FROM " . TABLE_VISUAL_VERIFY_CODE . " WHERE oscsid='" . $vvcode_oscsid . "'");
                     $sql_data_array = array('oscsid' => $vvcode_oscsid, 'code' => $visual_verify_code);
                     tep_db_perform(TABLE_VISUAL_VERIFY_CODE, $sql_data_array);
                     $visual_verify_code = "";
                     echo('<img src="' . FILENAME_VISUAL_VERIFY_CODE_DISPLAY . '?vvc=' . $vvcode_oscsid . '" alt="' . VISUAL_VERIFY_CODE_CATEGORY . '">');
                  ?>
                </td>
                <td colspan="2" class="main"><?php echo VISUAL_VERIFY_CODE_BOX_IDENTIFIER; ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
  <?php
 }
} */




?>
<!-- VISUAL VERIFY CODE stop   -->
 </tr>
 <tr>
   <td>
				<?php
				if ($captcha->captcha_status('VVC_TELL_FRIEND_ARTICLE_ON_OFF')){
					$captcha_instructions = trim($captcha->display_captcha_instructions());
				?>
				<!-- Captcha CODE start -->
				<h3><?php echo $captcha->display_captcha_label(); ?></h3>
				<div class="form-group full-width margin-bottom"><?php echo $captcha->render_captcha_image();?><?php echo $captcha->render_captcha_control('class="form-control"');?></div>
				<?php if($captcha_instructions != '') {?>
				<div class="form-group full-width margin-bottom"><?php echo $captcha_instructions; ?></div>
				<?php } ?>
				<!-- Captcha VERIFY CODE end -->
				<?php } ?>
   </td>
 </tr>



        </table></td>
      </tr>
<?php
// RCI code start
echo $cre_RCI->get('tellafriendarticle', 'menu');
// RCI code eof
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          <tr class="infoBoxContents">
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
<?php if ($valid_article == "true") {
?>
                <td><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $_GET['articles_id']) . '"><button class="pull-left btn btn-lg btn-default" type="button">'. IMAGE_BUTTON_BACK .'</button></a>'; ?></td>

<?php
}
?>
                <td align="right"><button class="pull-right btn btn-lg btn-primary"><?php echo IMAGE_BUTTON_CONTINUE; ?></button></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></form>
<?php
// RCI code start
echo $cre_RCI->get('tellafriendarticle', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
