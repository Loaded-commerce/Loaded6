<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('createaccount', 'top');
// RCI code eof
echo tep_draw_form('create_account', tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'), 'post', 'onSubmit="return check_form(create_account);" enctype="multipart/form-data"') . tep_draw_hidden_field('action', 'process') . tep_draw_hidden_field('show_passwords', '1'); ?>
<div class="row">

 <div class="col-sm-12 col-lg-12 col-xs-12 no-padding account-form">
     <h1 class="no-margin-top"><?php echo HEADING_TITLE; ?></h1>
<?php
 /* if ($messageStack->size('create_account') > 0) {
?>
<div class="message-stack-container alert alert-danger small-margin-bottom small-margin-left">
        <?php echo $messageStack->output('create_account'); ?>


</div>
<?php
  } */
?>
          <div class="row">
            <div class="col-sm-6 col-lg-6 large-padding-left margin-top personal-div">
	          <div class="well create_address no-padding-top">
				<h3><?php echo CATEGORY_PERSONAL; ?></h3>
				<?php
					if (ACCOUNT_GENDER == 'true')
					{
						echo ENTRY_GENDER;
						echo tep_draw_radio_field('gender', 'm') . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('gender', 'f') . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . (tep_not_null(ENTRY_GENDER_TEXT) ? '<span class="inputRequirement">' . ENTRY_GENDER_TEXT . '</span>': '');
					}
				?>
				<div class="row">
				  <div class="col-lg-4" style="padding-right:6px;">First Name:</div>
				  <div class="col-lg-8 form-group  margin-bottom" style="padding-left:4px;"><label class="sr-only"></label><?php echo tep_draw_input_field('firstname', '' , 'class="form-control" placeholder="' . ENTRY_FIRST_NAME . '"'); ?></div>
				</div>
				<div class="row">
				  <div class="col-lg-4" style="padding-right:6px;">Last Name:</div>
				  <div class="col-lg-8 form-group  margin-bottom" style="padding-left:4px;"><label class="sr-only"></label><?php echo tep_draw_input_field('lastname', '' , 'class="form-control" placeholder="' . ENTRY_LAST_NAME . '"'); ?></div>
				</div>
				<?php if (ACCOUNT_DOB == 'true') { ?>
				  <div class="form-group  margin-bottom"><label class="sr-only"></label><?php echo tep_draw_input_field('dob', '' , 'class="form-control" placeholder="' . ENTRY_DATE_OF_BIRTH . ENTRY_DATE_OF_BIRTH_TEXT . '"'); ?></div>
				<?php } ?>
				<div class="row">
				  <div class="col-lg-4" style="padding-right:6px;">E-Mail Address:</div>
				  <div class="col-lg-8 form-group  margin-bottom" style="padding-left:4px;"><label class="sr-only"></label><?php echo tep_draw_input_field('email_address', '' , 'class="form-control" placeholder="' . ENTRY_EMAIL_ADDRESS . '"'); ?></div>
				</div>
				<div class="row">
				  <div class="col-lg-4" style="padding-right:6px;">Password:</div>
	              <div class="col-lg-8 form-group  margin-bottom" style="padding-left:4px;"><?php echo tep_draw_password_field('password', '' , 'class="form-control" placeholder="' . ENTRY_PASSWORD . '"'); ?></div>
	            </div>
	            <div class="row">
	              <div class="col-lg-4" style="padding-right:6px;">Confirmation Password:</div>
	              <div class="col-lg-8 form-group  margin-bottom" style="padding-left:4px;"><?php echo tep_draw_password_field('confirmation', '' , 'class="form-control" placeholder="' . ENTRY_PASSWORD_CONFIRMATION . '"'); ?></div>
	            </div>
			 </div>
			</div>
			<div class="col-sm-6 col-lg-6 margin-top clearfix">
	          <div class="well no-padding-top">
	          <?php if (ACCOUNT_COMPANY == 'true') { ?>
				<h3><?php echo CATEGORY_COMPANY; ?></h3>
				<div class="form-group full-width margin-bottom"><label class="sr-only"></label><?php echo tep_draw_input_field('company', '' , 'class="form-control" placeholder="' . ENTRY_COMPANY . '"'); ?></div>
				<div class="form-group full-width margin-bottom"><label class="sr-only"></label><?php echo tep_draw_input_field('company_tax_id', '' , 'class="form-control" placeholder="' . ENTRY_COMPANY_TAX_ID . '"'); ?></div>
			  <?php } ?>

				<h3><?php echo CATEGORY_OPTIONS; ?></h3>
                <div class="checkbox margin-top"><label class="normal" style="padding-left:5px;"><?php echo ENTRY_NEWSLETTER; ?><input id="newsletter" class="small-margin-left" type="checkbox" checked="checked" value="1" name="newsletter"></label></div>

						<?php
						if ($captcha->captcha_status('VVC_CREATE_ACCOUNT_ON_OFF')){
							$captcha_instructions = trim($captcha->display_captcha_instructions());
						?>
						<!-- Captcha CODE start -->
						<h3><?php echo $captcha->display_captcha_label(); ?></h3>
		    			<div class="form-group full-width margin-bottom"><?php echo $captcha->render_captcha_image();?><?php echo $captcha->render_captcha_control('class="form-control"');?></div>
						<?php if($captcha_instructions != '') {?>
		    			<div class="form-group full-width margin-bottom"><?php echo $captcha_instructions; ?></div>
						<?php } ?>
						<!-- Captcha VERIFY CODE end -->
						<?php } ?>

			 </div>
			</div>

       </div>
      <div class="row">
		  <?php
		  // RCI code start
			 echo $cre_RCI->get('createaccount', 'forms');
		  // RCI code eof
		  ?>
	</div>
    <div class="btn-set small-margin-top clearfix"><button class="pull-right btn btn-lg btn-primary" type="submit"><?php echo IMAGE_BUTTON_CONTINUE; ?></button></div>

 </div>
</div>

<?php
// RCI code start
echo $cre_RCI->get('createaccount', 'menu');
// RCI code eof
?>
   </form>
<?php
// RCI code start
echo $cre_RCI->get('createaccount', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
