<?php
// RCI code start
echo $cre_RCI->get('global', 'top');
echo $cre_RCI->get('static', 'top');
// RCI code eof
?>
<h1><?php echo HEADING_TITLE; ?></h1>
<table border="0" width="100%" cellspacing="0" cellpadding="<?php echo CELLPADDING_SUB; ?>">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
// BOF: Lango Added for template MOD
if (MAIN_TABLE_BORDER == 'yes'){
table_image_border_top(false, false, $header_text);
}
// EOF: Lango Added for template MOD
?>
          <tr>
            <td class="main"><?php echo TEXT_INFORMATION; ?></td>
          </tr>
<?php
// RCI code start
echo $cre_RCI->get('static', 'menu');
// RCI code eof
// BOF: Lango Added for template MOD
if (MAIN_TABLE_BORDER == 'yes'){
table_image_border_bottom();
}
// EOF: Lango Added for template MOD
?>
        </table></td>
      </tr>
	  <tr class="infoBoxContents">
		<td><table border="0" width="100%" cellspacing="0" cellpadding="2">
		  <tr>
			<td align="right"><?php echo '<a class="btn btn-primary" href="' . tep_href_link(FILENAME_DEFAULT) . '">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></td>
		  </tr>
		</table></td>
	  </tr>
    </table>
<?php
// RCI code start
echo $cre_RCI->get('static', 'bottom');
echo $cre_RCI->get('global', 'bottom');
// RCI code eof
?>
