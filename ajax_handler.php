<?php
/*
  Loaded Commerce, Open Source E-Commerce Solutions
  https://www.loadedcommerce.com

  Copyright (c) 2020 Loaded Commerce

  Released under the GNU General Public License
*/

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

require('includes/application_top.php');

// load all enabled payment modules
require(DIR_WS_CLASSES . 'payment.php');
$payment_modules = new payment;


$action = isset($_GET['action'])?strip_tags($_GET['action']):'';
$arr_action = explode('/', $action);

if(count($arr_action) > 1) {
	$class_name = trim(strip_tags($arr_action[0]));
	$method_name = trim(strip_tags($arr_action[1]));
} else {
	$class_name = 'rpc';
	$method_name = trim(strip_tags($arr_action[0]));

}

if(method_exists($class_name, 'rpc_'.$method_name)) {
	$method_name = 'rpc_'.$method_name;
    call_user_func(array($class_name, $method_name));
    exit;

} else {

	echo "Invalid Call";
	exit();
}
?>