<?php
function lc_get_all_directory($directory)
{
	$arr_addons = array();
	if ($handle = opendir($directory)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				if(is_dir($directory.$entry))
					$arr_addons[] = $entry;
			}
		}
		closedir($handle);
	}
	return $arr_addons;
}

function lc_get_all_files($directory)
{
	$arr_addons = array();
	if ($handle = opendir($directory)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				if(!is_dir($directory.$entry))
					$arr_addons[] = $entry;
			}
		}
		closedir($handle);
	}
	return $arr_addons;
}

//Load the installed addon functions
$arr_addons = lc_get_all_directory(DIR_FS_CATALOG.'addons/');
define('AVAILABLE_ADDONS_jSON', json_encode($arr_addons));
$auto_include = array('includes/functions/general.php', 'includes/languages/english.php', 'includes/filenames.php', 'includes/database_tables.php');
foreach($arr_addons as $addon_dir)
{
	foreach($auto_include as $auto_file_name) {
		if(file_exists('addons/'.$addon_dir.'/catalog/'.$auto_file_name)) {
			require_once('addons/'.$addon_dir.'/catalog/'.$auto_file_name);
		}
	}
	//Include the default class
	if(file_exists('addons/'.$addon_dir.'/'.$addon_dir.'.class.php')) {
		require_once('addons/'.$addon_dir.'/'.$addon_dir.'.class.php');
		$addon_dir = new $addon_dir();
	}
}

function lc_addon_init()
{
	global $arr_addons;
	foreach($arr_addons as $addon_dir)
	{
		if(function_exists($addon_dir.'_addon_modules_init')) {
			$func_name = $addon_dir.'_addon_modules_init';
			$func_name();
		}
	}
	if(defined('SYSTEM_ADDON_B2B'))
		define('SYSTEM_ADDON', SYSTEM_ADDON_B2B);
	elseif(defined('SYSTEM_ADDON_PRO'))	
		define('SYSTEM_ADDON', SYSTEM_ADDON_PRO);
}

function lc_addon_before_applicationtop()
{
	global $arr_addons;
	foreach($arr_addons as $addon_dir)
	{
		if(function_exists($addon_dir.'_before_applicationtop')) {
			$func_name = $addon_dir.'_before_applicationtop';
			$func_name();
		}
	}
}

function lc_addon_after_applicationtop()
{
	global $arr_addons;
	foreach($arr_addons as $addon_dir)
	{
		if(function_exists($addon_dir.'_after_applicationtop')) {
			$func_name = $addon_dir.'_after_applicationtop';
			$func_name();
		}
	}
}

function lc_addon_shoppingcart_actions()
{
	global $arr_addons;
	foreach($arr_addons as $addon_dir)
	{
		if(function_exists($addon_dir.'_after_session')) {
			$func_name = $addon_dir.'_after_session';
			$func_name();
		}
	}
}

function lc_addon_after_session()
{
	global $arr_addons;
	foreach($arr_addons as $addon_dir)
	{
		if(function_exists($addon_dir.'_after_session')) {
			$func_name = $addon_dir.'_after_session';
			$func_name();
		}
	}
}

function lc_addon_pagedata()
{
	global $arr_addons;
	foreach($arr_addons as $addon_dir)
	{
		if(function_exists($addon_dir.'_pagedata')) {
			$func_name = $addon_dir.'_pagedata';
			$func_name();
		}
	}
}


function lc_check_addon_class($filename)
{
	global $order;
	if(defined('SYSTEM_ADDON')) {
		if(file_exists('addons/lc_'. SYSTEM_ADDON .'/catalog/includes/classes/'.$filename))
			require('addons/lc_'. SYSTEM_ADDON .'/catalog/includes/classes/'.$filename);
		else
			require(DIR_FS_CLASSES . $filename);
	} else {
		require(DIR_FS_CLASSES . $filename);
	}
}
?>