<?php
/*
  CRE Loaded , Open Source E-Commerce Solutions
  http://www.creloaded.com
  Portions  Copyright (c) 2003 osCommerce

     Chain Reaction Works, Inc
     Copyright &copy; 2005 - 2007 Chain Reaction Works, Inc.

   Released under the GNU General Public License
*/
require('includes/languages/' . $language . '/install_4.php');
require('includes/classes/tableProcessor.php');

  $script_filename = getenv('SCRIPT_FILENAME');

  $script_filename = str_replace('\\', '/', $script_filename);
  $script_filename = str_replace('//', '/', $script_filename);

  $dir_fs_www_root_array = explode('/', dirname($script_filename));
  $dir_fs_www_root = array();
  for ($i=0, $n=sizeof($dir_fs_www_root_array)-1; $i<$n; $i++) {
    $dir_fs_www_root[] = $dir_fs_www_root_array[$i];
  }
  $dir_fs_www_root = implode('/', $dir_fs_www_root) . '/';
?>

<h1>Database Import</h1>

<?php
  if ((osc_in_array('database', $_POST['install'])) || (osc_in_array('database_1', $_POST['install'])) ) {
    $db = array();
    $db['DB_SERVER'] = trim(stripslashes($_POST['DB_SERVER']));
    $db['DB_SERVER_USERNAME'] = trim(stripslashes($_POST['DB_SERVER_USERNAME']));
    $db['DB_SERVER_PASSWORD'] = trim(stripslashes($_POST['DB_SERVER_PASSWORD']));
    $db['DB_DATABASE'] = trim(stripslashes($_POST['DB_DATABASE']));

$use_pconnect = (isset($_POST['USE_PCONNECT']) ? $_POST['USE_PCONNECT'] : 'false');
$store_sessions = (isset($_POST['STORE_SESSIONS']) ? $_POST['STORE_SESSIONS'] : 'mysql');

    osc_db_connect($db['DB_SERVER'], $db['DB_SERVER_USERNAME'], $db['DB_SERVER_PASSWORD']);

    //$db_error = 'false';
    $sql_file = $dir_fs_www_root . SQL_SCHEMA;
    $sql_file_1= $dir_fs_www_root . SQL_BASEDATA;
    $sql_file_2= $dir_fs_www_root . SQL_CONFIGDATA;
    $sql_file_3= $dir_fs_www_root . SQL_DEMO_DATA;


    osc_set_time_limit(0);
    if (osc_in_array('database_1', $_POST['install'])) {
      osc_db_install($db['DB_DATABASE'], $sql_file);
      osc_db_install($db['DB_DATABASE'], $sql_file_1);
      osc_db_install($db['DB_DATABASE'], $sql_file_2);
    }

    //process the Addons if any
    if(count($arr_addons) > 0)  { //Addons available    	

      $tblProcessor = new tableProcessor();      

	  foreach($arr_addons as $addon_dir) {
		if(file_exists('../addons/'.$addon_dir.'/reference_db.xml')) {

		  // call the method to encode the current table
		  $currTable = $tblProcessor->getTableStructure($db['DB_SERVER'], $db['DB_SERVER_USERNAME'], $db['DB_SERVER_PASSWORD'], $db['DB_DATABASE']);

		  // call the method to encode the reference table
		  $refTable = file_get_contents('../addons/'.$addon_dir.'/reference_db.xml');

		  // now what is needed to conver the current table into the same structure as the reference table
		  $diffTable = $tblProcessor->diffTableStructure($currTable, $refTable, false, false);

		  $action_results = $tblProcessor->applyTableChanges($diffTable, $db['DB_SERVER'], $db['DB_SERVER_USERNAME'], $db['DB_SERVER_PASSWORD'], $db['DB_DATABASE']);

		  unset($refTable);
		  unset($currTable);
		  unset($diffTable);
		} 
	  }
    
    	//Insert the config data after table restructure
	  foreach($arr_addons as $addon_dir) {
		if(file_exists('../addons/'.$addon_dir.'/install.sql')) {
	      osc_db_install($db['DB_DATABASE'], '../addons/'.$addon_dir.'/install.sql');
		}	  	
	  }
	  //--------------- END -------------------
    }
    
    if (osc_in_array('database_2', $_POST['install'])){
      osc_db_install($db['DB_DATABASE'], $sql_file_3);
      //Copy the demo categories, products images
		//process the products images
		$query = "SELECT products_image, products_image_med, products_image_lrg, products_image_sm_1, products_image_xl_1, products_image_sm_2, products_image_xl_2
			,  products_image_sm_3,  products_image_xl_3,  products_image_sm_4,  products_image_xl_4,  products_image_sm_5,  products_image_xl_5,  products_image_sm_6
			,  products_image_xl_6  FROM " . $db['DB_DATABASE'] . ".products WHERE products_status=1 LIMIT 5000";
		$rs =  mysqli_query($db_link, $query);
		while($rw = mysqli_fetch_assoc($rs)) {
			foreach($rw as $fkey=>$fval) {
				if(trim($fval) != '') {
					@copy('images/demo_images/products/'.$fval, '../images/products/'.$fval);
				}
			}
		}

		//process the categories images
		$query = "SELECT categories_image FROM " . $db['DB_DATABASE'] . ".categories";
		$rs =  mysqli_query($db_link, $query);
		while($rw = mysqli_fetch_assoc($rs)) {
			if(trim($rw['categories_image']) != '') {
				@copy('images/demo_images/categories/'.$rw['categories_image'], '../images/categories/'.$rw['categories_image']);
			}
		}

		//process the Banner images
		$query = "SELECT banners_image FROM " . $db['DB_DATABASE'] . ".banners";
		$rs =  mysqli_query($db_link, $query);
		while($rw = mysqli_fetch_assoc($rs)) {
			if(trim($rw['banners_image']) != '') {
				@copy('images/demo_images/banners/'.$rw['banners_image'], '../images/banners/'.$rw['banners_image']);
			}
		}      
    }
    
    if ( (isset($db_error)) && ($db_error != 'false') ) {
?>
<!-- istall 4 load db error -->
<form name="install" action="install.php?step=5" method="post">

<p><?php echo TEXT_INSTALL_1 ;?></p>
<p><?php echo $db_error; ?></p>
<p><?php echo $data_error; ?></p>

<?php
      reset($_POST);

	  foreach($_POST as $key => $value) {
        if (($key != 'x') && ($key != 'y') && ($key != 'DB_TEST_CONNECTION')) {
          if (is_array($value)) {
            for ($i=0; $i<sizeof($value); $i++) {
              echo osc_draw_hidden_field($key . '[]', $value[$i]) . "\n";
            }
          } else {
            echo osc_draw_hidden_field($key, $value) . "\n";
          }
        }
      }
?>

<div style="text-align: center;">
  <p>
    <a href="index.php"><span class="installation-button-left">&nbsp;</span><span class="installation-button-middle"><?php echo BUTTON_NEW_CANCEL ;?></span><span class="installation-button-right">&nbsp;</span></a>

    <a href="javascript:void(0)" onclick="document.install.submit(); return false;"><span class="installation-button-left">&nbsp;</span><span class="installation-button-middle"><?php echo BUTTON_NEW_CONTINUE ;?></span><span class="installation-button-right">&nbsp;</span></a>
  </p>
</div>

</form>

<?php
    } else {
?>
<!-- db import -->
<form name="install" action="install.php?step=5" method="post">

<p><?php echo TEXT_INSTALL_3 ;?></p>

<?php
      reset($_POST);
	  foreach($_POST as $key=>$value) {
        if (($key != 'x') && ($key != 'y') && ($key != 'DB_TEST_CONNECTION')) {
          if (is_array($value)) {
            for ($i=0; $i<sizeof($value); $i++) {
              echo osc_draw_hidden_field($key . '[]', $value[$i]) . "\n";
            }
          } else {
            echo osc_draw_hidden_field($key, $value) . "\n";
          }
        }
      }
?>

<?php
      if (osc_in_array('configure', $_POST['install'])) {
?>
<p style="text-align: center;">
  <a href="javascript:void(0)" onclick="document.install.submit(); return false;"><span class="installation-button-left">&nbsp;</span><span class="installation-button-middle"><?php echo BUTTON_NEW_CONTINUE ;?></span><span class="installation-button-right">&nbsp;</span></a>
</p>
<?php
      } else {
?>
    <form name="install" action="install.php" method="post">
      <p style="text-align: center;">
        <a href="javascript:void(0)" onclick="document.install.submit(); return false;"><span class="installation-button-left">&nbsp;</span><span class="installation-button-middle"><?php echo BUTTON_NEW_CONTINUE ;?></span><span class="installation-button-right">&nbsp;</span></a>
      </p>
    </form>
<?php
      }
?>

</form>

<?php
    }
  }
?>
